## AlgDotCalculus

A project that examines decidability and algorithmic properties of the DOT family.

My thesis is [here](https://uwspace.uwaterloo.ca/handle/10012/14964).

Full DOT calculus is [here](https://github.com/amaurremi/dot-calculus).

### Directory structure

The following are correspondence between directories and chapters in my thesis.

| directory            | chapter      | progress                          |
|----------------------|--------------|-----------------------------------|
| [agda/](agda/)       | 3, 5(partly) | done                              |
| [dsub/](dsub/)       | 4            | done                              |
| dart/                |              | abandoned, no longer in interest. |
| [dint/](dint/)       | 5            | 100%                              |
| [mu-dart/](mu-dart/) | 6            | 100%                              |
| [jdot/](jdot/)       | 7            | 100%                              |

Detailed correspondences between theorems in thesis and formalization are listed
within the directories. The following are high-level explanations of the purposes of
the directories. 


* `agda/` contains various calculi for decidability investigation. Only this piece is
    done in Agda, in order to take the advantage of termination checking algorithm in
    Agda.
* `comm/` contains some of my early informal discussions. They appear to be not very
  useful in the final results.
* `dart/` contains another weakened form of DOT. It's abandoned since it's too simple.
* `dsub/` is a weakened form of DOT without intersection and recursive types.  Only
  one bounded type member is allowed.
* `dint/` is another weakened form of DOT. It's D<: + fields + intersection types +
  countably infinite number of field and type labels.
* `experiment/` is an experiment directory that contains mainly garbage.
* `lib/` contains external library dependencies.
* `share/` contains libraries that I wrote for this project.
* `mu-dart/` is yet another weakened form of DOT. This variant is `dart` + `mu`
  type. Adding `mu` type significantly puts on more complexity on type checker,
  due to introduction of cycles in typing context.
* `jdot/` is a simplified DOT which can have very simple algorithmic typing. (even
  simpler than mu-dart despite more expressiveness!)

### Dependencies

External library dependencies are in `lib/`. Following command can be used to sync up
these dependencies:

```bash
$ git submodule init
$ git submodule update
```

* Coq 8.8.2
* [Metalib](https://github.com/plclub/metalib)
* [coq-ext-lib](https://github.com/coq-ext-lib/coq-ext-lib) for monads.
* [Equations](https://github.com/mattam82/Coq-Equations)

For Agda,

* Agda 2.5.4.2
* [agda-stdlib](https://github.com/agda/agda-stdlib)


### How to build

I recently updated the repo with a `Makefile`, which allows the projects to be built
in a few key strokes. With a clean clone, git submodules will be automatically
pulled. 

``` bash
$ make dsub       # build dsub folder, pull and build dependencies if necessary
$ make mu-dart    # work similarly
```

Agda files can be built by simply running Agda:

``` bash
$ cd agda/
$ agda Everything.agda
```

### Complementary Materials

* [This article](https://hustmphrrr.github.io/blog/2019/agda-vs-coq.html) discusses
  for what technical reason I use both Coq and Agda in my thesis project.
