Set Implicit Arguments.
Require Import Definitions.
Require Import OperationProperties.

Section Weakening.
  Local Notation weakening ctor :=
    (forall G1 G2 G3 t T,
        ctor (G1 ++ G3) t T -> 
        ctor (G1 ++ G2 ++ G3) t T).

  Local Ltac ind_massage :=
    simpl; match goal with
    | [ |- context[_ :: _ ++ _ ++ _]] =>
      cofinite
    | _ =>
      intros; eauto
    end;
    match goal with
    | [ H : context[_ `notin` _ -> _] |- _ ] =>
      reassoc 4 with 2;
      match type of H with
      | context[_ ++ _ ++ _] =>
          eapply H; simpl_env
      end
    end.

  (** master tactic for this lemma *)
  Local Ltac boom := ind_massage; auto.

  Lemma weaken_typing_gen : weakening typing
  with weaken_subty_gen : weakening subty
  with weaken_def_gen : weakening typing_d
  with weaken_defs_gen : forall x G1 G2 G3 ds T,
      G1 ++ G3 ⊩[ x ][ ds ⦂ T ] ->
      G1 ++ G2 ++ G3 ⊩[ x ][ ds ⦂ T ].
  Proof.
    - clear weaken_typing_gen.
      intros. gen G2. dependent induction H; intros.
      all:try solve [econstructor; boom].
      econstructor; trivial. cofinite.
      reassoc 4 with 2. apply weaken_defs_gen.
        routine.
    - clear weaken_subty_gen.
      intros. gen G2. dependent induction H; intros.
      all:auto.
      2:eapply st_all; boom.
      all:eauto.
    - clear weaken_def_gen.
      intros. gen G2. dependent induction H; intros.
      all:econstructor; boom.
    - clear weaken_defs_gen.
      intros. gen G2. dependent induction H; intros.
      all:econstructor; boom.
  Qed.

End Weakening.

Lemma weaken_typing: forall G G' t T,
    G ⊢ t ⦂ T ->
    G' ++ G ⊢ t ⦂ T.
Proof.
  intros; reassoc 2 with 0; eapply weaken_typing_gen; try eassumption; trivial.
Qed.

Lemma weaken_subty: forall G G' T1 T2,
    G ⊢ T1 <⦂ T2 ->
    G' ++ G ⊢ T1 <⦂ T2.
Proof.
  intros; reassoc 2 with 0; eapply weaken_subty_gen; try eassumption; trivial.
Qed.
Local Hint Resolve weaken_typing weaken_subty.

Inductive ope_sub : env -> env -> Prop :=
| os_nil : ope_sub nil nil
| os_drop : forall G1 G2 x T,
    ope_sub G1 G2 ->
    ope_sub G1 ((x, T) :: G2)
| os_keep : forall G1 G2 x T1 T2,
    ope_sub G1 G2 ->
    G2 ⊢ T1 <⦂ T2 ->
    ope_sub ((x, T2) :: G1) ((x, T1) :: G2).
Local Hint Constructors ope_sub.

Section OpeProperties.
  
  Lemma ope_narrow_var :
    forall G G' x T,
      ope_sub G G' ->
      binds x T G ->
      G' ⊢ trm_var (avar_f x) ⦂ T.
  Proof.
    induction on ope_sub; intros.
    - routine.
    - apply IHope_sub in H.
      reassoc 2 with 1. apply weaken_typing.
      trivial.
    - tidy_up.
      + eapply ty_sub; simpl_env; eauto.
      + apply IHope_sub in H0.
        simpl_env. apply weaken_typing.
        trivial.
  Qed.
  Hint Resolve ope_narrow_var.
  
  Lemma ope_narrow_typing : forall G t T,
      G ⊢ t ⦂ T ->
      forall G',
        ope_sub G G' ->
        G' ⊢ t ⦂ T
  with ope_narrow_subty : forall G T U,
      G ⊢ T <⦂ U ->
      forall G',
        ope_sub G G' ->
        G' ⊢ T <⦂ U
  with ope_narrow_def : forall G d D,
      G ⊩ d ⦂ D ->
      forall G',
        ope_sub G G' ->
        G' ⊩ d ⦂ D
  with ope_narrow_defs : forall x G ds T,
      G ⊩[ x ][ ds ⦂ T ] ->
      forall G',
        ope_sub G G' ->
        G' ⊩[ x ][ ds ⦂ T ].
  Proof.
    - clear ope_narrow_typing.
      induction on typing; intros; eauto.
      + econstructor. cofinite.
        apply H0; simpl; auto.
      + econstructor; auto. cofinite.
        eapply ope_narrow_defs; eauto.
        simpl; auto.
      + econstructor; [eauto |].
        cofinite.
        apply H0; simpl; auto.
      + econstructor; [eauto |].
        cofinite.
        apply H0; simpl; auto.        
      + eapply ty_sub; eauto.
        
    - clear ope_narrow_subty.
      induction on subty; intros; auto.
      2:eapply st_all; auto; cofinite; simpl in *; auto.
      all:eauto.
      
    - clear ope_narrow_def.
      induction on typing_d; intros; eauto.

    - clear ope_narrow_defs.
      induction on typing_ds; intros; eauto.
  Qed.
  Hint Resolve ope_narrow_subty.
  
  Lemma ope_sub_trans : forall G1 G2 G3,
      ope_sub G1 G2 ->
      ope_sub G2 G3 ->
      ope_sub G1 G3.
  Proof.
    intros. gen G1. induction H0; intros; auto.
    invert H1; subst; eauto.
  Qed.

  Lemma ope_sub_refl : forall G,
      ope_sub G G.
  Proof. induction on env; routine. Qed.

  Lemma ope_sub_nil : forall G,
      ope_sub nil G.
  Proof. induction on env; routine. Qed.

  Lemma ope_sub_app_r : forall G G1 G2,
      ope_sub G G2 ->
      ope_sub G (G1 ++ G2).
  Proof. induction G1; routine. Qed.

End OpeProperties.  

Reserved Notation "G1 ⪯ G2" (at level 70).  
Inductive subenv: env -> env -> Prop :=
| se_empty : nil ⪯ nil
| se_grow: forall G G' x T T',
    G ⪯ G' ->
    G ⊢ T <⦂ T' ->
    (x, T) :: G ⪯ (x, T') :: G'
where "G1 ⪯ G2" := (subenv G1 G2).
Hint Constructors subenv.

Section Narrowing.
  
  Hint Resolve ope_sub_refl.
  
  Lemma narrow_subty : forall x S1 S2 G U1 U2,
    x ~ S2 ++ G ⊢ U1 <⦂ U2 ->
    G ⊢ S1 <⦂ S2 ->
    x ~ S1 ++ G ⊢ U1 <⦂ U2.
  Proof.
    intros.
    eapply ope_narrow_subty; eauto.
    simpl; auto.
  Qed.
    
  Lemma narrow_typing : forall x S1 S2 G t T,
      x ~ S2 ++ G ⊢ t ⦂ T ->
      G ⊢ S1 <⦂ S2 ->
      x ~ S1 ++ G ⊢ t ⦂ T.
  Proof.
    intros.
    eapply ope_narrow_typing; eauto.
    simpl; auto.
  Qed.

  Lemma subenv_to_ope_sub : forall G1 G2,
      G1 ⪯ G2 ->
      ope_sub G2 G1.
  Proof.
    induction on subenv; routine.
  Qed.

  Lemma subenv_refl : forall G,
      G ⪯ G.
  Proof.
    induction on env; routine.
  Qed.
  Local Hint Resolve subenv_refl.

  Lemma subenv_length : forall G1 G2,
      G1 ⪯ G2 ->
      length G1 = length G2.
  Proof.
    induction on subenv; routine.
  Qed.

  Lemma subenv_dom : forall G1 G2,
      G1 ⪯ G2 ->
      dom G1 [=] dom G2.
  Proof.
    induction on subenv; simpl; fsetdec.
  Qed.

  Lemma subenv_push : forall G1 G2 x T,
      G1 ⪯ G2 ->
      (x, T) :: G1 ⪯ (x, T) :: G2.
  Proof. induction G1; eroutine. Qed.

  Lemma subenv_app_gen : forall G1 G2,
      G1 ⪯ G2 ->
      forall G3 G4,
        G3 ⪯ G4 ->
        G1 ++ G3 ⪯ G2 ++ G4.
  Proof.
    induction on subenv; routine.
    constructor; auto.
    reassoc 2 with 1 2.
    apply weaken_subty_gen.
    simpl_env. trivial.
  Qed.

  Lemma subenv_app : forall G G1 G2,
      G1 ⪯ G2 ->
      G ++ G1 ⪯ G ++ G2.
  Proof.
    intros. eapply subenv_app_gen; auto.
  Qed.
  
  Lemma subenv_trans : forall G1 G2,
      subenv G1 G2 ->
      forall G3,
        subenv G2 G3 ->
        subenv G1 G3.
  Proof.
    induction on subenv; routine.
    constructor; auto.
    apply ope_narrow_subty with (G' := G) in H7.
    - eauto.
    - apply subenv_to_ope_sub.
      eauto.
  Qed.

  Lemma narrow_subty_gen : forall G S U,
      G ⊢ S <⦂ U ->
      forall G',
        G' ⪯ G ->
        G' ⊢ S <⦂ U.
  Proof.
    intros.
    eapply ope_narrow_subty; eauto.
    apply subenv_to_ope_sub. trivial.
  Qed.

  Lemma narrow_typing_gen : forall G t T,
      G ⊢ t ⦂ T ->
      forall G',
        G' ⪯ G ->
        G' ⊢ t ⦂ T.
  Proof.
    intros.
    eapply ope_narrow_typing; eauto.
    apply subenv_to_ope_sub. trivial.
  Qed.
  
End Narrowing.
  
Local Hint Resolve subst_open_comm_typ subst_open_comm_dec.

Definition subst_env (z u: var) (G : env) : env := map (substi z u) G.

Global Instance SubstiEnv : CanSubst env := { substi := subst_env }.

Section SubstProps.
  
  Lemma same_ldom : forall x y (ds : defs),
      ldom (subst_defs x y ds) = ldom ds.
  Proof using.
    induction on defs.
    - routine.
    - destruct d; routine.
  Qed.

  Lemma substi_resp_luniq : forall x y (ds : defs),
      luniq ds ->
      luniq (subst_defs x y ds).
  Proof using.
    induction on defs.
    - routine.
    - destruct d; routine;
        constructor; try rewrite same_ldom; routine.
  Qed.
  Hint Resolve substi_resp_luniq.
  
  Lemma substi_resp_wf_lab_def : forall x y l d,
      wf_lab_def (l, d) ->
      wf_lab_def (l, substi x y d).
  Proof using. destr on def; routine. Qed.
  Hint Resolve substi_resp_wf_lab_def.  

  Lemma substi_resp_all_wf_lab_def : forall x y (ds : defs),
      Forall wf_lab_def ds ->
      Forall wf_lab_def (substi x y ds).
  Proof using.
    induction on defs.
    - routine.
    - destruct d; routine.
  Qed.
  Hint Resolve substi_resp_all_wf_lab_def.
  
End SubstProps.

Lemma free_all : forall x y T (G : env),
    y `notin` fv G ->
    binds x T G ->
    y `notin` fv T.
Proof. induction on env; routine. Qed.
Local Hint Resolve free_all.

Section SubstRules.
  Variable y : atom.
  Variable S : typ.

  Local Notation subst_for ctor :=
    (forall G t T,
        ctor G t T ->
        forall G1 G2 x,
          G = G2 ++ x ~ S ++ G1 ->
          uniq G ->
          x `notin` fv G1 ->
          substi x y G2 ++ G1 ⊢ trm_var y ⦂ substi x y S ->
          ctor (substi x y G2 ++ G1) (substi x y t) (substi x y T)).

  Hint Resolve get_to_binds.
  Lemma subst_var : forall G1 G2 x0 y x T S,
      binds x0 T (G2 ++ x ~ S ++ G1) ->
      uniq (G2 ++ x ~ S ++ G1) ->
      x `notin` fv G1 ->
      substi x y G2 ++ G1 ⊢ trm_var y ⦂ substi x y S ->
      substi x y G2 ++ G1 ⊢ trm_var (subst_fvar x y x0) ⦂ substi x y T.
  Proof.
    intros. pose proof (binds_to_get _ _ H H0).
    repeat lazymatch goal with
           | H : get _ (_ ++ _) = _ |- _ =>
             apply get_uniq_app in H;
               [ | solve_uniq];
               destruct H; destruct_conjs
           end; tidy_up.
    - constructor. apply binds_app_2. apply binds_map_2.
      auto.
    - exrewrite subst_fresh_typ.
      + auto.
      + eapply free_all;
          [| apply get_to_binds; eassumption].
        simpl. solve_notin.
  Qed.
  Hint Resolve subst_var.

  Definition subst_open_comm_all :=
    conj subst_open_comm_avar $
    conj subst_open_comm_typ $
    conj subst_open_comm_dec $
    conj subst_open_comm_trm $
    conj subst_open_comm_val $
    conj subst_open_comm_def subst_open_comm_defs.
  
  Ltac comm_subst_open :=
    repeat match goal with
           | |- context[?f _ (subst_fvar _ _ _) (substi _ _ _)] =>
             lazymatch f with
             | context[open_rec] => idtac
             end;
             exrewrite <- subst_open_comm_all
           end.
  
  Ltac folding :=
    fold_cls;
    repeat match goal with
           | |- context[if ?x == ?y then ?z else ?x] =>
             change (if x == y then z else x) with (subst_fvar y z x)
           end.
  
  Ltac prep :=
    intros; subst; simpl; fold_substi;
    try exrewrite subst_open_comm_typ;
    folding.

  Ltac xform_ctx :=
    reassoc 3 with 2;
    match goal with
    | |- context[?v ~ substi ?x ?y ?T ++ substi ?x ?y ?G] =>
      change (v ~ substi x y T ++ substi x y G)
        with (substi x y (v ~ T ++ G))
    end.

  Ltac xform := comm_subst_open; open_substi_transform; try xform_ctx.

  Ltac weakening := simpl; simpl_env; auto.
  Ltac finish := xform; solve [ctx_app; eauto].

  Ltac closure H :=
    auto; cofinite; xform; eapply H; weakening.
  
  Hint Resolve subst_open_comm_trm subst_open_comm_def subst_open_comm_defs.
  Hint Resolve substi_resp_luniq.
  
  Lemma subst_typing_gen : subst_for typing
  with subst_subty_gen : subst_for subty
  with subst_def_gen : subst_for typing_d
  with subst_defs_gen : forall z G ds T,
      G ⊩[ z ][ ds ⦂ T ] ->
      forall G1 G2 x,
        G = G2 ++ x ~ S ++ G1 ->
        uniq G ->
        x `notin` fv G1 ->
        substi x y G2 ++ G1 ⊢ trm_var y ⦂ substi x y S ->
        substi x y G2 ++ G1 ⊩[ subst_fvar x y z ][ substi x y ds ⦂ substi x y T ].
  Proof.
    - clear subst_typing_gen.
      induction on typing; prep; eauto 4;
        try solve [econstructor; finish].
      
      + econstructor; closure H0.
      + econstructor; auto.
        cofinite. xform.
        assert (x0 = subst_fvar x y x0) by routine.
        rewrite H1 at 1.
        eapply subst_defs_gen; weakening.
      + rewrite subst_open_comm_dec.
        apply ty_mu_d_e. finish.
      + econstructor; closure H0.
      + econstructor; closure H0.

    - clear subst_subty_gen.
      induction on subty; prep; eauto 4.

      + eapply st_all; auto. closure H0.
      + eapply st_sel1.
        instantiate (1 := substi x0 y U).
        change (substi x0 y G2 ++ G1 ⊢ substi x0 y (trm_var x) ⦂
                       substi x0 y (typ_dec $ dec_bnd A S0 U)).
        eapply subst_typing_gen; eauto.
      + eapply st_sel2.
        instantiate (1 := substi x0 y S0).
        change (substi x0 y G2 ++ G1 ⊢ substi x0 y (trm_var x) ⦂
                       substi x0 y (typ_dec $ dec_bnd A S0 U)).
        eapply subst_typing_gen; eauto.

    - clear subst_def_gen.
      induction on typing_d; prep; eauto 4.

    - clear subst_defs_gen.
      induction on typing_ds;
        intros; simpl; constructor; auto;
          prep; xform; eauto 4.
  Qed.

  (** specialization. *)
  Section Specialization.
    
    Local Notation subst_for' ctor :=
      (forall G t T x,
          ctor (x ~ S ++ G) t T ->
          uniq (x ~ S ++ G) ->
          x `notin` fv G ->
          G ⊢ trm_var y ⦂ substi x y S ->
          ctor G (substi x y t) (substi x y T)).

    Ltac boom :=
      intros;
      match goal with
      | |- _ ?G _ (substi ?x ?y _) =>
        change G with (substi x y nil ++ G);
        (eapply subst_typing_gen || eapply subst_def_gen ||
         eapply subst_defs_gen || eapply subst_subty_gen); eauto
      end.
    
    Lemma subst_ty_trm : subst_for' typing.
    Proof using. boom. Qed.

    Lemma subst_ty_def : subst_for' typing_d.
    Proof using. boom. Qed.
    
    Lemma subst_ty_defs : forall z G ds T x,
        x ~ S ++ G ⊩[ z][ ds ⦂ T ] ->
        uniq (x ~ S ++ G) ->
        x `notin` fv G ->
        G ⊢ trm_var y ⦂ substi x y S ->
        G ⊩[ subst_fvar x y z ][ substi x y ds ⦂ substi x y T ].
    Proof using. boom. Qed.

    Lemma subst_subty : subst_for' subty.
    Proof using. boom. Qed.
    
  End Specialization.

End SubstRules.

Section Renaming.

  Ltac prelude :=
    intros;
    try erewrite subst_intro_typ;
    try erewrite subst_intro_trm;
    try erewrite subst_intro_def;
    try erewrite subst_intro_defs;
    try erewrite subst_intro_dec.

  Ltac fin :=
    try (eapply subst_ty_trm);
    routine;
    try (simpl; fold_substi; fold_open_rec;
         erewrite subst_fresh_typ
         || erewrite subst_fresh_dec);
    routine.

  Ltac boom := prelude; fin.
  
  Lemma renaming_trm : forall G z t T U (x : var),
      uniq G ->
      z `notin` fv G `union` fv T `union` fv U `union` fv t ->
      z ~ U ++ G ⊢ open z t ⦂ open z T ->
      G ⊢ trm_var x ⦂ U ->
      G ⊢ open x t ⦂ open x T.
  Proof using. boom. Qed.

  Lemma typing_in_dom : forall G x T,
      G ⊢ trm_var $ avar_f x ⦂ T ->
      x `in` dom G.
  Proof.
    dep induction on typing; routine.
    eauto using binds_In.
  Qed.
  
  Lemma renaming_defs : forall G z T ds (x : var),
      uniq G ->
      z `notin` fv G `union` fv T `union` fv ds ->
      z ~ T ++ G ⊩[ z ][ ds ⦂ T ] ->
      G ⊢ trm_var $ avar_f x ⦂ T ->
      G ⊩[ x ][ ds ⦂ T].
  Proof using.
    intros.
    rewrite <- (@subst_fresh_typ z x T); set solve.
    rewrite <- (@subst_fresh_defs z x ds); set solve.
    pose proof (typing_in_dom H2).
    simpl in *.
    assert (z `notin` singleton x) by fsetdec.
    assert (x <> z) by routine.
    assert (x = subst_fvar z x z) by routine.
    rewrite H6 at 1.
    eapply subst_ty_defs; fin.
  Qed.

  Lemma renaming_subty : forall G z T1 T2 U (x : var),
      uniq G ->
      z `notin` fv G `union` fv T1 `union` fv T2 `union` fv U ->
      z ~ U ++ G ⊢ open z T1 <⦂ open z T2 ->
      G ⊢ trm_var x ⦂ U ->
      G ⊢ open x T1 <⦂ open x T2.
  Proof using.
    intros.
    erewrite subst_intro_typ.
    erewrite subst_intro_typ with (t := T2).
    eapply subst_subty.
    all:try eauto.
    routine.
    erewrite subst_fresh_typ; routine.
  Qed.
  
  Lemma renaming_fresh : forall L G T u U (x : var),
      uniq G ->
      (forall x, x `notin` L -> x ~ T ++ G ⊢ open x u ⦂ U) ->
      G ⊢ trm_var x ⦂ T ->
      G ⊢ open x u ⦂ U.
  Proof using. intros;
    let z := fresh in
    pick_fresh z; prelude;
      try instantiate (1 := z);
      try eexrewrite <- subst_fresh_typ;
      fin.
  Qed.

End Renaming.

Lemma open_subst_trm : forall x S G t U y,
    uniq G ->
    x `notin` fv G `union` fv S `union` fv U `union` fv t ->
    y `notin` fv G `union` fv S `union` fv U `union` fv t `union` singleton x ->
    x ~ S ++ G ⊢ open x t ⦂ open x U ->
    y ~ S ++ G ⊢ open y t ⦂ open y U.
Proof.
  intros.
  apply renaming_trm with (z := x) (U := S); routine.
  simpl_env.
  apply weaken_typing_gen. trivial.
Qed.

Lemma open_subst_subty : forall x S G U1 U2 y,
    uniq G ->
    x `notin` fv G `union` fv S `union` fv U1 `union` fv U2 ->
    y `notin` fv G `union` fv S `union` fv U1 `union` fv U2 `union` singleton x ->
    x ~ S ++ G ⊢ open x U1 <⦂ open x U2 ->
    y ~ S ++ G ⊢ open y U1 <⦂ open y U2.
Proof.
  intros.
  apply renaming_subty with (z := x) (U := S); routine.
  simpl_env.
  apply weaken_subty_gen. trivial.
Qed.

Lemma open_subst_defs : forall x y G ds T,
    uniq G ->
    x `notin` fv G `union` fv ds `union` fv T ->
    y `notin` fv G `union` fv ds `union` fv T `union` singleton x ->
    x ~ T ++ G ⊩[ x ][ ds ⦂ T ] ->
    y ~ T ++ G ⊩[ y ][ ds ⦂ T].
Proof.
  intros.
  apply renaming_defs with (z := x); routine.
  simpl_env.
  apply weaken_defs_gen. trivial.
Qed.
