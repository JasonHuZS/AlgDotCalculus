Set Implicit Arguments.

Require Export StdSetup.

Inductive avar : Set :=
| avar_b : nat -> avar
| avar_f : var -> avar.

Hint Constructors avar.

Coercion avar_b : nat >-> avar.
Coercion avar_f : var >-> avar.

Instance EqAvar : EqDec_eq avar := { }.
Proof. decide equality. apply Nat.eq_dec. Defined.

Inductive typ : Set :=
| typ_top : typ
| typ_bot : typ
| typ_sel : avar -> typ_label -> typ
| typ_all : typ -> typ -> typ
| typ_dec : dec -> typ
| typ_mu  : dec -> typ
| typ_and : typ -> typ -> typ
with
dec : Set :=
| dec_bnd : typ_label -> typ -> typ -> dec
| dec_fld : trm_label -> typ -> dec.
Hint Constructors typ dec.

Inductive trm : Set :=
| trm_var : avar -> trm
| trm_val : val -> trm
| trm_sel : avar -> trm_label -> trm
| trm_app : avar -> avar -> trm
| trm_let : option typ -> trm -> trm -> trm
with
val : Set :=
| val_obj : typ -> defs -> val
| val_lam : typ -> trm -> val
with
def : Set :=
| def_typ : typ_label -> typ -> def
| def_trm : trm_label -> trm -> def
with
defs : Set :=
| defs_nil : defs
| defs_cons : def -> defs -> defs.  
Hint Constructors trm val def defs.

Section LocalClosure.

  Inductive lc_avar_at : nat -> avar -> Prop :=
  | lc_ab : forall {n m}, n < m -> lc_avar_at m (avar_b n)
  | lc_af : forall {n x}, lc_avar_at n (avar_f x).

  Global Instance LcAvar : LC avar := { lc_at := lc_avar_at }.

  Inductive lc_typ_at : nat -> typ -> Prop :=
  | lc_typ_top : forall {n}, lc_typ_at n typ_top
  | lc_typ_bot : forall {n}, lc_typ_at n typ_bot
  | lc_typ_sel : forall {n x} A, lc_at n x -> lc_typ_at n (typ_sel x A)
  | lc_typ_all : forall {n T U}, lc_typ_at n T -> lc_typ_at (S n) U ->
                            lc_typ_at n (typ_all T U)
  | lc_typ_dec : forall {n D},
      lc_dec_at n D ->
      lc_typ_at n (typ_dec D)
  | lc_typ_mu : forall {n D},
      lc_dec_at (S n) D ->
      lc_typ_at n (typ_mu D)
  | lc_typ_and : forall {n S U},
      lc_typ_at n S -> lc_typ_at n U ->
      lc_typ_at n (typ_and S U)
  with
  lc_dec_at : nat -> dec -> Prop :=
  | lc_dec_bnd : forall {n S U} A,
      lc_typ_at n S -> lc_typ_at n U ->
      lc_dec_at n (dec_bnd A S U)
  | lc_dec_fld : forall {n T} a,
      lc_typ_at n T ->
      lc_dec_at n (dec_fld a T).

  Global Instance LcTyp : LC typ := { lc_at := lc_typ_at }.
  Global Instance LcDec : LC dec := { lc_at := lc_dec_at }.
  
  Inductive lc_trm_at : nat -> trm -> Prop :=
  | lc_trm_var : forall {n v}, lc_at n v -> lc_trm_at n (trm_var v)
  | lc_trm_val : forall {n vl}, lc_val_at n vl -> lc_trm_at n (trm_val vl)
  | lc_trm_sel : forall {n x a}, lc_at n x -> lc_trm_at n (trm_sel x a)
  | lc_trm_app : forall {n x y}, lc_at n x -> lc_at n y -> lc_trm_at n (trm_app x y)
  | lc_trm_let1 : forall {n t1 t2},
      lc_trm_at n t1 -> lc_trm_at (S n) t2 ->
      lc_trm_at n (trm_let None t1 t2)
  | lc_trm_let2 : forall {n T t1 t2},
      lc_at n T ->
      lc_trm_at n t1 -> lc_trm_at (S n) t2 ->
      lc_trm_at n (trm_let (Some T) t1 t2)
  with
  lc_val_at : nat -> val -> Prop :=
  | lc_val_bnd : forall {n T ds}, lc_at n T -> lc_defs_at (S n) ds -> lc_val_at n (val_obj T ds)
  | lc_val_lam : forall {n T t}, lc_at n T -> lc_trm_at (S n) t ->
                            lc_val_at n (val_lam T t)
  with
  lc_defs_at : nat -> defs -> Prop :=
  | lc_defs_nil : forall n, lc_defs_at n defs_nil
  | lc_defs_cons : forall {n d ds}, lc_def_at n d -> lc_defs_at n ds -> lc_defs_at n (defs_cons d ds)
  with
  lc_def_at : nat -> def -> Prop :=
  | lc_def_typ : forall {n T} A, lc_at n T -> lc_def_at n (def_typ A T)
  | lc_def_trm : forall {n t} a, lc_trm_at n t -> lc_def_at n (def_trm a t).

  Global Instance LcTrm : LC trm := { lc_at := lc_trm_at }.
  Global Instance LcVal : LC val := { lc_at := lc_val_at }.
  Global Instance LcDef : LC def := { lc_at := lc_def_at }.
  Global Instance LcDefs : LC defs := { lc_at := lc_defs_at }.
  
End LocalClosure.

Hint Constructors lc_avar_at lc_typ_at lc_dec_at.
Hint Constructors lc_trm_at lc_val_at lc_def_at lc_defs_at.

Section OpeningDefinitions.
  
  Definition open_rec_avar (k : nat) (u : var) (a : avar) : avar :=
    match a with
    | avar_b i => if k == i then avar_f u else avar_b i
    | avar_f x => avar_f x
    end.

  Fixpoint open_rec_typ (k : nat) (u : var) (T : typ) : typ :=
    match T with
    | typ_top => typ_top
    | typ_bot => typ_bot
    | typ_sel x A => typ_sel (open_rec_avar k u x) A
    | typ_all T U => typ_all (open_rec_typ k u T) $ open_rec_typ (S k) u U
    | typ_dec D => typ_dec (open_rec_dec k u D)
    | typ_mu D => typ_mu (open_rec_dec (S k) u D)
    | typ_and T U => typ_and (open_rec_typ k u T) $ open_rec_typ k u U
    end
  with
  open_rec_dec (k : nat) (u : var) (D : dec) : dec :=
    match D with
    | dec_bnd A T U => dec_bnd A (open_rec_typ k u T) (open_rec_typ k u U)
    | dec_fld a T => dec_fld a (open_rec_typ k u T)
    end.

  Global Instance OpenAvar : CanOpen avar := { open_rec := open_rec_avar }.
  Global Instance OpenTyp : CanOpen typ := { open_rec := open_rec_typ }.
  Global Instance OpenDec : CanOpen dec := { open_rec := open_rec_dec }.
  
  Fixpoint open_rec_trm k u (t : trm) : trm :=
    match t with
    | trm_var x => trm_var $ open_rec_avar k u x
    | trm_val v => trm_val $ open_rec_val k u v
    | trm_sel x a => trm_sel (open_rec k u x) a
    | trm_app x y => trm_app (open_rec_avar k u x) $ open_rec_avar k u y
    | trm_let T s t => trm_let (option_map (open_rec k u) T)
                         (open_rec_trm k u s) $ open_rec_trm (S k) u t
    end
  with
  open_rec_val k u (vl : val) : val :=
    match vl with
    | val_obj T ds => val_obj (open_rec k u T) (open_rec_defs (S k) u ds)
    | val_lam T t => val_lam (open_rec_typ k u T) $ open_rec_trm (S k) u t
    end
  with
  open_rec_def k u (d : def) : def :=
    match d with
    | def_typ A T => def_typ A (open_rec k u T)
    | def_trm a t => def_trm a (open_rec_trm k u t)
    end
  with
  open_rec_defs k u (ds : defs) : defs :=
    match ds with
    | defs_nil => defs_nil
    | defs_cons d ds => defs_cons (open_rec_def k u d) (open_rec_defs k u ds)
    end.

  Global Instance OpenTrm : CanOpen trm := { open_rec := open_rec_trm }.
  Global Instance OpenVal : CanOpen val := { open_rec := open_rec_val }.
  Global Instance OpenDef : CanOpen def := { open_rec := open_rec_def }.
  Global Instance OpenDefs : CanOpen defs := { open_rec := open_rec_defs }.
  
End OpeningDefinitions.

Section ClosingDefinitions.

  Definition close_rec_avar (k : nat) (u : var) (a : avar) : avar :=
    match a with
    | avar_b n => avar_b n
    | avar_f x => if x == u then avar_b k else avar_f x
    end.

  Fixpoint close_rec_typ (k : nat) (u : var) (T : typ) : typ :=
    match T with
    | typ_top => typ_top
    | typ_bot => typ_bot
    | typ_sel x A => typ_sel (close_rec_avar k u x) A
    | typ_all T U => typ_all (close_rec_typ k u T) $ close_rec_typ (S k) u U
    | typ_dec D => typ_dec (close_rec_dec k u D)
    | typ_mu D => typ_mu (close_rec_dec (S k) u D)
    | typ_and T U => typ_and (close_rec_typ k u T) $ close_rec_typ k u U
    end
  with
  close_rec_dec (k : nat) (u : var) (D : dec) : dec :=
    match D with
    | dec_bnd A T U => dec_bnd A (close_rec_typ k u T) (close_rec_typ k u U)
    | dec_fld a T => dec_fld a (close_rec_typ k u T)
    end.

  Global Instance CloseAvar : CanClose avar := { close_rec := close_rec_avar }.
  Global Instance CloseTyp : CanClose typ := { close_rec := close_rec_typ }.
  Global Instance CloseDec : CanClose dec := { close_rec := close_rec_dec }.
  
  Fixpoint close_rec_trm k u (t : trm) : trm :=
    match t with
    | trm_var x => trm_var $ close_rec_avar k u x
    | trm_val v => trm_val $ close_rec_val k u v
    | trm_sel x a => trm_sel (close_rec k u x) a
    | trm_app x y => trm_app (close_rec_avar k u x) $ close_rec_avar k u y
    | trm_let T s t => trm_let (option_map (close_rec k u) T)
                              (close_rec_trm k u s) $ close_rec_trm (S k) u t
    end
  with
  close_rec_val k u (vl : val) : val :=
    match vl with
    | val_obj T ds => val_obj (close_rec k u T) (close_rec_defs (S k) u ds)
    | val_lam T t => val_lam (close_rec_typ k u T) $ close_rec_trm (S k) u t
    end
  with
  close_rec_def k u (d : def) : def :=
    match d with
    | def_typ A T => def_typ A (close_rec k u T)
    | def_trm a t => def_trm a (close_rec_trm k u t)
    end
  with
  close_rec_defs k u (ds : defs) : defs :=
    match ds with
    | defs_nil => defs_nil
    | defs_cons d ds => defs_cons (close_rec_def k u d) (close_rec_defs k u ds)
    end.

  Global Instance CloseTrm : CanClose trm := { close_rec := close_rec_trm }.
  Global Instance CloseVal : CanClose val := { close_rec := close_rec_val }.
  Global Instance CloseDef : CanClose def := { close_rec := close_rec_def }.
  Global Instance CloseDefs : CanClose defs := { close_rec := close_rec_defs }.

End ClosingDefinitions.

Section Substitutions.
  
  Definition subst_avar (z u: var) (a : avar) : avar :=
    match a with
    | avar_b i as a => a
    | avar_f x => avar_f $ if x == z then u else x
    end.

  Fixpoint subst_typ (z u: var) (T : typ) : typ :=
    match T with
    | typ_top => typ_top
    | typ_bot => typ_bot
    | typ_sel x A => typ_sel (subst_avar z u x) A
    | typ_all T U => typ_all (subst_typ z u T) (subst_typ z u U)
    | typ_dec D => typ_dec (subst_dec z u D)
    | typ_mu D => typ_mu (subst_dec z u D)
    | typ_and T U => typ_and (subst_typ z u T) (subst_typ z u U)
    end
  with
  subst_dec (z u: var) (D : dec) : dec :=
    match D with
    | dec_bnd A T U => dec_bnd A (subst_typ z u T) (subst_typ z u U)
    | dec_fld a T => dec_fld a (subst_typ z u T)
    end.

  Global Instance SubstAvar : CanSubst avar := { substi := subst_avar }.
  Global Instance SubstTyp : CanSubst typ := { substi := subst_typ }.
  Global Instance SubstDec : CanSubst dec := { substi := subst_dec }.
  
  Fixpoint subst_trm (z u: var) (t : trm) : trm :=
    match t with
    | trm_var x => trm_var $ subst_avar z u x
    | trm_val v => trm_val $ subst_val z u v
    | trm_app x y => trm_app (subst_avar z u x) $ subst_avar z u y
    | trm_sel x a => trm_sel (subst_avar z u x) a
    | trm_let T t1 t2 => trm_let (option_map (substi z u) T)
                                (subst_trm z u t1) $ subst_trm z u t2
    end
  with
  subst_val (z u: var) (v : val) : val :=
    match v with
    | val_obj T ds => val_obj (substi z u T) (subst_defs z u ds)
    | val_lam T t => val_lam (substi z u T) (subst_trm z u t)
    end
  with
  subst_defs (z u : var) (ds : defs) : defs :=
    match ds with
    | defs_nil => defs_nil
    | defs_cons d ds => defs_cons (subst_def z u d) (subst_defs z u ds)
    end
  with
  subst_def (z u : var) (d : def) : def :=
    match d with
    | def_typ A T => def_typ A (substi z u T)
    | def_trm a t => def_trm a (subst_trm z u t)
    end.
  
  Global Instance SubstTrm : CanSubst trm := { substi := subst_trm }.
  Global Instance SubstVal : CanSubst val := { substi := subst_val }.
  Global Instance SubstDef : CanSubst def := { substi := subst_def }.
  Global Instance SubstDefs : CanSubst defs := { substi := subst_defs }.

End Substitutions.

Notation env := (list (var * typ)).

Hint Rewrite -> dom_app : meta_ext.

Section FreeVariables.

  Definition fv_avar (v : avar) : vars :=
    match v with
    | avar_b _ => {}
    | avar_f x => {{ x }}
    end.

  Local Ltac empty := exact {}.
  Local Ltac singleton :=
    apply fv_avar; assumption.
  Local Ltac recur f :=
    apply f; assumption.
  Local Ltac recur2 f :=
    lazymatch type of f with
    | forall _: ?T, _ =>
      lazymatch goal with
      | T1 : T, T2 : T |- vars =>
        exact (f T1 \u f T2)
      end
    end.
  
  Fixpoint fv_typ (T : typ) : vars :=
    match T with
    | typ_top | typ_bot => {}
    | typ_sel _ _ => ltac:(singleton)
    | typ_dec D | typ_mu D => fv_dec D
    | typ_all _ _ | typ_and _ _ => ltac:(recur2 fv_typ)
    end
  with
  fv_dec (D : dec) : vars :=
    match D with
    | dec_bnd A T U => ltac:(recur2 fv_typ)
    | dec_fld a T => ltac:(recur fv_typ)
    end.

  Global Instance FvAvar : HasFv avar := { fv := fv_avar }.
  Global Instance FvTyp : HasFv typ := { fv := fv_typ }.
  Global Instance FvDec : HasFv dec := { fv := fv_dec }.

  Fixpoint fv_trm (t : trm) : vars :=
    match t with
    | trm_var _ => ltac:(singleton)
    | trm_val _ => ltac:(recur fv_val)
    | trm_app x y => fv_avar x \u fv_avar y
    | trm_sel _ _ => ltac:(singleton)
    | trm_let T _ _ => match T with
                      | Some T => fv T
                      | None => empty
                      end `union` ltac:(recur2 fv_trm)
    end
  with
  fv_val (vl : val) : vars :=
    match vl with
    | val_obj T ds => fv T `union` fv_defs ds
    | val_lam T t => fv_typ T \u fv_trm t
    end
  with
  fv_defs (ds : defs) : vars :=
    match ds with
    | defs_nil => {}
    | defs_cons d ds => fv_def d `union` fv_defs ds
    end
  with
  fv_def (d : def) : vars :=
    match d with
    | def_typ A T => fv T
    | def_trm a t => fv_trm t
    end.

  Global Instance FvTrm : HasFv trm := { fv := fv_trm }.
  Global Instance FvVal : HasFv val := { fv := fv_val }.
  Global Instance FvDef : HasFv def := { fv := fv_def }.
  Global Instance FvDefs : HasFv defs := { fv := fv_defs }.

  Definition fv_values {T : Type} (f : T -> atoms)
             (l : list (atom * T)) : atoms :=
    fold_right (fun (b : (atom * T)) a =>
                  a \u let (_, t) := b in f t) {} l.

  Lemma fv_unpack : forall T (f : T -> _) e1 e2,
      fv_values f (e1 ++ e2) [=] fv_values f e1 `union` fv_values f e2.
  Proof.
    induction on list; intros. simpl.
    - set solve.
    - routine.
      + rewrite IHlist in H2. fsetdec.
      + rewrite IHlist. fsetdec.
  Qed.
  
  Global Instance FvEnv : HasFv env := { fv := fun e => dom e `union` fv_values fv e }.

  Hint Rewrite -> fv_unpack : meta_ext.
  
  Lemma fv_union : forall (e1 e2 : env) , fv (e1 ++ e2) [=] fv e1 `union` fv e2.
  Proof.
    intros; simpl. 
    apply AtomSetProperties.subset_antisym;
      set solve.
  Qed.
  
End FreeVariables.

Hint Rewrite -> fv_union : meta_ext.

Ltac gather_atoms ::=
  let A := gather_atoms_with (fun x : atoms => x) in
  let B := gather_atoms_with (fun x : atom => singleton x) in
  let C := gather_atoms_with (fun x : env => fv x) in
  let D := gather_atoms_with fv_avar in
  let E := gather_atoms_with fv_typ in
  let F := gather_atoms_with fv_trm in
  let G := gather_atoms_with fv_val in
  let H := gather_atoms_with fv_def in
  let I := gather_atoms_with fv_defs in
  let J := gather_atoms_with fv_dec in
  let K := gather_atoms_with (fun l : list atom => AtomSetProperties.of_list l) in
  constr:(A `union` B `union` C `union` D `union` E
            `union` F `union` G `union` H `union` I `union` J `union` K
         ).

Definition lab_def_of D : label :=
  match D with
  | def_typ A _ => A
  | def_trm a _ => a
  end.

Inductive wf_lab_def : label * def -> Prop :=
| wf_ldef_typ : forall X T, wf_lab_def (label_typ X, def_typ X T)
| wf_ldef_trm : forall x t, wf_lab_def (label_trm x, def_trm x t).
Hint Constructors wf_lab_def.

Fixpoint defs_to_list' (DS : defs) :=
  match DS with
  | defs_nil => nil
  | defs_cons d DS' => (lab_def_of d, d) :: defs_to_list' DS'
  end.

Fixpoint defs_from_list' (l : list (label * def)) :=
  match l with
  | nil => defs_nil
  | cons (l, d) l' => defs_cons d $ defs_from_list' l'
  end.

Fixpoint defs_append' (DS1 : defs) (DS2 : defs) : defs :=
  match DS1 with
  | defs_nil => DS2
  | defs_cons D DS1' => defs_cons D $ defs_append' DS1' DS2
  end.

Instance DefsListCond : ListIsoCond (label * def) defs :=
  {
    to_list' := defs_to_list';
    from_list' := defs_from_list';
    append' := defs_append';
    cond := wf_lab_def
  }.
Proof.
  all:induction on defs || induction on list; routine.
  - invert H3; routine.
  - constructor; trivial.
    destruct d; routine.
Defined.

Coercion defs_to_list' : defs >-> list.

Fixpoint defs_to_list (DS : defs) :=
  match DS with
  | defs_nil => nil
  | defs_cons D DS' => cons D $ defs_to_list DS'
  end.

Fixpoint defs_from_list (l : list def) :=
  match l with
  | nil => defs_nil
  | cons D l' => defs_cons D (defs_from_list l')
  end.

Fixpoint defs_append (DS1 DS2 : defs) :=
  match DS1 with
  | defs_nil => DS2
  | defs_cons D DS1' => defs_cons D (defs_append DS1' DS2)
  end.

Instance DefsList : ListIso def defs :=
  {
    from_list := defs_from_list;
    to_list := defs_to_list;
    append := defs_append
  }.
Proof.
  all:induction on defs || induction on list; routine.
Defined.

Inductive typing : env -> trm -> typ -> Prop :=
| ty_var : forall {G v T}, binds v T G -> typing G (trm_var $ avar_f v) T
| ty_lam : forall {L G S U t},
    (forall x, x \notin L ->
          typing (x ~ S ++ G) (open x t) (open x U)) ->
    typing G (trm_val $ val_lam S t) (typ_all S U)
| ty_app : forall {G S U x y},
    typing G (trm_var $ avar_f x) (typ_all S U) ->
    typing G (trm_var $ avar_f y) S ->
    typing G (trm_app (avar_f x) $ avar_f y) (open y U)
| ty_obj : forall {L G T ds},
    (forall x, x `notin` L -> typing_ds x (x ~ T ++ G) ds T) ->
    luniq ds ->
    typing G (trm_val $ val_obj T ds) T
| ty_sel : forall G x a T,
    typing G (trm_var x) (typ_dec $ dec_fld a T) ->
    typing G (trm_sel x a) T
| ty_mu_d_i : forall G x D,
    typing G (trm_var $ avar_f x) (typ_dec (open x D)) ->
    typing G (trm_var $ avar_f x) (typ_mu D)
| ty_mu_d_e : forall G x D,
    typing G (trm_var $ avar_f x) (typ_mu D) ->
    typing G (trm_var $ avar_f x) (typ_dec (open x D))
| ty_refine : forall G x T U,
    typing G x T ->
    typing G x U ->
    typing G x (typ_and T U)
| ty_let1 : forall {L G s S u U},
    typing G s S ->
    (forall x, x \notin L -> typing (x ~ S ++ G) (open x u) U) ->
    typing G (trm_let None s u) U
| ty_let2 : forall {L G s S u U},
    typing G s S ->
    (forall x, x \notin L -> typing (x ~ S ++ G) (open x u) U) ->
    typing G (trm_let (Some S) s u) U
| ty_sub : forall {G t S U}, typing G t S -> subty G S U -> typing G t U
with
subty : env -> typ -> typ -> Prop :=
| st_refl : forall G T, subty G T T
| st_top : forall G T, subty G T typ_top
| st_bot : forall G T, subty G typ_bot T
| st_trans : forall {G S T U}, subty G S T -> subty G T U -> subty G S U
| st_bnd : forall {G S T1 T2 U} A,
    subty G S T1 -> subty G T2 U ->
    subty G (typ_dec $ dec_bnd A T1 T2) (typ_dec $ dec_bnd A S U)
| st_fld : forall {G T1 T2} a,
    subty G T1 T2 ->
    subty G (typ_dec $ dec_fld a T1) (typ_dec $ dec_fld a T2)
| st_all : forall {L G S1 S2 U1 U2},
    subty G S2 S1 ->
    (forall x, x \notin L -> subty (x ~ S2 ++ G) (open x U1) (open x U2)) ->
    subty G (typ_all S1 U1) (typ_all S2 U2)
| st_sel1 : forall {G x A S U},
    typing G (trm_var $ avar_f x) (typ_dec $ dec_bnd A S U) ->
    subty G S (typ_sel (avar_f x) A)
| st_sel2 : forall {G x A S U},
    typing G (trm_var $ avar_f x) (typ_dec $ dec_bnd A S U) ->
    subty G (typ_sel (avar_f x) A) U
| st_and_e1 : forall {G T U}, subty G (typ_and T U) T
| st_and_e2 : forall {G T U}, subty G (typ_and T U) U
| st_and_i : forall {G U T1 T2},
    subty G U T1 ->
    subty G U T2 ->
    subty G U (typ_and T1 T2)
with
typing_d : env -> def -> dec -> Prop :=
| td_typ : forall G T A, typing_d G (def_typ A T) (dec_bnd A T T)
| td_trm : forall G t T a,
    typing G t T ->
    typing_d G (def_trm a t) (dec_fld a T)
with
typing_ds : var -> env -> defs -> typ -> Prop :=
| tds_one : forall x G d D,
    typing_d G (open x d) (open x D) ->
    typing_ds x G (defs_cons d defs_nil) (typ_mu D)
| tds_cons : forall x G d D ds T,
    typing_d G (open x d) (open x D) ->
    typing_ds x G ds T ->
    typing_ds x G (defs_cons d ds) (typ_and (typ_mu D) T).

Hint Constructors typing subty typing_d typing_ds.
(** removing this rule from hintbase to avoid unexpected upcasting. *)
Remove Hints ty_sub.

Notation "G ⊢ t ⦂ T" := (typing G t T) (at level 90).
Notation "G ⊢ S <⦂ U" := (subty G S U) (at level 90).
Notation "G ⊩ d ⦂ T" := (typing_d G d T) (at level 90).
Notation "G ⊩[ x ][ ds ⦂ T ]" := (typing_ds x G ds T) (at level 90).