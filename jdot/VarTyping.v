Set Implicit Arguments.
Require Import Definitions.
Require Import OperationProperties.
Require Import StructuralProperties.
Require Import Revealing.
Require Import Subtyping.
Require Import Motions.
Require Import Misc.

Require Import ListRelations.

Reserved Notation "[ L ] G1 !> T '↦[' x ']' U <! G2" (at level 70).
Inductive convert : vars -> var -> env -> typ -> typ -> env -> Prop :=
| cv_sel_refl : forall L G1 x y G2 A, [ L ] G1 !> typ_sel y A ↦[ x ] typ_sel y A <! G2
| cv_sel_left : forall L G1 x y A G2 T U G1',
    upcast G1 y A G1' T ->
    [ L ] G1' !> T ↦[ x ] U <! G2 ->
    [ L ] G1 !> typ_sel y A ↦[ x ] U <! G2
| cv_sel_right : forall L G1 x y A G2 T U G2',
    downcast G2 y A G2' U ->
    [ L ] G1 !> T ↦[ x ] U <! G2' ->
    [ L ] G1 !> T ↦[ x ] typ_sel y A <! G2

| cv_mu_left : forall L x G1 D T G2,
    [ L ] G1 !> typ_dec (open x D) ↦[ x ] T <! G2 ->
    [ L ] G1 !> typ_mu D ↦[ x ] T <! G2
| cv_mu_right : forall L x G1 T D G2,
    [ L ] G1 !> T ↦[ x ] typ_dec (open x D) <! G2 ->
    [ L ] G1 !> T ↦[ x ] typ_mu D <! G2
       
| cv_and_left1 : forall L x G1 G2 S U T,
    ~is_and T ->
    [ L ] G1 !> S ↦[ x ] T <! G2 ->
    [ L ] G1 !> typ_and S U ↦[ x ] T <! G2
| cv_and_left2 : forall L x G1 G2 S U T,
    ~is_and T ->
    [ L ] G1 !> U ↦[ x ] T <! G2 ->
    [ L ] G1 !> typ_and S U ↦[ x ] T <! G2
| cv_and_right : forall L x G1 G2 T S U,
    [ L ] G1 !> T ↦[ x ] S <! G2 ->
    [ L ] G1 !> T ↦[ x ] U <! G2 ->
    [ L ] G1 !> T ↦[ x ] typ_and S U <! G2

| cv_subty : forall L x G1 G2 S U,
    [ L ] G1 >> S <⦂ U << G2 ->
    [ L ] G1 !> S ↦[ x ] U <! G2
where "[ L ] G1 !> T '↦[' x ']' U <! G2" := (convert L x G1 T U G2)%type.

Inductive convert_termination : forall L x G1 S U G2, [ L ] G1 !> S ↦[ x ] U <! G2 -> Prop :=
| ct_sel_refl : forall L G1 x y G2 A, convert_termination (cv_sel_refl L G1 x y G2 A)
| ct_sel_left : forall L G1 x y A G2 T U G1'
                  (Uc : upcast G1 y A G1' T)
                  (Rec : [ L ] G1' !> T ↦[ x ] U <! G2),
    bi_subty_rel (G1', T, U, G2) (G1, typ_sel x A, U, G2) ->
    convert_termination (cv_sel_left Uc Rec)
| ct_sel_right : forall L G1 x y A G2 T U G2'
                   (Dc : downcast G2 y A G2' U)
                   (Rec : [ L ] G1 !> T ↦[ x ] U <! G2'),
    bi_subty_rel (G1, T, U, G2') (G1, T, typ_sel x A, G2) ->
    convert_termination (cv_sel_right Dc Rec)

| ct_mu_left : forall L x G1 D T G2
                 (Rec : [ L ] G1 !> typ_dec (open x D) ↦[ x ] T <! G2),
    bi_subty_rel (G1, typ_dec (open x D), T, G2) (G1, typ_mu D, T, G2) ->
    convert_termination (cv_mu_left D Rec)

| ct_mu_right : forall L x G1 T D G2
                 (Rec : [ L ] G1 !> T ↦[ x ] typ_dec (open x D) <! G2),
    bi_subty_rel (G1, T, typ_dec (open x D), G2) (G1, T, typ_mu D, G2) ->
    convert_termination (cv_mu_right D Rec)
                        
| ct_and_left1 : forall L x G1 G2 S U T
                   (Nand : ~is_and T)
                   (Rec : [ L ] G1 !> S ↦[ x ] T <! G2),
    bi_subty_rel (G1, S, T, G2)
                 (G1, typ_and S U, T, G2) ->
    convert_termination (cv_and_left1 U Nand Rec)
| ct_and_left2 : forall L x G1 G2 S U T
                   (Nand : ~is_and T)
                   (Rec : [ L ] G1 !> U ↦[ x ] T <! G2),
    bi_subty_rel (G1, U, T, G2)
                 (G1, typ_and S U, T, G2) ->
    convert_termination (cv_and_left2 S Nand Rec)
| ct_and_right : forall L x G1 G2 T S U
                   (Rec1 : [ L ] G1 !> T ↦[ x ] S <! G2)
                   (Rec2 : [ L ] G1 !> T ↦[ x ] U <! G2),
    bi_subty_rel (G1, T, S, G2) (G1, T, typ_and S U, G2) ->
    bi_subty_rel (G1, T, U, G2) (G1, T, typ_and S U, G2) ->
    convert_termination (cv_and_right Rec1 Rec2)
| ct_subty : forall L x G1 G2 S U
               (ST : [ L ] G1 >> S <⦂ U << G2),
    convert_termination (cv_subty x ST).

Section Termination.

  Local Hint Constructors convert_termination.

  Hint Constructors lexprod.
  
  Hint Extern 1 (_ <= _) =>
  repeat match goal with
         | T : typ |- _ =>
           pose proof (typ_struct_measure2_ge_1 T);
             fail_if_dup
         end;
    lia.

  Hint Rewrite -> open_typ_same_measure : measures.
  Hint Rewrite -> open_typ_same_measure2 : measures.
  Hint Rewrite -> open_dec_same_measure : measures.
  Hint Rewrite -> open_dec_same_measure2 : measures.
  
  Fixpoint convert_terminates L x G1 S U G2
           (D : [ L ] G1 !> S ↦[ x ] U <! G2) {struct D} :
    convert_termination D.
  Proof.
    destruct D; constructor;
      lazymatch goal with
      | H : upcast _ _ _ _ _ |- _ =>
        apply upcast_measure in H
      | H : downcast _ _ _ _ _ |- _ =>
        apply downcast_measure in H
      | _ => idtac
      end;
      simpl in *; auto.

    - apply left_lex.
      autorewrite with measures.
      auto.
    - apply left_lex.
      autorewrite with measures.
      auto.
    - destruct (is_and_m0_dec U).
      + apply is_and_m0_struct_measure_0 in H.
        rewrite H.
        rewrite Nat.add_0_r. auto.
      + apply not_m0_typ_struct_measure_ge_1 in H.
        auto.

    - destruct (is_and_m0_dec S).
      + apply is_and_m0_struct_measure_0 in H.
        rewrite H. auto.
      + apply not_m0_typ_struct_measure_ge_1 in H.
        auto.

    - destruct (is_and_m0_dec U).
      + apply is_and_m0_struct_measure_0 in H.
        rewrite H.
        rewrite Nat.add_0_r. auto.
      + apply not_m0_typ_struct_measure_ge_1 in H.
        auto.

    - destruct (is_and_m0_dec S).
      + apply is_and_m0_struct_measure_0 in H.
        rewrite H. auto.
      + apply not_m0_typ_struct_measure_ge_1 in H.
        auto.
  Qed.

End Termination.

Section ConvertProperties.

  Hint Resolve subenv_trans.
  
  Lemma convert_sound_gen : forall L x G1 S U G2,
    [ L ] G1 !> S ↦[ x ] U <! G2 ->
    forall G,
      uniq G ->
      fv G [<=] L ->
      G ⪯ G1 ->
      G ⪯ G2 ->
      G ⊢ trm_var (avar_f x) ⦂ S ->
      G ⊢ trm_var (avar_f x) ⦂ U.
  Proof.
    induction on convert; intros; auto.
    - apply upcast_soundness in H. destruct_conjs.
      apply IHconvert; eauto using ty_sub, narrow_subty_gen.
    - apply downcast_soundness in H. destruct_conjs.
      rec_pose IHconvert Hrec; eauto 10 using ty_sub, narrow_subty_gen.
    - rec_pose IHconvert Hrec; eauto 10 using ty_sub, narrow_subty_gen.
    - rec_pose IHconvert Hrec; eauto 10 using ty_sub, narrow_subty_gen.
    - eapply bi_subty_sound_gen in H; eauto using ty_sub.
  Qed.      

End ConvertProperties.

Definition assignable G (x : var) T :=
  exists T', binds x T' G /\
        [ fv G ] G !> T' ↦[ x ] T <! G.
Arguments assignable G x T/.

Theorem assginable_sound : forall G x T,
    assignable G x T ->
    uniq G ->
    G ⊢ trm_var (avar_f x) ⦂ T.
Proof.
  intros; simpl in *;
    destruct_conjs;
    eauto using convert_sound_gen, subenv_refl.
Qed.


Inductive synthesize : env -> var -> typ -> list var -> typ -> Prop :=
| sth_stop : forall G x y A V,
    ~ captured (typ_sel y A) V ->
    synthesize G x (typ_sel y A) V (typ_sel y A)
| sth_sel : forall G G' x y A T V T',
    upcast G y A G' T ->
    synthesize G' x T V T' ->
    synthesize G x (typ_sel y A) V T'
| sth_mu : forall G x D V D',
    synthesize G x (typ_dec (open x D)) (In_remove x V) (typ_dec D') ->
    synthesize G x (typ_mu D) V (typ_mu (close x D'))
| sth_and : forall G x S U V S' U',
    synthesize G x S V S' ->
    synthesize G x U V U' ->
    synthesize G x (typ_and S U) V (typ_and S' U')
| sth_prom : forall G x T V T',
    promotion G T V T' ->
    synthesize G x T V T'.

Inductive syn_var : env -> var -> list var -> typ -> Prop :=
| sv_syn : forall G2 G1 x T V T',
    synthesize G1 x T V T' ->
    syn_var (G2 ++ x ~ T ++ G1) x V T'.

Inductive synthesize_termination : forall G x T V T', synthesize G x T V T' -> Prop :=
| st_stop : forall G x y A V
              (Nc : ~ captured (typ_sel y A) V),
    synthesize_termination (sth_stop G x Nc)
| st_sel : forall G G' x y A T V T'
             (Uc : upcast G y A G' T)
             (Rec : synthesize G' x T V T'),
    tuple_measure_rel (G', T) (G, typ_sel y A) ->
    synthesize_termination (sth_sel Uc Rec)
| st_mu : forall G x D V D'
            (Rec : synthesize G x (typ_dec (open x D)) (In_remove x V) (typ_dec D')),
    tuple_measure_rel (G, typ_dec (open x D)) (G, typ_mu D) ->
    synthesize_termination (sth_mu D V Rec)
| st_and : forall G x S U V S' U'
             (Rec1 : synthesize G x S V S')
             (Rec2 : synthesize G x U V U'),
    tuple_measure_rel (G, S) (G, typ_and S U) ->
    tuple_measure_rel (G, U) (G, typ_and S U) ->
    synthesize_termination (sth_and Rec1 Rec2)
| st_prom : forall G x T V T'
              (P : promotion G T V T'),
    synthesize_termination (sth_prom x P).

Section SynVarProperties.

  Hint Constructors synthesize_termination.
  Hint Rewrite -> open_typ_same_measure : measures.
  Hint Rewrite -> open_typ_same_measure2 : measures.
  Hint Rewrite -> open_dec_same_measure : measures.
  Hint Rewrite -> open_dec_same_measure2 : measures.
  
  Fixpoint synthesize_terminates G x T V T'
           (D : synthesize G x T V T') {struct D} :
    synthesize_termination D.
  Proof.
    destruct D; constructor;
      lazymatch goal with
      | H : upcast _ _ _ _ _ |- _ =>
        apply upcast_measure in H
      | H : downcast _ _ _ _ _ |- _ =>
        apply downcast_measure in H
      | _ => idtac
      end;
      simpl in *; eauto.
    - apply left_lex. lia.
    - apply left_lex.
      autorewrite with measures.
      lia.
    - destruct (is_and_m0_dec U).
      + apply is_and_m0_struct_measure_0 in H.
        rewrite H.
        rewrite Nat.add_0_r.
        apply right_lex. lia.
      + apply not_m0_typ_struct_measure_ge_1 in H.
        apply left_lex. lia.
    - destruct (is_and_m0_dec S).
      + apply is_and_m0_struct_measure_0 in H.
        rewrite H. simpl.
        apply right_lex. lia.
      + apply not_m0_typ_struct_measure_ge_1 in H.
        apply left_lex. lia.
  Qed.
  
  Hint Resolve ope_sub_trans ope_sub_refl ope_sub_app_r.
  
  Lemma synthesize_soundness : forall G x T V T',
    synthesize G x T V T' ->
    wf_env G ->
    forall G',
      ope_sub G G' ->
      lc T ->
      G' ⊢ trm_var (avar_f x) ⦂ T ->
      G' ⊢ trm_var (avar_f x) ⦂ T'.
  Proof.
    induction on synthesize; routine.
    - pose proof (upcast_gives_wf_env H H0).
      pose proof (upcast_finds_lc H H0).
      apply upcast_soundness in H. tidy_up.
      apply subenv_to_ope_sub in H.
      eapply IHsynthesize; eauto using ty_sub, ope_narrow_subty.

    - apply ty_mu_d_i.
      apply open_lc_dec with (x := x) in H6.
      rewrite open_left_inv_dec; eauto.
      apply prodemotion_preserves_lc with (n := 0) in H3; routine.

    - apply ty_refine; eauto using ty_sub.
    - apply promotion_soundness in H; trivial.
      eauto using ty_sub, ope_narrow_subty.
  Qed.

  Lemma syn_var_soundness : forall G x V T,
      syn_var G x V T ->
      wf_env G ->
      G ⊢ trm_var (avar_f x) ⦂ T.
  Proof.
    intros. destruct H.
    wf_env.
    eapply synthesize_soundness; eauto.
  Qed.

  Lemma synthesize_preserves_wf : forall G x T V T',
      synthesize G x T V T' ->
      wf_env G ->
      lc T ->
      fv T' [<=] fv T `union` dom G `union` singleton x /\ lc T'.
  Proof.
    induction on synthesize; intros; auto.
    - pose proof (upcast_gives_wf_env H H0).
      pose proof (upcast_finds_lc H H0).
      pose proof (upcast_fv_bound H).
      destruct_conjs.
      destruct IHsynthesize; trivial.
      rewrite (wf_fv_is_dom H0) in *.
      split; auto. simpl in *. fsetdec.
    - progressive_inversions.
      apply open_lc_dec with (x := x) in H3.
      destruct IHsynthesize; simpl; auto.
      split.
      + pose proof (fv_close_dec D' x 0).
        pose proof (fv_open_dec D x 0).
        fsetdec.
      + constructor. apply close_lc_dec.
        routine.
    - progressive_inversions.
      destruct IHsynthesize1; auto.
      destruct IHsynthesize2; auto.
      split.
      + simpl in *. fsetdec.
      + routine.
    - pose proof (prodemotion_fv_specs H).
      pose proof (prodemotion_preserves_lc H H0 H1).
      destruct_conjs.
      rewrite (wf_fv_is_dom H0) in *.
      split; trivial.
      fsetdec.
  Qed.

  Theorem syn_var_preserves_wf : forall G x V T,
      syn_var G x V T ->
      wf_env G ->
      fv T [<=] dom G /\ lc T.
  Proof.
    intros. destruct H.
    wf_env.
    apply synthesize_preserves_wf in H; trivial.
    destruct_conjs. split; trivial.
    autorewrite with meta_ext.
    simpl. fsetdec.
  Qed.

  Lemma synthesize_not_capture : forall G x T V T',
      synthesize G x T V T' ->
      ~ captured T' V.
  Proof.
    induction on synthesize; simp_captured; intros; auto.
    - gen x0.
      rewrite <- Forall_forall in *.
      eapply remove_from_list; eauto.
      + simpl. apply fv_close_dec.
      + apply fv_close_self_dec.
    - apply prodemotion_fv_specs in H.
      destruct_conjs. simp_captured.
      auto.
  Qed.

  Theorem syn_var_not_capture : forall G x V T,
      syn_var G x V T ->
      ~ captured T V.
  Proof.
    intros. destruct H.
    apply synthesize_not_capture in H.
    trivial.
  Qed.

End SynVarProperties.
