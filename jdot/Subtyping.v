Set Implicit Arguments.
Require Import Definitions.
Require Import OperationProperties.
Require Import StructuralProperties.
Require Import Revealing.
Require Import Misc.

Reserved Notation "[ L ] G1 >> T '<⦂' U << G2" (at level 70).
Inductive bi_subty : atoms -> env -> typ -> typ -> env -> Prop :=
| bs_bot : forall L G1 T G2, [ L ] G1 >> typ_bot <⦂ T << G2
| bs_top : forall L G1 T G2, [ L ] G1 >> T <⦂ typ_top << G2
| bs_sel_refl : forall L G1 x G2 A, [ L ] G1 >> typ_sel x A <⦂ typ_sel x A << G2
| bs_sel_left : forall L G1 x A G2 T U G1',
    upcast G1 x A G1' T ->
    [ L ] G1' >> T <⦂ U << G2 ->
    [ L ] G1 >> typ_sel x A <⦂ U << G2
| bs_sel_right : forall L G1 x A G2 T U G2',
    downcast G2 x A G2' U ->
    [ L ] G1 >> T <⦂ U << G2' ->
    [ L ] G1 >> T <⦂ typ_sel x A << G2

| bs_all : forall L G1 T1 U1 G2 T2 U2 x,
    x `notin` fv G1 `union` fv T1 `union` fv T2
      `union` fv U1 `union` fv U2 `union` fv G2 `union` L ->
    [ L ] G2 >> T2 <⦂ T1 << G1 ->
    [ L  `union` singleton x `union` fv T2 ]
      x ~ T1 ++ G1 >> open x U1 <⦂ open x U2 << x ~ T2 ++ G2 ->
    [ L ] G1 >> typ_all T1 U1 <⦂ typ_all T2 U2 << G2
| bs_bnd : forall L G1 A S1 U1 S2 U2 G2,
    [ L ] G2 >> S2 <⦂ S1 << G1 ->
    [ L ] G1 >> U1 <⦂ U2 << G2 ->
    [ L ] G1 >> typ_dec $ dec_bnd A S1 U1 <⦂ typ_dec $ dec_bnd A S2 U2 << G2
| bs_fld : forall L G1 a T T' G2,
    [ L ] G1 >> T <⦂ T' << G2 ->
    [ L ] G1 >> typ_dec $ dec_fld a T <⦂ typ_dec $ dec_fld a T' << G2
| bs_and_left1 : forall L G1 G2 S U T,
    ~is_and T ->
    [ L ] G1 >> S <⦂ T << G2 ->
    [ L ] G1 >> typ_and S U <⦂ T << G2
| bs_and_left2 : forall L G1 G2 S U T,
    ~is_and T ->
    [ L ] G1 >> U <⦂ T << G2 ->
    [ L ] G1 >> typ_and S U <⦂ T << G2
| bs_and_right : forall L G1 G2 T S U,
    [ L ] G1 >> T <⦂ S << G2 ->
    [ L ] G1 >> T <⦂ U << G2 ->
    [ L ] G1 >> T <⦂ typ_and S U << G2
| bs_typ_mu : forall L G1 D G2,
    [ L ] G1 >> typ_mu D <⦂ typ_mu D << G2
where "[ L ] G1 >> T '<⦂' U << G2" := (bi_subty L G1 T U G2)%type.
Hint Constructors bi_subty.

Section AndInductivePrinciple.

  Variable P : typ -> Prop.
  
  Variable CaseNotAnd : forall T, ~is_and T -> P T.
  Variable CaseAnd : forall S U, P S -> P U -> P (typ_and S U).

  Lemma typ_and_ind : forall T, P T.
  Proof.
    induction T; routine.
  Qed.

End AndInductivePrinciple.

Section Properties.
    
  Hint Extern 1 (_ <= _) =>
  repeat match goal with
         | T : typ |- _ =>
           pose proof (typ_struct_measure2_ge_1 T);
             fail_if_dup
         end;
    lia.

  Lemma bi_subty_and_refl_aux : forall U L G1 G2 S,
      (forall T L G1 G2,
          typ_struct_measure2 T <= typ_struct_measure2 U ->
          [ L ] G1 >> T <⦂ T << G2) ->
      (forall T, and_in U T -> and_in S T) ->
      [ L ] G1 >> S <⦂ U << G2.
  Proof.
    induction U using typ_and_ind; intros.
    - specialize (H1 U ltac:(simpl; eauto)).
      simpl in *. tidy_up.
      induction H2; auto.
    - rec_pose IHU1 Hrec1; eroutine.
      rec_pose IHU2 Hrec2; eroutine.
  Qed.

  Program Fixpoint bi_subty_refl T {measure (typ_struct_measure2 T)} :
    forall L G1 G2,
      [ L ] G1 >> T <⦂ T << G2 := _.
  Next Obligation.
    destruct T; routine.
    - pick_fresh x. econstructor; auto.
      apply bi_subty_refl. rewrite open_typ_same_measure2.
      lia.
    - destruct d; eroutine.
    - apply bs_and_right;
        apply bi_subty_and_refl_aux; eroutine.
  Qed.                    

  Lemma bi_subty_and_is_set : forall U L G1 G2 S,
      (forall T, and_in U T -> and_in S T) ->
      [ L ] G1 >> S <⦂ U << G2.
  Proof.
    eauto using bi_subty_and_refl_aux, bi_subty_refl.
  Qed.
  
  Hint Resolve subenv_trans.
  
  Lemma bi_subty_sound_gen : forall L G1 S U G2,
    [ L ] G1 >> S <⦂ U << G2 ->
    forall G,
      uniq G ->
      fv G [<=] L ->
      G ⪯ G1 ->
      G ⪯ G2 ->
      G ⊢ S <⦂ U.
  Proof.
    induction on bi_subty; try solve [routine]; intros.
    4-5:eauto.
    - apply upcast_soundness in H.
      tidy_up. eauto using narrow_subty_gen.
    - apply downcast_soundness in H.
      tidy_up. eauto using narrow_subty_gen.
    - eapply st_all; eauto.
      cofinite.
      apply open_subst_subty with (x := x); simpl in *; auto;
        clear Fr. 
      + fsetdec.
      + apply IHbi_subty2; auto.
        * constructor; trivial.
          fsetdec.
        * simpl. clear H. fsetdec.
  Qed.
  
End Properties.

Definition stare_at G S U : Prop := bi_subty (fv G) G S U G.
Arguments stare_at G S U/.
Notation "G ⊢S T '<⦂' U" := (stare_at G T U) (at level 70).

Theorem bi_subty_sound : forall G S U,
    G ⊢S S <⦂ U ->
    uniq G ->
    G ⊢ S <⦂ U.
Proof.
  intros; simpl in *;
    destruct_conjs;
    eauto using bi_subty_sound_gen, subenv_refl.
Qed.

Inductive bi_subty_termination : forall L G1 S U G2, [ L ] G1 >> S <⦂ U << G2 -> Prop :=
| bt_bot : forall L G1 T G2, bi_subty_termination (bs_bot L G1 T G2)
| bt_top : forall L G1 T G2, bi_subty_termination (bs_top L G1 T G2)
| bt_sel_refl : forall L G1 x G2 A, bi_subty_termination (bs_sel_refl L G1 x G2 A)
| bt_sel_left : forall L G1 x A G2 T U G1'
                  (Uc : upcast G1 x A G1' T)
                  (Rec : [ L ] G1' >> T <⦂ U << G2),
    bi_subty_rel (G1', T, U, G2) (G1, typ_sel x A, U, G2) ->
    bi_subty_termination (bs_sel_left Uc Rec)
| bt_sel_right : forall L G1 x A G2 T U G2'
                   (Dc : downcast G2 x A G2' U)
                   (Rec : [ L ] G1 >> T <⦂ U << G2'),
    bi_subty_rel (G1, T, U, G2') (G1, T, typ_sel x A, G2) ->
    bi_subty_termination (bs_sel_right Dc Rec)

| bt_all : forall L G1 T1 U1 G2 T2 U2 x
             (F : x `notin` fv G1 `union` fv T1 `union` fv T2
                   `union` fv U1 `union` fv U2 `union` fv G2 `union` L)
             (Rec1 : [ L ] G2 >> T2 <⦂ T1 << G1)
             (Rec2 : [ L  `union` singleton x `union` fv T2 ]
                       x ~ T1 ++ G1 >> open x U1 <⦂ open x U2 << x ~ T2 ++ G2),
    bi_subty_rel (G2, T2, T1, G1)
                 (G1, typ_all T1 U1, typ_all T2 U2, G2) ->
    bi_subty_rel (x ~ T1 ++ G1, open x U1, open x U2, x ~ T2 ++ G2)
                 (G1, typ_all T1 U1, typ_all T2 U2, G2) ->
    bi_subty_termination (bs_all U1 U2 F Rec1 Rec2)
| bt_bnd : forall L G1 A S1 U1 S2 U2 G2
             (Rec1 : [ L ] G2 >> S2 <⦂ S1 << G1)
             (Rec2 : [ L ] G1 >> U1 <⦂ U2 << G2),
    bi_subty_rel (G2, S2, S1, G1)
                 (G1, typ_dec $ dec_bnd A S1 U1, typ_dec $ dec_bnd A S2 U2, G2) ->
    bi_subty_rel (G1, U1, U2, G2)
                 (G1, typ_dec $ dec_bnd A S1 U1, typ_dec $ dec_bnd A S2 U2, G2) ->
    bi_subty_termination (bs_bnd A Rec1 Rec2)
| bt_fld : forall L G1 a T T' G2
             (Rec : [ L ] G1 >> T <⦂ T' << G2),
    bi_subty_rel (G1, T, T', G2)
                 (G1, typ_dec $ dec_fld a T, typ_dec $ dec_fld a T', G2) ->
    bi_subty_termination (bs_fld a Rec)
| bt_and_left1 : forall L G1 G2 S U T
                   (Nand : ~is_and T)
                   (Rec : [ L ] G1 >> S <⦂ T << G2),
    bi_subty_rel (G1, S, T, G2)
                 (G1, typ_and S U, T, G2) ->
    bi_subty_termination (bs_and_left1 U Nand Rec)
| bt_and_left2 : forall L G1 G2 S U T
                   (Nand : ~is_and T)
                   (Rec : [ L ] G1 >> U <⦂ T << G2),
    bi_subty_rel (G1, U, T, G2)
                 (G1, typ_and S U, T, G2) ->
    bi_subty_termination (bs_and_left2 S Nand Rec)
| bt_and_right : forall L G1 G2 T S U
                   (Rec1 : [ L ] G1 >> T <⦂ S << G2)
                   (Rec2 : [ L ] G1 >> T <⦂ U << G2),
    bi_subty_rel (G1, T, S, G2) (G1, T, typ_and S U, G2) ->
    bi_subty_rel (G1, T, U, G2) (G1, T, typ_and S U, G2) ->
    bi_subty_termination (bs_and_right Rec1 Rec2)
| bt_typ_mu : forall L G1 D G2,
    bi_subty_termination (bs_typ_mu L G1 D G2).
Local Hint Constructors bi_subty_termination.

Section Termination.

  Hint Constructors lexprod.
  
  Hint Extern 1 (_ <= _) =>
  repeat match goal with
         | T : typ |- _ =>
           pose proof (typ_struct_measure2_ge_1 T);
             fail_if_dup
         end;
    lia.

  Hint Rewrite -> open_typ_same_measure : measures.
  Hint Rewrite -> open_typ_same_measure2 : measures.
  
  Fixpoint bi_subty_terminates L G1 S U G2
           (D : [ L ] G1 >> S <⦂ U << G2) {struct D} :
    bi_subty_termination D.
  Proof.
    destruct D; constructor;
      lazymatch goal with
      | H : upcast _ _ _ _ _ |- _ =>
        apply upcast_measure in H
      | H : downcast _ _ _ _ _ |- _ =>
        apply downcast_measure in H
      | _ => idtac
      end;
      simpl in *; auto.
    
    - apply left_lex.
      autorewrite with measures.
      auto.
    - destruct (is_and_m0_dec U).
      + apply is_and_m0_struct_measure_0 in H.
        rewrite H.
        rewrite Nat.add_0_r. auto.
      + apply not_m0_typ_struct_measure_ge_1 in H.
        auto.

    - destruct (is_and_m0_dec S).
      + apply is_and_m0_struct_measure_0 in H.
        rewrite H. auto.
      + apply not_m0_typ_struct_measure_ge_1 in H.
        auto.

    - destruct (is_and_m0_dec U).
      + apply is_and_m0_struct_measure_0 in H.
        rewrite H.
        rewrite Nat.add_0_r. auto.
      + apply not_m0_typ_struct_measure_ge_1 in H.
        auto.

    - destruct (is_and_m0_dec S).
      + apply is_and_m0_struct_measure_0 in H.
        rewrite H. auto.
      + apply not_m0_typ_struct_measure_ge_1 in H.
        auto.
  Qed.

End Termination.