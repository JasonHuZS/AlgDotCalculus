Set Implicit Arguments.
Require Import Definitions.
Require Import Misc.
Require Import OperationProperties.
Require Import StructuralProperties.
Require Import ListRelations.

Inductive and_trav : typ -> typ -> typ -> Prop :=
| at_found : forall T, ~is_and T -> and_trav T T typ_top
| at_left : forall S U S' T,
    and_trav S S' T ->
    and_trav (typ_and S U) S' (typ_and T U)
| at_right : forall S U U' T,
    and_trav U U' T ->
    and_trav (typ_and S U) U' (typ_and S T).
Hint Constructors and_trav.

Definition and_in (S U : typ) : Prop := exists T, and_trav S U T.
Arguments and_in S U/.

Section AndTravProps.
  
  Lemma and_trav_measure_eq : forall T S U,
    and_trav T S U ->
    typ_struct_measure T = typ_struct_measure $ typ_and U S.
  Proof.
    induction on and_trav; auto.
    all:simpl in *; lia.
  Qed.

  Lemma and_trav_measure_ge : forall T S U,
      and_trav T S U ->
      typ_struct_measure T >= typ_struct_measure S + typ_struct_measure U.
  Proof.
    induction on and_trav; routine.
  Qed.

  Lemma and_mono : forall G S U S' U',
      G ⊢ S <⦂ S' ->
      G ⊢ U <⦂ U' ->
      G ⊢ typ_and S U <⦂ typ_and S' U'.
  Proof.
    intros. eauto.
  Qed.

  Lemma and_trav_sub : forall T S U,
      and_trav T S U ->
      forall G,
        G ⊢ T <⦂ typ_and S U.
  Proof.
    induction on and_trav; routine.
    - eapply st_trans.
      + eapply and_mono.
        * apply IHand_trav.
        * apply st_refl.
      + eauto.
    - eapply st_trans.
      + eapply and_mono.
        * apply st_refl.
        * apply IHand_trav.
      + eauto.
  Qed.

  Lemma and_trav_sup : forall T S U,
      and_trav T S U ->
      forall G,
        G ⊢ typ_and S U <⦂ T.
  Proof.
    induction on and_trav; routine.
    - eapply st_trans.
      2:eapply and_mono; [apply IHand_trav | apply st_refl].
      eauto.
    - eapply st_trans.
      2:eapply and_mono; [apply st_refl | apply IHand_trav].
      eauto.
  Qed.

  Lemma and_trav_fv : forall T S U,
      and_trav T S U ->
      fv S [<=] fv T /\ fv U [<=] fv T.
  Proof.
    induction on and_trav; set solve.
  Qed.

  Lemma and_trav_lc : forall T S U,
      and_trav T S U ->
      forall n,
        lc_at n T ->
        lc_at n S /\ lc_at n U.
  Proof.
    induction on and_trav; intros; simpl; tidy_up; auto.
    - edestruct IHand_trav; eauto.
    - edestruct IHand_trav; eauto.
  Qed.

End AndTravProps.

Ltac and_trav_m :=
  repeat match goal with
         | H : and_trav _ _ _ |- _ =>
           apply and_trav_measure_eq in H;
           fail_if_dup
         end.

Ltac and_trav_sub G' :=
  repeat match goal with
         | H : and_trav _ _ _ |- _ =>
           apply and_trav_sub with (G := G') in H;
           fail_if_dup
         end.

Inductive revealing : env -> typ -> env -> typ -> Prop :=
| rv_stop : forall G T,
    ~is_sel T ->
    revealing G T G T
| rv_top : forall G T,
    revealing G T G typ_top
| rv_bot : forall G1 G2 T x A G1' T0 T1 T2 T3,
    and_trav T T0 T1 ->
    revealing G1 T0 G1' T2 ->
    and_trav T2 typ_bot T3 ->
    revealing (G2 ++ x ~ T ++ G1) (typ_sel (avar_f x) A)
              (G2 ++ x ~ typ_and T1 T3 ++ G1') typ_bot
| rv_dec : forall G1 G2 T x A G1' G T0 T1 T2 T' L U U0 U1 U2,
    and_trav T T0 T1 ->
    revealing G1 T0 G1' T2 ->
    and_trav T2 (typ_dec $ dec_bnd A L U) T' ->
    and_trav U U0 U1 ->
    revealing G1' U0 G U2 ->
    revealing (G2 ++ x ~ T ++ G1) (typ_sel (avar_f x) A)
              (G2 ++ x ~ typ_and T1 T' ++ G) U2
| rv_mu : forall G1 G2 T x A G1' G T0 T1 T2 T' L U U0 U1 U2,
    and_trav T T0 T1 ->
    revealing G1 T0 G1' T2 ->
    and_trav T2 (typ_mu $ dec_bnd A L U) T' ->
    and_trav (open x U) U0 U1 ->
    revealing (x ~ typ_and T1 T' ++ G1') U0 G U2 ->
    revealing (G2 ++ x ~ T ++ G1) (typ_sel (avar_f x) A)
              (G2 ++ G) U2.
Hint Constructors revealing.

Local Hint Resolve subenv_refl subenv_trans weaken_subty.

Ltac and_trav_wf H :=
  pose proof (and_trav_fv H);
  pose proof (and_trav_lc H).

Section RevealingProperties.
  
  Lemma revealing_soundness : forall G S G' U,
    revealing G S G' U ->
    G ⪯ G' /\ (G ⊢ S <⦂ U).
  Proof.
    induction on revealing; auto.
    - and_trav_sub G1. tidy_up.
      assert (G1 ⊢ T <⦂ typ_and (typ_and typ_bot T3) T1) by eauto.
      split.
      + apply subenv_app. eauto.
      + pose proof (binds_for_sure G2 G1 x T).
        assert (G1 ⊢ T <⦂ typ_bot) by eauto.
        eapply st_sel2.
        instantiate (1 := typ_top).
        simpl_env. eapply ty_sub; eauto.

    - apply and_trav_sub with (G := G1) in H.
      apply and_trav_sub with (G := G1) in H0.
      apply and_trav_sub with (G := G1') in H1.
      tidy_up.
      assert (G1 ⊢ T <⦂ typ_and (typ_and (typ_dec (dec_bnd A L U)) T') T1) by eauto.
      split.
      + apply subenv_app. 
        constructor; eauto.
      + pose proof (binds_for_sure G2 G1 x T).
        assert (G1 ⊢ T <⦂ (typ_dec (dec_bnd A L U))) by eauto.
        eapply st_sel2.
        instantiate (1 := L).
        eapply ty_sub.
        * instantiate (1 := typ_dec (dec_bnd A L U)).
          simpl_env; eapply ty_sub; eauto.
        * simpl_env; do 2 apply weaken_subty.
          constructor; trivial.
          eapply narrow_subty_gen; [ | eassumption].
          eauto.

    - apply and_trav_sub with (G := G1) in H.
      apply and_trav_sub with (G := G1) in H0.
      apply and_trav_sub with (G := x ~ typ_and T1 T' ++ G1') in H1.
      tidy_up.
      assert (G1 ⊢ T <⦂ typ_and (typ_and (typ_mu (dec_bnd A L U)) T') T1) by eauto.
      split.
      + apply subenv_app. constructor; [eauto |].
        eapply narrow_subty_gen in H11; [ | eassumption].
        eapply st_trans; [ | eassumption].
        eauto.
      + pose proof (binds_for_sure G2 G1 x T).
        assert (G1 ⊢ T <⦂ (typ_mu (dec_bnd A L U))) by eauto.
        eapply st_sel2.
        instantiate (1 := open x L).
        eapply ty_sub.
        * instantiate (1 := typ_dec (dec_bnd A (open x L) $ open x U)).
          change (dec_bnd A (open x L) (open x U))
                 with (open x $ dec_bnd A L U).
          apply ty_mu_d_e. 
          simpl_env; eapply ty_sub; eauto.
        * simpl_env; apply weaken_subty.
          constructor; trivial.
          eapply narrow_subty_gen.
          -- eapply st_trans; [eassumption |].
             eauto.
          -- simpl; eauto.
  Qed.
  
  Lemma revealing_measure : forall G S G' U,
      revealing G S G' U ->
      subty_measure G S >= subty_measure G' U.
  Proof.
    induction on revealing; simpl in *;
      autorewrite with measures in *; simpl in *; try lia.
    1-2:and_trav_m; simpl in *; lia.

    and_trav_m. rewrite open_typ_same_measure in H1.
    simpl in *; lia.
  Qed.

  Lemma revealing_subenv : forall G S G' U,
      revealing G S G' U ->
      G ⪯ G'.
  Proof.
    intros. eapply revealing_soundness. eassumption.
  Qed.

  Lemma revealing_same_dom : forall G S G' U,
      revealing G S G' U ->
      dom G [=] dom G'.
  Proof.
    intros. apply revealing_subenv in H.
    apply subenv_dom in H. trivial.
  Qed.

  Lemma revealing_not_sel : forall G S G' U,
      revealing G S G' U ->
      ~is_sel U.
  Proof.
    induction on revealing; routine.
  Qed.
    
  Local Hint Constructors lc_typ_at.

  Lemma revealing_fv_bound : forall G S G' U,
      revealing G S G' U ->
      fv G' [<=] fv G /\ fv U [<=] fv G `union` fv S.
  Proof.
    induction on revealing; autorewrite with meta_ext.
    1-2:split; set solve.
    - and_trav_wf H. and_trav_wf H0.
      destruct_conjs.
      simpl in *; repeat split; fsetdec.
    - and_trav_wf H. and_trav_wf H0. and_trav_wf H1.
      destruct_conjs.
      simpl in *; repeat split; try fsetdec.
    - and_trav_wf H. and_trav_wf H0. and_trav_wf H1.
      destruct_conjs.
      pose proof (fv_open_typ U x 0).
      simpl in *; repeat split; try fsetdec.
  Qed.    
  
  Lemma revealing_finds_lc : forall G S G' U,
      revealing G S G' U ->
      wf_env G ->
      lc S -> wf_env G' /\ lc U.
  Proof.
    induction on revealing; intros; simpl; eauto.
    - split; auto.
      pose proof H1. wf_env.
      and_trav_wf H. and_trav_wf H0.
      specialize (H6 _ ltac:(eauto)).
      destruct_conjs.
      destruct IHrevealing; auto.
      specialize (H12 _ ltac:(eauto)).
      destruct_conjs.
      pose proof H3.
      apply revealing_same_dom in H3.
      apply revealing_fv_bound in H19.
      destruct_conjs.
      eapply wf_app; eauto.
      + simpl; fsetdec.
      + constructor; auto.
        * fsetdec.
        * rewrite (wf_fv_is_dom H10) in H20.
          simpl in *; fsetdec.
        * routine.
    - pose proof H2. wf_env.
      and_trav_wf H. and_trav_wf H0. and_trav_wf H1.
      specialize (H6 _ ltac:(eauto)).
      destruct_conjs.
      destruct IHrevealing1; auto.
      specialize (H12 _ ltac:(eauto)).
      destruct_conjs. progressive_inversions.
      specialize (H14 _ ltac:(eauto)).
      destruct_conjs.
      destruct IHrevealing2; auto.       
      pose proof H3_. pose proof H3_0.
      apply revealing_same_dom in H3_.
      apply revealing_same_dom in H3_0.
      apply revealing_fv_bound in H29.
      apply revealing_fv_bound in H30.
      destruct_conjs.
      split; [eapply wf_app |]; eauto.
      + simpl; fsetdec.
      + constructor; auto.
        * fsetdec.
        * rewrite (wf_fv_is_dom H10) in H32.
          rewrite (wf_fv_is_dom H19) in H31.
          simpl in *; fsetdec.
        * routine.

    - pose proof H2. wf_env.
      and_trav_wf H. and_trav_wf H0. and_trav_wf H1.
      specialize (H6 _ ltac:(eauto)).
      destruct_conjs.
      destruct IHrevealing1; auto.
      specialize (H12 _ ltac:(eauto)).
      destruct_conjs. progressive_inversions.
      pose proof (open_lc_typ x H28).
      specialize (H14 _ ltac:(eauto)).
      pose proof H3_. pose proof H3_0.
      apply revealing_same_dom in H3_.
      apply revealing_same_dom in H3_0.
      apply revealing_fv_bound in H23.
      apply revealing_fv_bound in H27.
      destruct_conjs.
      autorewrite with meta_ext in H29.
      rewrite (wf_fv_is_dom H10) in H30.
      rewrite (wf_fv_is_dom H19) in H29.
      destruct IHrevealing2; auto.
      + constructor; auto.
        * fsetdec.
        * clear H13 H15 H29.
          assert (fv T' [<=] dom G1 `union` fv T) by fsetdec.
          simpl; fold_cls.
          fsetdec.
        * routine.
      + split; [eapply wf_app |]; eauto.
        simpl; simpl in H3_0. fsetdec.
  Qed.

  Lemma revealing_gives_wf_env : forall G S G' U,
      revealing G S G' U ->
      wf_env G ->
      wf_env G'.
  Proof.
    induction on revealing; routine.
    - pose proof H1. wf_env.
      and_trav_wf H. and_trav_wf H0.
      specialize (H5 _ ltac:(eauto)).
      destruct_conjs.
      specialize (IHrevealing ltac:(trivial)).
      pose proof (revealing_finds_lc H3 ltac:(trivial) ltac:(trivial)).
      destruct_conjs.
      specialize (H11 _ ltac:(eauto)).
      pose proof H3.
      apply revealing_same_dom in H3.
      apply revealing_fv_bound in H17.
      destruct_conjs.
      eapply wf_app; eauto.
      + simpl; fsetdec.
      + constructor; auto.
        * fsetdec.
        * rewrite (wf_fv_is_dom H9) in H18.
          simpl in *; fsetdec.
        * routine.
    - pose proof H2. wf_env.
      and_trav_wf H. and_trav_wf H0. and_trav_wf H1.
      specialize (H5 _ ltac:(eauto)).
      destruct_conjs.
      specialize (IHrevealing1 ltac:(trivial)).
      pose proof (revealing_finds_lc H3_ ltac:(trivial) ltac:(trivial)).
      destruct_conjs.
      specialize (H11 _ ltac:(eauto)).
      destruct_conjs. progressive_inversions.
      specialize (H13 _ ltac:(eauto)).
      destruct_conjs.
      pose proof (revealing_finds_lc H3_0 ltac:(trivial) ltac:(trivial)).
      destruct_conjs.
      pose proof H3_. pose proof H3_0.
      apply revealing_same_dom in H3_.
      apply revealing_same_dom in H3_0.
      apply revealing_fv_bound in H26.
      apply revealing_fv_bound in H28.
      destruct_conjs.
      eapply wf_app; eauto.
      + simpl; fsetdec.
      + constructor; auto.
        * fsetdec.
        * rewrite (wf_fv_is_dom H9) in H30.
          rewrite (wf_fv_is_dom H18) in H29.
          simpl in *; fsetdec.
        * routine.

    - pose proof H2. wf_env.
      and_trav_wf H. and_trav_wf H0. and_trav_wf H1.
      specialize (H5 _ ltac:(eauto)).
      destruct_conjs.
      specialize (IHrevealing1 ltac:(trivial)).
      pose proof (revealing_finds_lc H3_ ltac:(trivial) ltac:(trivial)).
      destruct_conjs.
      specialize (H11 _ ltac:(eauto)).
      destruct_conjs. progressive_inversions.
      pose proof (open_lc_typ x H27).
      specialize (H13 _ ltac:(eauto)).
      destruct_conjs.
      pose proof H3_. do 2 pose proof H3_0.
      apply revealing_same_dom in H3_.
      apply revealing_same_dom in H3_0.
      apply revealing_fv_bound in H24.
      apply revealing_fv_bound in H26.
      destruct_conjs.
      rewrite (wf_fv_is_dom H9) in H30.
      simpl_env in H29. autorewrite with meta_ext in H29.
      rewrite (wf_fv_is_dom H18) in H29.
      assert (wf_env (x ~ typ_and T1 T' ++ G1')). {
        constructor; auto.
        + fsetdec.
        + assert (fv T' [<=] dom G1 `union` fv T) by fsetdec.
          simpl; fold_cls.
          fsetdec.
        + routine.        
      }
      pose proof (revealing_finds_lc H28 ltac:(trivial) ltac:(trivial)).
      destruct_conjs.
      eapply wf_app; eauto.
      simpl. simpl in H3_0. fsetdec.
  Qed.
  
  Lemma revealing_wf :  forall G S G' U,
      revealing G S G' U ->
      wf_env G ->
      fv S [<=] dom G -> lc S ->
       wf_env G' /\ fv U [<=] dom G' /\ lc U.
  Proof.
    intros.
    pose proof (revealing_fv_bound H).
    pose proof (revealing_finds_lc H ltac:(trivial) ltac:(trivial)).
    destruct_conjs.
    repeat split; trivial.
    rewrite (wf_fv_is_dom H0) in *.
    apply revealing_same_dom in H.
    fsetdec.
  Qed.
        
End RevealingProperties.

Inductive revealing_termination : forall G S G' U, revealing G S G' U -> Prop :=
| rt_stop : forall G T (not_sel : ~is_sel T),
    revealing_termination (rv_stop G T not_sel)
| rt_top : forall G T,
    revealing_termination (rv_top G T)
| rt_bot : forall G1 G2 T x A G1' T0 T1 T2 T3
             (At1 : and_trav T T0 T1)
             (Rv : revealing G1 T0 G1' T2)
             (At2 : and_trav T2 typ_bot T3),
    subty_measure (G2 ++ x ~ T ++ G1) (typ_sel (avar_f x) A)
    > subty_measure G1 T0 ->
    revealing_termination (rv_bot G2 x A At1 Rv At2)
| rt_dec : forall G1 G2 T x A G1' G T0 T1 T2 T' L U U0 U1 U2
             (At1 : and_trav T T0 T1)
             (Rv1 : revealing G1 T0 G1' T2)
             (At2 : and_trav T2 (typ_dec $ dec_bnd A L U) T')
             (At3 : and_trav U U0 U1)
             (Rv2 : revealing G1' U0 G U2),
    subty_measure (G2 ++ x ~ T ++ G1) (typ_sel (avar_f x) A)
    > subty_measure G1 T0 ->
    subty_measure (G2 ++ x ~ T ++ G1) (typ_sel (avar_f x) A)
    > subty_measure G1' U0 ->
    revealing_termination (rv_dec G2 x At1 Rv1 At2 At3 Rv2)
| rt_mu : forall G1 G2 T x A G1' G T0 T1 T2 T' L U U0 U1 U2
            (At1 : and_trav T T0 T1)
            (Rv1 : revealing G1 T0 G1' T2)
            (At2 : and_trav T2 (typ_mu $ dec_bnd A L U) T')
            (At3 : and_trav (open x U) U0 U1)
            (Rv2 : revealing (x ~ typ_and T1 T' ++ G1') U0 G U2),
    subty_measure (G2 ++ x ~ T ++ G1) (typ_sel (avar_f x) A)
    > subty_measure G1 T0 ->
    subty_measure (G2 ++ x ~ T ++ G1) (typ_sel (avar_f x) A)
    > subty_measure G1' U0 ->
    revealing_termination (rv_mu G2 At1 Rv1 At2 At3 Rv2).
Local Hint Constructors revealing_termination.

Fixpoint revealing_terminates G S G' U (H : revealing G S G' U) {struct H} :
  revealing_termination H.
Proof.
  destruct H; try (constructor; assumption);
    constructor; simpl; 
      autorewrite with measures in *;
      and_trav_m;
      repeat lazymatch goal with
             | H : revealing _ _ _ _ |- _ =>
               apply revealing_measure in H
             end;
      simpl in *; try lia.

  rewrite open_typ_same_measure in *. lia.
Qed.

Inductive upcast : env -> avar -> typ_label -> env -> typ -> Prop :=
| uc_top : forall G x A,
    upcast G x A G typ_top
| uc_bot : forall G1 G2 x A T G1' T0 T1 T2 T3,
    and_trav T T0 T1 ->
    revealing G1 T0 G1' T2 ->
    and_trav T2 typ_bot T3 ->
    upcast (G2 ++ x ~ T ++ G1) (avar_f x) A (G2 ++ x ~ typ_and T1 T3 ++ G1') typ_bot
| uc_dec : forall G1 G2 x A T G1' T0 T1 T2 T' L U,
    and_trav T T0 T1 ->
    revealing G1 T0 G1' T2 ->
    and_trav T2 (typ_dec $ dec_bnd A L U) T' ->
    upcast (G2 ++ x ~ T ++ G1) (avar_f x) A
           (G2 ++ x ~ typ_and T1 T' ++ G1') U
| uc_mu : forall G1 G2 x A T G1' T0 T1 T2 T' L U,
    and_trav T T0 T1 ->
    revealing G1 T0 G1' T2 ->
    and_trav T2 (typ_mu $ dec_bnd A L U) T' ->
    upcast (G2 ++ x ~ T ++ G1) (avar_f x) A
           (G2 ++ x ~ typ_and T1 T' ++ G1') (open x U).
Hint Constructors upcast.

Inductive downcast : env -> avar -> typ_label -> env -> typ -> Prop :=
| dc_bot : forall G x A,
    downcast G x A G typ_bot
| dc_top : forall G1 G2 x A T G1' T0 T1 T2 T3,
    and_trav T T0 T1 ->
    revealing G1 T0 G1' T2 ->
    and_trav T2 typ_bot T3 ->
    downcast (G2 ++ x ~ T ++ G1) (avar_f x) A (G2 ++ x ~ typ_and T1 T3 ++ G1') typ_top
| dc_dec : forall G1 G2 x A T G1' T0 T1 T2 T' L U,
    and_trav T T0 T1 ->
    revealing G1 T0 G1' T2 ->
    and_trav T2 (typ_dec $ dec_bnd A L U) T' ->
    downcast (G2 ++ x ~ T ++ G1) (avar_f x) A
           (G2 ++ x ~ typ_and T1 T' ++ G1') L
| dc_mu : forall G1 G2 x A T G1' T0 T1 T2 T' L U,
    and_trav T T0 T1 ->
    revealing G1 T0 G1' T2 ->
    and_trav T2 (typ_mu $ dec_bnd A L U) T' ->
    downcast (G2 ++ x ~ T ++ G1) (avar_f x) A
           (G2 ++ x ~ typ_and T1 T' ++ G1') (open x L).
Hint Constructors downcast.

Section CastingProperties.

  Local Hint Resolve subenv_app.
  
  Lemma upcast_soundness : forall G x A G' U,
    upcast G x A G' U ->
    G ⪯ G' /\ (G ⊢ typ_sel x A <⦂ U).
  Proof.
    destr on upcast; auto;
      match goal with
      | H : revealing _ _ _ _ |- _ =>
        apply revealing_soundness in H
      end;
      destruct_conjs.
    - and_trav_sub G1. 
      assert (G1 ⊢ T <⦂ typ_and (typ_and typ_bot T3) T1) by eauto.
      split.
      + simpl; eauto 6.
      + pose proof (binds_for_sure G2 G1 x T).
        assert (G1 ⊢ T <⦂ typ_bot) by eauto.
        eapply st_sel2.
        instantiate (1 := typ_top).
        simpl_env. eapply ty_sub; eauto.

    - and_trav_sub G1. 
      assert (G1 ⊢ T <⦂ typ_and (typ_and (typ_dec (dec_bnd A L U)) T') T1) by eauto.
      split.
      + simpl; eauto 6.
      + pose proof (binds_for_sure G2 G1 x T).
        assert (G1 ⊢ T <⦂ (typ_dec (dec_bnd A L U))) by eauto.
        eapply st_sel2.
        instantiate (1 := L).
        simpl_env; eapply ty_sub; eauto.

    - and_trav_sub G1.
      assert (G1 ⊢ T <⦂ typ_and (typ_and (typ_mu (dec_bnd A L U)) T') T1) by eauto.
      split.
      + simpl; eauto 6.
      + pose proof (binds_for_sure G2 G1 x T).
        assert (G1 ⊢ T <⦂ (typ_mu (dec_bnd A L U))) by eauto.
        eapply st_sel2.
        instantiate (1 := open x L).
        change (dec_bnd A (open x L) (open x U))
          with (open x $ dec_bnd A L U).
          apply ty_mu_d_e. 
          simpl_env; eapply ty_sub; eauto.
  Qed.

  Lemma upcast_measure : forall G x A G' U,
      upcast G x A G' U ->
      subty_measure G (typ_sel x A) > subty_measure G' U.
  Proof.
    destr on upcast; simpl in *;
      autorewrite with measures in *;
      try lazymatch goal with
          | H : revealing _ _ _ _ |- _ =>
            apply revealing_measure in H
          end;
      and_trav_m; simpl in *;
      try lia.

    rewrite open_typ_same_measure in *. lia.
  Qed.

  Lemma upcast_fv_bound : forall G x A G' U,
      upcast G x A G' U ->
      fv G' [<=] fv G /\ fv U [<=] fv G `union` fv x.
  Proof.
    induction on upcast; autorewrite with meta_ext.
    - split; set solve.
    - and_trav_wf H. and_trav_wf H1.
      apply revealing_fv_bound in H0.
      destruct_conjs.
      simpl in *; repeat split; fsetdec.
    - and_trav_wf H. and_trav_wf H1.
      apply revealing_fv_bound in H0.
      destruct_conjs.
      simpl in *; repeat split; try fsetdec.
    - and_trav_wf H. and_trav_wf H1.
      apply revealing_fv_bound in H0.
      destruct_conjs.
      pose proof (fv_open_typ U x 0).
      simpl in *; repeat split; try fsetdec.
  Qed.

  Lemma upcast_gives_wf_env : forall G x A G' U,
      upcast G x A G' U ->
      wf_env G ->
      wf_env G'.
  Proof.
    induction on upcast; routine.
    - pose proof H2. wf_env.
      and_trav_wf H. and_trav_wf H1.
      specialize (H5 _ ltac:(eauto)).
      destruct_conjs.
      pose proof (revealing_finds_lc H0 ltac:(trivial) ltac:(trivial)).
      destruct_conjs.
      specialize (H11 _ ltac:(eauto)).
      pose proof H0.
      apply revealing_same_dom in H0.
      apply revealing_fv_bound in H17.
      destruct_conjs.
      eapply wf_app; eauto.
      + simpl; fsetdec.
      + constructor; auto.
        * fsetdec.
        * rewrite (wf_fv_is_dom H9) in H18.
          simpl in *; fsetdec.
        * routine.
    - pose proof H2. wf_env.
      and_trav_wf H. and_trav_wf H1.
      specialize (H5 _ ltac:(eauto)).
      destruct_conjs.
      pose proof (revealing_finds_lc H0 ltac:(trivial) ltac:(trivial)).
      destruct_conjs.
      specialize (H11 _ ltac:(eauto)).
      destruct_conjs. progressive_inversions.
      pose proof H0.
      apply revealing_same_dom in H0.
      apply revealing_fv_bound in H18.
      destruct_conjs.
      eapply wf_app; eauto.
      + simpl; fsetdec.
      + constructor; auto.
        * fsetdec.
        * rewrite (wf_fv_is_dom H9) in H19.
          simpl in *; fsetdec.
        * routine.

    - pose proof H2. wf_env.
      and_trav_wf H. and_trav_wf H1.
      specialize (H5 _ ltac:(eauto)).
      destruct_conjs.
      pose proof (revealing_finds_lc H0 ltac:(trivial) ltac:(trivial)).
      destruct_conjs.
      specialize (H11 _ ltac:(eauto)).
      destruct_conjs. progressive_inversions.
      pose proof (open_lc_typ x H24).
      pose proof H0.
      apply revealing_same_dom in H0.
      apply revealing_fv_bound in H19.
      destruct_conjs.
      eapply wf_app; eauto.
      + simpl; fsetdec.
      + constructor; auto.
        * fsetdec.
        * rewrite (wf_fv_is_dom H9) in H21.
          simpl in *; fsetdec.
        * routine.
  Qed.

  Lemma upcast_finds_lc : forall G x A G' U,
      upcast G x A G' U ->
      wf_env G ->
      lc U.
  Proof.
    induction on upcast; routine.
    - pose proof H2. wf_env.
      and_trav_wf H; and_trav_wf H1.
      specialize (H5 _ ltac:(eauto)).
      destruct_conjs.
      apply revealing_finds_lc in H0; trivial.
      destruct_conjs.
      specialize (H11 _ ltac:(eauto)).
      routine.
    - pose proof H2. wf_env.
      and_trav_wf H; and_trav_wf H1.
      specialize (H5 _ ltac:(eauto)).
      destruct_conjs.
      apply revealing_finds_lc in H0; trivial.
      destruct_conjs.
      specialize (H11 _ ltac:(eauto)).
      apply open_lc_typ. routine.
  Qed.    

  Lemma upcast_same_dom : forall G x A G' U,
      upcast G x A G' U ->
      dom G [=] dom G'.
  Proof.
    intros. apply upcast_soundness in H.
    destruct_conjs.
    apply subenv_dom in H. trivial.
  Qed.
  
  Theorem upcast_preserves_wf : forall G x A G' U,
      upcast G x A G' U ->
      wf_env G ->
      fv x [<=] dom G ->
      wf_env G' /\ fv U [<=] dom G' /\ lc U.
  Proof.
    intros.
    pose proof (upcast_gives_wf_env H H0).
    pose proof (upcast_fv_bound H).
    pose proof (upcast_finds_lc H H0).
    repeat split; trivial.
    destruct_conjs. rewrite (wf_fv_is_dom H0) in *.
    apply upcast_same_dom in H.
    fsetdec.
  Qed.    

  Lemma downcast_soundness : forall G x A G' U,
    downcast G x A G' U ->
    G ⪯ G' /\ (G ⊢ U <⦂ typ_sel x A).
  Proof.
    destr on downcast; auto;
      match goal with
      | H : revealing _ _ _ _ |- _ =>
        apply revealing_soundness in H
      end;
      destruct_conjs.
    - and_trav_sub G1. 
      assert (G1 ⊢ T <⦂ typ_and (typ_and typ_bot T3) T1) by eauto.
      split.
      + simpl; eauto 6.
      + pose proof (binds_for_sure G2 G1 x T).
        assert (G1 ⊢ T <⦂ typ_bot) by eauto.
        eapply st_sel1.
        instantiate (1 := typ_top).
        simpl_env. eapply ty_sub; eauto.

    - and_trav_sub G1. 
      assert (G1 ⊢ T <⦂ typ_and (typ_and (typ_dec (dec_bnd A L U)) T') T1) by eauto.
      split.
      + simpl; eauto 6.
      + pose proof (binds_for_sure G2 G1 x T).
        assert (G1 ⊢ T <⦂ (typ_dec (dec_bnd A L U))) by eauto.
        eapply st_sel1.
        instantiate (1 := U).
        simpl_env; eapply ty_sub; eauto.

    - and_trav_sub G1.
      assert (G1 ⊢ T <⦂ typ_and (typ_and (typ_mu (dec_bnd A L U)) T') T1) by eauto.
      split.
      + simpl; eauto 6.
      + pose proof (binds_for_sure G2 G1 x T).
        assert (G1 ⊢ T <⦂ (typ_mu (dec_bnd A L U))) by eauto.
        eapply st_sel1.
        instantiate (1 := open x U).
        change (dec_bnd A (open x L) (open x U))
          with (open x $ dec_bnd A L U).
          apply ty_mu_d_e. 
          simpl_env; eapply ty_sub; eauto.
  Qed.

  Lemma downcast_measure : forall G x A G' U,
      downcast G x A G' U ->
      subty_measure G (typ_sel x A) > subty_measure G' U.
  Proof.
    destr on downcast; simpl in *;
      autorewrite with measures in *;
      try lazymatch goal with
          | H : revealing _ _ _ _ |- _ =>
            apply revealing_measure in H
          end;
      and_trav_m; simpl in *;
      try lia.

    rewrite open_typ_same_measure in *. lia.
  Qed.

  Lemma downcast_fv_bound : forall G x A G' U,
      downcast G x A G' U ->
      fv G' [<=] fv G /\ fv U [<=] fv G `union` fv x.
  Proof.
    induction on downcast; autorewrite with meta_ext.
    - split; set solve.
    - and_trav_wf H. and_trav_wf H1.
      apply revealing_fv_bound in H0.
      destruct_conjs.
      simpl in *; repeat split; fsetdec.
    - and_trav_wf H. and_trav_wf H1.
      apply revealing_fv_bound in H0.
      destruct_conjs.
      simpl in *; repeat split; try fsetdec.
    - and_trav_wf H. and_trav_wf H1.
      apply revealing_fv_bound in H0.
      destruct_conjs.
      pose proof (fv_open_typ L x 0).
      simpl in *; repeat split; try fsetdec.
  Qed.

  Lemma downcast_gives_wf_env : forall G x A G' U,
      downcast G x A G' U ->
      wf_env G ->
      wf_env G'.
  Proof.
    induction on downcast; routine.
    - pose proof H2. wf_env.
      and_trav_wf H. and_trav_wf H1.
      specialize (H5 _ ltac:(eauto)).
      destruct_conjs.
      pose proof (revealing_finds_lc H0 ltac:(trivial) ltac:(trivial)).
      destruct_conjs.
      specialize (H11 _ ltac:(eauto)).
      pose proof H0.
      apply revealing_same_dom in H0.
      apply revealing_fv_bound in H17.
      destruct_conjs.
      eapply wf_app; eauto.
      + simpl; fsetdec.
      + constructor; auto.
        * fsetdec.
        * rewrite (wf_fv_is_dom H9) in H18.
          simpl in *; fsetdec.
        * routine.
    - pose proof H2. wf_env.
      and_trav_wf H. and_trav_wf H1.
      specialize (H5 _ ltac:(eauto)).
      destruct_conjs.
      pose proof (revealing_finds_lc H0 ltac:(trivial) ltac:(trivial)).
      destruct_conjs.
      specialize (H11 _ ltac:(eauto)).
      destruct_conjs. progressive_inversions.
      pose proof H0.
      apply revealing_same_dom in H0.
      apply revealing_fv_bound in H18.
      destruct_conjs.
      eapply wf_app; eauto.
      + simpl; fsetdec.
      + constructor; auto.
        * fsetdec.
        * rewrite (wf_fv_is_dom H9) in H19.
          simpl in *; fsetdec.
        * routine.

    - pose proof H2. wf_env.
      and_trav_wf H. and_trav_wf H1.
      specialize (H5 _ ltac:(eauto)).
      destruct_conjs.
      pose proof (revealing_finds_lc H0 ltac:(trivial) ltac:(trivial)).
      destruct_conjs.
      specialize (H11 _ ltac:(eauto)).
      destruct_conjs. progressive_inversions.
      pose proof (open_lc_typ x H22).
      pose proof H0.
      apply revealing_same_dom in H0.
      apply revealing_fv_bound in H19.
      destruct_conjs.
      eapply wf_app; eauto.
      + simpl; fsetdec.
      + constructor; auto.
        * fsetdec.
        * rewrite (wf_fv_is_dom H9) in H21.
          simpl in *; fsetdec.
        * routine.
  Qed.


  Lemma downcast_finds_lc : forall G x A G' U,
      downcast G x A G' U ->
      wf_env G ->
      lc U.
  Proof.
    induction on downcast; routine.
    - pose proof H2. wf_env.
      and_trav_wf H; and_trav_wf H1.
      specialize (H5 _ ltac:(eauto)).
      destruct_conjs.
      apply revealing_finds_lc in H0; trivial.
      destruct_conjs.
      specialize (H11 _ ltac:(eauto)).
      routine.
    - pose proof H2. wf_env.
      and_trav_wf H; and_trav_wf H1.
      specialize (H5 _ ltac:(eauto)).
      destruct_conjs.
      apply revealing_finds_lc in H0; trivial.
      destruct_conjs.
      specialize (H11 _ ltac:(eauto)).
      apply open_lc_typ. routine.
  Qed.    

  Lemma downcast_same_dom : forall G x A G' U,
      downcast G x A G' U ->
      dom G [=] dom G'.
  Proof.
    intros. apply downcast_soundness in H.
    destruct_conjs.
    apply subenv_dom in H. trivial.
  Qed.
  
  Theorem downcast_preserves_wf : forall G x A G' U,
      downcast G x A G' U ->
      wf_env G ->
      fv x [<=] dom G ->
      wf_env G' /\ fv U [<=] dom G' /\ lc U.
  Proof.
    intros.
    pose proof (downcast_gives_wf_env H H0).
    pose proof (downcast_fv_bound H).
    pose proof (downcast_finds_lc H H0).
    repeat split; trivial.
    destruct_conjs. rewrite (wf_fv_is_dom H0) in *.
    apply downcast_same_dom in H.
    fsetdec.
  Qed.    

End CastingProperties.
