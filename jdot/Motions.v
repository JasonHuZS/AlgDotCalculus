Set Implicit Arguments.
Require Import Definitions.
Require Import OperationProperties.
Require Import StructuralProperties.
Require Import Revealing.
Require Import Misc.

Inductive mode : Set := pro | de.

Definition oppode (m : mode) : mode :=
  match m with
  | pro => de
  | de => pro
  end.

Definition weak_case (m : mode) :=
  match m with
  | pro => typ_top
  | de => typ_bot
  end.

(** a list of vars captures some free variables of e *)
Definition captured {A : Type} `{_ : HasFv A} (e : A) (vs : list var) :=
  Exists (fun v => v `in` fv e) vs.
Arguments captured {A} {_} e vs/.

Definition captured_dec {A : Type} `{_ : HasFv A} (e : A) V
  : {captured e V} + {~captured e V}.
Proof.
  apply Exists_dec; intros.
  apply AtomSetProperties.In_dec.
Qed.   

Lemma not_capture_bot : forall V,
    ~ captured typ_bot V.
Proof.
  intros l Contra.
  induction Contra; routine.
  set solve.
  apply notin_empty_1 in H. trivial.
Qed.

Inductive prodemotion : mode -> env -> typ -> list var -> typ -> Prop :=
| pd_top : forall m G V, prodemotion m G typ_top V typ_top
| pr_top : forall G T V, prodemotion pro G T V typ_top
| pd_bot : forall m G V, prodemotion m G typ_bot V typ_bot
| de_bot : forall G T V, prodemotion de G T V typ_bot

| pd_seln : forall m G n A V,
    prodemotion m G (typ_sel (avar_b n) A) V (typ_sel (avar_b n) A)
| pd_sel_neq : forall m G (x : var) A V,
    ~In x V ->
    prodemotion m G (typ_sel x A) V (typ_sel x A)

| pr_sel1 : forall G1 G2 G1' (x : var) A T T1 T2 T3 T' V,
    In x V ->
    and_trav T T1 T2 ->
    revealing G1 T1 G1' T' ->
    and_trav T' typ_bot T3 ->
    prodemotion pro (G2 ++ x ~ T ++ G1) (typ_sel x A) V typ_bot
| pr_sel2 : forall G1 G2 G1' (x : var) A T T1 T2 T3 T' V S U U',
    In x V ->
    and_trav T T1 T2 ->
    revealing G1 T1 G1' T' ->
    and_trav T' (typ_dec $ dec_bnd A S U) T3 ->
    prodemotion pro G1' U V U' ->
    prodemotion pro (G2 ++ x ~ T ++ G1) (typ_sel x A) V U'
| pr_sel3 : forall G1 G2 G1' (x : var) A T T1 T2 T3 T' V S U U',
    In x V ->
    and_trav T T1 T2 ->
    revealing G1 T1 G1' T' ->
    and_trav T' (typ_mu $ dec_bnd A S U) T3 ->
    prodemotion pro (x ~ typ_and T2 T3 ++ G1') (open x U) V U' ->
    prodemotion pro (G2 ++ x ~ T ++ G1) (typ_sel x A) V U'

| de_sel1 : forall G1 G2 G1' (x : var) A T T1 T2 T3 T' V,
    In x V ->
    and_trav T T1 T2 ->
    revealing G1 T1 G1' T' ->
    and_trav T' typ_bot T3 ->
    prodemotion de (G2 ++ x ~ T ++ G1) (typ_sel x A) V typ_top
| de_sel2 : forall G1 G2 G1' (x : var) A T T1 T2 T3 T' V S U S',
    In x V ->
    and_trav T T1 T2 ->
    revealing G1 T1 G1' T' ->
    and_trav T' (typ_dec $ dec_bnd A S U) T3 ->
    prodemotion de G1' S V S' ->
    prodemotion de (G2 ++ x ~ T ++ G1) (typ_sel x A) V S'
| de_sel3 : forall G1 G2 G1' (x : var) A T T1 T2 T3 T' V S U S',
    In x V ->
    and_trav T T1 T2 ->
    revealing G1 T1 G1' T' ->
    and_trav T' (typ_mu $ dec_bnd A S U) T3 ->
    prodemotion de (x ~ typ_and T2 T3 ++ G1') (open x S) V S' ->
    prodemotion de (G2 ++ x ~ T ++ G1) (typ_sel x A) V S'

| pd_all : forall m G T U T' U' V,
    prodemotion (oppode m) G T V T' ->
    prodemotion m G U V U' ->
    prodemotion m G (typ_all T U) V (typ_all T' U')

| pd_bnd : forall m G A S U S' U' V,
    prodemotion (oppode m) G S V S' ->
    prodemotion m G U V U' ->
    prodemotion m G (typ_dec $ dec_bnd A S U) V (typ_dec $ dec_bnd A S' U')
| pd_fld : forall m G a T T' V,
    prodemotion m G T V T' ->
    prodemotion m G (typ_dec $ dec_fld a T) V (typ_dec $ dec_fld a T')

| pd_and : forall m G S U S' U' V,
    prodemotion m G S V S' ->
    prodemotion m G U V U' ->
    prodemotion m G (typ_and S U) V (typ_and S' U')
| pd_mu : forall m G D V,
    ~ captured D V ->
    prodemotion m G (typ_mu D) V (typ_mu D).
Local Hint Constructors prodemotion.

Inductive motion_termination : forall m G T V T',
    prodemotion m G T V T' -> Prop :=
| pdt_top : forall m G V, motion_termination (pd_top m G V)
| prt_top : forall G T V, motion_termination (pr_top G T V)
| pdt_bot : forall m G V, motion_termination (pd_bot m G V)
| det_bot : forall G T V, motion_termination (de_bot G T V)

| pdt_seln : forall m G n A V,
    motion_termination (pd_seln m G n A V)
| pdt_sel_neq : forall m G (x : var) A V
                  (notin : ~In x V),
    motion_termination (pd_sel_neq m G x A V notin)

| prt_sel1 : forall G1 G2 G1' (x : var) A T T1 T2 T3 T' V
               (notin : In x V)
               (At1 : and_trav T T1 T2)
               (Rv : revealing G1 T1 G1' T')
               (At2 : and_trav T' typ_bot T3),
    motion_termination (pr_sel1 G2 x A V notin At1 Rv At2)
| prt_sel2 : forall G1 G2 G1' (x : var) A T T1 T2 T3 T' V S U U'
               (notin : In x V)
               (At1 : and_trav T T1 T2)
               (Rv : revealing G1 T1 G1' T')
               (At2 : and_trav T' (typ_dec $ dec_bnd A S U) T3)
               (Rec : prodemotion pro G1' U V U'),
    tuple_measure_rel (G1', U) (G2 ++ x ~ T ++ G1, typ_sel x A) ->
    motion_termination (pr_sel2 G2 x notin At1 Rv At2 Rec)
| prt_sel3 : forall G1 G2 G1' (x : var) A T T1 T2 T3 T' V S U U'
               (notin : In x V)
               (At1 : and_trav T T1 T2)
               (Rv : revealing G1 T1 G1' T')
               (At2 : and_trav T' (typ_mu $ dec_bnd A S U) T3)
               (Rec : prodemotion pro (x ~ typ_and T2 T3 ++ G1') (open x U) V U'),
    tuple_measure_rel (x ~ typ_and T2 T3 ++ G1', open x U)
                      (G2 ++ x ~ T ++ G1, typ_sel x A) ->
    motion_termination (pr_sel3 G2 notin At1 Rv At2 Rec)

| det_sel1 : forall G1 G2 G1' (x : var) A T T1 T2 T3 T' V
               (notin : In x V)
               (At1 : and_trav T T1 T2)
               (Rv : revealing G1 T1 G1' T')
               (At2 : and_trav T' typ_bot T3),
    motion_termination (de_sel1 G2 x A V notin At1 Rv At2)
| det_sel2 : forall G1 G2 G1' (x : var) A T T1 T2 T3 T' V S U S'
               (notin : In x V)
               (At1 : and_trav T T1 T2)
               (Rv : revealing G1 T1 G1' T')
               (At2 : and_trav T' (typ_dec $ dec_bnd A S U) T3)
               (Rec : prodemotion de G1' S V S'),
    tuple_measure_rel (G1', S) (G2 ++ x ~ T ++ G1, typ_sel x A) ->
    motion_termination (de_sel2 G2 x notin At1 Rv At2 Rec)
| det_sel3 : forall G1 G2 G1' (x : var) A T T1 T2 T3 T' V S U S'
               (notin : In x V)
               (At1 : and_trav T T1 T2)
               (Rv : revealing G1 T1 G1' T')
               (At2 : and_trav T' (typ_mu $ dec_bnd A S U) T3)
               (Rec : prodemotion de (x ~ typ_and T2 T3 ++ G1') (open x S) V S'),
    tuple_measure_rel (x ~ typ_and T2 T3 ++ G1', open x S)
                      (G2 ++ x ~ T ++ G1, typ_sel x A) ->
    motion_termination (de_sel3 G2 notin At1 Rv At2 Rec)

| pdt_all : forall m G T U T' U' V
              (Rec1 : prodemotion (oppode m) G T V T')
              (Rec2 : prodemotion m G U V U'),
    tuple_measure_rel (G, T) (G, typ_all T U) ->
    tuple_measure_rel (G, U) (G, typ_all T U) ->
    motion_termination (pd_all Rec1 Rec2)

| pdt_bnd : forall m G A S U S' U' V
              (Rec1 : prodemotion (oppode m) G S V S')
              (Rec2 : prodemotion m G U V U'),
    tuple_measure_rel (G, S) (G, typ_dec $ dec_bnd A S U) ->
    tuple_measure_rel (G, U) (G, typ_dec $ dec_bnd A S U) ->
    motion_termination (pd_bnd A Rec1 Rec2)
| pdt_fld : forall m G a T T' V
              (Rec : prodemotion m G T V T'),
    tuple_measure_rel (G, T) (G, typ_dec $ dec_fld a T) ->
    motion_termination (pd_fld a Rec)

| pdt_and : forall m G S U S' U' V
              (Rec1 : prodemotion m G S V S')
              (Rec2 : prodemotion m G U V U'),
    tuple_measure_rel (G, S) (G, typ_and S U) ->
    tuple_measure_rel (G, U) (G, typ_and S U) ->
    motion_termination (pd_and Rec1 Rec2)
| pdt_mu : forall m G D V
             (nc : ~ captured D V),
    motion_termination (pd_mu m G nc).

Local Hint Extern 1 (_ >= _) => lia.

Section Termination.

  Hint Extern 1 (_ < _) => lia.
  Hint Constructors lexprod.
  Hint Rewrite open_typ_same_measure : measures.
  
  Fixpoint motion_terminates m G T V T' (D : prodemotion m G T V T')
           {struct D} :
    motion_termination D.
  Proof.
    destruct D; constructor; simpl;
      autorewrite with measures in *;
      and_trav_m;
      repeat match goal with
             | H : revealing _ _ _ _ |- _ =>
               apply revealing_measure in H
             end;
      simpl in *;
      auto.

    - destruct (is_and_m0_dec U).
      + apply is_and_m0_struct_measure_0 in H.
        rewrite H.
        rewrite Nat.add_0_r. auto.
      + apply not_m0_typ_struct_measure_ge_1 in H.
        auto.

    - destruct (is_and_m0_dec S).
      + apply is_and_m0_struct_measure_0 in H.
        rewrite H. auto.
      + apply not_m0_typ_struct_measure_ge_1 in H.
        auto.
  Qed.

End Termination.

Ltac simp_captured :=
  repeat lazymatch goal with
         | H : context[captured] |- _ =>
           simpl in H;
           rewrite <- Forall_Exists_neg in H;
           rewrite Forall_forall in H
         end;
  cbn [captured];
  try (rewrite <- Forall_Exists_neg in *;
       rewrite Forall_forall in *).

Section MotionProperties.

  Section NotCaptured.
    
    Ltac solve_it :=
      match goal with
      | H : forall _, In _ ?V -> _, H0 : In _ ?V |- _ =>
    specialize (H _ H0)
    end;
    fsetdec.

    Hint Extern 1 (~ captured _ _) => simp_captured.
    Hint Extern 1 (_ `notin` _) => solve_it.
    
    Lemma prodemotion_not_captured : forall T V,
        ~captured T V ->
        forall m G,
          prodemotion m G T V T
    with prodemotion_not_captured_aux : forall D V,
        ~captured D V ->
        forall m G,
          prodemotion m G (typ_dec D) V (typ_dec D).
    Proof.
      - clear prodemotion_not_captured.
        induction T; intros; auto 6.
        destruct a; routine.
        constructor. intro Contra.
        simp_captured. solve_it.

      - clear prodemotion_not_captured_aux.
        induction D; intros; constructor; auto.
    Qed.

  End NotCaptured.
    
  Lemma prodemotion_open_consistent : forall m G T V T',
    prodemotion m G T V T' ->
    wf_env G ->
    forall n y,
      y `notin` dom G ->
      ~In y V ->
      prodemotion m G (open_rec n y T) V (open_rec n y T').
  Proof.
    induction on prodemotion; intros; simpl; simpl_env; eauto.
    - destruct_eq; auto.
    - econstructor; eauto.
      wf_env.
      and_trav_wf H0. specialize (H8 _ H13).
      destruct_conjs.
      pose proof H1.
      apply revealing_wf in H1; auto.
      apply revealing_same_dom in H15.
      destruct_conjs.
      and_trav_wf H2. specialize (H19 _ H17).
      progressive_inversions.    
      rec_pose IHprodemotion Hrec; eauto.
      + rewrite dom_app in H5. simpl in *.
        fsetdec.
      + instantiate (1 := n) in Hrec.
        rewrite <- (@open_lc_le_typ U 0 n y); auto.
    - eapply pr_sel3; eauto.
      wf_env.
      and_trav_wf H0. specialize (H8 _ H13).
      destruct_conjs.
      pose proof H1.
      apply revealing_wf in H1; auto.
      apply revealing_same_dom in H15.
      destruct_conjs.
      and_trav_wf H2. specialize (H19 _ H17).
      progressive_inversions.
      rewrite dom_app in *; simpl in *.
      rec_pose IHprodemotion Hrec; auto.
      + constructor; auto.
        * fsetdec.
        * simpl in *. fsetdec.
        * routine.
      + instantiate (1 := y). fsetdec.
      + trivial.
      + instantiate (1 := n) in Hrec.
        fold_cls.
        rewrite <- (@open_lc_le_typ (open x U) 0 n y); auto.
        auto using open_lc_typ.

    - econstructor; eauto.
      wf_env.
      and_trav_wf H0. specialize (H8 _ H13).
      destruct_conjs.
      pose proof H1.
      apply revealing_wf in H1; auto.
      apply revealing_same_dom in H15.
      destruct_conjs.
      and_trav_wf H2. specialize (H19 _ H17).
      progressive_inversions.    
      rec_pose IHprodemotion Hrec; eauto.
      + rewrite dom_app in H5. simpl in *.
        fsetdec.
      + instantiate (1 := n) in Hrec.
        rewrite <- (@open_lc_le_typ S 0 n y); auto.
    - eapply de_sel3; eauto.
      wf_env.
      and_trav_wf H0. specialize (H8 _ H13).
      destruct_conjs.
      pose proof H1.
      apply revealing_wf in H1; auto.
      apply revealing_same_dom in H15.
      destruct_conjs.
      and_trav_wf H2. specialize (H19 _ H17).
      progressive_inversions.
      rewrite dom_app in *; simpl in *.
      rec_pose IHprodemotion Hrec; auto.
      + constructor; auto.
        * fsetdec.
        * simpl in *. fsetdec.
        * routine.
      + instantiate (1 := y). fsetdec.
      + trivial.
      + instantiate (1 := n) in Hrec.
        fold_cls.
        rewrite <- (@open_lc_le_typ (open x S) 0 n y); auto.
        auto using open_lc_typ.

    - constructor. simpl in *.
      rewrite <- Forall_Exists_neg in *.
      rewrite Forall_forall in *.
      intros.
      specialize (H _ H3).
      assert (x <> y) by eauto using notin_neq.
      pose proof (fv_open_dec D y (S n)).
      fsetdec.
  Qed.

  Lemma not_captured_top : forall V,
      ~ captured typ_top V.
  Proof.
    intros. simpl.
    rewrite <- Forall_Exists_neg in *.
    rewrite Forall_forall in *.
    intros. auto.
  Qed.

  Lemma not_captured_sel_n : forall V n A,
      ~ captured (typ_sel (avar_b n) A) V.
  Proof.
    intros. simpl.
    rewrite <- Forall_Exists_neg in *.
    rewrite Forall_forall in *.
    intros. auto.
  Qed.

  Hint Rewrite -> open_typ_same_measure : measures.
  Hint Rewrite -> open_typ_same_measure2 : measures.
  
  Program Fixpoint motion_soundness (tup : env * typ)
          {measure tup ( tuple_measure_rel )} :
    let (G, T) := tup in
    forall m V T',
      prodemotion m G T V T' ->
      wf_env G ->
      match m with
      | pro => G ⊢ T <⦂ T'
      | de => G ⊢ T' <⦂ T
      end := _.
  Obligation 2.
  apply tuple_measure_rel_wf.
  Qed.
  Next Obligation.
    intros. pose proof (motion_terminates H).
    destruct H1.
    all:try solve [destruct m; simpl; auto]; auto.
    all:lazymatch goal with
        | |- ?G2 ++ ?x ~ ?T ++ ?G1 ⊢ _ <⦂ _ =>
          pose proof (binds_for_sure G2 G1 x T)
        | _ => idtac
        end.

    - eapply st_sel2. instantiate (1 := typ_top).
      and_trav_sub G1.
      apply revealing_soundness in Rv.
      tidy_up.
      assert (G1 ⊢ T <⦂ typ_bot) by eauto.
      eapply ty_sub; auto.
      simpl_env. do 2 apply weaken_subty.
      eauto.
    - eapply st_sel2. instantiate (1 := S).
      and_trav_wf At1. and_trav_sub G1.
      pose proof Rv.
      apply revealing_soundness in Rv.
      wf_env. destruct_conjs.
      specialize (H3 _ H11). destruct_conjs.
      apply revealing_wf in H4; auto.
      destruct_conjs.
      assert (G1 ⊢ T <⦂ typ_dec $ dec_bnd A S U) by eauto.
      eapply ty_sub; auto.
      simpl_env. do 2 apply weaken_subty.
      specialize (motion_soundness (G1', U)).
      rec_pose motion_soundness Hrec; try eassumption.
      simpl in *.
      eapply st_trans; [eassumption | eapply st_bnd]; auto.
      eauto using narrow_subty_gen.
    - eapply st_sel2. instantiate (1 := (open x S)).
      and_trav_wf At1. and_trav_wf At2. and_trav_sub G1.
      do 2 pose proof Rv.
      apply revealing_soundness in Rv.
      wf_env. destruct_conjs.
      specialize (H3 _ H14). destruct_conjs.
      apply revealing_wf in H6; auto.
      apply revealing_same_dom in H7.
      destruct_conjs.
      assert (G1 ⊢ T <⦂ typ_mu $ dec_bnd A S U) by eauto.
      eapply ty_sub; [apply ty_mu_d_e; eapply ty_sub;
                      [eauto | eauto using weaken_subty] |].
      specialize (motion_soundness (x ~ typ_and T2 T3 ++ G1', open x U)).
      rec_pose motion_soundness Hrec; try eassumption.
      + constructor; auto.
        * fsetdec.
        * simpl in *. fsetdec.
        * specialize (H5 _ H18).
          routine.
      + simpl in *.
        eapply st_bnd; auto.
        simpl_env. apply weaken_subty.
        eapply narrow_subty_gen; try eassumption.
        constructor; trivial.
        eapply st_and_i; eauto.

    - eapply st_sel1. instantiate (1 := typ_top).
      and_trav_sub G1.
      apply revealing_soundness in Rv.
      tidy_up.
      assert (G1 ⊢ T <⦂ typ_bot) by eauto.
      eapply ty_sub; auto.
      simpl_env. eauto using weaken_subty.
    - eapply st_sel1. instantiate (1 := U).
      and_trav_wf At1. and_trav_sub G1.
      pose proof Rv.
      apply revealing_soundness in Rv.
      wf_env. destruct_conjs.
      specialize (H3 _ H11). destruct_conjs.
      apply revealing_wf in H4; auto.
      destruct_conjs.
      assert (G1 ⊢ T <⦂ typ_dec $ dec_bnd A S U) by eauto.
      eapply ty_sub; auto.
      simpl_env. do 2 apply weaken_subty.
      specialize (motion_soundness (G1', S)).
      rec_pose motion_soundness Hrec; try eassumption.
      simpl in *.
      eapply st_trans; [eassumption | eapply st_bnd]; auto.
      eauto using narrow_subty_gen.
    - eapply st_sel1. instantiate (1 := (open x U)).
      and_trav_wf At1. and_trav_wf At2. and_trav_sub G1.
      do 2 pose proof Rv.
      apply revealing_soundness in Rv.
      wf_env. destruct_conjs.
      specialize (H3 _ H14). destruct_conjs.
      apply revealing_wf in H6; auto.
      apply revealing_same_dom in H7.
      destruct_conjs.
      assert (G1 ⊢ T <⦂ typ_mu $ dec_bnd A S U) by eauto.
      eapply ty_sub; [apply ty_mu_d_e; eapply ty_sub;
                      [eauto | eauto using weaken_subty] |].
      specialize (motion_soundness (x ~ typ_and T2 T3 ++ G1', open x S)).
      rec_pose motion_soundness Hrec; try eassumption.
      + constructor; auto.
        * fsetdec.
        * simpl in *. fsetdec.
        * specialize (H5 _ H18).
          routine.
      + simpl in *.
        eapply st_bnd; auto.
        simpl_env. apply weaken_subty.
        eapply narrow_subty_gen; try eassumption.
        constructor; trivial.
        eapply st_and_i; eauto.

    - apply (motion_soundness (G, T)) in Rec1; auto.
      destruct m; simpl in *;
        eapply st_all; trivial; cofinite.
      + assert (~ In x V) by (rewrite In_is_in; auto).
        apply prodemotion_open_consistent with (n := 0) (y := x) in Rec2;
          trivial; auto.
        apply (motion_soundness (G, open x U)) in Rec2; auto;
          auto using weaken_subty.
        simpl. autorewrite with measures. trivial.
      + assert (~ In x V) by (rewrite In_is_in; auto).
        apply prodemotion_open_consistent with (n := 0) (y := x) in Rec2;
          trivial; auto.
        apply (motion_soundness (G, open x U)) in Rec2; auto;
          auto using weaken_subty.
        simpl. autorewrite with measures. trivial.

    - apply (motion_soundness (G, S)) in Rec1; auto.
      apply (motion_soundness (G, U)) in Rec2; auto.
      destruct m; routine.
    - apply (motion_soundness (G, T)) in Rec; auto.
      destruct m; routine.
    - apply (motion_soundness (G, S)) in Rec1; auto.
      apply (motion_soundness (G, U)) in Rec2; auto.
      destruct m; eroutine.
  Qed.  

  Ltac pose_neq :=
    repeat match goal with
           | H1 : ~In ?x ?V, H2 : In ?y ?V |- _ =>
             assert (y <> x) by eauto using notin_neq;
             fail_if_dup
           end.
  Hint Extern 1 (_ `notin` _) => pose_neq.
    
  Theorem prodemotion_fv_specs : forall m G T V T',
      prodemotion m G T V T' ->
      fv T' [<=] fv G `union` fv T /\ ~ captured T' V.
  Proof.
    induction on prodemotion;
      autorewrite with meta_ext;
      simpl in *; repeat rewrite <- Forall_Exists_neg, Forall_forall in *.
    1-7,10,15,17:split; auto; set solve.
    all:destruct_conjs.
    1-4:split; trivial.

    - and_trav_wf H0. and_trav_wf H2.
      apply revealing_fv_bound in H1.
      destruct_conjs. simpl in *.
      fsetdec.
    - and_trav_wf H0. and_trav_wf H2.
      apply revealing_fv_bound in H1.
      destruct_conjs.
      pose proof (fv_open_typ U x 0).
      simpl in *. fsetdec.
      
    - and_trav_wf H0. and_trav_wf H2.
      apply revealing_fv_bound in H1.
      destruct_conjs. simpl in *.
      fsetdec.
    - and_trav_wf H0. and_trav_wf H2.
      apply revealing_fv_bound in H1.
      destruct_conjs.
      pose proof (fv_open_typ S x 0).
      simpl in *. fsetdec.

    - split; [fsetdec |].
      intros.
      specialize (H2 _ H3). specialize (H0 _ H3).
      fsetdec.
    - split; [fsetdec |].
      intros.
      specialize (H2 _ H3). specialize (H0 _ H3).
      fsetdec.
    - split; [fsetdec |].
      intros.
      specialize (H2 _ H3). specialize (H0 _ H3).
      fsetdec.
  Qed.

  Theorem prodemotion_preserves_lc : forall m G T V T',
      prodemotion m G T V T' ->
      wf_env G ->
      forall n,
        lc_at n T ->
        lc_at n T'.
  Proof.
    induction on prodemotion; intros; auto.
    1-3,6:constructor.
    5-8:routine.
    - wf_env.
      and_trav_wf H0. and_trav_wf H2.
      pose proof (revealing_same_dom H1).
      specialize (H7 _ ltac:(eauto)).
      destruct_conjs.
      apply revealing_finds_lc in H1; auto.
      destruct_conjs.
      specialize (H13 _ ltac:(eauto)).
      destruct_conjs.
      progressive_inversions.
      eapply lc_relax_typ; eauto.
      lia.
    - wf_env.
      and_trav_wf H0. and_trav_wf H2.
      pose proof (revealing_same_dom H1).
      specialize (H7 _ ltac:(eauto)).
      destruct_conjs.
      pose proof (revealing_fv_bound H1).
      apply revealing_finds_lc in H1; auto.
      destruct_conjs.
      specialize (H13 _ ltac:(eauto)).
      destruct_conjs.
      progressive_inversions.
      apply IHprodemotion.
      + constructor; auto.
        * fsetdec.
        * rewrite (wf_fv_is_dom H11) in H19.
          simpl in *; fsetdec.
        * routine.
      + apply open_lc_typ with (x := x) in H28.
        eapply lc_relax_typ; eauto.
        lia.

    - wf_env.
      and_trav_wf H0. and_trav_wf H2.
      pose proof (revealing_same_dom H1).
      specialize (H7 _ ltac:(eauto)).
      destruct_conjs.
      apply revealing_finds_lc in H1; auto.
      destruct_conjs.
      specialize (H13 _ ltac:(eauto)).
      destruct_conjs.
      progressive_inversions.
      eapply lc_relax_typ; eauto.
      lia.
    - wf_env.
      and_trav_wf H0. and_trav_wf H2.
      pose proof (revealing_same_dom H1).
      specialize (H7 _ ltac:(eauto)).
      destruct_conjs.
      pose proof (revealing_fv_bound H1).
      apply revealing_finds_lc in H1; auto.
      destruct_conjs.
      specialize (H13 _ ltac:(eauto)).
      destruct_conjs.
      progressive_inversions.
      apply IHprodemotion.
      + constructor; auto.
        * fsetdec.
        * rewrite (wf_fv_is_dom H11) in H19.
          simpl in *; fsetdec.
        * routine.
      + apply open_lc_typ with (x := x) in H26.
        eapply lc_relax_typ; eauto.
        lia.
  Qed.
  
End MotionProperties.

Definition promotion := prodemotion pro.
Definition demotion := prodemotion de.
Arguments promotion/.
Arguments demotion/.

Theorem promotion_soundness : forall G S V U,
    promotion G S V U ->
    wf_env G ->
    G ⊢ S <⦂ U.
Proof.
  intros.
  pose proof (motion_soundness (G, S)).
  simpl in *.
  apply H1 in H; trivial.
Qed.

Theorem demotion_soundness : forall G S V U,
    demotion G S V U ->
    wf_env G ->
    G ⊢ U <⦂ S.
Proof.
  intros.
  pose proof (motion_soundness (G, S)).
  simpl in *.
  apply H1 in H; trivial.
Qed.
