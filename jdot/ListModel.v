Set Implicit Arguments.
Require Import Definitions.

Definition Head : trm_label := trm_lab 0.
Definition Tail : trm_label := trm_lab 1.
Definition Nil : trm_label := trm_lab 2.
Definition Cons : trm_label := trm_lab 3.
Definition A : typ_label := typ_lab 0.
Definition B : typ_label := typ_lab 1.
Definition ListTag : typ_label := typ_lab 2.

Definition this : avar := avar_b 0.
Definition super : avar := avar_b 1.
Definition ssuper : avar := avar_b 2.
Definition sssuper : avar := avar_b 3.
Definition ssssuper : avar := avar_b 4.

Definition ListOf rel T :=
  typ_and (typ_dec $ dec_bnd A typ_bot T) (typ_sel rel ListTag).
Arguments ListOf rel T/.
Notation "'List[' T ']'" := (ListOf this T).

Definition List rel :=
  typ_and (typ_mu $ dec_fld Head (typ_sel this A))
          (typ_mu $ dec_fld Tail (ListOf rel $ typ_sel this A)).
Arguments List rel/.

Definition nil_sig : typ := List[ typ_bot ].
Definition cons_sig : typ :=
  typ_all (typ_dec $ dec_bnd B typ_bot typ_top) $
  typ_all (typ_sel this B) $
  typ_all (ListOf ssuper (typ_sel super B))
  (ListOf sssuper (typ_sel ssuper B)).
Arguments nil_sig/.
Arguments cons_sig/.

Definition instan_bnd A T :=
  dec_bnd A T T.
Arguments instan_bnd A T/.

Definition package_sig : typ :=
  typ_and (typ_mu $ instan_bnd ListTag (List super)) $
  typ_and (typ_mu $ dec_fld Nil nil_sig)
          (typ_mu $ dec_fld Cons cons_sig).
Arguments package_sig/.

Definition func (T : typ) (v : trm) : trm :=
  trm_val $ val_lam T v.
Arguments func T v/.

Definition package_def : trm :=
  trm_val $ val_obj package_sig $
          defs_cons (def_typ ListTag (List super)) $
          defs_cons (def_trm Nil $
                             trm_let None
                             (trm_val $ val_obj (typ_and (typ_mu $ instan_bnd A typ_bot)
                                                         (List super)) $
                                      defs_cons (def_typ A typ_bot) $
                                      defs_cons (def_trm Head (trm_sel this Head)) $
                                      defs_cons (def_trm Tail (trm_sel this Tail)) $
                                      defs_nil)
                             (trm_var this)) $

          defs_cons (def_trm Cons $
                             func (typ_dec $ dec_bnd B typ_bot typ_top) $
                             func (typ_sel this B) $
                             func (ListOf ssuper (typ_sel super B)) $
                             trm_let None 
                             (trm_val $ val_obj (typ_and (typ_mu $ instan_bnd A (typ_sel sssuper B))
                                                         (List ssssuper)) $
                                      defs_cons (def_typ A (typ_sel sssuper B)) $
                                      defs_cons (def_trm Head (trm_var ssuper)) $
                                      defs_cons (def_trm Tail (trm_var super)) $
                                      defs_nil)
                             (trm_var this))
          defs_nil.
Arguments package_def/.

Local Ltac solve_luniq :=
  repeat constructor; solve_label_notin; cbv; congruence.

Local Ltac obj_defs := repeat (apply tds_cons || apply tds_one).
Local Ltac ty_obj := eapply ty_obj; [| solve_luniq]; cofinite; obj_defs.

Local Ltac dec_close D x lv :=
  lazymatch D with
  | context C [avar_f x] =>
    let D' := context C[lv] in
    dec_close D' x lv
  | _ => D
  end.

Local Ltac dec_to_open x :=
  lazymatch goal with
  | |- typing _ _ (typ_dec ?D) =>
    (let D' := dec_close D x this in
     change D with (open x D'))
    ||
    (let D' := dec_close D x super in
     change D with (open x D'))
    ||
    (let D' := dec_close D x ssuper in
    change D with (open x D'))
    ||
    (let D' := dec_close D x sssuper in
    change D with (open x D'))
    ||
    (let D' := dec_close D x ssssuper in
    change D with (open x D'))
  end.

Local Ltac mu_e x :=
  dec_to_open x;
  apply ty_mu_d_e; eauto using ty_sub.

Local Ltac ty_var := eapply ty_var; eauto.

Local Ltac st_and_e1 := eapply st_trans; [eapply st_and_e1 |].
Local Ltac st_and_e2 := eapply st_trans; [eapply st_and_e2 |].

Theorem package_type_check :
  nil ⊢ package_def ⦂ package_sig.
Proof.
  unfold package_def, package_sig.
  ty_obj.
  - constructor.
  - simpl; constructor. eapply ty_let1.
    + ty_obj.
      * constructor.
      * simpl. do 2 constructor. mu_e x0.
      * simpl. do 2 constructor. mu_e x0.
    + cofinite. simpl.
      apply ty_refine.
      * mu_e x0.
      * eapply ty_sub;
          [| eapply st_sel1; do 2 instantiate (1 := List (avar_f x));
             mu_e x].

        simpl. eauto using ty_sub.
  - simpl; constructor. repeat (eapply ty_lam; cofinite).
    simpl. eapply ty_let1.
    + ty_obj.
      * constructor.
      * simpl. constructor.
        eapply ty_sub; [ty_var |].

        eapply st_sel1. instantiate (1 := typ_sel x0 B).
        mu_e x3.
      * simpl. constructor.
        eapply ty_sub; [ty_var |].
        eapply st_and_i; auto.
        st_and_e1. eapply st_bnd; trivial.
        eapply st_sel1; auto.
        instantiate (1 := typ_sel x0 B).
        mu_e x3.
    + cofinite. simpl.
      apply ty_refine.
      * eapply ty_sub.
        -- instantiate (1 := typ_dec $ instan_bnd A (typ_sel x0 B)).
           mu_e x3.
        -- eapply st_bnd; auto.
      * eapply ty_sub;
          [| eapply st_sel1; do 2 instantiate (1 := List (avar_f x))].
        -- simpl. eauto using ty_sub.
        -- mu_e x. eapply ty_sub; [ty_var |]; eauto 10.
Qed.

Require Import OperationProperties.
Require Import Revealing.
Require Import Motions.
Require Import Subtyping.
Require Import VarTyping.
Require Import TypingRules.
Require Import Misc.

Local Ltac ch_objs :=
  repeat (apply ch_cons || apply ch_one || apply ch_typ || apply ch_trm).

Local Hint Constructors ty_check ty_synthesis def_check defs_check.
Local Hint Constructors convert synthesize.
Local Hint Extern 1 => simpl.

Lemma empty_not_captured : forall (T : typ),
    ~ captured T nil.
Proof.
  intros. simp_captured.
  intros. contradiction.
Qed.
Local Hint Resolve empty_not_captured bi_subty_refl.

Theorem alg_package_type_check :
  nil ⊢⇓ package_def ⦂ package_sig.
Proof.
  unfold package_def, package_sig.
  pick_fresh x. apply (@ty_ch_obj x); try solve_luniq; auto.
  simpl. ch_objs.
  - pick_fresh y. eapply (@ty_ch_let1 y).
    + apply (@ty_syn_obj y);
        simpl; try apply prodemotion_not_captured;
        try solve_luniq; auto.
      simpl. ch_objs; simpl.
      * reassoc 2 with 0. eapply ty_ch_sel3.
        -- apply at_right. apply at_left. auto.
        -- apply rv_stop. auto.
        -- auto.
        -- simpl. auto.
      * reassoc 2 with 0. eapply ty_ch_sel3.
        -- apply at_right. apply at_right. auto.
        -- apply rv_stop. auto.
        -- auto.
        -- simpl. auto.
    + simpl. auto.
    + simpl. constructor. eexists. split; simpl.
      * eauto.
      * apply cv_and_right; [apply cv_and_left1 | apply cv_and_left2]; auto.
        eapply cv_sel_right.
        -- reassoc 2 with 1 2.
           eapply dc_mu.
           apply at_left. auto.
           apply rv_stop; auto.
           auto.
        -- simpl. auto.

  - simpl. pick_fresh y. eapply (@ty_ch_fun2 y); simpl; auto.
    simpl. pick_fresh z. eapply (@ty_ch_fun2 z); simpl; auto.
    simpl. pick_fresh w. eapply (@ty_ch_fun2 w); simpl; auto.
    
    pick_fresh v.
    eapply (@ty_ch_let1 v).
    pick_fresh a.
    apply (@ty_syn_obj a);
        simpl; try apply prodemotion_not_captured;
          try solve_luniq; auto.
    2:simpl; auto.
    simpl. ch_objs; simpl.
    + constructor. eexists; split; eauto.
      eapply cv_sel_right.
      * reassoc 5 with 0 1. eapply dc_mu.
        apply at_left; auto.
        apply rv_stop; auto.
        auto.
      * simpl; auto.

    + constructor. eexists; split; auto.
      apply cv_and_right; [apply cv_and_left1 | apply cv_and_left2]; auto.
      do 2 constructor; trivial.
      eapply bs_sel_right.
      * reassoc 5 with 0 1. eapply dc_mu.
        apply at_left; auto.
        apply rv_stop; auto.
        auto.
      * simpl; auto.
    + simpl. constructor.
      eexists; split; eauto.
      apply cv_and_right; [apply cv_and_left1 | apply cv_and_left2]; auto.
      eapply cv_sel_right.
      * reassoc 5 with 4 5. eapply dc_mu.
        apply at_left; auto.
        apply rv_stop; auto.
        auto.
      * simpl. auto.
Qed.
