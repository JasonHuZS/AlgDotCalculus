## Algorithmic Analysis of jDOT

This directory focuses on algorithmic analysis of jDOT. It defines:

1. The declarative jDOT, and
1. Stare-at subtyping.

The naming convention in this directory is locally nameless representation with
cofinite quantification.

### Definitions

| Definition | File                   | Correspondences                                       |
|------------|------------------------|-------------------------------------------------------|
| 7.1        | Definitions.v          | `typing`, `subty`, `typing_d`, `typing_ds`            |
| 7.2        | Revealing.v            | `and_trav`, `revealing`                               |
| 7.3        | Revealing.v            | `upcast`, `downcast`                                  |
| 7.4        | StructuralProperties.v | `subenv`                                              |
| 7.5        | Misc.v                 | `typ_struct_measure`, `typ_struct_measure2`           |
| 7.6        | Misc.v                 | `subty_measure`                                       |
| 7.7        | Subtyping.v            | `bi_subty`                                            |
| 7.8        | VarTyping.v            | `convert`, `assignable`                               |
| 7.9        | VarTyping.v            | `synthesize`, `syn_var`                               |
| 7.10       | Motions.v              | `prodemotion`                                         |
| 7.11       | TypingRules.v          | `ty_synthesis`, `ty_check`, `def_check`, `defs_check` |

### Conclusions

| Conclusion               | File                   | Correspondences                                     |
|--------------------------|------------------------|-----------------------------------------------------|
| Lemma 7.1                | Revealing.v            | `and_trav_sub`, `and_trav_sup`                      |
| Theorem 7.2, 7.3         | Revealing.v            | `revealing_soundness`                               |
| Lemma 7.4                | StructuralProperties.v | `subenv_dom`                                        |
| Lemma 7.5                | Revealing.v            | `revealing_not_sel`                                 |
| Theorem 7.6              | Revealing.v            | `revealing_same_dom`, `revealing_fv_bound`          |
| Lemma 7.7                | Revealing.v            | `revealing_measure`                                 |
| Theorem 7.8              | Revealing.v            | `revealing_terminates`                              |
| Lemma 7.9                | Revealing.v            | `upcast_soundness`, `downcast_soundness`            |
| Lemma 7.10               | Revealing.v            | `upcast_measure`, `downcast_measure`                |
| Theorem 7.11             | Subtyping.v            | `bi_subty_refl`                                     |
| Theorem 7.12             | Subtyping.v            | `bi_subty_sound_gen`                                |
| Theorem 7.13             | Subtyping.v            | `bi_subty_sound`                                    |
| Theorem 7.14             | Subtyping.v            | `bi_subty_terminates`                               |
| Theorem 7.15             | VarTyping.v            | `assginable_sound`                                  |
| Theorem 7.16             | VarTyping.v            | `convert_terminates`                                |
| Theorem 7.17             | VarTyping.v            | `syn_var_soundness`                                 |
| Lemma 7.18               | VarTyping.v            | `synthesize_preserves_wf`, `synthesize_not_capture` |
| Theorem 7.19             | VarTyping.v            | `synthesize_terminates`                             |
| Lemma 7.20               | Motions.v              | `motion_soundness`                                  |
| Lemma 7.21               | Motions.v              | `prodemotion_fv_specs`                              |
| Theorem 7.22, 7.23, 7.24 | TypingRules.v          | `alg_soundness`                                     |
| Lemma 7.26               | ListModel.v            | `package_type_check`                                |
| Lemma 7.27               | ListModel.v            | `alg_package_type_check`                            |
