Set Implicit Arguments.
Require Import Definitions.
Require Import ListRelations.

Inductive wf_env : env -> Prop :=
| wf_nil : wf_env nil
| wf_cons : forall {x T G}, x \notin dom G ->
                       fv T [<=] dom G ->
                       wf_env G ->
                       lc T ->
                       wf_env (x ~ T ++ G).
Hint Constructors wf_env.

Lemma wf_decons : forall x T G, wf_env (x ~ T ++ G) -> x \notin fv T.
Proof. routine. Qed.

Lemma wf_deapp : forall G1 G2, wf_env (G1 ++ G2) -> wf_env G2.
Proof. induction on env; routine. Qed.

Lemma wf_uniq : forall G, wf_env G -> uniq G.
Proof. induction on env; routine. Qed.

Lemma wf_var_in : forall G1 G2 v,
    wf_env (G1 ++ G2) ->
    v `in` dom G2 ->
    v `notin` dom G1.
Proof.
  induction G1; intros.
  - routine.
  - tidy_up. repeat rewrite dom_app in *.
    destruct_notin.
    rewrite AtomSetProperties.add_union_singleton.
    intro Contra.
    apply AtomSetImpl.union_1 in Contra.
    destruct Contra.
    + apply AtomSetImpl.singleton_1 in H1. subst.
      intuition.
    + eapply IHG1; eassumption.
Qed.        

Lemma wf_app : forall G1 G2,
    wf_env (G1 ++ G2) ->
    forall G3,
      dom G3 [=] dom G2 ->
      wf_env G3 ->
      wf_env (G1 ++ G3).
Proof.
  dep induction on wf_env; intros.
  - destruct H, H0; tidy_up.
    trivial.
  - destruct H; progressive_inversions;
      simpl in *; subst; simpl in *;
        trivial.

    specialize (IHwf_env _ _ eq_refl _ H5 H6).
    constructor; auto;
      rewrite dom_app in *; fsetdec.
Qed.

Lemma wf_fv_is_dom : forall G,
    wf_env G ->
    fv G [=] dom G.
Proof.
  induction on wf_env; set solve.
  autorewrite with meta_ext. rewrite IHwf_env.
  simpl. fsetdec.
Qed.

Lemma binds_fv : forall x T G,
    binds x T G ->
    wf_env G ->
    fv T [<=] dom G.
Proof. induction G; routine. Qed.

Ltac wf_env :=
  lazymatch goal with
  | H : wf_env (_ ++ _) |- _ => apply wf_deapp in H; invert H; subst
  end.

Fixpoint typ_struct_measure (T : typ) :=
  match T with
  | typ_top => 0
  | typ_bot => 0
  | typ_sel x A => 2
  | typ_all T U => 1 + typ_struct_measure T + typ_struct_measure U
  | typ_dec D => 1 + dec_struct_measure D
  | typ_mu D => 2 + dec_struct_measure D
  | typ_and T U => typ_struct_measure T + typ_struct_measure U
  end
with
dec_struct_measure (D : dec) :=
  match D with
  | dec_bnd A T U => typ_struct_measure T + typ_struct_measure U
  | dec_fld a T => typ_struct_measure T
  end.

Fixpoint typ_struct_measure2 (T : typ) :=
  match T with
  | typ_top => 1
  | typ_bot => 1
  | typ_sel x A => 2
  | typ_all T U => 1 + typ_struct_measure2 T + typ_struct_measure2 U
  | typ_dec D => 1 + dec_struct_measure2 D
  | typ_mu D => 2 + dec_struct_measure2 D
  | typ_and T U => 1 + typ_struct_measure2 T + typ_struct_measure2 U
  end
with
dec_struct_measure2 (D : dec) :=
  match D with
  | dec_bnd A T U => typ_struct_measure2 T + typ_struct_measure2 U
  | dec_fld a T => typ_struct_measure2 T
  end.

Local Ltac simplify :=
  intros; cbn in *; try lia.

Local Ltac finish :=
  repeat match goal with
         | H : context[forall _, _ = _] |- _ =>
           rewrite H
         end;
  reflexivity.

Lemma open_typ_same_measure : forall T k u,
    typ_struct_measure $ open_rec_typ k u T = typ_struct_measure T
with open_dec_same_measure : forall D k u,
    dec_struct_measure $ open_rec_dec k u D = dec_struct_measure D.
Proof.
  - clear open_typ_same_measure.
    induction T; simplify; finish.
  - clear open_dec_same_measure.
    induction D; simplify; finish.
Qed.

Lemma open_typ_same_measure2 : forall T k u,
    typ_struct_measure2 $ open_rec_typ k u T = typ_struct_measure2 T
with open_dec_same_measure2 : forall D k u,
    dec_struct_measure2 $ open_rec_dec k u D = dec_struct_measure2 D.
Proof.
  - clear open_typ_same_measure2.
    induction T; simplify; finish.
  - clear open_dec_same_measure2.
    induction D; simplify; finish.
Qed.

Lemma typ_struct_measure2_ge_1 : forall T,
    typ_struct_measure2 T >= 1.
Proof. destruct T; simpl; lia. Qed.

Definition total (ns : list nat) : nat :=
  fold_right plus 0 ns.

Lemma total_app : forall ns1 ns2,
    total (ns1 ++ ns2) = total ns1 + total ns2.
Proof.
  unfold total.
  induction on list; simpl in *; intros; trivial.
  rewrite IHlist. lia.
Qed.

Definition env_measure (G : env) : nat :=
  total (List.map (fun (tup : var * typ) =>
        let (x, T) := tup in typ_struct_measure T) G).

Lemma env_measure_cons : forall x T G,
    env_measure ((x, T) :: G) = typ_struct_measure T + env_measure G.
Proof. routine. Qed.

Lemma env_measure_app : forall G1 G2,
    env_measure (G2 ++ G1) = env_measure G2 + env_measure G1.
Proof.
  induction G2; simpl; trivial.
  destruct a. rewrite! env_measure_cons.
  rewrite IHG2. lia.
Qed.
  
Arguments env_measure G : simpl never.
Create HintDb measures discriminated.
Hint Rewrite -> env_measure_cons env_measure_app total_app : measures.

Definition subty_measure G T : nat :=
  env_measure G + typ_struct_measure T.
Arguments subty_measure G T/.

Section Predicates.

  Definition is_top (T : typ) :=
    match T with
    | typ_top => True
    | _ => False
    end.

  Definition is_top_dec (T : typ) : {is_top T} + {~is_top T}.
  Proof. destruct T; simpl; auto. Defined.

  Definition is_bot (T : typ) :=
    match T with
    | typ_bot => True
    | _ => False
    end.

  Definition is_bot_dec (T : typ) : {is_bot T} + {~is_bot T}.
  Proof. destruct T; simpl; auto. Defined.

  Definition is_sel (T : typ) :=
    match T with
    | typ_sel _ _ => True
    | _ => False
    end.

  Definition is_sel_dec (T : typ) : {x & { A | T = typ_sel x A } } + {~is_sel T}.
  Proof. destruct T; simpl; eauto. Defined.

  Definition is_all (T : typ) :=
    match T with
    | typ_all _ _ => True
    | _ => False
    end.

  Definition is_all_dec (T : typ) : {S & { U | T = typ_all S U } } + {~is_all T}.
  Proof. destruct T; simpl; eauto. Defined.
  
  Definition is_bnd (D : dec) :=
    match D with
    | dec_bnd _ _ _ => True
    | _ => False
    end.

  Definition is_bnd_dec (D : dec) :
    {A & {S & { U | D = dec_bnd A S U } } } + {~is_bnd D}.
  Proof. destruct D; simpl; eauto. Defined.

  Definition is_fld (D : dec) :=
    match D with
    | dec_fld _ _ => True
    | _ => False
    end.

  Definition is_fld_dec (D : dec) :
    {a & { T' | D = dec_fld a T' } } + {~is_fld D}.
  Proof. destruct D; simpl; eauto. Defined.

  Definition is_dec (T : typ) :=
    match T with
    | typ_dec _ => True
    | _ => False
    end.

  Definition is_dec_dec (T : typ) :
    {D | T = typ_dec D } + { ~ is_dec T }.
  Proof. destruct T; simpl; eauto. Defined.

  Definition is_mu (T : typ) :=
    match T with
    | typ_mu _ => True
    | _ => False
    end.

  Definition is_mu_dec (T : typ) :
    {D | T = typ_mu D } + { ~ is_mu T }.
  Proof. destruct T; simpl; eauto. Defined.
  
  Definition is_and (T : typ) :=
    match T with
    | typ_and _ _ => True
    | _ => False
    end.

  Definition is_and_dec (T : typ) : {S & { U | T = typ_and S U } } + {~is_and T}.
  Proof. destruct T; simpl; eauto. Defined.
  
  Fixpoint is_and_m0 (T : typ) : Prop :=
    match T with
    | typ_top => True
    | typ_bot => True
    | typ_and T U => is_and_m0 T /\ is_and_m0 U
    | _ => False
    end.

  Lemma is_and_m0_dec : forall T,
      is_and_m0 T \/ ~is_and_m0 T.
  Proof.
    induction on typ; routine;
      right; intro Contra; routine.
  Qed.

  Lemma is_and_m0_struct_measure_0 : forall T,
      is_and_m0 T ->
      typ_struct_measure T = 0.
  Proof.
    induction T; routine.
    rewrite IHT1, IHT2; auto.
  Qed.
  
  Lemma not_m0_typ_struct_measure_ge_1 : forall T,
      ~is_and_m0 T ->
      typ_struct_measure T >= 1.
  Proof.
    induction T; routine; try contradiction.
    apply Decidable.not_and in H;
      tidy_up.
    - apply IHT1 in H. lia.
    - apply IHT2 in H. lia.
    - apply is_and_m0_dec.
  Qed.

End Predicates.

Lemma prefix_wf_env : forall G1 G2,
    prefix G1 G2 ->
    wf_env G2 ->
    wf_env G1.
Proof. induction on @prefix; routine. Qed.

Lemma ope_list_fv_env_subset : forall (G1 G2 : env),
    ope_list G1 G2 ->
    fv G1 [<=] fv G2.
Proof.
  induction on @ope_list; set solve.
  all:destruct_conjs; simpl in *; fsetdec.
Qed.

Require Import Coq.Program.Wf.
Require Import Lexicographic_Product.
Require Export Coq.Relations.Relation_Operators.
Require Import Coq.Arith.Wf_nat.

Definition pair_nat := lexprod nat (fun _ => nat) lt (fun _ => lt).
Arguments pair_nat/.

Lemma pair_nat_wf : well_founded pair_nat.
Proof.
  apply wf_lexprod.
  - apply well_founded_ltof.
  - intros. apply well_founded_ltof.
Defined.

Section QuadrupleWf.

  Context (A B C D : Type).
  Variable (f g : A * B * C * D -> nat).

  Definition fg_measure (t : A * B * C * D) := existT (fun _ => nat) (f t) $ g t.
  
  Definition quadruple_tuple : relation (A * B * C * D) :=
    fun t1 t2 => pair_nat (fg_measure t1) (fg_measure t2).

  Lemma quadruple_tuple_wf : well_founded quadruple_tuple.
  Proof.
    apply measure_wf. apply pair_nat_wf.
  Qed.
  
End QuadrupleWf.
Arguments fg_measure {A B C D} f g t/.
Arguments quadruple_tuple {A B C D} f g t1 t2/.

Definition bi_subty_measure_account (t : env * typ * typ * env) :=
  match t with
  | (G1, T, U, G2) =>
    subty_measure G1 T + subty_measure G2 U
  end.

Definition bi_subty_measure_struct (t : env * typ * typ * env) :=
  match t with
  | (G1, T, U, G2) =>
    typ_struct_measure2 T + typ_struct_measure2 U
  end.

Definition bi_subty_rel :=
  quadruple_tuple bi_subty_measure_account bi_subty_measure_struct.
Arguments bi_subty_rel/.

Lemma bi_subty_measure_wf : well_founded bi_subty_rel.
Proof. apply quadruple_tuple_wf. Qed.

Definition tuple_measure (t : env * typ) : {_ : nat & nat} :=
  let (G, T) := t in
  existT _ (subty_measure G T) (typ_struct_measure2 T).

Definition tuple_measure_rel t1 t2 :=
  pair_nat (tuple_measure t1) (tuple_measure t2).
Arguments tuple_measure_rel t1 t2/.

Lemma tuple_measure_rel_wf : well_founded tuple_measure_rel.
Proof.
  apply measure_wf. apply pair_nat_wf.
Qed.