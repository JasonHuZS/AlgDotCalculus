Set Implicit Arguments.
Require Import Definitions.

Section LcOpenClosingProperties.

  Local Notation lc_relax T :=
    (forall (v : T) n m, lc_at n v -> n <= m -> lc_at m v).

  Local Hint Extern 1 => lia.
  
  Lemma lc_relax_avar : lc_relax avar.
  Proof using. induction 1; routine. Qed.
  Hint Resolve lc_relax_avar.

  Lemma lc_relax_typ : lc_relax typ
  with lc_relax_dec : lc_relax dec.
  Proof using.
    - clear lc_relax_typ.
      intros. gen m.
      induction H; eroutine.
    - clear lc_relax_dec.
      intros. gen m.
      induction H; eroutine.
  Qed.
  Hint Resolve lc_relax_typ lc_relax_dec.

  Lemma lc_relax_trm : lc_relax trm
  with lc_relax_val : lc_relax val
  with lc_relax_def : lc_relax def
  with lc_relax_defs : lc_relax defs.
  Proof using.
    - clear lc_relax_trm.
      intros. gen m.
      induction H; eroutine.
    - clear lc_relax_val.
      intros. gen m.
      induction H; eroutine.
    - clear lc_relax_def.
      intros. gen m.
      induction H; eroutine.
    - clear lc_relax_defs.
      intros. gen m.
      induction H; eroutine.
  Qed.

  Local Notation open_lc T :=
    (forall (v : T) n x, lc_at (S n) v -> lc_at n (open_rec n x v)).
  
  Lemma open_lc_avar : open_lc avar.
  Proof using.
    intros. invert H.
    - destruct_eq; routine.
    - routine.
  Qed.
  Hint Resolve open_lc_avar.

  Lemma open_lc_typ : open_lc typ
  with open_lc_dec : open_lc dec.
  Proof using.
    - clear open_lc_typ.
      intros. simpl in H.
      dependent induction H; routine.
    - clear open_lc_dec.
      intros. simpl in H.
      dependent induction H; routine.
  Qed.
  Hint Resolve open_lc_typ.

  Lemma open_lc_trm : open_lc trm
  with open_lc_val : open_lc val
  with open_lc_def : open_lc def
  with open_lc_defs : open_lc defs.
  Proof using.
    - clear open_lc_trm.
      intros. simpl in H.
      dependent induction H; routine.
    - clear open_lc_val.
      intros. simpl in H.
      dependent induction H; routine.
    - clear open_lc_def.
      intros. simpl in H.
      dependent induction H; routine.
    - clear open_lc_defs.
      intros. simpl in H.
      dependent induction H; routine.
  Qed.

  Local Notation open_lc_le T :=
    (forall (v : T) n m x, lc_at n v -> m >= n -> open_rec m x v = v).

  Lemma open_lc_le_avar : open_lc_le avar.
  Proof. destr on avar; routine. Qed.

  Lemma open_lc_le_typ : open_lc_le typ
  with open_lc_le_dec : open_lc_le dec.
  Proof.
    - clear open_lc_le_typ.
      induction on typ; routine.
      eapply open_lc_le_avar; eassumption.
      all:ctx_app; try eassumption; lia.
    - clear open_lc_le_dec.
      induction on dec; routine.
      all:ctx_app; try eassumption; lia.
  Qed.

  Lemma open_lc_le_trm : open_lc_le trm
  with open_lc_le_val : open_lc_le val
  with open_lc_le_def : open_lc_le def
  with open_lc_le_defs : open_lc_le defs.
  Proof.
    - clear open_lc_le_trm.
      induction on trm; routine.
      all:try (eapply open_lc_le_avar; eassumption).
      + ctx_app; try eassumption; lia.
      + destruct o; routine.
        eapply open_lc_le_typ; eauto.
      + invert H1; eroutine.
      + invert H1; eroutine.
    - clear open_lc_le_val.
      induction on val; routine.
      all: try (eapply open_lc_le_typ; eassumption).
      all:ctx_app; try eassumption; lia.
    - clear open_lc_le_def.
      induction on def; routine.
      all: try (eapply open_lc_le_typ; eassumption).
      all:ctx_app; try eassumption; lia.
    - clear open_lc_le_defs.
      induction on defs; routine.
      all:ctx_app; try eassumption; lia.
  Qed.
      
  Local Notation open_lc_inv T :=
    (forall (v : T) n x, lc_at n (open_rec n x v) -> lc_at (S n) v).
  
  Lemma open_lc_inv_avar : open_lc_inv avar.
  Proof using.
    intros. simpl in H. invert H.
    - destruct v; routine.
    - destruct v; routine.
  Qed.
  Hint Resolve open_lc_inv_avar.

  Lemma open_lc_inv_typ : open_lc_inv typ
  with open_lc_inv_dec : open_lc_inv dec.
  Proof using.
    - clear open_lc_inv_typ.
      induction on typ; eroutine.
    - clear open_lc_inv_dec.
      induction on dec; eroutine.
  Qed.
  Hint Resolve open_lc_inv_typ.

  Lemma open_lc_inv_trm : open_lc_inv trm
  with open_lc_inv_val : open_lc_inv val
  with open_lc_inv_def : open_lc_inv def
  with open_lc_inv_defs : open_lc_inv defs.
  Proof using.
    - clear open_lc_inv_trm.
      induction on trm; eroutine.
      destruct o; eroutine.
    - clear open_lc_inv_val.
      induction on val; eroutine.
    - clear open_lc_inv_def.
      induction on def; eroutine.
    - clear open_lc_inv_defs.
      induction on defs; eroutine.
  Qed.

  Local Notation open_left_inv T :=
    (forall (v : T) n x, lc_at n v -> open_rec n x (close_rec n x v) = v).

  Lemma open_left_inv_avar : open_left_inv avar.
  Proof using. induction 1; routine. Qed.
  Hint Resolve open_left_inv_avar.

  Lemma open_left_inv_typ : open_left_inv typ
  with open_left_inv_dec : open_left_inv dec.
  Proof using.
    - clear open_left_inv_typ.
      induction 1; routine.
    - clear open_left_inv_dec.
      induction 1; routine.
  Qed.
  Hint Resolve open_left_inv_typ.

  Lemma open_left_inv_trm : open_left_inv trm
  with open_left_inv_val : open_left_inv val
  with open_left_inv_def : open_left_inv def
  with open_left_inv_defs : open_left_inv defs.
  Proof using.
    - clear open_left_inv_trm.
      induction 1; routine.
    - clear open_left_inv_val.
      induction 1; routine.
    - clear open_left_inv_def.
      induction 1; routine.
    - clear open_left_inv_defs.
      induction 1; routine.
  Qed.

  Local Notation close_lc T :=
    (forall (v : T) n x, lc_at n v -> lc_at (S n) (close_rec n x v)).
  
  Lemma close_lc_avar : close_lc avar.
  Proof. induction on avar; routine. Qed.
  Hint Resolve close_lc_avar.
  
  Lemma close_lc_typ : close_lc typ
  with close_lc_dec : close_lc dec.
  Proof.
    - clear close_lc_typ.
      induction on typ; routine.
    - clear close_lc_dec.
      induction on dec; routine.
  Qed.
  Hint Resolve close_lc_typ.

  Lemma close_lc_trm : close_lc trm
  with close_lc_val : close_lc val
  with close_lc_def : close_lc def
  with close_lc_defs : close_lc defs.
  Proof.
    - clear close_lc_trm.
      induction on trm; routine.
      destruct o; eroutine.
    - clear close_lc_val.
      induction on val; routine.
    - clear close_lc_def.
      induction on def; routine.
    - clear close_lc_defs.
      induction on defs; routine.
  Qed.

End LcOpenClosingProperties.

Section OpenFreshInj.
  
  Variable z : atom.

  Local Notation fresh_inj T :=
    (forall (x y : T) k,
        z `notin` fv x ->
        z `notin` fv y ->
        open_rec k z x = open_rec k z y ->
        x = y).
  
  Lemma open_fresh_inj_avar : fresh_inj avar.
  Proof using.
    intros. destruct x, y; routine.
  Qed.
  Local Hint Resolve open_fresh_inj_avar.

  Local Ltac boom :=
    eroutine by 
      (idtac; match goal with
              | [ H : _ = _ _ z ?t' |- _ ] =>
                destruct t'; inversion H
              end).
  
  Lemma open_fresh_inj_typ: fresh_inj typ
  with open_fresh_inj_dec: fresh_inj dec.
  Proof using.
    - clear open_fresh_inj_typ.
      induction on typ; boom.
    - clear open_fresh_inj_dec.
      induction on dec; boom.
  Qed.
  Hint Resolve open_fresh_inj_typ.
  
  Lemma open_fresh_inj_trm : fresh_inj trm
  with open_fresh_inj_val : fresh_inj val
  with open_fresh_inj_def : fresh_inj def
  with open_fresh_inj_defs : fresh_inj defs.
  Proof using.
    - clear open_fresh_inj_trm.
      induction on trm; boom.
      destruct o, o0; boom.
    - clear open_fresh_inj_val.
      induction on val; boom.
    - clear open_fresh_inj_def.
      induction on def; boom.
    - clear open_fresh_inj_defs.
      induction on defs; boom.
  Qed.
  
End OpenFreshInj.

Section SubstFresh.
  
  Variable x y : var.

  Local Notation subst_fresh T :=
    (forall t : T, x `notin` fv t -> substi x y t = t).

  Lemma subst_fresh_avar : subst_fresh avar.
  Proof using.
    intros. destruct t; routine.
  Qed.
  Local Hint Resolve subst_fresh_avar.

  Lemma subst_fresh_typ : subst_fresh typ
  with subst_fresh_dec : subst_fresh dec.
  Proof using.
    - clear subst_fresh_typ.
      induction on typ; routine.
    - clear subst_fresh_dec.
      induction on dec; routine.
  Qed.
  Hint Resolve subst_fresh_typ.

  Lemma subst_fresh_trm : subst_fresh trm
  with subst_fresh_val : subst_fresh val
  with subst_fresh_def : subst_fresh def
  with subst_fresh_defs : subst_fresh defs.
  Proof using.
    - clear subst_fresh_trm.
      induction on trm; routine.
      destruct o; routine.
    - clear subst_fresh_val.
      induction on val; routine.
    - clear subst_fresh_def.
      induction on def; routine.
    - clear subst_fresh_defs.
      induction on defs; routine.
  Qed.
  
End SubstFresh.

Section SubstOpenComm.

  Variable x y : var.

  (** z[y/x] *)
  Definition subst_fvar (z : var) : var :=
    if z == x then y else z.

  Local Notation subst_open_comm T u :=
    (forall (t : T) (n : nat),
        substi x y (open_rec n u t) =
        open_rec n (subst_fvar u) $ substi x y t).

  Lemma subst_open_comm_avar : forall u,
      subst_open_comm avar u.
  Proof using.
    intros. destruct t; routine by (unfold subst_fvar).
  Qed.
  Hint Resolve subst_open_comm_avar.
  
  Lemma subst_open_comm_typ : forall u, subst_open_comm typ u
  with subst_open_comm_dec : forall u, subst_open_comm dec u.
  Proof using.
    - clear subst_open_comm_typ.
      induction on typ; routine.
    - clear subst_open_comm_dec.
      induction on dec; routine.
  Qed.
  Hint Resolve subst_open_comm_typ.
  
  Lemma subst_open_comm_trm : forall u, subst_open_comm trm u
  with subst_open_comm_val : forall u, subst_open_comm val u
  with subst_open_comm_def : forall u, subst_open_comm def u
  with subst_open_comm_defs : forall u, subst_open_comm defs u.
  Proof using.
    - clear subst_open_comm_trm.
      induction on trm; try solve [routine].
      intros. destruct o; routine.
    - clear subst_open_comm_val.
      induction on val; routine.
    - clear subst_open_comm_def.
      induction on def; routine.
    - clear subst_open_comm_defs.
      induction on defs; routine.
  Qed.

End SubstOpenComm.
Arguments subst_fvar x y z/.
Hint Unfold subst_fvar.

Section SubstIntro.

  Local Notation subst_intro T :=
    (forall x u (t : T) (n : nat),
        x `notin` fv t ->
        open_rec n u t = substi x u $ open_rec n x t).
  
  Local Hint Extern 1 => exrewrite subst_fresh_typ.
  Local Hint Extern 1 => exrewrite subst_fresh_dec.
  Local Hint Extern 1 => exrewrite subst_fresh_trm.
  Local Hint Extern 1 => exrewrite subst_fresh_val.
  Local Hint Extern 1 => exrewrite subst_fresh_def.
  Local Hint Extern 1 => exrewrite subst_fresh_defs.

  Local Ltac boom := routine by
        (exrewrite (conj subst_open_comm_typ $
                    conj subst_open_comm_dec $
                    conj subst_open_comm_def $
                    conj subst_open_comm_defs $
                    conj subst_open_comm_trm subst_open_comm_val)).

  Lemma subst_intro_trm : subst_intro trm.
  Proof using. boom. Qed.

  Lemma subst_intro_val : subst_intro val.
  Proof using. boom. Qed.

  Lemma subst_intro_typ : subst_intro typ.
  Proof using. boom. Qed.

  Lemma subst_intro_def : subst_intro def.
  Proof using. boom. Qed.

  Lemma subst_intro_defs : subst_intro defs.
  Proof using. boom. Qed.

  Lemma subst_intro_dec : subst_intro dec.
  Proof using. boom. Qed.

End SubstIntro.

Section SubstFvarProps.
  Variable x y z : var.
  Variable T : Type.

  Context {SubstT : CanSubst T} {OpenT : CanOpen T}.

  Variable t : T.

  Lemma open_substi_rewrite :
    z `notin` singleton x `union` singleton y ->
    open z (substi x y t) =
    open (subst_fvar x y z) (substi x y t).
  Proof using. routine. Qed.

  Hypothesis subst_open_comm :
    substi x y (open z t) = open (subst_fvar x y z) (substi x y t).
  
  
  Lemma open_substi_rewrite2 :
    z `notin` singleton x `union` singleton y ->
    open z (substi x y t) =
    substi x y (open z t).
  Proof.
    intros. rewrite open_substi_rewrite by auto.
    rewrite <- subst_open_comm. trivial.
  Qed.

End SubstFvarProps.

Ltac open_substi_transform :=
  repeat match goal with
         | |- context[?f ?v (substi ?x ?y ?t)] =>
           lazymatch f with
           | context[open] => idtac
           end;
           lazymatch v with
           | context[subst_fvar] => fail
           | _ => idtac
           end;
           replace (f v (substi x y t)) with (substi x y (open v t))
             by (rewrite open_substi_rewrite; auto)
         end.

Section FvOpen.

  Local Notation fv_open T :=
    (forall (v : T) x n, fv (open_rec n x v) [<=] singleton x \u fv v).
  
  Lemma fv_open_avar : fv_open avar.
  Proof.
    induction on avar; intros; simpl.
    - progressive_destructions; set solve.
    - set solve.
  Qed.
  Hint Resolve fv_open_avar.

  Local Ltac union_pf :=
    apply AtomSetProperties.union_subset_3;
    etransitivity; try eassumption; set solve.

  Local Ltac fold_cls :=
    fold_open_rec; fold_fv.
  
  Lemma fv_open_typ : fv_open typ
  with fv_open_dec : fv_open dec.
  Proof.
    - clear fv_open_typ.
      induction on typ; intros; simpl; set solve.
      + fold_cls.
        specialize (IHtyp1 x n).
        specialize (IHtyp2 x (S n)).
        union_pf.
      + fold_cls.
        specialize (IHtyp1 x n).
        specialize (IHtyp2 x n).
        union_pf.
    -clear fv_open_dec.
      induction on dec; intros; simpl; set solve.
      fold_cls.
      pose proof (fv_open_typ t0 x n).
      pose proof (fv_open_typ t1 x n).
      union_pf.
  Qed.  

  Lemma fv_open_trm : fv_open trm
  with fv_open_val : fv_open val
  with fv_open_def : fv_open def
  with fv_open_defs : fv_open defs.
  Proof.
    - clear fv_open_trm.
      induction on trm; intros; simpl; set solve.
      + pose proof (fv_open_avar a0 x n).
        pose proof (fv_open_avar a x n).
        union_pf.
      + fold_cls.
        specialize (IHtrm1 x n).
        specialize (IHtrm2 x (S n)).
        destruct o; simpl.
        * pose proof (fv_open_typ t x n).
          fsetdec.
        * fsetdec.

    - clear fv_open_val.
      induction on val; intros; simpl; set solve.
      + pose proof (fv_open_typ t x n).
        pose proof (fv_open_defs d x (S n)).
        union_pf.
      + pose proof (fv_open_typ t x n).
        pose proof (fv_open_trm t0 x (S n)).
        union_pf.

    - clear fv_open_def.
      induction on def; intros; simpl; set solve.
      apply fv_open_typ.

    - clear fv_open_defs.
      induction on defs; intros; simpl; set solve.
      specialize (IHdefs x n).
      pose proof (fv_open_def d x n).
      union_pf.
  Qed.

End FvOpen.

Section FvClose.

  Local Notation fv_close_self T :=
    (forall (v : T) x n, x `notin` fv (close_rec n x v)).

  Lemma fv_close_self_avar : fv_close_self avar.
  Proof. intros. destruct v; routine. Qed.

  Hint Resolve AtomSetProperties.not_in_union fv_close_self_avar.
  
  Lemma fv_close_self_typ : fv_close_self typ
  with fv_close_self_dec : fv_close_self dec.
  Proof.
    - clear fv_close_self_typ.
      induction on typ; intros; simpl in *; auto.
    - clear fv_close_self_dec.
      induction on dec; intros; simpl in *; auto.
  Qed.

  Ltac pf_union := simpl; apply AtomSetProperties.not_in_union.
  
  Lemma fv_close_self_trm : fv_close_self trm
  with fv_close_self_val : fv_close_self val
  with fv_close_self_def : fv_close_self def
  with fv_close_self_defs : fv_close_self defs.
  Proof.
    - clear fv_close_self_trm.
      induction on trm; intros; auto.
      + apply fv_close_self_avar.
      + apply fv_close_self_val.
      + apply fv_close_self_avar.
      + destruct o; auto.
        pose proof (@fv_close_self_typ t x n).
        auto.
    - clear fv_close_self_val.
      induction on val; intros.
      + pf_union.
        * apply fv_close_self_typ.
        * apply fv_close_self_defs.
      + pf_union.
        * apply fv_close_self_typ.
        * apply fv_close_self_trm.

    - clear fv_close_self_def.
      induction on def; intros.
      + apply fv_close_self_typ.
      + apply fv_close_self_trm.

    - clear fv_close_self_defs.
      induction on defs; auto.
  Qed.

  Local Notation fv_close T :=
    (forall (v : T) x n, fv (close_rec n x v) [<=] fv v).

  Lemma fv_close_avar : fv_close avar.
  Proof.
    intros. destruct v; auto.
    destruct_eq; set solve.
  Qed.

  Ltac pf_union ::= apply AtomSetProperties.union_subset_3.
  
  Lemma fv_close_typ : fv_close typ
  with fv_close_dec : fv_close dec.
  Proof.
    - clear fv_close_typ.
      induction on typ; intros; auto.
      + apply fv_close_avar.
      + simpl.
        specialize (IHtyp1 x n).
        specialize (IHtyp2 x (S n)).
        pf_union; set solve.
      + simpl. auto.
      + simpl. auto.
      + simpl.
        specialize (IHtyp1 x n).
        specialize (IHtyp2 x n).
        pf_union; set solve.

    - clear fv_close_dec.
      induction on dec; intros; simpl.
      + pose proof (fv_close_typ t0 x n).
        pose proof (fv_close_typ t1 x n).
        set solve.
      + apply fv_close_typ.
  Qed.
  
  Lemma fv_close_trm : fv_close trm
  with fv_close_val : fv_close val
  with fv_close_def : fv_close def
  with fv_close_defs : fv_close defs.
  Proof.
    - clear fv_close_trm.
      induction on trm; intros.
      + apply fv_close_avar.
      + apply fv_close_val.
      + apply fv_close_avar.
      + simpl.
        pose proof (fv_close_avar a x n).
        pose proof (fv_close_avar a0 x n).
        pf_union; set solve.
      + simpl. specialize (IHtrm1 x n).
        specialize (IHtrm2 x (S n)).
        destruct o; simpl; pf_union; set solve.
        pose proof (fv_close_typ t x n).
        set solve.

    - clear fv_close_val.
      induction on val; intros.
      + pose proof (fv_close_typ t x n).
        pose proof (fv_close_defs d x (S n)).
        set solve.
      + simpl.
        pose proof (fv_close_typ t x n).
        pose proof (fv_close_trm t0 x (S n)).
        pf_union; set solve.

    - clear fv_close_def.
      induction on def; intros.
      + apply fv_close_typ.
      + apply fv_close_trm.

    - clear fv_close_defs.
      induction on defs; intros; auto.
      pose proof (fv_close_def d x n).
      specialize (IHdefs x n).
      set solve.
  Qed.

  Local Notation fv_add_close T :=
    (forall (v : T) x (G : env) n, fv v [<=] add x (dom G) ->
                              fv (close_rec n x v) [<=] dom G).
  
  Lemma fv_add_close_avar : fv_add_close avar.
  Proof.
    intros. destruct v; simpl in *.
    - set solve.
    - destruct_eq. set solve.
      rewrite AtomSetProperties.add_union_singleton in H.
      apply AtomSetImpl.union_1 with (x := a) in H.
      destruct H.
      + apply AtomSetImpl.singleton_1 in H.
        congruence.
      + autounfold. simpl. intros.
        apply AtomSetImpl.singleton_1 in H0.
        subst. trivial.
      + apply AtomSetImpl.singleton_2. trivial.
  Qed.
  
  Lemma fv_add_close_typ : fv_add_close typ
  with fv_add_close_dec : fv_add_close dec.
  Proof.
    - clear fv_add_close_typ.
      induction on typ; intros; set solve.
      + apply fv_add_close_avar. trivial.
      + simpl in *.
        pf_union; [apply IHtyp1 | apply IHtyp2];
          set solve.
      + apply fv_add_close_dec. set solve.
      + apply fv_add_close_dec. set solve.
      + simpl in *.
        pf_union; [apply IHtyp1 | apply IHtyp2];
          set solve.

    - clear fv_add_close_dec.
      induction on dec; intros.
      + simpl in *.
        pose proof (fv_add_close_typ t0 x G n).
        pose proof (fv_add_close_typ t1 x G n).
        set solve.
      + simpl in *.
        pose proof (fv_add_close_typ t0 x G n).
        set solve.
  Qed.
  
  Lemma fv_add_close_trm : fv_add_close trm
  with fv_add_close_val : fv_add_close val
  with fv_add_close_def : fv_add_close def
  with fv_add_close_defs : fv_add_close defs.
  Proof.
    - clear fv_add_close_trm.
      induction on trm; simpl in *; intros; set solve.
      + apply fv_add_close_avar. trivial.
      + apply fv_add_close_val. trivial.
      + apply fv_add_close_avar. trivial.
      + pf_union; apply fv_add_close_avar; set solve.
      + destruct o; simpl in *.
        * pose proof (@fv_add_close_typ t x G n ltac:(auto)).
          repeat pf_union; [ | apply IHtrm1 | apply IHtrm2];
            set solve.
        * repeat pf_union; [ | apply IHtrm1 | apply IHtrm2];
            set solve.

    - clear fv_add_close_val.
      induction on val; intros; simpl in *.
      + pf_union.
        * apply fv_add_close_typ. set solve.
        * apply fv_add_close_defs. set solve.
      + pf_union.
        * apply fv_add_close_typ. set solve.
        * apply fv_add_close_trm. set solve.

    - clear fv_add_close_def.
      induction on def; intros; simpl in *.
      + apply fv_add_close_typ. set solve.
      + apply fv_add_close_trm. trivial.

    - clear fv_add_close_defs.
      induction on defs; intros; simpl in *; set solve.
      specialize (IHdefs x G n).
      pose proof (fv_add_close_def d x G n).
      set solve.
  Qed.
  
End FvClose.
