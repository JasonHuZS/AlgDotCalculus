Set Implicit Arguments.
Require Import Definitions.
Require Import OperationProperties.
Require Import StructuralProperties.
Require Import Revealing.
Require Import Subtyping.
Require Import Motions.
Require Import Misc.
Require Import VarTyping.

Reserved Notation "G '⊢⇑' t '//' V '⦂' T" (at level 70, t at level 79).
Reserved Notation "G '⊢⇓' t '⦂' U" (at level 70, t at level 79).
Reserved Notation "G ⊢⇓[ x ][ ds ⦂ DS ]" (at level 70, DS at level 79).

Inductive ty_synthesis : env -> trm -> list var -> typ -> Prop :=
| ty_syn_var : forall G x V T,
    syn_var G x V T -> G ⊢⇑ trm_var x // V ⦂ T

| ty_syn_obj : forall x G ds V T T',
    x `notin` fv G `union` fv ds `union` fv T ->
    x ~ T ++ G ⊢⇓[ x ][ ds ⦂ T ] ->
    luniq ds ->
    promotion G T V T' ->
    G ⊢⇑ trm_val (val_obj T ds) // V ⦂ T'

| ty_syn_fun : forall x G T t V T' U,
    x `notin` fv G `union` fv T `union` fv t
      `union` AtomSetProperties.of_list V ->
    x ~ T ++ G ⊢⇑ open x t // V ⦂ U ->
    demotion G T V T' ->
    G ⊢⇑ trm_val (val_lam T t) // V ⦂ (typ_all T' (close x U))

| ty_syn_sel1 : forall G2 G1 G1' x (a : trm_label) T V T0 T1 T2 T3,
    and_trav T T0 T1 ->
    revealing G1 T0 G1' T2 ->
    and_trav T2 typ_bot T3 ->
    G2 ++ x ~ T ++ G1 ⊢⇑ trm_sel x a // V ⦂ typ_bot
| ty_syn_sel2 : forall G2 G1 G1' x (a : trm_label) T V T0 T1 T2 T3 U U',
    and_trav T T0 T1 ->
    revealing G1 T0 G1' T2 ->
    and_trav T2 (typ_dec $ dec_fld a U) T3 ->
    promotion (G2 ++ x ~ T ++ G1) U V U' ->
    G2 ++ x ~ T ++ G1 ⊢⇑ trm_sel x a // V ⦂ U'
| ty_syn_sel3 : forall G2 G1 G1' x (a : trm_label) T V T0 T1 T2 T3 U U',
    and_trav T T0 T1 ->
    revealing G1 T0 G1' T2 ->
    and_trav T2 (typ_mu $ dec_fld a U) T3 ->
    promotion (G2 ++ x ~ T ++ G1) (open x U) V U' ->
    G2 ++ x ~ T ++ G1 ⊢⇑ trm_sel x a // V ⦂ U'

| ty_syn_app1 : forall G2 G1 G1' x y T V T0 T1 T2 T3,
    and_trav T T0 T1 ->
    revealing G1 T0 G1' T2 ->
    and_trav T2 typ_bot T3 ->
    y `in` dom (G2 ++ x ~ T ++ G1) ->
    G2 ++ x ~ T ++ G1 ⊢⇑ trm_app x y // V ⦂ typ_bot
| ty_syn_app2 : forall G2 G1 G1' x y T V T0 T1 T2 T3 S U U',
    and_trav T T0 T1 ->
    revealing G1 T0 G1' T2 ->
    and_trav T2 (typ_all S U) T3 ->
    G2 ++ x ~ T ++ G1 ⊢⇓ trm_var (avar_f y) ⦂ S ->
    promotion (G2 ++ x ~ T ++ G1) (open y U) V U' ->
    G2 ++ x ~ T ++ G1 ⊢⇑ trm_app (avar_f x) (avar_f y) // V ⦂ U'

| ty_syn_let1 : forall x G t1 t2 V T1 T2,
    G ⊢⇑ t1 // nil ⦂ T1 ->
    x `notin` fv G `union` fv t1 `union` fv t2
      `union` AtomSetProperties.of_list V `union` fv T1 ->
    x ~ T1 ++ G ⊢⇑ open x t2 // (x :: V) ⦂ T2 ->
    G ⊢⇑ trm_let None t1 t2 // V ⦂ T2
| ty_syn_let2 : forall x G t1 t2 V T1 T2,
    G ⊢⇓ t1 ⦂ T1 ->
    x `notin` fv G `union` fv t1 `union` fv t2
      `union` AtomSetProperties.of_list V `union` fv T1 ->
    x ~ T1 ++ G ⊢⇑ open x t2 // (x :: V) ⦂ T2 ->
    G ⊢⇑ trm_let (Some T1) t1 t2 // V ⦂ T2
                       
where "G ⊢⇑ t // V ⦂ T" := (ty_synthesis G t V T)%type
with
ty_check : env -> trm -> typ -> Prop :=
| ty_ch_var : forall G x T,
    assignable G x T -> G ⊢⇓ trm_var x ⦂ T
| ty_ch_obj : forall x G ds T T',
    x `notin` fv G `union` fv ds `union` fv T ->
    x ~ T ++ G ⊢⇓[ x ][ ds ⦂ T ] ->
    luniq ds ->
    stare_at G T T' ->
    G ⊢⇓ trm_val (val_obj T ds) ⦂ T'

| ty_ch_fun1 : forall x G T t U T',
    x `notin` fv G `union` fv T `union` fv t `union` fv T' ->
    x ~ T ++ G ⊢⇑ open x t // nil ⦂ U ->
    stare_at G (typ_all T (close x U)) T' ->
    G ⊢⇓ trm_val (val_lam T t) ⦂ T'

| ty_ch_fun2 : forall x G T t S U,
    x `notin` fv G `union` fv T `union` fv t `union` fv S `union` fv U ->
    x ~ T ++ G ⊢⇓ open x t ⦂ open x U ->
    stare_at G S T ->
    G ⊢⇓ trm_val (val_lam T t) ⦂ (typ_all S U)
      
| ty_ch_sel1 : forall G2 G1 G1' x (a : trm_label) T T0 T1 T2 T3 U,
    and_trav T T0 T1 ->
    revealing G1 T0 G1' T2 ->
    and_trav T2 typ_bot T3 ->
    G2 ++ x ~ T ++ G1 ⊢⇓ trm_sel x a ⦂ U
| ty_ch_sel2 : forall G2 G1 G1' x (a : trm_label) T T0 T1 T2 T3 U U',
    and_trav T T0 T1 ->
    revealing G1 T0 G1' T2 ->
    and_trav T2 (typ_dec $ dec_fld a U) T3 ->
    stare_at (G2 ++ x ~ T ++ G1) U U' ->
    G2 ++ x ~ T ++ G1 ⊢⇓ trm_sel x a ⦂ U'
| ty_ch_sel3 : forall G2 G1 G1' x (a : trm_label) T T0 T1 T2 T3 U U',
    and_trav T T0 T1 ->
    revealing G1 T0 G1' T2 ->
    and_trav T2 (typ_mu $ dec_fld a U) T3 ->
    stare_at (G2 ++ x ~ T ++ G1) (open x U) U' ->
    G2 ++ x ~ T ++ G1 ⊢⇓ trm_sel x a ⦂ U'

| ty_ch_app1 : forall G2 G1 G1' x y T T0 T1 T2 T3 U,
    and_trav T T0 T1 ->
    revealing G1 T0 G1' T2 ->
    and_trav T2 typ_bot T3 ->
    y `in` dom (G2 ++ x ~ T ++ G1) ->
    G2 ++ x ~ T ++ G1 ⊢⇓ trm_app x y ⦂ U
| ty_ch_app2 : forall G2 G1 G1' x y T T0 T1 T2 T3 S U U',
    and_trav T T0 T1 ->
    revealing G1 T0 G1' T2 ->
    and_trav T2 (typ_all S U) T3 ->
    G2 ++ x ~ T ++ G1 ⊢⇓ trm_var $ avar_f y ⦂ S ->
    stare_at (G2 ++ x ~ T ++ G1) (open y U) U' ->
    G2 ++ x ~ T ++ G1 ⊢⇓ trm_app x y ⦂ U'

| ty_ch_let1 : forall x G t1 t2 T1 T2,
    G ⊢⇑ t1 // nil ⦂ T1 ->
    x `notin` fv G `union` fv t1 `union` fv t2
      `union` fv T1 `union` fv T2 ->
    x ~ T1 ++ G ⊢⇓ open x t2 ⦂ T2 ->
    G ⊢⇓ trm_let None t1 t2  ⦂ T2
| ty_ch_let2 : forall x G t1 t2 T1 T2,
    G ⊢⇓ t1 ⦂ T1 ->
    x `notin` fv G `union` fv t1 `union` fv t2
      `union` fv T1 `union` fv T2 ->
    x ~ T1 ++ G ⊢⇓ open x t2 ⦂ T2 ->
    G ⊢⇓ trm_let (Some T1) t1 t2 ⦂ T2
                       
where "G ⊢⇓ t ⦂ T" := (ty_check G t T)%type
with
def_check : env -> def -> dec -> Prop :=
| ch_typ : forall G T A, def_check G (def_typ A T) (dec_bnd A T T)
| ch_trm : forall G t T a,
    G ⊢⇓ t ⦂ T ->
    def_check G (def_trm a t) (dec_fld a T)
with
defs_check : var -> env -> defs -> typ -> Prop :=
| ch_one : forall x G d D,
    def_check G (open x d) (open x D) ->
    G ⊢⇓[ x ][ defs_cons d defs_nil ⦂ typ_mu D ]
| ch_cons : forall x G d ds D T,
    def_check G (open x d) (open x D) ->
    G ⊢⇓[ x ][ ds ⦂ T ] ->
    G ⊢⇓[ x ][ defs_cons d ds ⦂ typ_and (typ_mu D) T ]
where "G ⊢⇓[ x ][ ds ⦂ DS ]" := (defs_check x G ds DS)%type.

Scheme ty_syn_mut := Induction for ty_synthesis Sort Prop
  with ty_ch_mut := Induction for ty_check Sort Prop
  with ty_def_mut := Induction for def_check Sort Prop
  with ty_defs_mut := Induction for defs_check Sort Prop.
Combined Scheme alg_mut from ty_syn_mut, ty_ch_mut, ty_def_mut, ty_defs_mut.

Local Ltac motions :=
  repeat (lazymatch goal with
          | H : promotion _ _ _ _ |- _ =>
            pose proof (promotion_soundness H ltac:(trivial))
          | H : prodemotion pro _ _ _ _ |- _ =>
            pose proof (promotion_soundness H ltac:(trivial))
          | H : demotion _ _ _ _ |- _ =>
            pose proof (demotion_soundness H ltac:(trivial))
          | H : prodemotion de _ _ _ _ |- _ =>
            pose proof (demotion_soundness H ltac:(trivial))
          end;
          fail_if_dup).

Local Ltac prime :=
  progressive_inversions; simpl in *;
  clear_dups; motions;
  repeat lazymatch goal with
         | H : prodemotion _ _ _ _ _ |- _ =>
           pose proof (prodemotion_fv_specs H);
           let H' := fresh "Hlc" in
           rec_pose (prodemotion_preserves_lc H) H'; eauto;
           fail_if_dup
         end;
  destruct_conjs;
  repeat erewrite wf_fv_is_dom in * by trivial.

Local Ltac trav_wf :=
  repeat match goal with
         | H : and_trav _ _ _ |- _ =>
           and_trav_wf H; fail_if_dup
         end;
  destruct_conjs.

Local Ltac revealing_pf :=
  match goal with
  | H : revealing _ _ _ _ |- _ =>
    pose proof (revealing_soundness H);
    pose proof (revealing_fv_bound H);
    let H' := fresh "Hlc" in
    rec_pose (revealing_finds_lc H) H'; trivial
  end;
  destruct_conjs.

Local Ltac instan_lc :=
  repeat match goal with
         | H : forall _ : nat, lc_at _ ?T -> _, H' : lc_at _ ?T |- _ =>
           specialize (H _ H')
         end;
         destruct_conjs.

Local Hint Resolve syn_var_preserves_wf syn_var_soundness syn_var_not_capture.
Local Hint Resolve wf_uniq.

Theorem alg_soundness :
  (forall G t V T,
      G ⊢⇑ t // V ⦂ T ->
      wf_env G ->
      fv t [<=] dom G -> lc t ->
      (G ⊢ t ⦂ T) /\ ~ captured T V /\ fv T [<=] dom G /\ lc T) /\
  (forall G t T,
      G ⊢⇓ t ⦂ T ->
      wf_env G ->
      fv t [<=] dom G -> lc t ->
      fv T [<=] dom G -> lc T ->
      G ⊢ t ⦂ T) /\
  (forall G d D,
      def_check G d D ->
      wf_env G ->
      fv d [<=] dom G -> lc d ->
      fv D [<=] dom G -> lc D ->
      G ⊩ d ⦂ D) /\ 
  (forall x G ds T,
      G ⊢⇓[ x ][ ds ⦂ T ] ->
      wf_env G ->
      fv ds [<=] dom G -> lc_at 1 ds -> 
      fv T [<=] dom G -> lc T ->
      x `in` dom G ->
      G ⊩[ x ][ ds ⦂ T ]).
Proof.
  apply alg_mut; intros.
  - eauto 6.
  - prime. 
    repeat split; auto.
    + rec_pose H Hrec; auto.
      * constructor; auto.
      * eapply ty_sub; eauto.
        econstructor; trivial.
        cofinite. eapply open_subst_defs; auto.
    + fsetdec.

  - prime. destruct H; destruct_conjs.
    + constructor; auto.
    + pose proof (fv_open_trm t x 0).
      simpl in *. clear n. fsetdec.
    + apply open_lc_trm. trivial.
    + pose proof (@fv_close_self_typ U x 0).
      pose proof (fv_close_typ U x 0).
      repeat split; simp_captured; intros.
      * erewrite <- (@open_left_inv_typ U 0 x) in H; auto.
        econstructor; [eapply ty_lam | eapply st_all; try cofinite; eauto ].
        cofinite.
        apply open_subst_trm with (x := x); simpl; auto.
        clear n. fsetdec.
      * specialize (H6 _ H14).
        specialize (H9 _ H14).
        clear n. fsetdec.
      * fold_close_rec.
        clear n. fsetdec.
      * constructor; trivial.
        apply close_lc_typ; trivial.

  - repeat split; simp_captured; simpl; set solve.
    and_trav_sub G1. apply revealing_soundness in r.
    destruct_conjs.
    assert (G1 ⊢ T <⦂ typ_and (typ_and typ_bot T3) T1) by eauto.
    assert (G1 ⊢ T <⦂ typ_bot) by eauto.
    pose proof (binds_for_sure G2 G1 x T).
    constructor. eapply ty_sub; [eauto |].
    simpl_env. do 2 apply weaken_subty.
    eauto.
      
  - pose proof H. wf_env.
    trav_wf. instan_lc.
    revealing_pf. instan_lc.
    and_trav_sub G1.
    repeat erewrite wf_fv_is_dom in * by trivial.
    prime.
    
    assert (G1 ⊢ T <⦂ typ_and (typ_and (typ_dec $ dec_fld a U) T3) T1) by eauto.
    assert (G1 ⊢ T <⦂ (typ_dec $ dec_fld a U)) by eauto.
    pose proof (binds_for_sure G2 G1 x T).
    repeat split; auto.
    + constructor. eapply ty_sub.
      * instantiate (1 := typ_dec $ dec_fld a U).
        eapply ty_sub; [eauto |].
        simpl_env. auto using weaken_subty.
      * eauto.
    + rewrite dom_app in *. simpl in *.
      fsetdec.

  - pose proof H. wf_env.
    trav_wf. instan_lc.
    revealing_pf. instan_lc.
    and_trav_sub G1.
    repeat erewrite wf_fv_is_dom in * by trivial.
    progressive_inversions.
    apply open_lc_typ with (x := x) in H24. prime.
    
    assert (G1 ⊢ T <⦂ typ_and (typ_and (typ_mu $ dec_fld a U) T3) T1) by eauto.
    assert (G1 ⊢ T <⦂ (typ_mu $ dec_fld a U)) by eauto.
    pose proof (binds_for_sure G2 G1 x T).
    repeat split; auto.
    + constructor. eapply ty_sub.
      * instantiate (1 := open x $ typ_dec $ dec_fld a U).
        apply ty_mu_d_e. eapply ty_sub; [eauto |].
        simpl_env. auto using weaken_subty.
      * simpl. eauto.
    + rewrite dom_app in *. 
      pose proof (fv_open_typ U x 0).
      simpl in *. fsetdec.

  - pose proof H. wf_env.
    trav_wf. instan_lc.
    revealing_pf. instan_lc.
    and_trav_sub G1.
    repeat erewrite wf_fv_is_dom in * by trivial.
    prime.

    repeat split; simp_captured; simpl; set solve.
    apply in_to_binds in i. destruct_conjs.
    assert (G1 ⊢ T <⦂ typ_and (typ_and typ_bot T3) T1) by eauto.
    assert (G1 ⊢ T <⦂ typ_bot) by eauto.
    pose proof (binds_for_sure G2 G1 x T).
    change typ_bot with (open y typ_bot).
    eapply ty_app; [| eauto].
    eapply ty_sub; [eauto |].
    simpl_env. eauto using weaken_subty.

  - pose proof H0. wf_env.
    trav_wf. instan_lc.
    revealing_pf. instan_lc.
    and_trav_sub G1.
    repeat erewrite wf_fv_is_dom in * by trivial.
    progressive_inversions.
    apply open_lc_typ with (x := y) in H26.
    prime. 
    clear H39 H37 H29 H32 H36.
    
    assert (G1 ⊢ T <⦂ typ_and (typ_and (typ_all S U) T3) T1) by eauto.
    assert (G1 ⊢ T <⦂ typ_all S U) by eauto.
    pose proof (binds_for_sure G2 G1 x T).
    repeat split; auto.
    + eapply ty_sub.
      * eapply ty_app.
        -- eapply ty_sub; [eauto |].
           simpl_env. do 2 apply weaken_subty. eassumption.
        -- apply H; auto.
           rewrite dom_app in *. simpl in *.
           fsetdec.
      * trivial.
    + apply binds_In in H38.
      rewrite dom_app in *. 
      pose proof (fv_open_typ U y 0).
      simpl in *. 
      assert (fv_typ U [<=] dom G1) by fsetdec.
      assert (fv_typ (open_rec_typ 0 y U) [<=] dom G2 `union` add x (dom G1)). {
        etransitivity; [eassumption |].
        set solve.
      }
      etransitivity; [eassumption |].
      set solve.

  - progressive_inversions. simpl in *.
    destruct H; auto; destruct_conjs.
    apply open_lc_trm with (x := x) in H8.
    pose proof (fv_open_trm t2 x 0).
    destruct H0; auto; destruct_conjs.
    + constructor; auto.
    + simpl in *. clear n. fsetdec.
    + simp_captured.
      pose proof (H10 x ltac:(set solve)).
      repeat split; simpl; set solve.
      * econstructor; eauto.
        cofinite.
        rewrite <- (@open_lc_le_typ T2 0 0 x0); auto.
        apply open_subst_trm with (x := x); auto.
        rewrite (@open_lc_le_typ T2 0 0 x); auto.
      * clear n. fsetdec.

  - progressive_inversions. simpl in *.
    rec_pose H Hrec; auto.
    apply open_lc_trm with (x := x) in H10.
    pose proof (fv_open_trm t2 x 0).
    destruct H0; auto; destruct_conjs.
    + constructor; auto.
    + simpl in *. fsetdec.
    + simp_captured.
      pose proof (H5 x ltac:(set solve)).
      repeat split; simpl; set solve.
      * econstructor; eauto.
        cofinite.
        rewrite <- (@open_lc_le_typ T2 0 0 x0); auto.
        apply open_subst_trm with (x := x); auto.
        rewrite (@open_lc_le_typ T2 0 0 x); auto.
      * clear n. fsetdec.

  - auto using assginable_sound.
  - pose proof (bi_subty_sound s ltac:(auto)).
    eapply ty_sub; [| eassumption].
    eapply ty_obj; trivial.
    progressive_inversions. simpl in *.
    cofinite. eapply open_subst_defs; auto.
    apply H; simpl; auto.
    constructor; auto.

  - eapply ty_sub; [|apply bi_subty_sound; eauto].
    econstructor. cofinite.
    progressive_inversions; simpl in *.
    pose proof (@fv_close_self_typ U x 0).
    pose proof (fv_close_typ U x 0).
    apply open_subst_trm with (x := x); auto 2.
    + simpl. fsetdec.
    + apply open_lc_trm with (x := x) in H10.
      pose proof (fv_open_trm t x 0).
      destruct H; auto; destruct_conjs.
      * constructor; auto.
      * clear n Fr. simpl in *. fsetdec.
      * rewrite open_left_inv_typ; trivial.

  - eapply ty_sub.
    + econstructor. cofinite.
      progressive_inversions; simpl in *.
      pose proof (fv_open_typ U x 0).
      pose proof (fv_open_trm t x 0).
      apply open_lc_trm with (x := x) in H12.
      apply open_lc_typ with (x := x) in H9.
      apply open_subst_trm with (x := x); auto 2.
      apply H; auto.
      * constructor; auto.
      * clear n Fr. simpl in *. fsetdec.
      * clear n Fr. simpl in *. fsetdec.
    + eapply st_all; [auto using bi_subty_sound |].
      cofinite. auto.
        
  - and_trav_sub G1. apply revealing_soundness in r.
    destruct_conjs.
    assert (G1 ⊢ T <⦂ typ_and (typ_and typ_bot T3) T1) by eauto.
    assert (G1 ⊢ T <⦂ typ_bot) by eauto.
    pose proof (binds_for_sure G2 G1 x T).
    constructor. eapply ty_sub; [eauto |].
    simpl_env. eauto using weaken_subty.
      
  - pose proof H. wf_env.
    trav_wf. instan_lc.
    revealing_pf. instan_lc.
    and_trav_sub G1.
      
    assert (G1 ⊢ T <⦂ typ_and (typ_and (typ_dec $ dec_fld a U) T3) T1) by eauto.
    assert (G1 ⊢ T <⦂ (typ_dec $ dec_fld a U)) by eauto.
    pose proof (binds_for_sure G2 G1 x T).
    constructor. eapply ty_sub.
    + instantiate (1 := typ_dec $ dec_fld a U).
      eapply ty_sub; [eauto |].
      simpl_env. auto using weaken_subty.
    + eauto using bi_subty_sound.

  - pose proof H. wf_env.
    trav_wf. instan_lc.
    revealing_pf. instan_lc.
    and_trav_sub G1.
      
    assert (G1 ⊢ T <⦂ typ_and (typ_and (typ_mu $ dec_fld a U) T3) T1) by eauto.
    assert (G1 ⊢ T <⦂ (typ_mu $ dec_fld a U)) by eauto.
    pose proof (binds_for_sure G2 G1 x T).
    constructor. eapply ty_sub.
    + instantiate (1 := open x $ typ_dec $ dec_fld a U).
      apply ty_mu_d_e. eapply ty_sub; [eauto |].
      simpl_env. auto using weaken_subty.
    + simpl. eauto using bi_subty_sound.

  - pose proof H. wf_env.
    trav_wf. instan_lc.
    revealing_pf. instan_lc.
    and_trav_sub G1.
    repeat erewrite wf_fv_is_dom in * by trivial.

    apply in_to_binds in i. destruct_conjs.
    assert (G1 ⊢ T <⦂ typ_and (typ_and typ_bot T3) T1) by eauto.
    assert (G1 ⊢ T <⦂ typ_bot) by eauto.
    pose proof (binds_for_sure G2 G1 x T).
    eapply ty_sub; [| apply st_bot].
    change typ_bot with (open y typ_bot).
    eapply ty_app; [| eauto].
    eapply ty_sub; [eauto |].
    simpl_env. eauto using weaken_subty.

  - pose proof H0. wf_env.
    trav_wf. instan_lc.
    revealing_pf. instan_lc.
    and_trav_sub G1.
    repeat erewrite wf_fv_is_dom in * by trivial.

    assert (G1 ⊢ T <⦂ typ_and (typ_and (typ_all S U) T3) T1) by eauto.
    assert (G1 ⊢ T <⦂ typ_all S U) by eauto.
    pose proof (binds_for_sure G2 G1 x T).
    simpl in *. progressive_inversions.
    rec_pose H Hrec; auto.
    + rewrite dom_app in *.
      simpl in *. fsetdec.
    + apply bi_subty_sound in s; auto.
      eapply ty_sub; [| eassumption].
      eapply ty_app; eauto.
      eapply ty_sub; [eauto |].
      simpl_env; auto using weaken_subty.

  - progressive_inversions. simpl in *.
    destruct H; auto; destruct_conjs.
    apply open_lc_trm with (x := x) in H10.
    pose proof (fv_open_trm t2 x 0).
    rec_pose H0 Hrec; auto.
    + constructor; auto.
    + simpl in *. clear n. fsetdec.
    + econstructor; eauto.
      cofinite.
      rewrite <- (@open_lc_le_typ T2 0 0 x0); auto.
      apply open_subst_trm with (x := x); auto.
      rewrite (@open_lc_le_typ T2 0 0 x); auto.

  - progressive_inversions. simpl in *.
    rec_pose H Hrec1; auto.
    apply open_lc_trm with (x := x) in H12.
    pose proof (fv_open_trm t2 x 0).
    rec_pose H0 Hrec2; auto.
    + constructor; auto.
    + simpl in *. clear n. fsetdec.
    + econstructor; eauto.
      cofinite.
      rewrite <- (@open_lc_le_typ T2 0 0 x0); auto.
      apply open_subst_trm with (x := x); auto.
      rewrite (@open_lc_le_typ T2 0 0 x); auto.

  - auto.
  - progressive_inversions. auto.

  - progressive_inversions.
    constructor.
    apply open_lc_def with (x := x) in H10.
    apply open_lc_dec with (x := x) in H8.
    pose proof (fv_open_def d x 0).
    pose proof (fv_open_dec D x 0).
    apply H; auto.
    all:simpl in *; fsetdec.

  - progressive_inversions.
    constructor.
    + apply open_lc_def with (x := x) in H13.
      apply open_lc_dec with (x := x) in H9.
      pose proof (fv_open_def d x 0).
      pose proof (fv_open_dec D x 0).
      apply H; auto.
      all:simpl in *; fsetdec.
    + apply H0; trivial.
      all:simpl in *; set solve.
Qed.

Theorem synthesis_soundness : forall G t V T,
    G ⊢⇑ t // V ⦂ T ->
    wf_env G ->
    fv t [<=] dom G -> lc t ->
    (G ⊢ t ⦂ T) /\ ~ captured T V /\ fv T [<=] dom G /\ lc T.
Proof. apply alg_soundness. Qed.

Theorem check_soundness : forall G t T,
    G ⊢⇓ t ⦂ T ->
    wf_env G ->
    fv t [<=] dom G -> lc t ->
    fv T [<=] dom G -> lc T ->
    G ⊢ t ⦂ T.
Proof. apply alg_soundness. Qed.
