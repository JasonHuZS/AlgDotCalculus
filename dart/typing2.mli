
type __ = Obj.t

val negb : bool -> bool

type ('a, 'b) sum =
| Inl of 'a
| Inr of 'b

val fst : ('a1 * 'a2) -> 'a1

val snd : ('a1 * 'a2) -> 'a2

val length : 'a1 list -> int

val app : 'a1 list -> 'a1 list -> 'a1 list

type 'a sig0 = 'a
  (* singleton inductive, whose constructor was exist *)

type ('a, 'p) sigT =
| ExistT of 'a * 'p

val projT1 : ('a1, 'a2) sigT -> 'a1

val projT2 : ('a1, 'a2) sigT -> 'a2



val flip : ('a1 -> 'a2 -> 'a3) -> 'a2 -> 'a1 -> 'a3

module type DecidableType =
 sig
  type t

  val eq_dec : t -> t -> bool
 end

module type DecidableTypeOrig =
 sig
  type t

  val eq_dec : t -> t -> bool
 end

module type UsualDecidableTypeOrig =
 sig
  type t

  val eq_dec : t -> t -> bool
 end

module Nat :
 sig
  val max : int -> int -> int
 end

val in_dec : ('a1 -> 'a1 -> bool) -> 'a1 -> 'a1 list -> bool

val rev : 'a1 list -> 'a1 list

val map : ('a1 -> 'a2) -> 'a1 list -> 'a2 list

val fold_left : ('a1 -> 'a2 -> 'a1) -> 'a2 list -> 'a1 -> 'a1

val fold_right : ('a2 -> 'a1 -> 'a1) -> 'a1 -> 'a2 list -> 'a1

module type Coq_DecidableType =
 DecidableTypeOrig

module type UsualDecidableType =
 UsualDecidableTypeOrig

module WFacts_fun :
 functor (E:Coq_DecidableType) ->
 functor (M:sig
  type elt = E.t

  type t

  val empty : t

  val is_empty : t -> bool

  val mem : elt -> t -> bool

  val add : elt -> t -> t

  val singleton : elt -> t

  val remove : elt -> t -> t

  val union : t -> t -> t

  val inter : t -> t -> t

  val diff : t -> t -> t

  val eq_dec : t -> t -> bool

  val equal : t -> t -> bool

  val subset : t -> t -> bool

  val fold : (elt -> 'a1 -> 'a1) -> t -> 'a1 -> 'a1

  val for_all : (elt -> bool) -> t -> bool

  val exists_ : (elt -> bool) -> t -> bool

  val filter : (elt -> bool) -> t -> t

  val partition : (elt -> bool) -> t -> t * t

  val cardinal : t -> int

  val elements : t -> elt list

  val choose : t -> elt option
 end) ->
 sig
  val eqb : E.t -> E.t -> bool
 end

module WDecide_fun :
 functor (E:Coq_DecidableType) ->
 functor (M:sig
  type elt = E.t

  type t

  val empty : t

  val is_empty : t -> bool

  val mem : elt -> t -> bool

  val add : elt -> t -> t

  val singleton : elt -> t

  val remove : elt -> t -> t

  val union : t -> t -> t

  val inter : t -> t -> t

  val diff : t -> t -> t

  val eq_dec : t -> t -> bool

  val equal : t -> t -> bool

  val subset : t -> t -> bool

  val fold : (elt -> 'a1 -> 'a1) -> t -> 'a1 -> 'a1

  val for_all : (elt -> bool) -> t -> bool

  val exists_ : (elt -> bool) -> t -> bool

  val filter : (elt -> bool) -> t -> t

  val partition : (elt -> bool) -> t -> t * t

  val cardinal : t -> int

  val elements : t -> elt list

  val choose : t -> elt option
 end) ->
 sig
  module F :
   sig
    val eqb : E.t -> E.t -> bool
   end

  module FSetLogicalFacts :
   sig
   end

  module FSetDecideAuxiliary :
   sig
   end

  module FSetDecideTestCases :
   sig
   end
 end

module WProperties_fun :
 functor (E:Coq_DecidableType) ->
 functor (M:sig
  type elt = E.t

  type t

  val empty : t

  val is_empty : t -> bool

  val mem : elt -> t -> bool

  val add : elt -> t -> t

  val singleton : elt -> t

  val remove : elt -> t -> t

  val union : t -> t -> t

  val inter : t -> t -> t

  val diff : t -> t -> t

  val eq_dec : t -> t -> bool

  val equal : t -> t -> bool

  val subset : t -> t -> bool

  val fold : (elt -> 'a1 -> 'a1) -> t -> 'a1 -> 'a1

  val for_all : (elt -> bool) -> t -> bool

  val exists_ : (elt -> bool) -> t -> bool

  val filter : (elt -> bool) -> t -> t

  val partition : (elt -> bool) -> t -> t * t

  val cardinal : t -> int

  val elements : t -> elt list

  val choose : t -> elt option
 end) ->
 sig
  module Dec :
   sig
    module F :
     sig
      val eqb : E.t -> E.t -> bool
     end

    module FSetLogicalFacts :
     sig
     end

    module FSetDecideAuxiliary :
     sig
     end

    module FSetDecideTestCases :
     sig
     end
   end

  module FM :
   sig
    val eqb : E.t -> E.t -> bool
   end

  val coq_In_dec : M.elt -> M.t -> bool

  val of_list : M.elt list -> M.t

  val to_list : M.t -> M.elt list

  val fold_rec :
    (M.elt -> 'a1 -> 'a1) -> 'a1 -> M.t -> (M.t -> __ -> 'a2) -> (M.elt ->
    'a1 -> M.t -> M.t -> __ -> __ -> __ -> 'a2 -> 'a2) -> 'a2

  val fold_rec_bis :
    (M.elt -> 'a1 -> 'a1) -> 'a1 -> M.t -> (M.t -> M.t -> 'a1 -> __ -> 'a2 ->
    'a2) -> 'a2 -> (M.elt -> 'a1 -> M.t -> __ -> __ -> 'a2 -> 'a2) -> 'a2

  val fold_rec_nodep :
    (M.elt -> 'a1 -> 'a1) -> 'a1 -> M.t -> 'a2 -> (M.elt -> 'a1 -> __ -> 'a2
    -> 'a2) -> 'a2

  val fold_rec_weak :
    (M.elt -> 'a1 -> 'a1) -> 'a1 -> (M.t -> M.t -> 'a1 -> __ -> 'a2 -> 'a2)
    -> 'a2 -> (M.elt -> 'a1 -> M.t -> __ -> 'a2 -> 'a2) -> M.t -> 'a2

  val fold_rel :
    (M.elt -> 'a1 -> 'a1) -> (M.elt -> 'a2 -> 'a2) -> 'a1 -> 'a2 -> M.t ->
    'a3 -> (M.elt -> 'a1 -> 'a2 -> __ -> 'a3 -> 'a3) -> 'a3

  val set_induction :
    (M.t -> __ -> 'a1) -> (M.t -> M.t -> 'a1 -> M.elt -> __ -> __ -> 'a1) ->
    M.t -> 'a1

  val set_induction_bis :
    (M.t -> M.t -> __ -> 'a1 -> 'a1) -> 'a1 -> (M.elt -> M.t -> __ -> 'a1 ->
    'a1) -> M.t -> 'a1

  val cardinal_inv_2 : M.t -> int -> M.elt

  val cardinal_inv_2b : M.t -> M.elt
 end

module MakeRaw :
 functor (X:DecidableType) ->
 sig
  type elt = X.t

  type t = elt list

  val empty : t

  val is_empty : t -> bool

  val mem : elt -> t -> bool

  val add : elt -> t -> t

  val singleton : elt -> t

  val remove : elt -> t -> t

  val fold : (elt -> 'a1 -> 'a1) -> t -> 'a1 -> 'a1

  val union : t -> t -> t

  val diff : t -> t -> t

  val inter : t -> t -> t

  val subset : t -> t -> bool

  val equal : t -> t -> bool

  val filter : (elt -> bool) -> t -> t

  val for_all : (elt -> bool) -> t -> bool

  val exists_ : (elt -> bool) -> t -> bool

  val partition : (elt -> bool) -> t -> t * t

  val cardinal : t -> int

  val elements : t -> elt list

  val choose : t -> elt option

  val isok : elt list -> bool
 end

module Make :
 functor (X:DecidableType) ->
 sig
  module Raw :
   sig
    type elt = X.t

    type t = elt list

    val empty : t

    val is_empty : t -> bool

    val mem : elt -> t -> bool

    val add : elt -> t -> t

    val singleton : elt -> t

    val remove : elt -> t -> t

    val fold : (elt -> 'a1 -> 'a1) -> t -> 'a1 -> 'a1

    val union : t -> t -> t

    val diff : t -> t -> t

    val inter : t -> t -> t

    val subset : t -> t -> bool

    val equal : t -> t -> bool

    val filter : (elt -> bool) -> t -> t

    val for_all : (elt -> bool) -> t -> bool

    val exists_ : (elt -> bool) -> t -> bool

    val partition : (elt -> bool) -> t -> t * t

    val cardinal : t -> int

    val elements : t -> elt list

    val choose : t -> elt option

    val isok : elt list -> bool
   end

  module E :
   sig
    type t = X.t

    val eq_dec : t -> t -> bool
   end

  type elt = X.t

  type t_ = Raw.t
    (* singleton inductive, whose constructor was Mkt *)

  val this : t_ -> Raw.t

  type t = t_

  val mem : elt -> t -> bool

  val add : elt -> t -> t

  val remove : elt -> t -> t

  val singleton : elt -> t

  val union : t -> t -> t

  val inter : t -> t -> t

  val diff : t -> t -> t

  val equal : t -> t -> bool

  val subset : t -> t -> bool

  val empty : t

  val is_empty : t -> bool

  val elements : t -> elt list

  val choose : t -> elt option

  val fold : (elt -> 'a1 -> 'a1) -> t -> 'a1 -> 'a1

  val cardinal : t -> int

  val filter : (elt -> bool) -> t -> t

  val for_all : (elt -> bool) -> t -> bool

  val exists_ : (elt -> bool) -> t -> bool

  val partition : (elt -> bool) -> t -> t * t

  val eq_dec : t -> t -> bool
 end

module Coq_WDecide_fun :
 functor (E:Coq_DecidableType) ->
 functor (M:sig
  type elt = E.t

  type t

  val empty : t

  val is_empty : t -> bool

  val mem : elt -> t -> bool

  val add : elt -> t -> t

  val singleton : elt -> t

  val remove : elt -> t -> t

  val union : t -> t -> t

  val inter : t -> t -> t

  val diff : t -> t -> t

  val eq_dec : t -> t -> bool

  val equal : t -> t -> bool

  val subset : t -> t -> bool

  val fold : (elt -> 'a1 -> 'a1) -> t -> 'a1 -> 'a1

  val for_all : (elt -> bool) -> t -> bool

  val exists_ : (elt -> bool) -> t -> bool

  val filter : (elt -> bool) -> t -> t

  val partition : (elt -> bool) -> t -> t * t

  val cardinal : t -> int

  val elements : t -> elt list

  val choose : t -> elt option
 end) ->
 sig
  module F :
   sig
    val eqb : E.t -> E.t -> bool
   end

  module FSetLogicalFacts :
   sig
   end

  module FSetDecideAuxiliary :
   sig
   end

  module FSetDecideTestCases :
   sig
   end
 end

type 'a eqDec = 'a -> 'a -> bool

val nat_eq_eqdec : int eqDec

type 'a eqDec_eq = 'a -> 'a -> bool

val eq_dec0 : 'a1 eqDec_eq -> 'a1 -> 'a1 -> bool

val eqDec_eq_of_EqDec : 'a1 eqDec -> 'a1 eqDec_eq

module Coq_Make :
 functor (X:UsualDecidableType) ->
 functor (KeySet:sig
  type elt = X.t

  type t

  val empty : t

  val is_empty : t -> bool

  val mem : elt -> t -> bool

  val add : elt -> t -> t

  val singleton : elt -> t

  val remove : elt -> t -> t

  val union : t -> t -> t

  val inter : t -> t -> t

  val diff : t -> t -> t

  val eq_dec : t -> t -> bool

  val equal : t -> t -> bool

  val subset : t -> t -> bool

  val fold : (elt -> 'a1 -> 'a1) -> t -> 'a1 -> 'a1

  val for_all : (elt -> bool) -> t -> bool

  val exists_ : (elt -> bool) -> t -> bool

  val filter : (elt -> bool) -> t -> t

  val partition : (elt -> bool) -> t -> t * t

  val cardinal : t -> int

  val elements : t -> elt list

  val choose : t -> elt option
 end) ->
 sig
  val coq_EqDec_of_X : X.t eqDec

  val coq_EqDec_eq_of_X : X.t eqDec_eq

  module D :
   sig
    module F :
     sig
      val eqb : X.t -> X.t -> bool
     end

    module FSetLogicalFacts :
     sig
     end

    module FSetDecideAuxiliary :
     sig
     end

    module FSetDecideTestCases :
     sig
     end
   end

  module KeySetProperties :
   sig
    module Dec :
     sig
      module F :
       sig
        val eqb : X.t -> X.t -> bool
       end

      module FSetLogicalFacts :
       sig
       end

      module FSetDecideAuxiliary :
       sig
       end

      module FSetDecideTestCases :
       sig
       end
     end

    module FM :
     sig
      val eqb : X.t -> X.t -> bool
     end

    val coq_In_dec : KeySet.elt -> KeySet.t -> bool

    val of_list : KeySet.elt list -> KeySet.t

    val to_list : KeySet.t -> KeySet.elt list

    val fold_rec :
      (KeySet.elt -> 'a1 -> 'a1) -> 'a1 -> KeySet.t -> (KeySet.t -> __ ->
      'a2) -> (KeySet.elt -> 'a1 -> KeySet.t -> KeySet.t -> __ -> __ -> __ ->
      'a2 -> 'a2) -> 'a2

    val fold_rec_bis :
      (KeySet.elt -> 'a1 -> 'a1) -> 'a1 -> KeySet.t -> (KeySet.t -> KeySet.t
      -> 'a1 -> __ -> 'a2 -> 'a2) -> 'a2 -> (KeySet.elt -> 'a1 -> KeySet.t ->
      __ -> __ -> 'a2 -> 'a2) -> 'a2

    val fold_rec_nodep :
      (KeySet.elt -> 'a1 -> 'a1) -> 'a1 -> KeySet.t -> 'a2 -> (KeySet.elt ->
      'a1 -> __ -> 'a2 -> 'a2) -> 'a2

    val fold_rec_weak :
      (KeySet.elt -> 'a1 -> 'a1) -> 'a1 -> (KeySet.t -> KeySet.t -> 'a1 -> __
      -> 'a2 -> 'a2) -> 'a2 -> (KeySet.elt -> 'a1 -> KeySet.t -> __ -> 'a2 ->
      'a2) -> KeySet.t -> 'a2

    val fold_rel :
      (KeySet.elt -> 'a1 -> 'a1) -> (KeySet.elt -> 'a2 -> 'a2) -> 'a1 -> 'a2
      -> KeySet.t -> 'a3 -> (KeySet.elt -> 'a1 -> 'a2 -> __ -> 'a3 -> 'a3) ->
      'a3

    val set_induction :
      (KeySet.t -> __ -> 'a1) -> (KeySet.t -> KeySet.t -> 'a1 -> KeySet.elt
      -> __ -> __ -> 'a1) -> KeySet.t -> 'a1

    val set_induction_bis :
      (KeySet.t -> KeySet.t -> __ -> 'a1 -> 'a1) -> 'a1 -> (KeySet.elt ->
      KeySet.t -> __ -> 'a1 -> 'a1) -> KeySet.t -> 'a1

    val cardinal_inv_2 : KeySet.t -> int -> KeySet.elt

    val cardinal_inv_2b : KeySet.t -> KeySet.elt
   end

  module KeySetFacts :
   sig
    val eqb : X.t -> X.t -> bool
   end

  val one : 'a1 -> 'a1 list

  val dom : (X.t * 'a1) list -> KeySet.t

  val get : X.t -> (X.t * 'a1) list -> 'a1 option

  val map : ('a1 -> 'a2) -> (X.t * 'a1) list -> (X.t * 'a2) list

  val alist_ind :
    'a2 -> (X.t -> 'a1 -> (X.t * 'a1) list -> 'a2 -> 'a2) -> (X.t * 'a1) list
    -> 'a2

  val binds_dec :
    X.t -> 'a1 -> (X.t * 'a1) list -> ('a1 -> 'a1 -> bool) -> bool

  val binds_lookup : X.t -> (X.t * 'a1) list -> ('a1, __) sum
 end

module Coq0_Make :
 functor (X:Coq_DecidableType) ->
 sig
  module E :
   sig
    type t = X.t

    val eq_dec : t -> t -> bool
   end

  module X' :
   sig
    type t = X.t

    val eq_dec : t -> t -> bool
   end

  module MSet :
   sig
    module Raw :
     sig
      type elt = X.t

      type t = elt list

      val empty : t

      val is_empty : t -> bool

      val mem : elt -> t -> bool

      val add : elt -> t -> t

      val singleton : elt -> t

      val remove : elt -> t -> t

      val fold : (elt -> 'a1 -> 'a1) -> t -> 'a1 -> 'a1

      val union : t -> t -> t

      val diff : t -> t -> t

      val inter : t -> t -> t

      val subset : t -> t -> bool

      val equal : t -> t -> bool

      val filter : (elt -> bool) -> t -> t

      val for_all : (elt -> bool) -> t -> bool

      val exists_ : (elt -> bool) -> t -> bool

      val partition : (elt -> bool) -> t -> t * t

      val cardinal : t -> int

      val elements : t -> elt list

      val choose : t -> elt option

      val isok : elt list -> bool
     end

    module E :
     sig
      type t = X.t

      val eq_dec : X.t -> X.t -> bool
     end

    type elt = X.t

    type t_ = Raw.t
      (* singleton inductive, whose constructor was Mkt *)

    val this : t_ -> Raw.t

    type t = t_

    val mem : elt -> t -> bool

    val add : elt -> t -> t

    val remove : elt -> t -> t

    val singleton : elt -> t

    val union : t -> t -> t

    val inter : t -> t -> t

    val diff : t -> t -> t

    val equal : t -> t -> bool

    val subset : t -> t -> bool

    val empty : t

    val is_empty : t -> bool

    val elements : t -> elt list

    val choose : t -> elt option

    val fold : (elt -> 'a1 -> 'a1) -> t -> 'a1 -> 'a1

    val cardinal : t -> int

    val filter : (elt -> bool) -> t -> t

    val for_all : (elt -> bool) -> t -> bool

    val exists_ : (elt -> bool) -> t -> bool

    val partition : (elt -> bool) -> t -> t * t

    val eq_dec : t -> t -> bool
   end

  type elt = X.t

  type t = MSet.t

  val empty : t

  val is_empty : t -> bool

  val mem : elt -> t -> bool

  val add : elt -> t -> t

  val singleton : elt -> t

  val remove : elt -> t -> t

  val union : t -> t -> t

  val inter : t -> t -> t

  val diff : t -> t -> t

  val eq_dec : t -> t -> bool

  val equal : t -> t -> bool

  val subset : t -> t -> bool

  val fold : (elt -> 'a1 -> 'a1) -> t -> 'a1 -> 'a1

  val for_all : (elt -> bool) -> t -> bool

  val exists_ : (elt -> bool) -> t -> bool

  val filter : (elt -> bool) -> t -> t

  val partition : (elt -> bool) -> t -> t * t

  val cardinal : t -> int

  val elements : t -> elt list

  val choose : t -> elt option

  module MF :
   sig
    val eqb : X.t -> X.t -> bool
   end
 end

module type ATOM =
 sig
  type atom

  type t = atom

  val eq_dec : atom -> atom -> bool

  val atom_fresh_for_list : t list -> atom

  val fresh : atom list -> atom

  val nat_of : atom -> int
 end

module Atom :
 ATOM

module AtomSetImpl :
 sig
  type elt = Atom.atom

  type t

  val empty : t

  val is_empty : t -> bool

  val mem : Atom.atom -> t -> bool

  val add : Atom.atom -> t -> t

  val singleton : Atom.atom -> t

  val remove : Atom.atom -> t -> t

  val union : t -> t -> t

  val inter : t -> t -> t

  val diff : t -> t -> t

  val eq_dec : t -> t -> bool

  val equal : t -> t -> bool

  val subset : t -> t -> bool

  val fold : (Atom.atom -> 'a1 -> 'a1) -> t -> 'a1 -> 'a1

  val for_all : (Atom.atom -> bool) -> t -> bool

  val exists_ : (Atom.atom -> bool) -> t -> bool

  val filter : (Atom.atom -> bool) -> t -> t

  val partition : (Atom.atom -> bool) -> t -> t * t

  val cardinal : t -> int

  val elements : t -> Atom.atom list

  val choose : t -> Atom.atom option
 end

val atom_fresh : AtomSetImpl.t -> Atom.atom

module EnvImpl :
 sig
  val coq_EqDec_of_X : Atom.atom eqDec

  val coq_EqDec_eq_of_X : Atom.atom eqDec_eq

  module D :
   sig
    module F :
     sig
      val eqb : Atom.atom -> Atom.atom -> bool
     end

    module FSetLogicalFacts :
     sig
     end

    module FSetDecideAuxiliary :
     sig
     end

    module FSetDecideTestCases :
     sig
     end
   end

  module KeySetProperties :
   sig
    module Dec :
     sig
      module F :
       sig
        val eqb : Atom.atom -> Atom.atom -> bool
       end

      module FSetLogicalFacts :
       sig
       end

      module FSetDecideAuxiliary :
       sig
       end

      module FSetDecideTestCases :
       sig
       end
     end

    module FM :
     sig
      val eqb : Atom.atom -> Atom.atom -> bool
     end

    val coq_In_dec : AtomSetImpl.elt -> AtomSetImpl.t -> bool

    val of_list : AtomSetImpl.elt list -> AtomSetImpl.t

    val to_list : AtomSetImpl.t -> AtomSetImpl.elt list

    val fold_rec :
      (AtomSetImpl.elt -> 'a1 -> 'a1) -> 'a1 -> AtomSetImpl.t ->
      (AtomSetImpl.t -> __ -> 'a2) -> (AtomSetImpl.elt -> 'a1 ->
      AtomSetImpl.t -> AtomSetImpl.t -> __ -> __ -> __ -> 'a2 -> 'a2) -> 'a2

    val fold_rec_bis :
      (AtomSetImpl.elt -> 'a1 -> 'a1) -> 'a1 -> AtomSetImpl.t ->
      (AtomSetImpl.t -> AtomSetImpl.t -> 'a1 -> __ -> 'a2 -> 'a2) -> 'a2 ->
      (AtomSetImpl.elt -> 'a1 -> AtomSetImpl.t -> __ -> __ -> 'a2 -> 'a2) ->
      'a2

    val fold_rec_nodep :
      (AtomSetImpl.elt -> 'a1 -> 'a1) -> 'a1 -> AtomSetImpl.t -> 'a2 ->
      (AtomSetImpl.elt -> 'a1 -> __ -> 'a2 -> 'a2) -> 'a2

    val fold_rec_weak :
      (AtomSetImpl.elt -> 'a1 -> 'a1) -> 'a1 -> (AtomSetImpl.t ->
      AtomSetImpl.t -> 'a1 -> __ -> 'a2 -> 'a2) -> 'a2 -> (AtomSetImpl.elt ->
      'a1 -> AtomSetImpl.t -> __ -> 'a2 -> 'a2) -> AtomSetImpl.t -> 'a2

    val fold_rel :
      (AtomSetImpl.elt -> 'a1 -> 'a1) -> (AtomSetImpl.elt -> 'a2 -> 'a2) ->
      'a1 -> 'a2 -> AtomSetImpl.t -> 'a3 -> (AtomSetImpl.elt -> 'a1 -> 'a2 ->
      __ -> 'a3 -> 'a3) -> 'a3

    val set_induction :
      (AtomSetImpl.t -> __ -> 'a1) -> (AtomSetImpl.t -> AtomSetImpl.t -> 'a1
      -> AtomSetImpl.elt -> __ -> __ -> 'a1) -> AtomSetImpl.t -> 'a1

    val set_induction_bis :
      (AtomSetImpl.t -> AtomSetImpl.t -> __ -> 'a1 -> 'a1) -> 'a1 ->
      (AtomSetImpl.elt -> AtomSetImpl.t -> __ -> 'a1 -> 'a1) -> AtomSetImpl.t
      -> 'a1

    val cardinal_inv_2 : AtomSetImpl.t -> int -> AtomSetImpl.elt

    val cardinal_inv_2b : AtomSetImpl.t -> AtomSetImpl.elt
   end

  module KeySetFacts :
   sig
    val eqb : Atom.atom -> Atom.atom -> bool
   end

  val one : 'a1 -> 'a1 list

  val dom : (Atom.atom * 'a1) list -> AtomSetImpl.t

  val get : Atom.atom -> (Atom.atom * 'a1) list -> 'a1 option

  val map : ('a1 -> 'a2) -> (Atom.atom * 'a1) list -> (Atom.atom * 'a2) list

  val alist_ind :
    'a2 -> (Atom.atom -> 'a1 -> (Atom.atom * 'a1) list -> 'a2 -> 'a2) ->
    (Atom.atom * 'a1) list -> 'a2

  val binds_dec :
    Atom.atom -> 'a1 -> (Atom.atom * 'a1) list -> ('a1 -> 'a1 -> bool) -> bool

  val binds_lookup : Atom.atom -> (Atom.atom * 'a1) list -> ('a1, __) sum
 end

type 'a canOpen =
  int -> Atom.atom -> 'a -> 'a
  (* singleton inductive, whose constructor was Build_CanOpen *)

val open_rec : 'a1 canOpen -> int -> Atom.atom -> 'a1 -> 'a1

type 'a canClose =
  int -> Atom.atom -> 'a -> 'a
  (* singleton inductive, whose constructor was Build_CanClose *)

val close_rec : 'a1 canClose -> int -> Atom.atom -> 'a1 -> 'a1

type ('elem, 't) listIsoCond = { to_list' : ('t -> 'elem list);
                                 from_list' : ('elem list -> 't);
                                 append' : ('t -> 't -> 't) }

val append' : ('a1, 'a2) listIsoCond -> 'a2 -> 'a2 -> 'a2

type typ_label = int
  (* singleton inductive, whose constructor was typ_lab *)

type trm_label = int
  (* singleton inductive, whose constructor was trm_lab *)

val eqDecTypLab : typ_label eqDec_eq

val eqDecTrmLab : trm_label eqDec_eq

type label =
| Label_typ of typ_label
| Label_trm of trm_label

module Label :
 sig
  type t = label

  val eq_dec : t -> t -> bool
 end

module LabelSetImpl :
 sig
  module E :
   sig
    type t = label

    val eq_dec : label -> label -> bool
   end

  module X' :
   sig
    type t = label

    val eq_dec : label -> label -> bool
   end

  module MSet :
   sig
    module Raw :
     sig
      type elt = label

      type t = elt list

      val empty : t

      val is_empty : t -> bool

      val mem : elt -> t -> bool

      val add : elt -> t -> t

      val singleton : elt -> t

      val remove : elt -> t -> t

      val fold : (elt -> 'a1 -> 'a1) -> t -> 'a1 -> 'a1

      val union : t -> t -> t

      val diff : t -> t -> t

      val inter : t -> t -> t

      val subset : t -> t -> bool

      val equal : t -> t -> bool

      val filter : (elt -> bool) -> t -> t

      val for_all : (elt -> bool) -> t -> bool

      val exists_ : (elt -> bool) -> t -> bool

      val partition : (elt -> bool) -> t -> t * t

      val cardinal : t -> int

      val elements : t -> elt list

      val choose : t -> elt option

      val isok : elt list -> bool
     end

    module E :
     sig
      type t = label

      val eq_dec : label -> label -> bool
     end

    type elt = label

    type t_ = Raw.t
      (* singleton inductive, whose constructor was Mkt *)

    val this : t_ -> Raw.t

    type t = t_

    val mem : elt -> t -> bool

    val add : elt -> t -> t

    val remove : elt -> t -> t

    val singleton : elt -> t

    val union : t -> t -> t

    val inter : t -> t -> t

    val diff : t -> t -> t

    val equal : t -> t -> bool

    val subset : t -> t -> bool

    val empty : t

    val is_empty : t -> bool

    val elements : t -> elt list

    val choose : t -> elt option

    val fold : (elt -> 'a1 -> 'a1) -> t -> 'a1 -> 'a1

    val cardinal : t -> int

    val filter : (elt -> bool) -> t -> t

    val for_all : (elt -> bool) -> t -> bool

    val exists_ : (elt -> bool) -> t -> bool

    val partition : (elt -> bool) -> t -> t * t

    val eq_dec : t -> t -> bool
   end

  type elt = label

  type t = MSet.t

  val empty : t

  val is_empty : t -> bool

  val mem : elt -> t -> bool

  val add : elt -> t -> t

  val singleton : elt -> t

  val remove : elt -> t -> t

  val union : t -> t -> t

  val inter : t -> t -> t

  val diff : t -> t -> t

  val eq_dec : t -> t -> bool

  val equal : t -> t -> bool

  val subset : t -> t -> bool

  val fold : (elt -> 'a1 -> 'a1) -> t -> 'a1 -> 'a1

  val for_all : (elt -> bool) -> t -> bool

  val exists_ : (elt -> bool) -> t -> bool

  val filter : (elt -> bool) -> t -> t

  val partition : (elt -> bool) -> t -> t * t

  val cardinal : t -> int

  val elements : t -> elt list

  val choose : t -> elt option

  module MF :
   sig
    val eqb : label -> label -> bool
   end
 end

module LabelAssocList :
 sig
  val coq_EqDec_of_X : label eqDec

  val coq_EqDec_eq_of_X : label eqDec_eq

  module D :
   sig
    module F :
     sig
      val eqb : label -> label -> bool
     end

    module FSetLogicalFacts :
     sig
     end

    module FSetDecideAuxiliary :
     sig
     end

    module FSetDecideTestCases :
     sig
     end
   end

  module KeySetProperties :
   sig
    module Dec :
     sig
      module F :
       sig
        val eqb : label -> label -> bool
       end

      module FSetLogicalFacts :
       sig
       end

      module FSetDecideAuxiliary :
       sig
       end

      module FSetDecideTestCases :
       sig
       end
     end

    module FM :
     sig
      val eqb : label -> label -> bool
     end

    val coq_In_dec : LabelSetImpl.elt -> LabelSetImpl.t -> bool

    val of_list : LabelSetImpl.elt list -> LabelSetImpl.t

    val to_list : LabelSetImpl.t -> LabelSetImpl.elt list

    val fold_rec :
      (LabelSetImpl.elt -> 'a1 -> 'a1) -> 'a1 -> LabelSetImpl.t ->
      (LabelSetImpl.t -> __ -> 'a2) -> (LabelSetImpl.elt -> 'a1 ->
      LabelSetImpl.t -> LabelSetImpl.t -> __ -> __ -> __ -> 'a2 -> 'a2) -> 'a2

    val fold_rec_bis :
      (LabelSetImpl.elt -> 'a1 -> 'a1) -> 'a1 -> LabelSetImpl.t ->
      (LabelSetImpl.t -> LabelSetImpl.t -> 'a1 -> __ -> 'a2 -> 'a2) -> 'a2 ->
      (LabelSetImpl.elt -> 'a1 -> LabelSetImpl.t -> __ -> __ -> 'a2 -> 'a2)
      -> 'a2

    val fold_rec_nodep :
      (LabelSetImpl.elt -> 'a1 -> 'a1) -> 'a1 -> LabelSetImpl.t -> 'a2 ->
      (LabelSetImpl.elt -> 'a1 -> __ -> 'a2 -> 'a2) -> 'a2

    val fold_rec_weak :
      (LabelSetImpl.elt -> 'a1 -> 'a1) -> 'a1 -> (LabelSetImpl.t ->
      LabelSetImpl.t -> 'a1 -> __ -> 'a2 -> 'a2) -> 'a2 -> (LabelSetImpl.elt
      -> 'a1 -> LabelSetImpl.t -> __ -> 'a2 -> 'a2) -> LabelSetImpl.t -> 'a2

    val fold_rel :
      (LabelSetImpl.elt -> 'a1 -> 'a1) -> (LabelSetImpl.elt -> 'a2 -> 'a2) ->
      'a1 -> 'a2 -> LabelSetImpl.t -> 'a3 -> (LabelSetImpl.elt -> 'a1 -> 'a2
      -> __ -> 'a3 -> 'a3) -> 'a3

    val set_induction :
      (LabelSetImpl.t -> __ -> 'a1) -> (LabelSetImpl.t -> LabelSetImpl.t ->
      'a1 -> LabelSetImpl.elt -> __ -> __ -> 'a1) -> LabelSetImpl.t -> 'a1

    val set_induction_bis :
      (LabelSetImpl.t -> LabelSetImpl.t -> __ -> 'a1 -> 'a1) -> 'a1 ->
      (LabelSetImpl.elt -> LabelSetImpl.t -> __ -> 'a1 -> 'a1) ->
      LabelSetImpl.t -> 'a1

    val cardinal_inv_2 : LabelSetImpl.t -> int -> LabelSetImpl.elt

    val cardinal_inv_2b : LabelSetImpl.t -> LabelSetImpl.elt
   end

  module KeySetFacts :
   sig
    val eqb : label -> label -> bool
   end

  val one : 'a1 -> 'a1 list

  val dom : (label * 'a1) list -> LabelSetImpl.t

  val get : label -> (label * 'a1) list -> 'a1 option

  val map : ('a1 -> 'a2) -> (label * 'a1) list -> (label * 'a2) list

  val alist_ind :
    'a2 -> (label -> 'a1 -> (label * 'a1) list -> 'a2 -> 'a2) ->
    (label * 'a1) list -> 'a2

  val binds_dec :
    label -> 'a1 -> (label * 'a1) list -> ('a1 -> 'a1 -> bool) -> bool

  val binds_lookup : label -> (label * 'a1) list -> ('a1, __) sum
 end

type 'm monad = { ret : (__ -> __ -> 'm);
                  bind : (__ -> __ -> 'm -> (__ -> 'm) -> 'm) }

val ret : 'a1 monad -> 'a2 -> 'a1

val bind : 'a1 monad -> 'a1 -> ('a2 -> 'a1) -> 'a1

type 'm pMonad = { pret : (__ -> __ -> __ -> 'm);
                   pbind : (__ -> __ -> __ -> 'm -> (__ -> 'm) -> 'm) }

type ('m, 'x) monP = __

val pbind : 'a1 pMonad -> ('a1, 'a3) monP -> 'a1 -> ('a2 -> 'a1) -> 'a1

val pMonad_Monad : 'a1 monad -> 'a1 pMonad

val monad_either : ('a1, __) sum monad

val err_msg :
  (char list list, 'a1) sum -> char list -> (char list list, 'a1) sum

val erret : char list -> (char list list, 'a1) sum

val get_witness :
  Atom.atom -> (Atom.atom * 'a1) list -> 'a1 -> ((Atom.atom * 'a1) list,
  (Atom.atom * 'a1) list) sigT

val get_for_sure : (Atom.atom * 'a1) list -> Atom.atom -> 'a1

type avar =
| Avar_b of int
| Avar_f of Atom.atom

val eqAvar : avar eqDec_eq

type typ =
| Typ_top
| Typ_bot
| Typ_sel of avar * typ_label
| Typ_all of typ * typ
| Typ_rcd of decs
and dec =
| Dec_typ of typ_label * typ * typ
| Dec_trm of trm_label * typ
and decs =
| Decs_nil
| Decs_cons of dec * decs

val lab_dec_of : dec -> label

val decs_to_list' : decs -> (label * dec) list

val decs_from_list' : (label * dec) list -> decs

val decs_append' : decs -> decs -> decs

val decsListCond : (label * dec, decs) listIsoCond

val typ_eqdec : typ -> typ -> bool

val dec_eqdec : dec -> dec -> bool

val decs_eqdec : decs -> decs -> bool

val eqDecTyp : typ eqDec_eq

type trm =
| Trm_var of avar
| Trm_val of val0
| Trm_sel of avar * trm_label
| Trm_app of avar * avar
| Trm_let of trm * trm
and val0 =
| Val_rcd of defs
| Val_lam of typ * trm
and def =
| Def_typ of typ_label * typ
| Def_trm of trm_label * trm
and defs =
| Defs_nil
| Defs_cons of def * defs

val lab_def_of : def -> label

val defs_to_list' : defs -> (label * def) list

val open_rec_avar : int -> Atom.atom -> avar -> avar

val open_rec_typ : int -> Atom.atom -> typ -> typ

val open_rec_dec : int -> Atom.atom -> dec -> dec

val open_rec_decs : int -> Atom.atom -> decs -> decs

val open_rec_trm : int -> Atom.atom -> trm -> trm

val open_rec_val : int -> Atom.atom -> val0 -> val0

val open_rec_def : int -> Atom.atom -> def -> def

val open_rec_defs : int -> Atom.atom -> defs -> defs

val openTyp : typ canOpen

val openTrm : trm canOpen

val close_rec_avar : int -> Atom.atom -> avar -> avar

val close_rec_typ : int -> Atom.atom -> typ -> typ

val close_rec_dec : int -> Atom.atom -> dec -> dec

val close_rec_decs : int -> Atom.atom -> decs -> decs

val closeTyp : typ canClose

val fv_avar : avar -> AtomSetImpl.t

val fv_typ : typ -> AtomSetImpl.t

val fv_dec : dec -> AtomSetImpl.t

val fv_decs : decs -> AtomSetImpl.t

val fv_trm : trm -> AtomSetImpl.t

val fv_val : val0 -> AtomSetImpl.t

val fv_def : def -> AtomSetImpl.t

val fv_defs : defs -> AtomSetImpl.t

val fv_values :
  ('a1 -> AtomSetImpl.t) -> (Atom.atom * 'a1) list -> AtomSetImpl.t

val check_fv_avar : avar -> bool

val check_fv_typ : typ -> bool

val check_fv_dec : dec -> bool

val check_fv_decs : decs -> bool

val check_fv_trm : trm -> bool

val check_fv_val : val0 -> bool

val check_fv_def : def -> bool

val check_fv_defs : defs -> bool

val check_lc_at_avar : int -> avar -> bool

val check_lc_at_typ : int -> typ -> bool

val check_lc_at_dec : int -> dec -> bool

val check_lc_at_decs : int -> decs -> bool

val check_lc_at_trm : int -> trm -> bool

val check_lc_at_val : int -> val0 -> bool

val check_lc_at_def : int -> def -> bool

val check_lc_at_defs : int -> defs -> bool

val defs_luniq_dec : defs -> bool

val decs_luniq_dec : decs -> bool

val uniq_dom_typ_dec : typ -> bool

val uniq_dom_dec_dec : dec -> bool

val uniq_dom_decs_dec : decs -> bool

val uniq_dom_trm_dec : trm -> bool

val uniq_dom_val_dec : val0 -> bool

val uniq_dom_def_dec : def -> bool

val uniq_dom_defs_dec : defs -> bool

val exposure_impl : (Atom.atom * typ) list -> typ -> (char list list, typ) sum

val promo_demo_func_obligation_1 :
  (Atom.atom * typ) list -> Atom.atom -> typ -> ((Atom.atom * typ) list ->
  Atom.atom -> typ -> __ -> __ -> __ -> __ -> (char list list, typ)
  sum * (char list list, typ) sum) -> (char list list, typ) sum * (char list
  list, typ) sum

val promo_demo_func :
  ((Atom.atom * typ) list, (Atom.atom, typ) sigT) sigT -> (char list list,
  typ) sum * (char list list, typ) sum

val promo_demo :
  (Atom.atom * typ) list -> Atom.atom -> typ -> (char list list, typ)
  sum * (char list list, typ) sum

val promotion :
  (Atom.atom * typ) list -> Atom.atom -> typ -> (char list list, typ) sum

type subtyp_dec_sig = __ -> __ -> __ -> __ -> __ -> (char list list, __) sum

type subtyp_checker = (Atom.atom * typ) list -> typ -> typ -> subtyp_dec_sig

val subty_dec :
  (Atom.atom * typ) list -> typ -> typ -> (char list list, __) sum

val typing_impl_template :
  subtyp_checker -> (Atom.atom * typ) list -> trm -> (char list list, typ) sum

val typecheck_template' : subtyp_checker -> trm -> (char list list, typ) sum

val typecheck_template : subtyp_checker -> trm -> (char list list, typ) sum

val typecheck_equiv : trm -> (char list list, typ) sum
