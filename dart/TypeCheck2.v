Set Implicit Arguments.
Require Import Definitions.
Require Import SubstitutionLaws.
Require Import Substitution.
Require Import Narrowing.
Require Import Weakening.
Require Import OperationProperties.
Require Import TightTyping.
Require Import SubEnv.
Require Import Measures.
Require Import Helpers.
Require Import Equivalence.
Require Import TypeCheck.

Require String.

Section EquivRespMeasure.

  Definition equiv_typs D1 D2 := Forall2 typ_equiv D1 D2.
  Arguments equiv_typs D1 D2/.
  
  Definition equiv_env G1 G2 :=
    Forall2 (fun (tup1 tup2 : var * typ) =>
               let (x, T1) := tup1 in
               let (y, T2) := tup2 in
               x = y /\ T1 ≈ T2) G1 G2.
  Arguments equiv_env G1 G2/.
  
  Program Fixpoint typ_equiv_resp_measure_inp
          G1 D1 T1 {measure (join_measure G1 D1 T1)}:
    forall G2 D2 T2,
      T1 ≈ T2 ->
      equiv_env G1 G2 ->
      equiv_typs D1 D2 ->
      wf_search_impl G1 D1 T1 = wf_search_impl G2 D2 T2 := _.
  Next Obligation.
    destruct H.
    - repeat rewrite wf_search_impl'_equation. trivial.
    - repeat rewrite wf_search_impl'_equation. trivial.
    - change (wf_search_impl' (?x, ?y, ?z)) with (wf_search_impl x y z).
      repeat rewrite <- avar_search_equiv.
      destruct a; simpl.
      + destruct H1. trivial.
        destruct n; simpl.
        * f_equal. apply typ_equiv_resp_measure_inp; simpl; trivial.
          lia.
        * repeat rewrite bv_search_equiv with (A := T).
          apply typ_equiv_resp_measure_inp; simpl; trivial.
          pose proof (typ_struct_measure_neq_0 x).
          lia. reflexivity.
      + destruct H0. trivial.
        destruct_conjs. subst a1. simpl.
        destruct_eq.
        * f_equal. apply typ_equiv_resp_measure_inp; simpl; trivial.
          lia.
        * repeat rewrite fv_search_equiv with (A := T).
          apply typ_equiv_resp_measure_inp; simpl; trivial.
          pose proof (typ_struct_measure_neq_0 t0).
          lia. reflexivity.
    - repeat rewrite (wf_search_impl'_equation (_, _, typ_all _ _)).
      f_equal.
      apply typ_equiv_resp_measure_inp; simpl; trivial.
      lia. constructor; trivial.
    - assert (dec_resp : forall D D',
                 dec_struct_measure D <
                 typ_struct_measure ({< DS1 >}) ->
                 D ≈ D' ->
                 dec_wf_search_impl G1 D1 D =
                 dec_wf_search_impl G2 D2 D'). {
        induction on dec_equiv; simpl.
        - simpl in H4.
          repeat rewrite (wf_search_impl'_equation (_, _, (typ_rcd _))).
          erewrite typ_equiv_resp_measure_inp; try eassumption.
          do 2 f_equal.
          apply typ_equiv_resp_measure_inp; simpl; trivial.
          all:simpl; lia.
        - simpl in H4. f_equal.
          repeat rewrite (wf_search_impl'_equation (_, _, (typ_rcd _))).
          apply typ_equiv_resp_measure_inp; simpl; trivial.
          lia.
      }

      assert (decs_resp : forall DS DS',
                 decs_struct_measure DS <
                 typ_struct_measure ({< DS1 >}) ->
                 DS ≈ DS' ->
                 wf_search_impl' (G1, D1, {<DS >}) = wf_search_impl' (G2, D2, {<DS' >})). {
        induction DS; intros; simpl.
        - destruct DS'.
          + repeat rewrite (wf_search_impl'_equation (_, _, typ_rcd _)).
            trivial.
          + exfalso. eapply false_nil_equiv_cons. eassumption.
        - pose proof (dec_struct_measure_neq_0 d).
          pose proof (decs_struct_measure_neq_0 DS).
          pose proof (decs_equiv_drop_one_side H3).
          destruct_conjs. rewrite H10.
          repeat (rewrite wf_search_decons || rewrite wf_search_deapp).
          erewrite dec_resp; try eassumption; [ | simpl in *; lia].
          erewrite IHDS; try eassumption.
          rewrite wf_search_deapp. lia.
          simpl in *. lia.
      }
      apply decs_resp. simpl. lia. trivial.
  Qed.

  Instance equiv_typs_refl : Reflexive equiv_typs.
  Proof.
    intro D. induction D; routine.
    constructor. reflexivity. trivial.
  Qed.

  Instance equiv_env_refl : Reflexive equiv_env.
  Proof.
    intro G. induction G; routine.
    constructor. split; reflexivity. trivial.
  Qed.
  
  Lemma typ_equiv_resp_app_env_gen :
    forall G D x T1 T2 U,
      T1 ≈ T2 ->
      wf_search_impl (x ~ T1 ++ G) D U = wf_search_impl (x ~ T2 ++ G) D U.
  Proof.
    intros.
    apply typ_equiv_resp_measure_inp; try reflexivity.
    constructor. split; trivial; reflexivity.
    reflexivity.
  Qed.

  Lemma typ_equiv_resp_app_env :
    forall G x T1 T2 U,
      T1 ≈ T2 ->
      wf_search_measure (x ~ T1 ++ G) U = wf_search_measure (x ~ T2 ++ G) U.
  Proof.
    intros. apply typ_equiv_resp_app_env_gen. trivial.
  Qed.

End EquivRespMeasure.  

(** This function is mainly the same as `subty_dec`, except that
 * it takes the advantage of the decidability of `typ_equiv` and 
 * relax the case of `typ_all` further more.
 *)
Program Fixpoint subty_equiv_dec G T U {measure (subty_weight G T U)} :
  subtyp_dec_sig G T U := _.
Next Obligation.
  destruct T eqn:?; destruct U eqn:?;
    lazymatch goal with
    | _ : T = typ_bot |- _ => ref; apply subtyp_bot (* trivial case: bot <: T *)
    | _ : U = typ_top |- _ => ref; apply subtyp_top (* trivial case: T <: top *)
    | _ => idtac
    end;
    lazymatch goal with
    | _ : T = typ_top |- _ => st_undec T U (* trivial case: ??? top <: _ *)
    | _ : U = typ_bot |- _ => st_undec T U (* trivial case: ??? _ <: bot *)
    | _ => idtac
    end;
    repeat lazymatch goal with
           | a : avar |- _ => destruct a; lc_bv
           end.
  
  (* remaining 9 cases left. *)

  - destruct (a == a0); [subst |]. destruct (t == t0).
    ref; subst; apply subtyp_refl.
    
    lookup_from_left subty_equiv_dec a0 G.
    lookup_from_left subty_equiv_dec a G.
  - lookup_from_left subty_equiv_dec a G.
  - lookup_from_left subty_equiv_dec a G.
  - lookup_from_right subty_equiv_dec a G.
  - destruct (typ_equiv_dec t1 t3);
      [ subst | exact (erret "subtyper: the input types do not match")].
    pick_fresh z. simpl in *.
    rec_pose (subty_equiv_dec (z ~ t3 ++ G) (open z t2) (open z t4)).
    + repeat (rewrite measure_unfold_all with (x := z); mass_discharge).
      rewrite typ_equiv_resp_app_env with (T1 := t1) (T2 := t3).
      lia. trivial.
    + progressive_inversions.
      constructor; mass_discharge.
    + pose proof (fv_open_typ t2 z 0).
      etransitivity; [eassumption |].
      set solve.
    + invert H1. subst.
      apply open_lc_typ. trivial.
    + pose proof (fv_open_typ t4 z 0).
      etransitivity; [eassumption |].
      set solve.
    + invert H3. subst.
      apply open_lc_typ. trivial.
    + refine (x <- H4 !!> "subtyper: the body types failed" ;; _).
      ref. apply (typ_equiv_resp G) in e.
      eapply subtyp_all; [ apply e |].
      cofinite.
      apply (@open_subst_subtyp z); simpl in *; set solve; auto.
  - st_undec T U.             (* don't know how to compare typ_all and typ_rcd *)
  - lookup_from_right subty_equiv_dec a G.
  - st_undec T U.             (* don't know how to compare typ_rcd and typ_all *)
  - destruct d0.
    + ref. change d with (append' decs_nil d).
      apply subtyp_drop2_gen.
    + pose proof (wf_measure_decs_cons G d0 d1).
      rec_pose (subty_equiv_dec G (typ_rcd d) (typ_rcd d1)); mass_discharge.
      destruct d0.
      * destruct (lget t d) eqn:?;
        [| exact (erret "subtyper: a label is not found")].
        apply lget_to_lbinds in Heqo.
        decs_facts Heqo.
        pose_fv_decs_lkup t3 G. pose_fv_decs_lkup t4 G.

        rec_pose (subty_equiv_dec G t0 t3); mass_discharge; [solve_wf_decs G U |].
        rec_pose (subty_equiv_dec G t4 t1); mass_discharge; [solve_wf_decs G U |].
        refine (x0 <- H12 !!> "subtyper: lower bound comparison failed";;
                   x1 <- H13 !!> "subtyper: upper bound comparison failed";;
                   x <- H5 !!> "subtyper: recursive comparison for declarations failed";;
                   _).
        ref. list_reasoning.
        apply subtyp_merge; trivial.
        eapply subtyp_trans.
        eapply lbinds_subtyp. eassumption.
        progressive_inversions. apply subtyp_typ; trivial.
      * destruct (lget t d) eqn:?;
        [| exact (erret "subtyper: a label is not found")].
        apply lget_to_lbinds in Heqo.
        decs_facts Heqo.
        pose_fv_decs_lkup t2 G.
        rec_pose (subty_equiv_dec G t2 t0); mass_discharge; [ solve_wf_decs G U |].
        refine (x0 <- H10 !!> "subtyper: member type comparison failed";;
                   x <- H5 !!> "subtyper: recursive comparison for declarations failed";;
                   _).
        ref. list_reasoning.
        apply subtyp_merge; trivial.
        eapply subtyp_trans.
        eapply lbinds_subtyp. eassumption.
        progressive_inversions. apply subtyp_fld; trivial.
Defined.

Definition typecheck_equiv (t : trm) : Tc {T : typ | typing_spec nil t T } :=
  typecheck_template subty_dec t.
