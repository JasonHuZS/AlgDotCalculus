## A typing algorithm for Dependent And Record Types (DART)

This iteration is ~~WIP~~ ~~sort of done~~ ACTUALLY DONE.

~~Next I will be working on non-structural equivalence between types and make it work
with decidable type checking.~~ This is implemented in `Equivalence.v`.

### File structure

For performance purposes, the proofs are scattered in multiple files (though it still
takes more than 1 minutes to compile this folder on my fastest machine). Following
serves as an explanation of what the files are.

* `Definitions.v`, the definition of the language and related concepts.
* A number of structural properties are proved in following files. It should be
  straightforward from their names what they prove: `Substitutionlaws.v`,
  `Narrowing.v`, `Weakening.v`.
* Following files contain an enumeration of properties of the operations:
  `Operationproperties.v`, `Substitution.v` and `SubEnv.v`.
* `Helpers.v` contains random helper lemmas.
* `Equivalence.v` contains the definition and properties of relaxed equivalence
  relation between types.
* `Measures.v` contains definitions and properties of some measures.
* `TypeCheck.v` and `TypeCheck2.v` contain the implementations of type checking
  algorithms.
* `Extract.v` contains extraction logic.
* `Safety.v` contains the safety proof of the calculus.

Last but not least, `typing.ml`, `typing2.ml`, `Typing.hs` and `Typing2.hs` contain
the extracted code. Don't blame me on their ugly looking!
