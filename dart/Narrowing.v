Require Import Definitions.

Require Export SubEnv.
Require Import Weakening.

Section NarrowingPrep.

  Local Hint Extern 1 =>
  match goal with
  | [ |- _ ⊢ _ <⦂ _ ] => idtac
  | [ |- _ ⊢ _ ⦂ _ ] => idtac
  end; reassoc 2 with 0.
  
  Local Hint Extern 1 => eapply weaken_trm.
  
  Lemma narrow_var :
    forall G G' x T,
      G' ⪯ G ->
      binds x T G ->
      G' ⊢ trm_var x ⦂ T.
  Proof.
    induction on subenv; routine.
    eapply ty_sub; eauto.
  Qed.
  
End NarrowingPrep.

Section Narrowing.

  Local Notation narrowing ctor :=
    (forall G t T, ctor G t T -> forall G',
          G' ⪯ G ->
          ctor G' t T).
  
  Lemma narrow_rules :
    narrowing ty_trm /\ narrowing ty_def /\ narrowing ty_defs /\
    narrowing subtyp.
  Proof.
    mutual induction; routine;
      try (eapply narrow_var; eassumption);
      typing undec;
      match goal with
      | [ H : _ ⪯ _ |- _ ] => pose proof (subenv_implies_uniq H)
      end;
      eroutine by (try unfold wf_defs in *) at 6.
  Qed.
End Narrowing.

Lemma narrow_trm : forall G x t T U U',
  x `notin` dom G ->
  uniq G ->
  x ~ U ++ G ⊢ t ⦂ T ->
  G ⊢ U' <⦂ U ->
  x ~ U' ++ G ⊢ t ⦂ T.
Proof.
  intros. reassoc 2 with 0.
  eapply narrow_rules. eassumption.
  simpl. apply subenv_last; trivial.
  auto.
Qed.


Lemma narrow_subtyp : forall G x T T' U U',
  x `notin` dom G ->
  uniq G ->
  x ~ U ++ G ⊢ T' <⦂ T ->
  G ⊢ U' <⦂ U ->
  x ~ U' ++ G ⊢ T' <⦂ T.
Proof.
  intros. reassoc 2 with 0.
  eapply narrow_rules. eassumption.
  simpl. apply subenv_last; trivial.
  auto.
Qed.