{-# OPTIONS_GHC -cpp -XMagicHash #-}
{- For Hugs, use the option -F"cpp -P -traditional" -}

module Typing where

import qualified Prelude

#ifdef __GLASGOW_HASKELL__
import qualified GHC.Base
#else
-- HUGS
import qualified IOExts
#endif

#ifdef __GLASGOW_HASKELL__
unsafeCoerce :: a -> b
unsafeCoerce = GHC.Base.unsafeCoerce#
#else
-- HUGS
unsafeCoerce :: a -> b
unsafeCoerce = IOExts.unsafeCoerce
#endif

#ifdef __GLASGOW_HASKELL__
type Any = GHC.Base.Any
#else
-- HUGS
type Any = ()
#endif

__ :: any
__ = Prelude.error "Logical or arity value used"

false_rect :: a1
false_rect =
  Prelude.error "absurd case"

false_rec :: a1
false_rec =
  false_rect

fst :: ((,) a1 a2) -> a1
fst p =
  case p of {
   (,) x _ -> x}

snd :: ((,) a1 a2) -> a2
snd p =
  case p of {
   (,) _ y -> y}

list_rect :: a2 -> (a1 -> (([]) a1) -> a2 -> a2) -> (([]) a1) -> a2
list_rect f f0 l =
  case l of {
   ([]) -> f;
   (:) y l0 -> f0 y l0 (list_rect f f0 l0)}

length :: (([]) a1) -> Prelude.Integer
length l =
  case l of {
   ([]) -> 0;
   (:) _ l' -> Prelude.succ (length l')}

app :: (([]) a1) -> (([]) a1) -> ([]) a1
app l m =
  case l of {
   ([]) -> m;
   (:) a l1 -> (:) a (app l1 m)}

type Sig a = a
  -- singleton inductive, whose constructor was exist
  
data SigT a p =
   ExistT a p

projT1 :: (SigT a1 a2) -> a1
projT1 x =
  case x of {
   ExistT a _ -> a}

projT2 :: (SigT a1 a2) -> a2
projT2 x =
  case x of {
   ExistT _ h -> h}

flip :: (a1 -> a2 -> a3) -> a2 -> a1 -> a3
flip f x y =
  f y x

in_dec :: (a1 -> a1 -> Prelude.Bool) -> a1 -> (([]) a1) -> Prelude.Bool
in_dec h a l =
  list_rect Prelude.False (\a0 _ iHl ->
    let {s = h a0 a} in
    case s of {
     Prelude.True -> Prelude.True;
     Prelude.False -> iHl}) l

rev :: (([]) a1) -> ([]) a1
rev l =
  case l of {
   ([]) -> ([]);
   (:) x l' -> app (rev l') ((:) x ([]))}

map :: (a1 -> a2) -> (([]) a1) -> ([]) a2
map f l =
  case l of {
   ([]) -> ([]);
   (:) a t -> (:) (f a) (map f t)}

fold_left :: (a1 -> a2 -> a1) -> (([]) a2) -> a1 -> a1
fold_left f l a0 =
  case l of {
   ([]) -> a0;
   (:) b t -> fold_left f t (f a0 b)}

fold_right :: (a2 -> a1 -> a1) -> a1 -> (([]) a2) -> a1
fold_right f a0 l =
  case l of {
   ([]) -> a0;
   (:) b t -> f b (fold_right f a0 t)}

type EqDec a = a -> a -> Prelude.Bool

nat_eq_eqdec :: EqDec Prelude.Integer
nat_eq_eqdec =
  (Prelude.==)

type EqDec_eq a = a -> a -> Prelude.Bool

eq_dec :: (EqDec_eq a1) -> a1 -> a1 -> Prelude.Bool
eq_dec eqDec_eq =
  eqDec_eq

eqDec_eq_of_EqDec :: (EqDec a1) -> EqDec_eq a1
eqDec_eq_of_EqDec h =
  h

type Atom = Prelude.Integer

type T = Atom

eq_dec0 :: Prelude.Integer -> Prelude.Integer -> Prelude.Bool
eq_dec0 =
  (Prelude.==)

nat_list_max :: (([]) Prelude.Integer) -> Prelude.Integer
nat_list_max xs =
  list_rect 0 (\x _ h0 -> Prelude.max x h0) xs

atom_fresh_for_list :: (([]) Prelude.Integer) -> Prelude.Integer
atom_fresh_for_list xs =
  let {s = nat_list_max xs} in Prelude.succ s

fresh :: (([]) Atom) -> Prelude.Integer
fresh =
  atom_fresh_for_list

fresh_not_in :: ()
fresh_not_in =
  __

nat_of :: Atom -> Atom
nat_of x =
  x

type Eq = ()

eq_equiv :: ()
eq_equiv =
  __

eq_refl :: ()
eq_refl =
  __

eq_sym :: ()
eq_sym =
  __

eq_trans :: ()
eq_trans =
  __

type T0 = Atom

eq_dec1 :: Atom -> Atom -> Prelude.Bool
eq_dec1 =
  eq_dec0

type T1 = Atom

eq_dec2 :: Atom -> Atom -> Prelude.Bool
eq_dec2 =
  eq_dec0

type Elt = Atom

type T2 = ([]) Elt

empty :: T2
empty =
  ([])

is_empty :: T2 -> Prelude.Bool
is_empty l =
  case l of {
   ([]) -> Prelude.True;
   (:) _ _ -> Prelude.False}

mem :: Elt -> T2 -> Prelude.Bool
mem x s =
  case s of {
   ([]) -> Prelude.False;
   (:) y l ->
    case eq_dec2 x y of {
     Prelude.True -> Prelude.True;
     Prelude.False -> mem x l}}

add :: Elt -> T2 -> T2
add x s =
  case s of {
   ([]) -> (:) x ([]);
   (:) y l ->
    case eq_dec2 x y of {
     Prelude.True -> s;
     Prelude.False -> (:) y (add x l)}}

singleton :: Elt -> T2
singleton x =
  (:) x ([])

remove :: Elt -> T2 -> T2
remove x s =
  case s of {
   ([]) -> ([]);
   (:) y l ->
    case eq_dec2 x y of {
     Prelude.True -> l;
     Prelude.False -> (:) y (remove x l)}}

fold :: (Elt -> a1 -> a1) -> T2 -> a1 -> a1
fold f =
  fold_left (flip f)

union :: T2 -> T2 -> T2
union s =
  fold add s

diff :: T2 -> T2 -> T2
diff s s' =
  fold remove s' s

inter :: T2 -> T2 -> T2
inter s s' =
  fold (\x s0 ->
    case mem x s' of {
     Prelude.True -> add x s0;
     Prelude.False -> s0}) s ([])

subset :: T2 -> T2 -> Prelude.Bool
subset s s' =
  is_empty (diff s s')

equal :: T2 -> T2 -> Prelude.Bool
equal s s' =
  (Prelude.&&) (subset s s') (subset s' s)

filter :: (Elt -> Prelude.Bool) -> T2 -> T2
filter f s =
  case s of {
   ([]) -> ([]);
   (:) x l ->
    case f x of {
     Prelude.True -> (:) x (filter f l);
     Prelude.False -> filter f l}}

for_all :: (Elt -> Prelude.Bool) -> T2 -> Prelude.Bool
for_all f s =
  case s of {
   ([]) -> Prelude.True;
   (:) x l ->
    case f x of {
     Prelude.True -> for_all f l;
     Prelude.False -> Prelude.False}}

exists_ :: (Elt -> Prelude.Bool) -> T2 -> Prelude.Bool
exists_ f s =
  case s of {
   ([]) -> Prelude.False;
   (:) x l ->
    case f x of {
     Prelude.True -> Prelude.True;
     Prelude.False -> exists_ f l}}

partition :: (Elt -> Prelude.Bool) -> T2 -> (,) T2 T2
partition f s =
  case s of {
   ([]) -> (,) ([]) ([]);
   (:) x l ->
    case partition f l of {
     (,) s1 s2 ->
      case f x of {
       Prelude.True -> (,) ((:) x s1) s2;
       Prelude.False -> (,) s1 ((:) x s2)}}}

cardinal :: T2 -> Prelude.Integer
cardinal =
  length

elements :: T2 -> ([]) Elt
elements s =
  s

choose :: T2 -> Prelude.Maybe Elt
choose s =
  case s of {
   ([]) -> Prelude.Nothing;
   (:) x _ -> Prelude.Just x}

isok :: (([]) Elt) -> Prelude.Bool
isok l =
  case l of {
   ([]) -> Prelude.True;
   (:) a l0 -> (Prelude.&&) (Prelude.not (mem a l0)) (isok l0)}

type T3 = Atom

eq_dec3 :: Atom -> Atom -> Prelude.Bool
eq_dec3 =
  eq_dec2

type Elt0 = Atom

type T_ = T2
  -- singleton inductive, whose constructor was Mkt
  
this :: T_ -> T2
this t =
  t

type T4 = T_

mem0 :: Elt0 -> T4 -> Prelude.Bool
mem0 x s =
  mem x (this s)

add0 :: Elt0 -> T4 -> T4
add0 x s =
  add x (this s)

remove0 :: Elt0 -> T4 -> T4
remove0 x s =
  remove x (this s)

singleton0 :: Elt0 -> T4
singleton0 =
  singleton

union0 :: T4 -> T4 -> T4
union0 s s' =
  union (this s) (this s')

inter0 :: T4 -> T4 -> T4
inter0 s s' =
  inter (this s) (this s')

diff0 :: T4 -> T4 -> T4
diff0 s s' =
  diff (this s) (this s')

equal0 :: T4 -> T4 -> Prelude.Bool
equal0 s s' =
  equal (this s) (this s')

subset0 :: T4 -> T4 -> Prelude.Bool
subset0 s s' =
  subset (this s) (this s')

empty0 :: T4
empty0 =
  empty

is_empty0 :: T4 -> Prelude.Bool
is_empty0 s =
  is_empty (this s)

elements0 :: T4 -> ([]) Elt0
elements0 s =
  elements (this s)

choose0 :: T4 -> Prelude.Maybe Elt0
choose0 s =
  choose (this s)

fold0 :: (Elt0 -> a1 -> a1) -> T4 -> a1 -> a1
fold0 f s =
  fold f (this s)

cardinal0 :: T4 -> Prelude.Integer
cardinal0 s =
  cardinal (this s)

filter0 :: (Elt0 -> Prelude.Bool) -> T4 -> T4
filter0 f s =
  filter f (this s)

for_all0 :: (Elt0 -> Prelude.Bool) -> T4 -> Prelude.Bool
for_all0 f s =
  for_all f (this s)

exists_0 :: (Elt0 -> Prelude.Bool) -> T4 -> Prelude.Bool
exists_0 f s =
  exists_ f (this s)

partition0 :: (Elt0 -> Prelude.Bool) -> T4 -> (,) T4 T4
partition0 f s =
  let {p = partition f (this s)} in (,) (fst p) (snd p)

eq_dec4 :: T4 -> T4 -> Prelude.Bool
eq_dec4 s0 s'0 =
  let {b = equal s0 s'0} in
  case b of {
   Prelude.True -> Prelude.True;
   Prelude.False -> Prelude.False}

type Elt1 = Atom

type T5 = T4

empty1 :: T5
empty1 =
  empty0

is_empty1 :: T5 -> Prelude.Bool
is_empty1 =
  is_empty0

mem1 :: Elt1 -> T5 -> Prelude.Bool
mem1 =
  mem0

add1 :: Elt1 -> T5 -> T5
add1 =
  add0

singleton1 :: Elt1 -> T5
singleton1 =
  singleton0

remove1 :: Elt1 -> T5 -> T5
remove1 =
  remove0

union1 :: T5 -> T5 -> T5
union1 =
  union0

inter1 :: T5 -> T5 -> T5
inter1 =
  inter0

diff1 :: T5 -> T5 -> T5
diff1 =
  diff0

eq_dec5 :: T5 -> T5 -> Prelude.Bool
eq_dec5 =
  eq_dec4

equal1 :: T5 -> T5 -> Prelude.Bool
equal1 =
  equal0

subset1 :: T5 -> T5 -> Prelude.Bool
subset1 =
  subset0

fold1 :: (Elt1 -> a1 -> a1) -> T5 -> a1 -> a1
fold1 =
  fold0

for_all1 :: (Elt1 -> Prelude.Bool) -> T5 -> Prelude.Bool
for_all1 =
  for_all0

exists_1 :: (Elt1 -> Prelude.Bool) -> T5 -> Prelude.Bool
exists_1 =
  exists_0

filter1 :: (Elt1 -> Prelude.Bool) -> T5 -> T5
filter1 =
  filter0

partition1 :: (Elt1 -> Prelude.Bool) -> T5 -> (,) T5 T5
partition1 =
  partition0

cardinal1 :: T5 -> Prelude.Integer
cardinal1 =
  cardinal0

elements1 :: T5 -> ([]) Elt1
elements1 =
  elements0

choose1 :: T5 -> Prelude.Maybe Elt1
choose1 =
  choose0

eqb :: Atom -> Atom -> Prelude.Bool
eqb x y =
  case eq_dec3 x y of {
   Prelude.True -> Prelude.True;
   Prelude.False -> Prelude.False}

atom_fresh :: T5 -> Atom
atom_fresh l =
  atom_fresh_for_list (elements1 l)

eqDec_of_X :: EqDec Atom
eqDec_of_X =
  eq_dec0

eqDec_eq_of_X :: EqDec_eq Atom
eqDec_eq_of_X =
  eqDec_eq_of_EqDec eqDec_of_X

eqb0 :: Atom -> Atom -> Prelude.Bool
eqb0 x y =
  case eq_dec0 x y of {
   Prelude.True -> Prelude.True;
   Prelude.False -> Prelude.False}

eqb1 :: Atom -> Atom -> Prelude.Bool
eqb1 x y =
  case eq_dec0 x y of {
   Prelude.True -> Prelude.True;
   Prelude.False -> Prelude.False}

eqb2 :: Atom -> Atom -> Prelude.Bool
eqb2 x y =
  case eq_dec0 x y of {
   Prelude.True -> Prelude.True;
   Prelude.False -> Prelude.False}

in_dec0 :: Elt1 -> T5 -> Prelude.Bool
in_dec0 x s =
  case mem1 x s of {
   Prelude.True -> Prelude.True;
   Prelude.False -> Prelude.False}

of_list :: (([]) Elt1) -> T5
of_list l =
  fold_right add1 empty1 l

to_list :: T5 -> ([]) Elt1
to_list =
  elements1

fold_rec :: (Elt1 -> a1 -> a1) -> a1 -> T5 -> (T5 -> () -> a2) -> (Elt1 -> a1
            -> T5 -> T5 -> () -> () -> () -> a2 -> a2) -> a2
fold_rec f i s pempty pstep =
  let {l = rev (elements1 s)} in
  let {pstep' = \x a s' s'' x0 -> pstep x a s' s'' __ __ __ x0} in
  list_rect (\_ _ s0 _ -> pempty s0 __) (\a l0 iHl pstep'0 _ s0 _ ->
    pstep'0 a (fold_right f i l0) (of_list l0) s0 __ __ __
      (iHl (\x a0 s' s'' _ _ _ x0 -> pstep'0 x a0 s' s'' __ __ __ x0) __
        (of_list l0) __)) l (\x a s' s'' _ _ _ x0 -> pstep' x a s' s'' x0) __
    s __

fold_rec_bis :: (Elt1 -> a1 -> a1) -> a1 -> T5 -> (T5 -> T5 -> a1 -> () -> a2
                -> a2) -> a2 -> (Elt1 -> a1 -> T5 -> () -> () -> a2 -> a2) ->
                a2
fold_rec_bis f i s pmorphism pempty pstep =
  fold_rec f i s (\s' _ -> pmorphism empty1 s' i __ pempty)
    (\x a s' s'' _ _ _ x0 ->
    pmorphism (add1 x s') s'' (f x a) __ (pstep x a s' __ __ x0))

fold_rec_nodep :: (Elt1 -> a1 -> a1) -> a1 -> T5 -> a2 -> (Elt1 -> a1 -> ()
                  -> a2 -> a2) -> a2
fold_rec_nodep f i s x x0 =
  fold_rec_bis f i s (\_ _ _ _ x1 -> x1) x (\x1 a _ _ _ x2 -> x0 x1 a __ x2)

fold_rec_weak :: (Elt1 -> a1 -> a1) -> a1 -> (T5 -> T5 -> a1 -> () -> a2 ->
                 a2) -> a2 -> (Elt1 -> a1 -> T5 -> () -> a2 -> a2) -> T5 ->
                 a2
fold_rec_weak f i x x0 x1 s =
  fold_rec_bis f i s x x0 (\x2 a s' _ _ x3 -> x1 x2 a s' __ x3)

fold_rel :: (Elt1 -> a1 -> a1) -> (Elt1 -> a2 -> a2) -> a1 -> a2 -> T5 -> a3
            -> (Elt1 -> a1 -> a2 -> () -> a3 -> a3) -> a3
fold_rel f g i j s rempty rstep =
  let {l = rev (elements1 s)} in
  let {rstep' = \x a b x0 -> rstep x a b __ x0} in
  list_rect (\_ -> rempty) (\a l0 iHl rstep'0 ->
    rstep'0 a (fold_right f i l0) (fold_right g j l0) __
      (iHl (\x a0 b _ x0 -> rstep'0 x a0 b __ x0))) l (\x a b _ x0 ->
    rstep' x a b x0)

set_induction :: (T5 -> () -> a1) -> (T5 -> T5 -> a1 -> Elt1 -> () -> () ->
                 a1) -> T5 -> a1
set_induction x x0 s =
  fold_rec (\_ _ -> ()) () s x (\x1 _ s' s'' _ _ _ x2 ->
    x0 s' s'' x2 x1 __ __)

set_induction_bis :: (T5 -> T5 -> () -> a1 -> a1) -> a1 -> (Elt1 -> T5 -> ()
                     -> a1 -> a1) -> T5 -> a1
set_induction_bis x x0 x1 s =
  fold_rec_bis (\_ _ -> ()) () s (\s0 s' _ _ x2 -> x s0 s' __ x2) x0
    (\x2 _ s' _ _ x3 -> x1 x2 s' __ x3)

cardinal_inv_2 :: T5 -> Prelude.Integer -> Elt1
cardinal_inv_2 s _ =
  let {l = elements1 s} in case l of {
                            ([]) -> false_rect;
                            (:) e _ -> e}

cardinal_inv_2b :: T5 -> Elt1
cardinal_inv_2b s =
  let {n = cardinal1 s} in
  let {x = \x -> cardinal_inv_2 s x} in
  (\fO fS n -> if n Prelude.== 0 then fO () else fS (n Prelude.- 1))
    (\_ -> false_rect)
    (\n0 -> x n0)
    n

eqb3 :: Atom -> Atom -> Prelude.Bool
eqb3 x y =
  case eq_dec0 x y of {
   Prelude.True -> Prelude.True;
   Prelude.False -> Prelude.False}

one :: a1 -> ([]) a1
one item =
  (:) item ([])

dom :: (([]) ((,) Atom a1)) -> T5
dom e =
  case e of {
   ([]) -> empty1;
   (:) p e' -> case p of {
                (,) x _ -> add1 x (dom e')}}

get :: Atom -> (([]) ((,) Atom a1)) -> Prelude.Maybe a1
get x e =
  case e of {
   ([]) -> Prelude.Nothing;
   (:) p f ->
    case p of {
     (,) y c ->
      case eq_dec eqDec_eq_of_X x y of {
       Prelude.True -> Prelude.Just c;
       Prelude.False -> get x f}}}

map0 :: (a1 -> a2) -> (([]) ((,) Atom a1)) -> ([]) ((,) Atom a2)
map0 f e =
  map (\b -> case b of {
              (,) x a -> (,) x (f a)}) e

alist_ind :: a2 -> (Atom -> a1 -> (([]) ((,) Atom a1)) -> a2 -> a2) -> (([])
             ((,) Atom a1)) -> a2
alist_ind x x0 xs =
  list_rect x (\a0 xs0 iHxs -> case a0 of {
                                (,) x1 a -> x0 x1 a xs0 iHxs}) xs

binds_dec :: Atom -> a1 -> (([]) ((,) Atom a1)) -> (a1 -> a1 -> Prelude.Bool)
             -> Prelude.Bool
binds_dec x a e x0 =
  in_dec (\x1 y ->
    case x1 of {
     (,) x2 x3 ->
      case y of {
       (,) t0 a1 ->
        case eq_dec0 x2 t0 of {
         Prelude.True -> x0 x3 a1;
         Prelude.False -> Prelude.False}}}) ((,) x a) e

binds_lookup :: Atom -> (([]) ((,) Atom a1)) -> Prelude.Either a1 ()
binds_lookup x e =
  alist_ind (Prelude.Right __) (\x1 a1 _ x0 ->
    case x0 of {
     Prelude.Left s -> Prelude.Left s;
     Prelude.Right _ ->
      let {s = eq_dec0 x x1} in
      case s of {
       Prelude.True -> Prelude.Left a1;
       Prelude.False -> Prelude.Right __}}) e

type CanOpen a =
  Prelude.Integer -> Atom -> a -> a
  -- singleton inductive, whose constructor was Build_CanOpen
  
open_rec :: (CanOpen a1) -> Prelude.Integer -> Atom -> a1 -> a1
open_rec canOpen =
  canOpen

type CanClose a =
  Prelude.Integer -> Atom -> a -> a
  -- singleton inductive, whose constructor was Build_CanClose
  
close_rec :: (CanClose a1) -> Prelude.Integer -> Atom -> a1 -> a1
close_rec canClose =
  canClose

data ListIsoCond elem t =
   Build_ListIsoCond (t -> ([]) elem) ((([]) elem) -> t) (t -> t -> t)

append' :: (ListIsoCond a1 a2) -> a2 -> a2 -> a2
append' listIsoCond =
  case listIsoCond of {
   Build_ListIsoCond _ _ append'0 -> append'0}

type Typ_label =
  Prelude.Integer
  -- singleton inductive, whose constructor was typ_lab
  
type Trm_label =
  Prelude.Integer
  -- singleton inductive, whose constructor was trm_lab
  
eqDecTypLab :: EqDec_eq Typ_label
eqDecTypLab =
  (Prelude.==)

eqDecTrmLab :: EqDec_eq Trm_label
eqDecTrmLab =
  (Prelude.==)

data Label =
   Label_typ Typ_label
 | Label_trm Trm_label

type T6 = Label

eq_dec6 :: T6 -> T6 -> Prelude.Bool
eq_dec6 x y =
  case x of {
   Label_typ x0 ->
    case y of {
     Label_typ t1 -> eq_dec eqDecTypLab x0 t1;
     Label_trm _ -> Prelude.False};
   Label_trm x0 ->
    case y of {
     Label_typ _ -> Prelude.False;
     Label_trm t1 -> eq_dec eqDecTrmLab x0 t1}}

type T7 = Label

eq_dec7 :: Label -> Label -> Prelude.Bool
eq_dec7 =
  eq_dec6

type T8 = Label

eq_dec8 :: Label -> Label -> Prelude.Bool
eq_dec8 =
  eq_dec6

type Elt2 = Label

type T9 = ([]) Elt2

empty2 :: T9
empty2 =
  ([])

is_empty2 :: T9 -> Prelude.Bool
is_empty2 l =
  case l of {
   ([]) -> Prelude.True;
   (:) _ _ -> Prelude.False}

mem2 :: Elt2 -> T9 -> Prelude.Bool
mem2 x s =
  case s of {
   ([]) -> Prelude.False;
   (:) y l ->
    case eq_dec8 x y of {
     Prelude.True -> Prelude.True;
     Prelude.False -> mem2 x l}}

add2 :: Elt2 -> T9 -> T9
add2 x s =
  case s of {
   ([]) -> (:) x ([]);
   (:) y l ->
    case eq_dec8 x y of {
     Prelude.True -> s;
     Prelude.False -> (:) y (add2 x l)}}

singleton2 :: Elt2 -> T9
singleton2 x =
  (:) x ([])

remove2 :: Elt2 -> T9 -> T9
remove2 x s =
  case s of {
   ([]) -> ([]);
   (:) y l ->
    case eq_dec8 x y of {
     Prelude.True -> l;
     Prelude.False -> (:) y (remove2 x l)}}

fold2 :: (Elt2 -> a1 -> a1) -> T9 -> a1 -> a1
fold2 f =
  fold_left (flip f)

union2 :: T9 -> T9 -> T9
union2 s =
  fold2 add2 s

diff2 :: T9 -> T9 -> T9
diff2 s s' =
  fold2 remove2 s' s

inter2 :: T9 -> T9 -> T9
inter2 s s' =
  fold2 (\x s0 ->
    case mem2 x s' of {
     Prelude.True -> add2 x s0;
     Prelude.False -> s0}) s ([])

subset2 :: T9 -> T9 -> Prelude.Bool
subset2 s s' =
  is_empty2 (diff2 s s')

equal2 :: T9 -> T9 -> Prelude.Bool
equal2 s s' =
  (Prelude.&&) (subset2 s s') (subset2 s' s)

filter2 :: (Elt2 -> Prelude.Bool) -> T9 -> T9
filter2 f s =
  case s of {
   ([]) -> ([]);
   (:) x l ->
    case f x of {
     Prelude.True -> (:) x (filter2 f l);
     Prelude.False -> filter2 f l}}

for_all2 :: (Elt2 -> Prelude.Bool) -> T9 -> Prelude.Bool
for_all2 f s =
  case s of {
   ([]) -> Prelude.True;
   (:) x l ->
    case f x of {
     Prelude.True -> for_all2 f l;
     Prelude.False -> Prelude.False}}

exists_2 :: (Elt2 -> Prelude.Bool) -> T9 -> Prelude.Bool
exists_2 f s =
  case s of {
   ([]) -> Prelude.False;
   (:) x l ->
    case f x of {
     Prelude.True -> Prelude.True;
     Prelude.False -> exists_2 f l}}

partition2 :: (Elt2 -> Prelude.Bool) -> T9 -> (,) T9 T9
partition2 f s =
  case s of {
   ([]) -> (,) ([]) ([]);
   (:) x l ->
    case partition2 f l of {
     (,) s1 s2 ->
      case f x of {
       Prelude.True -> (,) ((:) x s1) s2;
       Prelude.False -> (,) s1 ((:) x s2)}}}

cardinal2 :: T9 -> Prelude.Integer
cardinal2 =
  length

elements2 :: T9 -> ([]) Elt2
elements2 s =
  s

choose2 :: T9 -> Prelude.Maybe Elt2
choose2 s =
  case s of {
   ([]) -> Prelude.Nothing;
   (:) x _ -> Prelude.Just x}

isok0 :: (([]) Elt2) -> Prelude.Bool
isok0 l =
  case l of {
   ([]) -> Prelude.True;
   (:) a l0 -> (Prelude.&&) (Prelude.not (mem2 a l0)) (isok0 l0)}

type T10 = Label

eq_dec9 :: Label -> Label -> Prelude.Bool
eq_dec9 =
  eq_dec8

type Elt3 = Label

type T_0 = T9
  -- singleton inductive, whose constructor was Mkt
  
this0 :: T_0 -> T9
this0 t =
  t

type T11 = T_0

mem3 :: Elt3 -> T11 -> Prelude.Bool
mem3 x s =
  mem2 x (this0 s)

add3 :: Elt3 -> T11 -> T11
add3 x s =
  add2 x (this0 s)

remove3 :: Elt3 -> T11 -> T11
remove3 x s =
  remove2 x (this0 s)

singleton3 :: Elt3 -> T11
singleton3 =
  singleton2

union3 :: T11 -> T11 -> T11
union3 s s' =
  union2 (this0 s) (this0 s')

inter3 :: T11 -> T11 -> T11
inter3 s s' =
  inter2 (this0 s) (this0 s')

diff3 :: T11 -> T11 -> T11
diff3 s s' =
  diff2 (this0 s) (this0 s')

equal3 :: T11 -> T11 -> Prelude.Bool
equal3 s s' =
  equal2 (this0 s) (this0 s')

subset3 :: T11 -> T11 -> Prelude.Bool
subset3 s s' =
  subset2 (this0 s) (this0 s')

empty3 :: T11
empty3 =
  empty2

is_empty3 :: T11 -> Prelude.Bool
is_empty3 s =
  is_empty2 (this0 s)

elements3 :: T11 -> ([]) Elt3
elements3 s =
  elements2 (this0 s)

choose3 :: T11 -> Prelude.Maybe Elt3
choose3 s =
  choose2 (this0 s)

fold3 :: (Elt3 -> a1 -> a1) -> T11 -> a1 -> a1
fold3 f s =
  fold2 f (this0 s)

cardinal3 :: T11 -> Prelude.Integer
cardinal3 s =
  cardinal2 (this0 s)

filter3 :: (Elt3 -> Prelude.Bool) -> T11 -> T11
filter3 f s =
  filter2 f (this0 s)

for_all3 :: (Elt3 -> Prelude.Bool) -> T11 -> Prelude.Bool
for_all3 f s =
  for_all2 f (this0 s)

exists_3 :: (Elt3 -> Prelude.Bool) -> T11 -> Prelude.Bool
exists_3 f s =
  exists_2 f (this0 s)

partition3 :: (Elt3 -> Prelude.Bool) -> T11 -> (,) T11 T11
partition3 f s =
  let {p = partition2 f (this0 s)} in (,) (fst p) (snd p)

eq_dec10 :: T11 -> T11 -> Prelude.Bool
eq_dec10 s0 s'0 =
  let {b = equal2 s0 s'0} in
  case b of {
   Prelude.True -> Prelude.True;
   Prelude.False -> Prelude.False}

type Elt4 = Label

type T12 = T11

empty4 :: T12
empty4 =
  empty3

is_empty4 :: T12 -> Prelude.Bool
is_empty4 =
  is_empty3

mem4 :: Elt4 -> T12 -> Prelude.Bool
mem4 =
  mem3

add4 :: Elt4 -> T12 -> T12
add4 =
  add3

singleton4 :: Elt4 -> T12
singleton4 =
  singleton3

remove4 :: Elt4 -> T12 -> T12
remove4 =
  remove3

union4 :: T12 -> T12 -> T12
union4 =
  union3

inter4 :: T12 -> T12 -> T12
inter4 =
  inter3

diff4 :: T12 -> T12 -> T12
diff4 =
  diff3

eq_dec11 :: T12 -> T12 -> Prelude.Bool
eq_dec11 =
  eq_dec10

equal4 :: T12 -> T12 -> Prelude.Bool
equal4 =
  equal3

subset4 :: T12 -> T12 -> Prelude.Bool
subset4 =
  subset3

fold4 :: (Elt4 -> a1 -> a1) -> T12 -> a1 -> a1
fold4 =
  fold3

for_all4 :: (Elt4 -> Prelude.Bool) -> T12 -> Prelude.Bool
for_all4 =
  for_all3

exists_4 :: (Elt4 -> Prelude.Bool) -> T12 -> Prelude.Bool
exists_4 =
  exists_3

filter4 :: (Elt4 -> Prelude.Bool) -> T12 -> T12
filter4 =
  filter3

partition4 :: (Elt4 -> Prelude.Bool) -> T12 -> (,) T12 T12
partition4 =
  partition3

cardinal4 :: T12 -> Prelude.Integer
cardinal4 =
  cardinal3

elements4 :: T12 -> ([]) Elt4
elements4 =
  elements3

choose4 :: T12 -> Prelude.Maybe Elt4
choose4 =
  choose3

eqb4 :: Label -> Label -> Prelude.Bool
eqb4 x y =
  case eq_dec9 x y of {
   Prelude.True -> Prelude.True;
   Prelude.False -> Prelude.False}

eqDec_of_X0 :: EqDec Label
eqDec_of_X0 =
  eq_dec6

eqDec_eq_of_X0 :: EqDec_eq Label
eqDec_eq_of_X0 =
  eqDec_eq_of_EqDec eqDec_of_X0

eqb5 :: Label -> Label -> Prelude.Bool
eqb5 x y =
  case eq_dec6 x y of {
   Prelude.True -> Prelude.True;
   Prelude.False -> Prelude.False}

eqb6 :: Label -> Label -> Prelude.Bool
eqb6 x y =
  case eq_dec6 x y of {
   Prelude.True -> Prelude.True;
   Prelude.False -> Prelude.False}

eqb7 :: Label -> Label -> Prelude.Bool
eqb7 x y =
  case eq_dec6 x y of {
   Prelude.True -> Prelude.True;
   Prelude.False -> Prelude.False}

in_dec1 :: Elt4 -> T12 -> Prelude.Bool
in_dec1 x s =
  case mem4 x s of {
   Prelude.True -> Prelude.True;
   Prelude.False -> Prelude.False}

of_list0 :: (([]) Elt4) -> T12
of_list0 l =
  fold_right add4 empty4 l

to_list0 :: T12 -> ([]) Elt4
to_list0 =
  elements4

fold_rec0 :: (Elt4 -> a1 -> a1) -> a1 -> T12 -> (T12 -> () -> a2) -> (Elt4 ->
             a1 -> T12 -> T12 -> () -> () -> () -> a2 -> a2) -> a2
fold_rec0 f i s pempty pstep =
  let {l = rev (elements4 s)} in
  let {pstep' = \x a s' s'' x0 -> pstep x a s' s'' __ __ __ x0} in
  list_rect (\_ _ s0 _ -> pempty s0 __) (\a l0 iHl pstep'0 _ s0 _ ->
    pstep'0 a (fold_right f i l0) (of_list0 l0) s0 __ __ __
      (iHl (\x a0 s' s'' _ _ _ x0 -> pstep'0 x a0 s' s'' __ __ __ x0) __
        (of_list0 l0) __)) l (\x a s' s'' _ _ _ x0 -> pstep' x a s' s'' x0)
    __ s __

fold_rec_bis0 :: (Elt4 -> a1 -> a1) -> a1 -> T12 -> (T12 -> T12 -> a1 -> ()
                 -> a2 -> a2) -> a2 -> (Elt4 -> a1 -> T12 -> () -> () -> a2
                 -> a2) -> a2
fold_rec_bis0 f i s pmorphism pempty pstep =
  fold_rec0 f i s (\s' _ -> pmorphism empty4 s' i __ pempty)
    (\x a s' s'' _ _ _ x0 ->
    pmorphism (add4 x s') s'' (f x a) __ (pstep x a s' __ __ x0))

fold_rec_nodep0 :: (Elt4 -> a1 -> a1) -> a1 -> T12 -> a2 -> (Elt4 -> a1 -> ()
                   -> a2 -> a2) -> a2
fold_rec_nodep0 f i s x x0 =
  fold_rec_bis0 f i s (\_ _ _ _ x1 -> x1) x (\x1 a _ _ _ x2 -> x0 x1 a __ x2)

fold_rec_weak0 :: (Elt4 -> a1 -> a1) -> a1 -> (T12 -> T12 -> a1 -> () -> a2
                  -> a2) -> a2 -> (Elt4 -> a1 -> T12 -> () -> a2 -> a2) ->
                  T12 -> a2
fold_rec_weak0 f i x x0 x1 s =
  fold_rec_bis0 f i s x x0 (\x2 a s' _ _ x3 -> x1 x2 a s' __ x3)

fold_rel0 :: (Elt4 -> a1 -> a1) -> (Elt4 -> a2 -> a2) -> a1 -> a2 -> T12 ->
             a3 -> (Elt4 -> a1 -> a2 -> () -> a3 -> a3) -> a3
fold_rel0 f g i j s rempty rstep =
  let {l = rev (elements4 s)} in
  let {rstep' = \x a b x0 -> rstep x a b __ x0} in
  list_rect (\_ -> rempty) (\a l0 iHl rstep'0 ->
    rstep'0 a (fold_right f i l0) (fold_right g j l0) __
      (iHl (\x a0 b _ x0 -> rstep'0 x a0 b __ x0))) l (\x a b _ x0 ->
    rstep' x a b x0)

set_induction0 :: (T12 -> () -> a1) -> (T12 -> T12 -> a1 -> Elt4 -> () -> ()
                  -> a1) -> T12 -> a1
set_induction0 x x0 s =
  fold_rec0 (\_ _ -> ()) () s x (\x1 _ s' s'' _ _ _ x2 ->
    x0 s' s'' x2 x1 __ __)

set_induction_bis0 :: (T12 -> T12 -> () -> a1 -> a1) -> a1 -> (Elt4 -> T12 ->
                      () -> a1 -> a1) -> T12 -> a1
set_induction_bis0 x x0 x1 s =
  fold_rec_bis0 (\_ _ -> ()) () s (\s0 s' _ _ x2 -> x s0 s' __ x2) x0
    (\x2 _ s' _ _ x3 -> x1 x2 s' __ x3)

cardinal_inv_0 :: T12 -> Prelude.Integer -> Elt4
cardinal_inv_0 s _ =
  let {l = elements4 s} in case l of {
                            ([]) -> false_rect;
                            (:) e _ -> e}

cardinal_inv_2b0 :: T12 -> Elt4
cardinal_inv_2b0 s =
  let {n = cardinal4 s} in
  let {x = \x -> cardinal_inv_0 s x} in
  (\fO fS n -> if n Prelude.== 0 then fO () else fS (n Prelude.- 1))
    (\_ -> false_rect)
    (\n0 -> x n0)
    n

eqb8 :: Label -> Label -> Prelude.Bool
eqb8 x y =
  case eq_dec6 x y of {
   Prelude.True -> Prelude.True;
   Prelude.False -> Prelude.False}

one0 :: a1 -> ([]) a1
one0 item =
  (:) item ([])

dom0 :: (([]) ((,) Label a1)) -> T12
dom0 e =
  case e of {
   ([]) -> empty4;
   (:) p e' -> case p of {
                (,) x _ -> add4 x (dom0 e')}}

get0 :: Label -> (([]) ((,) Label a1)) -> Prelude.Maybe a1
get0 x e =
  case e of {
   ([]) -> Prelude.Nothing;
   (:) p f ->
    case p of {
     (,) y c ->
      case eq_dec eqDec_eq_of_X0 x y of {
       Prelude.True -> Prelude.Just c;
       Prelude.False -> get0 x f}}}

map1 :: (a1 -> a2) -> (([]) ((,) Label a1)) -> ([]) ((,) Label a2)
map1 f e =
  map (\b -> case b of {
              (,) x a -> (,) x (f a)}) e

alist_ind0 :: a2 -> (Label -> a1 -> (([]) ((,) Label a1)) -> a2 -> a2) ->
              (([]) ((,) Label a1)) -> a2
alist_ind0 x x0 xs =
  list_rect x (\a0 xs0 iHxs -> case a0 of {
                                (,) x1 a -> x0 x1 a xs0 iHxs}) xs

binds_dec0 :: Label -> a1 -> (([]) ((,) Label a1)) -> (a1 -> a1 ->
              Prelude.Bool) -> Prelude.Bool
binds_dec0 x a e x0 =
  in_dec (\x1 y ->
    case x1 of {
     (,) x2 x3 ->
      case y of {
       (,) t0 a1 ->
        case eq_dec6 x2 t0 of {
         Prelude.True -> x0 x3 a1;
         Prelude.False -> Prelude.False}}}) ((,) x a) e

binds_lookup0 :: Label -> (([]) ((,) Label a1)) -> Prelude.Either a1 ()
binds_lookup0 x e =
  alist_ind0 (Prelude.Right __) (\x1 a1 _ x0 ->
    case x0 of {
     Prelude.Left s -> Prelude.Left s;
     Prelude.Right _ ->
      let {s = eq_dec6 x x1} in
      case s of {
       Prelude.True -> Prelude.Left a1;
       Prelude.False -> Prelude.Right __}}) e

data Monad m =
   Build_Monad (() -> Any -> m) (() -> () -> m -> (Any -> m) -> m)

ret :: (Monad a1) -> a2 -> a1
ret monad x =
  case monad of {
   Build_Monad ret0 _ -> unsafeCoerce ret0 __ x}

bind :: (Monad a1) -> a1 -> (a2 -> a1) -> a1
bind monad x x0 =
  case monad of {
   Build_Monad _ bind0 -> unsafeCoerce bind0 __ __ x x0}

data PMonad m =
   Build_PMonad (() -> Any -> Any -> m) (() -> () -> Any -> m -> (Any -> m)
                                        -> m)

type MonP m x = Any

pbind :: (PMonad a1) -> (MonP a1 a3) -> a1 -> (a2 -> a1) -> a1
pbind pMonad pu x x0 =
  case pMonad of {
   Build_PMonad _ pbind0 -> unsafeCoerce pbind0 __ __ pu x x0}

pMonad_Monad :: (Monad a1) -> PMonad a1
pMonad_Monad m =
  Build_PMonad (\_ -> unsafeCoerce (\_ x -> ret m x)) (\_ _ ->
    unsafeCoerce (\_ c f -> bind m c f))

monad_either :: Monad (Prelude.Either a1 Any)
monad_either =
  Build_Monad (\_ v -> Prelude.Right v) (\_ _ c1 c2 ->
    case c1 of {
     Prelude.Left v -> Prelude.Left v;
     Prelude.Right v -> c2 v})

err_msg :: (Prelude.Either (([]) Prelude.String) a1) -> Prelude.String ->
           Prelude.Either (([]) Prelude.String) a1
err_msg m msg =
  case m of {
   Prelude.Left msgs -> Prelude.Left ((:) msg msgs);
   Prelude.Right x -> Prelude.Right x}

erret :: Prelude.String -> Prelude.Either (([]) Prelude.String) a1
erret m =
  Prelude.Left ((:) m ([]))

get_witness :: Atom -> (([]) ((,) Atom a1)) -> a1 -> SigT
               (([]) ((,) Atom a1)) (([]) ((,) Atom a1))
get_witness v l r =
  list_rect (\_ _ -> false_rect) (\a l0 iHl r0 _ ->
    case a of {
     (,) a0 a1 ->
      let {s = eq_dec eqDec_eq_of_X v a0} in
      case s of {
       Prelude.True -> ExistT ([]) l0;
       Prelude.False ->
        let {h = iHl r0 __} in
        case h of {
         ExistT h0 x -> ExistT (app (one0 ((,) a0 a1)) h0) x}}}) l r __

get_for_sure :: (([]) ((,) Atom a1)) -> Atom -> a1
get_for_sure l x =
  list_rect (\_ _ -> false_rect) (\a _ iHl x0 _ ->
    case a of {
     (,) a0 t0 ->
      let {s = eq_dec eqDec_eq_of_X x0 a0} in
      case s of {
       Prelude.True -> t0;
       Prelude.False -> iHl x0 __}}) l x __

data Avar =
   Avar_b Prelude.Integer
 | Avar_f Atom

eqAvar :: EqDec_eq Avar
eqAvar x y =
  case x of {
   Avar_b x0 ->
    case y of {
     Avar_b n0 -> (Prelude.==) x0 n0;
     Avar_f _ -> Prelude.False};
   Avar_f x0 ->
    case y of {
     Avar_b _ -> Prelude.False;
     Avar_f a0 -> eq_dec0 x0 a0}}

data Typ =
   Typ_top
 | Typ_bot
 | Typ_sel Avar Typ_label
 | Typ_all Typ Typ
 | Typ_rcd Decs
data Dec =
   Dec_typ Typ_label Typ Typ
 | Dec_trm Trm_label Typ
data Decs =
   Decs_nil
 | Decs_cons Dec Decs

lab_dec_of :: Dec -> Label
lab_dec_of d =
  case d of {
   Dec_typ a _ _ -> Label_typ a;
   Dec_trm a _ -> Label_trm a}

decs_to_list' :: Decs -> ([]) ((,) Label Dec)
decs_to_list' dS =
  case dS of {
   Decs_nil -> ([]);
   Decs_cons d dS' -> (:) ((,) (lab_dec_of d) d) (decs_to_list' dS')}

decs_from_list' :: (([]) ((,) Label Dec)) -> Decs
decs_from_list' l =
  case l of {
   ([]) -> Decs_nil;
   (:) p l' -> case p of {
                (,) _ d -> Decs_cons d (decs_from_list' l')}}

decs_append' :: Decs -> Decs -> Decs
decs_append' dS1 dS2 =
  case dS1 of {
   Decs_nil -> dS2;
   Decs_cons d dS1' -> Decs_cons d (decs_append' dS1' dS2)}

decsListCond :: ListIsoCond ((,) Label Dec) Decs
decsListCond =
  Build_ListIsoCond decs_to_list' decs_from_list' decs_append'

typ_eqdec :: Typ -> Typ -> Prelude.Bool
typ_eqdec t1 t2 =
  case t1 of {
   Typ_top -> case t2 of {
               Typ_top -> Prelude.True;
               _ -> Prelude.False};
   Typ_bot -> case t2 of {
               Typ_bot -> Prelude.True;
               _ -> Prelude.False};
   Typ_sel a t ->
    case t2 of {
     Typ_sel a0 t0 ->
      case eq_dec eqAvar a a0 of {
       Prelude.True -> eq_dec eqDecTypLab t t0;
       Prelude.False -> Prelude.False};
     _ -> Prelude.False};
   Typ_all t t0 ->
    case t2 of {
     Typ_all t3 t4 ->
      case typ_eqdec t t3 of {
       Prelude.True -> typ_eqdec t0 t4;
       Prelude.False -> Prelude.False};
     _ -> Prelude.False};
   Typ_rcd d ->
    case t2 of {
     Typ_rcd d0 -> decs_eqdec d d0;
     _ -> Prelude.False}}

dec_eqdec :: Dec -> Dec -> Prelude.Bool
dec_eqdec d1 d2 =
  case d1 of {
   Dec_typ x x0 x1 ->
    case d2 of {
     Dec_typ t2 t3 t4 ->
      case eq_dec eqDecTypLab x t2 of {
       Prelude.True ->
        case typ_eqdec x0 t3 of {
         Prelude.True -> typ_eqdec x1 t4;
         Prelude.False -> Prelude.False};
       Prelude.False -> Prelude.False};
     Dec_trm _ _ -> Prelude.False};
   Dec_trm x x0 ->
    case d2 of {
     Dec_typ _ _ _ -> Prelude.False;
     Dec_trm t1 t2 ->
      case eq_dec eqDecTrmLab x t1 of {
       Prelude.True -> typ_eqdec x0 t2;
       Prelude.False -> Prelude.False}}}

decs_eqdec :: Decs -> Decs -> Prelude.Bool
decs_eqdec dS1 dS2 =
  case dS1 of {
   Decs_nil ->
    case dS2 of {
     Decs_nil -> Prelude.True;
     Decs_cons _ _ -> Prelude.False};
   Decs_cons d d0 ->
    case dS2 of {
     Decs_nil -> Prelude.False;
     Decs_cons d1 d2 ->
      case dec_eqdec d d1 of {
       Prelude.True -> decs_eqdec d0 d2;
       Prelude.False -> Prelude.False}}}

eqDecTyp :: EqDec_eq Typ
eqDecTyp =
  typ_eqdec

data Trm =
   Trm_var Avar
 | Trm_val Val
 | Trm_sel Avar Trm_label
 | Trm_app Avar Avar
 | Trm_let Trm Trm
data Val =
   Val_rcd Defs
 | Val_lam Typ Trm
data Def =
   Def_typ Typ_label Typ
 | Def_trm Trm_label Trm
data Defs =
   Defs_nil
 | Defs_cons Def Defs

trm_rect :: (Avar -> a1) -> (Val -> a1) -> (Avar -> Trm_label -> a1) -> (Avar
            -> Avar -> a1) -> (Trm -> a1 -> Trm -> a1 -> a1) -> Trm -> a1
trm_rect f f0 f1 f2 f3 t =
  case t of {
   Trm_var a -> f a;
   Trm_val v -> f0 v;
   Trm_sel a t0 -> f1 a t0;
   Trm_app a a0 -> f2 a a0;
   Trm_let t0 t1 ->
    f3 t0 (trm_rect f f0 f1 f2 f3 t0) t1 (trm_rect f f0 f1 f2 f3 t1)}

trm_rec :: (Avar -> a1) -> (Val -> a1) -> (Avar -> Trm_label -> a1) -> (Avar
           -> Avar -> a1) -> (Trm -> a1 -> Trm -> a1 -> a1) -> Trm -> a1
trm_rec =
  trm_rect

val_rect :: (Defs -> a1) -> (Typ -> Trm -> a1) -> Val -> a1
val_rect f f0 v =
  case v of {
   Val_rcd x -> f x;
   Val_lam x x0 -> f0 x x0}

val_rec :: (Defs -> a1) -> (Typ -> Trm -> a1) -> Val -> a1
val_rec =
  val_rect

def_rec :: (Typ_label -> Typ -> a1) -> (Trm_label -> Trm -> a1) -> Def -> a1
def_rec f f0 d =
  case d of {
   Def_typ x x0 -> f x x0;
   Def_trm x x0 -> f0 x x0}

lab_def_of :: Def -> Label
lab_def_of d =
  case d of {
   Def_typ a _ -> Label_typ a;
   Def_trm a _ -> Label_trm a}

defs_to_list' :: Defs -> ([]) ((,) Label Def)
defs_to_list' dS =
  case dS of {
   Defs_nil -> ([]);
   Defs_cons d dS' -> (:) ((,) (lab_def_of d) d) (defs_to_list' dS')}

open_rec_avar :: Prelude.Integer -> Atom -> Avar -> Avar
open_rec_avar k u a =
  case a of {
   Avar_b i ->
    case eq_dec (eqDec_eq_of_EqDec nat_eq_eqdec) k i of {
     Prelude.True -> Avar_f u;
     Prelude.False -> Avar_b i};
   Avar_f x -> Avar_f x}

open_rec_typ :: Prelude.Integer -> Atom -> Typ -> Typ
open_rec_typ k u t =
  case t of {
   Typ_sel x t0 -> Typ_sel (open_rec_avar k u x) t0;
   Typ_all t0 u0 -> Typ_all (open_rec_typ k u t0)
    (open_rec_typ (Prelude.succ k) u u0);
   Typ_rcd dS -> Typ_rcd (open_rec_decs k u dS);
   x -> x}

open_rec_dec :: Prelude.Integer -> Atom -> Dec -> Dec
open_rec_dec k u d =
  case d of {
   Dec_typ x t u0 -> Dec_typ x (open_rec_typ k u t) (open_rec_typ k u u0);
   Dec_trm x t -> Dec_trm x (open_rec_typ k u t)}

open_rec_decs :: Prelude.Integer -> Atom -> Decs -> Decs
open_rec_decs k u dS =
  case dS of {
   Decs_nil -> Decs_nil;
   Decs_cons d dS' -> Decs_cons (open_rec_dec k u d) (open_rec_decs k u dS')}

open_rec_trm :: Prelude.Integer -> Atom -> Trm -> Trm
open_rec_trm k u t =
  case t of {
   Trm_var a -> Trm_var (open_rec_avar k u a);
   Trm_val v -> Trm_val (open_rec_val k u v);
   Trm_sel v m -> Trm_sel (open_rec_avar k u v) m;
   Trm_app f x -> Trm_app (open_rec_avar k u f) (open_rec_avar k u x);
   Trm_let t1 t2 -> Trm_let (open_rec_trm k u t1)
    (open_rec_trm (Prelude.succ k) u t2)}

open_rec_val :: Prelude.Integer -> Atom -> Val -> Val
open_rec_val k u v =
  case v of {
   Val_rcd ds -> Val_rcd (open_rec_defs k u ds);
   Val_lam t e -> Val_lam (open_rec_typ k u t)
    (open_rec_trm (Prelude.succ k) u e)}

open_rec_def :: Prelude.Integer -> Atom -> Def -> Def
open_rec_def k u d =
  case d of {
   Def_typ x t -> Def_typ x (open_rec_typ k u t);
   Def_trm x e -> Def_trm x (open_rec_trm k u e)}

open_rec_defs :: Prelude.Integer -> Atom -> Defs -> Defs
open_rec_defs k u ds =
  case ds of {
   Defs_nil -> Defs_nil;
   Defs_cons df ds' -> Defs_cons (open_rec_def k u df)
    (open_rec_defs k u ds')}

openTyp :: CanOpen Typ
openTyp =
  open_rec_typ

openTrm :: CanOpen Trm
openTrm =
  open_rec_trm

close_rec_avar :: Prelude.Integer -> Atom -> Avar -> Avar
close_rec_avar k u a =
  case a of {
   Avar_b i -> Avar_b i;
   Avar_f x ->
    case eq_dec eqDec_eq_of_X x u of {
     Prelude.True -> Avar_b k;
     Prelude.False -> Avar_f x}}

close_rec_typ :: Prelude.Integer -> Atom -> Typ -> Typ
close_rec_typ k u t =
  case t of {
   Typ_sel x t0 -> Typ_sel (close_rec_avar k u x) t0;
   Typ_all t0 u0 -> Typ_all (close_rec_typ k u t0)
    (close_rec_typ (Prelude.succ k) u u0);
   Typ_rcd dS -> Typ_rcd (close_rec_decs k u dS);
   x -> x}

close_rec_dec :: Prelude.Integer -> Atom -> Dec -> Dec
close_rec_dec k u d =
  case d of {
   Dec_typ x t u0 -> Dec_typ x (close_rec_typ k u t) (close_rec_typ k u u0);
   Dec_trm x t -> Dec_trm x (close_rec_typ k u t)}

close_rec_decs :: Prelude.Integer -> Atom -> Decs -> Decs
close_rec_decs k u dS =
  case dS of {
   Decs_nil -> Decs_nil;
   Decs_cons d dS' -> Decs_cons (close_rec_dec k u d)
    (close_rec_decs k u dS')}

closeTyp :: CanClose Typ
closeTyp =
  close_rec_typ

fv_avar :: Avar -> T5
fv_avar a =
  case a of {
   Avar_b _ -> empty1;
   Avar_f x -> singleton1 x}

fv_typ :: Typ -> T5
fv_typ t =
  case t of {
   Typ_sel x _ -> fv_avar x;
   Typ_all t0 u -> union1 (fv_typ t0) (fv_typ u);
   Typ_rcd dS -> fv_decs dS;
   _ -> empty1}

fv_dec :: Dec -> T5
fv_dec d =
  case d of {
   Dec_typ _ t u -> union1 (fv_typ t) (fv_typ u);
   Dec_trm _ t -> fv_typ t}

fv_decs :: Decs -> T5
fv_decs dS =
  case dS of {
   Decs_nil -> empty1;
   Decs_cons d dS' -> union1 (fv_dec d) (fv_decs dS')}

fv_trm :: Trm -> T5
fv_trm t =
  case t of {
   Trm_var a -> fv_avar a;
   Trm_val v -> fv_val v;
   Trm_sel v _ -> fv_avar v;
   Trm_app f x -> union1 (fv_avar f) (fv_avar x);
   Trm_let t1 t2 -> union1 (fv_trm t1) (fv_trm t2)}

fv_val :: Val -> T5
fv_val v =
  case v of {
   Val_rcd ds -> fv_defs ds;
   Val_lam t e -> union1 (fv_typ t) (fv_trm e)}

fv_def :: Def -> T5
fv_def d =
  case d of {
   Def_typ _ t -> fv_typ t;
   Def_trm _ e -> fv_trm e}

fv_defs :: Defs -> T5
fv_defs ds =
  case ds of {
   Defs_nil -> empty1;
   Defs_cons d ds' -> union1 (fv_def d) (fv_defs ds')}

fv_values :: (a1 -> T5) -> (([]) ((,) Atom a1)) -> T5
fv_values f l =
  fold_right (\b a -> union1 a (case b of {
                                 (,) _ t -> f t})) empty1 l

check_fv_avar :: Avar -> Prelude.Bool
check_fv_avar x =
  case x of {
   Avar_b _ -> Prelude.True;
   Avar_f _ -> Prelude.False}

check_fv_typ :: Typ -> Prelude.Bool
check_fv_typ t =
  case t of {
   Typ_sel a _ -> check_fv_avar a;
   Typ_all t1 t2 ->
    let {s = check_fv_typ t1} in
    case s of {
     Prelude.True -> check_fv_typ t2;
     Prelude.False -> Prelude.False};
   Typ_rcd d -> check_fv_decs d;
   _ -> Prelude.True}

check_fv_dec :: Dec -> Prelude.Bool
check_fv_dec d =
  case d of {
   Dec_typ _ t0 t1 ->
    let {s = check_fv_typ t0} in
    case s of {
     Prelude.True -> check_fv_typ t1;
     Prelude.False -> Prelude.False};
   Dec_trm _ t0 -> check_fv_typ t0}

check_fv_decs :: Decs -> Prelude.Bool
check_fv_decs dS =
  case dS of {
   Decs_nil -> Prelude.True;
   Decs_cons d dS0 ->
    let {s = check_fv_dec d} in
    case s of {
     Prelude.True -> check_fv_decs dS0;
     Prelude.False -> Prelude.False}}

check_fv_trm :: Trm -> Prelude.Bool
check_fv_trm t =
  case t of {
   Trm_var a -> check_fv_avar a;
   Trm_val v -> check_fv_val v;
   Trm_sel a _ -> check_fv_avar a;
   Trm_app a a0 ->
    let {s = check_fv_avar a} in
    case s of {
     Prelude.True -> check_fv_avar a0;
     Prelude.False -> Prelude.False};
   Trm_let t0_1 t0_2 ->
    let {s = check_fv_trm t0_1} in
    case s of {
     Prelude.True -> check_fv_trm t0_2;
     Prelude.False -> Prelude.False}}

check_fv_val :: Val -> Prelude.Bool
check_fv_val vl =
  case vl of {
   Val_rcd d -> check_fv_defs d;
   Val_lam t t0 ->
    let {s = check_fv_typ t} in
    case s of {
     Prelude.True -> check_fv_trm t0;
     Prelude.False -> Prelude.False}}

check_fv_def :: Def -> Prelude.Bool
check_fv_def d =
  case d of {
   Def_typ _ t0 -> check_fv_typ t0;
   Def_trm _ t0 -> check_fv_trm t0}

check_fv_defs :: Defs -> Prelude.Bool
check_fv_defs ds =
  case ds of {
   Defs_nil -> Prelude.True;
   Defs_cons d ds0 ->
    let {s = check_fv_def d} in
    case s of {
     Prelude.True -> check_fv_defs ds0;
     Prelude.False -> Prelude.False}}

check_lc_at_avar :: Prelude.Integer -> Avar -> Prelude.Bool
check_lc_at_avar n x =
  case x of {
   Avar_b n0 -> (Prelude.<=) (Prelude.succ n0) n;
   Avar_f _ -> Prelude.True}

check_lc_at_typ :: Prelude.Integer -> Typ -> Prelude.Bool
check_lc_at_typ n t =
  case t of {
   Typ_sel a _ -> check_lc_at_avar n a;
   Typ_all t1 t2 ->
    let {s = check_lc_at_typ n t1} in
    case s of {
     Prelude.True -> check_lc_at_typ (Prelude.succ n) t2;
     Prelude.False -> Prelude.False};
   Typ_rcd d -> check_lc_at_decs n d;
   _ -> Prelude.True}

check_lc_at_dec :: Prelude.Integer -> Dec -> Prelude.Bool
check_lc_at_dec n d =
  case d of {
   Dec_typ _ t0 t1 ->
    let {s = check_lc_at_typ n t0} in
    case s of {
     Prelude.True -> check_lc_at_typ n t1;
     Prelude.False -> Prelude.False};
   Dec_trm _ t0 -> check_lc_at_typ n t0}

check_lc_at_decs :: Prelude.Integer -> Decs -> Prelude.Bool
check_lc_at_decs n dS =
  case dS of {
   Decs_nil -> Prelude.True;
   Decs_cons d dS0 ->
    let {s = check_lc_at_dec n d} in
    case s of {
     Prelude.True -> check_lc_at_decs n dS0;
     Prelude.False -> Prelude.False}}

check_lc_at_trm :: Prelude.Integer -> Trm -> Prelude.Bool
check_lc_at_trm n t =
  case t of {
   Trm_var a -> check_lc_at_avar n a;
   Trm_val v -> check_lc_at_val n v;
   Trm_sel a _ -> check_lc_at_avar n a;
   Trm_app a a0 ->
    let {s = check_lc_at_avar n a} in
    case s of {
     Prelude.True -> check_lc_at_avar n a0;
     Prelude.False -> Prelude.False};
   Trm_let t0_1 t0_2 ->
    let {s = check_lc_at_trm n t0_1} in
    case s of {
     Prelude.True -> check_lc_at_trm (Prelude.succ n) t0_2;
     Prelude.False -> Prelude.False}}

check_lc_at_val :: Prelude.Integer -> Val -> Prelude.Bool
check_lc_at_val n vl =
  case vl of {
   Val_rcd d -> check_lc_at_defs n d;
   Val_lam t t0 ->
    let {s = check_lc_at_typ n t} in
    case s of {
     Prelude.True -> check_lc_at_trm (Prelude.succ n) t0;
     Prelude.False -> Prelude.False}}

check_lc_at_def :: Prelude.Integer -> Def -> Prelude.Bool
check_lc_at_def n d =
  case d of {
   Def_typ _ t0 -> check_lc_at_typ n t0;
   Def_trm _ t0 -> check_lc_at_trm n t0}

check_lc_at_defs :: Prelude.Integer -> Defs -> Prelude.Bool
check_lc_at_defs n ds =
  case ds of {
   Defs_nil -> Prelude.True;
   Defs_cons d ds0 ->
    let {s = check_lc_at_def n d} in
    case s of {
     Prelude.True -> check_lc_at_defs n ds0;
     Prelude.False -> Prelude.False}}

defs_luniq_dec :: Defs -> Prelude.Bool
defs_luniq_dec d =
  case d of {
   Defs_nil -> Prelude.True;
   Defs_cons d0 d1 ->
    case defs_luniq_dec d1 of {
     Prelude.True ->
      let {s = in_dec1 (lab_def_of d0) (dom0 (defs_to_list' d1))} in
      case s of {
       Prelude.True -> Prelude.False;
       Prelude.False -> Prelude.True};
     Prelude.False -> Prelude.False}}

decs_luniq_dec :: Decs -> Prelude.Bool
decs_luniq_dec d =
  case d of {
   Decs_nil -> Prelude.True;
   Decs_cons d0 d1 ->
    case decs_luniq_dec d1 of {
     Prelude.True ->
      let {s = in_dec1 (lab_dec_of d0) (dom0 (decs_to_list' d1))} in
      case s of {
       Prelude.True -> Prelude.False;
       Prelude.False -> Prelude.True};
     Prelude.False -> Prelude.False}}

uniq_dom_typ_dec :: Typ -> Prelude.Bool
uniq_dom_typ_dec x =
  let {
   f t =
     case t of {
      Typ_all t0 t1 ->
       case f t1 of {
        Prelude.True -> f t0;
        Prelude.False -> Prelude.False};
      Typ_rcd d ->
       let {s = decs_luniq_dec d} in
       case s of {
        Prelude.True -> uniq_dom_decs_dec d;
        Prelude.False -> Prelude.False};
      _ -> Prelude.True}}
  in f x

uniq_dom_dec_dec :: Dec -> Prelude.Bool
uniq_dom_dec_dec x =
  case x of {
   Dec_typ _ x0 x1 ->
    let {s = uniq_dom_typ_dec x0} in
    case s of {
     Prelude.True -> uniq_dom_typ_dec x1;
     Prelude.False -> Prelude.False};
   Dec_trm _ x0 -> uniq_dom_typ_dec x0}

uniq_dom_decs_dec :: Decs -> Prelude.Bool
uniq_dom_decs_dec x =
  let {
   f d =
     case d of {
      Decs_nil -> Prelude.True;
      Decs_cons d0 d1 ->
       case f d1 of {
        Prelude.True -> uniq_dom_dec_dec d0;
        Prelude.False -> Prelude.False}}}
  in f x

uniq_dom_trm_dec :: Trm -> Prelude.Bool
uniq_dom_trm_dec x =
  trm_rec (\_ -> Prelude.True) uniq_dom_val_dec (\_ _ -> Prelude.True)
    (\_ _ -> Prelude.True) (\_ iHx1 _ iHx2 ->
    case iHx2 of {
     Prelude.True -> iHx1;
     Prelude.False -> Prelude.False}) x

uniq_dom_val_dec :: Val -> Prelude.Bool
uniq_dom_val_dec x =
  val_rec (\d ->
    let {s = defs_luniq_dec d} in
    case s of {
     Prelude.True -> uniq_dom_defs_dec d;
     Prelude.False -> Prelude.False}) (\t t0 ->
    let {s = uniq_dom_typ_dec t} in
    case s of {
     Prelude.True -> uniq_dom_trm_dec t0;
     Prelude.False -> Prelude.False}) x

uniq_dom_def_dec :: Def -> Prelude.Bool
uniq_dom_def_dec x =
  def_rec (\_ -> uniq_dom_typ_dec) (\_ -> uniq_dom_trm_dec) x

uniq_dom_defs_dec :: Defs -> Prelude.Bool
uniq_dom_defs_dec x =
  let {
   f d =
     case d of {
      Defs_nil -> Prelude.True;
      Defs_cons d0 d1 ->
       case f d1 of {
        Prelude.True -> uniq_dom_def_dec d0;
        Prelude.False -> Prelude.False}}}
  in f x

exposure_impl :: (([]) ((,) Atom Typ)) -> Typ -> Prelude.Either
                 (([]) Prelude.String) Typ
exposure_impl g s =
  let {
   fix_F_sub x =
     let {g0 = projT1 x} in
     let {s0 = projT2 x} in
     let {exposure_impl0 = \g1 s1 -> fix_F_sub (ExistT g1 s1)} in
     (\_ _ _ ->
     case s0 of {
      Typ_sel a a0 ->
       case a of {
        Avar_b _ -> false_rec;
        Avar_f a1 ->
         let {s1 = get_for_sure g0 a1} in
         let {h2 = get_witness a1 g0 s1} in
         case h2 of {
          ExistT _ x0 ->
           let {recur = exposure_impl0 x0 s1 __ __ __} in
           pbind (pMonad_Monad (unsafeCoerce monad_either)) __
             (err_msg recur ((:) 'e' ((:) 'x' ((:) 'p' ((:) 'o' ((:) 's' ((:)
               'u' ((:) 'r' ((:) 'e' ((:) ':' ((:) ' ' ((:) 'n' ((:) 'o' ((:)
               't' ((:) ' ' ((:) 'a' ((:) 'b' ((:) 'l' ((:) 'e' ((:) ' ' ((:)
               't' ((:) 'o' ((:) ' ' ((:) 'p' ((:) 'e' ((:) 'r' ((:) 'f' ((:)
               'o' ((:) 'r' ((:) 'm' ((:) ' ' ((:) 'e' ((:) 'x' ((:) 'p' ((:)
               'o' ((:) 's' ((:) 'u' ((:) 'r' ((:) 'e' ((:) ' ' ((:) 'a' ((:)
               'f' ((:) 't' ((:) 'e' ((:) 'r' ((:) ' ' ((:) 'l' ((:) 'o' ((:)
               'o' ((:) 'k' ((:) 'u' ((:) 'p'
               ([])))))))))))))))))))))))))))))))))))))))))))))))))))))
             (\x1 ->
             case x1 of {
              Typ_bot -> ret (unsafeCoerce monad_either) Typ_bot;
              Typ_rcd d ->
               let {o = get0 (Label_typ a0) (decs_to_list' d)} in
               case o of {
                Prelude.Just d0 ->
                 case d0 of {
                  Dec_typ _ _ t1 ->
                   let {recur0 = exposure_impl0 x0 t1 __ __ __} in
                   pbind (pMonad_Monad (unsafeCoerce monad_either)) __
                     (err_msg recur0 ((:) 'e' ((:) 'x' ((:) 'p' ((:) 'o' ((:)
                       's' ((:) 'u' ((:) 'r' ((:) 'e' ((:) ':' ((:) ' ' ((:)
                       'f' ((:) 'a' ((:) 'i' ((:) 'l' ((:) 'e' ((:) 'd' ((:)
                       ' ' ((:) 's' ((:) 'e' ((:) 'c' ((:) 'o' ((:) 'n' ((:)
                       'd' ((:) ' ' ((:) 'r' ((:) 'e' ((:) 'c' ((:) 'u' ((:)
                       'r' ((:) 's' ((:) 'i' ((:) 'v' ((:) 'e' ((:) ' ' ((:)
                       'c' ((:) 'a' ((:) 'l' ((:) 'l'
                       ([])))))))))))))))))))))))))))))))))))))))) (\x2 ->
                     ret (unsafeCoerce monad_either) x2);
                  Dec_trm _ _ -> false_rec};
                Prelude.Nothing ->
                 erret ((:) 'e' ((:) 'x' ((:) 'p' ((:) 'o' ((:) 's' ((:) 'u'
                   ((:) 'r' ((:) 'e' ((:) ':' ((:) ' ' ((:) 'l' ((:) 'a' ((:)
                   'b' ((:) 'e' ((:) 'l' ((:) ' ' ((:) 'i' ((:) 's' ((:) ' '
                   ((:) 'n' ((:) 'o' ((:) 't' ((:) ' ' ((:) 'f' ((:) 'o' ((:)
                   'u' ((:) 'n' ((:) 'd' ([])))))))))))))))))))))))))))))};
              _ ->
               erret ((:) 'e' ((:) 'x' ((:) 'p' ((:) 'o' ((:) 's' ((:) 'u'
                 ((:) 'r' ((:) 'e' ((:) ':' ((:) ' ' ((:) 'n' ((:) 'o' ((:)
                 't' ((:) ' ' ((:) 'a' ((:) ' ' ((:) 'b' ((:) 'o' ((:) 't'
                 ((:) 't' ((:) 'o' ((:) 'm' ((:) ' ' ((:) 'o' ((:) 'r' ((:)
                 ' ' ((:) 'a' ((:) ' ' ((:) 'r' ((:) 'e' ((:) 'c' ((:) 'o'
                 ((:) 'r' ((:) 'd' ((:) ' ' ((:) 't' ((:) 'y' ((:) 'p' ((:)
                 'e' ([]))))))))))))))))))))))))))))))))))))))))})}};
      x0 -> ret (unsafeCoerce monad_either) x0})}
  in fix_F_sub (ExistT g s) __ __ __

promo_demo_func_obligation_1 :: (([]) ((,) Atom Typ)) -> Atom -> Typ ->
                                ((([]) ((,) Atom Typ)) -> Atom -> Typ -> ()
                                -> () -> () -> () -> (,)
                                (Prelude.Either (([]) Prelude.String) Typ)
                                (Prelude.Either (([]) Prelude.String) Typ))
                                -> (,)
                                (Prelude.Either (([]) Prelude.String) Typ)
                                (Prelude.Either (([]) Prelude.String) Typ)
promo_demo_func_obligation_1 g x s promo_demo0 =
  case s of {
   Typ_sel a t ->
    case a of {
     Avar_b _ -> false_rec;
     Avar_f a0 ->
      let {s0 = eq_dec eqDec_eq_of_X x a0} in
      case s0 of {
       Prelude.True ->
        let {s1 = get_for_sure g a0} in
        let {h2 = get_witness a0 g s1} in
        case h2 of {
         ExistT _ x0 ->
          let {recur = exposure_impl x0 s1} in
          (,)
          (pbind (pMonad_Monad (unsafeCoerce monad_either)) __
            (err_msg recur ((:) 'p' ((:) 'r' ((:) 'o' ((:) 'm' ((:) 'o' ((:)
              '/' ((:) 'd' ((:) 'e' ((:) 'm' ((:) 'o' ((:) ':' ((:) ' ' ((:)
              'e' ((:) 'x' ((:) 'p' ((:) 'o' ((:) 's' ((:) 'u' ((:) 'r' ((:)
              'e' ((:) ' ' ((:) 'f' ((:) 'a' ((:) 'i' ((:) 'l' ((:) 'e' ((:)
              'd' ((:) ' ' ((:) 'f' ((:) 'o' ((:) 'r' ((:) ' ' ((:) 'v' ((:)
              'a' ((:) 'r' ((:) ' ' ((:) 'c' ((:) 'a' ((:) 's' ((:) 'e' ((:)
              '.' ([]))))))))))))))))))))))))))))))))))))))))))) (\x1 ->
            case x1 of {
             Typ_bot -> ret (unsafeCoerce monad_either) Typ_bot;
             Typ_rcd d ->
              let {o = get0 (Label_typ t) (decs_to_list' d)} in
              case o of {
               Prelude.Just d0 ->
                case d0 of {
                 Dec_typ _ _ t2 -> ret (unsafeCoerce monad_either) t2;
                 Dec_trm _ _ -> false_rec};
               Prelude.Nothing ->
                erret ((:) 'p' ((:) 'r' ((:) 'o' ((:) 'm' ((:) 'o' ((:) '/'
                  ((:) 'd' ((:) 'e' ((:) 'm' ((:) 'o' ((:) ':' ((:) ' ' ((:)
                  'l' ((:) 'a' ((:) 'b' ((:) 'e' ((:) 'l' ((:) ' ' ((:) 'i'
                  ((:) 's' ((:) ' ' ((:) 'n' ((:) 'o' ((:) 't' ((:) ' ' ((:)
                  'f' ((:) 'o' ((:) 'u' ((:) 'n' ((:) 'd'
                  ([])))))))))))))))))))))))))))))))};
             _ ->
              erret ((:) 'p' ((:) 'r' ((:) 'o' ((:) 'm' ((:) 'o' ((:) '/'
                ((:) 'd' ((:) 'e' ((:) 'm' ((:) 'o' ((:) ':' ((:) ' ' ((:)
                'n' ((:) 'o' ((:) 't' ((:) ' ' ((:) 'a' ((:) ' ' ((:) 'b'
                ((:) 'o' ((:) 't' ((:) 't' ((:) 'o' ((:) 'm' ((:) ' ' ((:)
                'o' ((:) 'r' ((:) ' ' ((:) 'a' ((:) ' ' ((:) 'r' ((:) 'e'
                ((:) 'c' ((:) 'o' ((:) 'r' ((:) 'd' ((:) ' ' ((:) 't' ((:)
                'y' ((:) 'p' ((:) 'e'
                ([]))))))))))))))))))))))))))))))))))))))))))}))
          (pbind (pMonad_Monad (unsafeCoerce monad_either)) __
            (err_msg recur ((:) 'p' ((:) 'r' ((:) 'o' ((:) 'm' ((:) 'o' ((:)
              '/' ((:) 'd' ((:) 'e' ((:) 'm' ((:) 'o' ((:) ':' ((:) ' ' ((:)
              'e' ((:) 'x' ((:) 'p' ((:) 'o' ((:) 's' ((:) 'u' ((:) 'r' ((:)
              'e' ((:) ' ' ((:) 'f' ((:) 'a' ((:) 'i' ((:) 'l' ((:) 'e' ((:)
              'd' ((:) ' ' ((:) 'f' ((:) 'o' ((:) 'r' ((:) ' ' ((:) 'v' ((:)
              'a' ((:) 'r' ((:) ' ' ((:) 'c' ((:) 'a' ((:) 's' ((:) 'e' ((:)
              '.' ([]))))))))))))))))))))))))))))))))))))))))))) (\x1 ->
            case x1 of {
             Typ_bot -> ret (unsafeCoerce monad_either) Typ_top;
             Typ_rcd d ->
              let {o = get0 (Label_typ t) (decs_to_list' d)} in
              case o of {
               Prelude.Just d0 ->
                case d0 of {
                 Dec_typ _ t1 _ -> ret (unsafeCoerce monad_either) t1;
                 Dec_trm _ _ -> false_rec};
               Prelude.Nothing ->
                erret ((:) 'p' ((:) 'r' ((:) 'o' ((:) 'm' ((:) 'o' ((:) '/'
                  ((:) 'd' ((:) 'e' ((:) 'm' ((:) 'o' ((:) ':' ((:) ' ' ((:)
                  'l' ((:) 'a' ((:) 'b' ((:) 'e' ((:) 'l' ((:) ' ' ((:) 'i'
                  ((:) 's' ((:) ' ' ((:) 'n' ((:) 'o' ((:) 't' ((:) ' ' ((:)
                  'f' ((:) 'o' ((:) 'u' ((:) 'n' ((:) 'd'
                  ([])))))))))))))))))))))))))))))))};
             _ ->
              erret ((:) 'p' ((:) 'r' ((:) 'o' ((:) 'm' ((:) 'o' ((:) '/'
                ((:) 'd' ((:) 'e' ((:) 'm' ((:) 'o' ((:) ':' ((:) ' ' ((:)
                'n' ((:) 'o' ((:) 't' ((:) ' ' ((:) 'a' ((:) ' ' ((:) 'b'
                ((:) 'o' ((:) 't' ((:) 't' ((:) 'o' ((:) 'm' ((:) ' ' ((:)
                'o' ((:) 'r' ((:) ' ' ((:) 'a' ((:) ' ' ((:) 'r' ((:) 'e'
                ((:) 'c' ((:) 'o' ((:) 'r' ((:) 'd' ((:) ' ' ((:) 't' ((:)
                'y' ((:) 'p' ((:) 'e'
                ([]))))))))))))))))))))))))))))))))))))))))))}))};
       Prelude.False -> (,)
        (ret (unsafeCoerce monad_either) (Typ_sel (Avar_f a0) t))
        (ret (unsafeCoerce monad_either) (Typ_sel (Avar_f a0) t))}};
   Typ_all s1 s2 ->
    let {p = promo_demo0 g x s1 __ __ __ __} in
    case p of {
     (,) s0 s3 -> (,)
      (pbind (pMonad_Monad (unsafeCoerce monad_either)) __
        (err_msg s3 ((:) 'p' ((:) 'r' ((:) 'o' ((:) 'm' ((:) 'o' ((:) 't'
          ((:) 'i' ((:) 'o' ((:) 'n' ((:) ':' ((:) ' ' ((:) 'f' ((:) 'a' ((:)
          'i' ((:) 'l' ((:) 'e' ((:) 'd' ((:) ' ' ((:) 'i' ((:) 'n' ((:) ' '
          ((:) 'f' ((:) 'u' ((:) 'n' ((:) 'c' ((:) 't' ((:) 'i' ((:) 'o' ((:)
          'n' ((:) ' ' ((:) 'c' ((:) 'a' ((:) 's' ((:) 'e'
          ([])))))))))))))))))))))))))))))))))))) (\x0 ->
        let {
         s4 = atom_fresh
                (union1 (singleton1 x)
                  (union1 (dom g)
                    (union1 (fv_values fv_typ g)
                      (union1 (singleton1 x)
                        (union1 (fv_typ s1) (union1 (fv_typ s2) (fv_typ x0)))))))}
        in
        let {
         p0 = promo_demo0 (app (one ((,) s4 x0)) g) x
                (open_rec openTyp 0 s4 s2) __ __ __ __}
        in
        case p0 of {
         (,) s5 _ ->
          pbind (pMonad_Monad (unsafeCoerce monad_either)) __
            (err_msg s5 ((:) 'p' ((:) 'r' ((:) 'o' ((:) 'm' ((:) 'o' ((:) 't'
              ((:) 'i' ((:) 'o' ((:) 'n' ((:) ':' ((:) ' ' ((:) 'f' ((:) 'a'
              ((:) 'i' ((:) 'l' ((:) 'e' ((:) 'd' ((:) ' ' ((:) 'i' ((:) 'n'
              ((:) ' ' ((:) 'f' ((:) 'u' ((:) 'n' ((:) 'c' ((:) 't' ((:) 'i'
              ((:) 'o' ((:) 'n' ((:) ' ' ((:) 'b' ((:) 'o' ((:) 'd' ((:) 'y'
              ((:) ' ' ((:) 't' ((:) 'y' ((:) 'p' ((:) 'e'
              ([]))))))))))))))))))))))))))))))))))))))))) (\x1 ->
            ret (unsafeCoerce monad_either) (Typ_all x0
              (close_rec closeTyp 0 s4 x1)))}))
      (pbind (pMonad_Monad (unsafeCoerce monad_either)) __
        (err_msg s0 ((:) 'd' ((:) 'e' ((:) 'm' ((:) 'o' ((:) 't' ((:) 'i'
          ((:) 'o' ((:) 'n' ((:) ':' ((:) ' ' ((:) 'f' ((:) 'a' ((:) 'i' ((:)
          'l' ((:) 'e' ((:) 'd' ((:) ' ' ((:) 'i' ((:) 'n' ((:) ' ' ((:) 'f'
          ((:) 'u' ((:) 'n' ((:) 'c' ((:) 't' ((:) 'i' ((:) 'o' ((:) 'n' ((:)
          ' ' ((:) 'c' ((:) 'a' ((:) 's' ((:) 'e'
          ([]))))))))))))))))))))))))))))))))))) (\x0 ->
        let {
         s4 = atom_fresh
                (union1 (singleton1 x)
                  (union1 (dom g)
                    (union1 (fv_values fv_typ g)
                      (union1 (singleton1 x)
                        (union1 (fv_typ s1) (union1 (fv_typ s2) (fv_typ x0)))))))}
        in
        let {
         p0 = promo_demo0 (app (one ((,) s4 s1)) g) x
                (open_rec openTyp 0 s4 s2) __ __ __ __}
        in
        case p0 of {
         (,) _ s5 ->
          pbind (pMonad_Monad (unsafeCoerce monad_either)) __
            (err_msg s5 ((:) 'p' ((:) 'r' ((:) 'o' ((:) 'm' ((:) 'o' ((:) 't'
              ((:) 'i' ((:) 'o' ((:) 'n' ((:) ':' ((:) ' ' ((:) 'f' ((:) 'a'
              ((:) 'i' ((:) 'l' ((:) 'e' ((:) 'd' ((:) ' ' ((:) 'i' ((:) 'n'
              ((:) ' ' ((:) 'f' ((:) 'u' ((:) 'n' ((:) 'c' ((:) 't' ((:) 'i'
              ((:) 'o' ((:) 'n' ((:) ' ' ((:) 'b' ((:) 'o' ((:) 'd' ((:) 'y'
              ((:) ' ' ((:) 't' ((:) 'y' ((:) 'p' ((:) 'e'
              ([]))))))))))))))))))))))))))))))))))))))))) (\x1 ->
            ret (unsafeCoerce monad_either) (Typ_all x0
              (close_rec closeTyp 0 s4 x1)))}))};
   Typ_rcd d ->
    let {
     h2 = \h2 ->
      let {
       f d0 =
         case d0 of {
          Decs_nil -> (,) (ret monad_either Decs_nil)
           (ret monad_either Decs_nil);
          Decs_cons d1 d2 ->
           let {p = f d2} in
           case p of {
            (,) s0 s1 ->
             case d1 of {
              Dec_typ t t0 t1 ->
               let {p0 = promo_demo0 g x t0 __ __ __ __} in
               case p0 of {
                (,) s2 s3 ->
                 let {p1 = promo_demo0 g x t1 __ __ __ __} in
                 case p1 of {
                  (,) s4 s5 -> (,)
                   (pbind (pMonad_Monad monad_either) __
                     (err_msg s0 ((:) 'p' ((:) 'r' ((:) 'o' ((:) 'm' ((:) 'o'
                       ((:) 't' ((:) 'i' ((:) 'o' ((:) 'n' ((:) ':' ((:) ' '
                       ((:) 'f' ((:) 'a' ((:) 'i' ((:) 'l' ((:) 'e' ((:) 'd'
                       ((:) ' ' ((:) 'i' ((:) 'n' ((:) ' ' ((:) 'r' ((:) 'e'
                       ((:) 'c' ((:) 'u' ((:) 'r' ((:) 's' ((:) 'i' ((:) 'v'
                       ((:) 'e' ((:) ' ' ((:) 'c' ((:) 'a' ((:) 's' ((:) 'e'
                       ((:) ' ' ((:) 'f' ((:) 'o' ((:) 'r' ((:) ' ' ((:) 'd'
                       ((:) 'e' ((:) 'c' ((:) 'l' ((:) 'a' ((:) 'r' ((:) 'a'
                       ((:) 't' ((:) 'i' ((:) 'o' ((:) 'n' ((:) 's'
                       ([]))))))))))))))))))))))))))))))))))))))))))))))))))))))
                     (\x0 ->
                     pbind (pMonad_Monad monad_either) __
                       (err_msg (unsafeCoerce s3) ((:) 'p' ((:) 'r' ((:) 'o'
                         ((:) 'm' ((:) 'o' ((:) 't' ((:) 'i' ((:) 'o' ((:)
                         'n' ((:) ':' ((:) ' ' ((:) 'f' ((:) 'a' ((:) 'i'
                         ((:) 'l' ((:) 'e' ((:) 'd' ((:) ' ' ((:) 'i' ((:)
                         'n' ((:) ' ' ((:) 'l' ((:) 'o' ((:) 'w' ((:) 'e'
                         ((:) 'r' ((:) ' ' ((:) 'b' ((:) 'o' ((:) 'u' ((:)
                         'n' ((:) 'd' ((:) ' ' ((:) 'c' ((:) 'a' ((:) 's'
                         ((:) 'e' ((:) ' ' ((:) 'f' ((:) 'o' ((:) 'r' ((:)
                         ' ' ((:) 'd' ((:) 'e' ((:) 'c' ((:) 'l' ((:) 'a'
                         ((:) 'r' ((:) 'a' ((:) 't' ((:) 'i' ((:) 'o' ((:)
                         'n' ((:) 's'
                         ([]))))))))))))))))))))))))))))))))))))))))))))))))))))))))
                       (\x1 ->
                       pbind (pMonad_Monad monad_either) __
                         (err_msg (unsafeCoerce s4) ((:) 'p' ((:) 'r' ((:)
                           'o' ((:) 'm' ((:) 'o' ((:) 't' ((:) 'i' ((:) 'o'
                           ((:) 'n' ((:) ':' ((:) ' ' ((:) 'f' ((:) 'a' ((:)
                           'i' ((:) 'l' ((:) 'e' ((:) 'd' ((:) ' ' ((:) 'i'
                           ((:) 'n' ((:) ' ' ((:) 'u' ((:) 'p' ((:) 'p' ((:)
                           'e' ((:) 'r' ((:) ' ' ((:) 'b' ((:) 'o' ((:) 'u'
                           ((:) 'n' ((:) 'd' ((:) ' ' ((:) 'c' ((:) 'a' ((:)
                           's' ((:) 'e' ((:) ' ' ((:) 'f' ((:) 'o' ((:) 'r'
                           ((:) ' ' ((:) 'd' ((:) 'e' ((:) 'c' ((:) 'l' ((:)
                           'a' ((:) 'r' ((:) 'a' ((:) 't' ((:) 'i' ((:) 'o'
                           ((:) 'n' ((:) 's'
                           ([]))))))))))))))))))))))))))))))))))))))))))))))))))))))))
                         (\x2 ->
                         ret monad_either
                           (append' decsListCond (Decs_cons (Dec_typ t x1 x2)
                             Decs_nil) x0)))))
                   (pbind (pMonad_Monad monad_either) __
                     (err_msg s1 ((:) 'd' ((:) 'e' ((:) 'm' ((:) 'o' ((:) 't'
                       ((:) 'i' ((:) 'o' ((:) 'n' ((:) ':' ((:) ' ' ((:) 'f'
                       ((:) 'a' ((:) 'i' ((:) 'l' ((:) 'e' ((:) 'd' ((:) ' '
                       ((:) 'i' ((:) 'n' ((:) ' ' ((:) 'r' ((:) 'e' ((:) 'c'
                       ((:) 'u' ((:) 'r' ((:) 's' ((:) 'i' ((:) 'v' ((:) 'e'
                       ((:) ' ' ((:) 'c' ((:) 'a' ((:) 's' ((:) 'e' ((:) ' '
                       ((:) 'f' ((:) 'o' ((:) 'r' ((:) ' ' ((:) 'd' ((:) 'e'
                       ((:) 'c' ((:) 'l' ((:) 'a' ((:) 'r' ((:) 'a' ((:) 't'
                       ((:) 'i' ((:) 'o' ((:) 'n' ((:) 's'
                       ([])))))))))))))))))))))))))))))))))))))))))))))))))))))
                     (\x0 ->
                     pbind (pMonad_Monad monad_either) __
                       (err_msg (unsafeCoerce s2) ((:) 'd' ((:) 'e' ((:) 'm'
                         ((:) 'o' ((:) 't' ((:) 'i' ((:) 'o' ((:) 'n' ((:)
                         ':' ((:) ' ' ((:) 'f' ((:) 'a' ((:) 'i' ((:) 'l'
                         ((:) 'e' ((:) 'd' ((:) ' ' ((:) 'i' ((:) 'n' ((:)
                         ' ' ((:) 'l' ((:) 'o' ((:) 'w' ((:) 'e' ((:) 'r'
                         ((:) ' ' ((:) 'b' ((:) 'o' ((:) 'u' ((:) 'n' ((:)
                         'd' ((:) ' ' ((:) 'c' ((:) 'a' ((:) 's' ((:) 'e'
                         ((:) ' ' ((:) 'f' ((:) 'o' ((:) 'r' ((:) ' ' ((:)
                         'd' ((:) 'e' ((:) 'c' ((:) 'l' ((:) 'a' ((:) 'r'
                         ((:) 'a' ((:) 't' ((:) 'i' ((:) 'o' ((:) 'n' ((:)
                         's'
                         ([])))))))))))))))))))))))))))))))))))))))))))))))))))))))
                       (\x1 ->
                       pbind (pMonad_Monad monad_either) __
                         (err_msg (unsafeCoerce s5) ((:) 'd' ((:) 'e' ((:)
                           'm' ((:) 'o' ((:) 't' ((:) 'i' ((:) 'o' ((:) 'n'
                           ((:) ':' ((:) ' ' ((:) 'f' ((:) 'a' ((:) 'i' ((:)
                           'l' ((:) 'e' ((:) 'd' ((:) ' ' ((:) 'i' ((:) 'n'
                           ((:) ' ' ((:) 'u' ((:) 'p' ((:) 'p' ((:) 'e' ((:)
                           'r' ((:) ' ' ((:) 'b' ((:) 'o' ((:) 'u' ((:) 'n'
                           ((:) 'd' ((:) ' ' ((:) 'c' ((:) 'a' ((:) 's' ((:)
                           'e' ((:) ' ' ((:) 'f' ((:) 'o' ((:) 'r' ((:) ' '
                           ((:) 'd' ((:) 'e' ((:) 'c' ((:) 'l' ((:) 'a' ((:)
                           'r' ((:) 'a' ((:) 't' ((:) 'i' ((:) 'o' ((:) 'n'
                           ((:) 's'
                           ([])))))))))))))))))))))))))))))))))))))))))))))))))))))))
                         (\x2 ->
                         ret monad_either
                           (append' decsListCond (Decs_cons (Dec_typ t x1 x2)
                             Decs_nil) x0)))))}};
              Dec_trm t t0 ->
               let {p0 = promo_demo0 g x t0 __ __ __ __} in
               case p0 of {
                (,) s2 s3 -> (,)
                 (pbind (pMonad_Monad monad_either) __
                   (err_msg s0 ((:) 'p' ((:) 'r' ((:) 'o' ((:) 'm' ((:) 'o'
                     ((:) 't' ((:) 'i' ((:) 'o' ((:) 'n' ((:) ':' ((:) ' '
                     ((:) 'f' ((:) 'a' ((:) 'i' ((:) 'l' ((:) 'e' ((:) 'd'
                     ((:) ' ' ((:) 'i' ((:) 'n' ((:) ' ' ((:) 'r' ((:) 'e'
                     ((:) 'c' ((:) 'u' ((:) 'r' ((:) 's' ((:) 'i' ((:) 'v'
                     ((:) 'e' ((:) ' ' ((:) 'c' ((:) 'a' ((:) 's' ((:) 'e'
                     ((:) ' ' ((:) 'f' ((:) 'o' ((:) 'r' ((:) ' ' ((:) 'd'
                     ((:) 'e' ((:) 'c' ((:) 'l' ((:) 'a' ((:) 'r' ((:) 'a'
                     ((:) 't' ((:) 'i' ((:) 'o' ((:) 'n' ((:) 's'
                     ([]))))))))))))))))))))))))))))))))))))))))))))))))))))))
                   (\x0 ->
                   pbind (pMonad_Monad monad_either) __
                     (err_msg (unsafeCoerce s2) ((:) 'p' ((:) 'r' ((:) 'o'
                       ((:) 'm' ((:) 'o' ((:) 't' ((:) 'i' ((:) 'o' ((:) 'n'
                       ((:) ':' ((:) ' ' ((:) 'f' ((:) 'a' ((:) 'i' ((:) 'l'
                       ((:) 'e' ((:) 'd' ((:) ' ' ((:) 'i' ((:) 'n' ((:) ' '
                       ((:) 'f' ((:) 'i' ((:) 'e' ((:) 'l' ((:) 'd' ((:) ' '
                       ((:) 't' ((:) 'y' ((:) 'p' ((:) 'e' ((:) ' ' ((:) 'c'
                       ((:) 'a' ((:) 's' ((:) 'e' ((:) ' ' ((:) 'f' ((:) 'o'
                       ((:) 'r' ((:) ' ' ((:) 'd' ((:) 'e' ((:) 'c' ((:) 'l'
                       ((:) 'a' ((:) 'r' ((:) 'a' ((:) 't' ((:) 'i' ((:) 'o'
                       ((:) 'n' ((:) 's'
                       ([])))))))))))))))))))))))))))))))))))))))))))))))))))))))
                     (\x1 ->
                     ret monad_either
                       (append' decsListCond (Decs_cons (Dec_trm t x1)
                         Decs_nil) x0))))
                 (pbind (pMonad_Monad monad_either) __
                   (err_msg s1 ((:) 'p' ((:) 'r' ((:) 'o' ((:) 'm' ((:) 'o'
                     ((:) 't' ((:) 'i' ((:) 'o' ((:) 'n' ((:) ':' ((:) ' '
                     ((:) 'f' ((:) 'a' ((:) 'i' ((:) 'l' ((:) 'e' ((:) 'd'
                     ((:) ' ' ((:) 'i' ((:) 'n' ((:) ' ' ((:) 'r' ((:) 'e'
                     ((:) 'c' ((:) 'u' ((:) 'r' ((:) 's' ((:) 'i' ((:) 'v'
                     ((:) 'e' ((:) ' ' ((:) 'c' ((:) 'a' ((:) 's' ((:) 'e'
                     ((:) ' ' ((:) 'f' ((:) 'o' ((:) 'r' ((:) ' ' ((:) 'd'
                     ((:) 'e' ((:) 'c' ((:) 'l' ((:) 'a' ((:) 'r' ((:) 'a'
                     ((:) 't' ((:) 'i' ((:) 'o' ((:) 'n' ((:) 's'
                     ([]))))))))))))))))))))))))))))))))))))))))))))))))))))))
                   (\x0 ->
                   pbind (pMonad_Monad monad_either) __
                     (err_msg (unsafeCoerce s3) ((:) 'p' ((:) 'r' ((:) 'o'
                       ((:) 'm' ((:) 'o' ((:) 't' ((:) 'i' ((:) 'o' ((:) 'n'
                       ((:) ':' ((:) ' ' ((:) 'f' ((:) 'a' ((:) 'i' ((:) 'l'
                       ((:) 'e' ((:) 'd' ((:) ' ' ((:) 'i' ((:) 'n' ((:) ' '
                       ((:) 'f' ((:) 'i' ((:) 'e' ((:) 'l' ((:) 'd' ((:) ' '
                       ((:) 't' ((:) 'y' ((:) 'p' ((:) 'e' ((:) ' ' ((:) 'c'
                       ((:) 'a' ((:) 's' ((:) 'e' ((:) ' ' ((:) 'f' ((:) 'o'
                       ((:) 'r' ((:) ' ' ((:) 'd' ((:) 'e' ((:) 'c' ((:) 'l'
                       ((:) 'a' ((:) 'r' ((:) 'a' ((:) 't' ((:) 'i' ((:) 'o'
                       ((:) 'n' ((:) 's'
                       ([])))))))))))))))))))))))))))))))))))))))))))))))))))))))
                     (\x1 ->
                     ret monad_either
                       (append' decsListCond (Decs_cons (Dec_trm t x1)
                         Decs_nil) x0))))}}}}}
      in f h2}
    in
    let {p = h2 d} in
    case p of {
     (,) s0 s1 -> (,)
      (pbind (pMonad_Monad (unsafeCoerce monad_either)) __
        (err_msg (unsafeCoerce s0) ((:) 'p' ((:) 'r' ((:) 'o' ((:) 'm' ((:)
          'o' ((:) 't' ((:) 'i' ((:) 'o' ((:) 'n' ((:) ':' ((:) ' ' ((:) 'f'
          ((:) 'a' ((:) 'i' ((:) 'l' ((:) 'e' ((:) 'd' ((:) ' ' ((:) 't' ((:)
          'o' ((:) ' ' ((:) 'p' ((:) 'r' ((:) 'o' ((:) 'm' ((:) 'o' ((:) 't'
          ((:) 'e' ((:) ' ' ((:) 'd' ((:) 'e' ((:) 'c' ((:) 'l' ((:) 'a' ((:)
          'r' ((:) 'a' ((:) 't' ((:) 'i' ((:) 'o' ((:) 'n' ((:) 's'
          ([]))))))))))))))))))))))))))))))))))))))))))) (\x0 ->
        ret (unsafeCoerce monad_either) (Typ_rcd x0)))
      (pbind (pMonad_Monad (unsafeCoerce monad_either)) __
        (err_msg (unsafeCoerce s1) ((:) 'd' ((:) 'e' ((:) 'm' ((:) 'o' ((:)
          't' ((:) 'i' ((:) 'o' ((:) 'n' ((:) ':' ((:) ' ' ((:) 'f' ((:) 'a'
          ((:) 'i' ((:) 'l' ((:) 'e' ((:) 'd' ((:) ' ' ((:) 't' ((:) 'o' ((:)
          ' ' ((:) 'd' ((:) 'e' ((:) 'm' ((:) 'o' ((:) 't' ((:) 'e' ((:) ' '
          ((:) 'd' ((:) 'e' ((:) 'c' ((:) 'l' ((:) 'a' ((:) 'r' ((:) 'a' ((:)
          't' ((:) 'i' ((:) 'o' ((:) 'n' ((:) 's'
          ([]))))))))))))))))))))))))))))))))))))))))) (\x0 ->
        ret (unsafeCoerce monad_either) (Typ_rcd x0)))};
   x0 -> (,) (ret (unsafeCoerce monad_either) x0)
    (ret (unsafeCoerce monad_either) x0)}

promo_demo_func :: (SigT (([]) ((,) Atom Typ)) (SigT Atom Typ)) -> (,)
                   (Prelude.Either (([]) Prelude.String) Typ)
                   (Prelude.Either (([]) Prelude.String) Typ)
promo_demo_func recarg =
  let {
   fix_F_sub x =
     let {g = projT1 x} in
     let {x0 = projT1 (projT2 x)} in
     let {s = projT2 (projT2 x)} in
     let {promo_demo0 = \g0 x1 s0 -> fix_F_sub (ExistT g0 (ExistT x1 s0))} in
     (\_ _ _ ->
     promo_demo_func_obligation_1 g x0 s (\g0 x1 s0 _ ->
       promo_demo0 g0 x1 s0))}
  in fix_F_sub recarg __ __ __

promo_demo :: (([]) ((,) Atom Typ)) -> Atom -> Typ -> (,)
              (Prelude.Either (([]) Prelude.String) Typ)
              (Prelude.Either (([]) Prelude.String) Typ)
promo_demo g x s =
  promo_demo_func (ExistT g (ExistT x s))

promotion :: (([]) ((,) Atom Typ)) -> Atom -> Typ -> Prelude.Either
             (([]) Prelude.String) Typ
promotion g x s =
  let {p = promo_demo g x s} in case p of {
                                 (,) s0 _ -> s0}

type Subtyp_dec_sig =
  () -> () -> () -> () -> () -> Prelude.Either (([]) Prelude.String) ()

type Subtyp_checker = (([]) ((,) Atom Typ)) -> Typ -> Typ -> Subtyp_dec_sig

subty_dec :: (([]) ((,) Atom Typ)) -> Typ -> Typ -> Prelude.Either
             (([]) Prelude.String) ()
subty_dec g t u =
  let {
   fix_F_sub x =
     let {g0 = projT1 x} in
     let {t0 = projT1 (projT2 x)} in
     let {u0 = projT2 (projT2 x)} in
     let {subty_dec0 = \g1 t1 u1 -> fix_F_sub (ExistT g1 (ExistT t1 u1))} in
     (\_ _ _ _ _ ->
     case t0 of {
      Typ_top ->
       case u0 of {
        Typ_top -> ret (unsafeCoerce monad_either) __;
        _ ->
         erret ((:) 's' ((:) 'u' ((:) 'b' ((:) 't' ((:) 'y' ((:) 'p' ((:) 'e'
           ((:) 'r' ((:) ':' ((:) ' ' ((:) 'u' ((:) 'n' ((:) 'k' ((:) 'n'
           ((:) 'o' ((:) 'w' ((:) 'n' ((:) ' ' ((:) 'h' ((:) 'o' ((:) 'w'
           ((:) ' ' ((:) 't' ((:) 'o' ((:) ' ' ((:) 'c' ((:) 'o' ((:) 'm'
           ((:) 'p' ((:) 'a' ((:) 'r' ((:) 'e' ((:) ' ' ((:) 't' ((:) 'w'
           ((:) 'o' ((:) ' ' ((:) 't' ((:) 'y' ((:) 'p' ((:) 'e' ((:) 's'
           ((:) '.' ([]))))))))))))))))))))))))))))))))))))))))))))};
      Typ_bot -> ret (unsafeCoerce monad_either) __;
      Typ_sel a t1 ->
       case u0 of {
        Typ_top -> ret (unsafeCoerce monad_either) __;
        Typ_bot ->
         erret ((:) 's' ((:) 'u' ((:) 'b' ((:) 't' ((:) 'y' ((:) 'p' ((:) 'e'
           ((:) 'r' ((:) ':' ((:) ' ' ((:) 'u' ((:) 'n' ((:) 'k' ((:) 'n'
           ((:) 'o' ((:) 'w' ((:) 'n' ((:) ' ' ((:) 'h' ((:) 'o' ((:) 'w'
           ((:) ' ' ((:) 't' ((:) 'o' ((:) ' ' ((:) 'c' ((:) 'o' ((:) 'm'
           ((:) 'p' ((:) 'a' ((:) 'r' ((:) 'e' ((:) ' ' ((:) 't' ((:) 'w'
           ((:) 'o' ((:) ' ' ((:) 't' ((:) 'y' ((:) 'p' ((:) 'e' ((:) 's'
           ((:) '.' ([]))))))))))))))))))))))))))))))))))))))))))));
        Typ_sel a0 t2 ->
         case a0 of {
          Avar_b _ -> false_rec;
          Avar_f a1 ->
           case a of {
            Avar_b _ -> false_rec;
            Avar_f a2 ->
             let {s = eq_dec eqDec_eq_of_X a2 a1} in
             case s of {
              Prelude.True ->
               let {s0 = eq_dec eqDecTypLab t1 t2} in
               case s0 of {
                Prelude.True -> ret (unsafeCoerce monad_either) __;
                Prelude.False ->
                 let {s1 = get_for_sure g0 a1} in
                 let {h6 = get_witness a1 g0 s1} in
                 case h6 of {
                  ExistT _ x0 ->
                   let {h12 = exposure_impl x0 s1} in
                   pbind (pMonad_Monad (unsafeCoerce monad_either)) __
                     (err_msg (unsafeCoerce h12) ((:) 's' ((:) 'u' ((:) 'b'
                       ((:) 't' ((:) 'y' ((:) 'p' ((:) 'e' ((:) 'r' ((:) ':'
                       ((:) ' ' ((:) 'e' ((:) 'x' ((:) 'p' ((:) 'o' ((:) 's'
                       ((:) 'u' ((:) 'r' ((:) 'e' ((:) ' ' ((:) 'f' ((:) 'a'
                       ((:) 'i' ((:) 'l' ((:) 'e' ((:) 'd'
                       ([]))))))))))))))))))))))))))) (\x1 ->
                     case x1 of {
                      Typ_bot -> ret (unsafeCoerce monad_either) __;
                      Typ_rcd d ->
                       let {o = get0 (Label_typ t1) (decs_to_list' d)} in
                       case o of {
                        Prelude.Just d0 ->
                         case d0 of {
                          Dec_typ _ _ t3 ->
                           let {
                            h24 = subty_dec0 g0 t3 (Typ_sel (Avar_f a1) t2)
                                    __ __ __ __ __}
                           in
                           pbind (pMonad_Monad (unsafeCoerce monad_either))
                             __
                             (err_msg h24 ((:) 's' ((:) 'u' ((:) 'b' ((:) 't'
                               ((:) 'y' ((:) 'p' ((:) 'e' ((:) 'r' ((:) ':'
                               ((:) ' ' ((:) 'r' ((:) 'e' ((:) 'c' ((:) 'u'
                               ((:) 'r' ((:) 's' ((:) 'i' ((:) 'v' ((:) 'e'
                               ((:) ' ' ((:) 's' ((:) 'u' ((:) 'b' ((:) 't'
                               ((:) 'y' ((:) 'p' ((:) 'i' ((:) 'n' ((:) 'g'
                               ((:) ' ' ((:) 'c' ((:) 'a' ((:) 'l' ((:) 'l'
                               ((:) ' ' ((:) 'f' ((:) 'a' ((:) 'i' ((:) 'l'
                               ((:) 'e' ((:) 'd' ((:) '.'
                               ([]))))))))))))))))))))))))))))))))))))))))))))
                             (\_ -> ret (unsafeCoerce monad_either) __);
                          Dec_trm _ _ -> false_rec};
                        Prelude.Nothing ->
                         erret ((:) 's' ((:) 'u' ((:) 'b' ((:) 't' ((:) 'y'
                           ((:) 'p' ((:) 'e' ((:) 'r' ((:) ':' ((:) ' ' ((:)
                           'a' ((:) ' ' ((:) 'l' ((:) 'a' ((:) 'b' ((:) 'e'
                           ((:) 'l' ((:) ' ' ((:) 'i' ((:) 's' ((:) ' ' ((:)
                           'n' ((:) 'o' ((:) 't' ((:) ' ' ((:) 'f' ((:) 'o'
                           ((:) 'u' ((:) 'n' ((:) 'd'
                           ([])))))))))))))))))))))))))))))))};
                      _ ->
                       erret ((:) 's' ((:) 'u' ((:) 'b' ((:) 't' ((:) 'y'
                         ((:) 'p' ((:) 'e' ((:) 'r' ((:) ':' ((:) ' ' ((:)
                         'u' ((:) 'n' ((:) 'a' ((:) 'b' ((:) 'l' ((:) 'e'
                         ((:) ' ' ((:) 't' ((:) 'o' ((:) ' ' ((:) 'h' ((:)
                         'a' ((:) 'n' ((:) 'd' ((:) 'l' ((:) 'e' ((:) ' '
                         ((:) 'r' ((:) 'e' ((:) 's' ((:) 'u' ((:) 'l' ((:)
                         't' ((:) 'i' ((:) 'n' ((:) 'g' ((:) ' ' ((:) 's'
                         ((:) 't' ((:) 'r' ((:) 'u' ((:) 'c' ((:) 't' ((:)
                         'u' ((:) 'r' ((:) 'e'
                         ([])))))))))))))))))))))))))))))))))))))))))))))))})}};
              Prelude.False ->
               let {s0 = get_for_sure g0 a2} in
               let {h6 = get_witness a2 g0 s0} in
               case h6 of {
                ExistT _ x0 ->
                 let {h12 = exposure_impl x0 s0} in
                 pbind (pMonad_Monad (unsafeCoerce monad_either)) __
                   (err_msg (unsafeCoerce h12) ((:) 's' ((:) 'u' ((:) 'b'
                     ((:) 't' ((:) 'y' ((:) 'p' ((:) 'e' ((:) 'r' ((:) ':'
                     ((:) ' ' ((:) 'e' ((:) 'x' ((:) 'p' ((:) 'o' ((:) 's'
                     ((:) 'u' ((:) 'r' ((:) 'e' ((:) ' ' ((:) 'f' ((:) 'a'
                     ((:) 'i' ((:) 'l' ((:) 'e' ((:) 'd'
                     ([]))))))))))))))))))))))))))) (\x1 ->
                   case x1 of {
                    Typ_bot -> ret (unsafeCoerce monad_either) __;
                    Typ_rcd d ->
                     let {o = get0 (Label_typ t1) (decs_to_list' d)} in
                     case o of {
                      Prelude.Just d0 ->
                       case d0 of {
                        Dec_typ _ _ t3 ->
                         let {
                          h24 = subty_dec0 g0 t3 (Typ_sel (Avar_f a1) t2) __
                                  __ __ __ __}
                         in
                         pbind (pMonad_Monad (unsafeCoerce monad_either)) __
                           (err_msg h24 ((:) 's' ((:) 'u' ((:) 'b' ((:) 't'
                             ((:) 'y' ((:) 'p' ((:) 'e' ((:) 'r' ((:) ':'
                             ((:) ' ' ((:) 'r' ((:) 'e' ((:) 'c' ((:) 'u'
                             ((:) 'r' ((:) 's' ((:) 'i' ((:) 'v' ((:) 'e'
                             ((:) ' ' ((:) 's' ((:) 'u' ((:) 'b' ((:) 't'
                             ((:) 'y' ((:) 'p' ((:) 'i' ((:) 'n' ((:) 'g'
                             ((:) ' ' ((:) 'c' ((:) 'a' ((:) 'l' ((:) 'l'
                             ((:) ' ' ((:) 'f' ((:) 'a' ((:) 'i' ((:) 'l'
                             ((:) 'e' ((:) 'd' ((:) '.'
                             ([]))))))))))))))))))))))))))))))))))))))))))))
                           (\_ -> ret (unsafeCoerce monad_either) __);
                        Dec_trm _ _ -> false_rec};
                      Prelude.Nothing ->
                       erret ((:) 's' ((:) 'u' ((:) 'b' ((:) 't' ((:) 'y'
                         ((:) 'p' ((:) 'e' ((:) 'r' ((:) ':' ((:) ' ' ((:)
                         'a' ((:) ' ' ((:) 'l' ((:) 'a' ((:) 'b' ((:) 'e'
                         ((:) 'l' ((:) ' ' ((:) 'i' ((:) 's' ((:) ' ' ((:)
                         'n' ((:) 'o' ((:) 't' ((:) ' ' ((:) 'f' ((:) 'o'
                         ((:) 'u' ((:) 'n' ((:) 'd'
                         ([])))))))))))))))))))))))))))))))};
                    _ ->
                     erret ((:) 's' ((:) 'u' ((:) 'b' ((:) 't' ((:) 'y' ((:)
                       'p' ((:) 'e' ((:) 'r' ((:) ':' ((:) ' ' ((:) 'u' ((:)
                       'n' ((:) 'a' ((:) 'b' ((:) 'l' ((:) 'e' ((:) ' ' ((:)
                       't' ((:) 'o' ((:) ' ' ((:) 'h' ((:) 'a' ((:) 'n' ((:)
                       'd' ((:) 'l' ((:) 'e' ((:) ' ' ((:) 'r' ((:) 'e' ((:)
                       's' ((:) 'u' ((:) 'l' ((:) 't' ((:) 'i' ((:) 'n' ((:)
                       'g' ((:) ' ' ((:) 's' ((:) 't' ((:) 'r' ((:) 'u' ((:)
                       'c' ((:) 't' ((:) 'u' ((:) 'r' ((:) 'e'
                       ([])))))))))))))))))))))))))))))))))))))))))))))))})}}}};
        Typ_all t0_1 t0_2 ->
         case a of {
          Avar_b _ -> false_rec;
          Avar_f a0 ->
           let {s = get_for_sure g0 a0} in
           let {h6 = get_witness a0 g0 s} in
           case h6 of {
            ExistT _ x0 ->
             let {h12 = exposure_impl x0 s} in
             pbind (pMonad_Monad (unsafeCoerce monad_either)) __
               (err_msg (unsafeCoerce h12) ((:) 's' ((:) 'u' ((:) 'b' ((:)
                 't' ((:) 'y' ((:) 'p' ((:) 'e' ((:) 'r' ((:) ':' ((:) ' '
                 ((:) 'e' ((:) 'x' ((:) 'p' ((:) 'o' ((:) 's' ((:) 'u' ((:)
                 'r' ((:) 'e' ((:) ' ' ((:) 'f' ((:) 'a' ((:) 'i' ((:) 'l'
                 ((:) 'e' ((:) 'd' ([]))))))))))))))))))))))))))) (\x1 ->
               case x1 of {
                Typ_bot -> ret (unsafeCoerce monad_either) __;
                Typ_rcd d ->
                 let {o = get0 (Label_typ t1) (decs_to_list' d)} in
                 case o of {
                  Prelude.Just d0 ->
                   case d0 of {
                    Dec_typ _ _ t2 ->
                     let {
                      h24 = subty_dec0 g0 t2 (Typ_all t0_1 t0_2) __ __ __ __
                              __}
                     in
                     pbind (pMonad_Monad (unsafeCoerce monad_either)) __
                       (err_msg h24 ((:) 's' ((:) 'u' ((:) 'b' ((:) 't' ((:)
                         'y' ((:) 'p' ((:) 'e' ((:) 'r' ((:) ':' ((:) ' '
                         ((:) 'r' ((:) 'e' ((:) 'c' ((:) 'u' ((:) 'r' ((:)
                         's' ((:) 'i' ((:) 'v' ((:) 'e' ((:) ' ' ((:) 's'
                         ((:) 'u' ((:) 'b' ((:) 't' ((:) 'y' ((:) 'p' ((:)
                         'i' ((:) 'n' ((:) 'g' ((:) ' ' ((:) 'c' ((:) 'a'
                         ((:) 'l' ((:) 'l' ((:) ' ' ((:) 'f' ((:) 'a' ((:)
                         'i' ((:) 'l' ((:) 'e' ((:) 'd' ((:) '.'
                         ([]))))))))))))))))))))))))))))))))))))))))))))
                       (\_ -> ret (unsafeCoerce monad_either) __);
                    Dec_trm _ _ -> false_rec};
                  Prelude.Nothing ->
                   erret ((:) 's' ((:) 'u' ((:) 'b' ((:) 't' ((:) 'y' ((:)
                     'p' ((:) 'e' ((:) 'r' ((:) ':' ((:) ' ' ((:) 'a' ((:)
                     ' ' ((:) 'l' ((:) 'a' ((:) 'b' ((:) 'e' ((:) 'l' ((:)
                     ' ' ((:) 'i' ((:) 's' ((:) ' ' ((:) 'n' ((:) 'o' ((:)
                     't' ((:) ' ' ((:) 'f' ((:) 'o' ((:) 'u' ((:) 'n' ((:)
                     'd' ([])))))))))))))))))))))))))))))))};
                _ ->
                 erret ((:) 's' ((:) 'u' ((:) 'b' ((:) 't' ((:) 'y' ((:) 'p'
                   ((:) 'e' ((:) 'r' ((:) ':' ((:) ' ' ((:) 'u' ((:) 'n' ((:)
                   'a' ((:) 'b' ((:) 'l' ((:) 'e' ((:) ' ' ((:) 't' ((:) 'o'
                   ((:) ' ' ((:) 'h' ((:) 'a' ((:) 'n' ((:) 'd' ((:) 'l' ((:)
                   'e' ((:) ' ' ((:) 'r' ((:) 'e' ((:) 's' ((:) 'u' ((:) 'l'
                   ((:) 't' ((:) 'i' ((:) 'n' ((:) 'g' ((:) ' ' ((:) 's' ((:)
                   't' ((:) 'r' ((:) 'u' ((:) 'c' ((:) 't' ((:) 'u' ((:) 'r'
                   ((:) 'e'
                   ([])))))))))))))))))))))))))))))))))))))))))))))))})}};
        Typ_rcd d ->
         case a of {
          Avar_b _ -> false_rec;
          Avar_f a0 ->
           let {s = get_for_sure g0 a0} in
           let {h6 = get_witness a0 g0 s} in
           case h6 of {
            ExistT _ x0 ->
             let {h12 = exposure_impl x0 s} in
             pbind (pMonad_Monad (unsafeCoerce monad_either)) __
               (err_msg (unsafeCoerce h12) ((:) 's' ((:) 'u' ((:) 'b' ((:)
                 't' ((:) 'y' ((:) 'p' ((:) 'e' ((:) 'r' ((:) ':' ((:) ' '
                 ((:) 'e' ((:) 'x' ((:) 'p' ((:) 'o' ((:) 's' ((:) 'u' ((:)
                 'r' ((:) 'e' ((:) ' ' ((:) 'f' ((:) 'a' ((:) 'i' ((:) 'l'
                 ((:) 'e' ((:) 'd' ([]))))))))))))))))))))))))))) (\x1 ->
               case x1 of {
                Typ_bot -> ret (unsafeCoerce monad_either) __;
                Typ_rcd d0 ->
                 let {o = get0 (Label_typ t1) (decs_to_list' d0)} in
                 case o of {
                  Prelude.Just d1 ->
                   case d1 of {
                    Dec_typ _ _ t2 ->
                     let {h24 = subty_dec0 g0 t2 (Typ_rcd d) __ __ __ __ __}
                     in
                     pbind (pMonad_Monad (unsafeCoerce monad_either)) __
                       (err_msg h24 ((:) 's' ((:) 'u' ((:) 'b' ((:) 't' ((:)
                         'y' ((:) 'p' ((:) 'e' ((:) 'r' ((:) ':' ((:) ' '
                         ((:) 'r' ((:) 'e' ((:) 'c' ((:) 'u' ((:) 'r' ((:)
                         's' ((:) 'i' ((:) 'v' ((:) 'e' ((:) ' ' ((:) 's'
                         ((:) 'u' ((:) 'b' ((:) 't' ((:) 'y' ((:) 'p' ((:)
                         'i' ((:) 'n' ((:) 'g' ((:) ' ' ((:) 'c' ((:) 'a'
                         ((:) 'l' ((:) 'l' ((:) ' ' ((:) 'f' ((:) 'a' ((:)
                         'i' ((:) 'l' ((:) 'e' ((:) 'd' ((:) '.'
                         ([]))))))))))))))))))))))))))))))))))))))))))))
                       (\_ -> ret (unsafeCoerce monad_either) __);
                    Dec_trm _ _ -> false_rec};
                  Prelude.Nothing ->
                   erret ((:) 's' ((:) 'u' ((:) 'b' ((:) 't' ((:) 'y' ((:)
                     'p' ((:) 'e' ((:) 'r' ((:) ':' ((:) ' ' ((:) 'a' ((:)
                     ' ' ((:) 'l' ((:) 'a' ((:) 'b' ((:) 'e' ((:) 'l' ((:)
                     ' ' ((:) 'i' ((:) 's' ((:) ' ' ((:) 'n' ((:) 'o' ((:)
                     't' ((:) ' ' ((:) 'f' ((:) 'o' ((:) 'u' ((:) 'n' ((:)
                     'd' ([])))))))))))))))))))))))))))))))};
                _ ->
                 erret ((:) 's' ((:) 'u' ((:) 'b' ((:) 't' ((:) 'y' ((:) 'p'
                   ((:) 'e' ((:) 'r' ((:) ':' ((:) ' ' ((:) 'u' ((:) 'n' ((:)
                   'a' ((:) 'b' ((:) 'l' ((:) 'e' ((:) ' ' ((:) 't' ((:) 'o'
                   ((:) ' ' ((:) 'h' ((:) 'a' ((:) 'n' ((:) 'd' ((:) 'l' ((:)
                   'e' ((:) ' ' ((:) 'r' ((:) 'e' ((:) 's' ((:) 'u' ((:) 'l'
                   ((:) 't' ((:) 'i' ((:) 'n' ((:) 'g' ((:) ' ' ((:) 's' ((:)
                   't' ((:) 'r' ((:) 'u' ((:) 'c' ((:) 't' ((:) 'u' ((:) 'r'
                   ((:) 'e'
                   ([])))))))))))))))))))))))))))))))))))))))))))))))})}}};
      Typ_all t1 t2 ->
       case u0 of {
        Typ_top -> ret (unsafeCoerce monad_either) __;
        Typ_sel a t3 ->
         case a of {
          Avar_b _ -> false_rec;
          Avar_f a0 ->
           let {s = get_for_sure g0 a0} in
           let {h6 = get_witness a0 g0 s} in
           case h6 of {
            ExistT _ x0 ->
             let {h12 = exposure_impl x0 s} in
             pbind (pMonad_Monad (unsafeCoerce monad_either)) __
               (err_msg (unsafeCoerce h12) ((:) 's' ((:) 'u' ((:) 'b' ((:)
                 't' ((:) 'y' ((:) 'p' ((:) 'e' ((:) 'r' ((:) ':' ((:) ' '
                 ((:) 'e' ((:) 'x' ((:) 'p' ((:) 'o' ((:) 's' ((:) 'u' ((:)
                 'r' ((:) 'e' ((:) ' ' ((:) 'f' ((:) 'a' ((:) 'i' ((:) 'l'
                 ((:) 'e' ((:) 'd' ([]))))))))))))))))))))))))))) (\x1 ->
               case x1 of {
                Typ_bot -> ret (unsafeCoerce monad_either) __;
                Typ_rcd d ->
                 let {o = get0 (Label_typ t3) (decs_to_list' d)} in
                 case o of {
                  Prelude.Just d0 ->
                   case d0 of {
                    Dec_typ _ t4 _ ->
                     let {
                      h24 = subty_dec0 g0 (Typ_all t1 t2) t4 __ __ __ __ __}
                     in
                     pbind (pMonad_Monad (unsafeCoerce monad_either)) __
                       (err_msg h24 ((:) 's' ((:) 'u' ((:) 'b' ((:) 't' ((:)
                         'y' ((:) 'p' ((:) 'e' ((:) 'r' ((:) ':' ((:) ' '
                         ((:) 'r' ((:) 'e' ((:) 'c' ((:) 'u' ((:) 'r' ((:)
                         's' ((:) 'i' ((:) 'v' ((:) 'e' ((:) ' ' ((:) 's'
                         ((:) 'u' ((:) 'b' ((:) 't' ((:) 'y' ((:) 'p' ((:)
                         'i' ((:) 'n' ((:) 'g' ((:) ' ' ((:) 'c' ((:) 'a'
                         ((:) 'l' ((:) 'l' ((:) ' ' ((:) 'f' ((:) 'a' ((:)
                         'i' ((:) 'l' ((:) 'e' ((:) 'd' ((:) '.'
                         ([]))))))))))))))))))))))))))))))))))))))))))))
                       (\_ -> ret (unsafeCoerce monad_either) __);
                    Dec_trm _ _ -> false_rec};
                  Prelude.Nothing ->
                   erret ((:) 's' ((:) 'u' ((:) 'b' ((:) 't' ((:) 'y' ((:)
                     'p' ((:) 'e' ((:) 'r' ((:) ':' ((:) ' ' ((:) 'a' ((:)
                     ' ' ((:) 'l' ((:) 'a' ((:) 'b' ((:) 'e' ((:) 'l' ((:)
                     ' ' ((:) 'i' ((:) 's' ((:) ' ' ((:) 'n' ((:) 'o' ((:)
                     't' ((:) ' ' ((:) 'f' ((:) 'o' ((:) 'u' ((:) 'n' ((:)
                     'd' ([])))))))))))))))))))))))))))))))};
                _ ->
                 erret ((:) 's' ((:) 'u' ((:) 'b' ((:) 't' ((:) 'y' ((:) 'p'
                   ((:) 'e' ((:) 'r' ((:) ':' ((:) ' ' ((:) 'u' ((:) 'n' ((:)
                   'a' ((:) 'b' ((:) 'l' ((:) 'e' ((:) ' ' ((:) 't' ((:) 'o'
                   ((:) ' ' ((:) 'h' ((:) 'a' ((:) 'n' ((:) 'd' ((:) 'l' ((:)
                   'e' ((:) ' ' ((:) 'r' ((:) 'e' ((:) 's' ((:) 'u' ((:) 'l'
                   ((:) 't' ((:) 'i' ((:) 'n' ((:) 'g' ((:) ' ' ((:) 's' ((:)
                   't' ((:) 'r' ((:) 'u' ((:) 'c' ((:) 't' ((:) 'u' ((:) 'r'
                   ((:) 'e'
                   ([])))))))))))))))))))))))))))))))))))))))))))))))})}};
        Typ_all t3 t4 ->
         let {s = eq_dec eqDecTyp t1 t3} in
         case s of {
          Prelude.True ->
           let {
            s0 = atom_fresh
                   (union1 (dom g0)
                     (union1 (fv_values fv_typ g0)
                       (union1 (fv_typ t2) (union1 (fv_typ t3) (fv_typ t4)))))}
           in
           let {
            h4 = subty_dec0 (app (one ((,) s0 t3)) g0)
                   (open_rec openTyp 0 s0 t2) (open_rec openTyp 0 s0 t4) __
                   __ __ __ __}
           in
           pbind (pMonad_Monad (unsafeCoerce monad_either)) __
             (err_msg h4 ((:) 's' ((:) 'u' ((:) 'b' ((:) 't' ((:) 'y' ((:)
               'p' ((:) 'e' ((:) 'r' ((:) ':' ((:) ' ' ((:) 't' ((:) 'h' ((:)
               'e' ((:) ' ' ((:) 'b' ((:) 'o' ((:) 'd' ((:) 'y' ((:) ' ' ((:)
               't' ((:) 'y' ((:) 'p' ((:) 'e' ((:) 's' ((:) ' ' ((:) 'f' ((:)
               'a' ((:) 'i' ((:) 'l' ((:) 'e' ((:) 'd'
               ([]))))))))))))))))))))))))))))))))) (\_ ->
             ret (unsafeCoerce monad_either) __);
          Prelude.False ->
           erret ((:) 's' ((:) 'u' ((:) 'b' ((:) 't' ((:) 'y' ((:) 'p' ((:)
             'e' ((:) 'r' ((:) ':' ((:) ' ' ((:) 't' ((:) 'h' ((:) 'e' ((:)
             ' ' ((:) 'i' ((:) 'n' ((:) 'p' ((:) 'u' ((:) 't' ((:) ' ' ((:)
             't' ((:) 'y' ((:) 'p' ((:) 'e' ((:) 's' ((:) ' ' ((:) 'd' ((:)
             'o' ((:) ' ' ((:) 'n' ((:) 'o' ((:) 't' ((:) ' ' ((:) 'm' ((:)
             'a' ((:) 't' ((:) 'c' ((:) 'h'
             ([])))))))))))))))))))))))))))))))))))))))};
        _ ->
         erret ((:) 's' ((:) 'u' ((:) 'b' ((:) 't' ((:) 'y' ((:) 'p' ((:) 'e'
           ((:) 'r' ((:) ':' ((:) ' ' ((:) 'u' ((:) 'n' ((:) 'k' ((:) 'n'
           ((:) 'o' ((:) 'w' ((:) 'n' ((:) ' ' ((:) 'h' ((:) 'o' ((:) 'w'
           ((:) ' ' ((:) 't' ((:) 'o' ((:) ' ' ((:) 'c' ((:) 'o' ((:) 'm'
           ((:) 'p' ((:) 'a' ((:) 'r' ((:) 'e' ((:) ' ' ((:) 't' ((:) 'w'
           ((:) 'o' ((:) ' ' ((:) 't' ((:) 'y' ((:) 'p' ((:) 'e' ((:) 's'
           ((:) '.' ([]))))))))))))))))))))))))))))))))))))))))))))};
      Typ_rcd d ->
       case u0 of {
        Typ_top -> ret (unsafeCoerce monad_either) __;
        Typ_sel a t1 ->
         case a of {
          Avar_b _ -> false_rec;
          Avar_f a0 ->
           let {s = get_for_sure g0 a0} in
           let {h6 = get_witness a0 g0 s} in
           case h6 of {
            ExistT _ x0 ->
             let {h12 = exposure_impl x0 s} in
             pbind (pMonad_Monad (unsafeCoerce monad_either)) __
               (err_msg (unsafeCoerce h12) ((:) 's' ((:) 'u' ((:) 'b' ((:)
                 't' ((:) 'y' ((:) 'p' ((:) 'e' ((:) 'r' ((:) ':' ((:) ' '
                 ((:) 'e' ((:) 'x' ((:) 'p' ((:) 'o' ((:) 's' ((:) 'u' ((:)
                 'r' ((:) 'e' ((:) ' ' ((:) 'f' ((:) 'a' ((:) 'i' ((:) 'l'
                 ((:) 'e' ((:) 'd' ([]))))))))))))))))))))))))))) (\x1 ->
               case x1 of {
                Typ_bot -> ret (unsafeCoerce monad_either) __;
                Typ_rcd d0 ->
                 let {o = get0 (Label_typ t1) (decs_to_list' d0)} in
                 case o of {
                  Prelude.Just d1 ->
                   case d1 of {
                    Dec_typ _ t2 _ ->
                     let {h24 = subty_dec0 g0 (Typ_rcd d) t2 __ __ __ __ __}
                     in
                     pbind (pMonad_Monad (unsafeCoerce monad_either)) __
                       (err_msg h24 ((:) 's' ((:) 'u' ((:) 'b' ((:) 't' ((:)
                         'y' ((:) 'p' ((:) 'e' ((:) 'r' ((:) ':' ((:) ' '
                         ((:) 'r' ((:) 'e' ((:) 'c' ((:) 'u' ((:) 'r' ((:)
                         's' ((:) 'i' ((:) 'v' ((:) 'e' ((:) ' ' ((:) 's'
                         ((:) 'u' ((:) 'b' ((:) 't' ((:) 'y' ((:) 'p' ((:)
                         'i' ((:) 'n' ((:) 'g' ((:) ' ' ((:) 'c' ((:) 'a'
                         ((:) 'l' ((:) 'l' ((:) ' ' ((:) 'f' ((:) 'a' ((:)
                         'i' ((:) 'l' ((:) 'e' ((:) 'd' ((:) '.'
                         ([]))))))))))))))))))))))))))))))))))))))))))))
                       (\_ -> ret (unsafeCoerce monad_either) __);
                    Dec_trm _ _ -> false_rec};
                  Prelude.Nothing ->
                   erret ((:) 's' ((:) 'u' ((:) 'b' ((:) 't' ((:) 'y' ((:)
                     'p' ((:) 'e' ((:) 'r' ((:) ':' ((:) ' ' ((:) 'a' ((:)
                     ' ' ((:) 'l' ((:) 'a' ((:) 'b' ((:) 'e' ((:) 'l' ((:)
                     ' ' ((:) 'i' ((:) 's' ((:) ' ' ((:) 'n' ((:) 'o' ((:)
                     't' ((:) ' ' ((:) 'f' ((:) 'o' ((:) 'u' ((:) 'n' ((:)
                     'd' ([])))))))))))))))))))))))))))))))};
                _ ->
                 erret ((:) 's' ((:) 'u' ((:) 'b' ((:) 't' ((:) 'y' ((:) 'p'
                   ((:) 'e' ((:) 'r' ((:) ':' ((:) ' ' ((:) 'u' ((:) 'n' ((:)
                   'a' ((:) 'b' ((:) 'l' ((:) 'e' ((:) ' ' ((:) 't' ((:) 'o'
                   ((:) ' ' ((:) 'h' ((:) 'a' ((:) 'n' ((:) 'd' ((:) 'l' ((:)
                   'e' ((:) ' ' ((:) 'r' ((:) 'e' ((:) 's' ((:) 'u' ((:) 'l'
                   ((:) 't' ((:) 'i' ((:) 'n' ((:) 'g' ((:) ' ' ((:) 's' ((:)
                   't' ((:) 'r' ((:) 'u' ((:) 'c' ((:) 't' ((:) 'u' ((:) 'r'
                   ((:) 'e'
                   ([])))))))))))))))))))))))))))))))))))))))))))))))})}};
        Typ_rcd d0 ->
         case d0 of {
          Decs_nil -> ret (unsafeCoerce monad_either) __;
          Decs_cons d1 d2 ->
           let {h5 = subty_dec0 g0 (Typ_rcd d) (Typ_rcd d2) __ __ __ __ __}
           in
           case d1 of {
            Dec_typ t1 t2 t3 ->
             let {o = get0 (Label_typ t1) (decs_to_list' d)} in
             case o of {
              Prelude.Just d3 ->
               case d3 of {
                Dec_typ _ t4 t5 ->
                 let {h12 = subty_dec0 g0 t2 t4 __ __ __ __ __} in
                 let {h13 = subty_dec0 g0 t5 t3 __ __ __ __ __} in
                 pbind (pMonad_Monad (unsafeCoerce monad_either)) __
                   (err_msg h12 ((:) 's' ((:) 'u' ((:) 'b' ((:) 't' ((:) 'y'
                     ((:) 'p' ((:) 'e' ((:) 'r' ((:) ':' ((:) ' ' ((:) 'l'
                     ((:) 'o' ((:) 'w' ((:) 'e' ((:) 'r' ((:) ' ' ((:) 'b'
                     ((:) 'o' ((:) 'u' ((:) 'n' ((:) 'd' ((:) ' ' ((:) 'c'
                     ((:) 'o' ((:) 'm' ((:) 'p' ((:) 'a' ((:) 'r' ((:) 'i'
                     ((:) 's' ((:) 'o' ((:) 'n' ((:) ' ' ((:) 'f' ((:) 'a'
                     ((:) 'i' ((:) 'l' ((:) 'e' ((:) 'd'
                     ([]))))))))))))))))))))))))))))))))))))))))) (\_ ->
                   pbind (pMonad_Monad (unsafeCoerce monad_either)) __
                     (err_msg h13 ((:) 's' ((:) 'u' ((:) 'b' ((:) 't' ((:)
                       'y' ((:) 'p' ((:) 'e' ((:) 'r' ((:) ':' ((:) ' ' ((:)
                       'u' ((:) 'p' ((:) 'p' ((:) 'e' ((:) 'r' ((:) ' ' ((:)
                       'b' ((:) 'o' ((:) 'u' ((:) 'n' ((:) 'd' ((:) ' ' ((:)
                       'c' ((:) 'o' ((:) 'm' ((:) 'p' ((:) 'a' ((:) 'r' ((:)
                       'i' ((:) 's' ((:) 'o' ((:) 'n' ((:) ' ' ((:) 'f' ((:)
                       'a' ((:) 'i' ((:) 'l' ((:) 'e' ((:) 'd'
                       ([]))))))))))))))))))))))))))))))))))))))))) (\_ ->
                     pbind (pMonad_Monad (unsafeCoerce monad_either)) __
                       (err_msg h5 ((:) 's' ((:) 'u' ((:) 'b' ((:) 't' ((:)
                         'y' ((:) 'p' ((:) 'e' ((:) 'r' ((:) ':' ((:) ' '
                         ((:) 'r' ((:) 'e' ((:) 'c' ((:) 'u' ((:) 'r' ((:)
                         's' ((:) 'i' ((:) 'v' ((:) 'e' ((:) ' ' ((:) 'c'
                         ((:) 'o' ((:) 'm' ((:) 'p' ((:) 'a' ((:) 'r' ((:)
                         'i' ((:) 's' ((:) 'o' ((:) 'n' ((:) ' ' ((:) 'f'
                         ((:) 'o' ((:) 'r' ((:) ' ' ((:) 'd' ((:) 'e' ((:)
                         'c' ((:) 'l' ((:) 'a' ((:) 'r' ((:) 'a' ((:) 't'
                         ((:) 'i' ((:) 'o' ((:) 'n' ((:) 's' ((:) ' ' ((:)
                         'f' ((:) 'a' ((:) 'i' ((:) 'l' ((:) 'e' ((:) 'd'
                         ([]))))))))))))))))))))))))))))))))))))))))))))))))))))))))
                       (\_ -> ret (unsafeCoerce monad_either) __)));
                Dec_trm _ _ -> false_rec};
              Prelude.Nothing ->
               erret ((:) 's' ((:) 'u' ((:) 'b' ((:) 't' ((:) 'y' ((:) 'p'
                 ((:) 'e' ((:) 'r' ((:) ':' ((:) ' ' ((:) 'a' ((:) ' ' ((:)
                 'l' ((:) 'a' ((:) 'b' ((:) 'e' ((:) 'l' ((:) ' ' ((:) 'i'
                 ((:) 's' ((:) ' ' ((:) 'n' ((:) 'o' ((:) 't' ((:) ' ' ((:)
                 'f' ((:) 'o' ((:) 'u' ((:) 'n' ((:) 'd'
                 ([])))))))))))))))))))))))))))))))};
            Dec_trm t1 t2 ->
             let {o = get0 (Label_trm t1) (decs_to_list' d)} in
             case o of {
              Prelude.Just d3 ->
               case d3 of {
                Dec_typ _ _ _ -> false_rec;
                Dec_trm _ t3 ->
                 let {h10 = subty_dec0 g0 t3 t2 __ __ __ __ __} in
                 pbind (pMonad_Monad (unsafeCoerce monad_either)) __
                   (err_msg h10 ((:) 's' ((:) 'u' ((:) 'b' ((:) 't' ((:) 'y'
                     ((:) 'p' ((:) 'e' ((:) 'r' ((:) ':' ((:) ' ' ((:) 'm'
                     ((:) 'e' ((:) 'm' ((:) 'b' ((:) 'e' ((:) 'r' ((:) ' '
                     ((:) 't' ((:) 'y' ((:) 'p' ((:) 'e' ((:) ' ' ((:) 'c'
                     ((:) 'o' ((:) 'm' ((:) 'p' ((:) 'a' ((:) 'r' ((:) 'i'
                     ((:) 's' ((:) 'o' ((:) 'n' ((:) ' ' ((:) 'f' ((:) 'a'
                     ((:) 'i' ((:) 'l' ((:) 'e' ((:) 'd'
                     ([]))))))))))))))))))))))))))))))))))))))))) (\_ ->
                   pbind (pMonad_Monad (unsafeCoerce monad_either)) __
                     (err_msg h5 ((:) 's' ((:) 'u' ((:) 'b' ((:) 't' ((:) 'y'
                       ((:) 'p' ((:) 'e' ((:) 'r' ((:) ':' ((:) ' ' ((:) 'r'
                       ((:) 'e' ((:) 'c' ((:) 'u' ((:) 'r' ((:) 's' ((:) 'i'
                       ((:) 'v' ((:) 'e' ((:) ' ' ((:) 'c' ((:) 'o' ((:) 'm'
                       ((:) 'p' ((:) 'a' ((:) 'r' ((:) 'i' ((:) 's' ((:) 'o'
                       ((:) 'n' ((:) ' ' ((:) 'f' ((:) 'o' ((:) 'r' ((:) ' '
                       ((:) 'd' ((:) 'e' ((:) 'c' ((:) 'l' ((:) 'a' ((:) 'r'
                       ((:) 'a' ((:) 't' ((:) 'i' ((:) 'o' ((:) 'n' ((:) 's'
                       ((:) ' ' ((:) 'f' ((:) 'a' ((:) 'i' ((:) 'l' ((:) 'e'
                       ((:) 'd'
                       ([]))))))))))))))))))))))))))))))))))))))))))))))))))))))))
                     (\_ -> ret (unsafeCoerce monad_either) __))};
              Prelude.Nothing ->
               erret ((:) 's' ((:) 'u' ((:) 'b' ((:) 't' ((:) 'y' ((:) 'p'
                 ((:) 'e' ((:) 'r' ((:) ':' ((:) ' ' ((:) 'a' ((:) ' ' ((:)
                 'l' ((:) 'a' ((:) 'b' ((:) 'e' ((:) 'l' ((:) ' ' ((:) 'i'
                 ((:) 's' ((:) ' ' ((:) 'n' ((:) 'o' ((:) 't' ((:) ' ' ((:)
                 'f' ((:) 'o' ((:) 'u' ((:) 'n' ((:) 'd'
                 ([])))))))))))))))))))))))))))))))}}};
        _ ->
         erret ((:) 's' ((:) 'u' ((:) 'b' ((:) 't' ((:) 'y' ((:) 'p' ((:) 'e'
           ((:) 'r' ((:) ':' ((:) ' ' ((:) 'u' ((:) 'n' ((:) 'k' ((:) 'n'
           ((:) 'o' ((:) 'w' ((:) 'n' ((:) ' ' ((:) 'h' ((:) 'o' ((:) 'w'
           ((:) ' ' ((:) 't' ((:) 'o' ((:) ' ' ((:) 'c' ((:) 'o' ((:) 'm'
           ((:) 'p' ((:) 'a' ((:) 'r' ((:) 'e' ((:) ' ' ((:) 't' ((:) 'w'
           ((:) 'o' ((:) ' ' ((:) 't' ((:) 'y' ((:) 'p' ((:) 'e' ((:) 's'
           ((:) '.' ([]))))))))))))))))))))))))))))))))))))))))))))}})}
  in fix_F_sub (ExistT g (ExistT t u)) __ __ __ __ __

typing_impl_template :: Subtyp_checker -> (([]) ((,) Atom Typ)) -> Trm ->
                        Prelude.Either (([]) Prelude.String) Typ
typing_impl_template subtyper g t =
  let {
   fix_F_sub x =
     let {subtyper0 = projT1 x} in
     let {g0 = projT1 (projT2 x)} in
     let {t0 = projT2 (projT2 x)} in
     let {
      typing_impl_template0 = \subtyper1 g1 t1 ->
       fix_F_sub (ExistT subtyper1 (ExistT g1 t1))}
     in
     (\_ _ _ _ ->
     let {typing_impl = \g1 t1 -> typing_impl_template0 subtyper0 g1 t1} in
     case t0 of {
      Trm_var a ->
       case a of {
        Avar_b _ -> false_rec;
        Avar_f a0 ->
         let {s = get_for_sure g0 a0} in ret (unsafeCoerce monad_either) s};
      Trm_val v ->
       case v of {
        Val_rcd d ->
         let {
          h3 = \h3 ->
           let {
            f d0 =
              case d0 of {
               Defs_nil -> ret monad_either Decs_nil;
               Defs_cons d1 d2 ->
                let {h8 = f d2} in
                case d1 of {
                 Def_typ t1 t2 ->
                  pbind (pMonad_Monad monad_either) __
                    (err_msg h8 ((:) 't' ((:) 'y' ((:) 'p' ((:) 'e' ((:) 'r'
                      ((:) ':' ((:) ' ' ((:) 'r' ((:) 'e' ((:) 'c' ((:) 'u'
                      ((:) 'r' ((:) 's' ((:) 'i' ((:) 'v' ((:) 'e' ((:) ' '
                      ((:) 'c' ((:) 'a' ((:) 'l' ((:) 'l' ((:) ' ' ((:) 'f'
                      ((:) 'o' ((:) 'r' ((:) ' ' ((:) 'a' ((:) ' ' ((:) 'r'
                      ((:) 'e' ((:) 'c' ((:) 'o' ((:) 'r' ((:) 'd' ((:) ' '
                      ((:) 'f' ((:) 'a' ((:) 'i' ((:) 'l' ((:) 'e' ((:) 'd'
                      ([]))))))))))))))))))))))))))))))))))))))))))) (\x0 ->
                    ret monad_either (Decs_cons (Dec_typ t1 t2 t2) x0));
                 Def_trm t1 t2 ->
                  let {h9 = typing_impl g0 t2 __ __ __ __} in
                  pbind (pMonad_Monad monad_either) __
                    (err_msg (unsafeCoerce h9) ((:) 't' ((:) 'y' ((:) 'p'
                      ((:) 'e' ((:) 'r' ((:) ':' ((:) ' ' ((:) 'f' ((:) 'a'
                      ((:) 'i' ((:) 'l' ((:) 'e' ((:) 'd' ((:) ' ' ((:) 't'
                      ((:) 'o' ((:) ' ' ((:) 't' ((:) 'y' ((:) 'p' ((:) 'e'
                      ((:) ' ' ((:) 'c' ((:) 'h' ((:) 'e' ((:) 'c' ((:) 'k'
                      ((:) ' ' ((:) 'a' ((:) ' ' ((:) 'd' ((:) 'a' ((:) 't'
                      ((:) 'a' ((:) ' ' ((:) 'm' ((:) 'e' ((:) 'm' ((:) 'b'
                      ((:) 'e' ((:) 'r'
                      ([]))))))))))))))))))))))))))))))))))))))))))) (\x0 ->
                    pbind (pMonad_Monad monad_either) __
                      (err_msg h8 ((:) 't' ((:) 'y' ((:) 'p' ((:) 'e' ((:)
                        'r' ((:) ':' ((:) ' ' ((:) 'r' ((:) 'e' ((:) 'c' ((:)
                        'u' ((:) 'r' ((:) 's' ((:) 'i' ((:) 'v' ((:) 'e' ((:)
                        ' ' ((:) 'c' ((:) 'a' ((:) 'l' ((:) 'l' ((:) ' ' ((:)
                        'f' ((:) 'o' ((:) 'r' ((:) ' ' ((:) 'a' ((:) ' ' ((:)
                        'r' ((:) 'e' ((:) 'c' ((:) 'o' ((:) 'r' ((:) 'd' ((:)
                        ' ' ((:) 'f' ((:) 'a' ((:) 'i' ((:) 'l' ((:) 'e' ((:)
                        'd' ([])))))))))))))))))))))))))))))))))))))))))))
                      (\x1 ->
                      ret monad_either (Decs_cons (Dec_trm t1 x0) x1)))}}}
           in f h3}
         in
         let {h5 = h3 d} in
         pbind (pMonad_Monad (unsafeCoerce monad_either)) __
           (err_msg (unsafeCoerce h5) ((:) 't' ((:) 'y' ((:) 'p' ((:) 'e'
             ((:) 'r' ((:) ':' ((:) ' ' ((:) 'r' ((:) 'e' ((:) 'c' ((:) 'o'
             ((:) 'r' ((:) 'd' ((:) ' ' ((:) 'f' ((:) 'a' ((:) 'i' ((:) 'l'
             ((:) 'e' ((:) 'd' ([])))))))))))))))))))))) (\x0 ->
           ret (unsafeCoerce monad_either) (Typ_rcd x0));
        Val_lam t1 t2 ->
         let {
          s = atom_fresh
                (union1 (dom g0)
                  (union1 (fv_values fv_typ g0)
                    (union1 (fv_typ t1) (fv_trm t2))))}
         in
         let {
          h3 = typing_impl (app (one ((,) s t1)) g0)
                 (open_rec openTrm 0 s t2) __ __ __ __}
         in
         pbind (pMonad_Monad (unsafeCoerce monad_either)) __
           (err_msg h3 ((:) 't' ((:) 'y' ((:) 'p' ((:) 'e' ((:) 'r' ((:) ':'
             ((:) ' ' ((:) 'f' ((:) 'u' ((:) 'n' ((:) 'c' ((:) 't' ((:) 'i'
             ((:) 'o' ((:) 'n' ((:) ' ' ((:) 'b' ((:) 'o' ((:) 'd' ((:) 'y'
             ((:) ' ' ((:) 'f' ((:) 'a' ((:) 'i' ((:) 'l' ((:) 'e' ((:) 'd'
             ((:) '.' ([])))))))))))))))))))))))))))))) (\x0 ->
           ret (unsafeCoerce monad_either) (Typ_all t1
             (close_rec closeTyp 0 s x0)))};
      Trm_sel a t1 ->
       case a of {
        Avar_b _ -> false_rec;
        Avar_f a0 ->
         let {s = get_for_sure g0 a0} in
         let {h5 = get_witness a0 g0 s} in
         case h5 of {
          ExistT _ x0 ->
           let {h11 = exposure_impl x0 s} in
           pbind (pMonad_Monad (unsafeCoerce monad_either)) __
             (err_msg h11 ((:) 't' ((:) 'y' ((:) 'p' ((:) 'e' ((:) 'r' ((:)
               ':' ((:) ' ' ((:) 'e' ((:) 'x' ((:) 'p' ((:) 'o' ((:) 's' ((:)
               'u' ((:) 'r' ((:) 'e' ((:) ' ' ((:) 'f' ((:) 'a' ((:) 'i' ((:)
               'l' ((:) 'e' ((:) 'd' ((:) ' ' ((:) 'f' ((:) 'o' ((:) 'r' ((:)
               ' ' ((:) 'm' ((:) 'e' ((:) 'm' ((:) 'b' ((:) 'e' ((:) 'r' ((:)
               ' ' ((:) 's' ((:) 'e' ((:) 'l' ((:) 'e' ((:) 'c' ((:) 't' ((:)
               'i' ((:) 'o' ((:) 'n'
               ([]))))))))))))))))))))))))))))))))))))))))))))) (\x1 ->
             case x1 of {
              Typ_bot -> ret (unsafeCoerce monad_either) Typ_bot;
              Typ_rcd d ->
               let {o = get0 (Label_trm t1) (decs_to_list' d)} in
               case o of {
                Prelude.Just d0 ->
                 case d0 of {
                  Dec_typ _ _ _ -> false_rec;
                  Dec_trm _ t2 -> ret (unsafeCoerce monad_either) t2};
                Prelude.Nothing ->
                 erret ((:) 't' ((:) 'y' ((:) 'p' ((:) 'e' ((:) 'r' ((:) ':'
                   ((:) ' ' ((:) 'a' ((:) ' ' ((:) 'l' ((:) 'a' ((:) 'b' ((:)
                   'e' ((:) 'l' ((:) ' ' ((:) 'i' ((:) 's' ((:) ' ' ((:) 'n'
                   ((:) 'o' ((:) 't' ((:) ' ' ((:) 'f' ((:) 'o' ((:) 'u' ((:)
                   'n' ((:) 'd' ([]))))))))))))))))))))))))))))};
              _ ->
               erret ((:) 't' ((:) 'y' ((:) 'p' ((:) 'e' ((:) 'r' ((:) ':'
                 ((:) ' ' ((:) 't' ((:) 'r' ((:) 'i' ((:) 'e' ((:) 'd' ((:)
                 ' ' ((:) 't' ((:) 'o' ((:) ' ' ((:) 'l' ((:) 'o' ((:) 'o'
                 ((:) 'k' ((:) 'u' ((:) 'p' ((:) ' ' ((:) 'n' ((:) 'o' ((:)
                 'n' ((:) '-' ((:) 'r' ((:) 'e' ((:) 'c' ((:) 'o' ((:) 'r'
                 ((:) 'd' ((:) ' ' ((:) 't' ((:) 'y' ((:) 'p' ((:) 'e'
                 ([])))))))))))))))))))))))))))))))))))))))})}};
      Trm_app a a0 ->
       case a of {
        Avar_b _ -> false_rec;
        Avar_f a1 ->
         case a0 of {
          Avar_b _ -> false_rec;
          Avar_f a2 ->
           let {s = get_for_sure g0 a1} in
           let {h5 = get_witness a1 g0 s} in
           case h5 of {
            ExistT _ x0 ->
             let {h11 = exposure_impl x0 s} in
             pbind (pMonad_Monad (unsafeCoerce monad_either)) __
               (err_msg h11 ((:) 't' ((:) 'y' ((:) 'p' ((:) 'e' ((:) 'r' ((:)
                 ':' ((:) ' ' ((:) 'e' ((:) 'x' ((:) 'p' ((:) 'o' ((:) 's'
                 ((:) 'u' ((:) 'r' ((:) 'e' ((:) ' ' ((:) 'f' ((:) 'a' ((:)
                 'i' ((:) 'l' ((:) 'e' ((:) 'd' ((:) ' ' ((:) 'i' ((:) 'n'
                 ((:) ' ' ((:) 'a' ((:) 'p' ((:) 'p' ((:) 'l' ((:) 'i' ((:)
                 'c' ((:) 'a' ((:) 't' ((:) 'i' ((:) 'o' ((:) 'n' ((:) ' '
                 ((:) 'c' ((:) 'a' ((:) 's' ((:) 'e'
                 ([])))))))))))))))))))))))))))))))))))))))))))) (\x1 ->
               let {s0 = get_for_sure g0 a2} in
               case x1 of {
                Typ_bot ->
                 ret (unsafeCoerce monad_either)
                   (open_rec openTyp 0 a2 Typ_bot);
                Typ_all t1 t2 ->
                 let {h26 = subtyper0 g0 s0 t1 __ __ __ __ __} in
                 pbind (pMonad_Monad (unsafeCoerce monad_either)) __
                   (err_msg (unsafeCoerce h26) ((:) 't' ((:) 'y' ((:) 'p'
                     ((:) 'e' ((:) 'r' ((:) ':' ((:) ' ' ((:) 'i' ((:) 'n'
                     ((:) 'p' ((:) 'u' ((:) 't' ((:) ' ' ((:) 's' ((:) 'u'
                     ((:) 'b' ((:) 't' ((:) 'y' ((:) 'p' ((:) 'i' ((:) 'n'
                     ((:) 'g' ((:) ' ' ((:) 'f' ((:) 'a' ((:) 'i' ((:) 'l'
                     ((:) 'e' ((:) 'd' ((:) ' ' ((:) 'i' ((:) 'n' ((:) ' '
                     ((:) 'a' ((:) 'p' ((:) 'p' ((:) 'l' ((:) 'i' ((:) 'c'
                     ((:) 'a' ((:) 't' ((:) 'i' ((:) 'o' ((:) 'n' ((:) ' '
                     ((:) 'c' ((:) 'a' ((:) 's' ((:) 'e'
                     ([])))))))))))))))))))))))))))))))))))))))))))))))))))
                   (\_ ->
                   ret (unsafeCoerce monad_either) (open_rec openTyp 0 a2 t2));
                _ ->
                 erret ((:) 't' ((:) 'y' ((:) 'p' ((:) 'e' ((:) 'r' ((:) ':'
                   ((:) ' ' ((:) 'n' ((:) 'o' ((:) 't' ((:) ' ' ((:) 'a' ((:)
                   ' ' ((:) 'f' ((:) 'u' ((:) 'n' ((:) 'c' ((:) 't' ((:) 'i'
                   ((:) 'o' ((:) 'n' ((:) ' ' ((:) 'i' ((:) 'n' ((:) ' ' ((:)
                   'a' ((:) 'p' ((:) 'p' ((:) 'l' ((:) 'i' ((:) 'c' ((:) 'a'
                   ((:) 't' ((:) 'i' ((:) 'o' ((:) 'n' ((:) ' ' ((:) 'c' ((:)
                   'a' ((:) 's' ((:) 'e'
                   ([]))))))))))))))))))))))))))))))))))))))))))})}}};
      Trm_let t1 t2 ->
       let {h3 = typing_impl g0 t1 __ __ __ __} in
       pbind (pMonad_Monad (unsafeCoerce monad_either)) __
         (err_msg h3 ((:) 't' ((:) 'y' ((:) 'p' ((:) 'e' ((:) 'r' ((:) ':'
           ((:) ' ' ((:) 'l' ((:) 'e' ((:) 't' ((:) ' ' ((:) 'b' ((:) 'i'
           ((:) 'n' ((:) 'd' ((:) 'i' ((:) 'n' ((:) 'g' ((:) ' ' ((:) 't'
           ((:) 'y' ((:) 'p' ((:) 'e' ((:) ' ' ((:) 'f' ((:) 'a' ((:) 'i'
           ((:) 'l' ((:) 'e' ((:) 'd' ([]))))))))))))))))))))))))))))))))
         (\x0 ->
         let {
          s = atom_fresh
                (union1 (dom g0)
                  (union1 (fv_values fv_typ g0)
                    (union1 (fv_typ x0) (union1 (fv_trm t1) (fv_trm t2)))))}
         in
         let {
          h7 = typing_impl (app (one ((,) s x0)) g0)
                 (open_rec openTrm 0 s t2) __ __ __ __}
         in
         pbind (pMonad_Monad (unsafeCoerce monad_either)) __
           (err_msg h7 ((:) 't' ((:) 'y' ((:) 'p' ((:) 'e' ((:) 'r' ((:) ':'
             ((:) ' ' ((:) 'l' ((:) 'e' ((:) 't' ((:) ' ' ((:) 'b' ((:) 'o'
             ((:) 'd' ((:) 'y' ((:) ' ' ((:) 't' ((:) 'y' ((:) 'p' ((:) 'e'
             ((:) ' ' ((:) 'f' ((:) 'a' ((:) 'i' ((:) 'l' ((:) 'e' ((:) 'd'
             ([]))))))))))))))))))))))))))))) (\x1 ->
           let {h11 = promotion (app (one ((,) s x0)) g0) s x1} in
           pbind (pMonad_Monad (unsafeCoerce monad_either)) __
             (err_msg h11 ((:) 't' ((:) 'y' ((:) 'p' ((:) 'e' ((:) 'r' ((:)
               ':' ((:) ' ' ((:) 'p' ((:) 'r' ((:) 'o' ((:) 'm' ((:) 'o' ((:)
               't' ((:) 'e' ((:) ' ' ((:) 'b' ((:) 'o' ((:) 'd' ((:) 'y' ((:)
               ' ' ((:) 't' ((:) 'y' ((:) 'p' ((:) 'e' ((:) ' ' ((:) 'f' ((:)
               'a' ((:) 'i' ((:) 'l' ((:) 'e' ((:) 'd'
               ([]))))))))))))))))))))))))))))))))) (\x2 ->
             ret (unsafeCoerce monad_either) x2)))})}
  in fix_F_sub (ExistT subtyper (ExistT g t)) __ __ __ __

typecheck_template' :: Subtyp_checker -> Trm -> Prelude.Either
                       (([]) Prelude.String) Typ
typecheck_template' subtyper t =
  typing_impl_template subtyper ([]) t

typecheck_template :: Subtyp_checker -> Trm -> Prelude.Either
                      (([]) Prelude.String) Typ
typecheck_template subtyper t =
  case check_fv_trm t of {
   Prelude.True ->
    case check_lc_at_trm 0 t of {
     Prelude.True ->
      case uniq_dom_trm_dec t of {
       Prelude.True -> typecheck_template' subtyper t;
       Prelude.False ->
        erret ((:) 's' ((:) 'a' ((:) 'n' ((:) 'i' ((:) 't' ((:) 'y' ((:) ':'
          ((:) ' ' ((:) 'i' ((:) 'n' ((:) 'p' ((:) 'u' ((:) 't' ((:) ' ' ((:)
          't' ((:) 'e' ((:) 'r' ((:) 'm' ((:) ' ' ((:) 'd' ((:) 'o' ((:) 'e'
          ((:) 's' ((:) ' ' ((:) 'n' ((:) 'o' ((:) 't' ((:) ' ' ((:) 'h' ((:)
          'a' ((:) 'v' ((:) 'e' ((:) ' ' ((:) 'u' ((:) 'n' ((:) 'i' ((:) 'q'
          ((:) 'u' ((:) 'e' ((:) ' ' ((:) 'd' ((:) 'o' ((:) 'm' ((:) 'a' ((:)
          'i' ((:) 'n' ((:) '.'
          ([]))))))))))))))))))))))))))))))))))))))))))))))))};
     Prelude.False ->
      erret ((:) 's' ((:) 'a' ((:) 'n' ((:) 'i' ((:) 't' ((:) 'y' ((:) ':'
        ((:) ' ' ((:) 'i' ((:) 'n' ((:) 'p' ((:) 'u' ((:) 't' ((:) ' ' ((:)
        't' ((:) 'e' ((:) 'r' ((:) 'm' ((:) ' ' ((:) 'i' ((:) 's' ((:) ' '
        ((:) 'n' ((:) 'o' ((:) 't' ((:) ' ' ((:) 'l' ((:) 'o' ((:) 'c' ((:)
        'a' ((:) 'l' ((:) 'l' ((:) 'y' ((:) ' ' ((:) 'c' ((:) 'l' ((:) 'o'
        ((:) 's' ((:) 'e' ((:) 'd' ((:) '.'
        ([]))))))))))))))))))))))))))))))))))))))))))};
   Prelude.False ->
    erret ((:) 's' ((:) 'a' ((:) 'n' ((:) 'i' ((:) 't' ((:) 'y' ((:) ':' ((:)
      ' ' ((:) 'i' ((:) 'n' ((:) 'p' ((:) 'u' ((:) 't' ((:) ' ' ((:) 't' ((:)
      'e' ((:) 'r' ((:) 'm' ((:) ' ' ((:) 'i' ((:) 's' ((:) ' ' ((:) 'n' ((:)
      'o' ((:) 't' ((:) ' ' ((:) 'c' ((:) 'l' ((:) 'o' ((:) 's' ((:) 'e' ((:)
      'd' ((:) '.' ([]))))))))))))))))))))))))))))))))))}

typecheck :: Trm -> Prelude.Either (([]) Prelude.String) Typ
typecheck t =
  typecheck_template (\x x0 x1 _ _ _ _ _ -> subty_dec x x0 x1) t

