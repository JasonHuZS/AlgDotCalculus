Require Import Definitions.
Require Import Helpers.

Reserved Notation "G '⊢#' t '⦂' T" (at level 70, t at level 79).
Reserved Notation "G '⊢#' T '<⦂' U" (at level 70, T at level 79).


Inductive tyt_trm : env -> trm -> typ -> Prop :=
| tyt_var : forall G x T,
    binds x T G ->
    G ⊢# trm_var x ⦂ T

| tyt_all_intro : forall L G T t U,
    (forall x, x `notin` L ->
          x ~ T ++ G ⊢ open x t ⦂ open x U) ->
    G ⊢# trm_val (λ( T ){ t }) ⦂ all( T ) U
| tyt_all_elim : forall G (x z : atom) S T,
    G ⊢# trm_var x ⦂ all( S ) T ->
    G ⊢# trm_var z ⦂ S ->
    G ⊢# (trm_app x z) ⦂ open z T

| tyt_rcd_intro : forall G ds DS,
    G ⊩[ ds ⦂ DS ] ->
    wf_defs ds ->
    G ⊢# trm_val ({[ ds ]}) ⦂ {< DS >}
| tyt_rcd_elim : forall G (x : var) a T,
    G ⊢# trm_var x ⦂ {< decs_cons (dec_trm a T) decs_nil >} ->
    G ⊢# trm_sel x a ⦂ T

| tyt_let : forall L G t u T U,
    G ⊢# t ⦂ T ->
    (forall x, x `notin` L ->
          x ~ T ++ G ⊢ open x u ⦂ U) ->
    G ⊢# lett t inn u ⦂ U
| tyt_sub : forall G t T T',
    G ⊢# t ⦂ T ->
    G ⊢# T <⦂ T' ->
    G ⊢# t ⦂ T'
where "G ⊢# t ⦂ T" := (tyt_trm G t T) : type_scope

with
subtypt : env -> typ -> typ -> Prop :=
| subtypt_top : forall G T,
    G ⊢# T <⦂ ⊤
| subtypt_bot : forall G T,
    G ⊢# ⊥ <⦂ T
| subtypt_refl : forall G T,
    G ⊢# T <⦂ T
| subtypt_trans : forall G S T U,
    G ⊢# S <⦂ T ->
    G ⊢# T <⦂ U ->
    G ⊢# S <⦂ U

| subtypt_all: forall L G S1 T1 S2 T2,
    G ⊢# S2 <⦂ S1 ->
    (forall x, x `notin` L ->
       x ~ S2 ++ G ⊢ open x T1 <⦂ open x T2) ->
    G ⊢# all(S1) T1 <⦂ all(S2) T2

| subtypt_fld : forall G a T U,
    G ⊢# T <⦂ U ->
    G ⊢# {< decs_cons (dec_trm a T) decs_nil >}
      <⦂ {< decs_cons (dec_trm a U) decs_nil >} (* DS[a := U] *)
| subtypt_typ : forall G A S1 T1 S2 T2,
    G ⊢# S2 <⦂ S1 ->
    G ⊢# T1 <⦂ T2 ->
    G ⊢# {< decs_cons (dec_typ A S1 T1) decs_nil >}
      <⦂ {< decs_cons (dec_typ A S2 T2) decs_nil >}
(* DS[A := S2 .. T2] *)

| subtypt_drop1 : forall G (DS1 : decs) (DS2 : decs),
    not_empty DS1 ->
    G ⊢# {< append' DS1 DS2 >} <⦂ {< DS2 >}
| subtypt_drop2 : forall G (DS1 : decs) (DS2 : decs),
    not_empty DS2 ->
    G ⊢# {< append' DS1 DS2 >} <⦂ {< DS1 >}
| subtypt_merge : forall G (DS : decs) (DS1 : decs) (DS2 : decs),
    G ⊢# {< DS >} <⦂ {< DS1 >} ->
    G ⊢# {< DS >} <⦂ {< DS2 >} ->
    G ⊢# {< DS >} <⦂ {< append' DS1 DS2 >}
                    
| subtypt_sel1 : forall G (x : var) A T DS,
    binds x ({< DS >}) G ->
    lbinds (label_typ A) (dec_typ A T T) DS ->
    G ⊢# T <⦂ x ⋅ A
| subtypt_sel2 : forall G (x : var) A T DS,
    binds x ({< DS >}) G ->
    lbinds (label_typ A) (dec_typ A T T) DS ->
    G ⊢# x ⋅ A <⦂ T
where "G ⊢# T <⦂ U" := (subtypt G T U) : type_scope.
Hint Constructors tyt_trm subtypt.

Scheme typt_trm_mut := Induction for tyt_trm Sort Prop
  with subtypt_mut := Induction for subtypt Sort Prop.
Combined Scheme typt_mut from typt_trm_mut, subtypt_mut.


Ltac mut_ind_2 ::=
  match goal with
  | [ |- (forall (G : env) (t : trm) (T : typ) (_ : G ⊢# t ⦂ T), _) /\
        (forall (G0 : env) (S U : typ) (_ : G0 ⊢# S <⦂ U), _) ] =>
    apply typt_mut
  end.

Local Hint Resolve decs_bind_typ.

Lemma tight_to_general :
  (forall G t T, G ⊢# t ⦂ T ->
            G ⊢ t ⦂ T) /\
  (forall G S U, G ⊢# S <⦂ U ->
            G ⊢ S <⦂ U).
Proof. mutual induction; eroutine. Qed.