Require Export TypeCheck.
Require Export TypeCheck2.
Require Export StdSetup.
Require Export Definitions.

Extraction Inline exposure_impl_func.
Extraction Inline TypeCheck.exposure_impl_func_obligation_1.
Extraction Inline TypeCheck.exposure_impl_func_obligation_2.

Extraction Inline subty_dec_func.
Extraction Inline TypeCheck.subty_dec_func_obligation_1.
Extraction Inline TypeCheck.subty_dec_func_obligation_2.

Extraction Inline typing_impl_template_func.
Extraction Inline TypeCheck.typing_impl_template_func_obligation_2.
Extraction Inline TypeCheck.typing_impl_template_func_obligation_1.

Extraction Inline subty_equiv_dec_func.
Extraction Inline TypeCheck2.subty_equiv_dec_func_obligation_1.
Extraction Inline TypeCheck2.subty_equiv_dec_func_obligation_2.

Extraction Inline def_rect defs_rec defs_rect.
Extraction Inline dec_rec dec_rect decs_rec decs_rect.
Extraction Inline typ_rect typ_rec.
Extraction Inline avar_rect avar_rec.
Extraction Inline sumbool_rect.
