Require Import Definitions.

Fixpoint typ_struct_measure (T : typ) :=
  match T with
  | typ_top => 1
  | typ_bot => 1
  | typ_sel _ _ => 1
  | typ_all T U => 1 + typ_struct_measure T + typ_struct_measure U
  | typ_rcd DS => 1 + decs_struct_measure DS
  end
with
dec_struct_measure (D : dec) :=
  match D with
  | dec_typ _ T U => 1 + typ_struct_measure T + typ_struct_measure U
  | dec_trm _ T => 1 + typ_struct_measure T
  end
with
decs_struct_measure (DS : decs) :=
  match DS with
  | decs_nil => 1
  | decs_cons D DS' => 1 + dec_struct_measure D + decs_struct_measure DS'
  end.

Fixpoint trm_struct_measure (t : trm) :=
  match t with
  | trm_var _ => 1
  | trm_val vl => val_struct_measure vl
  | trm_sel _ _ => 1
  | trm_app _ _ => 1
  | trm_let s t => 1 + trm_struct_measure s + trm_struct_measure t
  end
with
val_struct_measure (vl : val) :=
  match vl with
  | val_rcd ds => 1 + defs_struct_measure ds
  | val_lam T t => 1 + typ_struct_measure T + trm_struct_measure t
  end
with
def_struct_measure (d : def) :=
  match d with
  | def_typ _ T => 1 + typ_struct_measure T
  | def_trm _ t => 1 + trm_struct_measure t
  end
with
defs_struct_measure (ds : defs) :=
  match ds with
  | defs_nil => 0
  | defs_cons d ds => def_struct_measure d + defs_struct_measure ds
  end.

Local Ltac simplify :=
  intros; cbn in *; try lia.

Local Ltac finish :=
  repeat match goal with
         | H : context[forall _, _ = _] |- _ =>
           rewrite H
         end;
  reflexivity.

Local Ltac crush_sm T :=
  lazymatch goal with
  | H : context[T] |- _ => clear H
  end; induction on T; simplify; finish.

Lemma open_typ_same_measure : forall T k u,
    typ_struct_measure $ open_rec_typ k u T = typ_struct_measure T
with open_dec_same_measure : forall T k u,
    dec_struct_measure $ open_rec_dec k u T = dec_struct_measure T
with open_decs_same_measure : forall T k u,
    decs_struct_measure $ open_rec_decs k u T = decs_struct_measure T.
Proof.
  - crush_sm typ.
  - crush_sm dec.
  - crush_sm decs.
Qed.

Lemma open_trm_same_measure : forall t k u,
    trm_struct_measure $ open_rec_trm k u t = trm_struct_measure t
with open_val_same_measure : forall vl k u,
    val_struct_measure $ open_rec_val k u vl = val_struct_measure vl
with open_def_same_measure : forall T k u,
    def_struct_measure $ open_rec_def k u T = def_struct_measure T
with open_defs_same_measure : forall T k u,
    defs_struct_measure $ open_rec_defs k u T = defs_struct_measure T.
Proof.
  all:pose proof open_typ_same_measure.
  - crush_sm trm.
  - crush_sm val.
  - crush_sm def.
  - crush_sm defs.
Qed.

Notation denv := (list typ).

Definition denv_fv (D : denv) : atoms :=
  fold_right (fun T fvs => fvs \u fv T) {} D.

Instance FvDenv : HasFv denv := { fv := denv_fv }.

Definition env_weight (G : env) : nat :=
  fold_right (fun (tup : (var * typ)) w =>
                let (_, T) := tup in w + typ_struct_measure T) 0 G.

Definition denv_weight (D : denv) : nat :=
  fold_right (fun T w => w + typ_struct_measure T) 0 D.

Local Notation wf_tup := (env * denv * typ)%type.

Definition join_measure G D T :=
  env_weight G + denv_weight D + typ_struct_measure T.
Arguments join_measure G D T/.

Definition wf_measure (tup : wf_tup) : nat :=
  let '(G, D, T) := tup in join_measure G D T.

Lemma typ_struct_measure_neq_0 : forall T,
    typ_struct_measure T <> 0.
Proof. destruct T; simpl; lia. Qed.

Lemma dec_struct_measure_neq_0 : forall D,
    dec_struct_measure D <> 0.
Proof. destruct D; simpl; lia. Qed.

Lemma decs_struct_measure_neq_0 : forall DS,
    decs_struct_measure DS <> 0.
Proof. destruct DS; simpl; lia. Qed.


Function wf_search_impl' (tup : wf_tup) {measure wf_measure tup} : nat :=
  let '(G, D, T) := tup in
  match T with
  | typ_top => 1
  | typ_bot => 1
  | typ_sel x A =>
    match x with
    | avar_b n =>
      match D with
      | nil => 0
      | cons T' D =>
        match n with
        | O => 1 + wf_search_impl' (G, D, T')
        | S n' => wf_search_impl' (G, D, typ_sel (avar_b n') A)
        end
      end
    | avar_f v =>
      match G with
      | nil => 0
      | cons (v', T') G' =>
        if v == v'
        then 1 + wf_search_impl' (G', nil, T')
        else wf_search_impl' (G', D, typ_sel (avar_f v) A)
      end

    end
  | typ_all T U =>
    1 + wf_search_impl' (G, T :: D, U)
  | typ_rcd decs_nil => 1
  | typ_rcd (decs_cons d DS) =>
    1 + match d with
        | dec_typ _ T1 T2 =>
          1 + wf_search_impl' (G, D, T1) + wf_search_impl' (G, D, T2)
        | dec_trm _ T => 1 + wf_search_impl' (G, D, T)
        end + wf_search_impl' (G, D, typ_rcd DS)
  end.
Proof.
  all:intros; simpl; try lia.
  all:pose proof (typ_struct_measure_neq_0 T'); lia.
Qed.

(** now, we need to proceed on proving properties on wf_search_impl'.
 *
 * we can still transform some problems to structural recursion.
 *)

Definition wf_search_impl G D T := wf_search_impl' (G, D, T).
Arguments wf_search_impl G D T/.

Definition wf_search_measure G T : nat := wf_search_impl G nil T.
Arguments wf_search_measure G T/.

Ltac mass_discharge :=
  simpl; trivial; try eassumption; try lia; set solve; auto.

Ltac ev_wf := unfold wf_search_impl;
              rewrite wf_search_impl'_equation;
              trivial; try congruence.

Ltac untup :=
  repeat lazymatch goal with
         | _ : context[wf_search_impl' (?G, ?D, ?T)] |- _ =>
           change (wf_search_impl' (G, D, T))
             with (wf_search_impl G D T) in *
         | |- context[wf_search_impl' (?G, ?D, ?T)] =>
           change (wf_search_impl' (G, D, T))
             with (wf_search_impl G D T)
         end.

Fixpoint wf_search_fv G (D : denv) v : nat :=
  match G with
  | nil => 0
  | cons (v', T') G' =>
    if v == v'
    then 1 + wf_search_impl G' nil T'
    else wf_search_fv G' D v
  end.

Lemma fv_search_equiv : forall G D v A,
    wf_search_fv G D v = wf_search_impl G D (typ_sel (avar_f v) A).
Proof.
  induction G; intros; simpl; ev_wf; routine.
Qed.

Lemma fv_ignores_D : forall G D v,
    wf_search_fv G D v = wf_search_fv G nil v.
Proof. induction G; routine. Qed.

Fixpoint wf_search_bv G D n : nat :=
  match D with
  | nil => 0
  | cons T' D' =>
    match n with
    | O => 1 + wf_search_impl G D' T'
    | S n' => wf_search_bv G D' n'
    end
  end.

Lemma bv_search_equiv : forall G D n A,
    wf_search_bv G D n = wf_search_impl G D (typ_sel (avar_b n) A).
Proof.
  induction D; intros; simpl.
  - ev_wf.
  - destruct n.
    + rewrite (wf_search_impl'_equation (_, _, _ _)).
      trivial.
    + ev_wf. apply IHD.
Qed.

Lemma bv_search_idx : forall G D1 D2 T,
    wf_search_bv G (D1 ++ [T] ++ D2) (length D1) =
    1 + wf_search_impl G D2 T.
Proof.
  induction D1; intros; simpl.
  - trivial.
  - simpl in IHD1. rewrite IHD1. trivial.
Qed.

Definition wf_search_avar G D v : nat :=
  match v with
  | avar_b n => wf_search_bv G D n
  | avar_f v => wf_search_fv G D v
  end.

Lemma avar_search_equiv : forall G D v A,
    wf_search_avar G D v = wf_search_impl G D (typ_sel v A).
Proof.
  intros. destruct v; simpl.
  - apply bv_search_equiv.
  - apply fv_search_equiv.
Qed.

Lemma wf_decs_ge_1 : forall G D DS,
    wf_search_impl' (G, D, {< DS >}) >= 1.
Proof. induction on decs; ev_wf; lia. Qed.

Lemma wf_decs_lookup_lt_upper : forall G (l l' : typ_label) T U (DS : decs),
    lbinds l (dec_typ l' T U) DS ->
    wf_search_measure G U < wf_search_measure G ({< DS >}).
Proof.
  induction on decs; intros.
  - progressive_inversions.
  - tidy_up; rewrite (wf_search_impl'_equation (_, _, _ _)).
    + pose proof (wf_decs_ge_1 H nil H4).
      lia.
    + specialize (IHdecs H5). lia.
Qed.

Lemma wf_decs_lookup_lt_lower : forall G (l l' : typ_label) T U (DS : decs),
    lbinds l (dec_typ l' T U) DS ->
    wf_search_measure G T < wf_search_measure G ({< DS >}).
Proof.
  induction on decs; intros.
  - progressive_inversions.
  - tidy_up; rewrite (wf_search_impl'_equation (_, _, _ _)).
    + pose proof (wf_decs_ge_1 H nil H4).
      lia.
    + specialize (IHdecs H5). lia.
Qed.

Lemma wf_decs_lookup_lt_trm : forall G (l l' : trm_label) T (DS : decs),
    lbinds l (dec_trm l' T) DS ->
    wf_search_measure G T < wf_search_measure G ({< DS >}).
Proof.
  induction on decs; intros.
  - progressive_inversions.
  - tidy_up; rewrite (wf_search_impl'_equation (_, _, _ _)).
    + pose proof (wf_decs_ge_1 H nil H3).
      lia.
    + specialize (IHdecs H4). lia.
Qed.

Lemma wf_measure_decs_cons : forall G D DS,
    wf_search_measure G (typ_rcd (decs_cons D DS)) >
    wf_search_measure G (typ_rcd DS).
Proof. simpl. intros. ev_wf. lia. Qed.

Definition dec_wf_search_impl G D d :=
  match d with
  | _ ∈ T1 ⋯ T2 => 1 + wf_search_impl' (G, D, T1) + wf_search_impl' (G, D, T2)
  | _ ∷ T => 1 + wf_search_impl' (G, D, T)
  end.

Lemma wf_search_dec : forall G D d,
    wf_search_impl' (G, D, {< decs_cons d decs_nil >}) =
    2 + dec_wf_search_impl G D d.
Proof.
  intros. rewrite wf_search_impl'_equation.
  destruct d; rewrite (wf_search_impl'_equation (_, _, typ_rcd _));
    simpl; lia.
Qed.

Lemma wf_search_decons : forall G D d DS,
    wf_search_impl' (G, D, {< decs_cons d DS >}) =
    1 + dec_wf_search_impl G D d + wf_search_impl' (G, D, {< DS >}).
Proof.
  intros. repeat rewrite (wf_search_impl'_equation (_, _, typ_rcd (decs_cons _ _))).
  reflexivity.
Qed.

Lemma dec_wf_search_ge_1 : forall G D d,
    dec_wf_search_impl G D d >= 1.
Proof. destr on dec; routine. Qed.

Lemma decs_wf_search_ge_1 : forall G D DS,
    wf_search_impl' (G, D, {< DS >}) >= 1.
Proof. destr on decs; rewrite wf_search_impl'_equation; lia. Qed.

Lemma wf_search_deapp : forall G D DS1 DS2,
    wf_search_impl' (G, D, {< append' DS1 DS2 >}) =
    pred (wf_search_impl' (G, D, {< DS1 >})) + wf_search_impl' (G, D, {< DS2 >}).
Proof.
  induction on decs; intros; simpl.
  - simpl. rewrite (wf_search_impl'_equation (_, _, typ_rcd decs_nil)).
    trivial.
  - repeat rewrite wf_search_decons. simpl.
    rewrite IHdecs.
    pose proof (decs_wf_search_ge_1 H H0 H1).
    lia.
Qed.