Require Import Safety.
Require Import OperationProperties.
Require Import Helpers.

Local Hint Resolve lbinds_def_trm_on_typ.

Lemma lget_not_in_ldom : forall {A} (l : list (label * A)) a,
    lget a l = None -> a `lnotin` ldom l.
Proof. induction on list; routine. Qed.

Definition lookup_trm (ds : defs) (a : trm_label) :
  {t | lbinds a (def_trm a t) ds} + {a `lnotin` ldom ds}.
Proof.
  destruct (lget a ds) eqn:?.
  - left.
    apply lget_to_lbinds in Heqo.
    destruct d. exfalso; eauto.
    exists t0.
    pose proof (lbinds_defs_on_trm _ _ _ _ Heqo).
    routine.
  - right. apply lget_not_in_ldom. trivial.
Defined.

Local Ltac to_refute :=
  let Contra := fresh "Contra" in right; split; intro Contra; tidy_up.

Local Ltac refute :=
  to_refute;
  repeat match goal with
         | Heq : get ?a ?s = ?O,
                 Hb : binds ?a ?T ?s |- _ =>
           let ST := constr:(Some T) in
           different ST O;
           apply binds_to_get in Hb; trivial;
           congruence
         | H1 : binds ?a ?T1 ?s,
                H2 : binds ?a ?T2 ?s,
                     Hu : uniq ?s |- _ =>
           let H := fresh "H" in
           pose proof (binds_unique _ _ _ _ _ H1 H2 Hu) as H;
           clear H1 H2;
           congruence || (invert H; subst)
         | H : lbinds _ _ _ |- _ =>
           apply LabelAssocList.binds_In in H
         | H : ~ _ |- False => solve [apply H; eroutine]
         | _ => solve [eroutine]
         end.

Tactic Notation "go" uconstr(l) := left; left; exists l; simpl; eauto.

Local Ltac let_case H :=
  destruct H;
  try match goal with
      | H : _ + { _ } |- _ => destruct H
      end;
  destruct_conjs;
  [ match goal with
    | _ : [_] _ ↦ [?s ++ _] _ |- _ =>
      go s
    end
  | exfalso; tidy_up
  | refute].

Definition step (s : sta) (t : trm) :
  uniq s ->
  { s' & { t' | [s] t ↦ [s' ++ s] t'} } +
  {nf t} +
  {~(exists s' t', [s] t ↦ [s'] t') /\ ~nf t}.
Proof.
  induction t; intros; auto.
  - destruct a. refute.
    destruct (get a s) eqn:?.
    + apply get_to_binds in Heqo.
      destruct v.
      * destruct (lookup_trm d t); tidy_up.
        go nil.
        refute.
      * refute.
    + refute.
  - destruct a, a0; try solve [refute].
    destruct (get a s) eqn:?.
    + destruct v. refute.
      apply get_to_binds in Heqo.
      go nil.
    + refute.
  - destruct t1.
    par:try solve [let_case (IHt1 H)].
    + destruct a. refute. go nil.
    + destruct (atom_fresh (dom s)).
      go (x ~ v). simpl_env. eauto.
Defined.

Inductive state G s : Type :=
| state_intro :
    well_typed G s ->
    inert_env G ->
    state G s.

Inductive closure s t : sta -> trm -> Prop :=
| none : closure s t s t
| multi : forall u v s' t',
    closure s t u v ->
    [u] v ↦ [s'] t' ->
    closure s t s' t'.

Lemma not_de_bruijn_index : forall G n A T,
    G ⊢ trm_sel (avar_b n) A ⦂ T -> False.
Proof. dep induction on ty_trm; eauto. Qed.

(* Definition context_step (s : sta) (t : trm) : forall G T, *)
(*   well_typed G s -> *)
(*   inert_env G -> *)
(*   uniq s -> *)
(*   G ⊢ t ⦂ T -> *)
(*   {G' & { s' & { t' | inert_env G' /\ *)
(*                       well_typed (G' ++ G) (s' ++ s) /\ *)
(*                       G' ++ G ⊢ t' ⦂ T /\ *)
(*                       [s] t ↦ [s' ++ s] t'} } } + *)
(*   {nf t} + *)
(*   {~(exists s' t', [s] t ↦ [s'] t') /\ ~nf t}. *)
(* Proof. *)
(*   induction t; intros; auto. *)
(*   - destruct a. refute. *)
(*     destruct (get a s) eqn:?. *)
(*     + apply get_to_binds in Heqo. *)
(*       destruct v. *)
(*       * destruct (lookup_trm d t); tidy_up. *)
(*         -- left. left. exists nil, nil; simpl. eexists. *)
(*            split. auto. *)
(*            split. auto. *)
(*            split. *)
(*            Focus 2. eauto. *)
(*            admit. *)
(*         -- refute. *)
(*       * refute. *)
(*     + refute. *)
(*   - destruct a, a0; try solve [refute]. *)
(*     destruct (get a s) eqn:?. *)
(*     + destruct v. refute. *)
(*       apply get_to_binds in Heqo. *)

(*       left. left. exists nil, nil; simpl. eexists. *)
(*       split. auto. *)
(*       split. auto. *)
(*       split. *)
(*       Focus 2. eauto. *)
(*       admit. *)
(*     + refute. *)
(*   - destruct t1. *)
(*     (* par:try solve [let_case (IHt1 H)]. *) *)
(*     + destruct a. refute. *)

(*       left. left. exists nil, nil; simpl. eexists. *)
(*       split. auto. *)
(*       split. auto. *)
(*       split. *)
(*       Focus 2. eauto. *)
(*       admit. *)
(*     + destruct (atom_fresh (dom s)). *)

      
(*       go (x ~ v). simpl_env. eauto. *)
(* Defined. *)


(* Definition step_context : forall t G s T, *)
(*     well_typed G s -> *)
(*     inert_env G -> *)
(*     G ⊢ t ⦂ T -> *)
(*     forall s' t', *)
(*       [s] t ↦ [s'] t' -> *)
(*       {G' | *)
(*        inert_env G' /\ *)
(*        well_typed (G' ++ G) s' /\ *)
(*        G' ++ G ⊢ t' ⦂ T }. *)
(* Proof. *)



(* Definition step_context : forall t G s T, *)
(*     well_typed G s -> *)
(*     inert_env G -> *)
(*     G ⊢ t ⦂ T -> *)
(*     forall s' t', *)
(*       [s] t ↦ [s'] t' -> *)
(*       {G' | *)
(*        inert_env G' /\ *)
(*        well_typed (G' ++ G) s' /\ *)
(*        G' ++ G ⊢ t' ⦂ T }. *)
(* Proof. *)
(*   induction t; intros. *)
(*   -  *)

Definition no_err_step (s : sta) (t : trm) :
  forall G T,
    state G s ->
    G ⊢ t ⦂ T ->
    { s' & { t' | [s] t ↦ [s' ++ s] t'} } + {nf t}.
Proof.
  intros. destruct H.
  destruct (step s t).
  - unfold well_typed in w. routine.
  - auto.
  - exfalso. edestruct progress.
    econstructor; try eassumption.
    all:intuition.
Defined.


(* Definition eval (fuel : nat) {G s} (st : state G s) {t T} : *)
(*   forall {os ot}, *)
(*     closure os ot s t -> *)
(*     G ⊢ t ⦂ T -> *)
(*     {G' & {s' & {t' & {s'' & {t'' | state (G' ++ G) (s' ++ s) /\ *)
(*                                   closure os ot (s' ++ s) t' /\ *)
(*                                   [s' ++ s] t' ↦ [s'' ++ s' ++ s] t'' *)
(*     } } } } } + *)
(*     { nf t }. *)
(* Proof. *)
(*   gen T t st s. gen G. *)
(*   induction fuel; intros. *)
(*   - edestruct no_err_step; try eassumption; destruct_conjs. *)
(*     + left. exists nil, nil, t, s0, X. simpl. auto. *)
(*     + auto. *)
(*   - edestruct no_err_step; try eassumption; destruct_conjs. *)
(*     + pose proof (IHfuel ) *)
(*     + auto. *)
(* Defined. *)