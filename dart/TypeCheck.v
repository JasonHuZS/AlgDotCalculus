Set Implicit Arguments.
Require Import Definitions.
Require Import SubstitutionLaws.
Require Import Substitution.
Require Import Narrowing.
Require Import Weakening.
Require Import OperationProperties.
Require Import TightTyping.
Require Import SubEnv.
Require Import Measures.
Require Import Helpers.

Require String.

Inductive wf_env : env -> Prop :=
| wf_nil : wf_env nil
| wf_cons : forall {x T G}, x \notin fv T \u fv G ->
                       fv T [<=] dom G ->
                       lc T ->
                       wf_env G ->
                       wf_env (x ~ T ++ G).
Hint Constructors wf_env.

Lemma wf_decons : forall x T G, wf_env (x ~ T ++ G) -> x \notin fv T.
Proof. routine. Qed.

Lemma wf_deapp : forall G1 G2, wf_env (G1 ++ G2) -> wf_env G2.
Proof. induction on env; routine. Qed.

Lemma wf_uniq : forall G, wf_env G -> uniq G.
Proof. induction on env; routine. Qed.

Lemma wf_var_in : forall G1 G2 v,
    wf_env (G1 ++ G2) ->
    v `in` dom G2 ->
         v `notin` dom G1.
Proof.
  induction G1; intros.
  - routine.
  - tidy_up. repeat rewrite dom_app in *.
    destruct_notin.
    rewrite AtomSetProperties.add_union_singleton.
    intro Contra.
    apply AtomSetImpl.union_1 in Contra.
    destruct Contra.
    + apply AtomSetImpl.singleton_1 in H1. subst.
      intuition.
    + eapply IHG1; eassumption.
Qed.        

Hint Extern 3 (_ `notin` _) => simpl in *; solve_notin.

Section MeasuresAndFacts.
  
  Lemma wf_search_fv_weaken : forall G1 G2 D v,
    wf_env (G1 ++ G2) ->
    v `in` dom G2 ->
         wf_search_fv G2 D v = wf_search_fv (G1 ++ G2) D v.
  Proof.
    induction G1; intros; trivial.
    destruct_conjs. simpl. destruct_eq.
    - exfalso. eapply wf_var_in; mass_discharge.
    - apply IHG1; routine.
  Qed.

  Definition wf_weaken_measure (D : denv) (T : typ) : nat :=
    denv_weight D + typ_struct_measure T.
  Arguments wf_weaken_measure D T/.

  Program Fixpoint wf_search_weaken_gen D T {measure (wf_weaken_measure D T)} :
    forall G1 G2,
      wf_env (G1 ++ G2) ->
      fv T [<=] dom G2 ->
      fv D [<=] dom G2 ->
      wf_search_impl' (G2, D, T) = wf_search_impl' (G1 ++ G2, D, T) := _.
  Next Obligation.
    destruct T; try solve [do 2 ev_wf].
    - do 2 ev_wf. destruct a.
      + destruct D. trivial.
        destruct n.
        * simpl in H1.
          erewrite wf_search_weaken_gen; mass_discharge.
        * pose proof (typ_struct_measure_neq_0 t0).
          erewrite wf_search_weaken_gen; mass_discharge.
      + assert (a `in` dom G2) by set solve.
        pose proof (wf_search_fv_weaken G1 G2 D H H2).
        do 2 erewrite fv_search_equiv in H3.
        simpl in H3.
        do 2 rewrite wf_search_impl'_equation in H3.
        apply H3.
    - simpl in H0.
      do 2 rewrite (wf_search_impl'_equation (_, D, _)).
      erewrite wf_search_weaken_gen; mass_discharge.
    - simpl in H0.
      do 2 rewrite (wf_search_impl'_equation (_, _, {< _ >})).
      destruct d; trivial.
      destruct d.
      + do 3 (rewrite wf_search_weaken_gen with (G1 := G1) (G2 := G2);
              mass_discharge).
      + do 2 (rewrite wf_search_weaken_gen with (G1 := G1) (G2 := G2);
              mass_discharge).
  Qed.

  Theorem wf_search_weaken : forall G1 G2 T,
      wf_env (G1 ++ G2) ->
      fv T [<=] dom G2 ->
      wf_search_measure G2 T = wf_search_measure (G1 ++ G2) T.
  Proof.
    intros. apply wf_search_weaken_gen; mass_discharge.
  Qed.

  Lemma wf_lookup_lt : forall G1 G2 x T A,
      wf_env (G1 ++ x ~ T ++ G2) ->
      fv T [<=] dom G2 ->
      wf_search_measure G2 T <
      wf_search_measure (G1 ++ x ~ T ++ G2) (typ_sel (avar_f x) A).
  Proof.
    intros. rewrite <- wf_search_weaken; trivial.
    simpl. rewrite (wf_search_impl'_equation (_, _, _ _)).
    destruct_eq. lia.
    set solve.
  Qed.

  Inductive incr_lc : list typ -> Prop :=
  | lc_nil : incr_lc nil
  | lc_cons : forall T D, lc_at (length D) T -> incr_lc D -> incr_lc (T :: D).
  Hint Constructors incr_lc.

  Fixpoint open_rec_D v (D : denv) :=
    match D with
    | nil => nil
    | cons T D' => open_rec (length D') v T :: open_rec_D v D'
    end.

  Program Fixpoint wf_open_shift_gen D U {measure (wf_weaken_measure D U)} :
    forall G n T x,
      wf_env G ->
      fv U [<=] dom G ->
      fv D [<=] dom G ->
      incr_lc (D ++ [T]) ->
      length D = n ->
      x `notin` fv G \u fv D \u fv T \u fv U ->
      lc_at (S n) U ->
      wf_search_impl' (G, D ++ [T], U) =
      wf_search_impl' (x ~ T ++ G, open_rec_D x D, open_rec n x U) := _.
  Next Obligation.
    destruct U.
    - do 2 ev_wf.
    - do 2 ev_wf.
    - untup. rewrite <- avar_search_equiv. 
      destruct a; simpl; ev_wf.
      + erewrite bv_search_equiv. ev_wf.
        destruct_eq.
        * destruct_eq.
          destruct D; trivial.

          simpl. untup.
          rewrite <- bv_search_equiv.
          change [T] with ([T] ++ nil).
          rewrite bv_search_idx. trivial.
        * progressive_inversions.
          assert (n < length D) by lia.
          destruct D.
          -- simpl in *. lia.
          -- simpl. simpl in H1.
             progressive_inversions.
             destruct n.
             ++ f_equal. apply wf_open_shift_gen; mass_discharge.
                rewrite app_length in H10. simpl in H10.
                rewrite Nat.add_comm in H10. trivial.
             ++ simpl in H3. assert (n < length D) by lia.

                pose proof (typ_struct_measure_neq_0 t0).
                erewrite wf_open_shift_gen with (x := x); trivial.
                erewrite open_lc_le_typ.
                all:mass_discharge.
                all:repeat constructor.
                lia.
      + simpl. destruct_eq.
        * simpl in *. solve_notin.
        * untup. rewrite <- fv_search_equiv.
          rewrite fv_ignores_D.
          rewrite fv_ignores_D with (D := _ _ D).
          trivial.
    - ev_wf. rewrite (wf_search_impl'_equation (_ :: _, _, _)).
      simpl in *. progressive_destructions.
      f_equal. change (U1 :: D ++ [T]) with ((U1 :: D) ++ [T]).
      erewrite wf_open_shift_gen with (x := x); mass_discharge.
      constructor; trivial.
      rewrite app_length. simpl. rewrite Nat.add_comm.
      trivial.
    - destruct d; simpl; ev_wf.
      + ev_wf.
      + destruct d.
        * rewrite (wf_search_impl'_equation (_ :: _, _, _)).
          simpl in *. progressive_destructions.
          do 3 f_equal; [f_equal |].
          all:try apply wf_open_shift_gen; mass_discharge.
          apply (wf_open_shift_gen D ({< d0 >})); mass_discharge.
        * rewrite (wf_search_impl'_equation (_ :: _, _, _)).
          simpl in *. progressive_destructions.
          do 3 f_equal.
          apply wf_open_shift_gen; mass_discharge.
          apply (wf_open_shift_gen D ({< d0 >})); mass_discharge.
  Qed.

  Lemma wf_open_shift : forall G T U x,
      wf_env G ->
      fv T [<=] dom G ->
      fv U [<=] dom G ->
      x `notin` fv G \u fv T \u fv U ->
      lc T ->
      lc_at 1 U ->
      wf_search_impl G [T] U =
      wf_search_impl (x ~ T ++ G) nil (open x U).
  Proof.
    intros. eapply wf_open_shift_gen with (D := nil); trivial.
    all:progressive_destructions; simpl in *; mass_discharge.
    repeat constructor. trivial.
  Qed.

  Lemma measure_shrinks : forall G x T A,
      wf_env G ->
      get x G = Some T ->
      wf_search_measure G T <
      wf_search_measure G (typ_sel (avar_f x) A).
  Proof.
    intros. apply get_witness in H0.
    destruct_conjs.
    assert (wf_env (x ~ T ++ X)) by (eapply wf_deapp; routine).
    progressive_inversions.
    pose proof (wf_lookup_lt H0 X x T ltac:(subst; trivial) ltac:(trivial)).
    rewrite wf_search_weaken with (G1 := H0 ++ x ~ T) (G2 := X) in H1.
    all:repeat rewrite app_assoc in *; auto.
  Qed.

  Lemma measure_unfold_all : forall G T U x,
      wf_env G ->
      fv (typ_all T U) [<=] dom G ->
      x `notin` fv G \u fv T \u fv U ->
      lc (typ_all T U) ->
      wf_search_measure G (typ_all T U) =
      1 + wf_search_measure (x ~ T ++ G) (open x U).
  Proof.
    intros. simpl. ev_wf. untup.
    simpl. f_equal.
    eapply wf_open_shift; mass_discharge; routine.
  Qed.
  
End MeasuresAndFacts.
Arguments wf_search_impl G D T/.
Arguments wf_search_measure G T/.

Ltac ev_wf := unfold wf_search_impl;
              rewrite wf_search_impl'_equation;
              trivial; try congruence.

Ltac untup := 
  repeat lazymatch goal with
         | _ : context[wf_search_impl' (?G, ?D, ?T)] |- _ =>
           change (wf_search_impl' (G, D, T))
             with (wf_search_impl G D T) in *
         | |- context[wf_search_impl' (?G, ?D, ?T)] =>
           change (wf_search_impl' (G, D, T))
             with (wf_search_impl G D T)
         end.

(** the specification of exposure, annotated individually.
 * 
 * G : the original context
 * G1 : potential extra context
 * G2 : the witness context of the subtyping relation
 * S : the input type
 * U : the result type
 *)
Definition exposure_spec (G : env) (S U : typ) : Prop :=
  (G ⊢ S <⦂ U) /\             (* witness of subtyping *)
  fv U [<=] dom G /\
  (* free variables of output type is bound by context. *)
  lc U /\                      (* result type of locally closed. *)
  wf_search_measure G U <= wf_search_measure G S /\
  (* preserves measure for subtyping algorithm. *)
  match U with                (* result type is non-dependent. *)
  | typ_sel _ _ => False
  | _ => True
  end.
Arguments exposure_spec G S U/.

Ltac ref := apply ret; repeat eexists; repeat split.

Ltac st_refl := ref; apply subtyp_refl || simpl in *;
                      lazymatch goal with
                      | |- ?G1 ++ _ = ?G =>
                        lazymatch type of G with list ?T => unify G1 (@nil T) end;
                        simpl; reflexivity
                      | _ => idtac
                      end; auto.

Local Ltac solve_by_weaken := 
  apply weaken_subtyp; try eassumption;
  simpl_env; destruct_uniq; auto.

Local Ltac recurse t msg :=
  let n := fresh "recur" in
  pose proof t as n;
  refine (x <- n !!> msg ;; _);
  unfold exposure_spec in *;
  destruct_conjs;
  simpl in *.

Local Ltac precise_typ_pf a t tail Hname :=
  let H := fresh "H" in
  assert (H : get a (a ~ t ++ tail) = Some t) by routine;
  apply get_to_binds in H;
  pose proof (ty_var _ _ _ H) as Hname;
  eapply ty_sub in Hname.

Local Ltac destr a x msg :=
  destruct x eqn:?;
           lazymatch goal with
           | _ : x = typ_bot |- _ => idtac
           | _ : x = typ_rcd _ |- _ => idtac
           | _ : x = ?t |- _ => exact (erret msg)
           end.

Ltac decs_facts H :=
  lazymatch type of H with
  | lbinds ?A ?D ?DS =>
    let Hwfld := fresh "H" in
    let Eq := fresh "Eq" in
    assert (lc D) by (eapply lc_at_binds_decs; eroutine);
    destruct D eqn:Eq;
    try solve [exfalso; eapply lbinds_typ_on_trm + eapply lbinds_trm_on_typ;
               eassumption];
    pose proof H;
    (apply lbinds_decs_on_typ in H || apply lbinds_decs_on_trm in H);
    lazymatch type of Eq with
    | _ = _ _ ?t1 ?t2 => assert (lc t1 /\ lc t2)
    | _ = _ _ ?t => assert (lc t)
    end;
    [routine |]
  end; destruct_conjs.

Ltac pose_fv_decs_lkup T G :=
  assert (fv T [<=] dom G);
  [ etransitivity; [
          | etransitivity;
            [ eapply fv_decs_shrinks; eassumption
            | simpl in *; subst; set solve]];
    set solve
   |].

Local Ltac by_inv H := solve [invert H; trivial].

Hint Resolve wf_uniq.

(** this implementation gives a super type of S in the given context.
 * in the failure case, it also gives an explanation on why.
 * 
 * it actually is able to show that the subtyping holds in a potentially smaller
 * context than the original one.
 *)
Program Fixpoint exposure_impl G S {measure (length G)} :
  wf_env G ->                  (* env wellformed-ness *)
  fv S [<=] dom G ->            (* free variable in S needs to be captured by context. *)
  lc S ->                      (* S needs to be closed *)
  Tc { U : typ | exposure_spec G S U } := _.
Next Obligation.
  refine (match S as S0 return
                fv S0 [<=] dom G -> lc S0 ->
                Tc { U : typ | exposure_spec G S0 U } with
          | typ_sel a A => fun subset lcS => _
          | _ => fun subset lc => ltac:(st_refl)
          end H0 H1).

  destruct a; [simpl in lcS; exfalso; progressive_inversions |].
  assert (ainG : a `in` dom G) by set solve.
  destruct (get_for_sure G ainG) as [T Heqo].
  pose proof (get_witness _ _ Heqo). destruct_conjs.
  assert (lenX : length X < length G). {
    rewrite <- H3. repeat rewrite app_length.
    simpl. lia.
  }

  assert (wfX : wf_env X).
  { subst G. eapply wf_deapp. instantiate (1 := H2 ++ a ~ T). simpl_env. trivial. }
  assert (wfapp : wf_env (a ~ T ++ X)). {
    subst. eapply wf_deapp. eassumption.
  }
  assert (fvT : fv T [<=] dom X) by by_inv wfapp.
  assert (lcT : lc T) by by_inv wfapp.
  
  recurse (exposure_impl X T lenX wfX fvT lcT)
          "exposure: not able to perform exposure after lookup".
  destr a x "exposure: not a bottom or a record type".
  + ref; subst.
    let H0 := fresh "H" in
    precise_typ_pf a T X H0;
      [ eapply subtyp_sel2; apply weaken_trm; apply H0 |].
    do 2 instantiate (1 := typ_bot).
    eapply subtyp_trans.

    * solve_by_weaken. 
    * constructor.
    * set solve.
    * trivial.
    * pose proof (wf_lookup_lt H2 X a T A H fvT).
      pose proof (wf_search_weaken (H2 ++ a ~ T) X).
      rewrite app_assoc in *.
      pose proof (H9 typ_bot H ltac:(set solve)).
      specialize (H9 T H fvT).
      simpl in *. lia.
    * trivial.
      
  + destruct (lget A d) eqn:?; [| refine (erret "exposure: label is not found")].
    apply lget_to_lbinds in Heqo0.
    decs_facts Heqo0.
    pose_fv_decs_lkup t1 X.
    recurse (exposure_impl X t1 lenX wfX H13 H12)
            "exposure: failed second recursive call". subst.
    ref. eapply subtyp_trans.
    eapply subtyp_sel2. eapply decs_bind_typ.
    eapply ty_sub. constructor.
    * eroutine.
    * reassoc 3 with 2. solve_by_weaken.
    * tidy_up. eassumption.
    * reassoc 3 with 2. solve_by_weaken.
    * set simpl. set solve.
    * trivial.
    * pose proof (wf_search_weaken (H2 ++ a ~ T) X).
      rewrite app_assoc in *.
      pose proof (H3 x H ltac:(set solve)).
      pose proof (wf_lookup_lt H2 X a T A H fvT).
      pose proof (wf_decs_lookup_lt_upper X _ _ _ _ _ H10).
      simpl in *. lia.
    * trivial.
Defined.

Local Ltac unpack := repeat (split || eexists || apply ret).

Local Ltac prode_refl :=
  unpack; apply subtyp_refl || solve_notin.

Local Ltac recurse t ::=
  let n := fresh "recur" in
  pose proof t as n;
  destruct_conjs;
  simpl in *.

Local Ltac ref_body b msg :=
  let x := fresh "x" in refine (x <- b !!> msg ;; _); destruct_conjs.

Local Ltac precise_typ a t tail pf :=
  let H0 := fresh "H" in
  precise_typ_pf a t tail H0;
  [ eapply pf;
    eapply weaken_trm; apply H0 |].

Ltac subset_fv S :=
  lazymatch goal with
  | H : wf_env (_ :: _) |- ?a `notin` _ =>
    let Contra := fresh "Contra" in
    invert H; subst;
    autorewrite with meta_ext in *;
    intro Contra;
    match goal with
    | H1 : a `notin` _ |- _ =>
      apply H1; enough (a `in` fv S); set solve
    end
  end.

Local Ltac solve_decl :=
  destruct_conjs;
  unpack;
  [eapply subtyp_typ; eassumption |
   simpl; solve_notin |
   routine |
   simpl; set solve].

Local Ltac force_split H :=
  destruct H; try solve [routine].

Local Ltac concretize_body pf :=
  ref; try (list_reasoning;
       eapply decs_append_resp_subtyp; [ | eassumption];
       apply pf; eassumption);
  try set solve;
  routine.

(** specifictions for promotion and demotion. *)

(** 
 * G : the context
 * x : the variable to remove
 * S : the input type
 * U : the result type
 *)
Definition promo_spec G x (S U : typ) :=
  G ⊢ S <⦂ U /\                  (* U is a super type of S *)
  x `notin` fv U /\              (* x is indeed remvoed *)
  lc U /\                        (* U remains closed *)
  fv U [<=] dom G.               (* free variables are bound. *)

Definition demo_spec G x (S U : typ) :=
  G ⊢ U <⦂ S /\
  x `notin` fv U /\
  lc U /\
  fv U [<=] dom G.

(** it's tricky that promotion and demotion are mutually recursive.
 * 
 * luckily, they are dual, so we can as well compute them at the same time,
 * hence the signature returns a tuple of results.
 *)
Program Fixpoint promo_demo G x S {measure (typ_struct_measure S)} :
  wf_env G ->                    (* environment well-formed *)
  fv S [<=] dom G ->              (* free vars of input are bound. *)
  lc S ->                        (* input type is closed *)
  Tc { U1 | promo_spec G x S U1 } *
  Tc { U2 | demo_spec G x S U2 } := _.
Next Obligation.
  unfold promo_spec in *; unfold demo_spec in *.
  destruct S.
  - prode_refl.
  - prode_refl.
  - destruct a. simpl in H1; exfalso; progressive_inversions.

    destruct (x == a); [ subst x | prode_refl ].

    assert (ainG : a `in` dom G).
    { apply H0. simpl. apply AtomSetImpl.singleton_2. trivial. }
    destruct (get_for_sure G ainG) as [T Heqo].

    pose proof (get_witness _ _ Heqo). destruct_conjs.
    assert (lenX : length X < length G). {
      rewrite <- H3. repeat rewrite app_length.
      simpl. lia.
    }
    assert (wfapp : wf_env (a ~ T ++ X)). {
      subst. eapply wf_deapp. eassumption.
    }
    assert (wfX : wf_env X) by by_inv wfapp.
    assert (fvT : fv T [<=] dom X) by by_inv wfapp.
    assert (lcT : lc T) by by_inv wfapp.

    recurse (exposure_impl wfX fvT lcT).
    unfold exposure_spec in recur.
    split; ref_body recur "promo/demo: exposure failed for var case.".
    
    all:destr a x "promo/demo: not a bottom or a record type".
    + ref; subst.

      precise_typ a T X @subtyp_sel2.
      do 2 instantiate (1 := typ_bot).
      eapply subtyp_trans.
      * solve_by_weaken.
      * constructor.
      * solve_notin.
      * trivial.
      * set solve.
        
    + destruct (lget t d) eqn:?; [| refine (erret "promo/demo: label is not found")].
      apply lget_to_lbinds in Heqo0.
      decs_facts Heqo0.
      pose_fv_decs_lkup t2 X.

      ref; subst.
      * precise_typ a T X @subtyp_sel2.
        eapply subtyp_trans. solve_by_weaken.
        eapply lbinds_subtyp. progressive_inversions.
        eassumption.
      * assert (a `notin` dom X).
        { invert wfapp. simpl in H16. set solve. }
        eapply notin_subset_relax; eassumption.
      * trivial.
      * simpl_env. etransitivity; [eassumption |].
        set solve.

    + ref; subst.
      precise_typ a T X @subtyp_sel1.
      do 2 instantiate (1 := typ_top).

      eapply subtyp_trans.
      * solve_by_weaken.
      * constructor.
      * solve_notin.
      * trivial.
      * set solve.

    + destruct (lget t d) eqn:?; [| refine (erret "promo/demo: label is not found")].
      apply lget_to_lbinds in Heqo0.
      decs_facts Heqo0.
      pose_fv_decs_lkup t1 X.

      ref; subst.
      * precise_typ a T X @subtyp_sel1.
        eapply subtyp_trans. solve_by_weaken.
        eapply lbinds_subtyp. progressive_inversions.
        eassumption.
      * assert (a `notin` dom X).
        { invert wfapp. simpl in H16. set solve. }
        eapply notin_subset_relax; eassumption.
      * routine.
      * simpl_env. etransitivity; [eassumption |].
        set solve.
  - force_split (promo_demo G x S1).
    split.
    + ref_body s0 "promotion: failed in function case".
      pick_fresh z.
      assert (wf_env (z ~ x0 ++ G)) by (constructor; routine).
      assert (fv (open z S2) [<=] dom (z ~ x0 ++ G)). {
        etransitivity. apply fv_open_typ. simpl.
        rewrite AtomSetProperties.add_union_singleton.
        set solve.
      }
      assert (lc (open z S2)) by (apply open_lc_typ; routine).
      force_split (promo_demo (z ~ x0 ++ G) x (open z S2));
        [rewrite (open_typ_same_measure _ 0 z); simpl; lia |].
      ref_body s1 "promotion: failed in function body type". ref.
      * eapply subtyp_all. eassumption.
        cofinite.
        pose proof (@fv_close_self_typ x1 z 0).
        assert (x2 `notin` fv (close z x1)). {
          eapply notin_subset_relax.
          apply fv_close_typ. auto.
        }

        erewrite <- open_left_inv_typ in H9.
        eapply open_subst_subtyp in H9.
        
        -- eassumption.
        -- apply wf_uniq. trivial.
        -- solve_notin.
        -- solve_notin.
        -- trivial.
      * assert (x `notin` fv (close z x1)). {
          eapply notin_subset_relax.
          apply fv_close_typ. auto.
        }
        solve_notin.
      * constructor; trivial.
        apply close_lc_typ. trivial.
      * simpl.
        assert (fv (close z x1) [<=] dom G). {
          apply fv_add_close_typ. trivial.
        }
        set solve.

    + ref_body s "demotion: failed in function case".
      pick_fresh z.
      assert (wf_env (z ~ S1 ++ G)) by(constructor; routine).
      assert (fv (open z S2) [<=] dom (z ~ S1 ++ G)). {
        etransitivity. apply fv_open_typ. simpl.
        rewrite AtomSetProperties.add_union_singleton.
        set solve.
      }
      assert (lc (open z S2)) by (apply open_lc_typ; routine).
      force_split (promo_demo (z ~ S1 ++ G) x (open z S2));
        [rewrite (open_typ_same_measure _ 0 z); simpl; lia |].
      ref_body s2 "promotion: failed in function body type". ref.
      * eapply subtyp_all. eassumption.
        cofinite.
        pose proof (@fv_close_self_typ x1 z 0).
        assert (x2 `notin` fv (close z x1)). {
          eapply notin_subset_relax.
          apply fv_close_typ. auto.
        }

        erewrite <- (@open_left_inv_typ x1) in H9.
        eapply open_subst_subtyp in H9.

        -- eassumption.
        -- apply wf_uniq. trivial.
        -- solve_notin.
        -- solve_notin.
        -- trivial.
      * assert (x `notin` fv (close z x1)). {
          eapply notin_subset_relax.
          apply fv_close_typ. auto.
        }
        solve_notin.
      * constructor; trivial.
        apply close_lc_typ. trivial.
      * simpl.
        assert (fv (close z x1) [<=] dom G). {
          apply fv_add_close_typ. trivial.
        }
        set solve.

  - assert (forall DS, typ_struct_measure ({< DS >}) <= typ_struct_measure ({< d >}) ->
                  fv ({< DS >}) [<=] dom G ->
                  lc ({< DS >}) ->
                  Tc { DS1 | promo_spec G x ({< DS >}) ({< DS1 >}) } *
                  Tc { DS2 | demo_spec G x ({< DS >}) ({< DS2 >}) }). {
      induction on decs; intros.
      - prode_refl.
      - force_split IHdecs.
        destruct d0 eqn:?.
        + force_split (promo_demo G x t0).
          force_split (promo_demo G x t1).
          unfold promo_spec in s; unfold demo_spec in s0.
          split.
          * ref_body s "promotion: failed in recursive case for declarations";
              ref_body s2 "promotion: failed in lower bound case for declarations";
              ref_body s3 "promotion: failed in upper bound case for declarations".
            concretize_body subtyp_typ.
          * ref_body s0 "demotion: failed in recursive case for declarations";
              ref_body s1 "demotion: failed in lower bound case for declarations";
              ref_body s4 "demotion: failed in upper bound case for declarations".
            concretize_body subtyp_typ.
        + force_split (promo_demo G x t0).
          unfold promo_spec in s; unfold demo_spec in s0.
          split.
          * ref_body s "promotion: failed in recursive case for declarations";
              ref_body s1 "promotion: failed in field type case for declarations".
            concretize_body subtyp_fld.
          * ref_body s0 "promotion: failed in recursive case for declarations";
              ref_body s2 "promotion: failed in field type case for declarations".
            concretize_body subtyp_fld.
    }
    force_split (H2 d). split.
    + ref_body s "promotion: failed to promote declarations".
      apply ret. exists ({< x0 >}). apply H3.
    + ref_body s0 "demotion: failed to demote declarations".
      apply ret. exists ({< x0 >}). apply H3.
Defined.

Definition promotion G x S :
  wf_env G ->
  fv S [<=] dom G ->
  lc S ->
  Tc { U | promo_spec G x S U }.
Proof.
  intros. edestruct (promo_demo); eassumption.
Defined.

Definition demotion G x S :
  wf_env G ->
  fv S [<=] dom G ->
  lc S ->
  Tc { U | demo_spec G x S U }.
Proof.
  intros. edestruct (promo_demo); eassumption.
Defined.

(* subtyping/typing algorithm *)
Create HintDb subty discriminated.
Hint Rewrite -> dom_app : subty.
Ltac step := autorewrite with subty in *.
Ltac mass_discharge :=
  trivial; try solve [subst; simpl in *; step; (lia || set solve; routine)].

Ltac st_undec T U :=
  lazymatch goal with
  | _ : T = ?T', _ : U = ?U' |- _ => 
    exact (erret "subtyper: unknown how to compare two types.")
  end.

Ltac lc_bv :=
  lazymatch goal with
  | H : lc_typ_at 0 (typ_sel (avar_b _) _) |- _ =>
    exfalso; progressive_inversions
  | H : lc_trm_at 0 (trm_var (avar_b _)) |- _ =>
    exfalso; progressive_inversions
  | H : lc_trm_at 0 (trm_sel (avar_b _) _) |- _ =>
    exfalso; progressive_inversions
  | _ => idtac
  end.

Ltac lookup_in a G :=
  let InWit := fresh "H" in
  let H := fresh "H" in
  assert (InWit : a `in` dom G) by set solve;
  destruct (get_for_sure G InWit) as [? H];
  pose proof (get_witness _ _ H);
  destruct_conjs.

Ltac expose a T G msg :=
  let wfapp := fresh in
  let wfe := fresh in
  let fvT := fresh in
  let lcT := fresh in
  let er := fresh in
  let x' := fresh "x" in
  assert (wfapp : wf_env (a ~ T ++ G)) by (eapply wf_deapp; routine);
  assert (wfe : wf_env G /\ lc T /\ fv T [<=] dom G) by routine;
  clear wfapp;
  destruct wfe as [wfe [lcT fvT]];
  pose proof (exposure_impl wfe fvT lcT) as er;
  unfold exposure_spec in er;
  refine (x' <- er !!> msg ;; _);
  destruct_conjs.

Ltac measure_weaken H G1 G2 :=
  rewrite (wf_search_weaken G1 G2) in H;
  repeat rewrite app_assoc in *;
  try solve [subst; trivial; set solve].

Ltac shorten_env G :=
  repeat match goal with
         | H : _ = G |- _ => rewrite -> H in *
         | H : G = _ |- _ => rewrite <- H in *
         end.

Local Ltac precise_typ_upper a t tail pf :=
  let H0 := fresh "H" in
  precise_typ_pf a t tail H0;
  [ eapply subtyp_trans;
    [eapply pf;
    eapply weaken_trm; apply H0 | eassumption] |].

Local Ltac precise_typ_lower a t tail pf :=
  let H0 := fresh "H" in
  precise_typ_pf a t tail H0;
  [ eapply subtyp_trans;
    [ eassumption |
      eapply pf;
      eapply weaken_trm; apply H0] |].

Ltac solve_dep_typ a G pf :=
  lookup_in a G;
  match goal with
  | HwfG : wf_env G,
           Hfnd : get a G = Some ?T,
                  Hwit : ?Hd ++ a ~ ?T ++ ?X = G |- _ =>
    expose a T X "subtyper: exposure failed";
    lazymatch goal with
    | Hsub : ?X ⊢ ?T <⦂ ?U |- _ =>
      destr a U "subtyper: unable to handle resulting structure"; [
        ref; subst;
        precise_typ a T X pf;
        repeat instantiate (1 := typ_bot);
        eapply subtyp_trans; solve_by_weaken
      | step;
        lazymatch goal with
        | Hmeas : _ <= wf_search_measure X T |- context[typ_sel (avar_f a) ?A] =>
          repeat measure_weaken Hmeas (Hd ++ a ~ T) X;
          shorten_env G;
          pose proof (measure_shrinks a A HwfG Hfnd)
        end
      ]
    end
  end.

Ltac subgoal T tac :=
  let H := fresh "H" in
  unshelve evar (H : T);
  [| let H' := eval unfold H in H in
         clear H; tac H'].

Ltac rec_pose t :=
  let rec go t' :=
      lazymatch type of t' with
      | forall _ : ?T, _ =>
        subgoal T ltac:(fun gl => go constr:(t' gl))
      | _ => pose proof t'
      end in
  go t.

Ltac rec_subty_upper func G :=
  match goal with
  | Hsub : _ ⊢ _ <⦂ typ_rcd ?DS |-
    Tc (_ ⊢ (typ_sel (avar_f ?a) ?A) <⦂ ?target) =>
    let Heq := fresh "Heq" in
    destruct (lget A DS) eqn:Heq; [| refine (erret "subtyper: a label is not found")];
    apply lget_to_lbinds in Heq;
    decs_facts Heq;
    match goal with
    | H : lbinds _ (dec_typ _ _ ?upper) (_ DS) |- _ =>
      pose proof (wf_decs_lookup_lt_upper G _ _ _ _ _ H);
      pose proof (fv_decs_shrinks _ _ _ H);
      rec_pose (func G upper target); mass_discharge
    end
  end.

Ltac rec_subty_lower func G :=
  match goal with
  | Hsub : _ ⊢ _ <⦂ typ_rcd ?DS |-
    Tc (_ ⊢ ?target <⦂ (typ_sel (avar_f ?a) ?A)) =>
    let Heq := fresh "Heq" in
    destruct (lget A DS) eqn:Heq; [| refine (erret "subtyper: a label is not found")];
    apply lget_to_lbinds in Heq;
    decs_facts Heq;
    match goal with
    | H : lbinds _ (dec_typ _ ?lower _) (_ DS) |- _ =>
      pose proof (wf_decs_lookup_lt_lower G _ _ _ _ _ H);
      pose proof (fv_decs_shrinks _ _ _ H);
      rec_pose (func G target lower); mass_discharge
    end
  end.

Ltac upper_subty G pf :=
  let x := fresh "x" in
  match goal with
  | X : Tc (G ⊢ _ <⦂ ?T),
    HeqG : _ ++ ?v ~ ?V ++ ?Y = G |- Tc (G ⊢ _ <⦂ ?T) =>
    refine (x <- X !!> "subtyper: recursive subtyping call failed." ;; _);
    ref; progressive_inversions;
    precise_typ_upper v V Y pf;
    eapply subtyp_trans;
    [ solve_by_weaken
    | eapply lbinds_subtyp; eassumption]
  end.

Ltac lower_subty G pf :=
  let x := fresh "x" in
  match goal with
  | X : Tc (G ⊢ ?T <⦂ _),
    HeqG : _ ++ ?v ~ ?V ++ ?Y = G |- Tc (G ⊢ ?T <⦂ _) =>
    refine (x <- X !!> "subtyper: recursive subtyping call failed." ;; _);
    ref; progressive_inversions;
    precise_typ_lower v V Y pf;
    eapply subtyp_trans;
    [ solve_by_weaken
    | eapply lbinds_subtyp; eassumption]
  end.

Ltac lookup_from_left func a G :=
  solve_dep_typ a G subtyp_sel2;
  rec_subty_upper func G;
  upper_subty G subtyp_sel2.

Ltac lookup_from_right func a G :=
  solve_dep_typ a G subtyp_sel1;
  rec_subty_lower func G;
  lower_subty G subtyp_sel1.

Ltac solve_wf_decs G T :=
  match goal with
  | H : lbinds _ _ _,
    Heq : T = typ_rcd (decs_cons ?D ?DS) |- _ =>
    try pose proof (wf_decs_lookup_lt_lower G _ _ _ _ _ H);
    try pose proof (wf_decs_lookup_lt_upper G _ _ _ _ _ H);
    try pose proof (wf_decs_lookup_lt_trm G _ _ _ _ H);
    lazymatch D with
    | dec_typ ?l ?U1 ?U2 =>
      assert (wf_search_measure G U1 <
              wf_search_measure G ({<decs_cons (l ∈ U1 ⋯ U2) DS >}));
      [ eapply wf_decs_lookup_lt_lower; left; reflexivity |];
      assert (wf_search_measure G U2 <
              wf_search_measure G ({<decs_cons (l ∈ U1 ⋯ U2) DS >}));
      [ eapply wf_decs_lookup_lt_upper; left; reflexivity |]
    | dec_trm ?l ?U =>
      assert (wf_search_measure G U <
              wf_search_measure G ({<decs_cons (dec_trm l U) DS >}));
      [ eapply wf_decs_lookup_lt_trm; left; reflexivity |]
    end;
    lia
  end.

Definition subtyp_dec_sig G T U :=
  wf_env G ->                    (* environment is wellformed *)
  fv T [<=] dom G ->              (* all free vars are bound *)
  lc T ->                        (* all types are closed *)
  fv U [<=] dom G ->
  lc U ->
  Tc (G ⊢ T <⦂ U).
Arguments subtyp_dec_sig G T U/.

Definition subtyp_checker :=
  forall G T U, subtyp_dec_sig G T U.
Arguments subtyp_checker/.

Definition subty_weight G T U :=
  wf_search_measure G T + wf_search_measure G U.
Arguments subty_weight G T U/.
Arguments wf_search_measure : simpl never.

(** subtyping decision procedure.
 * 
 * G : the context
 * T : the supposed subtype
 * U : the supposed supertype
 *)
Program Fixpoint subty_dec G T U {measure (subty_weight G T U)} :
  subtyp_dec_sig G T U := _.
Next Obligation.
  destruct T eqn:?; destruct U eqn:?;
    lazymatch goal with
    | _ : T = typ_bot |- _ => ref; apply subtyp_bot (* trivial case: bot <: T *)
    | _ : U = typ_top |- _ => ref; apply subtyp_top (* trivial case: T <: top *)
    | _ => idtac
    end;
    lazymatch goal with
    | _ : T = typ_top |- _ => st_undec T U (* trivial case: ??? top <: _ *)
    | _ : U = typ_bot |- _ => st_undec T U (* trivial case: ??? _ <: bot *)
    | _ => idtac
    end;
    repeat lazymatch goal with
           | a : avar |- _ => destruct a; lc_bv
           end.
  
  (* remaining 9 cases left. *)

  - destruct (a == a0); [subst |]. destruct (t == t0).
    ref; subst; apply subtyp_refl.
    
    lookup_from_left subty_dec a0 G.
    lookup_from_left subty_dec a G.
  - lookup_from_left subty_dec a G.
  - lookup_from_left subty_dec a G.
  - lookup_from_right subty_dec a G.
  - destruct (t1 == t3); [ subst | exact (erret "subtyper: the input types do not match")].
    pick_fresh z. simpl in *.
    rec_pose (subty_dec (z ~ t3 ++ G) (open z t2) (open z t4)).
    + repeat (rewrite measure_unfold_all with (x := z); mass_discharge).
    + progressive_inversions.
      constructor; mass_discharge.
    + pose proof (fv_open_typ t2 z 0).
      etransitivity; [eassumption |].
      set solve.
    + invert H1. subst.
      apply open_lc_typ. trivial.
    + pose proof (fv_open_typ t4 z 0).
      etransitivity; [eassumption |].
      set solve.
    + invert H3. subst.
      apply open_lc_typ. trivial.
    + refine (x <- H4 !!> "subtyper: the body types failed" ;; _).
      ref. eapply subtyp_all; [ apply subtyp_refl |].
      cofinite.
      apply (@open_subst_subtyp z); simpl in *; set solve; auto.
  - st_undec T U.             (* don't know how to compare typ_all and typ_rcd *)
  - lookup_from_right subty_dec a G.
  - st_undec T U.             (* don't know how to compare typ_rcd and typ_all *)
  - destruct d0.
    + ref. change d with (append' decs_nil d).
      apply subtyp_drop2_gen.
    + pose proof (wf_measure_decs_cons G d0 d1).
      rec_pose (subty_dec G (typ_rcd d) (typ_rcd d1)); mass_discharge.
      destruct d0.
      * destruct (lget t d) eqn:?;
        [| exact (erret "subtyper: a label is not found")].
        apply lget_to_lbinds in Heqo.
        decs_facts Heqo.
        pose_fv_decs_lkup t3 G. pose_fv_decs_lkup t4 G.

        rec_pose (subty_dec G t0 t3); mass_discharge; [solve_wf_decs G U |].
        rec_pose (subty_dec G t4 t1); mass_discharge; [solve_wf_decs G U |].
        refine (x0 <- H12 !!> "subtyper: lower bound comparison failed";;
                   x1 <- H13 !!> "subtyper: upper bound comparison failed";;
                   x <- H5 !!> "subtyper: recursive comparison for declarations failed";;
                   _).
        ref. list_reasoning.
        apply subtyp_merge; trivial.
        eapply subtyp_trans.
        eapply lbinds_subtyp. eassumption.
        progressive_inversions. apply subtyp_typ; trivial.
      * destruct (lget t d) eqn:?;
        [| exact (erret "subtyper: a label is not found")].
        apply lget_to_lbinds in Heqo.
        decs_facts Heqo.
        pose_fv_decs_lkup t2 G.
        rec_pose (subty_dec G t2 t0); mass_discharge; [ solve_wf_decs G U |].
        refine (x0 <- H10 !!> "subtyper: member type comparison failed";;
                   x <- H5 !!> "subtyper: recursive comparison for declarations failed";;
                   _).
        ref. list_reasoning.
        apply subtyp_merge; trivial.
        eapply subtyp_trans.
        eapply lbinds_subtyp. eassumption.
        progressive_inversions. apply subtyp_fld; trivial.
Defined.

Ltac invref := ref; subst; progressive_inversions; simpl in *.
Ltac refsolve := invref; try solve [routine].

Ltac precise_typ_var a G :=
  match goal with
  | Heq : ?Hd ++ a ~ ?T ++ ?Tl = G |- _ =>
    let H := fresh "H" in
    precise_typ_pf a T Tl H;
    [ | apply subtyp_refl];
    eapply weaken_trm in H;
    match type of H with
    | ?Pre ++ a ~ T ++ Tl ⊢ _ ⦂ ?T' =>
      unify Pre Hd; unify T' T
    end
  end.

Local Hint Constructors uniq_dom_val.
Local Hint Resolve uniq_dom_val_decons.
Local Hint Resolve uniq_dom_trm_open.

(** the specification of type checking algorithm.
 * 
 * G : the context
 * t : the term
 * T : the result type
 *)
Definition typing_spec G t T :=
  (G ⊢ t ⦂ T) /\                 (* guarantees typing relation *)
  fv T [<=] dom G /\              (* free vars of result type is bound. *)
  lc T.                         (* result type is closed *)
Arguments typing_spec G t T/.
      
(** the actual type checking procedure.
 * 
 * G : the context
 * t : the term
 *)
Program Fixpoint typing_impl_template (subtyper : subtyp_checker) G t
        {measure (trm_struct_measure t)} :
  wf_env G ->                    (* context is well formed *)
  fv t [<=] dom G ->              (* free vars are bound *)
  lc t ->                        (* term is locally closed. *)
  uniq_dom_trm t ->              (* all trms define uniq domain, recursively *)
  Tc {T : typ | typing_spec G t T } := _.
Next Obligation.
  pose proof (typing_impl_template subtyper) as typing_impl.
  destruct t.
  - destruct a; lc_bv.
    lookup_in a G.
    assert (wf_env (a ~ x ++ X)) by (eapply wf_deapp; routine).
    refsolve. step. set solve.
  - destruct v.
    + assert (forall ds, trm_struct_measure (trm_val ({[ ds ]}))
                    <= trm_struct_measure (trm_val ({[ d ]})) ->
                  fv ds [<=] dom G ->
                  lc ({[ ds ]}) ->
                  uniq_dom_val ({[ ds ]}) ->
                  Tc { DS | G ⊩[ ds ⦂ DS ] /\ lc DS /\ fv DS [<=] dom G }). {
        induction on defs; intros.
        - ref. repeat constructor. all:mass_discharge.
        - rec_pose IHdefs; mass_discharge.
          destruct d0.
          + ref_body H8 "typer: recursive call for a record failed". ref.
            repeat constructor; try eassumption.
            routine.
            set solve.
          + rec_pose (typing_impl G t0); mass_discharge.
            refine (x0 <- H9 !!> "typer: failed to type check a data member";;
                       x <- H8 !!> "typer: recursive call for a record failed" ;; _).
            destruct_conjs.
            ref. repeat constructor; try eassumption.
            routine.
            set solve.
      }
      assert (luniq d) by routine.
      rec_pose (H3 d); mass_discharge.
      ref_body H5 "typer: record failed". ref. constructor. eassumption.
      all:set solve; routine.
    + pick_fresh z.
      rec_pose (typing_impl (z ~ t ++ G) (open z t0));
        progressive_inversions.
      * rewrite open_trm_same_measure. simpl. lia.
      * constructor; mass_discharge.
      * pose proof (fv_open_trm t0 z 0).
        etransitivity; [eassumption |].
        set solve.
      * apply open_lc_trm. trivial.
      * auto.
      * simpl in H3. ref_body H3 "typer: function body failed.".
        invref.
        -- econstructor.
           intros.
           cofinite.
           rewrite <- (@open_left_inv_typ x 0 z) in H4 by trivial.
           assert (z `notin` fv (close z x)) by apply fv_close_self_typ.
           assert (x0 `notin` fv (close z x)). {
             eapply notin_subset_relax.
             apply fv_close_typ. auto.
           }
           eapply (@open_subst_trm z); try eassumption.
           apply wf_uniq; trivial.
           all:solve_notin.
        -- pose proof (fv_add_close_typ x G 0 H5).
           set solve.
        -- constructor; trivial. 
           apply close_lc_typ; trivial.
  - destruct a; lc_bv.
    lookup_in a G.
    expose a x X "typer: exposure failed for member selection".
    precise_typ_var a G.
    shorten_env G.
    destruct x0 eqn:?;
             lazymatch goal with
             | _ : x0 = typ_bot |- _ => idtac
             | _ : x0 = typ_rcd _ |- _ => idtac
             | _ : x0 = ?t |- _ => exact (erret "typer: tried to lookup non-record type")
             end.
    + ref. econstructor.
      instantiate (1 := typ_bot).

      eapply ty_sub. eassumption.
      eapply subtyp_trans. subst.
      reassoc 3 with 2. solve_by_weaken. 
      all:mass_discharge.
    + destruct (lget t d) eqn:?; [| exact (erret "typer: a label is not found")].
      apply lget_to_lbinds in Heqo.
      decs_facts Heqo.
      pose_fv_decs_lkup t1 X.
      ref. constructor. eapply ty_sub. eapply ty_sub. eassumption.
      subst. reassoc 3 with 2. solve_by_weaken.
      subst x0. eapply lbinds_subtyp. progressive_inversions. eassumption.
      all:mass_discharge.
  - destruct a; destruct a0;
      lazymatch goal with
      | _ : nat |- _ => exfalso; progressive_inversions; lc_bv
      | _ => idtac
      end.

    lookup_in a G.
    expose a x X "typer: exposure failed in application case".
    lookup_in a0 G.
    precise_typ_var a0 G.
    assert (G ⊢ (trm_var $ avar_f a) ⦂ x0). {
      precise_typ_var a G. subst.
      eapply ty_sub. eassumption.
      reassoc 3 with 2. solve_by_weaken.
    }

    shorten_env G;
      destruct x0 eqn:?;
               lazymatch goal with
               | _ : x0 = typ_bot |- _ => idtac
               | _ : x0 = typ_all _ _ |- _ => idtac
               | _ : x0 = ?t |- _ => exact (erret "typer: not a function in application case")
               end.
    + ref. econstructor.
      instantiate (1 := typ_bot).

      eapply ty_sub. eassumption.
      all:mass_discharge.
    + assert (wf_env (a0 ~ x1 ++ X0)). {
        eapply wf_deapp. rewrite <- H19 in *; eassumption.
      }
      assert (fv x1 [<=] dom X0 /\ lc x1) by routine.
      destruct_conjs.
      rec_pose (subtyper G x1 t1); trivial.
      * rewrite <- H19 in *. step. set solve.
      * simpl in *. rewrite <- H6 in *. 
        step. simpl. set solve.
      * routine.
      * refine (sub <- H26 !!> "typer: input subtyping failed in application case" ;; _).
        ref. 
        -- econstructor. eassumption. eauto using ty_sub.
        -- assert (a0 `in` dom G) by (subst; set solve).
           pose proof (fv_open_typ t2 a0 0).
           etransitivity; [eassumption |].
           apply AtomSetProperties.union_subset_3.
           set solve. subst. step. set solve.
        -- apply open_lc_typ. routine.
  - rec_pose (typing_impl G t1); mass_discharge. 
    ref_body H3 "typer: let binding type failed".
    pick_fresh z.
    rec_pose (typing_impl (z ~ x ++ G) (open z t2)).
    + rewrite open_trm_same_measure. simpl. lia.
    + constructor; mass_discharge.
    + pose proof (fv_open_trm t2 z 0).
      etransitivity; [eassumption |].
      set solve.
    + apply open_lc_trm. routine.
    + routine.
    + ref_body H7 "typer: let body type failed".
      rec_pose (@promotion (z ~ x ++ G) z x0); mass_discharge.
      constructor; mass_discharge.
      unfold promo_spec in H11.
      ref_body H11 "typer: promote body type failed". ref.
      * eapply ty_let. eassumption.
        cofinite. instantiate (1 := x1).
        rewrite <- open_lc_le_typ with (m := 0) (n := 0) (x := x2)
          by mass_discharge.
        apply open_subst_trm with (x := z); mass_discharge.
        erewrite open_lc_le_typ by mass_discharge.
        eapply ty_sub. eassumption. trivial.
      * autounfold in *. intros.
        specialize (H15 _ H16). simpl in *.
        apply add_iff in H15. destruct H15; [subst | assumption].
        intuition.
      * trivial.
Defined.

Definition typecheck_template' (subtyper : subtyp_checker) (t : trm) :
  fv t [=] empty -> lc t -> uniq_dom_trm t -> Tc {T : typ | typing_spec nil t T }.
Proof.
  intros. apply typing_impl_template; trivial.
  rewrite H. set solve.
Defined.

(** we then needs to discharge that two extra predicates which are decidable. *)

Definition typecheck_template (subtyper : subtyp_checker) (t : trm) :
  Tc {T : typ | typing_spec nil t T } :=
  match check_fv_trm t, check_lc_at_trm 0 t, uniq_dom_trm_dec t with
  | left fvbp, left lcp, left udt => typecheck_template' subtyper fvbp lcp udt
  | right nfvbp, _, _ => erret "sanity: input term is not closed."
  | _, right nlcp, _ => erret "sanity: input term is not locally closed."
  | _, _, right nudt => erret "sanity: input term does not have unique domain."
  end.

Definition typecheck (t : trm) : Tc {T : typ | typing_spec nil t T } :=
  typecheck_template subty_dec t.