Require Import Definitions.

Section Weakening.

  Local Notation weakening ctor :=
    (forall G1 G2 G3 t T,
        ctor (G1 ++ G3) t T -> 
        ctor (G1 ++ G2 ++ G3) t T).

  (*
   * to tackle this lemma, it's a bit tricky, as we need
   * to understand what's the condition for discharging 
   * both ty rules and subtyp rules.
   *)
  
  (** this tactic massages the goal and then apply the
   * proper induction hypothesis in the context *)
  Local Ltac ind_massage :=
    simpl; match goal with
    | [ |- context[_ :: _ ++ _ ++ _]] =>
      cofinite
    | _ =>
      intros; eauto
    end;
    match goal with
    | [ H : context[_ `notin` _ -> _] |- _ ] =>
      reassoc 4 with 2;
      match type of H with
      | context[_ ++ _ ++ _] =>
          eapply H; simpl_env
      end
    end.

  (** master tactic for this lemma *)
  Local Ltac boom := ind_massage; auto.

  Lemma weaken_trm_gen : weakening ty_trm
  with weaken_def_gen : weakening ty_def
  with weaken_defs_gen : weakening ty_defs
  with weaken_subtyp_gen : weakening subtyp.
  Proof.
    - clear weaken_trm_gen.
      intros. gen G2. dependent induction H; intros.
      all:typing undec; boom.
    - clear weaken_def_gen.
      intros. gen G2. dependent induction H; intros.
      all:typing undec; boom.
    - clear weaken_defs_gen.
      intros. gen G2. dependent induction H; intros.
      all:typing undec; boom.
    - clear weaken_subtyp_gen.
      intros. gen G2. dependent induction H; intros.
      all:typing undec; boom.
  Qed.

End Weakening.

Lemma weaken_trm: forall G G' t T,
    G ⊢ t ⦂ T ->
    G' ++ G ⊢ t ⦂ T.
Proof.
  intros; reassoc 2 with 0; eapply weaken_trm_gen; try eassumption; trivial.
Qed.

Lemma weaken_subtyp: forall G G' T1 T2,
    G ⊢ T1 <⦂ T2 ->
    G' ++ G ⊢ T1 <⦂ T2.
Proof.
  intros; reassoc 2 with 0; eapply weaken_subtyp_gen; try eassumption; trivial.
Qed.
Hint Resolve weaken_trm weaken_subtyp.
