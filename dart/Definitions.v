Set Implicit Arguments.

Require Export StdSetup.

Inductive avar : Set :=
| avar_b : nat -> avar
| avar_f : var -> avar.

Hint Constructors avar.

Coercion avar_b : nat >-> avar.
Coercion avar_f : var >-> avar.

Instance EqAvar : EqDec_eq avar := { }.
Proof. decide equality. apply Nat.eq_dec. Defined.

Inductive typ : Set :=
| typ_top : typ
| typ_bot : typ
| typ_sel : avar -> typ_label -> typ
| typ_all : typ -> typ -> typ
| typ_rcd : decs -> typ
with
dec : Set :=
(** dec_typ X A B ::= X : A .. B *)
| dec_typ : typ_label -> typ -> typ -> dec
(** dec_trm x T ::= x : T *)
| dec_trm : trm_label -> typ -> dec
with
decs : Set :=
| decs_nil : decs
| decs_cons : dec -> decs -> decs.
Hint Constructors typ dec decs.

Notation "⊤" := typ_top.
Notation "⊥" := typ_bot.
Notation "x ⋅ T" := (typ_sel x T) (at level 40).
Notation "all( A ) B" := (typ_all A B) (at level 40).

Notation "X ∈ A ⋯ B" := (dec_typ X A B) (at level 40).
Notation "x ∷ T" := (dec_trm x T) (at level 40).
Notation "{< DS >}" := (typ_rcd DS) (at level 40).

Definition lab_dec_of D : label :=
  match D with
  | dec_typ A _ _ => A
  | dec_trm a _ => a
  end.

Inductive wf_lab_dec : label * dec -> Prop :=
| wf_ldec_typ : forall X A B, wf_lab_dec (label_typ X, X ∈ A ⋯ B)
| wf_ldec_trm : forall x T, wf_lab_dec (label_trm x, x ∷ T).
Hint Constructors wf_lab_dec.

Fixpoint decs_to_list' (DS : decs) :=
  match DS with
  | decs_nil => nil
  | decs_cons d DS' => (lab_dec_of d, d) :: decs_to_list' DS'
  end.

Fixpoint decs_from_list' (l : list (label * dec)) :=
  match l with
  | nil => decs_nil
  | cons (l, d) l' => decs_cons d $ decs_from_list' l'
  end.

Fixpoint decs_append' (DS1 : decs) (DS2 : decs) : decs :=
  match DS1 with
  | decs_nil => DS2
  | decs_cons D DS1' => decs_cons D $ decs_append' DS1' DS2
  end.

Instance DecsListCond : ListIsoCond (label * dec) decs :=
  {
    to_list' := decs_to_list';
    from_list' := decs_from_list';
    append' := decs_append';
    cond := wf_lab_dec
  }.
Proof.
  all:induction on decs || induction on list; routine.
  - invert H3; routine.
  - constructor; trivial.
    destruct d; routine.
Defined.

Coercion decs_to_list' : decs >-> list.

Fixpoint decs_to_list (DS : decs) :=
  match DS with
  | decs_nil => nil
  | decs_cons D DS' => cons D $ decs_to_list DS'
  end.

Fixpoint decs_from_list (l : list dec) :=
  match l with
  | nil => decs_nil
  | cons D l' => decs_cons D (decs_from_list l')
  end.

Fixpoint decs_append (DS1 DS2 : decs) :=
  match DS1 with
  | decs_nil => DS2
  | decs_cons D DS1' => decs_cons D (decs_append DS1' DS2)
  end.

Instance DecsList : ListIso dec decs :=
  {
    from_list := decs_from_list;
    to_list := decs_to_list;
    append := decs_append
  }.
Proof.
  all:induction on decs || induction on list; routine.
Defined.

Fixpoint typ_eqdec (T1 T2 : typ) : {T1 = T2} + {T1 <> T2}
with dec_eqdec (D1 D2 : dec) : {D1 = D2} + {D1 <> D2}
with decs_eqdec (DS1 DS2 : decs) : {DS1 = DS2} + {DS1 <> DS2}.
Proof.
  - decide equality.
    destruct (t == t0); auto.
    destruct (a == a0); auto.
  - decide equality.
    destruct (t == t2); auto.
    destruct (t == t1); auto.
  - decide equality.
Defined.

Instance EqDecTyp : EqDec_eq typ := { }.
Proof. exact typ_eqdec. Defined.

Definition wf_decs (l : decs) := luniq l.
Hint Unfold wf_decs.
Hint Transparent wf_decs.

Inductive trm : Set :=
| trm_var : avar -> trm
| trm_val : val -> trm
| trm_sel : avar -> trm_label -> trm
| trm_app : avar -> avar -> trm
| trm_let : trm -> trm -> trm
with
val : Set :=
| val_rcd : defs -> val
| val_lam : typ -> trm -> val
with
def : Set :=
| def_typ : typ_label -> typ -> def
| def_trm : trm_label -> trm -> def
with
defs : Set :=
| defs_nil : defs
| defs_cons : def -> defs -> defs.

Hint Constructors trm val def defs.

Definition lab_def_of D : label :=
  match D with
  | def_typ A _ => A
  | def_trm a _ => a
  end.

Inductive wf_lab_def : label * def -> Prop :=
| wf_ldef_typ : forall X T, wf_lab_def (label_typ X, def_typ X T)
| wf_ldef_trm : forall x t, wf_lab_def (label_trm x, def_trm x t).
Hint Constructors wf_lab_def.

Fixpoint defs_to_list' (DS : defs) :=
  match DS with
  | defs_nil => nil
  | defs_cons d DS' => (lab_def_of d, d) :: defs_to_list' DS'
  end.

Fixpoint defs_from_list' (l : list (label * def)) :=
  match l with
  | nil => defs_nil
  | cons (l, d) l' => defs_cons d $ defs_from_list' l'
  end.

Fixpoint defs_append' (DS1 : defs) (DS2 : defs) : defs :=
  match DS1 with
  | defs_nil => DS2
  | defs_cons D DS1' => defs_cons D $ defs_append' DS1' DS2
  end.

Instance DefsListCond : ListIsoCond (label * def) defs :=
  {
    to_list' := defs_to_list';
    from_list' := defs_from_list';
    append' := defs_append';
    cond := wf_lab_def
  }.
Proof.
  all:induction on defs || induction on list; routine.
  - invert H3; routine.
  - constructor; trivial.
    destruct d; routine.
Defined.

Coercion defs_to_list' : defs >-> list.

Fixpoint defs_to_list (DS : defs) :=
  match DS with
  | defs_nil => nil
  | defs_cons D DS' => cons D $ defs_to_list DS'
  end.

Fixpoint defs_from_list (l : list def) :=
  match l with
  | nil => defs_nil
  | cons D l' => defs_cons D (defs_from_list l')
  end.

Fixpoint defs_append (DS1 DS2 : defs) :=
  match DS1 with
  | defs_nil => DS2
  | defs_cons D DS1' => defs_cons D (defs_append DS1' DS2)
  end.

Instance DefsList : ListIso def defs :=
  {
    from_list := defs_from_list;
    to_list := defs_to_list;
    append := defs_append
  }.
Proof.
  all:induction on defs || induction on list; routine.
Defined.

Notation "'lett' x 'inn' y" := (trm_let x y) (at level 40).
Notation "A ≡ B" := (A, def_typ B) (at level 40).
Notation "x ⩴ t" := (x, def_trm t) (at level 40).
Notation "{[ ds ]}" := (val_rcd ds) (at level 40).
Notation "λ( T ){ t }" := (val_lam T t) (at level 40).

Definition wf_defs (l : defs) := luniq l.
Hint Unfold wf_defs.
Hint Transparent wf_defs.

(** OPENING *)
Section OpeningDefinition.
  
  Definition open_rec_avar (k : nat) (u : var) (a : avar) : avar :=
    match a with
    | avar_b i => if k == i then avar_f u else avar_b i
    | avar_f x => avar_f x
    end.

  Fixpoint open_rec_typ (k : nat) (u : var) (T : typ) : typ :=
    match T with
    | ⊤ => ⊤
    | ⊥ => ⊥
    | x ⋅ T => (open_rec_avar k u x) ⋅ T
    | all( T ) U => all( open_rec_typ k u T ) open_rec_typ (S k) u U
    | {< DS >} => {< open_rec_decs k u DS >}
    end
  with
  open_rec_dec (k : nat) (u : var) (D : dec) : dec :=
    match D with
    | dec_typ X T U => dec_typ X (open_rec_typ k u T) $ open_rec_typ k u U
    | dec_trm x T => dec_trm x $ open_rec_typ k u T
    end
  with
  open_rec_decs (k : nat) (u : var) (DS : decs) : decs :=
    match DS with
    | decs_nil => decs_nil
    | decs_cons D DS' =>
      decs_cons (open_rec_dec k u D) $ open_rec_decs k u DS'
    end.

  Fixpoint open_rec_trm (k : nat) (u : var) (t : trm) : trm :=
    match t with
    | trm_var a => trm_var $ open_rec_avar k u a
    | trm_val v => trm_val $ open_rec_val k u v
    | trm_sel v m => trm_sel (open_rec_avar k u v) m
    | trm_app f x => trm_app (open_rec_avar k u f) $ open_rec_avar k u x
    | trm_let t1 t2 => trm_let (open_rec_trm k u t1) $ open_rec_trm (S k) u t2
    end
  with
  open_rec_val (k : nat) (u : var) (v : val) : val :=
    match v with
    | λ( T ){ e } => λ( open_rec_typ k u T ){ open_rec_trm (S k) u e }
    | {[ ds ]} => {[ open_rec_defs k u ds ]}
    end
  with
  open_rec_def (k : nat) (u : var) (d : def) : def :=
    match d with
    | def_typ X T => def_typ X $ open_rec_typ k u T
    | def_trm x e => def_trm x $ open_rec_trm k u e
    end
  with
  open_rec_defs (k : nat) (u : var) (ds : defs) : defs :=
    match ds with
    | defs_nil => defs_nil
    | defs_cons df ds' => defs_cons (open_rec_def k u df) $ open_rec_defs k u ds'
    end.
    
End OpeningDefinition.

Instance OpenAvar : CanOpen avar := { open_rec := open_rec_avar }.
Instance OpenTyp : CanOpen typ := { open_rec := open_rec_typ }.
Instance OpenDec : CanOpen dec := { open_rec := open_rec_dec }.
Instance OpenDecs : CanOpen decs := { open_rec := open_rec_decs }.
Instance OpenTrm : CanOpen trm := { open_rec := open_rec_trm }.
Instance OpenVal : CanOpen val := { open_rec := open_rec_val }.
Instance OpenDef : CanOpen def := { open_rec := open_rec_def }.
Instance OpenDefs : CanOpen defs := { open_rec := open_rec_defs }.

(** CLOSING *)
Section ClosingDefinition.
  
  Definition close_rec_avar (k : nat) (u : var) (a : avar) : avar :=
    match a with
    | avar_b i => avar_b i
    | avar_f x => if x == u then avar_b k else avar_f x
    end.

  Fixpoint close_rec_typ (k : nat) (u : var) (T : typ) : typ :=
    match T with
    | ⊤ => ⊤
    | ⊥ => ⊥
    | x ⋅ T => (close_rec_avar k u x) ⋅ T
    | all( T ) U => all( close_rec_typ k u T ) close_rec_typ (S k) u U
    | {< DS >} => {< close_rec_decs k u DS >}
    end
  with
  close_rec_dec (k : nat) (u : var) (D : dec) : dec :=
    match D with
    | dec_typ X T U => dec_typ X (close_rec_typ k u T) $ close_rec_typ k u U
    | dec_trm x T => dec_trm x $ close_rec_typ k u T
    end
  with
  close_rec_decs (k : nat) (u : var) (DS : decs) : decs :=
    match DS with
    | decs_nil => decs_nil
    | decs_cons D DS' =>
      decs_cons (close_rec_dec k u D) $ close_rec_decs k u DS'
    end.

  Fixpoint close_rec_trm (k : nat) (u : var) (t : trm) : trm :=
    match t with
    | trm_var a => trm_var $ close_rec_avar k u a
    | trm_val v => trm_val $ close_rec_val k u v
    | trm_sel v m => trm_sel (close_rec_avar k u v) m
    | trm_app f x => trm_app (close_rec_avar k u f) $ close_rec_avar k u x
    | trm_let t1 t2 => trm_let (close_rec_trm k u t1) $ close_rec_trm (S k) u t2
    end
  with
  close_rec_val (k : nat) (u : var) (v : val) : val :=
    match v with
    | λ( T ){ e } => λ( close_rec_typ k u T ){ close_rec_trm (S k) u e }
    | {[ ds ]} => {[ close_rec_defs k u ds ]}
    end
  with
  close_rec_def (k : nat) (u : var) (d : def) : def :=
    match d with
    | def_typ X T => def_typ X $ close_rec_typ k u T
    | def_trm x e => def_trm x $ close_rec_trm k u e
    end
  with
  close_rec_defs (k : nat) (u : var) (ds : defs) : defs :=
    match ds with
    | defs_nil => defs_nil
    | defs_cons df ds' => defs_cons (close_rec_def k u df) $ close_rec_defs k u ds'
    end.
    
End ClosingDefinition.

Instance CloseAvar : CanClose avar := { close_rec := close_rec_avar }.
Instance CloseTyp : CanClose typ := { close_rec := close_rec_typ }.
Instance CloseDec : CanClose dec := { close_rec := close_rec_dec }.
Instance CloseDecs : CanClose decs := { close_rec := close_rec_decs }.
Instance CloseTrm : CanClose trm := { close_rec := close_rec_trm }.
Instance CloseVal : CanClose val := { close_rec := close_rec_val }.
Instance CloseDef : CanClose def := { close_rec := close_rec_def }.
Instance CloseDefs : CanClose defs := { close_rec := close_rec_defs }.

Section FreeVariables.

  Definition fv_avar (a : avar) : atoms :=
    match a with
    | avar_b i => {}
    | avar_f x => {{ x }}
    end.

  Fixpoint fv_typ (T : typ) : atoms :=
    match T with
    | ⊤ => {}
    | ⊥ => {}
    | x ⋅ T => fv_avar x
    | all( T ) U => fv_typ T `union` fv_typ U
    | {< DS >} => fv_decs DS
    end
  with
  fv_dec (D : dec) : atoms :=
    match D with
    | dec_typ X T U => fv_typ T `union` fv_typ U
    | dec_trm x T => fv_typ T
    end
  with
  fv_decs (DS : decs) : atoms :=
    match DS with
    | decs_nil => {}
    | decs_cons D DS' => fv_dec D `union` fv_decs DS'
    end.

  Fixpoint fv_trm (t : trm) : atoms :=
    match t with
    | trm_var a => fv_avar a
    | trm_val v => fv_val v
    | trm_sel v m => fv_avar v
    | trm_app f x => fv_avar f `union` fv_avar x
    | trm_let t1 t2 => fv_trm t1 `union` fv_trm t2
    end
  with
  fv_val (v : val) : atoms :=
    match v with
    | λ( T ){ e } => fv_typ T `union` fv_trm e
    | {[ ds ]} => fv_defs ds
    end
  with
  fv_def (d : def) : atoms :=
    match d with
    | def_typ _ T => fv_typ T
    | def_trm _ e => fv_trm e
    end
  with
  fv_defs (ds : defs) : atoms :=
    match ds with
    | defs_nil => {}
    | defs_cons d ds' => fv_def d `union` fv_defs ds'
    end.

  Definition fv_values {T : Type} (f : T -> atoms)
             (l : list (atom * T)) : atoms :=
    fold_right (fun (b : (atom * T)) a =>
                  a `union` let (_, t) := b in f t) {} l.

  Lemma fv_unpack : forall T (f : T -> _) e1 e2,
      fv_values f (e1 ++ e2) [=] fv_values f e1 `union` fv_values f e2.
  Proof.
    induction on list; intros. simpl.
    - set solve.
    - routine.
      + rewrite IHlist in H2. solve_in.
      + rewrite IHlist. solve_in.
  Qed.

End FreeVariables.
Hint Rewrite -> dom_app fv_unpack : meta_ext.

Instance FvAvar : HasFv avar := { fv := fv_avar }.
Instance FvTyp : HasFv typ := { fv := fv_typ }. 
Instance FvDec : HasFv dec := { fv := fv_dec }. 
Instance FvDecs : HasFv decs := { fv := fv_decs }.
Instance FvTrm : HasFv trm := { fv := fv_trm }.
Instance FvVal : HasFv val := { fv := fv_val }.
Instance FvDef : HasFv def := { fv := fv_def }.
Instance FvDefs : HasFv defs := { fv := fv_defs }.

Notation env := (list (atom * typ)).
Notation sta := (list (atom * val)).

Instance FvEnv : HasFv env := { fv := fun e => dom e `union` fv_values fv e }.
Instance FvSta : HasFv sta := { fv := fun s => dom s `union` fv_values fv s }.

Lemma fv_union : forall (e1 e2 : env), fv (e1 ++ e2) [=] fv e1 `union` fv e2.
Proof.
  intros; simpl. 
  apply AtomSetProperties.subset_antisym;
    set solve.
Qed.

Section LocalClosure.

  Inductive lc_avar_at : nat -> avar -> Prop :=
  | lc_ab : forall {n m}, n < m -> lc_avar_at m (avar_b n)
  | lc_af : forall {n x}, lc_avar_at n (avar_f x).

  Global Instance LcAvar : LC avar := { lc_at := lc_avar_at }.
  
  Inductive lc_typ_at : nat -> typ -> Prop :=
  | lc_typ_top : forall {n}, lc_typ_at n typ_top
  | lc_typ_bot : forall {n}, lc_typ_at n typ_bot
  | lc_typ_sel : forall {n v A}, lc_at n v -> lc_typ_at n (typ_sel v A)
  | lc_typ_all : forall {n T U}, lc_typ_at n T -> lc_typ_at (S n) U ->
                            lc_typ_at n (typ_all T U)
  | lc_typ_rcd : forall {n DS}, lc_decs_at n DS -> lc_typ_at n (typ_rcd DS)
  with
  lc_dec_at : nat -> dec -> Prop :=
  | lc_dec_typ : forall {n T U X}, lc_typ_at n T -> lc_typ_at n U ->
                              lc_dec_at n (dec_typ X T U)
  | lc_dec_trm : forall {n T x}, lc_typ_at n T ->
                            lc_dec_at n (dec_trm x T)
  with
  lc_decs_at : nat -> decs -> Prop :=
  | lc_decs_nil : forall {n}, lc_decs_at n decs_nil
  | lc_decs_cons : forall {n D DS}, lc_dec_at n D -> lc_decs_at n DS ->
                               lc_decs_at n (decs_cons D DS).

  Global Instance LcTyp : LC typ := { lc_at := lc_typ_at }.
  Global Instance LcDec : LC dec := { lc_at := lc_dec_at }.
  Global Instance LcDecs : LC decs := { lc_at := lc_decs_at }.

  Inductive lc_trm_at : nat -> trm -> Prop :=
  | lc_trm_var : forall {n v}, lc_at n v -> lc_trm_at n (trm_var v)
  | lc_trm_val : forall {n vl}, lc_val_at n vl -> lc_trm_at n (trm_val vl)
  | lc_trm_sel : forall {n a t}, lc_at n a -> lc_trm_at n (trm_sel a t)
  | lc_trm_app : forall {n x y}, lc_at n x -> lc_at n y -> lc_trm_at n (trm_app x y)
  | lc_trm_let : forall {n t1 t2}, lc_trm_at n t1 -> lc_trm_at (S n) t2 ->
                              lc_trm_at n (trm_let t1 t2)
  with
  lc_val_at : nat -> val -> Prop :=
  | lc_val_bnd : forall {n dfs}, lc_defs_at n dfs -> lc_val_at n (val_rcd dfs)
  | lc_val_lam : forall {n T t}, lc_at n T -> lc_trm_at (S n) t ->
                            lc_val_at n (val_lam T t)
  with
  lc_def_at : nat -> def -> Prop :=
  | lc_def_typ : forall {n T X}, lc_typ_at n T -> lc_def_at n (def_typ X T)
  | lc_def_trm : forall {n t x}, lc_trm_at n t -> lc_def_at n (def_trm x t)
  with
  lc_defs_at : nat -> defs -> Prop :=
  | lc_defs_nil : forall {n}, lc_defs_at n defs_nil
  | lc_defs_cons : forall {n D DS}, lc_def_at n D -> lc_defs_at n DS ->
                               lc_defs_at n (defs_cons D DS).

  Global Instance LcTrm : LC trm := { lc_at := lc_trm_at }.
  Global Instance LcVal : LC val := { lc_at := lc_val_at }.
  Global Instance LcDef : LC def := { lc_at := lc_def_at }.
  Global Instance LcDefs : LC defs := { lc_at := lc_defs_at }.

End LocalClosure.

Reserved Notation "G '⊢' t '⦂' T" (at level 70, t at level 79).
Reserved Notation "G '⊢' T '<⦂' U" (at level 70, T at level 79).
Reserved Notation "G ⊩ d ⦂ D" (at level 70, d at level 79).
Reserved Notation "G ⊩[ ds ⦂ DS ]" (at level 70, ds at level 79).


Inductive ty_trm : env -> trm -> typ -> Prop :=
| ty_var : forall G x T,
    binds x T G ->
    G ⊢ trm_var x ⦂ T

| ty_all_intro : forall L G T t U,
    (forall x, x `notin` L ->
          x ~ T ++ G ⊢ open x t ⦂ open x U) ->
    G ⊢ trm_val (λ( T ){ t }) ⦂ all( T ) U
| ty_all_elim : forall G (x z : atom) S T,
    G ⊢ trm_var x ⦂ all( S ) T ->
    G ⊢ trm_var z ⦂ S ->
    G ⊢ (trm_app x z) ⦂ open z T

| ty_rcd_intro : forall G ds DS,
    G ⊩[ ds ⦂ DS ] ->
    wf_defs ds ->
    G ⊢ trm_val ({[ ds ]}) ⦂ {< DS >}
| typ_rcd_elim : forall G (x : var) a T,
    G ⊢ trm_var x ⦂ {< decs_cons (dec_trm a T) decs_nil >} ->
    G ⊢ trm_sel x a ⦂ T

| ty_let : forall L G t u T U,
    G ⊢ t ⦂ T ->
    (forall x, x `notin` L ->
          x ~ T ++ G ⊢ open x u ⦂ U) ->
    G ⊢ lett t inn u ⦂ U
| ty_sub : forall G t T T',
    G ⊢ t ⦂ T ->
    G ⊢ T <⦂ T' ->
    G ⊢ t ⦂ T'
where "G ⊢ t ⦂ T" := (ty_trm G t T) : type_scope

with
ty_def : env -> def -> dec -> Prop :=
| ty_def_typ : forall G A T,
    G ⊩ def_typ A T ⦂ dec_typ A T T
| ty_def_trm : forall G a t T,
    G ⊢ t ⦂ T ->
    G ⊩ def_trm a t ⦂ dec_trm a T
where "G ⊩ d ⦂ D" := (ty_def G d D) : type_scope

with
ty_defs : env -> defs -> decs -> Prop :=
| ty_defs_nil : forall G,
    G ⊩[ defs_nil ⦂ decs_nil ]
| ty_defs_cons : forall G d D ds DS,
    G ⊩ d ⦂ D ->
    G ⊩[ ds ⦂ DS ] ->
    G ⊩[ defs_cons d ds ⦂ decs_cons D DS ]
where "G ⊩[ ds ⦂ DS ]" := (ty_defs G ds DS) : type_scope

with
subtyp : env -> typ -> typ -> Prop :=
| subtyp_top : forall G T,
    G ⊢ T <⦂ ⊤
| subtyp_bot : forall G T,
    G ⊢ ⊥ <⦂ T
| subtyp_refl : forall G T,
    G ⊢ T <⦂ T
| subtyp_trans : forall G S T U,
    G ⊢ S <⦂ T ->
    G ⊢ T <⦂ U ->
    G ⊢ S <⦂ U

| subtyp_all: forall L G S1 T1 S2 T2,
    G ⊢ S2 <⦂ S1 ->
    (forall x, x `notin` L ->
       x ~ S2 ++ G ⊢ open x T1 <⦂ open x T2) ->
    G ⊢ all(S1) T1 <⦂ all(S2) T2

| subtyp_fld : forall G a T U,
    G ⊢ T <⦂ U ->
    G ⊢ {< decs_cons (dec_trm a T) decs_nil >}
      <⦂ {< decs_cons (dec_trm a U) decs_nil >} (* DS[a := U] *)
| subtyp_typ : forall G A S1 T1 S2 T2,
    G ⊢ S2 <⦂ S1 ->
    G ⊢ T1 <⦂ T2 ->
    G ⊢ {< decs_cons (dec_typ A S1 T1) decs_nil >}
      <⦂ {< decs_cons (dec_typ A S2 T2) decs_nil >}
(* DS[A := S2 .. T2] *)

| subtyp_drop1 : forall G (DS1 : decs) (DS2 : decs),
    not_empty DS1 ->
    G ⊢ {< append' DS1 DS2 >} <⦂ {< DS2 >}
| subtyp_drop2 : forall G (DS1 : decs) (DS2 : decs),
    not_empty DS2 ->
    G ⊢ {< append' DS1 DS2 >} <⦂ {< DS1 >}
| subtyp_merge : forall G (DS : decs) (DS1 : decs) (DS2 : decs),
    G ⊢ {< DS >} <⦂ {< DS1 >} ->
    G ⊢ {< DS >} <⦂ {< DS2 >} ->
    G ⊢ {< DS >} <⦂ {< append' DS1 DS2 >}
                    
| subtyp_sel1 : forall G (x : var) A S T,
    G ⊢ trm_var x ⦂ {< decs_cons (dec_typ A S T) decs_nil >} ->
    G ⊢ S <⦂ x ⋅ A
| subtyp_sel2 : forall G (x : var) A S T,
    G ⊢ trm_var x ⦂ {< decs_cons (dec_typ A S T) decs_nil >} ->
    G ⊢ x ⋅ A <⦂ T
where "G ⊢ T <⦂ U" := (subtyp G T U) : type_scope.
Hint Constructors ty_trm ty_def ty_defs subtyp.


(** mutually inductive schemes *)

Scheme typ_mut := Induction for typ Sort Prop
  with dec_mut := Induction for dec Sort Prop
  with decs_mut := Induction for decs Sort Prop.
Combined Scheme typ_mutind from typ_mut, dec_mut, decs_mut.


Scheme trm_mut := Induction for trm Sort Prop
  with val_mut := Induction for val Sort Prop
  with def_mut := Induction for def Sort Prop
  with defs_mut := Induction for defs Sort Prop.
Combined Scheme trm_mutind from trm_mut, val_mut, def_mut, defs_mut.

Scheme ty_trm_mut := Induction for ty_trm Sort Prop
  with ty_def_mut := Induction for ty_def Sort Prop
  with ty_defs_mut := Induction for ty_defs Sort Prop
  with subtyp_mut := Induction for subtyp Sort Prop.
Combined Scheme typing_mut from ty_trm_mut, ty_def_mut, ty_defs_mut, subtyp_mut.

Scheme tty_trm_mut := Induction for ty_trm Sort Prop
  with tsubtyp_mut := Induction for subtyp Sort Prop.
Combined Scheme ttyp_mut from tty_trm_mut, tsubtyp_mut.

(** Tactics *)

Ltac gather_atoms ::=
  let A := gather_atoms_with (fun x : atoms => x) in
  let B := gather_atoms_with (fun x : atom => singleton x) in
  let C := gather_atoms_with (fun x : env => fv x) in
  let D := gather_atoms_with fv_avar in
  let E := gather_atoms_with fv_typ in
  let F := gather_atoms_with fv_dec in
  let G := gather_atoms_with fv_decs in
  let H := gather_atoms_with fv_trm in
  let I := gather_atoms_with fv_val in
  let J := gather_atoms_with fv_def in
  let K := gather_atoms_with fv_defs in
  let L := gather_atoms_with (fun x : sta => fv x) in
  constr:(A `union` B `union` C `union` D `union` E `union` F `union` G
            `union` H `union` I `union` J `union` K `union` L).


(** hook for mutual induction tactic *)
Ltac mut_ind_2 := fail.

Ltac mut_ind :=
  intros;
  (match goal with
  | [ |- (forall _ : typ, _) /\ (forall _ : dec, _) /\ (forall _ : decs, _) ] =>
    apply typ_mutind
  | [ |- (forall _ : trm, _) /\ (forall _ : val, _) /\ (forall _ : def, _) /\ (forall _ : defs, _)] =>
    apply trm_mutind
  | [ |- (forall (G : env) (t : trm) (T : typ) (_ : G ⊢ t ⦂ T), _) /\
        (forall (G0 : env) (d : def) (D : dec)
           (_ : G0 ⊩ d ⦂ D), _) /\
        (forall (G1 : env) (dfs : defs) (DS : decs)
           (_ : G1 ⊩[ dfs ⦂ DS ]), _) /\
        (forall (G2 : env) (S U : typ) (_ : G2 ⊢ S <⦂ U), _) ] =>
    apply typing_mut
  | [ |- (forall (G : env) (t : trm) (T : typ) (_ : G ⊢ t ⦂ T), _) /\
        (forall (G2 : env) (S U : typ) (_ : G2 ⊢ S <⦂ U), _) ] =>
    apply ttyp_mut
   end || mut_ind_2).

Tactic Notation "mutual" "induction" := mut_ind.
Tactic Notation "mutual" "induction*" := mutual induction; intros.

Tactic Notation "ensure" "typ" constr(t) :=
  match type of t with
  | typ => idtac
  | dec => idtac
  | decs => idtac
  end.

Tactic Notation "ensure" "trm" constr(t) :=
  match type of t with
  | trm => idtac
  | val => idtac
  | def => idtac
  | defs => idtac
  end.

(** The first kind of undecidability for typing. see if we can hit more. *)
Tactic Notation "typing" "undec" :=
  match goal with
  | [ |- _ ⊢ _ ⦂ _ ] => econstructor
  | [ |- context[typ_all] ] => eapply subtyp_all
  | [ |- context[dec_trm]] => eapply subtyp_fld
  | [ |- context[dec_typ]] => eapply subtyp_typ
  | [ |- context[ _ ⊢ _ <⦂ _ ⋅ _]] => eapply subtyp_sel1
  | [ |- context[ _ ⊢ _ ⋅ _ <⦂ _]] => eapply subtyp_sel2
  | _ => idtac
  end.

(** convert decs and defs problems to lists problems *)
Ltac list_reasoning :=
  repeat
    match goal with
    | [ |- context[decs_cons ?D ?DS]] =>
      different DS decs_nil;
      change (decs_cons D DS) with (append' (decs_cons D decs_nil) DS)
    | [H : context[decs_cons ?D ?DS] |- _] =>
      different DS decs_nil;
      change (decs_cons D DS) with (append' (decs_cons D decs_nil) DS) in H
    | [ |- context[decs_append' ?L1 ?L2] ] =>
      change (decs_append' L1 L2) with (append' L1 L2)
    | [H : context[decs_append' ?L1 ?L2] |- _ ] =>
      change (decs_append' L1 L2) with (append' L1 L2) in H
    | [ |- context[decs_to_list' ?L] ] =>
      change (decs_to_list' L) with (to_list' L)
    | [H : context[decs_to_list' ?L] |- _ ] =>
      change (decs_to_list' L) with (to_list' L) in H

    | [ |- context[defs_cons ?D ?DS]] =>
      different DS defs_nil;
      change (defs_cons D DS) with (append' (defs_cons D defs_nil) DS)
    | [H : context[defs_cons ?D ?DS] |- _] =>
      different DS defs_nil;
      change (defs_cons D DS) with (append' (defs_cons D defs_nil) DS) in H
    | [ |- context[defs_append' ?L1 ?L2] ] =>
      change (defs_append' L1 L2) with (append' L1 L2)
    | [H : context[defs_append' ?L1 ?L2] |- _ ] =>
      change (defs_append' L1 L2) with (append' L1 L2) in H
    | [ |- context[defs_to_list' ?L] ] =>
      change (defs_to_list' L) with (to_list' L)
    | [H : context[defs_to_list' ?L] |- _ ] =>
      change (defs_to_list' L) with (to_list' L) in H

    | [ |- context[to_list'] ] =>
      rewrite append_sound'
    | [H : context[to_list'] |- _ ] =>
      rewrite append_sound' in H
    end.

Ltac destruct_lbinds :=
  let rec des pred :=
      match goal with
      | [H : lbinds _ _ (_ ++ _) |- _] =>
        pred H;
        let H' := fresh in
        pose proof H as H';
        apply LabelAssocList.binds_app_1 in H';
        destruct H';
        des ltac:(fun name => different H name; pred name)
      | _ => idtac
      end in
    list_reasoning; des ltac:(fun _ => idtac).

Ltac solve_lbinds' :=
  try (eassumption || left; reflexivity);
  match goal with
  | [H : lbinds ?x _ ?l |- lbinds ?x _ (?A ++ ?B)] =>
    lazymatch A with
    | context[l] =>
      apply lbinds_app_l; solve_lbinds'
    end
    || lazymatch B with
      | context[l] =>
        apply lbinds_app_r; solve_lbinds'
      end
  | [|- lbinds ?x _ (?A ++ ?B)] =>
    lazymatch A with
    | context[x] => apply lbinds_app_l; solve_lbinds'
    end
    || lazymatch B with
      | context[x] => apply lbinds_app_r; solve_lbinds'
      end
  end.

Ltac solve_lbinds := list_reasoning; solve_lbinds'.
