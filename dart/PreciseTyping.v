Set Implicit Arguments.

Require Import Definitions.


Reserved Notation "G '⊢!' v '⦂' T" (at level 70, v at level 79).

Inductive ty_val_p : env -> val -> typ -> Prop :=
| ty_all_intro : forall L G T t U,
    (forall x, x `notin` L ->
          x ~ T ++ G ⊢ open x t ⦂ open x U) ->
    G ⊢! λ( T ){ t } ⦂ all( T ) U
| ty_obj_intro : forall G ds DS,
    G ⊩[ ds ⦂ DS ] ->
    wf_defs ds ->
    G ⊢! {[ ds ]} ⦂ {< DS >}
where "G ⊢! v ⦂ T" := (ty_val_p G v T) : type_scope.
Hint Constructors ty_val_p.