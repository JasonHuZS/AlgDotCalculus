Require Import Extract.
Require Import ExtrOcamlBasic.
Require Import ExtrOcamlNatInt.
Require Import ExtrOcamlString.
Require Import ExtrOcamlIntConv.

Extraction "typing.ml" typecheck.
Extraction "typing2.ml" typecheck_equiv.

