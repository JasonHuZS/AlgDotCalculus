Set Implicit Arguments.

Require Import Definitions.
Require Import LibUtils.
Require Import Coq.Lists.List.
Require Import Concepts.
Require Import Metalib.Metatheory.

Section SubstDefinitions.

  Definition subst_avar (z u: var) (a : avar) : avar :=
    match a with
    | avar_b i as a => a
    | avar_f x => avar_f $ if x == z then u else x
    end.

  Fixpoint subst_typ (z u: var) (T : typ) : typ :=
    match T with
    | ⊤ => ⊤
    | ⊥ => ⊥
    | x ⋅ L => (subst_avar z u x) ⋅ L
    | all(T) U => all( subst_typ z u T ) subst_typ z u U
    | {< DS >} => {< subst_decs z u DS >}
    end
  with
  subst_dec (z u: var) (D : dec) : dec :=
    match D with
    | dec_typ A T U => dec_typ A (subst_typ z u T) $ subst_typ z u U
    | dec_trm a T => dec_trm a $ subst_typ z u T
    end
  with
  subst_decs (z u: var) (DS : decs) : decs :=
    match DS with
    | decs_nil => decs_nil
    | decs_cons D DS' => decs_cons (subst_dec z u D) $ subst_decs z u DS'
    end.

  Fixpoint subst_trm (z u: var) (t : trm) : trm :=
    match t with
    | trm_var x => trm_var $ subst_avar z u x
    | trm_val v => trm_val $ subst_val z u v
    | trm_sel x L => trm_sel (subst_avar z u x) L
    | trm_app x1 x2 => trm_app (subst_avar z u x1) $ subst_avar z u x2
    | trm_let t1 t2 => trm_let (subst_trm z u t1) $ subst_trm z u t2
    end
  with
  subst_val (z u: var) (v : val) : val :=
    match v with
    | {[ ds ]} => {[ subst_defs z u ds ]}
    | λ( T ){ t } => λ( subst_typ z u T ){ subst_trm z u t }
    end
  with
  subst_def (z u: var) (d : def) : def :=
    match d with
    | def_typ A T => def_typ A $ subst_typ z u T
    | def_trm a t => def_trm a $ subst_trm z u t
    end
  with
  subst_defs (z u: var) (ds : defs) : defs :=
    match ds with
    | defs_nil => defs_nil
    | defs_cons d ds' => defs_cons (subst_def z u d) $ subst_defs z u ds'
    end.

End SubstDefinitions.

Instance SubstAvar : CanSubst avar := { substi := subst_avar }.
Instance SubstTyp : CanSubst typ := { substi := subst_typ }.
Instance SubstDec : CanSubst dec := { substi := subst_dec }.
Instance SubstLDec : CanSubst (label * dec) :=
  {substi := fun z u tup => let (l, D) := tup in (l, substi z u D) }.
Instance SubstDecs : CanSubst decs := { substi := subst_decs }.
Instance SubstTrm : CanSubst trm := { substi := subst_trm }.
Instance SubstVal : CanSubst val := { substi := subst_val }.
Instance SubstDef : CanSubst def := { substi := subst_def }.
Instance SubstLDef : CanSubst (label * def) :=
  {substi := fun z u tup => let (l, D) := tup in (l, substi z u D) }.
Instance SubstDefs : CanSubst defs := { substi := subst_defs }.

Definition subst_alist {A} {_ : CanSubst A} (z u : var) (l : list (atom * A)) :=
  map (substi z u) l.
Hint Unfold subst_alist.

Instance SubstEnv : CanSubst env := { substi := subst_alist }.
Instance SubstSta : CanSubst sta := { substi := subst_alist }.

Section OpenFreshInj.
  
  Variable z : atom.

  Local Notation fresh_inj T :=
    (forall (x y : T) k,
        z `notin` fv x ->
        z `notin` fv y ->
        open_rec k z x = open_rec k z y ->
        x = y).
  
  Lemma open_fresh_inj_avar : fresh_inj avar.
  Proof using.
    intros. destruct x, y; routine.
  Qed.
  Local Hint Resolve open_fresh_inj_avar.

  Local Ltac boom :=
    eroutine by 
      (idtac; match goal with
              | [ H : _ = _ _ z ?t' |- _ ] =>
                destruct t'; inversion H
              end).
  
  Lemma open_fresh_inj_typ : fresh_inj typ
  with open_fresh_inj_dec : fresh_inj dec
  with open_fresh_inj_decs : fresh_inj decs.
  Proof using.
    - clear open_fresh_inj_typ. induction on typ; boom.
    - clear open_fresh_inj_dec. induction on dec; boom.
    - clear open_fresh_inj_decs. induction on decs; boom.
  Qed.
  Hint Resolve open_fresh_inj_typ.
  
  Lemma open_fresh_inj_trm : fresh_inj trm
  with open_fresh_inj_val : fresh_inj val
  with open_fresh_inj_def : fresh_inj def
  with open_fresh_inj_defs : fresh_inj defs.
  Proof using.
    - clear open_fresh_inj_trm. induction on trm; boom.
    - clear open_fresh_inj_val. induction on val; boom.
    - clear open_fresh_inj_def. induction on def; boom.
    - clear open_fresh_inj_defs. induction on defs; boom.
  Qed.
  
End OpenFreshInj.

Section SubstFresh.
  
  Variable x y : var.

  Local Notation subst_fresh T :=
    (forall t : T, x `notin` fv t -> substi x y t = t).

  Lemma subst_fresh_avar : subst_fresh avar.
  Proof using.
    intros. destruct t; routine.
  Qed.
  Local Hint Resolve subst_fresh_avar.

  Lemma subst_fresh_typ : subst_fresh typ
  with subst_fresh_dec : subst_fresh dec
  with subst_fresh_decs : subst_fresh decs.
  Proof using.
    - clear subst_fresh_typ. induction on typ; routine.
    - clear subst_fresh_dec. induction on dec; routine.
    - clear subst_fresh_decs. induction on decs; routine.
  Qed.
  Hint Resolve subst_fresh_typ.

  Lemma subst_fresh_trm : subst_fresh trm
  with subst_fresh_val : subst_fresh val
  with subst_fresh_def : subst_fresh def
  with subst_fresh_defs : subst_fresh defs.
  Proof using.
    - clear subst_fresh_trm. induction on trm; routine.
    - clear subst_fresh_val. induction on val; routine.
    - clear subst_fresh_def. induction on def; routine.
    - clear subst_fresh_defs. induction on defs; routine.
  Qed.
  
End SubstFresh.

Section SubstOpenComm.

  Variable x y : var.

  (** z[y/x] *)
  Definition subst_fvar (z : var) : var :=
    if z == x then y else z.

  Local Notation subst_open_comm T :=
    (forall (t : T) (n : nat) u,
        substi x y (open_rec n u t) =
        open_rec n (subst_fvar u) $ substi x y t).

  Lemma subst_open_comm_avar : subst_open_comm avar.
  Proof using.
    intros. destruct t; routine by (unfold subst_fvar).
  Qed.
  Hint Resolve subst_open_comm_avar.
  
  Lemma subst_open_comm_typ : subst_open_comm typ
  with subst_open_comm_dec : subst_open_comm dec
  with subst_open_comm_decs : subst_open_comm decs.
  Proof using.
    - clear subst_open_comm_typ. induction on typ; routine.
    - clear subst_open_comm_dec. induction on dec; routine.
    - clear subst_open_comm_decs. induction on decs; routine.
  Qed.
  Hint Resolve subst_open_comm_typ.
  
  Lemma subst_open_comm_trm : subst_open_comm trm
  with subst_open_comm_val : subst_open_comm val
  with subst_open_comm_def : subst_open_comm def
  with subst_open_comm_defs : subst_open_comm defs.
  Proof using.
    - clear subst_open_comm_trm. induction on trm; routine.
    - clear subst_open_comm_val. induction on val; routine.
    - clear subst_open_comm_def. induction on def; routine.
    - clear subst_open_comm_defs. induction on defs; routine.
  Qed.

  Definition subst_open_comm_typ_dec_decs :=
    (conj subst_open_comm_typ
          (conj subst_open_comm_dec
                subst_open_comm_decs)).
  
  Definition subst_open_comm_all :=
    (conj subst_open_comm_typ_dec_decs
          (conj subst_open_comm_trm
                (conj subst_open_comm_val
                      (conj subst_open_comm_def
                            subst_open_comm_defs)))).
  
End SubstOpenComm.
Arguments subst_fvar x y z/.
Hint Unfold subst_fvar.

Section SubstIntro.

  Local Notation subst_intro T :=
    (forall x u (t : T) (n : nat),
        x `notin` fv t ->
        open_rec n u t = substi x u $ open_rec n x t).
  
  Local Hint Extern 1 => exrewrite subst_fresh_typ.
  Local Hint Extern 1 => exrewrite subst_fresh_trm.
  Local Hint Extern 1 => exrewrite subst_fresh_val.
  Local Hint Extern 1 => exrewrite subst_fresh_dec.
  Local Hint Extern 1 => exrewrite subst_fresh_decs.
  Local Hint Extern 1 => exrewrite subst_fresh_def.
  Local Hint Extern 1 => exrewrite subst_fresh_defs.

  Local Ltac boom := routine by (exrewrite subst_open_comm_all).

  Lemma subst_intro_trm : subst_intro trm.
  Proof using. boom. Qed.

  Lemma subst_intro_defs : subst_intro defs.
  Proof using. boom. Qed.

  Lemma subst_intro_def : subst_intro def.
  Proof using. boom. Qed.

  Lemma subst_intro_typ : subst_intro typ.
  Proof using. boom. Qed.

  Lemma subst_intro_decs : subst_intro decs.
  Proof using. boom. Qed.

  Lemma subst_intro_dec : subst_intro dec.
  Proof using. boom. Qed.

End SubstIntro.

Section SubstFvarProps.
  Variable x y z : var.
  Variable T : Type.

  Context {SubstT : CanSubst T} {OpenT : CanOpen T}.

  Variable t : T.

  Lemma open_substi_rewrite :
    z `notin` singleton x `union` singleton y ->
    open z (substi x y t) =
    open (subst_fvar x y z) (substi x y t).
  Proof using. routine. Qed.

  Hypothesis subst_open_comm :
    substi x y (open z t) = open (subst_fvar x y z) (substi x y t).  
  
  Lemma open_substi_rewrite2 :
    z `notin` singleton x `union` singleton y ->
    open z (substi x y t) =
    substi x y (open z t).
  Proof.
    intros. rewrite open_substi_rewrite by auto.
    rewrite <- subst_open_comm. trivial.
  Qed.

End SubstFvarProps.

Hint Resolve subst_open_comm_trm subst_open_comm_val
     subst_open_comm_def subst_open_comm_defs.

Hint Resolve subst_open_comm_typ subst_open_comm_dec subst_open_comm_decs.

Ltac open_substi_transform :=
  repeat match goal with
         | |- context[?f ?v (substi ?x ?y ?t)] =>
           lazymatch f with
           | context[open] => idtac
           end;
           lazymatch v with
           | context[subst_fvar] => fail
           | _ => idtac
           end;
           replace (f v (substi x y t)) with (substi x y (open v t))
             by (rewrite open_substi_rewrite; auto)
         end.
