Require Import Extract.
Require Import ExtrHaskellBasic.
Require Import ExtrHaskellNatInteger.
Require Import ExtrHaskellString.

Extraction Language Haskell.

Extraction "Typing.hs" typecheck.
Extraction "Typing2.hs" typecheck_equiv.
