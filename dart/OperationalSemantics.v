Require Import Definitions.

Reserved Notation "[ s1 ] t1 ↦ [ s2 ] t2" (at level 40, t2 at level 39).

Inductive opred : sta -> trm -> sta -> trm -> Prop :=
| red_sel : forall s x a t (dfs : defs),
    binds x ({[ dfs ]}) s ->
    lbinds (label_trm a) (def_trm a t) dfs ->
    [s] (trm_sel x a) ↦ [s] t
| red_app : forall s x y T t,
    binds x (λ(T){t}) s ->
    [s] trm_app x (avar_f y) ↦ [s] open y t
| red_let_val : forall s v x t,
    x `notin` dom s ->
    [s] lett (trm_val v) inn t ↦ [x ~ v ++ s] open x t
| red_let_var : forall s t (x : var),
    [s] lett (trm_var x) inn t ↦ [s] open x t
| red_let_tgt : forall s s' t0 t0' t,
    [s] t0 ↦  [s'] t0' ->
    [s] lett t0 inn t ↦ [s'] lett t0' inn t
where "[ s1 ] t1 ↦ [ s2 ] t2" := (opred s1 t1 s2 t2).
Hint Constructors opred.

Inductive nf : trm -> Prop :=
| nf_var : forall x, nf (trm_var x)
| nf_val : forall v, nf (trm_val v).
Hint Constructors nf.