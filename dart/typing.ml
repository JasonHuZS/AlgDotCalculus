
type __ = Obj.t
let __ = let rec f _ = Obj.repr f in Obj.repr f

(** val negb : bool -> bool **)

let negb = function
| true -> false
| false -> true

type ('a, 'b) sum =
| Inl of 'a
| Inr of 'b

(** val fst : ('a1 * 'a2) -> 'a1 **)

let fst = function
| (x, _) -> x

(** val snd : ('a1 * 'a2) -> 'a2 **)

let snd = function
| (_, y) -> y

(** val length : 'a1 list -> int **)

let rec length = function
| [] -> 0
| _ :: l' -> Pervasives.succ (length l')

(** val app : 'a1 list -> 'a1 list -> 'a1 list **)

let rec app l m =
  match l with
  | [] -> m
  | a :: l1 -> a :: (app l1 m)

type 'a sig0 = 'a
  (* singleton inductive, whose constructor was exist *)

type ('a, 'p) sigT =
| ExistT of 'a * 'p

(** val projT1 : ('a1, 'a2) sigT -> 'a1 **)

let projT1 = function
| ExistT (a, _) -> a

(** val projT2 : ('a1, 'a2) sigT -> 'a2 **)

let projT2 = function
| ExistT (_, h) -> h



(** val flip : ('a1 -> 'a2 -> 'a3) -> 'a2 -> 'a1 -> 'a3 **)

let flip f x y =
  f y x

module type DecidableType =
 sig
  type t

  val eq_dec : t -> t -> bool
 end

module type DecidableTypeOrig =
 sig
  type t

  val eq_dec : t -> t -> bool
 end

module type UsualDecidableTypeOrig =
 sig
  type t

  val eq_dec : t -> t -> bool
 end

module Nat =
 struct
  (** val max : int -> int -> int **)

  let rec max n m =
    (fun fO fS n -> if n=0 then fO () else fS (n-1))
      (fun _ -> m)
      (fun n' ->
      (fun fO fS n -> if n=0 then fO () else fS (n-1))
        (fun _ -> n)
        (fun m' -> Pervasives.succ (max n' m'))
        m)
      n
 end

(** val in_dec : ('a1 -> 'a1 -> bool) -> 'a1 -> 'a1 list -> bool **)

let rec in_dec h a = function
| [] -> false
| y :: l0 -> let s = h y a in if s then true else in_dec h a l0

(** val rev : 'a1 list -> 'a1 list **)

let rec rev = function
| [] -> []
| x :: l' -> app (rev l') (x :: [])

(** val map : ('a1 -> 'a2) -> 'a1 list -> 'a2 list **)

let rec map f = function
| [] -> []
| a :: t0 -> (f a) :: (map f t0)

(** val fold_left : ('a1 -> 'a2 -> 'a1) -> 'a2 list -> 'a1 -> 'a1 **)

let rec fold_left f l a0 =
  match l with
  | [] -> a0
  | b :: t0 -> fold_left f t0 (f a0 b)

(** val fold_right : ('a2 -> 'a1 -> 'a1) -> 'a1 -> 'a2 list -> 'a1 **)

let rec fold_right f a0 = function
| [] -> a0
| b :: t0 -> f b (fold_right f a0 t0)

module type Coq_DecidableType =
 DecidableTypeOrig

module type UsualDecidableType =
 UsualDecidableTypeOrig

module WFacts_fun =
 functor (E:Coq_DecidableType) ->
 functor (M:sig
  type elt = E.t

  type t

  val empty : t

  val is_empty : t -> bool

  val mem : elt -> t -> bool

  val add : elt -> t -> t

  val singleton : elt -> t

  val remove : elt -> t -> t

  val union : t -> t -> t

  val inter : t -> t -> t

  val diff : t -> t -> t

  val eq_dec : t -> t -> bool

  val equal : t -> t -> bool

  val subset : t -> t -> bool

  val fold : (elt -> 'a1 -> 'a1) -> t -> 'a1 -> 'a1

  val for_all : (elt -> bool) -> t -> bool

  val exists_ : (elt -> bool) -> t -> bool

  val filter : (elt -> bool) -> t -> t

  val partition : (elt -> bool) -> t -> t * t

  val cardinal : t -> int

  val elements : t -> elt list

  val choose : t -> elt option
 end) ->
 struct
  (** val eqb : E.t -> E.t -> bool **)

  let eqb x y =
    if E.eq_dec x y then true else false
 end

module WDecide_fun =
 functor (E:Coq_DecidableType) ->
 functor (M:sig
  type elt = E.t

  type t

  val empty : t

  val is_empty : t -> bool

  val mem : elt -> t -> bool

  val add : elt -> t -> t

  val singleton : elt -> t

  val remove : elt -> t -> t

  val union : t -> t -> t

  val inter : t -> t -> t

  val diff : t -> t -> t

  val eq_dec : t -> t -> bool

  val equal : t -> t -> bool

  val subset : t -> t -> bool

  val fold : (elt -> 'a1 -> 'a1) -> t -> 'a1 -> 'a1

  val for_all : (elt -> bool) -> t -> bool

  val exists_ : (elt -> bool) -> t -> bool

  val filter : (elt -> bool) -> t -> t

  val partition : (elt -> bool) -> t -> t * t

  val cardinal : t -> int

  val elements : t -> elt list

  val choose : t -> elt option
 end) ->
 struct
  module F = WFacts_fun(E)(M)

  module FSetLogicalFacts =
   struct
   end

  module FSetDecideAuxiliary =
   struct
   end

  module FSetDecideTestCases =
   struct
   end
 end

module WProperties_fun =
 functor (E:Coq_DecidableType) ->
 functor (M:sig
  type elt = E.t

  type t

  val empty : t

  val is_empty : t -> bool

  val mem : elt -> t -> bool

  val add : elt -> t -> t

  val singleton : elt -> t

  val remove : elt -> t -> t

  val union : t -> t -> t

  val inter : t -> t -> t

  val diff : t -> t -> t

  val eq_dec : t -> t -> bool

  val equal : t -> t -> bool

  val subset : t -> t -> bool

  val fold : (elt -> 'a1 -> 'a1) -> t -> 'a1 -> 'a1

  val for_all : (elt -> bool) -> t -> bool

  val exists_ : (elt -> bool) -> t -> bool

  val filter : (elt -> bool) -> t -> t

  val partition : (elt -> bool) -> t -> t * t

  val cardinal : t -> int

  val elements : t -> elt list

  val choose : t -> elt option
 end) ->
 struct
  module Dec = WDecide_fun(E)(M)

  module FM = Dec.F

  (** val coq_In_dec : M.elt -> M.t -> bool **)

  let coq_In_dec x s =
    if M.mem x s then true else false

  (** val of_list : M.elt list -> M.t **)

  let of_list l =
    fold_right M.add M.empty l

  (** val to_list : M.t -> M.elt list **)

  let to_list =
    M.elements

  (** val fold_rec :
      (M.elt -> 'a1 -> 'a1) -> 'a1 -> M.t -> (M.t -> __ -> 'a2) -> (M.elt ->
      'a1 -> M.t -> M.t -> __ -> __ -> __ -> 'a2 -> 'a2) -> 'a2 **)

  let fold_rec f i s pempty pstep =
    let l = rev (M.elements s) in
    let pstep' = fun x a s' s'' x0 -> pstep x a s' s'' __ __ __ x0 in
    let rec f0 l0 pstep'0 s0 =
      match l0 with
      | [] -> pempty s0 __
      | y :: l1 ->
        pstep'0 y (fold_right f i l1) (of_list l1) s0 __ __ __
          (f0 l1 (fun x a0 s' s'' _ _ _ x0 ->
            pstep'0 x a0 s' s'' __ __ __ x0) (of_list l1))
    in f0 l (fun x a s' s'' _ _ _ x0 -> pstep' x a s' s'' x0) s

  (** val fold_rec_bis :
      (M.elt -> 'a1 -> 'a1) -> 'a1 -> M.t -> (M.t -> M.t -> 'a1 -> __ -> 'a2
      -> 'a2) -> 'a2 -> (M.elt -> 'a1 -> M.t -> __ -> __ -> 'a2 -> 'a2) -> 'a2 **)

  let fold_rec_bis f i s pmorphism pempty pstep =
    fold_rec f i s (fun s' _ -> pmorphism M.empty s' i __ pempty)
      (fun x a s' s'' _ _ _ x0 ->
      pmorphism (M.add x s') s'' (f x a) __ (pstep x a s' __ __ x0))

  (** val fold_rec_nodep :
      (M.elt -> 'a1 -> 'a1) -> 'a1 -> M.t -> 'a2 -> (M.elt -> 'a1 -> __ ->
      'a2 -> 'a2) -> 'a2 **)

  let fold_rec_nodep f i s x x0 =
    fold_rec_bis f i s (fun _ _ _ _ x1 -> x1) x (fun x1 a _ _ _ x2 ->
      x0 x1 a __ x2)

  (** val fold_rec_weak :
      (M.elt -> 'a1 -> 'a1) -> 'a1 -> (M.t -> M.t -> 'a1 -> __ -> 'a2 -> 'a2)
      -> 'a2 -> (M.elt -> 'a1 -> M.t -> __ -> 'a2 -> 'a2) -> M.t -> 'a2 **)

  let fold_rec_weak f i x x0 x1 s =
    fold_rec_bis f i s x x0 (fun x2 a s' _ _ x3 -> x1 x2 a s' __ x3)

  (** val fold_rel :
      (M.elt -> 'a1 -> 'a1) -> (M.elt -> 'a2 -> 'a2) -> 'a1 -> 'a2 -> M.t ->
      'a3 -> (M.elt -> 'a1 -> 'a2 -> __ -> 'a3 -> 'a3) -> 'a3 **)

  let fold_rel f g i j s rempty rstep =
    let l = rev (M.elements s) in
    let rstep' = fun x a b x0 -> rstep x a b __ x0 in
    let rec f0 l0 rstep'0 =
      match l0 with
      | [] -> rempty
      | y :: l1 ->
        rstep'0 y (fold_right f i l1) (fold_right g j l1) __
          (f0 l1 (fun x a0 b _ x0 -> rstep'0 x a0 b __ x0))
    in f0 l (fun x a b _ x0 -> rstep' x a b x0)

  (** val set_induction :
      (M.t -> __ -> 'a1) -> (M.t -> M.t -> 'a1 -> M.elt -> __ -> __ -> 'a1)
      -> M.t -> 'a1 **)

  let set_induction x x0 s =
    fold_rec (fun _ _ -> ()) () s x (fun x1 _ s' s'' _ _ _ x2 ->
      x0 s' s'' x2 x1 __ __)

  (** val set_induction_bis :
      (M.t -> M.t -> __ -> 'a1 -> 'a1) -> 'a1 -> (M.elt -> M.t -> __ -> 'a1
      -> 'a1) -> M.t -> 'a1 **)

  let set_induction_bis x x0 x1 s =
    fold_rec_bis (fun _ _ -> ()) () s (fun s0 s' _ _ x2 -> x s0 s' __ x2) x0
      (fun x2 _ s' _ _ x3 -> x1 x2 s' __ x3)

  (** val cardinal_inv_2 : M.t -> int -> M.elt **)

  let cardinal_inv_2 s _ =
    let l = M.elements s in
    (match l with
     | [] -> assert false (* absurd case *)
     | e :: _ -> e)

  (** val cardinal_inv_2b : M.t -> M.elt **)

  let cardinal_inv_2b s =
    let n = M.cardinal s in
    let x = fun x -> cardinal_inv_2 s x in
    ((fun fO fS n -> if n=0 then fO () else fS (n-1))
       (fun _ -> assert false (* absurd case *))
       (fun n0 -> x n0)
       n)
 end

module MakeRaw =
 functor (X:DecidableType) ->
 struct
  type elt = X.t

  type t = elt list

  (** val empty : t **)

  let empty =
    []

  (** val is_empty : t -> bool **)

  let is_empty = function
  | [] -> true
  | _ :: _ -> false

  (** val mem : elt -> t -> bool **)

  let rec mem x = function
  | [] -> false
  | y :: l -> if X.eq_dec x y then true else mem x l

  (** val add : elt -> t -> t **)

  let rec add x s = match s with
  | [] -> x :: []
  | y :: l -> if X.eq_dec x y then s else y :: (add x l)

  (** val singleton : elt -> t **)

  let singleton x =
    x :: []

  (** val remove : elt -> t -> t **)

  let rec remove x = function
  | [] -> []
  | y :: l -> if X.eq_dec x y then l else y :: (remove x l)

  (** val fold : (elt -> 'a1 -> 'a1) -> t -> 'a1 -> 'a1 **)

  let fold f =
    fold_left (flip f)

  (** val union : t -> t -> t **)

  let union s =
    fold add s

  (** val diff : t -> t -> t **)

  let diff s s' =
    fold remove s' s

  (** val inter : t -> t -> t **)

  let inter s s' =
    fold (fun x s0 -> if mem x s' then add x s0 else s0) s []

  (** val subset : t -> t -> bool **)

  let subset s s' =
    is_empty (diff s s')

  (** val equal : t -> t -> bool **)

  let equal s s' =
    (&&) (subset s s') (subset s' s)

  (** val filter : (elt -> bool) -> t -> t **)

  let rec filter f = function
  | [] -> []
  | x :: l -> if f x then x :: (filter f l) else filter f l

  (** val for_all : (elt -> bool) -> t -> bool **)

  let rec for_all f = function
  | [] -> true
  | x :: l -> if f x then for_all f l else false

  (** val exists_ : (elt -> bool) -> t -> bool **)

  let rec exists_ f = function
  | [] -> false
  | x :: l -> if f x then true else exists_ f l

  (** val partition : (elt -> bool) -> t -> t * t **)

  let rec partition f = function
  | [] -> ([], [])
  | x :: l ->
    let (s1, s2) = partition f l in
    if f x then ((x :: s1), s2) else (s1, (x :: s2))

  (** val cardinal : t -> int **)

  let cardinal =
    length

  (** val elements : t -> elt list **)

  let elements s =
    s

  (** val choose : t -> elt option **)

  let choose = function
  | [] -> None
  | x :: _ -> Some x

  (** val isok : elt list -> bool **)

  let rec isok = function
  | [] -> true
  | a :: l0 -> (&&) (negb (mem a l0)) (isok l0)
 end

module Make =
 functor (X:DecidableType) ->
 struct
  module Raw = MakeRaw(X)

  module E =
   struct
    type t = X.t

    (** val eq_dec : t -> t -> bool **)

    let eq_dec =
      X.eq_dec
   end

  type elt = X.t

  type t_ = Raw.t
    (* singleton inductive, whose constructor was Mkt *)

  (** val this : t_ -> Raw.t **)

  let this t0 =
    t0

  type t = t_

  (** val mem : elt -> t -> bool **)

  let mem x s =
    Raw.mem x (this s)

  (** val add : elt -> t -> t **)

  let add x s =
    Raw.add x (this s)

  (** val remove : elt -> t -> t **)

  let remove x s =
    Raw.remove x (this s)

  (** val singleton : elt -> t **)

  let singleton =
    Raw.singleton

  (** val union : t -> t -> t **)

  let union s s' =
    Raw.union (this s) (this s')

  (** val inter : t -> t -> t **)

  let inter s s' =
    Raw.inter (this s) (this s')

  (** val diff : t -> t -> t **)

  let diff s s' =
    Raw.diff (this s) (this s')

  (** val equal : t -> t -> bool **)

  let equal s s' =
    Raw.equal (this s) (this s')

  (** val subset : t -> t -> bool **)

  let subset s s' =
    Raw.subset (this s) (this s')

  (** val empty : t **)

  let empty =
    Raw.empty

  (** val is_empty : t -> bool **)

  let is_empty s =
    Raw.is_empty (this s)

  (** val elements : t -> elt list **)

  let elements s =
    Raw.elements (this s)

  (** val choose : t -> elt option **)

  let choose s =
    Raw.choose (this s)

  (** val fold : (elt -> 'a1 -> 'a1) -> t -> 'a1 -> 'a1 **)

  let fold f s =
    Raw.fold f (this s)

  (** val cardinal : t -> int **)

  let cardinal s =
    Raw.cardinal (this s)

  (** val filter : (elt -> bool) -> t -> t **)

  let filter f s =
    Raw.filter f (this s)

  (** val for_all : (elt -> bool) -> t -> bool **)

  let for_all f s =
    Raw.for_all f (this s)

  (** val exists_ : (elt -> bool) -> t -> bool **)

  let exists_ f s =
    Raw.exists_ f (this s)

  (** val partition : (elt -> bool) -> t -> t * t **)

  let partition f s =
    let p = Raw.partition f (this s) in ((fst p), (snd p))

  (** val eq_dec : t -> t -> bool **)

  let eq_dec s0 s'0 =
    let b = Raw.equal s0 s'0 in if b then true else false
 end

module Coq_WDecide_fun =
 functor (E:Coq_DecidableType) ->
 functor (M:sig
  type elt = E.t

  type t

  val empty : t

  val is_empty : t -> bool

  val mem : elt -> t -> bool

  val add : elt -> t -> t

  val singleton : elt -> t

  val remove : elt -> t -> t

  val union : t -> t -> t

  val inter : t -> t -> t

  val diff : t -> t -> t

  val eq_dec : t -> t -> bool

  val equal : t -> t -> bool

  val subset : t -> t -> bool

  val fold : (elt -> 'a1 -> 'a1) -> t -> 'a1 -> 'a1

  val for_all : (elt -> bool) -> t -> bool

  val exists_ : (elt -> bool) -> t -> bool

  val filter : (elt -> bool) -> t -> t

  val partition : (elt -> bool) -> t -> t * t

  val cardinal : t -> int

  val elements : t -> elt list

  val choose : t -> elt option
 end) ->
 struct
  module F = WFacts_fun(E)(M)

  module FSetLogicalFacts =
   struct
   end

  module FSetDecideAuxiliary =
   struct
   end

  module FSetDecideTestCases =
   struct
   end
 end

type 'a eqDec = 'a -> 'a -> bool

(** val nat_eq_eqdec : int eqDec **)

let nat_eq_eqdec =
  (=)

type 'a eqDec_eq = 'a -> 'a -> bool

(** val eq_dec0 : 'a1 eqDec_eq -> 'a1 -> 'a1 -> bool **)

let eq_dec0 eqDec_eq0 =
  eqDec_eq0

(** val eqDec_eq_of_EqDec : 'a1 eqDec -> 'a1 eqDec_eq **)

let eqDec_eq_of_EqDec h =
  h

module Coq_Make =
 functor (X:UsualDecidableType) ->
 functor (KeySet:sig
  type elt = X.t

  type t

  val empty : t

  val is_empty : t -> bool

  val mem : elt -> t -> bool

  val add : elt -> t -> t

  val singleton : elt -> t

  val remove : elt -> t -> t

  val union : t -> t -> t

  val inter : t -> t -> t

  val diff : t -> t -> t

  val eq_dec : t -> t -> bool

  val equal : t -> t -> bool

  val subset : t -> t -> bool

  val fold : (elt -> 'a1 -> 'a1) -> t -> 'a1 -> 'a1

  val for_all : (elt -> bool) -> t -> bool

  val exists_ : (elt -> bool) -> t -> bool

  val filter : (elt -> bool) -> t -> t

  val partition : (elt -> bool) -> t -> t * t

  val cardinal : t -> int

  val elements : t -> elt list

  val choose : t -> elt option
 end) ->
 struct
  (** val coq_EqDec_of_X : X.t eqDec **)

  let coq_EqDec_of_X =
    X.eq_dec

  (** val coq_EqDec_eq_of_X : X.t eqDec_eq **)

  let coq_EqDec_eq_of_X =
    eqDec_eq_of_EqDec coq_EqDec_of_X

  module D = Coq_WDecide_fun(X)(KeySet)

  module KeySetProperties = WProperties_fun(X)(KeySet)

  module KeySetFacts = WFacts_fun(X)(KeySet)

  (** val one : 'a1 -> 'a1 list **)

  let one item =
    item :: []

  (** val dom : (X.t * 'a1) list -> KeySet.t **)

  let rec dom = function
  | [] -> KeySet.empty
  | p :: e' -> let (x, _) = p in KeySet.add x (dom e')

  (** val get : X.t -> (X.t * 'a1) list -> 'a1 option **)

  let rec get x = function
  | [] -> None
  | p :: f ->
    let (y, c) = p in
    if eq_dec0 coq_EqDec_eq_of_X x y then Some c else get x f

  (** val map : ('a1 -> 'a2) -> (X.t * 'a1) list -> (X.t * 'a2) list **)

  let map f e =
    map (fun b -> let (x, a) = b in (x, (f a))) e

  (** val alist_ind :
      'a2 -> (X.t -> 'a1 -> (X.t * 'a1) list -> 'a2 -> 'a2) -> (X.t * 'a1)
      list -> 'a2 **)

  let rec alist_ind x x0 = function
  | [] -> x
  | y :: l -> let iHxs = alist_ind x x0 l in let (x1, a) = y in x0 x1 a l iHxs

  (** val binds_dec :
      X.t -> 'a1 -> (X.t * 'a1) list -> ('a1 -> 'a1 -> bool) -> bool **)

  let binds_dec x a e x0 =
    in_dec (fun x1 y ->
      let (x2, x3) = x1 in
      let (t0, a1) = y in if X.eq_dec x2 t0 then x0 x3 a1 else false) (x, a) e

  (** val binds_lookup : X.t -> (X.t * 'a1) list -> ('a1, __) sum **)

  let binds_lookup x e =
    alist_ind (Inr __) (fun x1 a1 _ x0 ->
      match x0 with
      | Inl s -> Inl s
      | Inr _ -> let s = X.eq_dec x x1 in if s then Inl a1 else Inr __) e
 end

module Coq0_Make =
 functor (X:Coq_DecidableType) ->
 struct
  module E =
   struct
    type t = X.t

    (** val eq_dec : t -> t -> bool **)

    let eq_dec =
      X.eq_dec
   end

  module X' =
   struct
    type t = X.t

    (** val eq_dec : t -> t -> bool **)

    let eq_dec =
      X.eq_dec
   end

  module MSet = Make(X')

  type elt = X.t

  type t = MSet.t

  (** val empty : t **)

  let empty =
    MSet.empty

  (** val is_empty : t -> bool **)

  let is_empty =
    MSet.is_empty

  (** val mem : elt -> t -> bool **)

  let mem =
    MSet.mem

  (** val add : elt -> t -> t **)

  let add =
    MSet.add

  (** val singleton : elt -> t **)

  let singleton =
    MSet.singleton

  (** val remove : elt -> t -> t **)

  let remove =
    MSet.remove

  (** val union : t -> t -> t **)

  let union =
    MSet.union

  (** val inter : t -> t -> t **)

  let inter =
    MSet.inter

  (** val diff : t -> t -> t **)

  let diff =
    MSet.diff

  (** val eq_dec : t -> t -> bool **)

  let eq_dec =
    MSet.eq_dec

  (** val equal : t -> t -> bool **)

  let equal =
    MSet.equal

  (** val subset : t -> t -> bool **)

  let subset =
    MSet.subset

  (** val fold : (elt -> 'a1 -> 'a1) -> t -> 'a1 -> 'a1 **)

  let fold =
    MSet.fold

  (** val for_all : (elt -> bool) -> t -> bool **)

  let for_all =
    MSet.for_all

  (** val exists_ : (elt -> bool) -> t -> bool **)

  let exists_ =
    MSet.exists_

  (** val filter : (elt -> bool) -> t -> t **)

  let filter =
    MSet.filter

  (** val partition : (elt -> bool) -> t -> t * t **)

  let partition =
    MSet.partition

  (** val cardinal : t -> int **)

  let cardinal =
    MSet.cardinal

  (** val elements : t -> elt list **)

  let elements =
    MSet.elements

  (** val choose : t -> elt option **)

  let choose =
    MSet.choose

  module MF =
   struct
    (** val eqb : X.t -> X.t -> bool **)

    let eqb x y =
      if MSet.E.eq_dec x y then true else false
   end
 end

module type ATOM =
 sig
  type atom

  type t = atom

  val eq_dec : atom -> atom -> bool

  val atom_fresh_for_list : t list -> atom

  val fresh : atom list -> atom

  val nat_of : atom -> int
 end

module Atom =
 struct
  type atom = int

  type t = atom

  (** val eq_dec : int -> int -> bool **)

  let eq_dec =
    (=)

  (** val nat_list_max : int list -> int **)

  let rec nat_list_max = function
  | [] -> 0
  | y :: l0 -> Nat.max y (nat_list_max l0)

  (** val atom_fresh_for_list : int list -> int **)

  let atom_fresh_for_list xs =
    let s = nat_list_max xs in Pervasives.succ s

  (** val fresh : atom list -> int **)

  let fresh =
    atom_fresh_for_list

  (** val fresh_not_in : __ **)

  let fresh_not_in =
    __

  (** val nat_of : atom -> atom **)

  let nat_of x =
    x

  type eq = __

  (** val eq_equiv : __ **)

  let eq_equiv =
    __

  (** val eq_refl : __ **)

  let eq_refl =
    __

  (** val eq_sym : __ **)

  let eq_sym =
    __

  (** val eq_trans : __ **)

  let eq_trans =
    __
 end

module AtomSetImpl = Coq0_Make(Atom)

(** val atom_fresh : AtomSetImpl.t -> Atom.atom **)

let atom_fresh l =
  Atom.atom_fresh_for_list (AtomSetImpl.elements l)

module EnvImpl = Coq_Make(Atom)(AtomSetImpl)

type 'a canOpen =
  int -> Atom.atom -> 'a -> 'a
  (* singleton inductive, whose constructor was Build_CanOpen *)

(** val open_rec : 'a1 canOpen -> int -> Atom.atom -> 'a1 -> 'a1 **)

let open_rec canOpen0 =
  canOpen0

type 'a canClose =
  int -> Atom.atom -> 'a -> 'a
  (* singleton inductive, whose constructor was Build_CanClose *)

(** val close_rec : 'a1 canClose -> int -> Atom.atom -> 'a1 -> 'a1 **)

let close_rec canClose0 =
  canClose0

type ('elem, 't) listIsoCond = { to_list' : ('t -> 'elem list);
                                 from_list' : ('elem list -> 't);
                                 append' : ('t -> 't -> 't) }

(** val append' : ('a1, 'a2) listIsoCond -> 'a2 -> 'a2 -> 'a2 **)

let append' x = x.append'

type typ_label = int
  (* singleton inductive, whose constructor was typ_lab *)

type trm_label = int
  (* singleton inductive, whose constructor was trm_lab *)

(** val eqDecTypLab : typ_label eqDec_eq **)

let eqDecTypLab =
  (=)

(** val eqDecTrmLab : trm_label eqDec_eq **)

let eqDecTrmLab =
  (=)

type label =
| Label_typ of typ_label
| Label_trm of trm_label

module Label =
 struct
  type t = label

  (** val eq_dec : t -> t -> bool **)

  let eq_dec x y =
    match x with
    | Label_typ x0 ->
      (match y with
       | Label_typ t1 -> eq_dec0 eqDecTypLab x0 t1
       | Label_trm _ -> false)
    | Label_trm x0 ->
      (match y with
       | Label_typ _ -> false
       | Label_trm t1 -> eq_dec0 eqDecTrmLab x0 t1)
 end

module LabelSetImpl = Coq0_Make(Label)

module LabelAssocList = Coq_Make(Label)(LabelSetImpl)

type 'm monad = { ret : (__ -> __ -> 'm);
                  bind : (__ -> __ -> 'm -> (__ -> 'm) -> 'm) }

(** val ret : 'a1 monad -> 'a2 -> 'a1 **)

let ret monad0 x =
  let { ret = ret0; bind = _ } = monad0 in Obj.magic ret0 __ x

(** val bind : 'a1 monad -> 'a1 -> ('a2 -> 'a1) -> 'a1 **)

let bind monad0 x x0 =
  let { ret = _; bind = bind0 } = monad0 in Obj.magic bind0 __ __ x x0

type 'm pMonad = { pret : (__ -> __ -> __ -> 'm);
                   pbind : (__ -> __ -> __ -> 'm -> (__ -> 'm) -> 'm) }

type ('m, 'x) monP = __

(** val pbind :
    'a1 pMonad -> ('a1, 'a3) monP -> 'a1 -> ('a2 -> 'a1) -> 'a1 **)

let pbind pMonad0 pu x x0 =
  let { pret = _; pbind = pbind0 } = pMonad0 in Obj.magic pbind0 __ __ pu x x0

(** val pMonad_Monad : 'a1 monad -> 'a1 pMonad **)

let pMonad_Monad m =
  { pret = (fun _ -> Obj.magic (fun _ x -> ret m x)); pbind = (fun _ _ ->
    Obj.magic (fun _ c f -> bind m c f)) }

(** val monad_either : ('a1, __) sum monad **)

let monad_either =
  { ret = (fun _ v -> Inr v); bind = (fun _ _ c1 c2 ->
    match c1 with
    | Inl v -> Inl v
    | Inr v -> c2 v) }

(** val err_msg :
    (char list list, 'a1) sum -> char list -> (char list list, 'a1) sum **)

let err_msg m msg =
  match m with
  | Inl msgs -> Inl (msg :: msgs)
  | Inr x -> Inr x

(** val erret : char list -> (char list list, 'a1) sum **)

let erret m =
  Inl (m :: [])

(** val get_witness :
    Atom.atom -> (Atom.atom * 'a1) list -> 'a1 -> ((Atom.atom * 'a1) list,
    (Atom.atom * 'a1) list) sigT **)

let rec get_witness v l r =
  match l with
  | [] -> assert false (* absurd case *)
  | y :: l0 ->
    let (a, a0) = y in
    let s = eq_dec0 EnvImpl.coq_EqDec_eq_of_X v a in
    if s
    then ExistT ([], l0)
    else let h = get_witness v l0 r in
         let ExistT (h0, x) = h in
         ExistT ((app (LabelAssocList.one (a, a0)) h0), x)

(** val get_for_sure : (Atom.atom * 'a1) list -> Atom.atom -> 'a1 **)

let rec get_for_sure l x =
  match l with
  | [] -> assert false (* absurd case *)
  | y :: l0 ->
    let (a, t0) = y in
    let s = eq_dec0 EnvImpl.coq_EqDec_eq_of_X x a in
    if s then t0 else get_for_sure l0 x

type avar =
| Avar_b of int
| Avar_f of Atom.atom

(** val eqAvar : avar eqDec_eq **)

let eqAvar x y =
  match x with
  | Avar_b x0 -> (match y with
                  | Avar_b n0 -> (=) x0 n0
                  | Avar_f _ -> false)
  | Avar_f x0 ->
    (match y with
     | Avar_b _ -> false
     | Avar_f a0 -> Atom.eq_dec x0 a0)

type typ =
| Typ_top
| Typ_bot
| Typ_sel of avar * typ_label
| Typ_all of typ * typ
| Typ_rcd of decs
and dec =
| Dec_typ of typ_label * typ * typ
| Dec_trm of trm_label * typ
and decs =
| Decs_nil
| Decs_cons of dec * decs

(** val lab_dec_of : dec -> label **)

let lab_dec_of = function
| Dec_typ (a, _, _) -> Label_typ a
| Dec_trm (a, _) -> Label_trm a

(** val decs_to_list' : decs -> (label * dec) list **)

let rec decs_to_list' = function
| Decs_nil -> []
| Decs_cons (d, dS') -> ((lab_dec_of d), d) :: (decs_to_list' dS')

(** val decs_from_list' : (label * dec) list -> decs **)

let rec decs_from_list' = function
| [] -> Decs_nil
| p :: l' -> let (_, d) = p in Decs_cons (d, (decs_from_list' l'))

(** val decs_append' : decs -> decs -> decs **)

let rec decs_append' dS1 dS2 =
  match dS1 with
  | Decs_nil -> dS2
  | Decs_cons (d, dS1') -> Decs_cons (d, (decs_append' dS1' dS2))

(** val decsListCond : (label * dec, decs) listIsoCond **)

let decsListCond =
  { to_list' = decs_to_list'; from_list' = decs_from_list'; append' =
    decs_append' }

(** val typ_eqdec : typ -> typ -> bool **)

let rec typ_eqdec t1 t2 =
  match t1 with
  | Typ_top -> (match t2 with
                | Typ_top -> true
                | _ -> false)
  | Typ_bot -> (match t2 with
                | Typ_bot -> true
                | _ -> false)
  | Typ_sel (a, t0) ->
    (match t2 with
     | Typ_sel (a0, t3) ->
       if eq_dec0 eqAvar a a0 then eq_dec0 eqDecTypLab t0 t3 else false
     | _ -> false)
  | Typ_all (t0, t3) ->
    (match t2 with
     | Typ_all (t4, t5) -> if typ_eqdec t0 t4 then typ_eqdec t3 t5 else false
     | _ -> false)
  | Typ_rcd d -> (match t2 with
                  | Typ_rcd d0 -> decs_eqdec d d0
                  | _ -> false)

(** val dec_eqdec : dec -> dec -> bool **)

and dec_eqdec d1 d2 =
  match d1 with
  | Dec_typ (x, x0, x1) ->
    (match d2 with
     | Dec_typ (t2, t3, t4) ->
       if eq_dec0 eqDecTypLab x t2
       then if typ_eqdec x0 t3 then typ_eqdec x1 t4 else false
       else false
     | Dec_trm (_, _) -> false)
  | Dec_trm (x, x0) ->
    (match d2 with
     | Dec_typ (_, _, _) -> false
     | Dec_trm (t1, t2) ->
       if eq_dec0 eqDecTrmLab x t1 then typ_eqdec x0 t2 else false)

(** val decs_eqdec : decs -> decs -> bool **)

and decs_eqdec dS1 dS2 =
  match dS1 with
  | Decs_nil -> (match dS2 with
                 | Decs_nil -> true
                 | Decs_cons (_, _) -> false)
  | Decs_cons (d, d0) ->
    (match dS2 with
     | Decs_nil -> false
     | Decs_cons (d1, d2) ->
       if dec_eqdec d d1 then decs_eqdec d0 d2 else false)

(** val eqDecTyp : typ eqDec_eq **)

let eqDecTyp =
  typ_eqdec

type trm =
| Trm_var of avar
| Trm_val of val0
| Trm_sel of avar * trm_label
| Trm_app of avar * avar
| Trm_let of trm * trm
and val0 =
| Val_rcd of defs
| Val_lam of typ * trm
and def =
| Def_typ of typ_label * typ
| Def_trm of trm_label * trm
and defs =
| Defs_nil
| Defs_cons of def * defs

(** val lab_def_of : def -> label **)

let lab_def_of = function
| Def_typ (a, _) -> Label_typ a
| Def_trm (a, _) -> Label_trm a

(** val defs_to_list' : defs -> (label * def) list **)

let rec defs_to_list' = function
| Defs_nil -> []
| Defs_cons (d, dS') -> ((lab_def_of d), d) :: (defs_to_list' dS')

(** val open_rec_avar : int -> Atom.atom -> avar -> avar **)

let open_rec_avar k u = function
| Avar_b i ->
  if eq_dec0 (eqDec_eq_of_EqDec nat_eq_eqdec) k i then Avar_f u else Avar_b i
| Avar_f x -> Avar_f x

(** val open_rec_typ : int -> Atom.atom -> typ -> typ **)

let rec open_rec_typ k u = function
| Typ_sel (x, t1) -> Typ_sel ((open_rec_avar k u x), t1)
| Typ_all (t1, u0) ->
  Typ_all ((open_rec_typ k u t1), (open_rec_typ (Pervasives.succ k) u u0))
| Typ_rcd dS -> Typ_rcd (open_rec_decs k u dS)
| x -> x

(** val open_rec_dec : int -> Atom.atom -> dec -> dec **)

and open_rec_dec k u = function
| Dec_typ (x, t0, u0) ->
  Dec_typ (x, (open_rec_typ k u t0), (open_rec_typ k u u0))
| Dec_trm (x, t0) -> Dec_trm (x, (open_rec_typ k u t0))

(** val open_rec_decs : int -> Atom.atom -> decs -> decs **)

and open_rec_decs k u = function
| Decs_nil -> Decs_nil
| Decs_cons (d, dS') ->
  Decs_cons ((open_rec_dec k u d), (open_rec_decs k u dS'))

(** val open_rec_trm : int -> Atom.atom -> trm -> trm **)

let rec open_rec_trm k u = function
| Trm_var a -> Trm_var (open_rec_avar k u a)
| Trm_val v -> Trm_val (open_rec_val k u v)
| Trm_sel (v, m) -> Trm_sel ((open_rec_avar k u v), m)
| Trm_app (f, x) -> Trm_app ((open_rec_avar k u f), (open_rec_avar k u x))
| Trm_let (t1, t2) ->
  Trm_let ((open_rec_trm k u t1), (open_rec_trm (Pervasives.succ k) u t2))

(** val open_rec_val : int -> Atom.atom -> val0 -> val0 **)

and open_rec_val k u = function
| Val_rcd ds -> Val_rcd (open_rec_defs k u ds)
| Val_lam (t0, e) ->
  Val_lam ((open_rec_typ k u t0), (open_rec_trm (Pervasives.succ k) u e))

(** val open_rec_def : int -> Atom.atom -> def -> def **)

and open_rec_def k u = function
| Def_typ (x, t0) -> Def_typ (x, (open_rec_typ k u t0))
| Def_trm (x, e) -> Def_trm (x, (open_rec_trm k u e))

(** val open_rec_defs : int -> Atom.atom -> defs -> defs **)

and open_rec_defs k u = function
| Defs_nil -> Defs_nil
| Defs_cons (df, ds') ->
  Defs_cons ((open_rec_def k u df), (open_rec_defs k u ds'))

(** val openTyp : typ canOpen **)

let openTyp =
  open_rec_typ

(** val openTrm : trm canOpen **)

let openTrm =
  open_rec_trm

(** val close_rec_avar : int -> Atom.atom -> avar -> avar **)

let close_rec_avar k u = function
| Avar_b i -> Avar_b i
| Avar_f x ->
  if eq_dec0 EnvImpl.coq_EqDec_eq_of_X x u then Avar_b k else Avar_f x

(** val close_rec_typ : int -> Atom.atom -> typ -> typ **)

let rec close_rec_typ k u = function
| Typ_sel (x, t1) -> Typ_sel ((close_rec_avar k u x), t1)
| Typ_all (t1, u0) ->
  Typ_all ((close_rec_typ k u t1), (close_rec_typ (Pervasives.succ k) u u0))
| Typ_rcd dS -> Typ_rcd (close_rec_decs k u dS)
| x -> x

(** val close_rec_dec : int -> Atom.atom -> dec -> dec **)

and close_rec_dec k u = function
| Dec_typ (x, t0, u0) ->
  Dec_typ (x, (close_rec_typ k u t0), (close_rec_typ k u u0))
| Dec_trm (x, t0) -> Dec_trm (x, (close_rec_typ k u t0))

(** val close_rec_decs : int -> Atom.atom -> decs -> decs **)

and close_rec_decs k u = function
| Decs_nil -> Decs_nil
| Decs_cons (d, dS') ->
  Decs_cons ((close_rec_dec k u d), (close_rec_decs k u dS'))

(** val closeTyp : typ canClose **)

let closeTyp =
  close_rec_typ

(** val fv_avar : avar -> AtomSetImpl.t **)

let fv_avar = function
| Avar_b _ -> AtomSetImpl.empty
| Avar_f x -> AtomSetImpl.singleton x

(** val fv_typ : typ -> AtomSetImpl.t **)

let rec fv_typ = function
| Typ_sel (x, _) -> fv_avar x
| Typ_all (t1, u) -> AtomSetImpl.union (fv_typ t1) (fv_typ u)
| Typ_rcd dS -> fv_decs dS
| _ -> AtomSetImpl.empty

(** val fv_dec : dec -> AtomSetImpl.t **)

and fv_dec = function
| Dec_typ (_, t0, u) -> AtomSetImpl.union (fv_typ t0) (fv_typ u)
| Dec_trm (_, t0) -> fv_typ t0

(** val fv_decs : decs -> AtomSetImpl.t **)

and fv_decs = function
| Decs_nil -> AtomSetImpl.empty
| Decs_cons (d, dS') -> AtomSetImpl.union (fv_dec d) (fv_decs dS')

(** val fv_trm : trm -> AtomSetImpl.t **)

let rec fv_trm = function
| Trm_var a -> fv_avar a
| Trm_val v -> fv_val v
| Trm_sel (v, _) -> fv_avar v
| Trm_app (f, x) -> AtomSetImpl.union (fv_avar f) (fv_avar x)
| Trm_let (t1, t2) -> AtomSetImpl.union (fv_trm t1) (fv_trm t2)

(** val fv_val : val0 -> AtomSetImpl.t **)

and fv_val = function
| Val_rcd ds -> fv_defs ds
| Val_lam (t0, e) -> AtomSetImpl.union (fv_typ t0) (fv_trm e)

(** val fv_def : def -> AtomSetImpl.t **)

and fv_def = function
| Def_typ (_, t0) -> fv_typ t0
| Def_trm (_, e) -> fv_trm e

(** val fv_defs : defs -> AtomSetImpl.t **)

and fv_defs = function
| Defs_nil -> AtomSetImpl.empty
| Defs_cons (d, ds') -> AtomSetImpl.union (fv_def d) (fv_defs ds')

(** val fv_values :
    ('a1 -> AtomSetImpl.t) -> (Atom.atom * 'a1) list -> AtomSetImpl.t **)

let fv_values f l =
  fold_right (fun b a -> AtomSetImpl.union a (let (_, t0) = b in f t0))
    AtomSetImpl.empty l

(** val check_fv_avar : avar -> bool **)

let check_fv_avar = function
| Avar_b _ -> true
| Avar_f _ -> false

(** val check_fv_typ : typ -> bool **)

let rec check_fv_typ = function
| Typ_sel (a, _) -> check_fv_avar a
| Typ_all (t1, t2) ->
  let s = check_fv_typ t1 in if s then check_fv_typ t2 else false
| Typ_rcd d -> check_fv_decs d
| _ -> true

(** val check_fv_dec : dec -> bool **)

and check_fv_dec = function
| Dec_typ (_, t0, t1) ->
  let s = check_fv_typ t0 in if s then check_fv_typ t1 else false
| Dec_trm (_, t0) -> check_fv_typ t0

(** val check_fv_decs : decs -> bool **)

and check_fv_decs = function
| Decs_nil -> true
| Decs_cons (d, dS0) ->
  let s = check_fv_dec d in if s then check_fv_decs dS0 else false

(** val check_fv_trm : trm -> bool **)

let rec check_fv_trm = function
| Trm_var a -> check_fv_avar a
| Trm_val v -> check_fv_val v
| Trm_sel (a, _) -> check_fv_avar a
| Trm_app (a, a0) ->
  let s = check_fv_avar a in if s then check_fv_avar a0 else false
| Trm_let (t0_1, t0_2) ->
  let s = check_fv_trm t0_1 in if s then check_fv_trm t0_2 else false

(** val check_fv_val : val0 -> bool **)

and check_fv_val = function
| Val_rcd d -> check_fv_defs d
| Val_lam (t0, t1) ->
  let s = check_fv_typ t0 in if s then check_fv_trm t1 else false

(** val check_fv_def : def -> bool **)

and check_fv_def = function
| Def_typ (_, t0) -> check_fv_typ t0
| Def_trm (_, t0) -> check_fv_trm t0

(** val check_fv_defs : defs -> bool **)

and check_fv_defs = function
| Defs_nil -> true
| Defs_cons (d, ds0) ->
  let s = check_fv_def d in if s then check_fv_defs ds0 else false

(** val check_lc_at_avar : int -> avar -> bool **)

let check_lc_at_avar n = function
| Avar_b n0 -> (<=) (Pervasives.succ n0) n
| Avar_f _ -> true

(** val check_lc_at_typ : int -> typ -> bool **)

let rec check_lc_at_typ n = function
| Typ_sel (a, _) -> check_lc_at_avar n a
| Typ_all (t1, t2) ->
  let s = check_lc_at_typ n t1 in
  if s then check_lc_at_typ (Pervasives.succ n) t2 else false
| Typ_rcd d -> check_lc_at_decs n d
| _ -> true

(** val check_lc_at_dec : int -> dec -> bool **)

and check_lc_at_dec n = function
| Dec_typ (_, t0, t1) ->
  let s = check_lc_at_typ n t0 in if s then check_lc_at_typ n t1 else false
| Dec_trm (_, t0) -> check_lc_at_typ n t0

(** val check_lc_at_decs : int -> decs -> bool **)

and check_lc_at_decs n = function
| Decs_nil -> true
| Decs_cons (d, dS0) ->
  let s = check_lc_at_dec n d in if s then check_lc_at_decs n dS0 else false

(** val check_lc_at_trm : int -> trm -> bool **)

let rec check_lc_at_trm n = function
| Trm_var a -> check_lc_at_avar n a
| Trm_val v -> check_lc_at_val n v
| Trm_sel (a, _) -> check_lc_at_avar n a
| Trm_app (a, a0) ->
  let s = check_lc_at_avar n a in if s then check_lc_at_avar n a0 else false
| Trm_let (t0_1, t0_2) ->
  let s = check_lc_at_trm n t0_1 in
  if s then check_lc_at_trm (Pervasives.succ n) t0_2 else false

(** val check_lc_at_val : int -> val0 -> bool **)

and check_lc_at_val n = function
| Val_rcd d -> check_lc_at_defs n d
| Val_lam (t0, t1) ->
  let s = check_lc_at_typ n t0 in
  if s then check_lc_at_trm (Pervasives.succ n) t1 else false

(** val check_lc_at_def : int -> def -> bool **)

and check_lc_at_def n = function
| Def_typ (_, t0) -> check_lc_at_typ n t0
| Def_trm (_, t0) -> check_lc_at_trm n t0

(** val check_lc_at_defs : int -> defs -> bool **)

and check_lc_at_defs n = function
| Defs_nil -> true
| Defs_cons (d, ds0) ->
  let s = check_lc_at_def n d in if s then check_lc_at_defs n ds0 else false

(** val defs_luniq_dec : defs -> bool **)

let rec defs_luniq_dec = function
| Defs_nil -> true
| Defs_cons (d0, d1) ->
  if defs_luniq_dec d1
  then let s =
         LabelAssocList.KeySetProperties.coq_In_dec (lab_def_of d0)
           (LabelAssocList.dom (defs_to_list' d1))
       in
       if s then false else true
  else false

(** val decs_luniq_dec : decs -> bool **)

let rec decs_luniq_dec = function
| Decs_nil -> true
| Decs_cons (d0, d1) ->
  if decs_luniq_dec d1
  then let s =
         LabelAssocList.KeySetProperties.coq_In_dec (lab_dec_of d0)
           (LabelAssocList.dom (decs_to_list' d1))
       in
       if s then false else true
  else false

(** val uniq_dom_typ_dec : typ -> bool **)

let rec uniq_dom_typ_dec x =
  let rec f = function
  | Typ_all (t1, t2) -> if f t2 then f t1 else false
  | Typ_rcd d ->
    let s = decs_luniq_dec d in if s then uniq_dom_decs_dec d else false
  | _ -> true
  in f x

(** val uniq_dom_dec_dec : dec -> bool **)

and uniq_dom_dec_dec = function
| Dec_typ (_, x0, x1) ->
  let s = uniq_dom_typ_dec x0 in if s then uniq_dom_typ_dec x1 else false
| Dec_trm (_, x0) -> uniq_dom_typ_dec x0

(** val uniq_dom_decs_dec : decs -> bool **)

and uniq_dom_decs_dec x =
  let rec f = function
  | Decs_nil -> true
  | Decs_cons (d0, d1) -> if f d1 then uniq_dom_dec_dec d0 else false
  in f x

(** val uniq_dom_trm_dec : trm -> bool **)

let rec uniq_dom_trm_dec x =
  let rec f = function
  | Trm_val v -> uniq_dom_val_dec v
  | Trm_let (t1, t2) -> if f t2 then f t1 else false
  | _ -> true
  in f x

(** val uniq_dom_val_dec : val0 -> bool **)

and uniq_dom_val_dec = function
| Val_rcd x0 ->
  let s = defs_luniq_dec x0 in if s then uniq_dom_defs_dec x0 else false
| Val_lam (x0, x1) ->
  let s = uniq_dom_typ_dec x0 in if s then uniq_dom_trm_dec x1 else false

(** val uniq_dom_def_dec : def -> bool **)

and uniq_dom_def_dec = function
| Def_typ (_, x0) -> uniq_dom_typ_dec x0
| Def_trm (_, x0) -> uniq_dom_trm_dec x0

(** val uniq_dom_defs_dec : defs -> bool **)

and uniq_dom_defs_dec x =
  let rec f = function
  | Defs_nil -> true
  | Defs_cons (d0, d1) -> if f d1 then uniq_dom_def_dec d0 else false
  in f x

(** val exposure_impl :
    (Atom.atom * typ) list -> typ -> (char list list, typ) sum **)

let exposure_impl g s =
  let rec fix_F_sub x =
    let g0 = projT1 x in
    let s0 = projT2 x in
    let exposure_impl0 = fun g1 s1 -> fix_F_sub (ExistT (g1, s1)) in
    (fun _ _ _ ->
    match s0 with
    | Typ_sel (a, a0) ->
      (match a with
       | Avar_b _ -> assert false (* absurd case *)
       | Avar_f a1 ->
         let s1 = get_for_sure g0 a1 in
         let h2 = get_witness a1 g0 s1 in
         let ExistT (_, x0) = h2 in
         let recur = exposure_impl0 x0 s1 __ __ __ in
         pbind (pMonad_Monad (Obj.magic monad_either)) (Obj.magic __)
           (err_msg recur
             ('e'::('x'::('p'::('o'::('s'::('u'::('r'::('e'::(':'::(' '::('n'::('o'::('t'::(' '::('a'::('b'::('l'::('e'::(' '::('t'::('o'::(' '::('p'::('e'::('r'::('f'::('o'::('r'::('m'::(' '::('e'::('x'::('p'::('o'::('s'::('u'::('r'::('e'::(' '::('a'::('f'::('t'::('e'::('r'::(' '::('l'::('o'::('o'::('k'::('u'::('p'::[]))))))))))))))))))))))))))))))))))))))))))))))))))))
           (fun x1 ->
           match x1 with
           | Typ_bot -> ret (Obj.magic monad_either) Typ_bot
           | Typ_rcd d ->
             let o = LabelAssocList.get (Label_typ a0) (decs_to_list' d) in
             (match o with
              | Some d0 ->
                (match d0 with
                 | Dec_typ (_, _, t1) ->
                   let recur0 = exposure_impl0 x0 t1 __ __ __ in
                   pbind (pMonad_Monad (Obj.magic monad_either))
                     (Obj.magic __)
                     (err_msg recur0
                       ('e'::('x'::('p'::('o'::('s'::('u'::('r'::('e'::(':'::(' '::('f'::('a'::('i'::('l'::('e'::('d'::(' '::('s'::('e'::('c'::('o'::('n'::('d'::(' '::('r'::('e'::('c'::('u'::('r'::('s'::('i'::('v'::('e'::(' '::('c'::('a'::('l'::('l'::[])))))))))))))))))))))))))))))))))))))))
                     (fun x2 -> ret (Obj.magic monad_either) x2)
                 | Dec_trm (_, _) -> assert false (* absurd case *))
              | None ->
                erret
                  ('e'::('x'::('p'::('o'::('s'::('u'::('r'::('e'::(':'::(' '::('l'::('a'::('b'::('e'::('l'::(' '::('i'::('s'::(' '::('n'::('o'::('t'::(' '::('f'::('o'::('u'::('n'::('d'::[])))))))))))))))))))))))))))))
           | _ ->
             erret
               ('e'::('x'::('p'::('o'::('s'::('u'::('r'::('e'::(':'::(' '::('n'::('o'::('t'::(' '::('a'::(' '::('b'::('o'::('t'::('t'::('o'::('m'::(' '::('o'::('r'::(' '::('a'::(' '::('r'::('e'::('c'::('o'::('r'::('d'::(' '::('t'::('y'::('p'::('e'::[])))))))))))))))))))))))))))))))))))))))))
    | x0 -> ret (Obj.magic monad_either) x0)
  in fix_F_sub (ExistT (g, s)) __ __ __

(** val promo_demo_func_obligation_1 :
    (Atom.atom * typ) list -> Atom.atom -> typ -> ((Atom.atom * typ) list ->
    Atom.atom -> typ -> __ -> __ -> __ -> __ -> (char list list, typ)
    sum * (char list list, typ) sum) -> (char list list, typ)
    sum * (char list list, typ) sum **)

let promo_demo_func_obligation_1 g x s promo_demo0 =
  match s with
  | Typ_sel (a, t0) ->
    (match a with
     | Avar_b _ -> assert false (* absurd case *)
     | Avar_f a0 ->
       let s0 = eq_dec0 EnvImpl.coq_EqDec_eq_of_X x a0 in
       if s0
       then let s1 = get_for_sure g a0 in
            let h2 = get_witness a0 g s1 in
            let ExistT (_, x0) = h2 in
            let recur = exposure_impl x0 s1 in
            ((pbind (pMonad_Monad (Obj.magic monad_either)) (Obj.magic __)
               (err_msg recur
                 ('p'::('r'::('o'::('m'::('o'::('/'::('d'::('e'::('m'::('o'::(':'::(' '::('e'::('x'::('p'::('o'::('s'::('u'::('r'::('e'::(' '::('f'::('a'::('i'::('l'::('e'::('d'::(' '::('f'::('o'::('r'::(' '::('v'::('a'::('r'::(' '::('c'::('a'::('s'::('e'::('.'::[]))))))))))))))))))))))))))))))))))))))))))
               (fun x1 ->
               match x1 with
               | Typ_bot -> ret (Obj.magic monad_either) Typ_bot
               | Typ_rcd d ->
                 let o = LabelAssocList.get (Label_typ t0) (decs_to_list' d)
                 in
                 (match o with
                  | Some d0 ->
                    (match d0 with
                     | Dec_typ (_, _, t2) -> ret (Obj.magic monad_either) t2
                     | Dec_trm (_, _) -> assert false (* absurd case *))
                  | None ->
                    erret
                      ('p'::('r'::('o'::('m'::('o'::('/'::('d'::('e'::('m'::('o'::(':'::(' '::('l'::('a'::('b'::('e'::('l'::(' '::('i'::('s'::(' '::('n'::('o'::('t'::(' '::('f'::('o'::('u'::('n'::('d'::[])))))))))))))))))))))))))))))))
               | _ ->
                 erret
                   ('p'::('r'::('o'::('m'::('o'::('/'::('d'::('e'::('m'::('o'::(':'::(' '::('n'::('o'::('t'::(' '::('a'::(' '::('b'::('o'::('t'::('t'::('o'::('m'::(' '::('o'::('r'::(' '::('a'::(' '::('r'::('e'::('c'::('o'::('r'::('d'::(' '::('t'::('y'::('p'::('e'::[]))))))))))))))))))))))))))))))))))))))))))),
            (pbind (pMonad_Monad (Obj.magic monad_either)) (Obj.magic __)
              (err_msg recur
                ('p'::('r'::('o'::('m'::('o'::('/'::('d'::('e'::('m'::('o'::(':'::(' '::('e'::('x'::('p'::('o'::('s'::('u'::('r'::('e'::(' '::('f'::('a'::('i'::('l'::('e'::('d'::(' '::('f'::('o'::('r'::(' '::('v'::('a'::('r'::(' '::('c'::('a'::('s'::('e'::('.'::[]))))))))))))))))))))))))))))))))))))))))))
              (fun x1 ->
              match x1 with
              | Typ_bot -> ret (Obj.magic monad_either) Typ_top
              | Typ_rcd d ->
                let o = LabelAssocList.get (Label_typ t0) (decs_to_list' d) in
                (match o with
                 | Some d0 ->
                   (match d0 with
                    | Dec_typ (_, t1, _) -> ret (Obj.magic monad_either) t1
                    | Dec_trm (_, _) -> assert false (* absurd case *))
                 | None ->
                   erret
                     ('p'::('r'::('o'::('m'::('o'::('/'::('d'::('e'::('m'::('o'::(':'::(' '::('l'::('a'::('b'::('e'::('l'::(' '::('i'::('s'::(' '::('n'::('o'::('t'::(' '::('f'::('o'::('u'::('n'::('d'::[])))))))))))))))))))))))))))))))
              | _ ->
                erret
                  ('p'::('r'::('o'::('m'::('o'::('/'::('d'::('e'::('m'::('o'::(':'::(' '::('n'::('o'::('t'::(' '::('a'::(' '::('b'::('o'::('t'::('t'::('o'::('m'::(' '::('o'::('r'::(' '::('a'::(' '::('r'::('e'::('c'::('o'::('r'::('d'::(' '::('t'::('y'::('p'::('e'::[]))))))))))))))))))))))))))))))))))))))))))))
       else ((ret (Obj.magic monad_either) (Typ_sel ((Avar_f a0), t0))),
              (ret (Obj.magic monad_either) (Typ_sel ((Avar_f a0), t0)))))
  | Typ_all (s1, s2) ->
    let p = promo_demo0 g x s1 __ __ __ __ in
    let (s0, s3) = p in
    ((pbind (pMonad_Monad (Obj.magic monad_either)) (Obj.magic __)
       (err_msg s3
         ('p'::('r'::('o'::('m'::('o'::('t'::('i'::('o'::('n'::(':'::(' '::('f'::('a'::('i'::('l'::('e'::('d'::(' '::('i'::('n'::(' '::('f'::('u'::('n'::('c'::('t'::('i'::('o'::('n'::(' '::('c'::('a'::('s'::('e'::[])))))))))))))))))))))))))))))))))))
       (fun x0 ->
       let s4 =
         atom_fresh
           (AtomSetImpl.union (AtomSetImpl.singleton x)
             (AtomSetImpl.union (EnvImpl.dom g)
               (AtomSetImpl.union (fv_values fv_typ g)
                 (AtomSetImpl.union (AtomSetImpl.singleton x)
                   (AtomSetImpl.union (fv_typ s1)
                     (AtomSetImpl.union (fv_typ s2) (fv_typ x0)))))))
       in
       let p0 =
         promo_demo0 (app (EnvImpl.one (s4, x0)) g) x
           (open_rec openTyp 0 s4 s2) __ __ __ __
       in
       let (s5, _) = p0 in
       pbind (pMonad_Monad (Obj.magic monad_either)) (Obj.magic __)
         (err_msg s5
           ('p'::('r'::('o'::('m'::('o'::('t'::('i'::('o'::('n'::(':'::(' '::('f'::('a'::('i'::('l'::('e'::('d'::(' '::('i'::('n'::(' '::('f'::('u'::('n'::('c'::('t'::('i'::('o'::('n'::(' '::('b'::('o'::('d'::('y'::(' '::('t'::('y'::('p'::('e'::[]))))))))))))))))))))))))))))))))))))))))
         (fun x1 ->
         ret (Obj.magic monad_either) (Typ_all (x0,
           (close_rec closeTyp 0 s4 x1)))))),
    (pbind (pMonad_Monad (Obj.magic monad_either)) (Obj.magic __)
      (err_msg s0
        ('d'::('e'::('m'::('o'::('t'::('i'::('o'::('n'::(':'::(' '::('f'::('a'::('i'::('l'::('e'::('d'::(' '::('i'::('n'::(' '::('f'::('u'::('n'::('c'::('t'::('i'::('o'::('n'::(' '::('c'::('a'::('s'::('e'::[]))))))))))))))))))))))))))))))))))
      (fun x0 ->
      let s4 =
        atom_fresh
          (AtomSetImpl.union (AtomSetImpl.singleton x)
            (AtomSetImpl.union (EnvImpl.dom g)
              (AtomSetImpl.union (fv_values fv_typ g)
                (AtomSetImpl.union (AtomSetImpl.singleton x)
                  (AtomSetImpl.union (fv_typ s1)
                    (AtomSetImpl.union (fv_typ s2) (fv_typ x0)))))))
      in
      let p0 =
        promo_demo0 (app (EnvImpl.one (s4, s1)) g) x
          (open_rec openTyp 0 s4 s2) __ __ __ __
      in
      let (_, s5) = p0 in
      pbind (pMonad_Monad (Obj.magic monad_either)) (Obj.magic __)
        (err_msg s5
          ('p'::('r'::('o'::('m'::('o'::('t'::('i'::('o'::('n'::(':'::(' '::('f'::('a'::('i'::('l'::('e'::('d'::(' '::('i'::('n'::(' '::('f'::('u'::('n'::('c'::('t'::('i'::('o'::('n'::(' '::('b'::('o'::('d'::('y'::(' '::('t'::('y'::('p'::('e'::[]))))))))))))))))))))))))))))))))))))))))
        (fun x1 ->
        ret (Obj.magic monad_either) (Typ_all (x0,
          (close_rec closeTyp 0 s4 x1)))))))
  | Typ_rcd d ->
    let h2 = fun h2 ->
      let rec f = function
      | Decs_nil -> ((ret monad_either Decs_nil), (ret monad_either Decs_nil))
      | Decs_cons (d1, d2) ->
        let p = f d2 in
        let (s0, s1) = p in
        (match d1 with
         | Dec_typ (t0, t1, t2) ->
           let p0 = promo_demo0 g x t1 __ __ __ __ in
           let (s2, s3) = p0 in
           let p1 = promo_demo0 g x t2 __ __ __ __ in
           let (s4, s5) = p1 in
           ((pbind (pMonad_Monad monad_either) (Obj.magic __)
              (err_msg s0
                ('p'::('r'::('o'::('m'::('o'::('t'::('i'::('o'::('n'::(':'::(' '::('f'::('a'::('i'::('l'::('e'::('d'::(' '::('i'::('n'::(' '::('r'::('e'::('c'::('u'::('r'::('s'::('i'::('v'::('e'::(' '::('c'::('a'::('s'::('e'::(' '::('f'::('o'::('r'::(' '::('d'::('e'::('c'::('l'::('a'::('r'::('a'::('t'::('i'::('o'::('n'::('s'::[])))))))))))))))))))))))))))))))))))))))))))))))))))))
              (fun x0 ->
              pbind (pMonad_Monad monad_either) (Obj.magic __)
                (err_msg (Obj.magic s3)
                  ('p'::('r'::('o'::('m'::('o'::('t'::('i'::('o'::('n'::(':'::(' '::('f'::('a'::('i'::('l'::('e'::('d'::(' '::('i'::('n'::(' '::('l'::('o'::('w'::('e'::('r'::(' '::('b'::('o'::('u'::('n'::('d'::(' '::('c'::('a'::('s'::('e'::(' '::('f'::('o'::('r'::(' '::('d'::('e'::('c'::('l'::('a'::('r'::('a'::('t'::('i'::('o'::('n'::('s'::[])))))))))))))))))))))))))))))))))))))))))))))))))))))))
                (fun x1 ->
                pbind (pMonad_Monad monad_either) (Obj.magic __)
                  (err_msg (Obj.magic s4)
                    ('p'::('r'::('o'::('m'::('o'::('t'::('i'::('o'::('n'::(':'::(' '::('f'::('a'::('i'::('l'::('e'::('d'::(' '::('i'::('n'::(' '::('u'::('p'::('p'::('e'::('r'::(' '::('b'::('o'::('u'::('n'::('d'::(' '::('c'::('a'::('s'::('e'::(' '::('f'::('o'::('r'::(' '::('d'::('e'::('c'::('l'::('a'::('r'::('a'::('t'::('i'::('o'::('n'::('s'::[])))))))))))))))))))))))))))))))))))))))))))))))))))))))
                  (fun x2 ->
                  ret monad_either
                    (decsListCond.append' (Decs_cons ((Dec_typ (t0, x1, x2)),
                      Decs_nil)) x0))))),
           (pbind (pMonad_Monad monad_either) (Obj.magic __)
             (err_msg s1
               ('d'::('e'::('m'::('o'::('t'::('i'::('o'::('n'::(':'::(' '::('f'::('a'::('i'::('l'::('e'::('d'::(' '::('i'::('n'::(' '::('r'::('e'::('c'::('u'::('r'::('s'::('i'::('v'::('e'::(' '::('c'::('a'::('s'::('e'::(' '::('f'::('o'::('r'::(' '::('d'::('e'::('c'::('l'::('a'::('r'::('a'::('t'::('i'::('o'::('n'::('s'::[]))))))))))))))))))))))))))))))))))))))))))))))))))))
             (fun x0 ->
             pbind (pMonad_Monad monad_either) (Obj.magic __)
               (err_msg (Obj.magic s2)
                 ('d'::('e'::('m'::('o'::('t'::('i'::('o'::('n'::(':'::(' '::('f'::('a'::('i'::('l'::('e'::('d'::(' '::('i'::('n'::(' '::('l'::('o'::('w'::('e'::('r'::(' '::('b'::('o'::('u'::('n'::('d'::(' '::('c'::('a'::('s'::('e'::(' '::('f'::('o'::('r'::(' '::('d'::('e'::('c'::('l'::('a'::('r'::('a'::('t'::('i'::('o'::('n'::('s'::[]))))))))))))))))))))))))))))))))))))))))))))))))))))))
               (fun x1 ->
               pbind (pMonad_Monad monad_either) (Obj.magic __)
                 (err_msg (Obj.magic s5)
                   ('d'::('e'::('m'::('o'::('t'::('i'::('o'::('n'::(':'::(' '::('f'::('a'::('i'::('l'::('e'::('d'::(' '::('i'::('n'::(' '::('u'::('p'::('p'::('e'::('r'::(' '::('b'::('o'::('u'::('n'::('d'::(' '::('c'::('a'::('s'::('e'::(' '::('f'::('o'::('r'::(' '::('d'::('e'::('c'::('l'::('a'::('r'::('a'::('t'::('i'::('o'::('n'::('s'::[]))))))))))))))))))))))))))))))))))))))))))))))))))))))
                 (fun x2 ->
                 ret monad_either
                   (decsListCond.append' (Decs_cons ((Dec_typ (t0, x1, x2)),
                     Decs_nil)) x0))))))
         | Dec_trm (t0, t1) ->
           let p0 = promo_demo0 g x t1 __ __ __ __ in
           let (s2, s3) = p0 in
           ((pbind (pMonad_Monad monad_either) (Obj.magic __)
              (err_msg s0
                ('p'::('r'::('o'::('m'::('o'::('t'::('i'::('o'::('n'::(':'::(' '::('f'::('a'::('i'::('l'::('e'::('d'::(' '::('i'::('n'::(' '::('r'::('e'::('c'::('u'::('r'::('s'::('i'::('v'::('e'::(' '::('c'::('a'::('s'::('e'::(' '::('f'::('o'::('r'::(' '::('d'::('e'::('c'::('l'::('a'::('r'::('a'::('t'::('i'::('o'::('n'::('s'::[])))))))))))))))))))))))))))))))))))))))))))))))))))))
              (fun x0 ->
              pbind (pMonad_Monad monad_either) (Obj.magic __)
                (err_msg (Obj.magic s2)
                  ('p'::('r'::('o'::('m'::('o'::('t'::('i'::('o'::('n'::(':'::(' '::('f'::('a'::('i'::('l'::('e'::('d'::(' '::('i'::('n'::(' '::('f'::('i'::('e'::('l'::('d'::(' '::('t'::('y'::('p'::('e'::(' '::('c'::('a'::('s'::('e'::(' '::('f'::('o'::('r'::(' '::('d'::('e'::('c'::('l'::('a'::('r'::('a'::('t'::('i'::('o'::('n'::('s'::[]))))))))))))))))))))))))))))))))))))))))))))))))))))))
                (fun x1 ->
                ret monad_either
                  (decsListCond.append' (Decs_cons ((Dec_trm (t0, x1)),
                    Decs_nil)) x0)))),
           (pbind (pMonad_Monad monad_either) (Obj.magic __)
             (err_msg s1
               ('p'::('r'::('o'::('m'::('o'::('t'::('i'::('o'::('n'::(':'::(' '::('f'::('a'::('i'::('l'::('e'::('d'::(' '::('i'::('n'::(' '::('r'::('e'::('c'::('u'::('r'::('s'::('i'::('v'::('e'::(' '::('c'::('a'::('s'::('e'::(' '::('f'::('o'::('r'::(' '::('d'::('e'::('c'::('l'::('a'::('r'::('a'::('t'::('i'::('o'::('n'::('s'::[])))))))))))))))))))))))))))))))))))))))))))))))))))))
             (fun x0 ->
             pbind (pMonad_Monad monad_either) (Obj.magic __)
               (err_msg (Obj.magic s3)
                 ('p'::('r'::('o'::('m'::('o'::('t'::('i'::('o'::('n'::(':'::(' '::('f'::('a'::('i'::('l'::('e'::('d'::(' '::('i'::('n'::(' '::('f'::('i'::('e'::('l'::('d'::(' '::('t'::('y'::('p'::('e'::(' '::('c'::('a'::('s'::('e'::(' '::('f'::('o'::('r'::(' '::('d'::('e'::('c'::('l'::('a'::('r'::('a'::('t'::('i'::('o'::('n'::('s'::[]))))))))))))))))))))))))))))))))))))))))))))))))))))))
               (fun x1 ->
               ret monad_either
                 (decsListCond.append' (Decs_cons ((Dec_trm (t0, x1)),
                   Decs_nil)) x0))))))
      in f h2
    in
    let p = h2 d in
    let (s0, s1) = p in
    ((pbind (pMonad_Monad (Obj.magic monad_either)) (Obj.magic __)
       (err_msg (Obj.magic s0)
         ('p'::('r'::('o'::('m'::('o'::('t'::('i'::('o'::('n'::(':'::(' '::('f'::('a'::('i'::('l'::('e'::('d'::(' '::('t'::('o'::(' '::('p'::('r'::('o'::('m'::('o'::('t'::('e'::(' '::('d'::('e'::('c'::('l'::('a'::('r'::('a'::('t'::('i'::('o'::('n'::('s'::[]))))))))))))))))))))))))))))))))))))))))))
       (fun x0 -> ret (Obj.magic monad_either) (Typ_rcd x0))),
    (pbind (pMonad_Monad (Obj.magic monad_either)) (Obj.magic __)
      (err_msg (Obj.magic s1)
        ('d'::('e'::('m'::('o'::('t'::('i'::('o'::('n'::(':'::(' '::('f'::('a'::('i'::('l'::('e'::('d'::(' '::('t'::('o'::(' '::('d'::('e'::('m'::('o'::('t'::('e'::(' '::('d'::('e'::('c'::('l'::('a'::('r'::('a'::('t'::('i'::('o'::('n'::('s'::[]))))))))))))))))))))))))))))))))))))))))
      (fun x0 -> ret (Obj.magic monad_either) (Typ_rcd x0))))
  | x0 ->
    ((ret (Obj.magic monad_either) x0), (ret (Obj.magic monad_either) x0))

(** val promo_demo_func :
    ((Atom.atom * typ) list, (Atom.atom, typ) sigT) sigT -> (char list list,
    typ) sum * (char list list, typ) sum **)

let promo_demo_func recarg =
  let rec fix_F_sub x =
    let g = projT1 x in
    let x0 = projT1 (projT2 x) in
    let s = projT2 (projT2 x) in
    let promo_demo0 = fun g0 x1 s0 ->
      fix_F_sub (ExistT (g0, (ExistT (x1, s0))))
    in
    (fun _ _ _ ->
    promo_demo_func_obligation_1 g x0 s (fun g0 x1 s0 _ ->
      promo_demo0 g0 x1 s0))
  in fix_F_sub recarg __ __ __

(** val promo_demo :
    (Atom.atom * typ) list -> Atom.atom -> typ -> (char list list, typ)
    sum * (char list list, typ) sum **)

let promo_demo g x s =
  promo_demo_func (ExistT (g, (ExistT (x, s))))

(** val promotion :
    (Atom.atom * typ) list -> Atom.atom -> typ -> (char list list, typ) sum **)

let promotion g x s =
  let p = promo_demo g x s in let (s0, _) = p in s0

type subtyp_dec_sig = __ -> __ -> __ -> __ -> __ -> (char list list, __) sum

type subtyp_checker = (Atom.atom * typ) list -> typ -> typ -> subtyp_dec_sig

(** val subty_dec :
    (Atom.atom * typ) list -> typ -> typ -> (char list list, __) sum **)

let subty_dec g t0 u =
  let rec fix_F_sub x =
    let g0 = projT1 x in
    let t1 = projT1 (projT2 x) in
    let u0 = projT2 (projT2 x) in
    let subty_dec0 = fun g1 t2 u1 ->
      fix_F_sub (ExistT (g1, (ExistT (t2, u1))))
    in
    (fun _ _ _ _ _ ->
    match t1 with
    | Typ_top ->
      (match u0 with
       | Typ_top -> ret (Obj.magic monad_either) __
       | _ ->
         erret
           ('s'::('u'::('b'::('t'::('y'::('p'::('e'::('r'::(':'::(' '::('u'::('n'::('k'::('n'::('o'::('w'::('n'::(' '::('h'::('o'::('w'::(' '::('t'::('o'::(' '::('c'::('o'::('m'::('p'::('a'::('r'::('e'::(' '::('t'::('w'::('o'::(' '::('t'::('y'::('p'::('e'::('s'::('.'::[]))))))))))))))))))))))))))))))))))))))))))))
    | Typ_bot -> ret (Obj.magic monad_either) __
    | Typ_sel (a, t2) ->
      (match u0 with
       | Typ_top -> ret (Obj.magic monad_either) __
       | Typ_bot ->
         erret
           ('s'::('u'::('b'::('t'::('y'::('p'::('e'::('r'::(':'::(' '::('u'::('n'::('k'::('n'::('o'::('w'::('n'::(' '::('h'::('o'::('w'::(' '::('t'::('o'::(' '::('c'::('o'::('m'::('p'::('a'::('r'::('e'::(' '::('t'::('w'::('o'::(' '::('t'::('y'::('p'::('e'::('s'::('.'::[])))))))))))))))))))))))))))))))))))))))))))
       | Typ_sel (a0, t3) ->
         (match a0 with
          | Avar_b _ -> assert false (* absurd case *)
          | Avar_f a1 ->
            (match a with
             | Avar_b _ -> assert false (* absurd case *)
             | Avar_f a2 ->
               let s = eq_dec0 EnvImpl.coq_EqDec_eq_of_X a2 a1 in
               if s
               then let s0 = eq_dec0 eqDecTypLab t2 t3 in
                    if s0
                    then ret (Obj.magic monad_either) __
                    else let s1 = get_for_sure g0 a1 in
                         let h6 = get_witness a1 g0 s1 in
                         let ExistT (_, x0) = h6 in
                         let h12 = exposure_impl x0 s1 in
                         pbind (pMonad_Monad (Obj.magic monad_either))
                           (Obj.magic __)
                           (err_msg (Obj.magic h12)
                             ('s'::('u'::('b'::('t'::('y'::('p'::('e'::('r'::(':'::(' '::('e'::('x'::('p'::('o'::('s'::('u'::('r'::('e'::(' '::('f'::('a'::('i'::('l'::('e'::('d'::[]))))))))))))))))))))))))))
                           (fun x1 ->
                           match x1 with
                           | Typ_bot -> ret (Obj.magic monad_either) __
                           | Typ_rcd d ->
                             let o =
                               LabelAssocList.get (Label_typ t2)
                                 (decs_to_list' d)
                             in
                             (match o with
                              | Some d0 ->
                                (match d0 with
                                 | Dec_typ (_, _, t4) ->
                                   let h24 =
                                     subty_dec0 g0 t4 (Typ_sel ((Avar_f a1),
                                       t3)) __ __ __ __ __
                                   in
                                   pbind
                                     (pMonad_Monad (Obj.magic monad_either))
                                     (Obj.magic __)
                                     (err_msg h24
                                       ('s'::('u'::('b'::('t'::('y'::('p'::('e'::('r'::(':'::(' '::('r'::('e'::('c'::('u'::('r'::('s'::('i'::('v'::('e'::(' '::('s'::('u'::('b'::('t'::('y'::('p'::('i'::('n'::('g'::(' '::('c'::('a'::('l'::('l'::(' '::('f'::('a'::('i'::('l'::('e'::('d'::('.'::[])))))))))))))))))))))))))))))))))))))))))))
                                     (fun _ ->
                                     ret (Obj.magic monad_either) __)
                                 | Dec_trm (_, _) ->
                                   assert false (* absurd case *))
                              | None ->
                                erret
                                  ('s'::('u'::('b'::('t'::('y'::('p'::('e'::('r'::(':'::(' '::('a'::(' '::('l'::('a'::('b'::('e'::('l'::(' '::('i'::('s'::(' '::('n'::('o'::('t'::(' '::('f'::('o'::('u'::('n'::('d'::[])))))))))))))))))))))))))))))))
                           | _ ->
                             erret
                               ('s'::('u'::('b'::('t'::('y'::('p'::('e'::('r'::(':'::(' '::('u'::('n'::('a'::('b'::('l'::('e'::(' '::('t'::('o'::(' '::('h'::('a'::('n'::('d'::('l'::('e'::(' '::('r'::('e'::('s'::('u'::('l'::('t'::('i'::('n'::('g'::(' '::('s'::('t'::('r'::('u'::('c'::('t'::('u'::('r'::('e'::[])))))))))))))))))))))))))))))))))))))))))))))))
               else let s0 = get_for_sure g0 a2 in
                    let h6 = get_witness a2 g0 s0 in
                    let ExistT (_, x0) = h6 in
                    let h12 = exposure_impl x0 s0 in
                    pbind (pMonad_Monad (Obj.magic monad_either))
                      (Obj.magic __)
                      (err_msg (Obj.magic h12)
                        ('s'::('u'::('b'::('t'::('y'::('p'::('e'::('r'::(':'::(' '::('e'::('x'::('p'::('o'::('s'::('u'::('r'::('e'::(' '::('f'::('a'::('i'::('l'::('e'::('d'::[]))))))))))))))))))))))))))
                      (fun x1 ->
                      match x1 with
                      | Typ_bot -> ret (Obj.magic monad_either) __
                      | Typ_rcd d ->
                        let o =
                          LabelAssocList.get (Label_typ t2) (decs_to_list' d)
                        in
                        (match o with
                         | Some d0 ->
                           (match d0 with
                            | Dec_typ (_, _, t4) ->
                              let h24 =
                                subty_dec0 g0 t4 (Typ_sel ((Avar_f a1), t3))
                                  __ __ __ __ __
                              in
                              pbind (pMonad_Monad (Obj.magic monad_either))
                                (Obj.magic __)
                                (err_msg h24
                                  ('s'::('u'::('b'::('t'::('y'::('p'::('e'::('r'::(':'::(' '::('r'::('e'::('c'::('u'::('r'::('s'::('i'::('v'::('e'::(' '::('s'::('u'::('b'::('t'::('y'::('p'::('i'::('n'::('g'::(' '::('c'::('a'::('l'::('l'::(' '::('f'::('a'::('i'::('l'::('e'::('d'::('.'::[])))))))))))))))))))))))))))))))))))))))))))
                                (fun _ -> ret (Obj.magic monad_either) __)
                            | Dec_trm (_, _) -> assert false (* absurd case *))
                         | None ->
                           erret
                             ('s'::('u'::('b'::('t'::('y'::('p'::('e'::('r'::(':'::(' '::('a'::(' '::('l'::('a'::('b'::('e'::('l'::(' '::('i'::('s'::(' '::('n'::('o'::('t'::(' '::('f'::('o'::('u'::('n'::('d'::[])))))))))))))))))))))))))))))))
                      | _ ->
                        erret
                          ('s'::('u'::('b'::('t'::('y'::('p'::('e'::('r'::(':'::(' '::('u'::('n'::('a'::('b'::('l'::('e'::(' '::('t'::('o'::(' '::('h'::('a'::('n'::('d'::('l'::('e'::(' '::('r'::('e'::('s'::('u'::('l'::('t'::('i'::('n'::('g'::(' '::('s'::('t'::('r'::('u'::('c'::('t'::('u'::('r'::('e'::[])))))))))))))))))))))))))))))))))))))))))))))))))
       | Typ_all (t0_1, t0_2) ->
         (match a with
          | Avar_b _ -> assert false (* absurd case *)
          | Avar_f a0 ->
            let s = get_for_sure g0 a0 in
            let h6 = get_witness a0 g0 s in
            let ExistT (_, x0) = h6 in
            let h12 = exposure_impl x0 s in
            pbind (pMonad_Monad (Obj.magic monad_either)) (Obj.magic __)
              (err_msg (Obj.magic h12)
                ('s'::('u'::('b'::('t'::('y'::('p'::('e'::('r'::(':'::(' '::('e'::('x'::('p'::('o'::('s'::('u'::('r'::('e'::(' '::('f'::('a'::('i'::('l'::('e'::('d'::[]))))))))))))))))))))))))))
              (fun x1 ->
              match x1 with
              | Typ_bot -> ret (Obj.magic monad_either) __
              | Typ_rcd d ->
                let o = LabelAssocList.get (Label_typ t2) (decs_to_list' d) in
                (match o with
                 | Some d0 ->
                   (match d0 with
                    | Dec_typ (_, _, t3) ->
                      let h24 =
                        subty_dec0 g0 t3 (Typ_all (t0_1, t0_2)) __ __ __ __ __
                      in
                      pbind (pMonad_Monad (Obj.magic monad_either))
                        (Obj.magic __)
                        (err_msg h24
                          ('s'::('u'::('b'::('t'::('y'::('p'::('e'::('r'::(':'::(' '::('r'::('e'::('c'::('u'::('r'::('s'::('i'::('v'::('e'::(' '::('s'::('u'::('b'::('t'::('y'::('p'::('i'::('n'::('g'::(' '::('c'::('a'::('l'::('l'::(' '::('f'::('a'::('i'::('l'::('e'::('d'::('.'::[])))))))))))))))))))))))))))))))))))))))))))
                        (fun _ -> ret (Obj.magic monad_either) __)
                    | Dec_trm (_, _) -> assert false (* absurd case *))
                 | None ->
                   erret
                     ('s'::('u'::('b'::('t'::('y'::('p'::('e'::('r'::(':'::(' '::('a'::(' '::('l'::('a'::('b'::('e'::('l'::(' '::('i'::('s'::(' '::('n'::('o'::('t'::(' '::('f'::('o'::('u'::('n'::('d'::[])))))))))))))))))))))))))))))))
              | _ ->
                erret
                  ('s'::('u'::('b'::('t'::('y'::('p'::('e'::('r'::(':'::(' '::('u'::('n'::('a'::('b'::('l'::('e'::(' '::('t'::('o'::(' '::('h'::('a'::('n'::('d'::('l'::('e'::(' '::('r'::('e'::('s'::('u'::('l'::('t'::('i'::('n'::('g'::(' '::('s'::('t'::('r'::('u'::('c'::('t'::('u'::('r'::('e'::[]))))))))))))))))))))))))))))))))))))))))))))))))
       | Typ_rcd d ->
         (match a with
          | Avar_b _ -> assert false (* absurd case *)
          | Avar_f a0 ->
            let s = get_for_sure g0 a0 in
            let h6 = get_witness a0 g0 s in
            let ExistT (_, x0) = h6 in
            let h12 = exposure_impl x0 s in
            pbind (pMonad_Monad (Obj.magic monad_either)) (Obj.magic __)
              (err_msg (Obj.magic h12)
                ('s'::('u'::('b'::('t'::('y'::('p'::('e'::('r'::(':'::(' '::('e'::('x'::('p'::('o'::('s'::('u'::('r'::('e'::(' '::('f'::('a'::('i'::('l'::('e'::('d'::[]))))))))))))))))))))))))))
              (fun x1 ->
              match x1 with
              | Typ_bot -> ret (Obj.magic monad_either) __
              | Typ_rcd d0 ->
                let o = LabelAssocList.get (Label_typ t2) (decs_to_list' d0)
                in
                (match o with
                 | Some d1 ->
                   (match d1 with
                    | Dec_typ (_, _, t3) ->
                      let h24 = subty_dec0 g0 t3 (Typ_rcd d) __ __ __ __ __ in
                      pbind (pMonad_Monad (Obj.magic monad_either))
                        (Obj.magic __)
                        (err_msg h24
                          ('s'::('u'::('b'::('t'::('y'::('p'::('e'::('r'::(':'::(' '::('r'::('e'::('c'::('u'::('r'::('s'::('i'::('v'::('e'::(' '::('s'::('u'::('b'::('t'::('y'::('p'::('i'::('n'::('g'::(' '::('c'::('a'::('l'::('l'::(' '::('f'::('a'::('i'::('l'::('e'::('d'::('.'::[])))))))))))))))))))))))))))))))))))))))))))
                        (fun _ -> ret (Obj.magic monad_either) __)
                    | Dec_trm (_, _) -> assert false (* absurd case *))
                 | None ->
                   erret
                     ('s'::('u'::('b'::('t'::('y'::('p'::('e'::('r'::(':'::(' '::('a'::(' '::('l'::('a'::('b'::('e'::('l'::(' '::('i'::('s'::(' '::('n'::('o'::('t'::(' '::('f'::('o'::('u'::('n'::('d'::[])))))))))))))))))))))))))))))))
              | _ ->
                erret
                  ('s'::('u'::('b'::('t'::('y'::('p'::('e'::('r'::(':'::(' '::('u'::('n'::('a'::('b'::('l'::('e'::(' '::('t'::('o'::(' '::('h'::('a'::('n'::('d'::('l'::('e'::(' '::('r'::('e'::('s'::('u'::('l'::('t'::('i'::('n'::('g'::(' '::('s'::('t'::('r'::('u'::('c'::('t'::('u'::('r'::('e'::[])))))))))))))))))))))))))))))))))))))))))))))))))
    | Typ_all (t2, t3) ->
      (match u0 with
       | Typ_top -> ret (Obj.magic monad_either) __
       | Typ_sel (a, t4) ->
         (match a with
          | Avar_b _ -> assert false (* absurd case *)
          | Avar_f a0 ->
            let s = get_for_sure g0 a0 in
            let h6 = get_witness a0 g0 s in
            let ExistT (_, x0) = h6 in
            let h12 = exposure_impl x0 s in
            pbind (pMonad_Monad (Obj.magic monad_either)) (Obj.magic __)
              (err_msg (Obj.magic h12)
                ('s'::('u'::('b'::('t'::('y'::('p'::('e'::('r'::(':'::(' '::('e'::('x'::('p'::('o'::('s'::('u'::('r'::('e'::(' '::('f'::('a'::('i'::('l'::('e'::('d'::[]))))))))))))))))))))))))))
              (fun x1 ->
              match x1 with
              | Typ_bot -> ret (Obj.magic monad_either) __
              | Typ_rcd d ->
                let o = LabelAssocList.get (Label_typ t4) (decs_to_list' d) in
                (match o with
                 | Some d0 ->
                   (match d0 with
                    | Dec_typ (_, t5, _) ->
                      let h24 =
                        subty_dec0 g0 (Typ_all (t2, t3)) t5 __ __ __ __ __
                      in
                      pbind (pMonad_Monad (Obj.magic monad_either))
                        (Obj.magic __)
                        (err_msg h24
                          ('s'::('u'::('b'::('t'::('y'::('p'::('e'::('r'::(':'::(' '::('r'::('e'::('c'::('u'::('r'::('s'::('i'::('v'::('e'::(' '::('s'::('u'::('b'::('t'::('y'::('p'::('i'::('n'::('g'::(' '::('c'::('a'::('l'::('l'::(' '::('f'::('a'::('i'::('l'::('e'::('d'::('.'::[])))))))))))))))))))))))))))))))))))))))))))
                        (fun _ -> ret (Obj.magic monad_either) __)
                    | Dec_trm (_, _) -> assert false (* absurd case *))
                 | None ->
                   erret
                     ('s'::('u'::('b'::('t'::('y'::('p'::('e'::('r'::(':'::(' '::('a'::(' '::('l'::('a'::('b'::('e'::('l'::(' '::('i'::('s'::(' '::('n'::('o'::('t'::(' '::('f'::('o'::('u'::('n'::('d'::[])))))))))))))))))))))))))))))))
              | _ ->
                erret
                  ('s'::('u'::('b'::('t'::('y'::('p'::('e'::('r'::(':'::(' '::('u'::('n'::('a'::('b'::('l'::('e'::(' '::('t'::('o'::(' '::('h'::('a'::('n'::('d'::('l'::('e'::(' '::('r'::('e'::('s'::('u'::('l'::('t'::('i'::('n'::('g'::(' '::('s'::('t'::('r'::('u'::('c'::('t'::('u'::('r'::('e'::[]))))))))))))))))))))))))))))))))))))))))))))))))
       | Typ_all (t4, t5) ->
         let s = eq_dec0 eqDecTyp t2 t4 in
         if s
         then let s0 =
                atom_fresh
                  (AtomSetImpl.union (EnvImpl.dom g0)
                    (AtomSetImpl.union (fv_values fv_typ g0)
                      (AtomSetImpl.union (fv_typ t3)
                        (AtomSetImpl.union (fv_typ t4) (fv_typ t5)))))
              in
              let h4 =
                subty_dec0 (app (EnvImpl.one (s0, t4)) g0)
                  (open_rec openTyp 0 s0 t3) (open_rec openTyp 0 s0 t5) __ __
                  __ __ __
              in
              pbind (pMonad_Monad (Obj.magic monad_either)) (Obj.magic __)
                (err_msg h4
                  ('s'::('u'::('b'::('t'::('y'::('p'::('e'::('r'::(':'::(' '::('t'::('h'::('e'::(' '::('b'::('o'::('d'::('y'::(' '::('t'::('y'::('p'::('e'::('s'::(' '::('f'::('a'::('i'::('l'::('e'::('d'::[]))))))))))))))))))))))))))))))))
                (fun _ -> ret (Obj.magic monad_either) __)
         else erret
                ('s'::('u'::('b'::('t'::('y'::('p'::('e'::('r'::(':'::(' '::('t'::('h'::('e'::(' '::('i'::('n'::('p'::('u'::('t'::(' '::('t'::('y'::('p'::('e'::('s'::(' '::('d'::('o'::(' '::('n'::('o'::('t'::(' '::('m'::('a'::('t'::('c'::('h'::[]))))))))))))))))))))))))))))))))))))))
       | _ ->
         erret
           ('s'::('u'::('b'::('t'::('y'::('p'::('e'::('r'::(':'::(' '::('u'::('n'::('k'::('n'::('o'::('w'::('n'::(' '::('h'::('o'::('w'::(' '::('t'::('o'::(' '::('c'::('o'::('m'::('p'::('a'::('r'::('e'::(' '::('t'::('w'::('o'::(' '::('t'::('y'::('p'::('e'::('s'::('.'::[]))))))))))))))))))))))))))))))))))))))))))))
    | Typ_rcd d ->
      (match u0 with
       | Typ_top -> ret (Obj.magic monad_either) __
       | Typ_sel (a, t2) ->
         (match a with
          | Avar_b _ -> assert false (* absurd case *)
          | Avar_f a0 ->
            let s = get_for_sure g0 a0 in
            let h6 = get_witness a0 g0 s in
            let ExistT (_, x0) = h6 in
            let h12 = exposure_impl x0 s in
            pbind (pMonad_Monad (Obj.magic monad_either)) (Obj.magic __)
              (err_msg (Obj.magic h12)
                ('s'::('u'::('b'::('t'::('y'::('p'::('e'::('r'::(':'::(' '::('e'::('x'::('p'::('o'::('s'::('u'::('r'::('e'::(' '::('f'::('a'::('i'::('l'::('e'::('d'::[]))))))))))))))))))))))))))
              (fun x1 ->
              match x1 with
              | Typ_bot -> ret (Obj.magic monad_either) __
              | Typ_rcd d0 ->
                let o = LabelAssocList.get (Label_typ t2) (decs_to_list' d0)
                in
                (match o with
                 | Some d1 ->
                   (match d1 with
                    | Dec_typ (_, t3, _) ->
                      let h24 = subty_dec0 g0 (Typ_rcd d) t3 __ __ __ __ __ in
                      pbind (pMonad_Monad (Obj.magic monad_either))
                        (Obj.magic __)
                        (err_msg h24
                          ('s'::('u'::('b'::('t'::('y'::('p'::('e'::('r'::(':'::(' '::('r'::('e'::('c'::('u'::('r'::('s'::('i'::('v'::('e'::(' '::('s'::('u'::('b'::('t'::('y'::('p'::('i'::('n'::('g'::(' '::('c'::('a'::('l'::('l'::(' '::('f'::('a'::('i'::('l'::('e'::('d'::('.'::[])))))))))))))))))))))))))))))))))))))))))))
                        (fun _ -> ret (Obj.magic monad_either) __)
                    | Dec_trm (_, _) -> assert false (* absurd case *))
                 | None ->
                   erret
                     ('s'::('u'::('b'::('t'::('y'::('p'::('e'::('r'::(':'::(' '::('a'::(' '::('l'::('a'::('b'::('e'::('l'::(' '::('i'::('s'::(' '::('n'::('o'::('t'::(' '::('f'::('o'::('u'::('n'::('d'::[])))))))))))))))))))))))))))))))
              | _ ->
                erret
                  ('s'::('u'::('b'::('t'::('y'::('p'::('e'::('r'::(':'::(' '::('u'::('n'::('a'::('b'::('l'::('e'::(' '::('t'::('o'::(' '::('h'::('a'::('n'::('d'::('l'::('e'::(' '::('r'::('e'::('s'::('u'::('l'::('t'::('i'::('n'::('g'::(' '::('s'::('t'::('r'::('u'::('c'::('t'::('u'::('r'::('e'::[]))))))))))))))))))))))))))))))))))))))))))))))))
       | Typ_rcd d0 ->
         (match d0 with
          | Decs_nil -> ret (Obj.magic monad_either) __
          | Decs_cons (d1, d2) ->
            let h5 = subty_dec0 g0 (Typ_rcd d) (Typ_rcd d2) __ __ __ __ __ in
            (match d1 with
             | Dec_typ (t2, t3, t4) ->
               let o = LabelAssocList.get (Label_typ t2) (decs_to_list' d) in
               (match o with
                | Some d3 ->
                  (match d3 with
                   | Dec_typ (_, t5, t6) ->
                     let h12 = subty_dec0 g0 t3 t5 __ __ __ __ __ in
                     let h13 = subty_dec0 g0 t6 t4 __ __ __ __ __ in
                     pbind (pMonad_Monad (Obj.magic monad_either))
                       (Obj.magic __)
                       (err_msg h12
                         ('s'::('u'::('b'::('t'::('y'::('p'::('e'::('r'::(':'::(' '::('l'::('o'::('w'::('e'::('r'::(' '::('b'::('o'::('u'::('n'::('d'::(' '::('c'::('o'::('m'::('p'::('a'::('r'::('i'::('s'::('o'::('n'::(' '::('f'::('a'::('i'::('l'::('e'::('d'::[]))))))))))))))))))))))))))))))))))))))))
                       (fun _ ->
                       pbind (pMonad_Monad (Obj.magic monad_either))
                         (Obj.magic __)
                         (err_msg h13
                           ('s'::('u'::('b'::('t'::('y'::('p'::('e'::('r'::(':'::(' '::('u'::('p'::('p'::('e'::('r'::(' '::('b'::('o'::('u'::('n'::('d'::(' '::('c'::('o'::('m'::('p'::('a'::('r'::('i'::('s'::('o'::('n'::(' '::('f'::('a'::('i'::('l'::('e'::('d'::[]))))))))))))))))))))))))))))))))))))))))
                         (fun _ ->
                         pbind (pMonad_Monad (Obj.magic monad_either))
                           (Obj.magic __)
                           (err_msg h5
                             ('s'::('u'::('b'::('t'::('y'::('p'::('e'::('r'::(':'::(' '::('r'::('e'::('c'::('u'::('r'::('s'::('i'::('v'::('e'::(' '::('c'::('o'::('m'::('p'::('a'::('r'::('i'::('s'::('o'::('n'::(' '::('f'::('o'::('r'::(' '::('d'::('e'::('c'::('l'::('a'::('r'::('a'::('t'::('i'::('o'::('n'::('s'::(' '::('f'::('a'::('i'::('l'::('e'::('d'::[])))))))))))))))))))))))))))))))))))))))))))))))))))))))
                           (fun _ -> ret (Obj.magic monad_either) __)))
                   | Dec_trm (_, _) -> assert false (* absurd case *))
                | None ->
                  erret
                    ('s'::('u'::('b'::('t'::('y'::('p'::('e'::('r'::(':'::(' '::('a'::(' '::('l'::('a'::('b'::('e'::('l'::(' '::('i'::('s'::(' '::('n'::('o'::('t'::(' '::('f'::('o'::('u'::('n'::('d'::[])))))))))))))))))))))))))))))))
             | Dec_trm (t2, t3) ->
               let o = LabelAssocList.get (Label_trm t2) (decs_to_list' d) in
               (match o with
                | Some d3 ->
                  (match d3 with
                   | Dec_typ (_, _, _) -> assert false (* absurd case *)
                   | Dec_trm (_, t4) ->
                     let h10 = subty_dec0 g0 t4 t3 __ __ __ __ __ in
                     pbind (pMonad_Monad (Obj.magic monad_either))
                       (Obj.magic __)
                       (err_msg h10
                         ('s'::('u'::('b'::('t'::('y'::('p'::('e'::('r'::(':'::(' '::('m'::('e'::('m'::('b'::('e'::('r'::(' '::('t'::('y'::('p'::('e'::(' '::('c'::('o'::('m'::('p'::('a'::('r'::('i'::('s'::('o'::('n'::(' '::('f'::('a'::('i'::('l'::('e'::('d'::[]))))))))))))))))))))))))))))))))))))))))
                       (fun _ ->
                       pbind (pMonad_Monad (Obj.magic monad_either))
                         (Obj.magic __)
                         (err_msg h5
                           ('s'::('u'::('b'::('t'::('y'::('p'::('e'::('r'::(':'::(' '::('r'::('e'::('c'::('u'::('r'::('s'::('i'::('v'::('e'::(' '::('c'::('o'::('m'::('p'::('a'::('r'::('i'::('s'::('o'::('n'::(' '::('f'::('o'::('r'::(' '::('d'::('e'::('c'::('l'::('a'::('r'::('a'::('t'::('i'::('o'::('n'::('s'::(' '::('f'::('a'::('i'::('l'::('e'::('d'::[])))))))))))))))))))))))))))))))))))))))))))))))))))))))
                         (fun _ -> ret (Obj.magic monad_either) __)))
                | None ->
                  erret
                    ('s'::('u'::('b'::('t'::('y'::('p'::('e'::('r'::(':'::(' '::('a'::(' '::('l'::('a'::('b'::('e'::('l'::(' '::('i'::('s'::(' '::('n'::('o'::('t'::(' '::('f'::('o'::('u'::('n'::('d'::[])))))))))))))))))))))))))))))))))
       | _ ->
         erret
           ('s'::('u'::('b'::('t'::('y'::('p'::('e'::('r'::(':'::(' '::('u'::('n'::('k'::('n'::('o'::('w'::('n'::(' '::('h'::('o'::('w'::(' '::('t'::('o'::(' '::('c'::('o'::('m'::('p'::('a'::('r'::('e'::(' '::('t'::('w'::('o'::(' '::('t'::('y'::('p'::('e'::('s'::('.'::[])))))))))))))))))))))))))))))))))))))))))))))
  in fix_F_sub (ExistT (g, (ExistT (t0, u)))) __ __ __ __ __

(** val typing_impl_template :
    subtyp_checker -> (Atom.atom * typ) list -> trm -> (char list list, typ)
    sum **)

let typing_impl_template subtyper g t0 =
  let rec fix_F_sub x =
    let subtyper0 = projT1 x in
    let g0 = projT1 (projT2 x) in
    let t1 = projT2 (projT2 x) in
    let typing_impl_template0 = fun subtyper1 g1 t2 ->
      fix_F_sub (ExistT (subtyper1, (ExistT (g1, t2))))
    in
    (fun _ _ _ _ ->
    let typing_impl = fun g1 t2 -> typing_impl_template0 subtyper0 g1 t2 in
    (match t1 with
     | Trm_var a ->
       (match a with
        | Avar_b _ -> assert false (* absurd case *)
        | Avar_f a0 ->
          let s = get_for_sure g0 a0 in ret (Obj.magic monad_either) s)
     | Trm_val v ->
       (match v with
        | Val_rcd d ->
          let h3 = fun h3 ->
            let rec f = function
            | Defs_nil -> ret monad_either Decs_nil
            | Defs_cons (d1, d2) ->
              let h8 = f d2 in
              (match d1 with
               | Def_typ (t2, t3) ->
                 pbind (pMonad_Monad monad_either) (Obj.magic __)
                   (err_msg h8
                     ('t'::('y'::('p'::('e'::('r'::(':'::(' '::('r'::('e'::('c'::('u'::('r'::('s'::('i'::('v'::('e'::(' '::('c'::('a'::('l'::('l'::(' '::('f'::('o'::('r'::(' '::('a'::(' '::('r'::('e'::('c'::('o'::('r'::('d'::(' '::('f'::('a'::('i'::('l'::('e'::('d'::[]))))))))))))))))))))))))))))))))))))))))))
                   (fun x0 ->
                   ret monad_either (Decs_cons ((Dec_typ (t2, t3, t3)), x0)))
               | Def_trm (t2, t3) ->
                 let h9 = typing_impl g0 t3 __ __ __ __ in
                 pbind (pMonad_Monad monad_either) (Obj.magic __)
                   (err_msg (Obj.magic h9)
                     ('t'::('y'::('p'::('e'::('r'::(':'::(' '::('f'::('a'::('i'::('l'::('e'::('d'::(' '::('t'::('o'::(' '::('t'::('y'::('p'::('e'::(' '::('c'::('h'::('e'::('c'::('k'::(' '::('a'::(' '::('d'::('a'::('t'::('a'::(' '::('m'::('e'::('m'::('b'::('e'::('r'::[]))))))))))))))))))))))))))))))))))))))))))
                   (fun x0 ->
                   pbind (pMonad_Monad monad_either) (Obj.magic __)
                     (err_msg h8
                       ('t'::('y'::('p'::('e'::('r'::(':'::(' '::('r'::('e'::('c'::('u'::('r'::('s'::('i'::('v'::('e'::(' '::('c'::('a'::('l'::('l'::(' '::('f'::('o'::('r'::(' '::('a'::(' '::('r'::('e'::('c'::('o'::('r'::('d'::(' '::('f'::('a'::('i'::('l'::('e'::('d'::[]))))))))))))))))))))))))))))))))))))))))))
                     (fun x1 ->
                     ret monad_either (Decs_cons ((Dec_trm (t2, x0)), x1)))))
            in f h3
          in
          let h5 = h3 d in
          pbind (pMonad_Monad (Obj.magic monad_either)) (Obj.magic __)
            (err_msg (Obj.magic h5)
              ('t'::('y'::('p'::('e'::('r'::(':'::(' '::('r'::('e'::('c'::('o'::('r'::('d'::(' '::('f'::('a'::('i'::('l'::('e'::('d'::[])))))))))))))))))))))
            (fun x0 -> ret (Obj.magic monad_either) (Typ_rcd x0))
        | Val_lam (t2, t3) ->
          let s =
            atom_fresh
              (AtomSetImpl.union (EnvImpl.dom g0)
                (AtomSetImpl.union (fv_values fv_typ g0)
                  (AtomSetImpl.union (fv_typ t2) (fv_trm t3))))
          in
          let h3 =
            typing_impl (app (EnvImpl.one (s, t2)) g0)
              (open_rec openTrm 0 s t3) __ __ __ __
          in
          pbind (pMonad_Monad (Obj.magic monad_either)) (Obj.magic __)
            (err_msg h3
              ('t'::('y'::('p'::('e'::('r'::(':'::(' '::('f'::('u'::('n'::('c'::('t'::('i'::('o'::('n'::(' '::('b'::('o'::('d'::('y'::(' '::('f'::('a'::('i'::('l'::('e'::('d'::('.'::[])))))))))))))))))))))))))))))
            (fun x0 ->
            ret (Obj.magic monad_either) (Typ_all (t2,
              (close_rec closeTyp 0 s x0)))))
     | Trm_sel (a, t2) ->
       (match a with
        | Avar_b _ -> assert false (* absurd case *)
        | Avar_f a0 ->
          let s = get_for_sure g0 a0 in
          let h5 = get_witness a0 g0 s in
          let ExistT (_, x0) = h5 in
          let h11 = exposure_impl x0 s in
          pbind (pMonad_Monad (Obj.magic monad_either)) (Obj.magic __)
            (err_msg h11
              ('t'::('y'::('p'::('e'::('r'::(':'::(' '::('e'::('x'::('p'::('o'::('s'::('u'::('r'::('e'::(' '::('f'::('a'::('i'::('l'::('e'::('d'::(' '::('f'::('o'::('r'::(' '::('m'::('e'::('m'::('b'::('e'::('r'::(' '::('s'::('e'::('l'::('e'::('c'::('t'::('i'::('o'::('n'::[]))))))))))))))))))))))))))))))))))))))))))))
            (fun x1 ->
            match x1 with
            | Typ_bot -> ret (Obj.magic monad_either) Typ_bot
            | Typ_rcd d ->
              let o = LabelAssocList.get (Label_trm t2) (decs_to_list' d) in
              (match o with
               | Some d0 ->
                 (match d0 with
                  | Dec_typ (_, _, _) -> assert false (* absurd case *)
                  | Dec_trm (_, t3) -> ret (Obj.magic monad_either) t3)
               | None ->
                 erret
                   ('t'::('y'::('p'::('e'::('r'::(':'::(' '::('a'::(' '::('l'::('a'::('b'::('e'::('l'::(' '::('i'::('s'::(' '::('n'::('o'::('t'::(' '::('f'::('o'::('u'::('n'::('d'::[]))))))))))))))))))))))))))))
            | _ ->
              erret
                ('t'::('y'::('p'::('e'::('r'::(':'::(' '::('t'::('r'::('i'::('e'::('d'::(' '::('t'::('o'::(' '::('l'::('o'::('o'::('k'::('u'::('p'::(' '::('n'::('o'::('n'::('-'::('r'::('e'::('c'::('o'::('r'::('d'::(' '::('t'::('y'::('p'::('e'::[]))))))))))))))))))))))))))))))))))))))))
     | Trm_app (a, a0) ->
       (match a with
        | Avar_b _ -> assert false (* absurd case *)
        | Avar_f a1 ->
          (match a0 with
           | Avar_b _ -> assert false (* absurd case *)
           | Avar_f a2 ->
             let s = get_for_sure g0 a1 in
             let h5 = get_witness a1 g0 s in
             let ExistT (_, x0) = h5 in
             let h11 = exposure_impl x0 s in
             pbind (pMonad_Monad (Obj.magic monad_either)) (Obj.magic __)
               (err_msg h11
                 ('t'::('y'::('p'::('e'::('r'::(':'::(' '::('e'::('x'::('p'::('o'::('s'::('u'::('r'::('e'::(' '::('f'::('a'::('i'::('l'::('e'::('d'::(' '::('i'::('n'::(' '::('a'::('p'::('p'::('l'::('i'::('c'::('a'::('t'::('i'::('o'::('n'::(' '::('c'::('a'::('s'::('e'::[])))))))))))))))))))))))))))))))))))))))))))
               (fun x1 ->
               let s0 = get_for_sure g0 a2 in
               (match x1 with
                | Typ_bot ->
                  ret (Obj.magic monad_either) (open_rec openTyp 0 a2 Typ_bot)
                | Typ_all (t2, t3) ->
                  let h26 = subtyper0 g0 s0 t2 __ __ __ __ __ in
                  pbind (pMonad_Monad (Obj.magic monad_either))
                    (Obj.magic __)
                    (err_msg (Obj.magic h26)
                      ('t'::('y'::('p'::('e'::('r'::(':'::(' '::('i'::('n'::('p'::('u'::('t'::(' '::('s'::('u'::('b'::('t'::('y'::('p'::('i'::('n'::('g'::(' '::('f'::('a'::('i'::('l'::('e'::('d'::(' '::('i'::('n'::(' '::('a'::('p'::('p'::('l'::('i'::('c'::('a'::('t'::('i'::('o'::('n'::(' '::('c'::('a'::('s'::('e'::[]))))))))))))))))))))))))))))))))))))))))))))))))))
                    (fun _ ->
                    ret (Obj.magic monad_either) (open_rec openTyp 0 a2 t3))
                | _ ->
                  erret
                    ('t'::('y'::('p'::('e'::('r'::(':'::(' '::('n'::('o'::('t'::(' '::('a'::(' '::('f'::('u'::('n'::('c'::('t'::('i'::('o'::('n'::(' '::('i'::('n'::(' '::('a'::('p'::('p'::('l'::('i'::('c'::('a'::('t'::('i'::('o'::('n'::(' '::('c'::('a'::('s'::('e'::[])))))))))))))))))))))))))))))))))))))))))))))
     | Trm_let (t2, t3) ->
       let h3 = typing_impl g0 t2 __ __ __ __ in
       pbind (pMonad_Monad (Obj.magic monad_either)) (Obj.magic __)
         (err_msg h3
           ('t'::('y'::('p'::('e'::('r'::(':'::(' '::('l'::('e'::('t'::(' '::('b'::('i'::('n'::('d'::('i'::('n'::('g'::(' '::('t'::('y'::('p'::('e'::(' '::('f'::('a'::('i'::('l'::('e'::('d'::[])))))))))))))))))))))))))))))))
         (fun x0 ->
         let s =
           atom_fresh
             (AtomSetImpl.union (EnvImpl.dom g0)
               (AtomSetImpl.union (fv_values fv_typ g0)
                 (AtomSetImpl.union (fv_typ x0)
                   (AtomSetImpl.union (fv_trm t2) (fv_trm t3)))))
         in
         let h7 =
           typing_impl (app (EnvImpl.one (s, x0)) g0)
             (open_rec openTrm 0 s t3) __ __ __ __
         in
         pbind (pMonad_Monad (Obj.magic monad_either)) (Obj.magic __)
           (err_msg h7
             ('t'::('y'::('p'::('e'::('r'::(':'::(' '::('l'::('e'::('t'::(' '::('b'::('o'::('d'::('y'::(' '::('t'::('y'::('p'::('e'::(' '::('f'::('a'::('i'::('l'::('e'::('d'::[]))))))))))))))))))))))))))))
           (fun x1 ->
           let h11 = promotion (app (EnvImpl.one (s, x0)) g0) s x1 in
           pbind (pMonad_Monad (Obj.magic monad_either)) (Obj.magic __)
             (err_msg h11
               ('t'::('y'::('p'::('e'::('r'::(':'::(' '::('p'::('r'::('o'::('m'::('o'::('t'::('e'::(' '::('b'::('o'::('d'::('y'::(' '::('t'::('y'::('p'::('e'::(' '::('f'::('a'::('i'::('l'::('e'::('d'::[]))))))))))))))))))))))))))))))))
             (fun x2 -> ret (Obj.magic monad_either) x2)))))
  in fix_F_sub (ExistT (subtyper, (ExistT (g, t0)))) __ __ __ __

(** val typecheck_template' :
    subtyp_checker -> trm -> (char list list, typ) sum **)

let typecheck_template' subtyper t0 =
  typing_impl_template subtyper [] t0

(** val typecheck_template :
    subtyp_checker -> trm -> (char list list, typ) sum **)

let typecheck_template subtyper t0 =
  if check_fv_trm t0
  then if check_lc_at_trm 0 t0
       then if uniq_dom_trm_dec t0
            then typecheck_template' subtyper t0
            else erret
                   ('s'::('a'::('n'::('i'::('t'::('y'::(':'::(' '::('i'::('n'::('p'::('u'::('t'::(' '::('t'::('e'::('r'::('m'::(' '::('d'::('o'::('e'::('s'::(' '::('n'::('o'::('t'::(' '::('h'::('a'::('v'::('e'::(' '::('u'::('n'::('i'::('q'::('u'::('e'::(' '::('d'::('o'::('m'::('a'::('i'::('n'::('.'::[])))))))))))))))))))))))))))))))))))))))))))))))
       else erret
              ('s'::('a'::('n'::('i'::('t'::('y'::(':'::(' '::('i'::('n'::('p'::('u'::('t'::(' '::('t'::('e'::('r'::('m'::(' '::('i'::('s'::(' '::('n'::('o'::('t'::(' '::('l'::('o'::('c'::('a'::('l'::('l'::('y'::(' '::('c'::('l'::('o'::('s'::('e'::('d'::('.'::[])))))))))))))))))))))))))))))))))))))))))
  else erret
         ('s'::('a'::('n'::('i'::('t'::('y'::(':'::(' '::('i'::('n'::('p'::('u'::('t'::(' '::('t'::('e'::('r'::('m'::(' '::('i'::('s'::(' '::('n'::('o'::('t'::(' '::('c'::('l'::('o'::('s'::('e'::('d'::('.'::[])))))))))))))))))))))))))))))))))

(** val typecheck : trm -> (char list list, typ) sum **)

let typecheck t0 =
  typecheck_template (fun x x0 x1 _ _ _ _ _ -> subty_dec x x0 x1) t0
