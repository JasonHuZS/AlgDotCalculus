Require Import Definitions TightTyping.
Require Import InertTypes.

Reserved Notation "G '⊢##' x '⦂' T" (at level 70, T at level 79).

Inductive ty_var_inv : env -> atom -> typ -> Prop :=
| ty_v_inv : forall G x T, binds x T G -> G ⊢## x ⦂ T
| ty_obj_drop1_inv : forall G x (DS1 DS2 : decs),
    not_empty DS1 ->
    G ⊢## x ⦂ {< append' DS1 DS2 >} ->
    G ⊢## x ⦂ {< DS2 >}
| ty_obj_drop2_inv : forall G x (DS1 DS2 : decs),
    not_empty DS2 ->
    G ⊢## x ⦂ {< append' DS1 DS2 >} ->
    G ⊢## x ⦂ {< DS1 >}
| ty_obj_merge_inv : forall G x (DS1 DS2 : decs),
    G ⊢## x ⦂ {< DS1 >} ->
    G ⊢## x ⦂ {< DS2 >} ->
    G ⊢## x ⦂ {< append' DS1 DS2 >}

| ty_obj_fld_inv : forall G x a T U,
    G ⊢# T <⦂ U ->
    G ⊢## x ⦂ {< decs_cons (dec_trm a T) decs_nil >} ->
    G ⊢## x ⦂ {< decs_cons (dec_trm a U) decs_nil >}
| ty_obj_typ_inv : forall G (x : atom) A S1 T1 S2 T2,
    G ⊢## x ⦂ {< decs_cons (dec_typ A S1 T1) decs_nil >} ->
    G ⊢# S2 <⦂ S1 ->
    G ⊢# T1 <⦂ T2 ->
    G ⊢## x ⦂ {< decs_cons (dec_typ A S2 T2) decs_nil >}

| ty_obj_sel_inv : forall G x y S A DS,
    G ⊢## x ⦂ S ->
    binds y ({< DS >}) G ->
    lbinds (label_typ A) (dec_typ A S S) DS ->
    G ⊢## x ⦂ y ⋅ A
| ty_all_inv: forall L G x S1 T1 S2 T2,
    G ⊢## x ⦂ all(S1) T1 ->
    G ⊢# S2 <⦂ S1 ->
    (forall x, x `notin` L ->
       x ~ S2 ++ G ⊢ open x T1 <⦂ open x T2) ->
    G ⊢## x ⦂ all(S2) T2
| ty_top_inv : forall G x T,
    G ⊢## x ⦂ T ->
    G ⊢## x ⦂ ⊤
where "G ⊢## x ⦂ T" := (ty_var_inv G x T).
Hint Constructors ty_var_inv.


Reserved Notation "G '⊢##v' x '⦂' T" (at level 70, T at level 79).
Inductive ty_val_inv : env -> val -> typ -> Prop :=
| ty_objv_v : forall G ds DS,
    G ⊩[ ds ⦂ DS ] ->
    wf_defs ds ->
    G ⊢##v {[ ds ]} ⦂ {< DS >}
| ty_allv_v : forall L G T t U,    
    (forall x, x `notin` L ->
          x ~ T ++ G ⊢ open x t ⦂ open x U) ->
    G ⊢##v λ( T ){ t } ⦂ all( T ) U
| ty_all_inv_v: forall L G v S1 T1 S2 T2,
    G ⊢##v v ⦂ all(S1) T1 ->
    G ⊢# S2 <⦂ S1 ->
    (forall x, x `notin` L ->
       x ~ S2 ++ G ⊢ open x T1 <⦂ open x T2) ->
    G ⊢##v v ⦂ all(S2) T2

| ty_obj_fld_inv_v : forall G v a T U,
    G ⊢##v v ⦂ {< decs_cons (dec_trm a T) decs_nil >} ->
    G ⊢# T <⦂ U ->
    G ⊢##v v ⦂ {< decs_cons (dec_trm a U) decs_nil >}
| ty_obj_typ_inv_v : forall G v A S1 T1 S2 T2,
    G ⊢##v v ⦂ {< decs_cons (dec_typ A S1 T1) decs_nil >} ->
    G ⊢# S2 <⦂ S1 ->
    G ⊢# T1 <⦂ T2 ->
    G ⊢##v v ⦂ {< decs_cons (dec_typ A S2 T2) decs_nil >}
      
| ty_obj_sel_inv_v : forall G v y DS A T,
    G ⊢##v v ⦂ T ->
    binds y ({< DS >}) G ->
    lbinds (label_typ A) (dec_typ A T T) DS ->
    G ⊢##v v ⦂ y ⋅ A 

| ty_obj_drop1_inv_v : forall G v (DS1 DS2 : decs),
    not_empty DS1 ->
    G ⊢##v v ⦂ {< append' DS1 DS2 >} ->
    G ⊢##v v ⦂ {< DS2 >}
| ty_obj_drop2_inv_v : forall G v (DS1 DS2 : decs),
    not_empty DS2 ->
    G ⊢##v v ⦂ {< append' DS1 DS2 >} ->
    G ⊢##v v ⦂ {< DS1 >}
| ty_obj_merge_inv_v : forall G v (DS1 DS2 : decs),
    G ⊢##v v ⦂ {< DS1 >} ->
    G ⊢##v v ⦂ {< DS2 >} ->
    G ⊢##v v ⦂ {< append' DS1 DS2 >}

| ty_top_inv_v : forall G v T,
    G ⊢##v v ⦂ T ->
    G ⊢##v v ⦂ ⊤
where "G ⊢##v v ⦂ T" := (ty_val_inv G v T).
Hint Constructors ty_val_inv.


Tactic Notation "inv" "typing" "undec" "1" :=
  match goal with
  | [H : _ ⊢## _ ⦂ _ ⋅ _ |- _ ] => invert H
  | _ => idtac
  end.


Section InvertibleTyping.

  Lemma Forall_binds : forall (G : env) x T P,
    binds x T G ->
    Forall P G ->
    P (x, T).
  Proof.
    induction G; routine.
    apply IHG; assumption.
  Qed.
  
  Lemma invertible_typing_closure_tight: forall G x T U,
    inert_env G ->
    G ⊢## x ⦂ T ->
    G ⊢# T <⦂ U ->
    G ⊢## x ⦂ U.
  Proof using.
    induction on subtypt;
      inv typing undec 1;
      eprove from inert at 6.
  Qed.
  Hint Resolve invertible_typing_closure_tight.

  Lemma tight_to_invertible : forall G (x : var) U,
      inert_env G ->
      G ⊢# trm_var x ⦂ U ->
      G ⊢## x ⦂ U.
  Proof using. dep induction on tyt_trm; eauto. Qed.

  Lemma invertible_typing_closure_tight_v: forall G v T U,
      inert_env G ->
      G ⊢##v v ⦂ T ->
      G ⊢# T <⦂ U ->
      G ⊢##v v ⦂ U.
  Proof using.
    induction on subtypt; eprove from inert at 6.
  Qed.
  Hint Resolve invertible_typing_closure_tight_v.

  Lemma tight_to_invertible_v : forall G v T,
      inert_env G ->
      G ⊢# trm_val v ⦂ T ->
      G ⊢##v v ⦂ T.
  Proof using. dep induction on tyt_trm; eauto. Qed.

  Local Ltac destr_ind H :=
    tidy_up; edestruct H; try reflexivity;
    [ solve_lbinds
    | destruct_conjs;
      repeat eexists; eroutine].
    
  
  Lemma invertible_find_dec : forall G x (a : trm_label) T DS,
      G ⊢## x ⦂ {< DS >} ->
      lbinds a (dec_trm a T) DS ->
      exists T' DS',
        binds x (typ_rcd DS') G /\
        lbinds a (dec_trm a T') DS' /\
        G ⊢# T' <⦂ T.
  Proof using.
    intros. gen T H.
    dep induction on ty_var_inv; intros.
    par:try destr_ind IHty_var_inv.
    - repeat eexists; eroutine.
    - list_reasoning.
      apply LabelAssocList.binds_app_1 in H1. simpl in H1.
      destruct H1.
      + destr_ind IHty_var_inv1.
      + destr_ind IHty_var_inv2.
    - tidy_up.
  Qed.

  Lemma invertible_one_dec : forall G x (a : trm_label) T,
      G ⊢## x ⦂ {< decs_cons (dec_trm a T) decs_nil >} ->
      exists T' DS',
        binds x (typ_rcd DS') G /\
        lbinds a (dec_trm a T') DS' /\
        G ⊢# T' <⦂ T.
  Proof.
    intros. eapply invertible_find_dec; eroutine.
    routine.
  Qed.

  Lemma invertible_val_rcd : forall G v DS,
      G ⊢##v v ⦂ typ_rcd DS ->
      exists ds, v = val_rcd ds.
  Proof. dep induction on ty_val_inv; eauto. Qed.
  
  (* Following lemma is quite a bit difficult to deal with,
   * and it seems to expose a different style of undecidability,
   * which requires more customization. 
   *)
  
  Ltac boom :=
    destruct_lbinds;
    try match goal with
        | [H : _ |- _ ] =>
          guess_is_ind_hyp H;
          eapply H; eauto;
          solve_lbinds
        end.

  Lemma inert_intuitive_subtyping : forall G x DS A,
      inert_env G ->
      G ⊢## x ⦂ {< DS >} ->
      forall S U,
      lbinds (label_typ A) (dec_typ A S U) DS ->
      G ⊢# S <⦂ x ⋅ A /\ G ⊢# x ⋅ A <⦂ U.
  Proof using.
    dep induction on ty_var_inv; intros;
    match goal with
      | [ _ : binds _ ({< ?DS >}) ?G
        , _ : inert_env ?G
        , _ : lbinds _ _ (_ ?DS) |- _] =>
        (* this captures the base case *)
        eprove from inert
      | _ => boom
    end.
    all:match goal with
        | [H : lbinds _ _ (to_list' (decs_cons _ decs_nil)) |- _] =>
          cbn in H; progressive_destructions
        end.
    split; econstructor; try eassumption; boom.
  Qed.

End InvertibleTyping.
Local Hint Resolve tight_to_invertible.

Lemma general_to_tight :
  forall G0, inert_env G0 ->
        (forall G t T, G ⊢ t ⦂ T ->
                  G = G0 ->
                  G ⊢# t ⦂ T) /\
        (forall G S U, G ⊢ S <⦂ U ->
                  G = G0 ->
                  G ⊢# S <⦂ U).
Proof using.
  mutual induction; eroutine;
    eapply inert_intuitive_subtyping; eauto;
      solve_lbinds.
Qed.

Lemma general_to_tight_typ : forall G t T,
    inert_env G ->
    G ⊢ t ⦂ T ->
    G ⊢# t ⦂ T.
Proof.
  intros. eapply general_to_tight; try eassumption.
  trivial.
Qed.

Lemma general_to_tight_subtyp : forall G S T,
    inert_env G ->
    G ⊢ S <⦂ T ->
    G ⊢# S <⦂ T.
Proof.
  intros. eapply general_to_tight; try eassumption.
  trivial.
Qed.