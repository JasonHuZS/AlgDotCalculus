%{

Require Import String.

Inductive ptyp : Set :=
| ptyp_top : ptyp
| ptyp_bot : ptyp
| ptyp_sel : string -> nat -> ptyp
| ptyp_all : string -> ptyp -> ptyp -> ptyp
| ptyp_rcd : list pdec -> ptyp
with
pdec : Set :=
| pdec_typ : nat -> ptyp -> ptyp -> pdec
| pdec_trm : nat -> ptyp -> pdec.

Inductive ptrm : Set :=
| ptrm_var : string -> ptrm
| ptrm_val : pval -> ptrm
| ptrm_sel : string -> nat -> ptrm
| ptrm_app : string -> string -> ptrm
| ptrm_let : string -> ptrm -> ptrm -> ptrm
with
pval : Set :=
| pval_rcd : list pdef -> pval
| pval_lam : string -> ptyp -> ptrm -> pval
with
pdef : Set :=
| pdef_typ : nat -> ptyp -> pdef
| pdef_trm : nat -> ptrm -> pdef.

%}

%token EOF
%token<string> VAR
%token<nat> MEM

%token DOT COLON SEMICOL
%token TOP BOT ALL LPAREN RPAREN
%token LBRACE RBRACE

%token LET IN EQ LAM
%token TYPE DEF

%type<ptyp> typ_expr
%type<pdec> dec_expr
%type<list pdec> list_of(dec_expr)

%type<ptrm> trm_expr
%type<pval> val_expr
%type<pdef> def_expr
%type<list pdef> list_of(def_expr)

%start<ptrm> beg_expr
%%

beg_expr:
  | t = trm_expr EOF
    { t }

typ_expr:
  | TOP
    { ptyp_top }
  | BOT
    { ptyp_bot }
  | v = VAR n = MEM
    { ptyp_sel v n }
  | ALL LPAREN v = VAR COLON t1 = typ_expr RPAREN DOT t2 = typ_expr
    { ptyp_all v t1 t2 }
  | LBRACE ds = list_of(dec_expr) RBRACE
    { ptyp_rcd nil }

dec_expr:
  | n = MEM COLON t1 = typ_expr DOT DOT t2 = typ_expr
    { pdec_typ n t1 t2 }
  | n = MEM COLON t = typ_expr
    { pdec_trm n t }

list_of(ex):
  | { nil }
  | d = ex SEMICOL ds = list_of(ex)
    { cons d ds }

trm_expr:
  | v = VAR
    { ptrm_var v }
  | vl = val_expr
    { ptrm_val vl }
  | v = VAR DOT n = MEM
    { ptrm_sel v n }
  | v1 = VAR v2 = VAR
    { ptrm_app v1 v2 }
  | LET v = VAR EQ t1 = trm_expr IN t2 = trm_expr
    { ptrm_let v t1 t2 }

val_expr:
  | LBRACE df = list_of(def_expr) RBRACE
    { pval_rcd df }
  | LAM LPAREN v = VAR COLON t1 = typ_expr RPAREN LBRACE t2 = trm_expr RBRACE
    { pval_lam v t1 t2 }

def_expr:
  | TYPE n = MEM EQ t = typ_expr
    { pdef_typ n t }
  | DEF n = MEM EQ t = trm_expr
    { pdef_trm n t }
