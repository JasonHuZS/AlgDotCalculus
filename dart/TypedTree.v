Require Import Definitions.
Require Import Weakening.
Require Import Substitution.
Require Import SubstitutionLaws.
Require Import OperationProperties.
Require Import Narrowing.

Reserved Notation "G '⊢D' t '⦂' T" (at level 70, t at level 79).

Inductive ty_trm_d : env -> trm -> typ -> Type :=
| ty_d_var : forall G x T,
    binds x T G ->
    G ⊢D trm_var x ⦂ T

| ty_d_all_intro : forall L G T t U,
    (forall x, x `notin` L ->
          x ~ T ++ G ⊢D open x t ⦂ open x U) ->
    G ⊢D trm_val (λ( T ){ t }) ⦂ all( T ) U
| ty_d_all_elim : forall G (x z : atom) S T,
    G ⊢D trm_var x ⦂ all( S ) T ->
    G ⊢D trm_var z ⦂ S ->
    G ⊢D (trm_app x z) ⦂ open z T

| ty_d_rcd_intro : forall G ds DS,
    G ⊩[ ds ⦂ DS ] ->
    wf_defs ds ->
    G ⊢D trm_val ({[ ds ]}) ⦂ {< DS >}
| typ_rcd_elim : forall G (x : var) a T,
    G ⊢D trm_var x ⦂ {< decs_cons (dec_trm a T) decs_nil >} ->
    G ⊢D trm_sel x a ⦂ T

| ty_d_let : forall L G t u T U,
    G ⊢D t ⦂ T ->
    (forall x, x `notin` L ->
          x ~ T ++ G ⊢D open x u ⦂ U) ->
    G ⊢D lett t inn u ⦂ U
| ty_d_sub : forall G t T T',
    G ⊢D t ⦂ T ->
    G ⊢ T <⦂ T' ->
    G ⊢D t ⦂ T'
where "G ⊢D t ⦂ T" := (ty_trm_d G t T) : type_scope.
Local Hint Constructors ty_trm_d.

Lemma ty_trm_d_compat : forall G t T,
    G ⊢D t ⦂ T -> G ⊢ t ⦂ T.
Proof. induction on ty_trm_d; eroutine. Qed.

Definition weaken_trm_d_gen: forall G1 G2 G3 t T,
    G1 ++ G3 ⊢D t ⦂ T ->
    G1 ++ G2 ++ G3 ⊢D t ⦂ T.
Proof.
  intros. gen G2. dependent induction X; eroutine.
  - econstructor. cofinite.
    reassoc 4 with 2. eapply X; routine.
  - constructor; trivial. apply weaken_defs_gen. trivial.
  - econstructor; eroutine.
    reassoc 4 with 2. eapply X0; routine.
  - econstructor; eroutine.
    apply weaken_subtyp_gen. trivial.
Defined.

Definition weaken_trm_d : forall G G' t T,
    G ⊢D t ⦂ T ->
    G' ++ G ⊢D t ⦂ T.
Proof.
  intros; reassoc 2 with 0; eapply weaken_trm_d_gen; try eassumption; trivial.
Defined.

Definition get_app : forall {T} {l1 l2 : list (var * T)} {x r},
    uniq (l1 ++ l2) ->
    get x (l1 ++ l2) = Some r ->
    {get x l1 = Some r /\ x `notin` dom l2} + {get x l2 = Some r /\ x `notin` dom l1}.
Proof.
  induction l1; routine.
  - left. split; routine.
  - edestruct IHl1; eroutine.
Defined.

Ltac destr_get_app :=
  repeat match goal with
         | H : get _ (_ ++ _) = _ |- _ =>
           apply get_app in H; auto; destruct H; destruct_conjs
         end.

Section Substitution.

  Definition subst_trm_var_d : forall G1 G2 x0 y x T X,
    binds x0 T (G2 ++ x ~ X ++ G1) ->
    uniq (G2 ++ x ~ X ++ G1) ->
    x `notin` fv G1 ->
    substi x y G2 ++ G1 ⊢D trm_var (avar_f y) ⦂ substi x y X ->
    substi x y G2 ++ G1 ⊢D trm_var (avar_f $ subst_fvar x y x0) ⦂ substi x y T.
  Proof.
    intros. pose proof (binds_to_get _ _ H H0).
    destr_get_app; unfold subst_fvar.
    - destruct_eq.
      exfalso. routine.
      constructor. apply binds_app_2.
      apply binds_map_2.
      apply get_to_binds. trivial.
    - simpl in H2. destruct_eq.
      tidy_up. trivial.
    - destruct_eq. exfalso. routine.
      exrewrite subst_fresh_typ.
      + apply ty_d_var.
        destruct_binds_hyp_uniq H; tidy_up; routine.
        apply binds_In in BindsTac. intuition.
      + eapply free_all; try eassumption.
        apply get_to_binds. eassumption.
    - solve_uniq.
  Defined.

  Ltac xform_ctx :=
    match goal with
    | |- context[?v ~ substi ?x ?y ?T ++ substi ?x ?y ?G] =>
      change (v ~ substi x y T ++ substi x y G)
        with (substi x y (v ~ T ++ G))
    | |- context[?v ~ ({< substi ?x ?y ?T >}) ++ substi ?x ?y ?G] =>
      change (v ~ ({< substi x y T >}) ++ substi x y G)
        with (substi x y (v ~ ({< T >}) ++ G))
    end.
  
  Ltac solve_by_weaken H :=
    cofinite; fold_substi;
    open_substi_transform;
    reassoc 3 with 2; xform_ctx;
    eapply H; routine;
    simpl_env;
    apply weaken_trm_d; trivial.

  Ltac apply_ind H := solve [eapply H; routine].

  Ltac fold_subst_fvar :=
    repeat match goal with
           | |- context[if ?x == ?y then ?z else ?x] =>
             change (if x == y then z else x) with (subst_fvar y z x)
           end.
  
  Definition typing_subst_d_gen : forall x X t T G1 G2 y,
      G2 ++ x ~ X ++ G1 ⊢D t ⦂ T ->
      uniq (G2 ++ x ~ X ++ G1) ->
      x `notin` fv G1 ->
      substi x y G2 ++ G1 ⊢D trm_var (avar_f y) ⦂ substi x y X ->
      substi x y G2 ++ G1 ⊢D substi x y t ⦂ substi x y T.
  Proof.
    intros. gen y. dependent induction X0; intros.
    - eapply subst_trm_var_d; eroutine.
    - simpl. econstructor.
      solve_by_weaken X0.
    - simpl. fold_substi. fold_open_rec. fold_subst_fvar.
      rewrite subst_open_comm_typ. econstructor.
      + apply_ind IHX0_1.
      + apply_ind IHX0_2.
    - simpl; econstructor.
      + fold_substi. eapply subst_rules.
        all:eroutine.
        apply ty_trm_d_compat. trivial.
      + apply substi_resp_luniq. trivial.
    - constructor. apply_ind IHX0.
    - simpl; econstructor.
      + fold_substi. apply_ind IHX0.
      + solve_by_weaken X1.
    - eapply ty_d_sub.
      + apply_ind IHX0.
      + eapply subst_rules.
        all:eroutine.
        apply ty_trm_d_compat. trivial.
  Defined.

  Definition typing_subst_d : forall x X t T G y,
      x ~ X ++ G ⊢D t ⦂ T ->
      uniq (x ~ X ++ G) ->
      x `notin` fv G ->
      G ⊢D trm_var (avar_f y) ⦂ substi x y X ->
      G ⊢D substi x y t ⦂ substi x y T.
  Proof.
    intros. change G with (substi x y nil ++ G).
    eapply typing_subst_d_gen; eroutine.
  Defined.

End Substitution.

Section Narrowing.

  Reserved Notation "G1 ⪯D G2" (at level 70).

  Inductive subenv_d: env -> env -> Type :=
  | subenv_empty : nil ⪯D nil
  | subenv_grow: forall G G' x T T',
      G ⪯D G' ->
      uniq ((x, T) :: G) ->
      uniq ((x, T') :: G') ->
      G ⊢ T <⦂ T' ->
      (x, T) :: G ⪯D (x, T') :: G'
  where "G1 ⪯D G2" := (subenv_d G1 G2).
  Hint Constructors subenv_d.

  Lemma subenv_d_compat : forall G G',
      G ⪯D G' -> G ⪯ G'.
  Proof. induction on subenv_d; routine. Qed.
  Hint Resolve subenv_d_compat.
  
  Definition narrow_var_d :
    forall G G' x T,
      G' ⪯D G ->
      binds x T G ->
      G' ⊢D trm_var x ⦂ T.
  Proof.
    induction on subenv_d; intros.
    - routine.
    - change ((x , T') :: G') with (x ~ T' ++ G') in *.
      pose proof (binds_to_get _ _ H u0).
      destr_get_app; tidy_up.
      + eapply ty_d_sub; simpl_env;
          [ | eapply weaken_subtyp; eassumption].
        constructor; auto.
      + simpl_env. apply weaken_trm_d.
        apply IHsubenv_d.
        apply get_to_binds. trivial.
  Qed.

  Lemma subenv_refl : forall G, uniq G -> G ⪯D G.
  Proof. induction G; eroutine. Qed.
  Local Hint Resolve subenv_refl.

  Lemma subenv_d_push : forall G1 G2 x T,
      G1 ⪯D G2 ->
      uniq ((x, T) :: G1) ->
      uniq ((x, T) :: G2) ->
      (x, T) :: G1 ⪯D (x, T) :: G2.
  Proof. induction G1; eroutine. Qed.
  Local Hint Resolve subenv_d_push.

  Local Hint Extern 1 =>
  match goal with
  | [ H : uniq _ |- _ ] => inversion H
  end.

  Local Hint Extern 1 =>
  match goal with
  | [ H : _ ⪯D _ |- _ ] => inversion H
  end.
  
  Lemma subenv_d_last: forall G x S U,
      G ⊢ S <⦂ U ->
      uniq ((x, S) :: G) ->
      (x, S) :: G ⪯D (x, U) :: G.
  Proof. routine. Qed.
  Hint Resolve subenv_d_last.

  Lemma subenv_d_implies_uniq : forall G1 G2,
      G1 ⪯D G2 -> uniq G1 /\ uniq G2.
  Proof. routine. Qed.
  
  Definition narrow_typing_d_gen :
    forall G G' t T,
      G ⊢D t ⦂ T ->
      G' ⪯D G ->
      G' ⊢D t ⦂ T.
  Proof.
    intros. gen G'. induction X; routine.
    - eapply narrow_var_d; eassumption.
    - econstructor. cofinite.
      apply X. solve_notin.
      apply subenv_d_push; trivial; solve_uniq.
    - eauto.
    - constructor; trivial.
      eapply narrow_rules; eroutine.
    - econstructor.
      + eauto.
      + cofinite. apply X0. solve_notin.
        apply subenv_d_push; trivial; solve_uniq.
    - eapply ty_d_sub. eauto.
      eapply narrow_rules; eroutine.
  Defined.

  Definition narrow_typing : forall x S1 S2 G t T,
      x ~ S2 ++ G ⊢D t ⦂ T ->
      x `notin` dom G ->
      uniq G ->
      G ⊢ S1 <⦂ S2 ->
      x ~ S1 ++ G ⊢D t ⦂ T.
  Proof.
    intros. eapply narrow_typing_d_gen.
    apply X.
    constructor; trivial.
    auto.
    all:solve_uniq.
  Qed.
  
End Narrowing.
