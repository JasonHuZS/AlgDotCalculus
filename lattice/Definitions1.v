Set Implicit Arguments.

Require Export Metalib.Metatheory.
Require Import Coq.Lists.List.
Require Import Coq.Classes.RelationClasses.

Require Export LibUtils.
Require Export Concepts.
Require Import Lattice.

Inductive avar : Set :=
| avar_b : nat -> avar
| avar_f : var -> avar.

Hint Constructors avar.

Coercion avar_b : nat >-> avar.
Coercion avar_f : var >-> avar.

Inductive typ : Set :=
| typ_top : typ
| typ_bot : typ
| typ_sel : avar -> typ_label -> typ
| typ_all : typ -> typ -> typ
| typ_rcd : decs -> typ
| typ_sup : typ -> typ -> typ
| typ_inf : typ -> typ -> typ
with
dec : Set :=
(** dec_typ X A B ::= X : A .. B *)
| dec_typ : typ -> typ -> dec
(** dec_trm x T ::= x : T *)
| dec_trm : typ -> dec
with
decs : Set :=
| decs_nil : decs
| decs_cons : label -> dec -> decs -> decs.
Hint Constructors typ dec decs.


Fixpoint decs_to_list (DS : decs) :=
  match DS with
  | decs_nil => nil
  | decs_cons l d DS' => (l, d) :: decs_to_list DS'
  end.

Fixpoint decs_from_list (l : list (label * dec)) :=
  match l with
  | nil => decs_nil
  | cons (l, d) l' => decs_cons l d $ decs_from_list l'
  end.

Fixpoint decs_append (DS1 : decs) (DS2 : decs) : decs :=
  match DS1 with
  | decs_nil => DS2
  | decs_cons l D DS1' => decs_cons l D $ decs_append DS1' DS2
  end.

Instance DecsList : ListIso (label * dec) decs :=
  {
    to_list := decs_to_list;
    from_list := decs_from_list;
    append := decs_append
  }.
Proof.
  all:induction on decs || induction on list; routine.
Defined.

Coercion decs_to_list : decs >-> list.

Notation "⊤" := typ_top.
Notation "⊥" := typ_bot.
Notation "x ⋅ T" := (typ_sel x T) (at level 40).
Notation "all( A ) B" := (typ_all A B) (at level 40).
Notation "T1 ∨ T2" := (typ_sup T1 T2) (at level 39).
Notation "T1 ∧ T2" := (typ_inf T1 T2) (at level 39).

Notation "X ∈ A ⋯ B" := (X, dec_typ A B) (at level 40).
Notation "x ∷ T" := (x, dec_trm T) (at level 40).
Notation "{< DS >}" := (typ_rcd DS) (at level 40).

Inductive wf_lab_dec : label * dec -> Prop :=
| wf_ld_typ : forall X A B, wf_lab_dec (label_typ X ∈ A ⋯ B)
| wf_ld_trm : forall x T, wf_lab_dec (label_trm x ∷ T).
Hint Constructors wf_lab_dec.

Inductive trm : Set :=
| trm_var : avar -> trm
| trm_val : val -> trm
| trm_sel : avar -> trm_label -> trm
| trm_app : avar -> avar -> trm
| trm_let : trm -> trm -> trm
with
val : Set :=
| val_rcd : defs -> val
| val_lambda : typ -> trm -> val
with
def : Set :=
| def_typ : typ -> def
| def_trm : trm -> def
with
defs : Set :=
| defs_nil : defs
| defs_cons : label -> def -> defs -> defs.

Hint Constructors trm val def defs.

Fixpoint defs_to_list (DS : defs) :=
  match DS with
  | defs_nil => nil
  | defs_cons l d DS' => (l, d) :: defs_to_list DS'
  end.

Fixpoint defs_from_list (l : list (label * def)) :=
  match l with
  | nil => defs_nil
  | cons (l, d) l' => defs_cons l d $ defs_from_list l'
  end.

Fixpoint defs_append (DS1 : defs) (DS2 : defs) : defs :=
  match DS1 with
  | defs_nil => DS2
  | defs_cons l D DS1' => defs_cons l D $ defs_append DS1' DS2
  end.

Instance DefsList : ListIso (label * def) defs :=
  {
    to_list := defs_to_list;
    from_list := defs_from_list;
    append := defs_append
  }.
Proof.
  all:induction on defs || induction on list; routine.
Defined.

Coercion defs_to_list : defs >-> list.

Notation "'lett' x 'inn' y" := (trm_let x y) (at level 40).

Notation "A ≡ B" := (A, def_typ B) (at level 40).
Notation "x ⩴ t" := (x, def_trm t) (at level 40).

Notation "{[ ds ]}" := (val_rcd ds) (at level 40).

Notation "λ( T ){ t }" := (val_lambda T t) (at level 40).

Inductive wf_lab_def : label * def -> Prop :=
| wf_lf_typ : forall A B, wf_lab_def (label_typ A ≡ B)
| wf_lf_trm : forall x t, wf_lab_def (label_trm x ⩴ t).
Hint Constructors wf_lab_def.

(** OPENING *)
Section OpeningDefinition.
  
  Definition open_rec_avar (k : nat) (u : var) (a : avar) : avar :=
    match a with
    | avar_b i => if k == i then avar_f u else avar_b i
    | avar_f x => avar_f x
    end.

  Fixpoint open_rec_typ (k : nat) (u : var) (T : typ) : typ :=
    match T with
    | ⊤ => ⊤
    | ⊥ => ⊥
    | x ⋅ T => (open_rec_avar k u x) ⋅ T
    | all( T ) U => all( open_rec_typ k u T ) open_rec_typ (S k) u U
    | {< DS >} => {< open_rec_decs k u DS >}
    | T1 ∨ T2 => open_rec_typ k u T1 ∨ open_rec_typ k u T2
    | T1 ∧ T2 => open_rec_typ k u T1 ∧ open_rec_typ k u T2
    end
  with
  open_rec_dec (k : nat) (u : var) (D : dec) : dec :=
    match D with
    | dec_typ T U => dec_typ (open_rec_typ k u T) $ open_rec_typ k u U
    | dec_trm T => dec_trm $ open_rec_typ k u T
    end
  with
  open_rec_decs (k : nat) (u : var) (DS : decs) : decs :=
    match DS with
    | decs_nil => decs_nil
    | decs_cons l D DS' =>
      decs_cons l (open_rec_dec k u D) $ open_rec_decs k u DS'
    end.

  Fixpoint open_rec_trm (k : nat) (u : var) (t : trm) : trm :=
    match t with
    | trm_var a => trm_var $ open_rec_avar k u a
    | trm_val v => trm_val $ open_rec_val k u v
    | trm_sel v m => trm_sel (open_rec_avar k u v) m
    | trm_app f x => trm_app (open_rec_avar k u f) $ open_rec_avar k u x
    | trm_let t1 t2 => trm_let (open_rec_trm k u t1) $ open_rec_trm (S k) u t2
    end
  with
  open_rec_val (k : nat) (u : var) (v : val) : val :=
    match v with
    | λ( T ){ e } => λ( open_rec_typ k u T ){ open_rec_trm (S k) u e }
    | {[ ds ]} => {[ open_rec_defs k u ds ]}
    end
  with
  open_rec_def (k : nat) (u : var) (d : def) : def :=
    match d with
    | def_typ T => def_typ $ open_rec_typ k u T
    | def_trm e => def_trm $ open_rec_trm k u e
    end
  with
  open_rec_defs (k : nat) (u : var) (ds : defs) : defs :=
    match ds with
    | defs_nil => defs_nil
    | defs_cons l df ds' => defs_cons l (open_rec_def k u df) $ open_rec_defs k u ds'
    end.
    
End OpeningDefinition.

Instance OpenAvar : CanOpen avar := { open_rec := open_rec_avar }.
Instance OpenTyp : CanOpen typ := { open_rec := open_rec_typ }.
Instance OpenDec : CanOpen dec := { open_rec := open_rec_dec }.
Instance OpenDecs : CanOpen decs := { open_rec := open_rec_decs }.
Instance OpenTrm : CanOpen trm := { open_rec := open_rec_trm }.
Instance OpenVal : CanOpen val := { open_rec := open_rec_val }.
Instance OpenDef : CanOpen def := { open_rec := open_rec_def }.
Instance OpenDefs : CanOpen defs := { open_rec := open_rec_defs }.

Notation env := (list (atom * typ)).

Reserved Notation "G '⊢' t '⦂' T" (at level 70, t at level 79).
Reserved Notation "G '⊢' T '<⦂' U" (at level 70, T at level 79).
Reserved Notation "G ⊩ d ⦂ D" (at level 70, d at level 79).
Reserved Notation "G ⊩[ ds ⦂ DS ]" (at level 70, ds at level 79).

Inductive ty_trm : env -> trm -> typ -> Prop :=
| ty_var : forall G x T,
    binds x T G ->
    G ⊢ trm_var x ⦂ T

| ty_all_intro : forall L G T t U,
    (forall x, x `notin` L ->
          x ~ T ++ G ⊢ open x t ⦂ open x U) ->
    G ⊢ trm_val (λ( T ){ t }) ⦂ all( T ) U
| ty_all_elim : forall G (x z : atom) S' S T,
    G ⊢ trm_var x ⦂ all( S ) T ->
    G ⊢ trm_var z ⦂ S' ->
    G ⊢ (trm_app x z) ⦂ open z T

| ty_rcd_intro : forall G ds DS,
    G ⊩[ ds ⦂ DS ] ->
    Forall wf_lab_def ds ->
    G ⊢ trm_val ({[ ds ]}) ⦂ {< DS >}
| typ_rcd_elim : forall G (x : var) DS a T,
    G ⊢ trm_var x ⦂ {< DS >} ->
    lbinds (label_trm a) (dec_trm T) DS ->
    G ⊢ trm_sel x a ⦂ T

| ty_let : forall L G t u T U,
    G ⊢ t ⦂ T ->
    (forall x, x `notin` L ->
          x ~ T ++ G ⊢ open x u ⦂ U) ->
    G ⊢ lett t inn u ⦂ U
| ty_sub : forall G S U t,
    G ⊢ t ⦂ S ->
    G ⊢ S <⦂ U ->
    G ⊢ t ⦂ U
where "G ⊢ t ⦂ T" := (ty_trm G t T) : type_scope

with
ty_def : env -> (label * def) -> dec -> Prop :=
| ty_def_typ : forall G A T,
    G ⊩ (label_typ A, def_typ T) ⦂ dec_typ T T
| ty_def_trm : forall G a t T,
    G ⊢ t ⦂ T ->
    G ⊩ (label_trm a, def_trm t) ⦂ dec_trm T
where "G ⊩ d ⦂ D" := (ty_def G d D) : type_scope

with
ty_defs : env -> defs -> decs -> Prop :=
| ty_defs_nil : forall G, G ⊩[ defs_nil ⦂ decs_nil ]
| ty_defs_cons : forall G l d D ds DS,
    G ⊩ (l, d) ⦂ D ->
    G ⊩[ ds ⦂ DS ] ->
    G ⊩[ defs_cons l d ds ⦂ decs_cons l D DS ]
where "G ⊩[ ds ⦂ DS ]" := (ty_defs G ds DS) : type_scope

with
subtyp : env -> typ -> typ -> Prop :=
| subtyp_top : forall G T,
    G ⊢ T <⦂ ⊤
| subtyp_bot : forall G T,
    G ⊢ ⊥ <⦂ T
| subtyp_refl : forall G T,
    G ⊢ T <⦂ T
| subtyp_trans : forall G S T U,
    G ⊢ S <⦂ T ->
    G ⊢ T <⦂ U ->
    G ⊢ S <⦂ U

(** connectivity rules to make the types a lattice *)

| subtyp_inf_intro : forall G T1 T2 T,
    G ⊢ T <⦂ T1 ->
    G ⊢ T <⦂ T2 ->
    G ⊢ T <⦂ T1 ∧ T2
| subtyp_inf_elim1 : forall G T1 T2,
    G ⊢ T1 ∧ T2 <⦂ T1
| subtyp_inf_elim2 : forall G T1 T2,
    G ⊢ T1 ∧ T2 <⦂ T2

| subtyp_sup_elim : forall G T1 T2 T,
    G ⊢ T1 <⦂ T ->
    G ⊢ T2 <⦂ T ->
    G ⊢ T1 ∨ T2 <⦂ T
| subtyp_sup_intro1 : forall G T1 T2,
    G ⊢ T1 <⦂ T1 ∨ T2
| subtyp_sup_intro2 : forall G T1 T2,
    G ⊢ T2 <⦂ T1 ∨ T2

(** additional algebraic rules *)

(** following subtyping rules shows record sublattice and function lattice are disjoint. *)
| subtyp_rf_disjoint1 : forall G DS S U,
    G ⊢ ({< DS >} ∧ all(S) U) <⦂ ⊥

| subtyp_rf_disjoint2 : forall G DS S U,
    G ⊢ ⊤ <⦂ ({< DS >} ∨ all(S) U)
                        
(** algebraic rules for record types
  *
  * the rules encodes a set of rules to compute infimum and supremum of record types.
  *)

(** records combination is the same as their infimum type. *)
| subtyp_rcd_inf_equiv1 : forall G DS1 DS2,
    G ⊢ {< append DS1 DS2 >} <⦂ {< DS1 >} ∧ {< DS2 >}
| subtyp_rcd_inf_equiv2 : forall G DS1 DS2,
    G ⊢ {< DS1 >} ∧ {< DS2 >} <⦂ {< append DS1 DS2 >}

(** record supremum is the same as the their set intersection, with potential further
  * infimum/supremum distribution.
  *)
| subtyp_rcd_sup_cons : forall G l D DS DS' R,
    G ⊢ R <⦂ ({< decs_cons l D decs_nil >} ∨ {< DS' >}) ->
    G ⊢ R ∧ ({< DS >} ∨ {< DS' >}) <⦂ {< decs_cons l D DS >} ∨ {< DS' >}

(** following two rules can only be distinguished, if `lbinds` is decidable for records,
  * which is provable, except I am too lazy for it.
  *)
| subtyp_rcd_sup_comp_binds : forall G l D D' (DS : decs),
    lbinds l D' DS ->
    G ⊢ ({< decs_cons l D decs_nil >} ∨ {<decs_cons l D' decs_nil >})
      <⦂ {< decs_cons l D decs_nil >} ∨ {< DS >}
| subtyp_rcd_sup_comp_nbinds : forall G l D (DS : decs),
    l `lnotin` ldom DS ->
    G ⊢ {< decs_nil >} <⦂ {< decs_cons l D decs_nil >} ∨ {< DS >}

(** how records can do subtyping. *)
| subtyp_fld : forall G a T (DS1 DS2 : decs) U,
    G ⊢ T <⦂ U ->
    G ⊢ {< decs_cons (label_trm a) (dec_trm T) decs_nil >}
      <⦂ {< decs_cons (label_trm a) (dec_trm U) decs_nil >} (* DS[a := U] *)
| subtyp_typ : forall G A (DS1 DS2 : decs) S1 T1 S2 T2,
    G ⊢ S2 <⦂ S1 ->
    G ⊢ T1 <⦂ T2 ->
    G ⊢ {< decs_cons (label_typ A) (dec_typ S1 T1) decs_nil >}
      <⦂ {< decs_cons (label_typ A) (dec_typ S2 T2) decs_nil >}
(* DS[A := S2 .. T2] *)

| subtyp_sel1 : forall G (x : var) A DS S T,
    G ⊢ trm_var x ⦂ {< DS >} ->
    lbinds (label_typ A) (dec_typ S T) DS ->
    G ⊢ S <⦂ typ_sel x A
| subtyp_sel2 : forall G (x : var) A DS S T,
    G ⊢ trm_var x ⦂ {< DS >} ->
    lbinds (label_typ A) (dec_typ S T) DS ->
    G ⊢ typ_sel x A <⦂ T

(** following four rules shows how infimum/supremum distribute across records.
  *
  * these rules can be used to compute primitive object types, if combined with
  * `subtyp_rcd_inf_equiv` rules.
  *)
| subtyp_rcd_fld_inf_distr : forall G a T U,
    G ⊢ {< decs_cons (label_trm a) (dec_trm T) decs_nil >}
        ∧ {< decs_cons (label_trm a) (dec_trm U) decs_nil >}
      <⦂ {< decs_cons (label_trm a) (dec_trm (T ∧ U)) decs_nil >}
| subtyp_rcd_fld_sup_distr : forall G a T U,
    G ⊢ {< decs_cons (label_trm a) (dec_trm (T ∨ U)) decs_nil >}
      <⦂ {< decs_cons (label_trm a) (dec_trm T) decs_nil >}
      ∨ {< decs_cons (label_trm a) (dec_trm U) decs_nil >}

| subtyp_rcd_typ_inf_distr : forall G A S1 S2 U1 U2,
    G ⊢ {< decs_cons (label_typ A) (dec_typ S1 U1) decs_nil >}
      ∧ {< decs_cons (label_typ A) (dec_typ S2 U2) decs_nil >}
      <⦂ {< decs_cons (label_typ A) (dec_typ (S1 ∨ S2) (U1 ∧ U2)) decs_nil >}
| subtyp_rcd_typ_sup_distr : forall G A S1 S2 U1 U2,
    G ⊢ {< decs_cons (label_typ A) (dec_typ (S1 ∧ S2) (U1 ∨ U2)) decs_nil >}
      <⦂ {< decs_cons (label_typ A) (dec_typ S1 U1) decs_nil >}
      ∨ {< decs_cons (label_typ A) (dec_typ S2 U2) decs_nil >}

(** algebraic rules for function types *)

| subtyp_all: forall L G S1 T1 S2 T2,
    G ⊢ S2 <⦂ S1 ->
    (forall x, x `notin` L ->
       x ~ S2 ++ G ⊢ open x T1 <⦂ open x T2) ->
    G ⊢ all(S1) T1 <⦂ all(S2) T2

(** distributivity computes the effects of supremum/infimum applied to function types. *)
| subtyp_all_inf_distr : forall G S1 S2 U1 U2,
    G ⊢ (all(S1) U1) ∧ (all(S2) U2) <⦂ all(S1 ∨ S2) (U1 ∧ U2)
| subtyp_all_sup_distr : forall G S1 S2 U1 U2,
    G ⊢ all(S1 ∧ S2) (U1 ∨ U2) <⦂ (all(S1) U1) ∨ (all(S2) U2)
         
where "G ⊢ T <⦂ U" := (subtyp G T U) : type_scope.
Hint Constructors ty_trm ty_def ty_defs subtyp.

(** now we are going to verify subtyping form a bounded lattice. *)

Definition typ_equiv G := equiv (subtyp G).
Hint Transparent typ_equiv.

Notation "G ⊢ T ≈ U" := (typ_equiv G T U) (at level 70, T at level 79).

Import Order.

Section TypeLattice.

  Context {G : env}.

  Global Instance TypePreOrder : PreOrder (subtyp G) := { }.
  Proof. all:eroutine. Qed.

  Global Instance TypeLattice : OLattice (subtyp G) TypePreOrder :=
    {
      top := typ_top;
      bot := typ_bot;
      sup := typ_sup;
      inf := typ_inf
    }.
  Proof. all:eroutine. Qed.
  
End TypeLattice.