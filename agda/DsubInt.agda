{-# OPTIONS --without-K --safe #-}

module DsubInt where


open import Data.List as List
open import Data.List.All
open import Data.Nat as ℕ
open import Data.Maybe as Maybe
open import Data.Product
open import Data.Sum
open import Data.Empty renaming (⊥ to False)
open import Function

open import Data.Nat.Properties as ℕₚ
open import Data.Maybe.Properties as Maybeₚ
open import Relation.Nullary
open import Relation.Binary.PropositionalEquality as ≡

open import Induction.Nat

open import Utils

infix 8 _∙A
infix 6 Π_∙_ ⟨A:_⋯_⟩
infixl 7 _∩_

data Typ : Set where
  ⊤       : Typ
  ⊥       : Typ
  _∙A     : (n : ℕ) → Typ
  Π_∙_    : (S U : Typ) → Typ
  ⟨A:_⋯_⟩ : (S U : Typ) → Typ
  _∩_     : (S U : Typ) → Typ

Env : Set
Env = List Typ

infixl 7 _↑_
_↑_ : Typ → ℕ → Typ
⊤ ↑ n           = ⊤
⊥ ↑ n           = ⊥
(x ∙A) ↑ n with n ≤? x
... | yes n≤x   = suc x ∙A
... | no n>x    = x ∙A
(Π S ∙ U) ↑ n   = Π S ↑ n ∙ U ↑ suc n
⟨A: S ⋯ U ⟩ ↑ n = ⟨A: S ↑ n ⋯ U ↑ n ⟩
(S ∩ U) ↑ n     = (S ↑ n) ∩ (U ↑ n)

infix 4 _↦_∈_
data _↦_∈_ : ℕ → Typ → Env → Set where
  hd : ∀ {T Γ} → 0 ↦ T ↑ 0 ∈ T ∷ Γ
  tl : ∀ {n T T′ Γ} → n ↦ T ∈ Γ → suc n ↦ T ↑ 0 ∈ T′ ∷ Γ

env-lookup : Env → ℕ → Maybe Typ
env-lookup Γ n = Maybe.map (repeat (suc n) (_↑ 0)) (lookupOpt Γ n)

↦∈⇒lookup : ∀ {x T Γ} → x ↦ T ∈ Γ → env-lookup Γ x ≡ just T
↦∈⇒lookup hd = refl
↦∈⇒lookup {x} {_} {Γ} (tl T∈Γ) with lookupOpt Γ x | ↦∈⇒lookup T∈Γ
... | just T′ | eq = cong (just ∘ (_↑ zero)) (just-injective eq)
... | nothing | ()

lookup⇒↦∈ : ∀ {x T Γ} → env-lookup Γ x ≡ just T → x ↦ T ∈ Γ
lookup⇒↦∈ {x} {_} {[]} ()
lookup⇒↦∈ {zero} {_} {T ∷ Γ} refl = hd
lookup⇒↦∈ {suc x} {_} {T ∷ Γ} eq with lookupOpt Γ x | λ {T} → lookup⇒↦∈ {x} {T} {Γ}
lookup⇒↦∈ refl | just T′ | rec = tl $ rec refl
lookup⇒↦∈ ()   | nothing | _

⟨A:⟩-injective : ∀ {S U S′ U′} → ⟨A: S ⋯ U ⟩ ≡ ⟨A: S′ ⋯ U′ ⟩ → S ≡ S′ × U ≡ U′
⟨A:⟩-injective refl = refl , refl

↑-idx : ℕ → ℕ → ℕ
↑-idx x n with n ≤? x
... | yes p = suc x
... | no ¬p = x

↑-var : ∀ x n → x ∙A ↑ n ≡ ↑-idx x n ∙A
↑-var x n with n ≤? x
... | yes p = refl
... | no ¬p = refl

↑-↑-comm : ∀ T m n → m ≤ n → T ↑ m ↑ suc n ≡ T ↑ n ↑ m
↑-↑-comm ⊤ m n m≤n                                  = refl
↑-↑-comm ⊥ m n m≤n                                  = refl
↑-↑-comm (x ∙A) m n m≤n with n ≤? x
... | yes n≤x
    rewrite ≤?-yes (≤-trans m≤n n≤x)
          | ≤?-yes n≤x
          | ≤?-yes (≤-step (≤-trans m≤n n≤x))       = refl
... | no n>x with m ≤? x
...             | yes m≤x rewrite proj₂ $ ≤?-no n>x = refl
...             | no m>x with suc n ≤? x
...                         | yes 1+n≤x             = ⊥-elim (m>x (≤-trans (≤-step m≤n) 1+n≤x))
...                         | no 1+n>x              = refl
↑-↑-comm (Π S ∙ U) m n m≤n
  rewrite ↑-↑-comm S m n m≤n
        | ↑-↑-comm U (suc m) (suc n) (s≤s m≤n)      = refl
↑-↑-comm ⟨A: S ⋯ U ⟩ m n m≤n
  rewrite ↑-↑-comm S m n m≤n | ↑-↑-comm U m n m≤n   = refl
↑-↑-comm (S ∩ U) m n m≤n
  rewrite ↑-↑-comm S m n m≤n | ↑-↑-comm U m n m≤n   = refl

Typ-measure : Typ → ℕ
Typ-measure ⊤           = 1
Typ-measure ⊥           = 1
Typ-measure (_ ∙A)      = 2
Typ-measure (Π S ∙ U)   = 1 + Typ-measure S + Typ-measure U
Typ-measure ⟨A: S ⋯ U ⟩ = 1 + Typ-measure S + Typ-measure U
Typ-measure (S ∩ U)     = 1 + Typ-measure S + Typ-measure U

Typ-measure-↑ : ∀ T n → Typ-measure (T ↑ n) ≡ Typ-measure T
Typ-measure-↑ ⊤ n                 = refl
Typ-measure-↑ ⊥ n                 = refl
Typ-measure-↑ (x ∙A) n
  rewrite ↑-var x n               = refl
Typ-measure-↑ (Π S ∙ U) n
  rewrite Typ-measure-↑ S n
        | Typ-measure-↑ U (suc n) = refl
Typ-measure-↑ ⟨A: S ⋯ U ⟩ n
  rewrite Typ-measure-↑ S n
        | Typ-measure-↑ U n       = refl
Typ-measure-↑ (S ∩ U) n
  rewrite Typ-measure-↑ S n
        | Typ-measure-↑ U n       = refl

infix 4 _⊢_<:_

data _⊢_<:_ : Env → Typ → Typ → Set where
  <:⊤  : ∀ {Γ T} → Γ ⊢ T <: ⊤
  ⊥<:  : ∀ {Γ T} → Γ ⊢ ⊥ <: T
  refl : ∀ {Γ T} → Γ ⊢ T <: T
  bnd  : ∀ {Γ S U S′ U′} →
           Γ ⊢ S′ <: S →
           Γ ⊢ U <: U′ →
           Γ ⊢ ⟨A: S ⋯ U ⟩ <: ⟨A: S′ ⋯ U′ ⟩
  Π<:  : ∀ {Γ S U S′ U′} →
           Γ ⊢ S′ <: S →
           Γ ‣ S′ ! ⊢ U <: U′ →
           Γ ⊢ Π S ∙ U <: Π S′ ∙ U′
  sel₁ : ∀ {Γ n T S U} →
           env-lookup Γ n ≡ just T →
           Γ ⊢ T <: ⟨A: S ⋯ U ⟩ →
           Γ ⊢ S <: n ∙A
  sel₂ : ∀ {Γ n T S U} →
           env-lookup Γ n ≡ just T →
           Γ ⊢ T <: ⟨A: S ⋯ U ⟩ →
           Γ ⊢ n ∙A <: U
  <:>  : ∀ {Γ n T S₁ U₁ S₂ U₂} →
           env-lookup Γ n ≡ just T →
           Γ ⊢ T <: ⟨A: S₁ ⋯ U₁ ⟩ →
           Γ ⊢ T <: ⟨A: S₂ ⋯ U₂ ⟩ →
           Γ ⊢ S₁ <: U₂
  ∩-E₁ : ∀ {Γ S′ S U} →
           Γ ⊢ S′ <: S →
           Γ ⊢ S′ ∩ U <: S
  ∩-E₂ : ∀ {Γ U′ S U} →
           Γ ⊢ U′ <: U →
           Γ ⊢ S ∩ U′ <: U
  ∩-I  : ∀ {Γ T S U} →
           Γ ⊢ T <: S → Γ ⊢ T <: U →
           Γ ⊢ T <: S ∩ U

infixl 7 _⇑
_⇑ : Env → Env
[] ⇑      = []
(T ∷ Γ) ⇑ = T ↑ length Γ ∷ Γ ⇑

↦∈-weaken-≤ : ∀ {n T Γ} →
                n ↦ T ∈ Γ →
              ∀ Γ₁ Γ₂ T′ →
                Γ ≡ Γ₁ ‣ Γ₂ →
                length Γ₂ ≤ n →
                suc n ↦ T ↑ length Γ₂ ∈ Γ₁ ‣ T′ ! ‣ Γ₂ ⇑
↦∈-weaken-≤ hd .(_ ∷ _) [] T′ refl z≤n       = tl hd
↦∈-weaken-≤ hd Γ₁ (_ ∷ Γ₂) T′ eqΓ ()
↦∈-weaken-≤ (tl T∈Γ) .(_ ∷ _) [] T′ refl z≤n = tl (tl T∈Γ)
↦∈-weaken-≤ (tl {_} {T} T∈Γ) Γ₁ (_ ∷ Γ₂) T′ refl (s≤s l≤n)
  rewrite ↑-↑-comm T 0 (length Γ₂) z≤n       = tl (↦∈-weaken-≤ T∈Γ Γ₁ Γ₂ T′ refl l≤n)

↦∈-weaken-> : ∀ {n T Γ} →
                n ↦ T ∈ Γ →
              ∀ Γ₁ Γ₂ T′ →
                Γ ≡ Γ₁ ‣ Γ₂ →
                length Γ₂ > n →
                n ↦ T ↑ length Γ₂ ∈ Γ₁ ‣ T′ ! ‣ Γ₂ ⇑
↦∈-weaken-> T∈Γ Γ₁ [] T′ eqΓ ()
↦∈-weaken-> (hd {T}) Γ₁ (_ ∷ Γ₂) T′ refl (s≤s l>n)
  rewrite ↑-↑-comm T 0 (length Γ₂) l>n = hd
↦∈-weaken-> (tl {_} {T} T∈Γ) Γ₁ (_ ∷ Γ₂) T′ refl (s≤s l>n)
  rewrite ↑-↑-comm T 0 (length Γ₂) z≤n = tl (↦∈-weaken-> T∈Γ Γ₁ Γ₂ T′ refl l>n)

↦∈-weaken : ∀ {n T Γ} →
              n ↦ T ∈ Γ →
            ∀ Γ₁ Γ₂ T′ →
              Γ ≡ Γ₁ ‣ Γ₂ →
              ↑-idx n (length Γ₂) ↦ T ↑ length Γ₂ ∈ Γ₁ ‣ T′ ! ‣ Γ₂ ⇑
↦∈-weaken {n} T∈Γ _ Γ₂ _ eqΓ with length Γ₂ ≤? n
... | yes l≤n = ↦∈-weaken-≤ T∈Γ _ Γ₂ _ eqΓ l≤n
... | no l>n  = ↦∈-weaken-> T∈Γ _ Γ₂ _ eqΓ (≰⇒> l>n)

↦∈-weaken′ : ∀ {n T Γ} →
               env-lookup Γ n ≡ just T →
             ∀ Γ₁ Γ₂ T′ →
               Γ ≡ Γ₁ ‣ Γ₂ →
               env-lookup (Γ₁ ‣ T′ ! ‣ Γ₂ ⇑) (↑-idx n (length Γ₂)) ≡ just (T ↑ length Γ₂)
↦∈-weaken′ T∈Γ Γ₁ Γ₂ T′ eqΓ = ↦∈⇒lookup (↦∈-weaken (lookup⇒↦∈ T∈Γ) Γ₁ Γ₂ T′ eqΓ)

<:-weakening-gen : ∀ {Γ S U} →
                      Γ ⊢ S <: U →
                    ∀ Γ₁ Γ₂ T →
                      Γ ≡ Γ₁ ‣ Γ₂ →
                      Γ₁ ‣ T ! ‣ Γ₂ ⇑ ⊢ S ↑ length Γ₂ <: U ↑ length Γ₂
<:-weakening-gen <:⊤ Γ₁ Γ₂ T eqΓ               = <:⊤
<:-weakening-gen ⊥<: Γ₁ Γ₂ T eqΓ               = ⊥<:
<:-weakening-gen refl Γ₁ Γ₂ T eqΓ              = refl
<:-weakening-gen (bnd S′<:S U<:U′) Γ₁ Γ₂ T eqΓ = bnd (<:-weakening-gen S′<:S Γ₁ Γ₂ T eqΓ)
                                                     (<:-weakening-gen U<:U′ Γ₁ Γ₂ T eqΓ)
<:-weakening-gen (Π<: S′<:S U<:U′) Γ₁ Γ₂ T eqΓ = Π<: (<:-weakening-gen S′<:S Γ₁ Γ₂ T eqΓ)
                                                     (<:-weakening-gen U<:U′ Γ₁ (_ ∷ Γ₂) T (cong (_ ∷_) eqΓ))
<:-weakening-gen (sel₁ {_} {n} T∈Γ T<:SU) Γ₁ Γ₂ T eqΓ
  rewrite ↑-var n (length Γ₂)                  = sel₁ (↦∈-weaken′ T∈Γ Γ₁ Γ₂ T eqΓ)
                                                      (<:-weakening-gen T<:SU Γ₁ Γ₂ T eqΓ)
<:-weakening-gen (sel₂ {_} {n} T∈Γ T<:SU) Γ₁ Γ₂ T eqΓ
  rewrite ↑-var n (length Γ₂)                  = sel₂ (↦∈-weaken′ T∈Γ Γ₁ Γ₂ T eqΓ)
                                                      (<:-weakening-gen T<:SU Γ₁ Γ₂ T eqΓ)
<:-weakening-gen (<:> {_} {n} T∈Γ T<:SU T<:SU′) Γ₁ Γ₂ T eqΓ
  rewrite ↑-var n (length Γ₂)                  = <:> (↦∈-weaken′ T∈Γ Γ₁ Γ₂ T eqΓ)
                                                     (<:-weakening-gen T<:SU Γ₁ Γ₂ T eqΓ)
                                                     (<:-weakening-gen T<:SU′ Γ₁ Γ₂ T eqΓ)
<:-weakening-gen (∩-E₁ S′<:S) Γ₁ Γ₂ T eqΓ      = ∩-E₁ (<:-weakening-gen S′<:S Γ₁ Γ₂ T eqΓ)
<:-weakening-gen (∩-E₂ U′<:U) Γ₁ Γ₂ T eqΓ      = ∩-E₂ (<:-weakening-gen U′<:U Γ₁ Γ₂ T eqΓ)
<:-weakening-gen (∩-I T<:S T<:U) Γ₁ Γ₂ T eqΓ   = ∩-I (<:-weakening-gen T<:S Γ₁ Γ₂ T eqΓ)
                                                     (<:-weakening-gen T<:U Γ₁ Γ₂ T eqΓ)

<:-weakening : ∀ {Γ₁ Γ₂ S U} T →
                  Γ₁ ‣ Γ₂ ⊢ S <: U →
                  Γ₁ ‣ T ! ‣ Γ₂ ⇑ ⊢ S ↑ length Γ₂ <: U ↑ length Γ₂
<:-weakening T S<:U = <:-weakening-gen S<:U _ _ T refl

<:-weakening-hd : ∀ {Γ S U} T →
                     Γ ⊢ S <: U →
                     Γ ‣ T ! ⊢ S ↑ 0 <: U ↑ 0
<:-weakening-hd T = <:-weakening {Γ₂ = []} T


module Transitivity where    
  infix 4 _≺:[_]_
  data _≺:[_]_ : Env → ℕ → Env → Set where
    ≺[_,_] : ∀ {Γ U} S → Γ ⊢ S <: U → Γ ‣ S ! ≺:[ 0 ] Γ ‣ U !
    _∷_    : ∀ {Γ₁ n Γ₂} T → Γ₁ ≺:[ n ] Γ₂ → Γ₁ ‣ T ! ≺:[ suc n ] Γ₂ ‣ T !

  <:∈-find : ∀ {x T Γ Γ′ n} →
               x ↦ T ∈ Γ →
               Γ′ ≺:[ n ] Γ →
               x ≡ n × (∃ λ T′ → n ↦ T′ ∈ Γ′ × Γ′ ⊢ T′ <: T) ⊎ x ≢ n × x ↦ T ∈ Γ′
  <:∈-find hd ≺[ T′ , T′<:T ]           = inj₁ (refl , T′ ↑ 0 , hd , <:-weakening-hd T′ T′<:T)
  <:∈-find hd (T ∷ Γ′≺:Γ)               = inj₂ ((λ ()) , hd)
  <:∈-find (tl T∈Γ) ≺[ T′ , T′<:T ]     = inj₂ ((λ ()) , tl T∈Γ)
  <:∈-find (tl T∈Γ) (S ∷ Γ′≺:Γ) with <:∈-find T∈Γ Γ′≺:Γ
  ... | inj₁ (x≡n , T′ , T′∈Γ′ , T′<:T) = inj₁ (cong suc x≡n , T′ ↑ 0 , tl T′∈Γ′ , <:-weakening-hd S T′<:T)
  ... | inj₂ (x≢n , T∈Γ′)               = inj₂ (x≢n ∘ suc-injective , tl T∈Γ′)

  <:∈-find′ : ∀ {x T Γ Γ′ n} →
                env-lookup Γ x ≡ just T →
                Γ′ ≺:[ n ] Γ →
                x ≡ n × (∃ λ T′ → env-lookup Γ′ n ≡ just T′ × Γ′ ⊢ T′ <: T) ⊎ x ≢ n × env-lookup Γ′ x ≡ just T
  <:∈-find′ T∈Γ Γ′≺Γ with <:∈-find (lookup⇒↦∈ T∈Γ) Γ′≺Γ
  ... | inj₁ (x≡n , T′ , T′∈Γ′ , T′<:T) = inj₁ (x≡n , T′ , ↦∈⇒lookup T′∈Γ′ , T′<:T)
  ... | inj₂ (x≢n , T∈Γ′)               = inj₂ (x≢n , ↦∈⇒lookup T∈Γ′)

  private
    trans-on : Typ → Set
    trans-on T = ∀ {Γ S U} → Γ ⊢ S <: T → Γ ⊢ T <: U → Γ ⊢ S <: U

    narrow-on : Typ → Set
    narrow-on T = ∀ {Γ Γ′ n S U} →
                    Γ ⊢ S <: U →
                    Γ′ ≺:[ n ] Γ →
                    env-lookup Γ n ≡ just T →
                    Γ′ ⊢ S <: U

  ⟨A:⟩-layer : Typ → List Typ → Typ
  ⟨A:⟩-layer T [] = T
  ⟨A:⟩-layer T (S ∷ l) = ⟨A: S ⋯ ⟨A:⟩-layer T l ⟩

  mutual
    <:-trans-rec : ∀ T → (∀ T′ → Typ-measure T′ < Typ-measure T → trans-on T′ × narrow-on T′) → trans-on T
    <:-trans-rec ⊤ rec S<:T <:⊤                                            = S<:T
    <:-trans-rec ⊤ rec S<:T refl                                           = S<:T
    <:-trans-rec ⊥ rec ⊥<: T<:U                                            = T<:U
    <:-trans-rec ⊥ rec refl T<:U                                           = T<:U
    <:-trans-rec (n ∙A) rec ⊥<: T<:U                                       = ⊥<:
    <:-trans-rec (n ∙A) rec refl T<:U                                      = T<:U
    <:-trans-rec (n ∙A) rec (sel₁ B∈Γ B<:SU′) <:⊤                          = <:⊤
    <:-trans-rec (n ∙A) rec (sel₁ B∈Γ B<:SU′) refl                         = sel₁ B∈Γ B<:SU′
    <:-trans-rec (n ∙A) rec (sel₁ B∈Γ B<:SU′) (sel₂ B′∈Γ B′<:S′U)
      rewrite just-injective (≡.trans (≡.sym B′∈Γ) B∈Γ)                    = <:> B∈Γ B<:SU′ B′<:S′U
    <:-trans-rec ⟨A: S′ ⋯ U′ ⟩ rec ⊥<: T<:U                                = ⊥<:
    <:-trans-rec ⟨A: S′ ⋯ U′ ⟩ rec refl T<:U                               = T<:U
    <:-trans-rec ⟨A: S′ ⋯ U′ ⟩ rec (bnd S′<:S″ U″<:U′) <:⊤                 = <:⊤
    <:-trans-rec ⟨A: S′ ⋯ U′ ⟩ rec (bnd S′<:S″ U″<:U′) refl                = bnd S′<:S″ U″<:U′
    <:-trans-rec ⟨A: S′ ⋯ U′ ⟩ rec (bnd S′<:S″ U″<:U′) (bnd S‴<:S′ U′<:U‴) = bnd (<:-trans-rec S′ (λ T′ T′<S′ → rec T′ (≤-step (≤-stepsʳ _ T′<S′))) S‴<:S′ S′<:S″)
                                                                                 (<:-trans-rec U′ (λ T′ T′<U′ → rec T′ (≤-step (≤-stepsˡ _ T′<U′))) U″<:U′ U′<:U‴)
    <:-trans-rec (S′ ∩ U′) rec ⊥<: T<:U                                    = ⊥<:
    <:-trans-rec (S′ ∩ U′) rec refl T<:U                                   = T<:U
    <:-trans-rec (S′ ∩ U′) rec (∩-I T′<:S′ T′<:U′) <:⊤                     = <:⊤
    <:-trans-rec (S′ ∩ U′) rec (∩-I T′<:S′ T′<:U′) refl                    = ∩-I T′<:S′ T′<:U′
    <:-trans-rec (S′ ∩ U′) rec (∩-I T′<:S′ T′<:U′) (∩-E₁ S′<:U)            = <:-trans-rec S′ (λ T′ T′<S′ → rec T′ (≤-step (≤-stepsʳ _ T′<S′))) T′<:S′ S′<:U
    <:-trans-rec (S′ ∩ U′) rec (∩-I T′<:S′ T′<:U′) (∩-E₂ U′<:U)            = <:-trans-rec U′ (λ T′ T′<U′ → rec T′ (≤-step (≤-stepsˡ _ T′<U′))) T′<:U′ U′<:U
    <:-trans-rec (Π S′ ∙ U′) rec ⊥<: T<:U                                  = ⊥<:
    <:-trans-rec (Π S′ ∙ U′) rec refl T<:U                                 = T<:U
    <:-trans-rec (Π S′ ∙ U′) rec (Π<: S′<:S″ U″<:U′) <:⊤                   = <:⊤
    <:-trans-rec (Π S′ ∙ U′) rec (Π<: S′<:S″ U″<:U′) refl                  = Π<: S′<:S″ U″<:U′
    <:-trans-rec (Π S′ ∙ U′) rec (Π<: S′<:S″ U″<:U′) (Π<: S‴<:S′ U′<:U‴)   = Π<: (<:-trans-rec S′ (λ T′ T′<S′ → rec T′ (≤-step (≤-stepsʳ _ T′<S′))) S‴<:S′ S′<:S″)
                                                                                 (<:-trans-rec U′
                                                                                               (λ T′ T′<U′ → rec T′ (≤-step (≤-stepsˡ _ T′<U′)))
                                                                                               (proj₂ (rec (S′ ↑ 0) (s≤s $ ≤-stepsʳ _ $ ≤-reflexive (Typ-measure-↑ S′ 0)))
                                                                                                      U″<:U′ ≺[ _ , S‴<:S′ ] refl)
                                                                                               U′<:U‴)

    <:-trans-rec T rec (∩-E₁ S<:T) T<:U                                    = ∩-E₁ (<:-trans-rec T rec S<:T T<:U)
    <:-trans-rec T rec (∩-E₂ S<:T) T<:U                                    = ∩-E₂ (<:-trans-rec T rec S<:T T<:U)
    <:-trans-rec T rec S<:T (∩-I T<:S′ T<:U′)                              = ∩-I (<:-trans-rec T rec S<:T T<:S′) (<:-trans-rec T rec S<:T T<:U′)
    <:-trans-rec T rec (sel₂ B∈Γ B<:ST) T<:U                               = sel₂ B∈Γ (⟨A<:⟩-traverseʳ T rec T<:U B<:ST [] refl)
    <:-trans-rec T rec (<:> B∈Γ B<:SU′ B<:S′T) T<:U                        = <:> B∈Γ B<:SU′ (⟨A<:⟩-traverseʳ T rec T<:U B<:S′T [] refl)
    <:-trans-rec T rec S<:T (sel₁ B∈Γ B<:TU)                               = sel₁ B∈Γ (⟨A<:⟩-traverseˡ T rec S<:T B<:TU [] refl)
    <:-trans-rec T rec S<:T (<:> B∈Γ B<:TU′ B<:S′U)                        = <:> B∈Γ (⟨A<:⟩-traverseˡ T rec S<:T B<:TU′ [] refl) B<:S′U

    ⟨A<:⟩-traverseʳ : ∀ T →
                        (∀ T′ → Typ-measure T′ < Typ-measure T → trans-on T′ × narrow-on T′) →
                      ∀ {Γ U} →
                        Γ ⊢ T <: U →
                      ∀ {S S′ T′} →
                        Γ ⊢ S <: ⟨A: S′ ⋯ T′ ⟩ →
                      ∀ l →
                        T′ ≡ ⟨A:⟩-layer T l →
                        Γ ⊢ S <: ⟨A: S′ ⋯ ⟨A:⟩-layer U l ⟩
    ⟨A<:⟩-traverseʳ T rec T<:U ⊥<: l eqT′                        = ⊥<:
    ⟨A<:⟩-traverseʳ T rec T<:U refl [] refl                      = bnd refl T<:U
    ⟨A<:⟩-traverseʳ T rec T<:U refl (T′ ∷ l) refl                = bnd refl (⟨A<:⟩-traverseʳ T rec T<:U refl l refl)
    ⟨A<:⟩-traverseʳ T rec T<:U (bnd T<:S₁ U₁<:U₂) [] refl        = bnd T<:S₁ (<:-trans-rec T rec U₁<:U₂ T<:U)
    ⟨A<:⟩-traverseʳ T rec T<:U (bnd S₂<:S₁ U₁<:U₂) (S′ ∷ l) refl = bnd S₂<:S₁ (⟨A<:⟩-traverseʳ T rec T<:U U₁<:U₂ l refl)
    ⟨A<:⟩-traverseʳ T rec T<:U (sel₂ T′∈Γ T′<:B) l eqT′          = sel₂ T′∈Γ (⟨A<:⟩-traverseʳ T rec T<:U T′<:B (_ ∷ l) (cong (⟨A: _ ⋯_⟩) eqT′))
    ⟨A<:⟩-traverseʳ T rec T<:U (<:> T′∈Γ T′<:B T′<:B′) l eqT′    = <:> T′∈Γ T′<:B (⟨A<:⟩-traverseʳ T rec T<:U T′<:B′ (_ ∷ l) (cong (⟨A: _ ⋯_⟩) eqT′))
    ⟨A<:⟩-traverseʳ T rec T<:U (∩-E₁ S′<:B) l eqT′               = ∩-E₁ (⟨A<:⟩-traverseʳ T rec T<:U S′<:B l eqT′)
    ⟨A<:⟩-traverseʳ T rec T<:U (∩-E₂ U′<:B) l eqT′               = ∩-E₂ (⟨A<:⟩-traverseʳ T rec T<:U U′<:B l eqT′)

    ⟨A<:⟩-traverseˡ : ∀ T →
                        (∀ T′ → Typ-measure T′ < Typ-measure T → trans-on T′ × narrow-on T′) →
                      ∀ {Γ S} →
                        Γ ⊢ S <: T →
                      ∀ {S′ T′} →
                        Γ ⊢ S′ <: T′ →
                      ∀ {U} l →
                        T′ ≡ ⟨A:⟩-layer ⟨A: T ⋯ U ⟩ l →
                        Γ ⊢ S′ <: ⟨A:⟩-layer ⟨A: S ⋯ U ⟩ l
    ⟨A<:⟩-traverseˡ T rec S<:T <:⊤ [] ()
    ⟨A<:⟩-traverseˡ T rec S<:T <:⊤ (_ ∷ l) ()
    ⟨A<:⟩-traverseˡ T rec S<:T ⊥<: l eqT′                        = ⊥<:
    ⟨A<:⟩-traverseˡ T rec S<:T refl [] refl                      = bnd S<:T refl
    ⟨A<:⟩-traverseˡ T rec S<:T refl (T′ ∷ l) refl                = bnd refl (⟨A<:⟩-traverseˡ T rec S<:T refl l refl)
    ⟨A<:⟩-traverseˡ T rec S<:T (bnd T<:S₁ U₁<:U₂) [] refl        = bnd (<:-trans-rec T rec S<:T T<:S₁) U₁<:U₂
    ⟨A<:⟩-traverseˡ T rec S<:T (bnd S₂<:S₁ U₁<:U₂) (S′ ∷ l) refl = bnd S₂<:S₁ (⟨A<:⟩-traverseˡ T rec S<:T U₁<:U₂ l refl)
    ⟨A<:⟩-traverseˡ T rec S<:T (Π<: _ _) [] ()
    ⟨A<:⟩-traverseˡ T rec S<:T (Π<: _ _) (_ ∷ l) ()
    ⟨A<:⟩-traverseˡ T rec S<:T (sel₁ _ _) [] ()
    ⟨A<:⟩-traverseˡ T rec S<:T (sel₁ _ _) (_ ∷ l) ()
    ⟨A<:⟩-traverseˡ T rec S<:T (sel₂ T′∈Γ T′<:B) l eqT′          = sel₂ T′∈Γ (⟨A<:⟩-traverseˡ T rec S<:T T′<:B (_ ∷ l) (cong ⟨A: _ ⋯_⟩ eqT′))
    ⟨A<:⟩-traverseˡ T rec S<:T (<:> T′∈Γ T′<:B T′<:B′) l eqT′    = <:> T′∈Γ T′<:B (⟨A<:⟩-traverseˡ T rec S<:T T′<:B′ (_ ∷ l) (cong ⟨A: _ ⋯_⟩ eqT′))
    ⟨A<:⟩-traverseˡ T rec S<:T (∩-E₁ S′<:B) l eqT′               = ∩-E₁ (⟨A<:⟩-traverseˡ T rec S<:T S′<:B l eqT′)
    ⟨A<:⟩-traverseˡ T rec S<:T (∩-E₂ U′<:B) l eqT′               = ∩-E₂ (⟨A<:⟩-traverseˡ T rec S<:T U′<:B l eqT′)
    ⟨A<:⟩-traverseˡ T rec S<:T (∩-I _ _) [] ()
    ⟨A<:⟩-traverseˡ T rec S<:T (∩-I _ _) (_ ∷ l) ()


  <:-narrow-on : ∀ T → (∀ T′ → Typ-measure T′ ≡ Typ-measure T → trans-on T′) → narrow-on T
  <:-narrow-on T trans <:⊤ Γ′≺Γ T∈Γ                   = <:⊤
  <:-narrow-on T trans ⊥<: Γ′≺Γ T∈Γ                   = ⊥<:
  <:-narrow-on T trans refl Γ′≺Γ T∈Γ                  = refl
  <:-narrow-on T trans (bnd S′<:S U<:U′) Γ′≺Γ T∈Γ     = bnd (<:-narrow-on T trans S′<:S Γ′≺Γ T∈Γ)
                                                            (<:-narrow-on T trans U<:U′ Γ′≺Γ T∈Γ)
  <:-narrow-on T trans {Γ} {Γ′} {n} (Π<: {S′          = S′} S′<:S U<:U′) Γ′≺Γ T∈Γ
                                                      = Π<: (<:-narrow-on T trans S′<:S Γ′≺Γ T∈Γ)
          (<:-narrow-on (T ↑ 0)
                        (λ T′ eq → trans T′ (≡.trans eq (Typ-measure-↑ T 0)))
                        U<:U′ (_ ∷ Γ′≺Γ)
                        (↦∈⇒lookup (tl {n} {T′        = S′} {Γ} (lookup⇒↦∈ T∈Γ))))
  <:-narrow-on T trans (sel₁ T′∈Γ T′<:SU) Γ′≺Γ T∈Γ
    with <:∈-find′ T′∈Γ Γ′≺Γ
  ...  | inj₁ (refl , T″ , T″∈Γ′ , T″<:T)
    rewrite just-injective (≡.trans (≡.sym T′∈Γ) T∈Γ) = sel₁ T″∈Γ′ (trans T refl T″<:T (<:-narrow-on T trans T′<:SU Γ′≺Γ T∈Γ))
  ...  | inj₂ (x≢n , T′∈Γ′)                           = sel₁ T′∈Γ′ (<:-narrow-on T trans T′<:SU Γ′≺Γ T∈Γ)
  <:-narrow-on T trans (sel₂ T′∈Γ T′<:SU) Γ′≺Γ T∈Γ
    with <:∈-find′ T′∈Γ Γ′≺Γ
  ...  | inj₁ (refl , T″ , T″∈Γ′ , T″<:T)
    rewrite just-injective (≡.trans (≡.sym T′∈Γ) T∈Γ) = sel₂ T″∈Γ′ (trans T refl T″<:T (<:-narrow-on T trans T′<:SU Γ′≺Γ T∈Γ))
  ...  | inj₂ (x≢n , T′∈Γ′)                           = sel₂ T′∈Γ′ (<:-narrow-on T trans T′<:SU Γ′≺Γ T∈Γ)
  <:-narrow-on T trans (<:> T′∈Γ T′<:SU T′<:SU′) Γ′≺Γ T∈Γ
    with <:∈-find′ T′∈Γ Γ′≺Γ
  ...  | inj₁ (refl , T″ , T″∈Γ′ , T″<:T)
    rewrite just-injective (≡.trans (≡.sym T′∈Γ) T∈Γ) = <:> T″∈Γ′
                                                            (trans T refl T″<:T (<:-narrow-on T trans T′<:SU Γ′≺Γ T∈Γ))
                                                            (trans T refl T″<:T (<:-narrow-on T trans T′<:SU′ Γ′≺Γ T∈Γ))
  ...  | inj₂ (x≢n , T′∈Γ′)                           = <:> T′∈Γ′
                                                            (<:-narrow-on T trans T′<:SU Γ′≺Γ T∈Γ)
                                                            (<:-narrow-on T trans T′<:SU′ Γ′≺Γ T∈Γ)
  <:-narrow-on T trans (∩-E₁ S′<:U) Γ′≺Γ T∈Γ          = ∩-E₁ (<:-narrow-on T trans S′<:U Γ′≺Γ T∈Γ)
  <:-narrow-on T trans (∩-E₂ U′<:U) Γ′≺Γ T∈Γ          = ∩-E₂ (<:-narrow-on T trans U′<:U Γ′≺Γ T∈Γ)
  <:-narrow-on T trans (∩-I S<:S′ S<:U′) Γ′≺Γ T∈Γ     = ∩-I (<:-narrow-on T trans S<:S′ Γ′≺Γ T∈Γ)
                                                            (<:-narrow-on T trans S<:U′ Γ′≺Γ T∈Γ)

  <:-trans-narrow : ∀ T → trans-on T × narrow-on T
  <:-trans-narrow = wfRec _ aux
    where open Measure <-wellFounded Typ-measure
          aux : ∀ T → (∀ T′ → Typ-measure T′ < Typ-measure T → trans-on T′ × narrow-on T′) → trans-on T × narrow-on T
          aux T rec = <:-trans-rec T rec
                    , <:-narrow-on T (λ T′ T′≡T →
                                        <:-trans-rec T′ λ T″ T″<T′ → rec T″ (≤-trans T″<T′ (≤-reflexive T′≡T)))

  <:-trans : ∀ {T} → trans-on T
  <:-trans {T} = proj₁ (<:-trans-narrow T)

  <:-narrow : ∀ {T} → narrow-on T
  <:-narrow {T} = proj₂ (<:-trans-narrow T)

open Transitivity using (<:-trans ; <:-narrow) public
