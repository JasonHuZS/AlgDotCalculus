{-# OPTIONS --without-K --safe #-}

module Dsubsub where

open import Data.List as List
open import Data.List.All
open import Data.Nat as ℕ
open import Data.Maybe as Maybe
open import Data.Product
open import Data.Sum
open import Data.Empty renaming (⊥ to False)
open import Data.Unit using (tt) renaming (⊤ to True)
open import Function

open import Data.Nat.Properties as ℕₚ
open import Data.Maybe.Properties as Maybeₚ
open import Relation.Nullary
open import Relation.Binary.PropositionalEquality as ≡

open import Induction.Nat

open import Utils

infix 8 _∙A
infix 6 Π_∙_ ⟨A<:_⟩

data Typ : Set where
  ⊤       : Typ
  ⊥       : Typ
  _∙A     : (n : ℕ) → Typ
  Π_∙_    : (S U : Typ) → Typ
  ⟨A<:_⟩ : (U : Typ) → Typ


Env : Set
Env = List Typ

infixl 7 _↑_
_↑_ : Typ → ℕ → Typ
⊤ ↑ n         = ⊤
⊥ ↑ n         = ⊥
(x ∙A) ↑ n with n ≤? x
... | yes n≤x = suc x ∙A
... | no n>x  = x ∙A
(Π S ∙ U) ↑ n = Π S ↑ n ∙ U ↑ suc n
⟨A<: U ⟩ ↑ n  = ⟨A<: U ↑ n ⟩

infix 4 _↦_∈_
data _↦_∈_ : ℕ → Typ → Env → Set where
  hd : ∀ {T Γ} → 0 ↦ T ↑ 0 ∈ T ∷ Γ
  tl : ∀ {n T T′ Γ} → n ↦ T ∈ Γ → suc n ↦ T ↑ 0 ∈ T′ ∷ Γ

env-lookup : Env → ℕ → Maybe Typ
env-lookup Γ n = Maybe.map (repeat (suc n) (_↑ 0)) (lookupOpt Γ n)

↦∈⇒lookup : ∀ {x T Γ} → x ↦ T ∈ Γ → env-lookup Γ x ≡ just T
↦∈⇒lookup hd = refl
↦∈⇒lookup {x} {_} {Γ} (tl T∈Γ) with lookupOpt Γ x | ↦∈⇒lookup T∈Γ
... | just T′ | eq = cong (just ∘ (_↑ zero)) (just-injective eq)
... | nothing | ()

lookup⇒↦∈ : ∀ {x T Γ} → env-lookup Γ x ≡ just T → x ↦ T ∈ Γ
lookup⇒↦∈ {x} {_} {[]} ()
lookup⇒↦∈ {zero} {_} {T ∷ Γ} refl = hd
lookup⇒↦∈ {suc x} {_} {T ∷ Γ} eq with lookupOpt Γ x | λ {T} → lookup⇒↦∈ {x} {T} {Γ}
lookup⇒↦∈ refl | just T′ | rec = tl $ rec refl
lookup⇒↦∈ ()   | nothing | _

↑-idx : ℕ → ℕ → ℕ
↑-idx x n with n ≤? x
... | yes p = suc x
... | no ¬p = x

↑-var : ∀ x n → x ∙A ↑ n ≡ ↑-idx x n ∙A
↑-var x n with n ≤? x
... | yes p = refl
... | no ¬p = refl

↑-↑-comm : ∀ T m n → m ≤ n → T ↑ m ↑ suc n ≡ T ↑ n ↑ m
↑-↑-comm ⊤ m n m≤n                                  = refl
↑-↑-comm ⊥ m n m≤n                                  = refl
↑-↑-comm (x ∙A) m n m≤n with n ≤? x
... | yes n≤x
    rewrite ≤?-yes (≤-trans m≤n n≤x)
          | ≤?-yes n≤x
          | ≤?-yes (≤-step (≤-trans m≤n n≤x))       = refl
... | no n>x with m ≤? x
...             | yes m≤x rewrite proj₂ $ ≤?-no n>x = refl
...             | no m>x with suc n ≤? x
...                         | yes 1+n≤x             = ⊥-elim (m>x (≤-trans (≤-step m≤n) 1+n≤x))
...                         | no 1+n>x              = refl
↑-↑-comm (Π S ∙ U) m n m≤n
  rewrite ↑-↑-comm S m n m≤n
        | ↑-↑-comm U (suc m) (suc n) (s≤s m≤n)      = refl
↑-↑-comm ⟨A<: T ⟩ m n m≤n
  rewrite ↑-↑-comm T m n m≤n                        = refl

infix 4 _⊢_<:_
data _⊢_<:_ : Env → Typ → Typ → Set where
  <:⊤  : ∀ {Γ T} → Γ ⊢ T <: ⊤
  ⊥<:  : ∀ {Γ T} → Γ ⊢ ⊥ <: T
  refl : ∀ {Γ T} → Γ ⊢ T <: T
  tran : ∀ {Γ S T U} → Γ ⊢ S <: T → Γ ⊢ T <: U → Γ ⊢ S <: U
  bnd  : ∀ {Γ U U′} →
           (U<:U′ : Γ ⊢ U <: U′) →
           Γ ⊢ ⟨A<: U ⟩ <: ⟨A<: U′ ⟩
  sel  : ∀ {Γ n T U} →
           (T∈Γ : env-lookup Γ n ≡ just T) →
           Γ ⊢ T <: ⟨A<: U ⟩ →
           Γ ⊢ n ∙A <: U
  Π<:  : ∀ {Γ S U S′ U′} →
           Γ ⊢ S′ <: S →
           Γ ‣ S′ ! ⊢ U <: U′ →
           Γ ⊢ Π S ∙ U <: Π S′ ∙ U′


data Covar : Typ → Set where
  cv⊤  : Covar ⊤
  cv∙A : ∀ n → Covar (n ∙A)
  cvΠ  : ∀ {S U} → Covar S → Covar U → Covar (Π ⟨A<: S ⟩ ∙ U)

data Contra : Typ → Set where
  ctt : ∀ {T} → Covar T → Contra ⟨A<: T ⟩

ContraEnv : Env → Set
ContraEnv = All Contra

↑-Covar : ∀ {T} n → Covar T → Covar (T ↑ n)
↑-Covar n cv⊤       = cv⊤
↑-Covar n (cv∙A x)
  rewrite ↑-var x n = cv∙A _
↑-Covar n (cvΠ S U) = cvΠ (↑-Covar n S) (↑-Covar (suc n) U)

↑-Contra : ∀ {T} n → Contra T → Contra (T ↑ n)
↑-Contra n (ctt T) = ctt (↑-Covar n T)

↦∈ContraEnv : ∀ {Γ n T} → n ↦ T ∈ Γ → ContraEnv Γ → Contra T
↦∈ContraEnv hd (T ∷ cT)       = ↑-Contra 0 T
↦∈ContraEnv (tl T∈Γ) (_ ∷ cT) = ↑-Contra 0 (↦∈ContraEnv T∈Γ cT)

lookupContraEnv : ∀ {Γ n T} → env-lookup Γ n ≡ just T → ContraEnv Γ → Contra T
lookupContraEnv lk cT = ↦∈ContraEnv (lookup⇒↦∈ lk) cT

lookupContraBot : ∀ {Γ n} → ContraEnv Γ → ¬ env-lookup Γ n ≡ just ⊥
lookupContraBot all eq with lookupContraEnv eq all
... | ()

lookupContraBndBot : ∀ {Γ n} → ContraEnv Γ → ¬ env-lookup Γ n ≡ just ⟨A<: ⊥ ⟩
lookupContraBndBot all eq with lookupContraEnv eq all
... | ctt ()

lookupContraBndBnd : ∀ {Γ n U} → ContraEnv Γ →
                       ¬ env-lookup Γ n ≡ just ⟨A<: ⟨A<: U ⟩ ⟩
lookupContraBndBnd all eq with lookupContraEnv eq all
... | ctt ()

Typ-measure : Typ → ℕ
Typ-measure ⊤         = 1
Typ-measure ⊥         = 1
Typ-measure (n ∙A)    = 2
Typ-measure (Π S ∙ U) = 1 + Typ-measure S + Typ-measure U
Typ-measure ⟨A<: T ⟩  = 1 + Typ-measure T

Typ-measure-↑ : ∀ T n → Typ-measure (T ↑ n) ≡ Typ-measure T
Typ-measure-↑ ⊤ n                 = refl
Typ-measure-↑ ⊥ n                 = refl
Typ-measure-↑ (x ∙A) n
  rewrite ↑-var x n               = refl
Typ-measure-↑ (Π S ∙ U) n
  rewrite Typ-measure-↑ S n
        | Typ-measure-↑ U (suc n) = refl
Typ-measure-↑ ⟨A<: T ⟩ n          = cong suc (Typ-measure-↑ T n)


module EquivalentModel where
  infix 4 _⊢′_<:_
  mutual
    data _⊢′_<:_ : Env → Typ → Typ → Set where
      <:⊤  : ∀ {Γ T} → Γ ⊢′ T <: ⊤
      ⊥<:  : ∀ {Γ T} → Γ ⊢′ ⊥ <: T
      refl : ∀ {Γ T} → Γ ⊢′ T <: T
      bnd  : ∀ {Γ U U′} →
               (U<:U′ : Γ ⊢′ U <: U′) →
               Γ ⊢′ ⟨A<: U ⟩ <: ⟨A<: U′ ⟩
      sel  : ∀ {Γ n T U U′} →
               (T∈Γ : env-lookup Γ n ≡ just T) →
               (T<: : Γ ⊢′ T <: ⟨A<: U ⟩) →
               ¬bnd T<: →
               Γ ⊢′ U <: U′ →
               Γ ⊢′ n ∙A <: U′
      Π<:  : ∀ {Γ S U S′ U′} →
               Γ ⊢′ S′ <: S →
               Γ ‣ S′ ! ⊢′ U <: U′ →
               Γ ⊢′ Π S ∙ U <: Π S′ ∙ U′

    ¬bnd : ∀ {Γ S U} → Γ ⊢′ S <: U → Set
    ¬bnd <:⊤           = True
    ¬bnd ⊥<:           = True
    ¬bnd refl          = True
    ¬bnd (bnd _)       = False
    ¬bnd (sel _ _ _ _) = True
    ¬bnd (Π<: _ _)     = True
    
  is-refl : ∀ {Γ S U} → Γ ⊢′ S <: U → Set
  is-refl <:⊤           = False
  is-refl ⊥<:           = False
  is-refl refl          = True
  is-refl (bnd _)       = False
  is-refl (sel _ _ _ _) = False
  is-refl (Π<: _ _)     = False

  is-bnd : ∀ {Γ S U} → Γ ⊢′ S <: U → Set
  is-bnd <:⊤           = False
  is-bnd ⊥<:           = False
  is-bnd refl          = False
  is-bnd (bnd _)       = True
  is-bnd (sel _ _ _ _) = False
  is-bnd (Π<: _ _)     = False

  bnd-pred : ∀ {Γ S U} (S<:U : Γ ⊢′ S <: U) → ¬ is-bnd S<:U → ¬bnd S<:U
  bnd-pred <:⊤ ¬is-bnd           = tt
  bnd-pred ⊥<: ¬is-bnd           = tt
  bnd-pred refl ¬is-bnd          = tt
  bnd-pred (bnd _) ¬is-bnd       = ¬is-bnd tt
  bnd-pred (sel _ _ _ _) ¬is-bnd = tt
  bnd-pred (Π<: _ _) ¬is-bnd     = tt

  is-bnd-dec : ∀ {Γ S U} (S<:U : Γ ⊢′ S <: U) → is-bnd S<:U ⊎ ¬bnd S<:U
  is-bnd-dec <:⊤           = inj₂ tt
  is-bnd-dec ⊥<:           = inj₂ tt
  is-bnd-dec refl          = inj₂ tt
  is-bnd-dec (bnd _)       = inj₁ tt
  is-bnd-dec (sel _ _ _ _) = inj₂ tt
  is-bnd-dec (Π<: _ _)     = inj₂ tt

  is-refl-dec : ∀ {Γ S U} (S<:U : Γ ⊢′ S <: U) → is-refl S<:U ⊎ ¬ is-refl S<:U
  is-refl-dec <:⊤                    = inj₂ id
  is-refl-dec ⊥<:                    = inj₂ id
  is-refl-dec refl                   = inj₁ tt
  is-refl-dec (bnd S<:U)             = inj₂ id
  is-refl-dec (sel T∈Γ S<:U x S<:U₁) = inj₂ id
  is-refl-dec (Π<: S<:U S<:U₁)       = inj₂ id

  infixl 7 _⇑
  _⇑ : Env → Env
  [] ⇑      = []
  (T ∷ Γ) ⇑ = T ↑ length Γ ∷ Γ ⇑

  ↦∈-weaken-≤ : ∀ {n T Γ} →
                  n ↦ T ∈ Γ →
                ∀ Γ₁ Γ₂ T′ →
                  Γ ≡ Γ₁ ‣ Γ₂ →
                  length Γ₂ ≤ n →
                  suc n ↦ T ↑ length Γ₂ ∈ Γ₁ ‣ T′ ! ‣ Γ₂ ⇑
  ↦∈-weaken-≤ hd .(_ ∷ _) [] T′ refl z≤n       = tl hd
  ↦∈-weaken-≤ hd Γ₁ (_ ∷ Γ₂) T′ eqΓ ()
  ↦∈-weaken-≤ (tl T∈Γ) .(_ ∷ _) [] T′ refl z≤n = tl (tl T∈Γ)
  ↦∈-weaken-≤ (tl {_} {T} T∈Γ) Γ₁ (_ ∷ Γ₂) T′ refl (s≤s l≤n)
    rewrite ↑-↑-comm T 0 (length Γ₂) z≤n       = tl (↦∈-weaken-≤ T∈Γ Γ₁ Γ₂ T′ refl l≤n)
  
  ↦∈-weaken-> : ∀ {n T Γ} →
                  n ↦ T ∈ Γ →
                ∀ Γ₁ Γ₂ T′ →
                  Γ ≡ Γ₁ ‣ Γ₂ →
                  length Γ₂ > n →
                  n ↦ T ↑ length Γ₂ ∈ Γ₁ ‣ T′ ! ‣ Γ₂ ⇑
  ↦∈-weaken-> T∈Γ Γ₁ [] T′ eqΓ ()
  ↦∈-weaken-> (hd {T}) Γ₁ (_ ∷ Γ₂) T′ refl (s≤s l>n)
    rewrite ↑-↑-comm T 0 (length Γ₂) l>n = hd
  ↦∈-weaken-> (tl {_} {T} T∈Γ) Γ₁ (_ ∷ Γ₂) T′ refl (s≤s l>n)
    rewrite ↑-↑-comm T 0 (length Γ₂) z≤n = tl (↦∈-weaken-> T∈Γ Γ₁ Γ₂ T′ refl l>n)

  ↦∈-weaken : ∀ {n T Γ} →
                n ↦ T ∈ Γ →
              ∀ Γ₁ Γ₂ T′ →
                Γ ≡ Γ₁ ‣ Γ₂ →
                ↑-idx n (length Γ₂) ↦ T ↑ length Γ₂ ∈ Γ₁ ‣ T′ ! ‣ Γ₂ ⇑
  ↦∈-weaken {n} T∈Γ _ Γ₂ _ eqΓ with length Γ₂ ≤? n
  ... | yes l≤n = ↦∈-weaken-≤ T∈Γ _ Γ₂ _ eqΓ l≤n
  ... | no l>n  = ↦∈-weaken-> T∈Γ _ Γ₂ _ eqΓ (≰⇒> l>n)

  ↦∈-weaken′ : ∀ {n T Γ} →
                 env-lookup Γ n ≡ just T →
               ∀ Γ₁ Γ₂ T′ →
                 Γ ≡ Γ₁ ‣ Γ₂ →
                 env-lookup (Γ₁ ‣ T′ ! ‣ Γ₂ ⇑) (↑-idx n (length Γ₂)) ≡ just (T ↑ length Γ₂)
  ↦∈-weaken′ T∈Γ Γ₁ Γ₂ T′ eqΓ = ↦∈⇒lookup (↦∈-weaken (lookup⇒↦∈ T∈Γ) Γ₁ Γ₂ T′ eqΓ)

  <:′-weakening-gen : ∀ {Γ S U} →
                        (S<:U : Γ ⊢′ S <: U) →
                      ∀ Γ₁ Γ₂ T →
                        Γ ≡ Γ₁ ‣ Γ₂ →
                        Σ (Γ₁ ‣ T ! ‣ Γ₂ ⇑ ⊢′ S ↑ length Γ₂ <: U ↑ length Γ₂) (λ S<:U′ → ¬bnd S<:U → ¬bnd S<:U′)
  <:′-weakening-gen <:⊤ Γ₁ Γ₂ T eqΓ               = <:⊤ , id
  <:′-weakening-gen ⊥<: Γ₁ Γ₂ T eqΓ               = ⊥<: , id
  <:′-weakening-gen refl Γ₁ Γ₂ T eqΓ              = refl , id
  <:′-weakening-gen (bnd S<:U) Γ₁ Γ₂ T eqΓ        = bnd (proj₁ $ <:′-weakening-gen S<:U Γ₁ Γ₂ T eqΓ) , id
  <:′-weakening-gen (sel {_} {n} T∈Γ T<:B ¬b S<:U) Γ₁ Γ₂ T eqΓ
    rewrite ↑-var n (length Γ₂) with <:′-weakening-gen T<:B Γ₁ Γ₂ T eqΓ
  ... | T<:B′ , ¬b′                               = sel (↦∈-weaken′ T∈Γ Γ₁ Γ₂ T eqΓ)
                                                        T<:B′
                                                        (¬b′ ¬b)
                                                        (proj₁ $ <:′-weakening-gen S<:U Γ₁ Γ₂ T eqΓ)
                                                  , id
  <:′-weakening-gen (Π<: S′<:S U<:U′) Γ₁ Γ₂ T eqΓ = Π<: (proj₁ $ <:′-weakening-gen S′<:S Γ₁ Γ₂ T eqΓ)
                                                        (proj₁ $ <:′-weakening-gen U<:U′ Γ₁ (_ ∷ Γ₂) T (cong (_ ∷_) eqΓ))
                                                  , id

  <:′-weakening : ∀ {Γ₁ Γ₂ S U} T →
                    Γ₁ ‣ Γ₂ ⊢′ S <: U →
                    Γ₁ ‣ T ! ‣ Γ₂ ⇑ ⊢′ S ↑ length Γ₂ <: U ↑ length Γ₂
  <:′-weakening T S<:U = proj₁ (<:′-weakening-gen S<:U _ _ T refl)

  <:′-weakening-hd : ∀ {Γ S U} T →
                       Γ ⊢′ S <: U →
                       Γ ‣ T ! ⊢′ S ↑ 0 <: U ↑ 0
  <:′-weakening-hd T = <:′-weakening {Γ₂ = []} T

  module Transitivity where    
    infix 4 _≺:[_]_
    data _≺:[_]_ : Env → ℕ → Env → Set where
      ≺[_,_] : ∀ {Γ U} S → Γ ⊢′ S <: U → Γ ‣ S ! ≺:[ 0 ] Γ ‣ U !
      _∷_    : ∀ {Γ₁ n Γ₂} T → Γ₁ ≺:[ n ] Γ₂ → Γ₁ ‣ T ! ≺:[ suc n ] Γ₂ ‣ T !

    <:∈-find : ∀ {x T Γ Γ′ n} →
                 x ↦ T ∈ Γ →
                 Γ′ ≺:[ n ] Γ →
                 x ≡ n × (∃ λ T′ → n ↦ T′ ∈ Γ′ × Γ′ ⊢′ T′ <: T) ⊎ x ≢ n × x ↦ T ∈ Γ′
    <:∈-find hd ≺[ T′ , T′<:T ]           = inj₁ (refl , T′ ↑ 0 , hd , <:′-weakening-hd T′ T′<:T)
    <:∈-find hd (T ∷ Γ′≺:Γ)               = inj₂ ((λ ()) , hd)
    <:∈-find (tl T∈Γ) ≺[ T′ , T′<:T ]     = inj₂ ((λ ()) , tl T∈Γ)
    <:∈-find (tl T∈Γ) (S ∷ Γ′≺:Γ) with <:∈-find T∈Γ Γ′≺:Γ
    ... | inj₁ (x≡n , T′ , T′∈Γ′ , T′<:T) = inj₁ (cong suc x≡n , T′ ↑ 0 , tl T′∈Γ′ , <:′-weakening-hd S T′<:T)
    ... | inj₂ (x≢n , T∈Γ′)               = inj₂ (x≢n ∘ suc-injective , tl T∈Γ′)

    <:∈-find′ : ∀ {x T Γ Γ′ n} →
                  env-lookup Γ x ≡ just T →
                  Γ′ ≺:[ n ] Γ →
                  x ≡ n × (∃ λ T′ → env-lookup Γ′ n ≡ just T′ × Γ′ ⊢′ T′ <: T) ⊎ x ≢ n × env-lookup Γ′ x ≡ just T
    <:∈-find′ T∈Γ Γ′≺Γ with <:∈-find (lookup⇒↦∈ T∈Γ) Γ′≺Γ
    ... | inj₁ (x≡n , T′ , T′∈Γ′ , T′<:T) = inj₁ (x≡n , T′ , ↦∈⇒lookup T′∈Γ′ , T′<:T)
    ... | inj₂ (x≢n , T∈Γ′)               = inj₂ (x≢n , ↦∈⇒lookup T∈Γ′)

    private
      trans-on : Typ → Set
      trans-on T = ∀ {Γ S U} → Γ ⊢′ S <: T → Γ ⊢′ T <: U → Γ ⊢′ S <: U

      narrow-on : Typ → Set
      narrow-on T = ∀ {Γ Γ′ n S U} →
                      Γ ⊢′ S <: U →
                      Γ′ ≺:[ n ] Γ →
                      env-lookup Γ n ≡ just T →
                      Γ′ ⊢′ S <: U

      trans-on-dep : Typ → Set
      trans-on-dep T = ∀ {Γ S U} →
                         (S<:T : Γ ⊢′ S <: T) →
                         (T<:U : Γ ⊢′ T <: U) →
                         Σ (Γ ⊢′ S <: U) (λ S<:U → ¬bnd T<:U → (¬ is-refl T<:U → ¬bnd S<:U) × (¬bnd S<:T → ¬bnd S<:U))

      narrow-on-dep : Typ → Set
      narrow-on-dep T = ∀ {Γ Γ′ n S U} →
                          (S<:U : Γ ⊢′ S <: U) →
                          Γ′ ≺:[ n ] Γ →
                          env-lookup Γ n ≡ just T →
                          Σ (Γ′ ⊢′ S <: U) (λ S<:U′ → (¬bnd S<:U → ¬bnd S<:U′) × (¬ is-refl S<:U → ¬ is-refl S<:U′))

      dep⇒trans-on : ∀ {T} → trans-on-dep T → trans-on T
      dep⇒trans-on f S<:T T<:U = proj₁ (f S<:T T<:U)

      dep⇒narrow-on : ∀ {T} → narrow-on-dep T → narrow-on T
      dep⇒narrow-on f S<:U Γ′≺Γ T∈Γ = proj₁ (f S<:U Γ′≺Γ T∈Γ)

    <:′-trans-rec : ∀ T → (∀ T′ → Typ-measure T′ < Typ-measure T → trans-on T′ × narrow-on T′) → trans-on-dep T
    <:′-trans-rec ⊤ rec S<:T <:⊤                                = <:⊤  , λ _ → const tt                , const tt
    <:′-trans-rec ⊤ rec S<:T refl                               = S<:T , λ _ → (λ ¬⊤ → ⊥-elim (¬⊤ tt)) , id
    <:′-trans-rec ⊥ rec ⊥<: T<:U                                = ⊥<:  , λ _ → const tt                , const tt
    <:′-trans-rec ⊥ rec refl T<:U                               = T<:U , λ z → const z                 , const z
    <:′-trans-rec ⊥ rec (sel T∈Γ T<:B ¬b S<:S′) S′<:U           = sel T∈Γ T<:B ¬b (proj₁ $ <:′-trans-rec ⊥ rec S<:S′ S′<:U)
                                                                , λ _ → const tt , const tt
    <:′-trans-rec (n ∙A) rec ⊥<: T<:U                           = ⊥<:  , λ _ → const tt , const tt
    <:′-trans-rec (n ∙A) rec refl T<:U                          = T<:U , λ z → const z  , const z
    <:′-trans-rec (n ∙A) rec (sel T∈Γ T<:B ¬b S<:S′) S′<:U      = sel T∈Γ T<:B ¬b (proj₁ $ <:′-trans-rec (n ∙A) rec S<:S′ S′<:U)
                                                                , λ _ → const tt , const tt
    <:′-trans-rec ⟨A<: T ⟩ rec ⊥<: T<:U                         = ⊥<:  , λ _ → const tt , const tt
    <:′-trans-rec ⟨A<: T ⟩ rec refl T<:U                        = T<:U , λ z → const z  , const z
    <:′-trans-rec ⟨A<: T ⟩ rec (bnd S<:T) <:⊤                   = <:⊤  , λ _ → const tt , const tt
    <:′-trans-rec ⟨A<: T ⟩ rec (bnd S<:T) refl                  = bnd S<:T , λ _ → (λ ¬⊤ → ¬⊤ tt) , id
    <:′-trans-rec ⟨A<: T ⟩ rec (bnd S<:T) (bnd T<:U)            = bnd (proj₁ $ <:′-trans-rec T (λ T′ T′<T → rec T′ (≤-step T′<T)) S<:T T<:U) , (λ ())
    <:′-trans-rec ⟨A<: T ⟩ rec (sel T∈Γ T<:B ¬b S<:S′) S′<:U    = sel T∈Γ T<:B ¬b (proj₁ $ <:′-trans-rec ⟨A<: T ⟩ rec S<:S′ S′<:U)
                                                                , λ _ → const tt , const tt
    <:′-trans-rec (Π S′ ∙ U′) rec ⊥<: T<:U                      = ⊥<:  , λ _ → const tt , const tt
    <:′-trans-rec (Π S′ ∙ U′) rec refl T<:U                     = T<:U , λ z → const z  , const z
    <:′-trans-rec (Π S′ ∙ U′) rec (sel T∈Γ T<:B ¬b S<:S″) S″<:U = sel T∈Γ T<:B ¬b (proj₁ $ <:′-trans-rec (Π S′ ∙ U′) rec S<:S″ S″<:U)
                                                                , λ _ → const tt , const tt
    <:′-trans-rec (Π S′ ∙ U′) rec (Π<: S′<:S″ U″<:U′) <:⊤       = <:⊤ , λ _ → const tt , const tt
    <:′-trans-rec (Π S′ ∙ U′) rec (Π<: S′<:S″ U″<:U′) refl      = Π<: S′<:S″ U″<:U′ , λ _ → const tt , const tt
    <:′-trans-rec (Π S′ ∙ U′) rec (Π<: S′<:S″ U″<:U′) (Π<: S‴<:S′ U′<:U‴)
      = Π<: (proj₁ $ <:′-trans-rec S′ (λ T′ T′<S′ → rec T′ (≤-step (≤-stepsʳ _ T′<S′))) S‴<:S′ S′<:S″)
            (proj₁ $ <:′-trans-rec U′ (λ T′ T′<U′ → rec T′ (≤-step (≤-stepsˡ _ T′<U′)))
                                   (proj₂ (rec (S′ ↑ 0) S′↑0<1+S′+U′) U″<:U′ ≺[ _ , S‴<:S′ ] refl)
                                   U′<:U‴)
      , λ _ → const tt , const tt
      where open ≤-Reasoning
            S′↑0<1+S′+U′ : Typ-measure (S′ ↑ 0) < suc (Typ-measure S′ + Typ-measure U′)
            S′↑0<1+S′+U′ = s≤s $ begin
              Typ-measure (S′ ↑ 0)            ≡⟨ Typ-measure-↑ S′ 0 ⟩
              Typ-measure S′                  ≤⟨ ≤-stepsʳ _ ≤-refl ⟩
              Typ-measure S′ + Typ-measure U′ ∎


    <:′-narrow-rec : ∀ T → (∀ T′ → Typ-measure T′ ≤ Typ-measure T → trans-on-dep T′) → narrow-on-dep T
    <:′-narrow-rec T trans <:⊤ Γ′≺Γ T∈Γ            = <:⊤ , id , id
    <:′-narrow-rec T trans ⊥<: Γ′≺Γ T∈Γ            = ⊥<: , id , id
    <:′-narrow-rec T trans refl Γ′≺Γ T∈Γ           = refl , id , id
    <:′-narrow-rec T trans (bnd S<:U) Γ′≺Γ T∈Γ     = bnd (proj₁ $ <:′-narrow-rec T trans S<:U Γ′≺Γ T∈Γ) , id , id
    <:′-narrow-rec T trans (sel T′∈Γ T<:B ¬b U<:U′) Γ′≺Γ T∈Γ
      with <:∈-find′ T′∈Γ Γ′≺Γ
    ...  | inj₁ (refl , T″ , T″∈Γ′ , T″<:T)
      rewrite just-injective (≡.trans (≡.sym T′∈Γ) T∈Γ)
      with T<:B        | ¬b  | is-refl-dec T<:B
    ...  | T<:B′       | ¬b′ | inj₂ ¬refl          = case <:′-narrow-rec T trans T<:B′ Γ′≺Γ T∈Γ of λ where
                                                       (T<:B″ , ⇒¬bnd₁ , ⇒¬refl) →
                                                         case trans T ≤-refl T″<:T T<:B″ of λ where
                                                           (T″<:B , ⇒¬bnd₂) →
                                                             sel T″∈Γ′
                                                                 T″<:B (proj₁ (⇒¬bnd₂ (⇒¬bnd₁ ¬b′)) (⇒¬refl ¬refl))
                                                                 (proj₁ $ <:′-narrow-rec T trans U<:U′ Γ′≺Γ T∈Γ) , id , id
    ...  | ⊥<:         | ¬b′ | inj₁ ref            = ⊥-elim ref
    ...  | bnd _       | ¬b′ | inj₁ ref            = ⊥-elim ref
    ...  | sel _ _ _ _ | ¬b′ | inj₁ ref            = ⊥-elim ref
    ...  | refl        | ¬b′ | inj₁ _
      with T″<:T       | is-bnd-dec T″<:T
    ...  | T″<:T′      | inj₂ ¬b′                  = sel T″∈Γ′ T″<:T′ ¬b′ (proj₁ $ <:′-narrow-rec T trans U<:U′ Γ′≺Γ T∈Γ) , id , id
    ...  | ⊥<:         | inj₁ bnd!                 = ⊥-elim bnd!
    ...  | refl        | inj₁ bnd!                 = ⊥-elim bnd!
    ...  | sel _ _ _ _ | inj₁ bnd!                 = ⊥-elim bnd!
    ...  | bnd {_} {_} {U} U″<:U | inj₁ bnd!       = sel T″∈Γ′
                                                         refl _
                                                         (proj₁ $ trans U (≤-step ≤-refl) U″<:U (proj₁ $ <:′-narrow-rec T trans U<:U′ Γ′≺Γ T∈Γ))
                                                   , id , id
    <:′-narrow-rec T trans (sel T′∈Γ T<:B ¬b U<:U′) Γ′≺Γ T∈Γ
       | inj₂ (x≢n , T′∈Γ′) with <:′-narrow-rec T trans T<:B Γ′≺Γ T∈Γ
    ...                        | (T<:B′ , ¬b′ , _) = sel T′∈Γ′ T<:B′ (¬b′ ¬b) (proj₁ $ <:′-narrow-rec T trans U<:U′ Γ′≺Γ T∈Γ)
                                                   , id , id
    
    <:′-narrow-rec T trans {Γ} {Γ′} {n} (Π<: {S′ = S′} S′<:S U<:U′) Γ′≺Γ T∈Γ
      = Π<: (proj₁ $ <:′-narrow-rec T trans S′<:S Γ′≺Γ T∈Γ)
            (proj₁ $ <:′-narrow-rec (T ↑ 0)
                                    (λ T′ T′≤T → trans T′ (≤-trans T′≤T (≤-reflexive (Typ-measure-↑ T 0))))
                                    U<:U′
                                    (_ ∷ Γ′≺Γ)
                                    (↦∈⇒lookup (tl {n} {T′ = S′} {Γ} (lookup⇒↦∈ T∈Γ))))
      , id , id

    <:′-trans-narrow : ∀ T → trans-on T × narrow-on T
    <:′-trans-narrow = wfRec (λ T → trans-on T × narrow-on T) aux
      where open Measure <-wellFounded Typ-measure
            aux : ∀ T →
                    (∀ T′ → Typ-measure T′ < Typ-measure T → trans-on T′ × narrow-on T′) →
                    trans-on T × narrow-on T
            aux T rec = dep⇒trans-on (<:′-trans-rec T rec)
                      , dep⇒narrow-on (<:′-narrow-rec T (λ T′ T′≤T → <:′-trans-rec T′ (λ T″ T″<T′ → rec T″ (≤-trans T″<T′ T′≤T))))

    <:′-trans : ∀ {T} → trans-on T
    <:′-trans = proj₁ (<:′-trans-narrow _)

    <:′-narrow : ∀ {T} → narrow-on T
    <:′-narrow = proj₂ (<:′-trans-narrow _)

  open Transitivity using (<:′-trans ; <:′-narrow) public
  
  <:⇒<:′ : ∀ {Γ S U} → Γ ⊢ S <: U → Γ ⊢′ S <: U
  <:⇒<:′ <:⊤                  = <:⊤
  <:⇒<:′ ⊥<:                  = ⊥<:
  <:⇒<:′ refl                 = refl
  <:⇒<:′ (tran S<:T T<:U)     = <:′-trans (<:⇒<:′ S<:T) (<:⇒<:′ T<:U)
  <:⇒<:′ (bnd S<:U)           = bnd (<:⇒<:′ S<:U)
  <:⇒<:′ (sel T∈Γ T<:B) with (<:⇒<:′ T<:B) | is-bnd-dec (<:⇒<:′ T<:B)
  ... | T<:B′       | inj₂ ¬b = sel T∈Γ T<:B′ ¬b refl
  ... | ⊥<:         | inj₁ b! = ⊥-elim b!
  ... | refl        | inj₁ b! = ⊥-elim b!
  ... | sel _ _ _ _ | inj₁ b! = ⊥-elim b!
  ... | bnd U<:U′   | inj₁ b! = sel T∈Γ refl tt U<:U′
  <:⇒<:′ (Π<: S′<:S U<:U′)    = Π<: (<:⇒<:′ S′<:S) (<:⇒<:′ U<:U′)

  <:′⇒<: : ∀ {Γ S U} → Γ ⊢′ S <: U → Γ ⊢ S <: U
  <:′⇒<: <:⊤                    = <:⊤
  <:′⇒<: ⊥<:                    = ⊥<:
  <:′⇒<: refl                   = refl
  <:′⇒<: (bnd S<:U)             = bnd (<:′⇒<: S<:U)
  <:′⇒<: (sel T∈Γ T<:B _ U<:U′) = tran (sel T∈Γ (<:′⇒<: T<:B)) (<:′⇒<: U<:U′)
  <:′⇒<: (Π<: S′<:S U<:U′)      = Π<: (<:′⇒<: S′<:S) (<:′⇒<: U<:U′)
