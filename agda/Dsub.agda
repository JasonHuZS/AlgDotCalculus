{-# OPTIONS --without-K --safe #-}

module Dsub where

open import Data.List as List
open import Data.List.All as All
open import Data.Nat as ℕ
open import Data.Maybe as Maybe
open import Data.Product
open import Data.Sum
open import Data.Empty renaming (⊥ to False)
open import Data.Vec as Vec renaming (_∷_ to _‼_ ; [] to nil) hiding (_++_)
open import Function

open import Data.Maybe.Properties as Maybeₚ
open import Data.Nat.Properties as ℕₚ
open import Relation.Nullary
open import Relation.Binary.PropositionalEquality

open import DsubDef
open import Utils

module Dsub where

  infix 4 _⊢_<:_

  data _⊢_<:_ : Env → Typ → Typ → Set where
    dtop   : ∀ {Γ T} → Γ ⊢ T <: ⊤
    dbot   : ∀ {Γ T} → Γ ⊢ ⊥ <: T
    drefl  : ∀ {Γ T} → Γ ⊢ T <: T
    dtrans : ∀ {Γ S U} T →
               (S<:T : Γ ⊢ S <: T) →
               (T<:U : Γ ⊢ T <: U) →
               Γ ⊢ S <: U
    dbnd   : ∀ {Γ S U S′ U′} →
               (S′<:S : Γ ⊢ S′ <: S) →
               (U<:U′ : Γ ⊢ U <: U′) →
               Γ ⊢ ⟨A: S ⋯ U ⟩ <: ⟨A: S′ ⋯ U′ ⟩
    dall   : ∀ {Γ S U S′ U′} →
               (S′<:S : Γ ⊢ S′ <: S) →
               (U<:U′ : (S′ ∷ Γ) ⊢ U <: U′) →
               Γ ⊢ Π S ∙ U <: Π S′ ∙ U′
    dsel₁  : ∀ {Γ n T S} →
               (T∈Γ : env-lookup Γ n ≡ just T) →
               (D : Γ ⊢ T <: ⟨A: S ⋯ ⊤ ⟩) →
               Γ ⊢ S <: n ∙A
    dsel₂  : ∀ {Γ n T U} →
               (T∈Γ : env-lookup Γ n ≡ just T) →
               (D : Γ ⊢ T <: ⟨A: ⊥ ⋯ U ⟩) →
               Γ ⊢ n ∙A <: U

  D-measure : ∀ {Γ S U} (D : Γ ⊢ S <: U) → ℕ
  D-measure dtop                 = 1
  D-measure dbot                 = 1
  D-measure drefl                = 1
  D-measure (dtrans _ S<:T T<:U) = 1 + D-measure S<:T + D-measure T<:U
  D-measure (dbnd D₁ D₂)         = 1 + D-measure D₁ + D-measure D₂
  D-measure (dall Dp Db)         = 1 + D-measure Dp + D-measure Db
  D-measure (dsel₁ T∈Γ D)        = 1 + D-measure D
  D-measure (dsel₂ T∈Γ D)        = 1 + D-measure D

  D-measure≥1 : ∀ {Γ S U} (D : Γ ⊢ S <: U) → D-measure D ≥ 1
  D-measure≥1 dtop           = s≤s z≤n
  D-measure≥1 dbot           = s≤s z≤n
  D-measure≥1 drefl          = s≤s z≤n
  D-measure≥1 (dtrans _ _ _) = s≤s z≤n
  D-measure≥1 (dbnd _ _)     = s≤s z≤n
  D-measure≥1 (dall _ _)     = s≤s z≤n
  D-measure≥1 (dsel₁ _ _)    = s≤s z≤n
  D-measure≥1 (dsel₂ _ _)    = s≤s z≤n

  D-deriv : Set
  D-deriv = Σ (Env × Typ × Typ) λ { (Γ , S , U) → Γ ⊢ S <: U }

  env : D-deriv → Env
  env ((Γ , _) , _) = Γ

  typ₁ : D-deriv → Typ
  typ₁ ((_ , S , U) , _) = S

  typ₂ : D-deriv → Typ
  typ₂ ((_ , S , U) , _) = U

  drefl-dec : ∀ (D : D-deriv) → Dec (D ≡ ((env D , typ₁ D , _) , drefl))
  drefl-dec (_ , dtop)         = no (λ ())
  drefl-dec (_ , dbot)         = no (λ ())
  drefl-dec (_ , drefl)        = yes refl
  drefl-dec (_ , dtrans _ _ _) = no (λ ())
  drefl-dec (_ , dbnd _ _)     = no (λ ())
  drefl-dec (_ , dall _ _)     = no (λ ())
  drefl-dec (_ , dsel₁ _ _)    = no (λ ())
  drefl-dec (_ , dsel₂ _ _)    = no (λ ())

open Dsub public

module DsubInvProperties {P : Env → Set}
                         (invertible : InvertibleEnv P) where

  module ContraEnvProperties = InvertibleProperties invertible _⊢_<:_
  open ContraEnvProperties public

  mutual
    <:⇒<:ᵢ : ∀ {Γ S U} → Γ ⊢ S <: U → P Γ → Γ ⊢ᵢ S <: U
    <:⇒<:ᵢ dtop pΓ                 = ditop
    <:⇒<:ᵢ dbot pΓ                 = dibot
    <:⇒<:ᵢ drefl pΓ                = direfl
    <:⇒<:ᵢ (dtrans T S<:T T<:U) pΓ = ditrans T (<:⇒<:ᵢ S<:T pΓ) (<:⇒<:ᵢ T<:U pΓ)
    <:⇒<:ᵢ (dbnd S′<:S U<:U′) pΓ   = dibnd (<:⇒<:ᵢ S′<:S pΓ) (<:⇒<:ᵢ U<:U′ pΓ)
    <:⇒<:ᵢ (dall S′<:S U<:U′) pΓ   = diall (<:⇒<:ᵢ S′<:S pΓ) U<:U′
    <:⇒<:ᵢ (dsel₁ T∈Γ T<:B) pΓ with sel-case T∈Γ T<:B pΓ
    ... | refl , _                 = dibot
    <:⇒<:ᵢ (dsel₂ T∈Γ T<:B) pΓ with sel-case T∈Γ T<:B pΓ
    ... | refl , U′ , refl
        with ⟨A:⟩<:⟨A:⟩ (<:⇒<:ᵢ T<:B pΓ) pΓ
    ...    | _ , U′<:U             = ditrans U′ (disel T∈Γ) U′<:U

    sel-case : ∀ {T Γ n S U} →
                 env-lookup Γ n ≡ just T →
                 Γ ⊢ T <: ⟨A: S ⋯ U ⟩ →
                 P Γ →
                 S ≡ ⊥ × ∃ λ U′ → T ≡ ⟨A: ⊥ ⋯ U′ ⟩
    sel-case {⊤} T∈Γ T<:B pΓ = case ⊤<: (<:⇒<:ᵢ T<:B pΓ) of λ ()
    sel-case {⊥} T∈Γ T<:B pΓ = ⊥-elim (no-⊥ pΓ T∈Γ)
    sel-case {n ∙A} T∈Γ T<:B pΓ
      with <:⟨A:⟩ (<:⇒<:ᵢ T<:B pΓ) pΓ
    ...  | inj₁ ()
    ...  | inj₂ (_ , _ , ())
    sel-case {Π S ∙ U} T∈Γ T<:B pΓ
      with <:⟨A:⟩ (<:⇒<:ᵢ T<:B pΓ) pΓ
    ...  | inj₁ ()
    ...  | inj₂ (_ , _ , ())
    sel-case {⟨A: S ⋯ U ⟩} T∈Γ T<:B pΓ
      rewrite ⊥-lb pΓ T∈Γ with ⟨A:⟩<:⟨A:⟩ (<:⇒<:ᵢ T<:B pΓ) pΓ
    ... | S<:⊥ , _ with <:⊥ S<:⊥ pΓ
    ...               | refl = refl , U , refl

  <:ᵢ⇒<: : ∀ {Γ S U} → Γ ⊢ᵢ S <: U → Γ ⊢ S <: U
  <:ᵢ⇒<: ditop                 = dtop
  <:ᵢ⇒<: dibot                 = dbot
  <:ᵢ⇒<: direfl                = drefl
  <:ᵢ⇒<: (ditrans T S<:T T<:U) = dtrans T (<:ᵢ⇒<: S<:T) (<:ᵢ⇒<: T<:U)
  <:ᵢ⇒<: (dibnd S′<:S U<:U′)   = dbnd (<:ᵢ⇒<: S′<:S) (<:ᵢ⇒<: U<:U′)
  <:ᵢ⇒<: (diall S′<:S U<:U′)   = dall (<:ᵢ⇒<: S′<:S) U<:U′
  <:ᵢ⇒<: (disel T∈Γ)           = dsel₂ T∈Γ drefl

  ⊤<:′ : ∀ {Γ T} → Γ ⊢ ⊤ <: T → P Γ → T ≡ ⊤
  ⊤<:′ ⊤<:T = ⊤<: ∘ <:⇒<:ᵢ ⊤<:T
  
  <:⊥′ : ∀ {Γ T} → Γ ⊢ T <: ⊥ → P Γ → T ≡ ⊥
  <:⊥′ T<:⊥ pΓ = <:⊥ (<:⇒<:ᵢ T<:⊥ pΓ) pΓ
  
  ⟨A:⟩<:⟨A:⟩′ : ∀ {Γ S′ U U′} → Γ ⊢ ⟨A: ⊥ ⋯ U ⟩ <: ⟨A: S′ ⋯ U′ ⟩ → P Γ →
                        S′ ≡ ⊥
  ⟨A:⟩<:⟨A:⟩′ B<:B′ pΓ with ⟨A:⟩<:⟨A:⟩ (<:⇒<:ᵢ B<:B′ pΓ) pΓ
  ... | S<:⊥ , _ = <:⊥ S<:⊥ pΓ
  
  Π<:⟨A:⟩⇒⊥ : ∀ {Γ S U S′ U′} → Γ ⊢ᵢ Π S ∙ U <: ⟨A: S′ ⋯ U′ ⟩ → P Γ → False
  Π<:⟨A:⟩⇒⊥ Π<:⟨A:⟩ eΓ with <:⟨A:⟩ Π<:⟨A:⟩ eΓ
  ... | inj₁ ()
  ... | inj₂ (_ , _ , ())
  
  ∙A<:⟨A:⟩⇒⊥ : ∀ {Γ x S U} → Γ ⊢ᵢ x ∙A <: ⟨A: S ⋯ U ⟩ → P Γ → False
  ∙A<:⟨A:⟩⇒⊥ x∙A<:⟨A:⟩ eΓ with <:⟨A:⟩ x∙A<:⟨A:⟩ eΓ
  ... | inj₁ ()
  ... | inj₂ (_ , _ , ()) 
  
