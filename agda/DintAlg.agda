{-# OPTIONS --without-K --safe #-}

-- algebraic analysis of intersection types of D∧
module DintAlg where

open import Data.List as List
open import Data.List.All
open import Data.Nat as ℕ
open import Data.Maybe as Maybe
open import Data.Product
open import Data.Sum
open import Data.Unit using (tt) renaming (⊤ to True)
open import Data.Empty renaming (⊥ to False)
open import Function

open import Data.Nat.Properties as ℕₚ
open import Data.Maybe.Properties as Maybeₚ
open import Relation.Nullary
open import Relation.Binary
open import Relation.Binary.PropositionalEquality as ≡

open import Dint
open import Utils

infix 5 _∈∩_
data _∈∩_ : Typ → Typ → Set where
  leaf  : ∀ {T} → (¬∩ : ¬ is-∩ T) → T ∈∩ T
  left  : ∀ {S U S′} → S′ ∈∩ S → S′ ∈∩ S ∩ U
  right : ∀ {S U U′} → U′ ∈∩ U → U′ ∈∩ S ∩ U

module _ {ℓ} (P : Typ → Set ℓ) where

  data All∩ : Typ → Set ℓ where
    leaf : ∀ {T} → (¬∩ : ¬ is-∩ T) → (pT : P T) → All∩ T
    node : ∀ {S U} → All∩ S → All∩ U → All∩ (S ∩ U)

  data Any∩ : Typ → Set ℓ where
    found   : ∀ {T} → (¬∩ : ¬ is-∩ T) → (pT : P T) → Any∩ T
    ∩-left  : ∀ {S U} → Any∩ S → Any∩ (S ∩ U)
    ∩-right : ∀ {S U} → Any∩ U → Any∩ (S ∩ U)

module _ {ℓ} {P : Typ → Set ℓ} where

  All∩⇒∀ : ∀ {T} → All∩ P T → ∀ T′ → T′ ∈∩ T → P T′
  All∩⇒∀ (leaf ¬∩ pT) T′ (leaf ¬∩′)      = pT
  All∩⇒∀ (leaf ¬∩ pT) T′ (left T′∈∩T)    = ⊥-elim (¬∩ tt)
  All∩⇒∀ (leaf ¬∩ pT) T′ (right T′∈∩T)   = ⊥-elim (¬∩ tt)
  All∩⇒∀ (node PS PU) .(_ ∩ _) (leaf ¬∩) = ⊥-elim (¬∩ tt)
  All∩⇒∀ (node PS PU) T′ (left T′∈∩T)    = All∩⇒∀ PS T′ T′∈∩T
  All∩⇒∀ (node PS PU) T′ (right T′∈∩T)   = All∩⇒∀ PU T′ T′∈∩T

  Any∩⇒∃ : ∀ {T} → Any∩ P T → ∃ λ T′ → T′ ∈∩ T × P T′
  Any∩⇒∃ (found ¬∩ pT) = -, leaf ¬∩ , pT
  Any∩⇒∃ (∩-left PT) with Any∩⇒∃ PT
  ... | T′ , T′∈∩S , PT′ = T′ , left T′∈∩S , PT′
  Any∩⇒∃ (∩-right PT) with Any∩⇒∃ PT
  ... | T′ , T′∈∩S , PT′ = T′ , right T′∈∩S , PT′

  ∀⇒All∩ : ∀ T → (∀ T′ → T′ ∈∩ T → P T′) → All∩ P T
  ∀⇒All∩ T all with is-∩-dec T
  ... | inj₁ (S , U , refl) = node (∀⇒All∩ S (λ T′ T′∈∩S∩U → all T′ (left T′∈∩S∩U)))
                                   (∀⇒All∩ U (λ T′ T′∈∩S∩U → all T′ (right T′∈∩S∩U)))
  ... | inj₂ ¬∩             = leaf ¬∩ (all T (leaf ¬∩))

  ∃⇒Any∩ : ∀ {T T′} → T′ ∈∩ T → P T′ → Any∩ P T
  ∃⇒Any∩ (leaf ¬∩) pT′     = found ¬∩ pT′
  ∃⇒Any∩ (left T′∈∩T) pT′  = ∩-left (∃⇒Any∩ T′∈∩T pT′)
  ∃⇒Any∩ (right T′∈∩T) pT′ = ∩-right (∃⇒Any∩ T′∈∩T pT′)

  Any∩≡⇒∈∩ : ∀ {T T′} → Any∩ (_≡ T) T′ → T ∈∩ T′
  Any∩≡⇒∈∩ (found ¬∩ refl) = leaf ¬∩
  Any∩≡⇒∈∩ (∩-left T∈∩T′)  = left (Any∩≡⇒∈∩ T∈∩T′)
  Any∩≡⇒∈∩ (∩-right T∈∩T′) = right (Any∩≡⇒∈∩ T∈∩T′)

  ∈∩≡⇒Any∩ : ∀ {T T′} → T ∈∩ T′ → Any∩ (_≡ T) T′
  ∈∩≡⇒Any∩ (leaf ¬∩)     = found ¬∩ refl
  ∈∩≡⇒Any∩ (left T∈∩T′)  = ∩-left (∈∩≡⇒Any∩ T∈∩T′)
  ∈∩≡⇒Any∩ (right T∈∩T′) = ∩-right (∈∩≡⇒Any∩ T∈∩T′)

infix 5 _≃_
data _≃_ : Typ → Typ → Set where
  refl   : ∀ {T} → T ≃ T
  tran   : ∀ {S T U} → S ≃ T → T ≃ U → S ≃ U
  symm   : ∀ {S U} → S ≃ U → U ≃ S
  assoc  : ∀ {S T U} → S ∩ (T ∩ U) ≃ S ∩ T ∩ U
  comm   : ∀ {S U} → S ∩ U ≃ U ∩ S
  idem   : ∀ {T} → T ∩ T ≃ T
  ⊤∩     : ∀ {T} → ⊤ ∩ T ≃ T
  ⊥∩     : ∀ {T} → ⊥ ∩ T ≃ ⊥
  ∩-cong : _∩_ Preserves₂ _≃_ ⟶ _≃_ ⟶ _≃_

≃-isEquivalence : IsEquivalence _≃_
≃-isEquivalence = record
  { refl  = refl
  ; sym   = symm
  ; trans = tran
  }

≃-setoid : Setoid _ _
≃-setoid = record
  { _≈_           = _≃_
  ; isEquivalence = ≃-isEquivalence
  }

open import Algebra.FunctionProperties _≃_

∩⊤ : ∀ {T} → T ∩ ⊤ ≃ T
∩⊤ = tran comm ⊤∩

∩⊥ : ∀ {T} → T ∩ ⊥ ≃ ⊥
∩⊥ = tran comm ⊥∩

⊤-ident : Identity ⊤ _∩_
⊤-ident = (λ T → ⊤∩) , λ T → tran comm ⊤∩

mutual

  ⊤-≃₁ : ∀ {S U} → S ≃ U → All∩ (_≡ ⊤) S → All∩ (_≡ ⊤) U
  ⊤-≃₁ refl ⊤≃S                          = ⊤≃S
  ⊤-≃₁ (tran S≃T T≃U) ⊤≃S                = ⊤-≃₁ T≃U (⊤-≃₁ S≃T ⊤≃S)
  ⊤-≃₁ (symm U≃S) ⊤≃S                    = ⊤-≃₂ U≃S ⊤≃S
  ⊤-≃₁ assoc (leaf ¬∩ ())
  ⊤-≃₁ assoc (node ⊤≃S (leaf ¬∩ ()))
  ⊤-≃₁ assoc (node ⊤≃S (node ⊤≃T ⊤≃U))   = node (node ⊤≃S ⊤≃T) ⊤≃U
  ⊤-≃₁ comm (leaf ¬∩ ())
  ⊤-≃₁ comm (node ⊤≃S ⊤≃U)               = node ⊤≃U ⊤≃S
  ⊤-≃₁ idem (leaf ¬∩ ())
  ⊤-≃₁ idem (node ⊤≃S _)                 = ⊤≃S
  ⊤-≃₁ ⊤∩ (leaf ¬∩ ())
  ⊤-≃₁ ⊤∩ (node _ ⊤≃U)                   = ⊤≃U
  ⊤-≃₁ ⊥∩ (leaf ¬∩ ())
  ⊤-≃₁ ⊥∩ (node (leaf ¬∩ ()) _)
  ⊤-≃₁ (∩-cong S≃S′ U≃U′) (leaf ¬∩ ())
  ⊤-≃₁ (∩-cong S≃S′ U≃U′) (node ⊤≃S ⊤≃U) = node (⊤-≃₁ S≃S′ ⊤≃S) (⊤-≃₁ U≃U′ ⊤≃U)
  
  ⊤-≃₂ : ∀ {S U} → S ≃ U → All∩ (_≡ ⊤) U → All∩ (_≡ ⊤) S
  ⊤-≃₂ refl U≃⊤                            = U≃⊤
  ⊤-≃₂ (tran S≃T T≃U) U≃⊤                  = ⊤-≃₂ S≃T (⊤-≃₂ T≃U U≃⊤)
  ⊤-≃₂ (symm U≃S) U≃⊤                      = ⊤-≃₁ U≃S U≃⊤
  ⊤-≃₂ assoc (leaf ¬∩ ())
  ⊤-≃₂ assoc (node (leaf ¬∩ ()) _)
  ⊤-≃₂ assoc (node (node S≃⊤ T≃⊤) U≃⊤)     = node S≃⊤ (node T≃⊤ U≃⊤)
  ⊤-≃₂ comm (leaf ¬∩ ())
  ⊤-≃₂ comm (node U≃⊤ S≃⊤)                 = node S≃⊤ U≃⊤
  ⊤-≃₂ idem U≃⊤                            = node U≃⊤ U≃⊤
  ⊤-≃₂ ⊤∩ U≃⊤                              = node (leaf id refl) U≃⊤
  ⊤-≃₂ ⊥∩ (leaf ¬∩ ())
  ⊤-≃₂ (∩-cong S≃S′ U≃U′) (leaf ¬∩ ())
  ⊤-≃₂ (∩-cong S≃S′ U≃U′) (node ⊤≃S′ ⊤≃U′) = node (⊤-≃₂ S≃S′ ⊤≃S′) (⊤-≃₂ U≃U′ ⊤≃U′)

⊤-≃ : ∀ {T} → ⊤ ≃ T → All∩ (_≡ ⊤) T
⊤-≃ ⊤≃T = ⊤-≃₁ ⊤≃T (leaf id refl)

⊤-≃′ : ∀ {T} → ⊤ ≃ T → ∀ {T′} → T′ ∈∩ T → T′ ≡ ⊤
⊤-≃′ T = All∩⇒∀ (⊤-≃ T) _

≃-⊤ : ∀ {T} → All∩ (_≡ ⊤) T → T ≃ ⊤
≃-⊤ (leaf ¬∩ refl)   = refl
≃-⊤ (node allS allU) = tran (∩-cong (≃-⊤ allS) (≃-⊤ allU)) idem

mutual
  ⊥-≃₁ : ∀ {S U} → S ≃ U → Any∩ (_≡ ⊥) S → Any∩ (_≡ ⊥) U
  ⊥-≃₁ refl ⊥∈∩S                         = ⊥∈∩S
  ⊥-≃₁ (tran S≃T T≃U) ⊥∈∩S               = ⊥-≃₁ T≃U (⊥-≃₁ S≃T ⊥∈∩S)
  ⊥-≃₁ (symm U≃S) ⊥∈∩S                   = ⊥-≃₂ U≃S ⊥∈∩S
  ⊥-≃₁ assoc (found ¬∩ ())
  ⊥-≃₁ assoc (∩-left ⊥∈∩S)               = ∩-left (∩-left ⊥∈∩S)
  ⊥-≃₁ assoc (∩-right (found ¬∩ ()))
  ⊥-≃₁ assoc (∩-right (∩-left ⊥∈∩T))     = ∩-left (∩-right ⊥∈∩T)
  ⊥-≃₁ assoc (∩-right (∩-right ⊥∈∩U))    = ∩-right ⊥∈∩U
  ⊥-≃₁ comm (found ¬∩ ())
  ⊥-≃₁ comm (∩-left ⊥∈∩S)                = ∩-right ⊥∈∩S
  ⊥-≃₁ comm (∩-right ⊥∈∩U)               = ∩-left ⊥∈∩U
  ⊥-≃₁ idem (found ¬∩ ())
  ⊥-≃₁ idem (∩-left ⊥∈∩S)                = ⊥∈∩S
  ⊥-≃₁ idem (∩-right ⊥∈∩S)               = ⊥∈∩S
  ⊥-≃₁ ⊤∩ (found ¬∩ ())
  ⊥-≃₁ ⊤∩ (∩-left (found ¬∩ ()))
  ⊥-≃₁ ⊤∩ (∩-right ⊥∈∩S)                 = ⊥∈∩S
  ⊥-≃₁ ⊥∩ ⊥∈∩S                           = found id refl
  ⊥-≃₁ (∩-cong S≃S′ U≃U′) (found ¬∩ ())
  ⊥-≃₁ (∩-cong S≃S′ U≃U′) (∩-left ⊥∈∩S)  = ∩-left (⊥-≃₁ S≃S′ ⊥∈∩S)
  ⊥-≃₁ (∩-cong S≃S′ U≃U′) (∩-right ⊥∈∩U) = ∩-right (⊥-≃₁ U≃U′ ⊥∈∩U)

  ⊥-≃₂ : ∀ {S U} → S ≃ U → Any∩ (_≡ ⊥) U → Any∩ (_≡ ⊥) S
  ⊥-≃₂ refl ⊥∈∩U                          = ⊥∈∩U
  ⊥-≃₂ (tran S≃T T≃U) ⊥∈∩U                = ⊥-≃₂ S≃T (⊥-≃₂ T≃U ⊥∈∩U)
  ⊥-≃₂ (symm U≃S) ⊥∈∩U                    = ⊥-≃₁ U≃S ⊥∈∩U
  ⊥-≃₂ assoc (found ¬∩ ())
  ⊥-≃₂ assoc (∩-left (found ¬∩ ()))
  ⊥-≃₂ assoc (∩-left (∩-left ⊥∈∩S))       = ∩-left ⊥∈∩S
  ⊥-≃₂ assoc (∩-left (∩-right ⊥∈∩T))      = ∩-right (∩-left ⊥∈∩T)
  ⊥-≃₂ assoc (∩-right ⊥∈∩U)               = ∩-right (∩-right ⊥∈∩U)
  ⊥-≃₂ comm (found ¬∩ ())
  ⊥-≃₂ comm (∩-left ⊥∈∩U)                 = ∩-right ⊥∈∩U
  ⊥-≃₂ comm (∩-right ⊥∈∩S)                = ∩-left ⊥∈∩S
  ⊥-≃₂ idem ⊥∈∩U                          = ∩-left ⊥∈∩U
  ⊥-≃₂ ⊤∩ ⊥∈∩U                            = ∩-right ⊥∈∩U
  ⊥-≃₂ ⊥∩ ⊥∈∩U                            = ∩-left ⊥∈∩U
  ⊥-≃₂ (∩-cong S≃S′ U≃U′) (found ¬∩ ())
  ⊥-≃₂ (∩-cong S≃S′ U≃U′) (∩-left ⊥∈∩S′)  = ∩-left (⊥-≃₂ S≃S′ ⊥∈∩S′)
  ⊥-≃₂ (∩-cong S≃S′ U≃U′) (∩-right ⊥∈∩U′) = ∩-right (⊥-≃₂ U≃U′ ⊥∈∩U′)
  
⊥-≃ : ∀ {T} → ⊥ ≃ T → Any∩ (_≡ ⊥) T
⊥-≃ ⊥≃T = ⊥-≃₁ ⊥≃T (found id refl)

⊥-≃′ : ∀ {T} → ⊥ ≃ T → ⊥ ∈∩ T
⊥-≃′ ⊥≃T with Any∩⇒∃ (⊥-≃ ⊥≃T)
... | _ , ⊥∈∩T , refl = ⊥∈∩T

≃-⊥ : ∀ {T} → ⊥ ∈∩ T → T ≃ ⊥
≃-⊥ (leaf ¬∩)    = refl
≃-⊥ (left ⊥∈∩T)  = tran (∩-cong (≃-⊥ ⊥∈∩T) refl) ⊥∩
≃-⊥ (right ⊥∈∩T) = tran (∩-cong refl (≃-⊥ ⊥∈∩T)) ∩⊥

≃⇒⊢≃ : ∀ {S U} Γ → S ≃ U → Γ ⊢ S ≃ U
≃⇒⊢≃ Γ refl                         = refl , refl
≃⇒⊢≃ Γ (tran S≃T T≃U) with ≃⇒⊢≃ Γ S≃T | ≃⇒⊢≃ Γ T≃U
... | S<:T , T<:S | T<:U , U<:T     = <:-trans S<:T T<:U , <:-trans U<:T T<:S
≃⇒⊢≃ Γ (symm U≃S) with ≃⇒⊢≃ Γ U≃S
... | U<:S , S<:U                   = S<:U , U<:S
≃⇒⊢≃ Γ assoc                        = ∩-I (∩-I (∩-E₁ refl) (∩-E₂ (∩-E₁ refl))) (∩-E₂ (∩-E₂ refl))
                                    , ∩-I (∩-E₁ (∩-E₁ refl)) (∩-I (∩-E₁ (∩-E₂ refl)) (∩-E₂ refl))
≃⇒⊢≃ Γ comm                         = ∩-I (∩-E₂ refl) (∩-E₁ refl) , ∩-I (∩-E₂ refl) (∩-E₁ refl)
≃⇒⊢≃ Γ idem                         = ∩-E₁ refl , ∩-I refl refl
≃⇒⊢≃ Γ ⊤∩                           = ∩-E₂ refl , ∩-I <:⊤ refl
≃⇒⊢≃ Γ ⊥∩                           = ∩-E₁ ⊥<: , ⊥<:
≃⇒⊢≃ Γ (∩-cong S≃S′ U≃U′) with ≃⇒⊢≃ Γ S≃S′ | ≃⇒⊢≃ Γ U≃U′
... | S<:S′ , S′<:S | U<:U′ , U′<:U = ∩-I (∩-E₁ S<:S′) (∩-E₂ U<:U′)
                                    , ∩-I (∩-E₁ S′<:S) (∩-E₂ U′<:U)

∈∩×¬∩⇒≡ : ∀ {T T′} → T ∈∩ T′ → ¬ is-∩ T′ → T′ ≡ T
∈∩×¬∩⇒≡ (leaf _) ¬∩      = refl
∈∩×¬∩⇒≡ (left T∈∩T′) ¬∩  = ⊥-elim (¬∩ tt)
∈∩×¬∩⇒≡ (right T∈∩T′) ¬∩ = ⊥-elim (¬∩ tt)
