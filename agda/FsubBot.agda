{-# OPTIONS --without-K --safe #-}

module FsubBot where

open import Data.List as List
open import Data.Nat
open import Data.Product
open import Function

open import Relation.Nullary
open import Relation.Binary.PropositionalEquality as ≡

open import Utils
open import FsubMinus

open Full

module FsubBot where

  infix 6 var_
  infixr 6 _⇒_ Π<:_∙_
  
  data Typ : Set where
    ⊤      : Typ
    ⊥      : Typ
    var_   : ℕ → Typ
    _⇒_    : Typ → Typ → Typ
    Π<:_∙_ : Typ → Typ → Typ
  
  Env′ : Set
  Env′ = List Typ
  
  infixl 7 _⇑_
  _⇑_ : Typ → ℕ → Typ
  ⊤ ⇑ n           = ⊤
  ⊥ ⇑ n           = ⊥
  (var x) ⇑ n with n ≤? x
  ... | yes p     = var (suc x)
  ... | no ¬p     = var x
  (S ⇒ U) ⇑ n     = S ⇑ n ⇒ U ⇑ n
  (Π<: S ∙ U) ⇑ n = Π<: S ⇑ n ∙ U ⇑ suc n
  
  infix 4 _<:′_∈_
  data _<:′_∈_ : ℕ → Typ → Env′ → Set where
    hd : ∀ {T Γ} → 0 <:′ T ⇑ 0 ∈ T ∷ Γ
    tl : ∀ {n T T′ Γ} → n <:′ T ∈ Γ → suc n <:′ T ⇑ 0 ∈ T′ ∷ Γ
  
  infix 4 _⊢_<:_
  
  data _⊢_<:_ : Env′ → Typ → Typ → Set where
    ftop   : ∀ {Γ T} → Γ ⊢ T <: ⊤
    fbot   : ∀ {Γ T} → Γ ⊢ ⊥ <: T
    fvrefl : ∀ {Γ n} → Γ ⊢ var n <: var n
    fbinds : ∀ {Γ n T U} →
               n <:′ T ∈ Γ →
               Γ ⊢ T <: U →
               Γ ⊢ var n <: U
    ffun   : ∀ {Γ S₁ S₂ U₁ U₂} →
               Γ ⊢ S₂ <: S₁ →
               Γ ⊢ U₁ <: U₂ →
               Γ ⊢ S₁ ⇒ U₁ <: S₂ ⇒ U₂
    fall   : ∀ {Γ S₁ S₂ U₁ U₂} →
               Γ ⊢ S₂ <: S₁ →
               S₂ ∷ Γ ⊢ U₁ <: U₂ →
               Γ ⊢ Π<: S₁ ∙ U₁ <: Π<: S₂ ∙ U₂

open FsubBot

infixl 5 _*

_* : Ftyp → Typ
⊤ *         = ⊤
var x *     = var x
S ⇒ U *     = (S *) ⇒ (U *)
Π<: S ∙ U * = Π<: S * ∙ (U *)

infix 6 ivar_
infixr 6 _i⇒_ iΠ<:_∙_
data Image : Typ → Set where
  i⊤      : Image ⊤
  ivar_   : ∀ x → Image (var x)
  _i⇒_    : ∀ {S U} → Image S → Image U → Image (S ⇒ U)
  iΠ<:_∙_ : ∀ {S U} → Image S → Image U → Image (Π<: S ∙ U)

module *-Bijectivity where
  open import Function.Bijection
  open import Function.Surjection
  open import Function.Equality
  
  infixl 5 _*⁻¹

  Typᵢ : Set
  Typᵢ = Σ Typ Image

  _*⁻¹ : Typᵢ → Ftyp
  (.⊤ , i⊤) *⁻¹                   = ⊤
  (.(var x) , ivar x) *⁻¹         = var x
  (.(_ ⇒ _) , S i⇒ U) *⁻¹         = ((-, S) *⁻¹) ⇒ ((-, U) *⁻¹)
  (.(Π<: _ ∙ _) , iΠ<: S ∙ U) *⁻¹ = Π<: (-, S) *⁻¹ ∙ ((-, U) *⁻¹)

  *-injective : ∀ {S U} → S * ≡ U * → S ≡ U
  *-injective {⊤} {⊤} eq            = refl
  *-injective {⊤} {var x} ()
  *-injective {⊤} {S ⇒ U} ()
  *-injective {⊤} {Π<: S ∙ U} ()
  *-injective {var x} {⊤} ()
  *-injective {var x} {var .x} refl = refl
  *-injective {var x} {S ⇒ U} ()
  *-injective {var x} {Π<: S ∙ U} ()
  *-injective {S ⇒ U} {⊤} ()
  *-injective {S ⇒ U} {var x} ()
  *-injective  {S₁ ⇒ U₁} {S₂ ⇒ U₂} eq
    with S₁ * | S₂ * | U₁ * | U₂ *
       | *-injective {S₁} {S₂} | *-injective {U₁} {U₂}
  *-injective  {S₁ ⇒ U₁} {S₂ ⇒ U₂} refl | _ | _ | _ | _ | rec₁ | rec₂
    rewrite rec₁ refl | rec₂ refl   = refl
  *-injective {_ ⇒ _} {Π<: _ ∙ _} ()
  *-injective {Π<: _ ∙ _} {⊤} ()
  *-injective {Π<: _ ∙ _} {var x} ()
  *-injective {Π<: _ ∙ _} {_ ⇒ _} ()
  *-injective {Π<: S₁ ∙ U₁} {Π<: S₂ ∙ U₂} eq
    with S₁ * | S₂ * | U₁ * | U₂ *
       | *-injective {S₁} {S₂} | *-injective {U₁} {U₂}
  *-injective {Π<: S₁ ∙ U₁} {Π<: S₂ ∙ U₂} refl | _ | _ | _ | _ | rec₁ | rec₂
    rewrite rec₁ refl | rec₂ refl   = refl

  *-image : ∀ T → Image (T *)
  *-image ⊤           = i⊤
  *-image (var x)     = ivar x
  *-image (S ⇒ U)     = *-image S i⇒ *-image U
  *-image (Π<: S ∙ U) = iΠ<: *-image S ∙ *-image U

  *-func : ≡.setoid Ftyp ⟶ ≡.setoid Typᵢ
  *-func    = record
    { _⟨$⟩_ = < _* , *-image >
    ; cong  = ≡.cong < _* , *-image >
    }

  *-func-inv : ≡.setoid Typᵢ ⟶ ≡.setoid Ftyp
  *-func-inv = record
    { _⟨$⟩_  = _*⁻¹
    ; cong   = ≡.cong _*⁻¹
    }

  *-func-injective : ∀ {S U} → *-func ⟨$⟩ S ≡  *-func ⟨$⟩ U → S ≡ U
  *-func-injective {S} {U} eq
    with S * | *-image S | U * | *-image U | *-injective {S} {U}
  *-func-injective {S} {U} refl | S* | Si | U* | Ui | eq = eq refl

  *-left-inverse-of-*⁻¹ : ∀ {T} (Ti : Image T) →
                            ((T , Ti) *⁻¹ * , *-image ((T , Ti) *⁻¹)) ≡ (Typᵢ ∋ (T , Ti))
  *-left-inverse-of-*⁻¹ i⊤                 = refl
  *-left-inverse-of-*⁻¹ (ivar x)           = refl
  *-left-inverse-of-*⁻¹ (S i⇒ U)
    with (-, S) *⁻¹ * | (-, U) *⁻¹ *
       | *-image $ (-, S) *⁻¹ | *-image $ (-, U) *⁻¹
       | *-left-inverse-of-*⁻¹ S | *-left-inverse-of-*⁻¹ U
  ...  | S′ | U′ | Si′ | Ui′ | refl | refl = refl
  *-left-inverse-of-*⁻¹ (iΠ<: S ∙ U)
    with (-, S) *⁻¹ * | (-, U) *⁻¹ *
       | *-image $ (-, S) *⁻¹ | *-image $ (-, U) *⁻¹
       | *-left-inverse-of-*⁻¹ S | *-left-inverse-of-*⁻¹ U
  ...  | S′ | U′ | Si′ | Ui′ | refl | refl = refl

  *-bijective : Bijective *-func
  *-bijective    = record
    { injective  = *-func-injective
    ; surjective = record
      { from             = *-func-inv
      ; right-inverse-of = λ { (_ , T) → *-left-inverse-of-*⁻¹ T }
      }
    }

*-⇑-comm : ∀ T n → (T *) ⇑ n ≡ (T ↑ n) *
*-⇑-comm ⊤ n                 = refl
*-⇑-comm (var x) n with n ≤? x
... | yes n≤x                = refl
... | no n>x                 = refl
*-⇑-comm (S ⇒ U) n
  rewrite *-⇑-comm S n
        | *-⇑-comm U n       = refl
*-⇑-comm (Π<: S ∙ U) n
  rewrite *-⇑-comm S n
        | *-⇑-comm U (suc n) = refl

<:′∈⇒<:∈ : ∀ {x T* Γ Γ*} → x <:′ T* ∈ Γ* → Γ* ≡ List.map _* Γ →
             ∃ λ T → T* ≡ T * × x <: T ∈ Γ
<:′∈⇒<:∈ {Γ = []} hd ()
<:′∈⇒<:∈ {Γ = T ∷ Γ} hd refl
  rewrite *-⇑-comm T 0 = T ↑ 0 , refl , hd
<:′∈⇒<:∈ {Γ = []} (tl T*∈Γ*) ()
<:′∈⇒<:∈ {Γ = T′ ∷ Γ} (tl T*∈Γ*) refl with <:′∈⇒<:∈ T*∈Γ* refl
... | T , refl , T∈Γ
  rewrite *-⇑-comm T 0 = T ↑ zero , refl , tl T∈Γ

bot⇒full-gen : ∀ {Γ* S* U*} → Γ* ⊢ S* <: U* →
               ∀ {Γ S U} →
                 Γ* ≡ List.map _* Γ → S* ≡ S * → U* ≡ U * →
                 Γ ⊢F S <: U
bot⇒full-gen ftop {Γ} {S} {⊤} eqΓ eqS refl             = ftop
bot⇒full-gen ftop {Γ} {S} {var x} eqΓ eqS ()
bot⇒full-gen ftop {Γ} {S} {U ⇒ U₁} eqΓ eqS ()
bot⇒full-gen ftop {Γ} {S} {Π<: U ∙ U₁} eqΓ eqS ()
bot⇒full-gen fbot {Γ} {⊤} {U} eqΓ () eqU
bot⇒full-gen fbot {Γ} {var x} {U} eqΓ () eqU
bot⇒full-gen fbot {Γ} {S ⇒ S′} {U} eqΓ () eqU
bot⇒full-gen fbot {Γ} {Π<: S ∙ S′} {U} eqΓ () eqU
bot⇒full-gen fvrefl {Γ} {⊤} {⊤} eqΓ () eqU
bot⇒full-gen fvrefl {Γ} {⊤} {var x} eqΓ () eqU
bot⇒full-gen fvrefl {Γ} {⊤} {U ⇒ U′} eqΓ () eqU
bot⇒full-gen fvrefl {Γ} {⊤} {Π<: U ∙ U′} eqΓ () eqU
bot⇒full-gen fvrefl {Γ} {var x} {⊤} eqΓ refl ()
bot⇒full-gen fvrefl {Γ} {var x} {var .x} eqΓ refl refl = fvrefl
bot⇒full-gen fvrefl {Γ} {var x} {U ⇒ U′} eqΓ refl ()
bot⇒full-gen fvrefl {Γ} {var x} {Π<: U ∙ U′} eqΓ refl ()
bot⇒full-gen fvrefl {Γ} {S ⇒ S′} {⊤} eqΓ () eqU
bot⇒full-gen fvrefl {Γ} {S ⇒ S′} {var x} eqΓ () eqU
bot⇒full-gen fvrefl {Γ} {S ⇒ S′} {U ⇒ U′} eqΓ () eqU
bot⇒full-gen fvrefl {Γ} {S ⇒ S′} {Π<: U ∙ U′} eqΓ () eqU
bot⇒full-gen fvrefl {Γ} {Π<: S ∙ S′} {⊤} eqΓ () eqU
bot⇒full-gen fvrefl {Γ} {Π<: S ∙ S′} {var x} eqΓ () eqU
bot⇒full-gen fvrefl {Γ} {Π<: S ∙ S′} {U ⇒ U′} eqΓ () eqU
bot⇒full-gen fvrefl {Γ} {Π<: S ∙ S′} {Π<: U ∙ U′} eqΓ () eqU
bot⇒full-gen (fbinds S*∈Γ* S*<:U*) {Γ} {⊤} {U} eqΓ () eqU
bot⇒full-gen (fbinds S*∈Γ* S*<:U*) {Γ} {var x} {U} eqΓ refl eqU
  with <:′∈⇒<:∈ S*∈Γ* eqΓ
... | S , refl , S∈Γ = fbinds S∈Γ (bot⇒full-gen S*<:U* eqΓ refl eqU)
bot⇒full-gen (fbinds S*∈Γ* S*<:U*) {Γ} {S ⇒ S′} {U} eqΓ () eqU
bot⇒full-gen (fbinds S*∈Γ* S*<:U*) {Γ} {Π<: S ∙ S′} {U} eqΓ () eqU
bot⇒full-gen (ffun U*<:S* S′*<:U′*) {Γ} {⊤} {⊤} eqΓ () eqU
bot⇒full-gen (ffun U*<:S* S′*<:U′*) {Γ} {⊤} {var x} eqΓ () eqU
bot⇒full-gen (ffun U*<:S* S′*<:U′*) {Γ} {⊤} {U ⇒ U′} eqΓ () eqU
bot⇒full-gen (ffun U*<:S* S′*<:U′*) {Γ} {⊤} {Π<: U ∙ U′} eqΓ () eqU
bot⇒full-gen (ffun U*<:S* S′*<:U′*) {Γ} {var x} {⊤} eqΓ () eqU
bot⇒full-gen (ffun U*<:S* S′*<:U′*) {Γ} {var x} {var x₁} eqΓ () eqU
bot⇒full-gen (ffun U*<:S* S′*<:U′*) {Γ} {var x} {U ⇒ U′} eqΓ () eqU
bot⇒full-gen (ffun U*<:S* S′*<:U′*) {Γ} {var x} {Π<: U ∙ U′} eqΓ () eqU
bot⇒full-gen (ffun U*<:S* S′*<:U′*) {Γ} {S ⇒ S′} {⊤} eqΓ refl ()
bot⇒full-gen (ffun U*<:S* S′*<:U′*) {Γ} {S ⇒ S′} {var x} eqΓ refl ()
bot⇒full-gen (ffun U*<:S* S′*<:U′*) {Γ} {S ⇒ S′} {U ⇒ U′} eqΓ refl refl
  = ffun (bot⇒full-gen U*<:S* eqΓ refl refl)
         (bot⇒full-gen S′*<:U′* eqΓ refl refl)
bot⇒full-gen (ffun U*<:S* S′*<:U′*) {Γ} {S ⇒ S′} {Π<: U ∙ U′} eqΓ refl ()
bot⇒full-gen (ffun U*<:S* S′*<:U′*) {Γ} {Π<: S ∙ S′} {⊤} eqΓ () eqU
bot⇒full-gen (ffun U*<:S* S′*<:U′*) {Γ} {Π<: S ∙ S′} {var x} eqΓ () eqU
bot⇒full-gen (ffun U*<:S* S′*<:U′*) {Γ} {Π<: S ∙ S′} {U ⇒ U′} eqΓ () eqU
bot⇒full-gen (ffun U*<:S* S′*<:U′*) {Γ} {Π<: S ∙ S′} {Π<: U ∙ U′} eqΓ () eqU
bot⇒full-gen (fall U*<:S* S′*<:U′*) {Γ} {⊤} {⊤} eqΓ () eqU
bot⇒full-gen (fall U*<:S* S′*<:U′*) {Γ} {⊤} {var x} eqΓ () eqU
bot⇒full-gen (fall U*<:S* S′*<:U′*) {Γ} {⊤} {U ⇒ U′} eqΓ () eqU
bot⇒full-gen (fall U*<:S* S′*<:U′*) {Γ} {⊤} {Π<: U ∙ U′} eqΓ () eqU
bot⇒full-gen (fall U*<:S* S′*<:U′*) {Γ} {var x} {⊤} eqΓ () eqU
bot⇒full-gen (fall U*<:S* S′*<:U′*) {Γ} {var x} {var x₁} eqΓ () eqU
bot⇒full-gen (fall U*<:S* S′*<:U′*) {Γ} {var x} {U ⇒ U′} eqΓ () eqU
bot⇒full-gen (fall U*<:S* S′*<:U′*) {Γ} {var x} {Π<: U ∙ U′} eqΓ () eqU
bot⇒full-gen (fall U*<:S* S′*<:U′*) {Γ} {S ⇒ S′} {⊤} eqΓ () eqU
bot⇒full-gen (fall U*<:S* S′*<:U′*) {Γ} {S ⇒ S′} {var x} eqΓ () eqU
bot⇒full-gen (fall U*<:S* S′*<:U′*) {Γ} {S ⇒ S′} {U ⇒ U′} eqΓ () eqU
bot⇒full-gen (fall U*<:S* S′*<:U′*) {Γ} {S ⇒ S′} {Π<: U ∙ U′} eqΓ () eqU
bot⇒full-gen (fall U*<:S* S′*<:U′*) {Γ} {Π<: S ∙ S′} {⊤} eqΓ refl ()
bot⇒full-gen (fall U*<:S* S′*<:U′*) {Γ} {Π<: S ∙ S′} {var x} eqΓ refl ()
bot⇒full-gen (fall U*<:S* S′*<:U′*) {Γ} {Π<: S ∙ S′} {U ⇒ U′} eqΓ refl ()
bot⇒full-gen (fall U*<:S* S′*<:U′*) {Γ} {Π<: S ∙ S′} {Π<: U ∙ U′} eqΓ refl refl
  = fall (bot⇒full-gen U*<:S* eqΓ refl refl)
         (bot⇒full-gen S′*<:U′* (cong (U * ∷_) eqΓ) refl refl)

bot⇒full : ∀ {Γ S U} → List.map _* Γ ⊢ S * <: U * → Γ ⊢F S <: U
bot⇒full S*<:U* = bot⇒full-gen S*<:U* refl refl refl

<:∈⇒<:′∈ : ∀ {x T Γ} → x <: T ∈ Γ → x <:′ T * ∈ List.map _* Γ
<:∈⇒<:′∈ (hd {T})
  rewrite sym $ *-⇑-comm T 0 = hd
<:∈⇒<:′∈ (tl {_} {T} T∈Γ)
  rewrite sym $ *-⇑-comm T 0 = tl (<:∈⇒<:′∈ T∈Γ)

full⇒bot : ∀ {Γ S U} →
             Γ ⊢F S <: U →
             List.map _* Γ ⊢ S * <: U *
full⇒bot ftop               = ftop
full⇒bot fvrefl             = fvrefl
full⇒bot (fbinds S∈Γ S<:U)  = fbinds (<:∈⇒<:′∈ S∈Γ) (full⇒bot S<:U)
full⇒bot (ffun U<:S S′<:U′) = ffun (full⇒bot U<:S) (full⇒bot S′<:U′)
full⇒bot (fall U<:S S′<:U′) = fall (full⇒bot U<:S) (full⇒bot S′<:U′)             
