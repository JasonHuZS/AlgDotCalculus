{-# OPTIONS --without-K --safe #-}

module DintUndec where

open import Data.List as List
open import Data.List.All
open import Data.Nat as ℕ
open import Data.Maybe as Maybe
open import Data.Product
open import Data.Sum
open import Data.Unit using (tt) renaming (⊤ to True)
open import Data.Empty renaming (⊥ to False)
open import Function

open import Data.Nat.Properties as ℕₚ
open import Data.Maybe.Properties as Maybeₚ
open import Relation.Nullary
open import Relation.Binary.PropositionalEquality as ≡

open import Dint
open import DintAlg
open import Utils

A : TLabel
A = record { idx = 0 }

data Covar : Typ → Set where
  cv⊤  : Covar ⊤
  cv∙A : ∀ n → Covar (n ∙ A)
  cvΠ  : ∀ {S U} → Covar S → Covar U → Covar (Π ⟨ A ∶ ⊥ ⋯ S ⟩ ∙ U)

data Contra : Typ → Set where
  ctt : ∀ {T} → Covar T → Contra ⟨ A ∶ ⊥ ⋯ T ⟩

ContraEnv : Env → Set
ContraEnv = All Contra

↑-Covar : ∀ {T} n → Covar T → Covar (T ↑ n)
↑-Covar n cv⊤         = cv⊤
↑-Covar n (cv∙A x)
  rewrite ↑-var x n A = cv∙A _
↑-Covar n (cvΠ S U)   = cvΠ (↑-Covar n S) (↑-Covar (suc n) U)

↑-Contra : ∀ {T} n → Contra T → Contra (T ↑ n)
↑-Contra n (ctt T) = ctt (↑-Covar n T)

↦∈ContraEnv : ∀ {Γ n T} → n ↦ T ∈ Γ → ContraEnv Γ → Contra T
↦∈ContraEnv hd (T ∷ cT)       = ↑-Contra 0 T
↦∈ContraEnv (tl T∈Γ) (_ ∷ cT) = ↑-Contra 0 (↦∈ContraEnv T∈Γ cT)

lookupContraEnv : ∀ {Γ n T} → env-lookup Γ n ≡ just T → ContraEnv Γ → Contra T
lookupContraEnv lk cT = ↦∈ContraEnv (lookup⇒↦∈ lk) cT

lookupContraBot : ∀ {Γ n} → ContraEnv Γ → ¬ env-lookup Γ n ≡ just ⊥
lookupContraBot all eq with lookupContraEnv eq all
... | ()

lookupContraBndBot : ∀ {Γ n A S U} → ContraEnv Γ → env-lookup Γ n ≡ just ⟨ A ∶ S ⋯ U ⟩ → ¬ ⊥ ∈∩ U
lookupContraBndBot all eq ⊥∈∩U      with lookupContraEnv eq all
lookupContraBndBot all eq (leaf ¬∩)    | ctt ()
lookupContraBndBot all eq (left ⊥∈∩U)  | ctt ()
lookupContraBndBot all eq (right ⊥∈∩U) | ctt ()

lookupContraBndBnd : ∀ {Γ n T T′ S U A B} → ContraEnv Γ →
                       env-lookup Γ n ≡ just ⟨ A ∶ T ⋯ T′ ⟩ → ¬ ⟨ B ∶ S ⋯ U ⟩ ∈∩ T′
lookupContraBndBnd all eq SU∈∩T′      with lookupContraEnv eq all
lookupContraBndBnd all eq (leaf ¬∩)      | ctt ()
lookupContraBndBnd all eq (left SU∈∩T′)  | ctt ()
lookupContraBndBnd all eq (right SU∈∩T′) | ctt ()

lookupContra⊥Lb : ∀ {Γ n S U B} → ContraEnv Γ →
                    env-lookup Γ n ≡ just ⟨ B ∶ S ⋯ U ⟩ → B ≡ A × S ≡ ⊥
lookupContra⊥Lb all eq with lookupContraEnv eq all
... | ctt _ = refl , refl

lookupContra∩ : ∀ {Γ n S U} → ContraEnv Γ → ¬ env-lookup Γ n ≡ just (S ∩ U)
lookupContra∩ all eq with lookupContraEnv eq all
... | ()

record InvertibleEnv (P : Env → Set) : Set where
  field
    no-⊥       : ∀ {Γ n} → P Γ → ¬ env-lookup Γ n ≡ just ⊥
    no-bnd-⊥   : ∀ {Γ n A S U} → P Γ → env-lookup Γ n ≡ just ⟨ A ∶ S ⋯ U ⟩ → ¬ ⊥ ∈∩ U
    no-bnd-bnd : ∀ {Γ n T T′ S U A B} → P Γ → env-lookup Γ n ≡ just ⟨ A ∶ T ⋯ T′ ⟩ → ¬ ⟨ B ∶ S ⋯ U ⟩ ∈∩ T′
    ⊥-lb       : ∀ {Γ n S U B} → P Γ → env-lookup Γ n ≡ just ⟨ B ∶ S ⋯ U ⟩ → B ≡ A × S ≡ ⊥
    no-∩       : ∀ {Γ n S U} → P Γ → ¬ env-lookup Γ n ≡ just (S ∩ U)

  no-bnd-⊥⊎bnd : ∀ {Γ n A S U B} → P Γ → env-lookup Γ n ≡ just ⟨ A ∶ S ⋯ U ⟩ → ¬ All∩ (λ T → T ≡ ⊥ ⊎ ∃₂ λ S U → T ≡ ⟨ B ∶ S ⋯ U ⟩) U
  no-bnd-⊥⊎bnd {U = U} pΓ SU∈Γ all = pf ev₁ ev₂ all
    where ev₁ : ¬ ⊥ ∈∩ U
          ev₁ = no-bnd-⊥ pΓ SU∈Γ
          ev₂ : ∀ {B T T′} → ¬ ⟨ B ∶ T ⋯ T′ ⟩ ∈∩ U
          ev₂ = no-bnd-bnd pΓ SU∈Γ
          pf : ∀ {B U} → ¬ ⊥ ∈∩ U → (∀ {B T T′} → ¬ ⟨ B ∶ T ⋯ T′ ⟩ ∈∩ U) → ¬ All∩ (λ T → T ≡ ⊥ ⊎ ∃₂ λ S U → T ≡ ⟨ B ∶ S ⋯ U ⟩) U
          pf ev₁ ev₂ (leaf ¬∩ (inj₁ refl))           = ev₁ (leaf id)
          pf ev₁ ev₂ (leaf ¬∩ (inj₂ (_ , _ , refl))) = ev₂ (leaf id)
          pf ev₁ ev₂ (node all _)                    = pf (λ ⊥∈∩S → ev₁ (left ⊥∈∩S))
                                                          (λ B∈∩S → ev₂ (left B∈∩S))
                                                          all

contraInvertible : InvertibleEnv ContraEnv
contraInvertible = record
  { no-⊥       = lookupContraBot
  ; no-bnd-⊥   = lookupContraBndBot
  ; no-bnd-bnd = lookupContraBndBnd
  ; ⊥-lb       = lookupContra⊥Lb
  ; no-∩       = lookupContra∩
  }


-- invertibility with intersection type is much harder
-- there needs to be a definition of setoid of types which is defined on top of equivalence
-- up to the algebraic properties of intersection types.
--
-- this further emphasizes the point that studies of types need to be based on algebraic theories.

module InvertibleProperties {P : Env → Set}
                            (invertible : InvertibleEnv P)
                            (_⊢_<:_ : Env → Typ → Typ → Set) where
  open InvertibleEnv invertible public
  
  infix 4 _⊢ᵢ_<:_
  data _⊢ᵢ_<:_ : Env → Typ → Typ → Set where
    ditop   : ∀ {Γ T} → Γ ⊢ᵢ T <: ⊤
    dibot   : ∀ {Γ T} → Γ ⊢ᵢ ⊥ <: T
    direfl  : ∀ {Γ T} → Γ ⊢ᵢ T <: T
    ditrans : ∀ {Γ S U} T →
                (S<:T : Γ ⊢ᵢ S <: T) →
                (T<:U : Γ ⊢ᵢ T <: U) →
                Γ ⊢ᵢ S <: U
    dibnd   : ∀ {Γ S U S′ U′ A} →
                (S′<:S : Γ ⊢ᵢ S′ <: S) →
                (U<:U′ : Γ ⊢ᵢ U <: U′) →
                Γ ⊢ᵢ ⟨ A ∶ S ⋯ U ⟩ <: ⟨ A ∶ S′ ⋯ U′ ⟩
    difld   : ∀ {Γ T T′ a} →
                (T<:T′ : Γ ⊢ᵢ T <: T′) →
                Γ ⊢ᵢ ⟨ a ∶ T ⟩ <: ⟨ a ∶ T′ ⟩
    diall   : ∀ {Γ S U S′ U′} →
                (S′<:S : Γ ⊢ᵢ S′ <: S) →
                (U<:U′ : (Γ ‣ S′ !) ⊢ U <: U′) →
                Γ ⊢ᵢ Π S ∙ U <: Π S′ ∙ U′
    disel   : ∀ {Γ n U A} →
                (T∈Γ : env-lookup Γ n ≡ just ⟨ A ∶ ⊥ ⋯ U ⟩) →
                Γ ⊢ᵢ n ∙ A <: U
    di∩E₁   : ∀ {Γ S U} →
                Γ ⊢ᵢ S ∩ U <: S
    di∩E₂   : ∀ {Γ S U} →
                Γ ⊢ᵢ S ∩ U <: U
    di∩I    : ∀ {Γ T S U} →
                Γ ⊢ᵢ T <: S → Γ ⊢ᵢ T <: U →
                Γ ⊢ᵢ T <: S ∩ U

  size : ∀ {Γ S U} → Γ ⊢ᵢ S <: U → ℕ
  size ditop                 = 1
  size dibot                 = 1
  size direfl                = 1
  size (ditrans T S<:T T<:U) = 1 + size S<:T + size T<:U
  size (dibnd S′<:S U<:U′)   = 1 + size S′<:S + size U<:U′
  size (difld T<:T′)         = 1 + size T<:T′
  size (diall S′<:S U<:U′)   = 1 + size S′<:S
  size (disel T∈Γ)           = 1
  size di∩E₁                 = 1
  size di∩E₂                 = 1
  size (di∩I T<:S T<:U)      = 1 + size T<:S + size T<:U

  module _ where

    ⊤<:-gen : ∀ {Γ S U} → Γ ⊢ᵢ S <: U → All∩ (_≡ ⊤) S → All∩ (_≡ ⊤) U
    ⊤<:-gen ditop ⊤≃S = leaf id refl
    ⊤<:-gen dibot (leaf ¬∩ ())
    ⊤<:-gen direfl ⊤≃S = ⊤≃S
    ⊤<:-gen (ditrans T S<:T T<:U) ⊤≃S with ⊤<:-gen S<:T ⊤≃S
    ... | ⊤≃T = ⊤<:-gen T<:U ⊤≃T
    ⊤<:-gen (dibnd _ _) (leaf ¬∩ ())
    ⊤<:-gen (difld _) (leaf ¬∩ ())
    ⊤<:-gen (diall _ _) (leaf ¬∩ ())
    ⊤<:-gen (disel T∈Γ) (leaf ¬∩ ())
    ⊤<:-gen di∩E₁ (leaf ¬∩ ())
    ⊤<:-gen di∩E₁ (node ⊤≃S _) = ⊤≃S
    ⊤<:-gen di∩E₂ (leaf ¬∩ ())
    ⊤<:-gen di∩E₂ (node _ ⊤≃S) = ⊤≃S
    ⊤<:-gen (di∩I T<:S T<:U) ⊤≃S = node (⊤<:-gen T<:S ⊤≃S) (⊤<:-gen T<:U ⊤≃S)

    ⊤<: : ∀ {Γ T} → Γ ⊢ᵢ ⊤ <: T → All∩ (_≡ ⊤) T
    ⊤<: ⊤<:T = ⊤<:-gen ⊤<:T (leaf id refl)

    <:⊥-gen : ∀ {Γ S U} → Γ ⊢ᵢ S <: U → ⊥ ∈∩ U → P Γ → ⊥ ∈∩ S
    <:⊥-gen ditop () pΓ
    <:⊥-gen dibot ⊥∈∩U pΓ                    = leaf id
    <:⊥-gen direfl ⊥∈∩U pΓ                   = ⊥∈∩U
    <:⊥-gen (ditrans T S<:T T<:U) ⊥∈∩U pΓ with <:⊥-gen T<:U ⊥∈∩U pΓ
    ... | ⊥∈∩T                               = <:⊥-gen S<:T ⊥∈∩T pΓ
    <:⊥-gen (dibnd S<:U S<:U₁) () pΓ
    <:⊥-gen (difld S<:U) () pΓ
    <:⊥-gen (diall S<:U U<:U′) () pΓ
    <:⊥-gen (disel T∈Γ) ⊥∈∩U pΓ              = ⊥-elim (no-bnd-⊥ pΓ T∈Γ ⊥∈∩U)
    <:⊥-gen di∩E₁ ⊥∈∩S pΓ                    = left ⊥∈∩S
    <:⊥-gen di∩E₂ ⊥∈∩U pΓ                    = right ⊥∈∩U
    <:⊥-gen (di∩I T<:S T<:U) (left ⊥∈∩S) pΓ  = <:⊥-gen T<:S ⊥∈∩S pΓ
    <:⊥-gen (di∩I T<:S T<:U) (right ⊥∈∩U) pΓ = <:⊥-gen T<:U ⊥∈∩U pΓ
    
    <:⊥ : ∀ {Γ T} → Γ ⊢ᵢ T <: ⊥ → P Γ → ⊥ ∈∩ T
    <:⊥ T<:⊥ pΓ = <:⊥-gen T<:⊥ (leaf id) pΓ

    S<:U×¬⊥∈∩U⇒¬⊥∈∩S : ∀ {Γ S U} → Γ ⊢ᵢ S <: U → P Γ → ¬ ⊥ ∈∩ S → ¬ ⊥ ∈∩ U
    S<:U×¬⊥∈∩U⇒¬⊥∈∩S S<:U pΓ ¬⊥∈∩S ⊥∈∩U = ¬⊥∈∩S (<:⊥-gen S<:U ⊥∈∩U pΓ)

    ⟨A∶⟩<:-gen : ∀ {Γ T T′ A} →
                   Γ ⊢ᵢ T <: T′ →
                   All∩ (λ T′ → T′ ≡ ⊤ ⊎ ∃₂ (λ S′ U′ → T′ ≡ ⟨ A ∶ S′ ⋯ U′ ⟩)) T →
                   All∩ (λ T′ → T′ ≡ ⊤ ⊎ ∃₂ (λ S′ U′ → T′ ≡ ⟨ A ∶ S′ ⋯ U′ ⟩)) T′
    ⟨A∶⟩<:-gen ditop all                                           = leaf id (inj₁ refl)
    ⟨A∶⟩<:-gen dibot (leaf ¬∩ (inj₁ ()))
    ⟨A∶⟩<:-gen dibot (leaf ¬∩ (inj₂ (_ , _ , ())))
    ⟨A∶⟩<:-gen direfl all                                          = all
    ⟨A∶⟩<:-gen (ditrans T S<:T T<:U) all                           = ⟨A∶⟩<:-gen T<:U (⟨A∶⟩<:-gen S<:T all)
    ⟨A∶⟩<:-gen (dibnd S′<:S U<:U′) (leaf ¬∩ (inj₁ ()))
    ⟨A∶⟩<:-gen (dibnd S′<:S U<:U′) (leaf ¬∩ (inj₂ (S , U , refl))) = leaf id (inj₂ (-, -, refl))
    ⟨A∶⟩<:-gen (difld _) (leaf ¬∩ (inj₁ ()))
    ⟨A∶⟩<:-gen (difld _) (leaf ¬∩ (inj₂ (_ , _ , ())))
    ⟨A∶⟩<:-gen (diall _ _) (leaf ¬∩ (inj₁ ()))
    ⟨A∶⟩<:-gen (diall _ _) (leaf ¬∩ (inj₂ (_ , _ , ())))
    ⟨A∶⟩<:-gen (disel T∈Γ) (leaf ¬∩ (inj₁ ()))
    ⟨A∶⟩<:-gen (disel T∈Γ) (leaf ¬∩ (inj₂ (_ , _ , ())))
    ⟨A∶⟩<:-gen di∩E₁ (leaf ¬∩ (inj₁ ()))
    ⟨A∶⟩<:-gen di∩E₁ (leaf ¬∩ (inj₂ (_ , _ , ())))
    ⟨A∶⟩<:-gen di∩E₁ (node all _)                                  = all
    ⟨A∶⟩<:-gen di∩E₂ (leaf ¬∩ (inj₁ ()))
    ⟨A∶⟩<:-gen di∩E₂ (leaf ¬∩ (inj₂ (_ , _ , ())))
    ⟨A∶⟩<:-gen di∩E₂ (node _ all)                                  = all
    ⟨A∶⟩<:-gen (di∩I T<:S T<:U) all                                = node (⟨A∶⟩<:-gen T<:S all) (⟨A∶⟩<:-gen T<:U all)
    
    ⟨A∶⟩<: : ∀ {Γ T S U A} → Γ ⊢ᵢ ⟨ A ∶ S ⋯ U ⟩ <: T → All∩ (λ T′ → T′ ≡ ⊤ ⊎ ∃₂ (λ S′ U′ → T′ ≡ ⟨ A ∶ S′ ⋯ U′ ⟩)) T
    ⟨A∶⟩<: SU<:T = ⟨A∶⟩<:-gen SU<:T (leaf id (inj₂ (-, -, refl)))

    <:⟨A∶⟩-gen : ∀ {Γ T T′ A} →
                   Γ ⊢ᵢ T <: T′ →
                   (⊥ ∈∩ T′ ⊎ ∃₂ λ S′ U′ → ⟨ A ∶ S′ ⋯ U′ ⟩ ∈∩ T′) → 
                   P Γ →
                   ⊥ ∈∩ T ⊎ ∃₂ λ S′ U′ → ⟨ A ∶ S′ ⋯ U′ ⟩ ∈∩ T
    <:⟨A∶⟩-gen ditop (inj₁ ()) pΓ
    <:⟨A∶⟩-gen ditop (inj₂ ()) pΓ
    <:⟨A∶⟩-gen dibot any pΓ                                    = inj₁ (leaf id)
    <:⟨A∶⟩-gen direfl any pΓ                                   = any
    <:⟨A∶⟩-gen (ditrans T S<:T T<:U) any pΓ                    = <:⟨A∶⟩-gen S<:T (<:⟨A∶⟩-gen T<:U any pΓ) pΓ
    <:⟨A∶⟩-gen (dibnd S′<:S U<:U′) (inj₁ ()) pΓ
    <:⟨A∶⟩-gen (dibnd S′<:S U<:U′) (inj₂ (_ , _ , leaf ¬∩)) pΓ = inj₂ (-, -, leaf id)
    <:⟨A∶⟩-gen (difld _) (inj₁ ()) pΓ
    <:⟨A∶⟩-gen (difld _) (inj₂ ()) pΓ
    <:⟨A∶⟩-gen (diall _ _) (inj₁ ()) pΓ
    <:⟨A∶⟩-gen (diall _ _) (inj₂ ()) pΓ
    <:⟨A∶⟩-gen (disel T∈Γ) (inj₁ ⊥∈∩T′) pΓ                     = ⊥-elim (no-bnd-⊥ pΓ T∈Γ ⊥∈∩T′)
    <:⟨A∶⟩-gen (disel T∈Γ) (inj₂ (_ , _ , B∈∩T′)) pΓ           = ⊥-elim (no-bnd-bnd pΓ T∈Γ B∈∩T′)
    <:⟨A∶⟩-gen di∩E₁ (inj₁ ⊥∈∩T′) pΓ                           = inj₁ (left ⊥∈∩T′)
    <:⟨A∶⟩-gen di∩E₁ (inj₂ (_ , _ , B∈∩T′)) pΓ                 = inj₂ (-, -, left B∈∩T′)
    <:⟨A∶⟩-gen di∩E₂ (inj₁ ⊥∈∩T′) pΓ                           = inj₁ (right ⊥∈∩T′)
    <:⟨A∶⟩-gen di∩E₂ (inj₂ (_ , _ , B∈∩T′)) pΓ                 = inj₂ (-, -, right B∈∩T′)
    <:⟨A∶⟩-gen (di∩I T<:S T<:U) (inj₁ (left ⊥∈∩S)) pΓ          = <:⟨A∶⟩-gen T<:S (inj₁ ⊥∈∩S) pΓ
    <:⟨A∶⟩-gen (di∩I T<:S T<:U) (inj₁ (right ⊥∈∩U)) pΓ         = <:⟨A∶⟩-gen T<:U (inj₁ ⊥∈∩U) pΓ
    <:⟨A∶⟩-gen (di∩I T<:S T<:U) (inj₂ (_ , _ , left B∈∩S)) pΓ  = <:⟨A∶⟩-gen T<:S (inj₂ (-, -, B∈∩S)) pΓ
    <:⟨A∶⟩-gen (di∩I T<:S T<:U) (inj₂ (_ , _ , right B∈∩U)) pΓ = <:⟨A∶⟩-gen T<:U (inj₂ (-, -, B∈∩U)) pΓ    

    <:⟨A∶⟩ : ∀ {Γ T S U A} → Γ ⊢ᵢ T <: ⟨ A ∶ S ⋯ U ⟩ → P Γ →
               ⊥ ∈∩ T ⊎ ∃₂ λ S′ U′ → ⟨ A ∶ S′ ⋯ U′ ⟩ ∈∩ T
    <:⟨A∶⟩ T<:SU pΓ = <:⟨A∶⟩-gen T<:SU (inj₂ (-, -, leaf id)) pΓ

    leaf⟨A∶⟩-injective : ∀ {A B S S′ U U′} → ⟨ A ∶ S ⋯ U ⟩ ∈∩ ⟨ B ∶ S′ ⋯ U′ ⟩ → A ≡ B × S ≡ S′ × U ≡ U′
    leaf⟨A∶⟩-injective (leaf _) = refl , refl , refl

    <:⟨A∶⟩-size-gen : ∀ {Γ T T′ A S′ U′} (D : Γ ⊢ᵢ T <: T′) →
                        ⟨ A ∶ S′ ⋯ U′ ⟩ ∈∩ T′ →
                        ¬ ⊥ ∈∩ T →
                        P Γ →
                        ∃₂ λ S U → ⟨ A ∶ S ⋯ U ⟩ ∈∩ T ×
                                   (Σ (Γ ⊢ᵢ S′ <: S) λ D′ → size D′ ≤ size D) ×
                                   (Σ (Γ ⊢ᵢ U <: U′) λ D′ → size D′ ≤ size D)
    <:⟨A∶⟩-size-gen ditop () ¬⊥∈∩T pΓ
    <:⟨A∶⟩-size-gen dibot B′∈∩T′ ¬⊥∈∩T pΓ                  = ⊥-elim (¬⊥∈∩T (leaf id))
    <:⟨A∶⟩-size-gen direfl B′∈∩T′ ¬⊥∈∩T pΓ                 = -, -, B′∈∩T′ , (direfl , s≤s z≤n) , direfl , s≤s z≤n
    <:⟨A∶⟩-size-gen (ditrans T″ T<:T″ T″<:T′) B′∈∩T′ ¬⊥∈∩T pΓ
      with <:⟨A∶⟩-size-gen T″<:T′ B′∈∩T′ (S<:U×¬⊥∈∩U⇒¬⊥∈∩S T<:T″ pΓ ¬⊥∈∩T) pΓ
    ...  | S″ , U″ , B″∈∩T″ , (S′<:S″ , sz≤₁) , U″<:U′ , sz≤₂
         with <:⟨A∶⟩-size-gen T<:T″ B″∈∩T″ ¬⊥∈∩T pΓ
    ...     | S , U , B∈∩T , (S″<:S , sz≤₃) , U<:U″ , sz≤₄ = -, -, B∈∩T
                                                           , (ditrans _ S′<:S″ S″<:S , s≤s szpf)
                                                           , ditrans _ U<:U″ U″<:U′ , s≤s (+-mono-≤ sz≤₄ sz≤₂)
      where open ≤-Reasoning
            szpf : size S′<:S″ + size S″<:S ≤ size T<:T″ + size T″<:T′
            szpf = begin
              size S′<:S″ + size S″<:S ≤⟨ +-mono-≤ sz≤₁ sz≤₃ ⟩
              size T″<:T′ + size T<:T″ ≡⟨ +-comm (size T″<:T′) _ ⟩
              size T<:T″ + size T″<:T′ ∎
    <:⟨A∶⟩-size-gen (dibnd S′<:S U<:U′) (leaf ¬∩) ¬⊥∈∩T pΓ = -, -, leaf id
                                                           , (S′<:S , ≤-step (≤-stepsʳ _ ≤-refl))
                                                           , U<:U′ , ≤-step (≤-stepsˡ _ ≤-refl)
    <:⟨A∶⟩-size-gen (difld _) () ¬⊥∈∩T pΓ
    <:⟨A∶⟩-size-gen (diall _ _) () ¬⊥∈∩T pΓ
    <:⟨A∶⟩-size-gen (disel T∈Γ) B′∈∩T′ ¬⊥∈∩T pΓ            = ⊥-elim (no-bnd-bnd pΓ T∈Γ B′∈∩T′)
    <:⟨A∶⟩-size-gen di∩E₁ B′∈∩T′ ¬⊥∈∩T pΓ                  = -, -, left B′∈∩T′
                                                           , (direfl , s≤s z≤n)
                                                           , direfl , s≤s z≤n
    <:⟨A∶⟩-size-gen di∩E₂ B′∈∩T′ ¬⊥∈∩T pΓ                  = -, -, right B′∈∩T′
                                                           , (direfl , s≤s z≤n)
                                                           , direfl , s≤s z≤n
    <:⟨A∶⟩-size-gen (di∩I T<:S T<:U) (left B′∈∩S) ¬⊥∈∩T pΓ
      with <:⟨A∶⟩-size-gen T<:S B′∈∩S ¬⊥∈∩T pΓ
    ...  | S , U , B∈∩T , (S′<:S , sz≤₁) , U<:U′ , sz≤₂    = -, -, B∈∩T
                                                           , (S′<:S , ≤-step (≤-stepsʳ _ sz≤₁))
                                                           , U<:U′ , ≤-step (≤-stepsʳ _ sz≤₂)
    <:⟨A∶⟩-size-gen (di∩I T<:S T<:U) (right B′∈∩U) ¬⊥∈∩T pΓ
      with <:⟨A∶⟩-size-gen T<:U B′∈∩U ¬⊥∈∩T pΓ
    ...  | S , U , B∈∩T , (S′<:S , sz≤₁) , U<:U′ , sz≤₂    = -, -, B∈∩T
                                                           , (S′<:S , ≤-step (≤-stepsˡ _ sz≤₁))
                                                           , U<:U′ , ≤-step (≤-stepsˡ _ sz≤₂)

    ⟨A∶⟩<:⟨A∶⟩ : ∀ {Γ S S′ U U′ A B} →
                   Γ ⊢ᵢ ⟨ A ∶ S ⋯ U ⟩ <: ⟨ B ∶ S′ ⋯ U′ ⟩ → P Γ →
                   A ≡ B × Γ ⊢ᵢ S′ <: S × Γ ⊢ᵢ U <: U′
    ⟨A∶⟩<:⟨A∶⟩ B<:B′ pΓ with <:⟨A∶⟩-size-gen B<:B′ (leaf id) (λ ()) pΓ
    ... | _ , _ , B∈∩B , (S′<:S , _) , U<:U′ , _
        with leaf⟨A∶⟩-injective B∈∩B
    ...    | refl , refl , refl = refl , S′<:S , U<:U′

    ≃⊥<: : ∀ {S} Γ U → ⊥ ∈∩ S → Γ ⊢ᵢ S <: U
    ≃⊥<: Γ U (leaf ¬∩)    = dibot
    ≃⊥<: Γ U (left ⊥∈∩S)  = ditrans _ di∩E₁ (≃⊥<: Γ U ⊥∈∩S)
    ≃⊥<: Γ U (right ⊥∈∩S) = ditrans _ di∩E₂ (≃⊥<: Γ U ⊥∈∩S)

    ≃⊥<:′ : ∀ {S} Γ U → S ≃ ⊥ → Γ ⊢ᵢ S <: U
    ≃⊥<:′ Γ U S≃⊥ = ≃⊥<: Γ U (⊥-≃′ (symm S≃⊥))

module DintInvProperties {P : Env → Set}
                         (invertible : InvertibleEnv P) where

  module ContraEnvProperties = InvertibleProperties invertible _⊢_<:_
  open ContraEnvProperties public

  no-∩′ : ∀ {Γ n T} → P Γ → env-lookup Γ n ≡ just T → ¬ is-∩ T
  no-∩′ {T = T} pΓ T∈Γ is∩ with is-∩-dec T
  ... | inj₁ (_ , _ , refl) = no-∩ pΓ T∈Γ
  ... | inj₂ ¬∩             = ¬∩ is∩

  sel-case : ∀ {T Γ n A S U} →
               env-lookup Γ n ≡ just T →
               ¬ is-∩ T → 
               Γ ⊢ᵢ T <: ⟨ A ∶ S ⋯ U ⟩ →
               P Γ →
               S ≃ ⊥ × ∃ λ U′ → T ≡ ⟨ A ∶ ⊥ ⋯ U′ ⟩
  sel-case {⊤} T∈Γ ¬∩ T<:B pΓ     = case ⊤<: T<:B of λ where (leaf ¬∩ ())
  sel-case {⊥} T∈Γ ¬∩ T<:B pΓ     = ⊥-elim (no-⊥ pΓ T∈Γ)
  sel-case {n ∙ A} T∈Γ ¬∩ T<:B pΓ
    with <:⟨A∶⟩ T<:B pΓ
  ...  | inj₁ ()
  ...  | inj₂ ()
  sel-case {Π S ∙ U} T∈Γ ¬∩ T<:B pΓ
    with <:⟨A∶⟩ T<:B pΓ
  ...  | inj₁ ()
  ...  | inj₂ ()
  sel-case {⟨ A ∶ S ⋯ U ⟩} T∈Γ ¬∩ T<:B pΓ
    rewrite proj₂ $ ⊥-lb pΓ T∈Γ
    with ⟨A∶⟩<:⟨A∶⟩ T<:B pΓ
  ...  | refl , S<:⊥ , _          = ≃-⊥ (<:⊥ S<:⊥ pΓ) , -, refl
  sel-case {⟨ a ∶ T ⟩} T∈Γ ¬∩ T<:B pΓ
    with <:⟨A∶⟩ T<:B pΓ
  ...  | inj₁ ()
  ...  | inj₂ ()
  sel-case {S ∩ U} T∈Γ ¬∩ T<:B pΓ = ⊥-elim (no-∩ pΓ T∈Γ)

  <:⇒<:ᵢ : ∀ {Γ S U} → Γ ⊢ S <: U → P Γ → Γ ⊢ᵢ S <: U
  <:⇒<:ᵢ <:⊤ pΓ               = ditop
  <:⇒<:ᵢ ⊥<: pΓ               = dibot
  <:⇒<:ᵢ refl pΓ              = direfl
  <:⇒<:ᵢ (bnd S′<:S U<:U′) pΓ = dibnd (<:⇒<:ᵢ S′<:S pΓ) (<:⇒<:ᵢ U<:U′ pΓ)
  <:⇒<:ᵢ (fld T<:T′) pΓ       = difld (<:⇒<:ᵢ T<:T′ pΓ)
  <:⇒<:ᵢ (Π<: S′<:S U<:U′) pΓ = diall (<:⇒<:ᵢ S′<:S pΓ) U<:U′
  <:⇒<:ᵢ (sel₁ _ T∈Γ T<:SU) pΓ with sel-case T∈Γ (no-∩′ pΓ T∈Γ) (<:⇒<:ᵢ T<:SU pΓ) pΓ
  ... | S≃⊥ , _               = ≃⊥<:′ _ _ S≃⊥
  <:⇒<:ᵢ (sel₂ _ T∈Γ T<:SU) pΓ with sel-case T∈Γ (no-∩′ pΓ T∈Γ) (<:⇒<:ᵢ T<:SU pΓ) pΓ
  ... | S≃⊥ , U′ , refl
      with ⟨A∶⟩<:⟨A∶⟩ (<:⇒<:ᵢ T<:SU pΓ) pΓ
  ...    | _ , _ , U′<:U      = ditrans U′ (disel T∈Γ) U′<:U
  <:⇒<:ᵢ (<:> _ T∈Γ T<:SU T<:SU′) pΓ with sel-case T∈Γ (no-∩′ pΓ T∈Γ) (<:⇒<:ᵢ T<:SU pΓ) pΓ
  ... | S≃⊥ , _               = ≃⊥<:′ _ _ S≃⊥
  <:⇒<:ᵢ (∩-E₁ S<:U) pΓ       = ditrans _ di∩E₁ (<:⇒<:ᵢ S<:U pΓ)
  <:⇒<:ᵢ (∩-E₂ S<:U) pΓ       = ditrans _ di∩E₂ (<:⇒<:ᵢ S<:U pΓ)
  <:⇒<:ᵢ (∩-I T<:S T<:U) pΓ   = di∩I (<:⇒<:ᵢ T<:S pΓ) (<:⇒<:ᵢ T<:U pΓ)

  <:ᵢ⇒<: : ∀ {Γ S U} → Γ ⊢ᵢ S <: U → Γ ⊢ S <: U
  <:ᵢ⇒<: ditop                 = <:⊤
  <:ᵢ⇒<: dibot                 = ⊥<:
  <:ᵢ⇒<: direfl                = refl
  <:ᵢ⇒<: (ditrans T S<:T T<:U) = <:-trans (<:ᵢ⇒<: S<:T) (<:ᵢ⇒<: T<:U)
  <:ᵢ⇒<: (dibnd S′<:S U<:U′)   = bnd (<:ᵢ⇒<: S′<:S) (<:ᵢ⇒<: U<:U′)
  <:ᵢ⇒<: (difld T<:T′)         = fld (<:ᵢ⇒<: T<:T′)
  <:ᵢ⇒<: (diall S′<:S U<:U′)   = Π<: (<:ᵢ⇒<: S′<:S) U<:U′
  <:ᵢ⇒<: (disel T∈Γ)           = sel₂ _ T∈Γ refl
  <:ᵢ⇒<: di∩E₁                 = ∩-E₁ refl
  <:ᵢ⇒<: di∩E₂                 = ∩-E₂ refl
  <:ᵢ⇒<: (di∩I T<:S T<:U)      = ∩-I (<:ᵢ⇒<: T<:S) (<:ᵢ⇒<: T<:U)

  ⊤<:′ : ∀ {Γ T} → Γ ⊢ ⊤ <: T → P Γ → All∩ (_≡ ⊤) T
  ⊤<:′ ⊤<:T = ⊤<: ∘ <:⇒<:ᵢ ⊤<:T
  
  <:⊥′ : ∀ {Γ T} → Γ ⊢ T <: ⊥ → P Γ → ⊥ ∈∩ T
  <:⊥′ T<:⊥ pΓ = <:⊥ (<:⇒<:ᵢ T<:⊥ pΓ) pΓ
  
  ⟨A∶⟩<:⟨A∶⟩′ : ∀ {Γ S′ U U′ A B} → Γ ⊢ ⟨ A ∶ ⊥ ⋯ U ⟩ <: ⟨ B ∶ S′ ⋯ U′ ⟩ → P Γ →
                  S′ ≃ ⊥
  ⟨A∶⟩<:⟨A∶⟩′ B<:B′ pΓ with ⟨A∶⟩<:⟨A∶⟩ (<:⇒<:ᵢ B<:B′ pΓ) pΓ
  ... | _ , S<:⊥ , _ = ≃-⊥ (<:⊥ S<:⊥ pΓ)
  
  Π<:⟨A∶⟩⇒⊥ : ∀ {Γ S U S′ U′} → Γ ⊢ᵢ Π S ∙ U <: ⟨ A ∶ S′ ⋯ U′ ⟩ → P Γ → False
  Π<:⟨A∶⟩⇒⊥ Π<:⟨A∶⟩ eΓ with <:⟨A∶⟩ Π<:⟨A∶⟩ eΓ
  ... | inj₁ ()
  ... | inj₂ ()
  
  ∙A<:⟨A∶⟩⇒⊥ : ∀ {Γ x S U} → Γ ⊢ᵢ x ∙ A <: ⟨ A ∶ S ⋯ U ⟩ → P Γ → False
  ∙A<:⟨A∶⟩⇒⊥ x∙A<:⟨A∶⟩ eΓ with <:⟨A∶⟩ x∙A<:⟨A∶⟩ eΓ
  ... | inj₁ ()
  ... | inj₂ ()

open import FsubMinus
open import FsubMinus2 using (<:-refl)

open FsubMinus.FsubMinus renaming (Env to Env′ ; _↑_ to _⇑_ ; Π<:_∙_ to Π<:′_∙_) hiding (env-lookup)

infix 5 ⟦_⟧ ⟪_⟫
⟦_⟧ : Ftyp → Typ
⟦ ⊤ ⟧          = ⊤
⟦ var x ⟧      = x ∙ A
⟦ Π<:′ S ∙ U ⟧ = Π ⟨ A ∶ ⊥ ⋯ ⟦ S ⟧ ⟩ ∙ ⟦ U ⟧

⟪_⟫ : Env′ → Env
⟪ [] ⟫    = []
⟪ T ∷ Γ ⟫ = ⟨ A ∶ ⊥ ⋯ ⟦ T ⟧ ⟩ ∷ ⟪ Γ ⟫


module ⟦⟧-Bijective where
  open import Function.Bijection
  open import Function.Surjection
  open import Function.Equality

  CovTyp : Set
  CovTyp = ∃ λ T → Covar T

  ⟦⟧-covar : ∀ T → Covar ⟦ T ⟧
  ⟦⟧-covar ⊤           = cv⊤
  ⟦⟧-covar (var x)     = cv∙A _
  ⟦⟧-covar (Π<:′ S ∙ U) = cvΠ (⟦⟧-covar S) (⟦⟧-covar U)

  ⟦⟧-func : ≡.setoid Ftyp ⟶ ≡.setoid CovTyp
  ⟦⟧-func   = record
    { _⟨$⟩_ = < ⟦_⟧ , ⟦⟧-covar >
    ; cong  = ≡.cong < ⟦_⟧ , ⟦⟧-covar >
    }

  ⟦⟧-injective : ∀ {S U} → ⟦ S ⟧ ≡ ⟦ U ⟧ → S ≡ U
  ⟦⟧-injective {⊤} {⊤} eq            = refl
  ⟦⟧-injective {⊤} {var x} ()
  ⟦⟧-injective {⊤} {Π<:′ _ ∙ _} ()
  ⟦⟧-injective {var x} {⊤} ()
  ⟦⟧-injective {var x} {var .x} refl = refl
  ⟦⟧-injective {var x} {Π<:′ _ ∙ _} ()
  ⟦⟧-injective {Π<:′ _ ∙ _} {⊤} ()
  ⟦⟧-injective {Π<:′ _ ∙ _} {var x} ()
  ⟦⟧-injective {Π<:′ S₁ ∙ U₁} {Π<:′ S₂ ∙ U₂} eq
    with ⟦ S₁ ⟧ | ⟦ S₂ ⟧ | ⟦ U₁ ⟧ | ⟦ U₂ ⟧
       | ⟦⟧-injective {S₁} {S₂} | ⟦⟧-injective {U₁} {U₂}
  ⟦⟧-injective {Π<:′ S₁ ∙ U₁} {Π<:′ S₂ ∙ U₂} refl
       | _ | _ | _ | _ | rec₁ | rec₂ = ≡.cong₂ Π<:′_∙_ (rec₁ refl) (rec₂ refl)

  ⟦⟧-func-injective : ∀ {S U} → (CovTyp ∋ (⟦ S ⟧ , ⟦⟧-covar S)) ≡ (⟦ U ⟧ , ⟦⟧-covar U) → S ≡ U
  ⟦⟧-func-injective {S} {U} eq
    with ⟦ S ⟧ | ⟦⟧-covar S | ⟦ U ⟧ | ⟦⟧-covar U
       | ⟦⟧-injective {S} {U}
  ⟦⟧-func-injective {S} {U} refl
       | _ | _ | _ | _ | inj = inj refl

  infix 5 ⟦_⟧⁻¹
  ⟦_⟧⁻¹ : CovTyp → Ftyp
  ⟦ _ , cv⊤ ⟧⁻¹     = ⊤
  ⟦ _ , cv∙A x ⟧⁻¹  = var x
  ⟦ _ , cvΠ S U ⟧⁻¹ = Π<:′ ⟦ -, S ⟧⁻¹ ∙ ⟦ -, U ⟧⁻¹

  ⟦⟧-func-inv : ≡.setoid CovTyp ⟶ ≡.setoid Ftyp
  ⟦⟧-func-inv = record
    { _⟨$⟩_   = ⟦_⟧⁻¹
    ; cong    = ≡.cong ⟦_⟧⁻¹
    }

  ⟦⟧-left-inverse-⟦⟧⁻¹ : ∀ {T} (cT : Covar T) → (⟦ ⟦ -, cT ⟧⁻¹ ⟧ , ⟦⟧-covar ⟦ -, cT ⟧⁻¹) ≡ (CovTyp ∋ (-, cT))
  ⟦⟧-left-inverse-⟦⟧⁻¹ cv⊤           = refl
  ⟦⟧-left-inverse-⟦⟧⁻¹ (cv∙A n)      = refl
  ⟦⟧-left-inverse-⟦⟧⁻¹ (cvΠ S U)
    with ⟦ ⟦ -, S ⟧⁻¹ ⟧         | ⟦ ⟦ -, U ⟧⁻¹ ⟧ 
       | ⟦⟧-covar ⟦ -, S ⟧⁻¹    | ⟦⟧-covar ⟦ -, U ⟧⁻¹
       | ⟦⟧-left-inverse-⟦⟧⁻¹ S | ⟦⟧-left-inverse-⟦⟧⁻¹ U
  ...  | _ | _ | _ | _ | refl | refl = refl

  ⟦⟧-bijective : Bijective ⟦⟧-func
  ⟦⟧-bijective   = record
    { injective  = ⟦⟧-func-injective
    ; surjective = record
      { from             = ⟦⟧-func-inv
      ; right-inverse-of = λ { (_ , T) → ⟦⟧-left-inverse-⟦⟧⁻¹ T }
      }
    }

open ⟦⟧-Bijective using (⟦⟧-covar ; ⟦⟧-injective) public

⟪⟫-contraEnv : ∀ Γ → ContraEnv ⟪ Γ ⟫
⟪⟫-contraEnv []      = []
⟪⟫-contraEnv (T ∷ Γ) = ctt (⟦⟧-covar T) ∷ ⟪⟫-contraEnv Γ

⟦⟧-↑-comm : ∀ T n → ⟦ T ⟧ ↑ n ≡ ⟦ T ⇑ n ⟧
⟦⟧-↑-comm ⊤ n = refl
⟦⟧-↑-comm (var x) n with n ≤? x
... | yes n≤x = refl
... | no n>x  = refl
⟦⟧-↑-comm (Π<:′ S ∙ U) n
  rewrite ⟦⟧-↑-comm S n | ⟦⟧-↑-comm U (suc n)
              = refl

<:∈⇒env-lookup : ∀ {n T Γ} → n <: T ∈ Γ → env-lookup ⟪ Γ ⟫ n ≡ just ⟨ A ∶ ⊥ ⋯ ⟦ T ⟧ ⟩
<:∈⇒env-lookup {_} {_} {T ∷ ._} hd
  rewrite ⟦⟧-↑-comm T 0       = refl
<:∈⇒env-lookup {suc n} {_} {._ ∷ Γ} (tl {_} {T} T∈Γ)
  with lookupOpt ⟪ Γ ⟫ n | <:∈⇒env-lookup T∈Γ
...  | nothing           | ()
...  | just _            | eq
  rewrite sym $ ⟦⟧-↑-comm T 0 = cong (just ∘ (_↑ 0)) (just-injective eq)

env-lookup⇒<:∈ : ∀ {n T Γ} → env-lookup Γ n ≡ just ⟨ A ∶ ⊥ ⋯ T ⟩ →
                 ∀ {Γ′} → Γ ≡ ⟪ Γ′ ⟫ →
                 ∃ λ T′ → T ≡ ⟦ T′ ⟧ × n <: T′ ∈ Γ′
env-lookup⇒<:∈ T∈Γ eqΓ = aux (lookup⇒↦∈ T∈Γ) refl eqΓ
  where aux : ∀ {n T Γ} → n ↦ T ∈ Γ →
                 ∀ {U Γ′} → T ≡ ⟨ A ∶ ⊥ ⋯ U ⟩ → Γ ≡ ⟪ Γ′ ⟫ →
                 ∃ λ U′ → U ≡ ⟦ U′ ⟧ × n <: U′ ∈ Γ′
        aux hd {_} {[]} eqT ()
        aux hd {_} {T ∷ Γ′} refl refl
          rewrite ⟦⟧-↑-comm T 0 = T ⇑ zero , refl , hd
        aux (tl T∈Γ) {_} {[]} eqT ()
        aux (tl {T = ⊤} T∈Γ) {_} {_ ∷ Γ′} () refl
        aux (tl {T = ⊥} T∈Γ) {_} {_ ∷ Γ′} () refl
        aux (tl {T = n ∙ B} T∈Γ) {_} {_ ∷ Γ′} () refl
        aux (tl {T = Π S ∙ U} T∈Γ) {_} {_ ∷ Γ′} () refl
        aux (tl {T = ⟨ B ∶ ⊤ ⋯ U ⟩} T∈Γ) {_} {_ ∷ Γ′} () refl
        aux (tl {T = ⟨ B ∶ ⊥ ⋯ U ⟩} T∈Γ) {_} {_ ∷ Γ′} refl refl
          with aux T∈Γ refl refl
        ...  | U′ , refl , U′∈Γ′
          rewrite ⟦⟧-↑-comm U′ 0 = U′ ⇑ zero , refl , tl U′∈Γ′
        aux (tl {T = ⟨ B ∶ _ ∙ C ⋯ U ⟩} T∈Γ) {_} {_ ∷ Γ′} () refl
        aux (tl {T = ⟨ B ∶ Π _ ∙ _ ⋯ U ⟩} T∈Γ) {_} {_ ∷ Γ′} () refl
        aux (tl {T = ⟨ B ∶ ⟨ C ∶ _ ⋯ _ ⟩ ⋯ U ⟩} T∈Γ) {_} {_ ∷ Γ′} () refl
        aux (tl {T = ⟨ B ∶ ⟨ C ∶ T ⟩ ⋯ U ⟩} T∈Γ) {_} {Γ} () eqΓ
        aux (tl {T = ⟨ B ∶ S ∩ T ⋯ U ⟩} T∈Γ) {_} {Γ} () eqΓ
        aux (tl {T = ⟨ a ∶ T ⟩} T∈Γ) {_} {Γ} () eqΓ
        aux (tl {T = S ∩ U} T∈Γ) {_} {Γ} () eqΓ

covar⇒¬∩ : ∀ {T} → Covar T → ¬ is-∩ T
covar⇒¬∩ cv⊤ is∩       = is∩
covar⇒¬∩ (cv∙A n) is∩  = is∩
covar⇒¬∩ (cvΠ S U) is∩ = is∩

open DintInvProperties contraInvertible

mutual

  private
    helper : ∀ {Γ′ A B T′ S U′} → Γ′ ⊢ ⟨ A ∶ ⊥ ⋯ T′ ⟩ <: ⟨ B ∶ S ⋯ U′ ⟩ → ∀ {Γ T U} → Γ′ ≡ ⟪ Γ ⟫ → T′ ≡ ⟦ T ⟧ → U′ ≡ ⟦ U ⟧ → Γ ⊢F T <: U
    helper refl eqΓ eqT eqU
      rewrite ⟦⟧-injective (trans (sym eqT) eqU) = <:-refl _ _
    helper (bnd _ T′<:U′) eqΓ eqT eqU            = D∧⇒F<:-gen T′<:U′ eqΓ eqT eqU
    helper (<:> _ B″∈Γ B″<:B _) {Γ} {T} refl refl eqU
      with lookupContraEnv B″∈Γ (⟪⟫-contraEnv Γ) | ⟦⟧-covar T
    ...  | ctt _ | cT                            = case ∈∩×¬∩⇒≡ (⊥-≃′ $ symm $ ⟨A∶⟩<:⟨A∶⟩′ B″<:B (⟪⟫-contraEnv Γ)) id of λ ()

  D∧⇒F<:-gen : ∀ {Γ′ S′ U′} → Γ′ ⊢ S′ <: U′ →
               ∀ {Γ S U} →
                 Γ′ ≡ ⟪ Γ ⟫ → S′ ≡ ⟦ S ⟧ → U′ ≡ ⟦ U ⟧ →
                 Γ ⊢F S <: U
  D∧⇒F<:-gen <:⊤ {Γ} {S} {⊤} eqΓ eqS refl      = ftop
  D∧⇒F<:-gen <:⊤ {Γ} {S} {var _} eqΓ eqS ()
  D∧⇒F<:-gen <:⊤ {Γ} {S} {Π<:′ _ ∙ _} eqΓ eqS ()
  D∧⇒F<:-gen ⊥<: {Γ} {⊤} {U} eqΓ () eqU
  D∧⇒F<:-gen ⊥<: {Γ} {var x} {U} eqΓ () eqU
  D∧⇒F<:-gen ⊥<: {Γ} {Π<:′ S ∙ S₁} {U} eqΓ () eqU
  D∧⇒F<:-gen refl {Γ} {S} {U} eqΓ eqS eqU
    rewrite ⟦⟧-injective (trans (sym eqS) eqU) = <:-refl _ _
  D∧⇒F<:-gen (bnd _ _) {Γ} {⊤} {U} eqΓ () eqU
  D∧⇒F<:-gen (bnd _ _) {Γ} {var _} {U} eqΓ () eqU
  D∧⇒F<:-gen (bnd _ _) {Γ} {Π<:′ _ ∙ _} {U} eqΓ () eqU
  D∧⇒F<:-gen (fld _) {Γ} {⊤} {U} eqΓ () eqU
  D∧⇒F<:-gen (fld _) {Γ} {var _} {U} eqΓ () eqU
  D∧⇒F<:-gen (fld _) {Γ} {Π<:′ _ ∙ _} {U} eqΓ () eqU
  D∧⇒F<:-gen (Π<: S′<:S U<:U′) {Γ} {⊤} {⊤} eqΓ () eqU
  D∧⇒F<:-gen (Π<: S′<:S U<:U′) {Γ} {⊤} {var _} eqΓ () eqU
  D∧⇒F<:-gen (Π<: S′<:S U<:U′) {Γ} {⊤} {Π<:′ _ ∙ _} eqΓ () eqU
  D∧⇒F<:-gen (Π<: S′<:S U<:U′) {Γ} {var _} {⊤} eqΓ () eqU
  D∧⇒F<:-gen (Π<: S′<:S U<:U′) {Γ} {var _} {var _} eqΓ () eqU
  D∧⇒F<:-gen (Π<: S′<:S U<:U′) {Γ} {var _} {Π<:′ _ ∙ _} eqΓ () eqU
  D∧⇒F<:-gen (Π<: S′<:S U<:U′) {Γ} {Π<:′ _ ∙ _} {⊤} eqΓ refl ()
  D∧⇒F<:-gen (Π<: S′<:S U<:U′) {Γ} {Π<:′ _ ∙ _} {var _} eqΓ refl ()
  D∧⇒F<:-gen (Π<: S′<:S U<:U′) {Γ} {Π<:′ S ∙ U} {Π<:′ S′ ∙ U′} refl refl refl
                                               = fall (helper S′<:S refl refl refl)
                                                      (D∧⇒F<:-gen U<:U′ refl refl refl)
  D∧⇒F<:-gen (sel₁ B T∈Γ T<:B) {Γ} {S} {U} refl refl eqU
    with lookupContraEnv T∈Γ (⟪⟫-contraEnv Γ) | ⟦⟧-covar S
  ...  | ctt _ | cS                            = case subst Covar (∈∩×¬∩⇒≡ (⊥-≃′ $ symm $ ⟨A∶⟩<:⟨A∶⟩′ T<:B (⟪⟫-contraEnv Γ))
                                                                           (covar⇒¬∩ cS)) cS of λ ()
  D∧⇒F<:-gen (sel₂ B T∈Γ T<:B) {Γ} {⊤} {U} eqΓ () eqU
  D∧⇒F<:-gen (sel₂ .A T∈Γ T<:B) {Γ} {var x} {U} refl refl refl
    with lookupContraEnv T∈Γ (⟪⟫-contraEnv Γ)
  ...  | ctt cT
       with env-lookup⇒<:∈ {x} {_} {⟪ Γ ⟫} T∈Γ refl
  ...     | _ , eqT , T′∈Γ                     = fbinds T′∈Γ (helper T<:B refl eqT refl)
  D∧⇒F<:-gen (sel₂ B T∈Γ T<:B) {Γ} {Π<:′ _ ∙ _} {U} eqΓ () eqU
  D∧⇒F<:-gen (<:> B T∈Γ T<:B T<:B′) {Γ} {S} {U} refl refl eqU
    with lookupContraEnv T∈Γ (⟪⟫-contraEnv Γ) | ⟦⟧-covar S
  ...  | ctt _ | cS                            = case subst Covar (∈∩×¬∩⇒≡ (⊥-≃′ $ symm $ ⟨A∶⟩<:⟨A∶⟩′ T<:B (⟪⟫-contraEnv Γ))
                                                                           (covar⇒¬∩ cS)) cS of λ ()
  D∧⇒F<:-gen (∩-E₁ S′<:U′) {Γ} {⊤} {U} eqΓ () eqU
  D∧⇒F<:-gen (∩-E₁ S′<:U′) {Γ} {var _} {U} eqΓ () eqU
  D∧⇒F<:-gen (∩-E₁ S′<:U′) {Γ} {Π<:′ _ ∙ _} {U} eqΓ () eqU
  D∧⇒F<:-gen (∩-E₂ S′<:U′) {Γ} {⊤} {U} eqΓ () eqU
  D∧⇒F<:-gen (∩-E₂ S′<:U′) {Γ} {var _} {U} eqΓ () eqU
  D∧⇒F<:-gen (∩-E₂ S′<:U′) {Γ} {Π<:′ _ ∙ _} {U} eqΓ () eqU
  D∧⇒F<:-gen (∩-I _ _) {Γ} {S} {⊤} eqΓ eqS ()
  D∧⇒F<:-gen (∩-I _ _) {Γ} {S} {var _} eqΓ eqS ()
  D∧⇒F<:-gen (∩-I _ _) {Γ} {S} {Π<:′ _ ∙ _} eqΓ eqS ()
  
  D∧⇒F<: : ∀ {Γ S U} → ⟪ Γ ⟫ ⊢ ⟦ S ⟧ <: ⟦ U ⟧ → Γ ⊢F S <: U
  D∧⇒F<: S<:U = D∧⇒F<:-gen S<:U refl refl refl

  F<:⇒D∧ : ∀ {Γ S U} → Γ ⊢F S <: U → ⟪ Γ ⟫ ⊢ ⟦ S ⟧ <: ⟦ U ⟧
  F<:⇒D∧ ftop               = <:⊤
  F<:⇒D∧ fvrefl             = refl
  F<:⇒D∧ (fbinds T∈Γ T<:T′) = sel₂ _ (<:∈⇒env-lookup T∈Γ) (bnd ⊥<: (F<:⇒D∧ T<:T′))
  F<:⇒D∧ (fall S′<:S U<:U′) = Π<: (bnd ⊥<: (F<:⇒D∧ S′<:S)) (F<:⇒D∧ U<:U′)
