{-# OPTIONS --without-K --safe #-}

module Everything where

-- subtyping problems of all following calculi are undecidable
-- assuming F<: deterministic (F<:d) is undecidable

-- F<: F<:- F<:d
import FsubMinus
-- F<: with bottom
import FsubBot
-- F<:>-
import FsubsupMinus2

-- D<<:
import DsubsubAlt
import DsubsubUndec

-- D<:
import DsubEquiv
import DsubTermUndec
-- D<: without transitivity
import DsubNoTrans

-- D∧
import DintAlg
import DintUndec

-- other things

-- properties of F<:-
import FsubMinus2
-- small-step subtyping for F<:-
import FsubMinusStep

import DsubFull
