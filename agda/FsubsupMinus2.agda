{-# OPTIONS --without-K --safe #-}

module FsubsupMinus2 where

open import Data.List as List
open import Data.List.All as All
open import Data.Nat as ℕ
open import Data.Maybe as Maybe
open import Data.Product as Σ
open import Data.Sum as ⊎
open import Data.Vec as Vec renaming (_∷_ to _‼_ ; [] to nil) hiding (_++_)
open import Function
open import Data.Empty renaming (⊥ to False)

open import Relation.Nullary
open import Relation.Binary.PropositionalEquality as ≡

open import Data.Nat.Properties as ℕₚ
open import Data.Maybe.Properties as Maybeₚ
open import Data.Product.Properties

open import Utils
open import FsubsupMinus
open EquivalentModel

module LBEnvProperties where
  LBEnv : Env → Set
  LBEnv = All (λ T → X>:? T ≡ ⊥ × X<:? T ≢ ⊥)

  ⊤<: : ∀ {Γ T} → Γ ⊢ ⊤ <: T → LBEnv Γ → T ≡ ⊤
  ⊤<: <:⊤ lbe            = refl
  ⊤<: refl lbe           = refl
  ⊤<: (tran ⊤<:T T<:T′) lbe
    rewrite ⊤<: ⊤<:T lbe = ⊤<: T<:T′ lbe
  ⊤<: (var₁ {Γ} {n} B∈Γ) lbe
    with lookupOpt Γ n | inspect (lookupOpt Γ) n
  ⊤<: (var₁ {_} {n} ()) lbe | nothing | _
  ⊤<: (var₁ {_} {n} B∈Γ) lbe | just (S , U) | [ eq ]
    rewrite proj₁ $ lookupOpt-All eq lbe | repeat-⊥↑ n 0 with B∈Γ
  ...     | ()

  ↑≡⊥ : ∀ T n → T ↑ n ≡ ⊥ → T ≡ ⊥
  ↑≡⊥ ⊤ n ()
  ↑≡⊥ ⊥ n eq = refl
  ↑≡⊥ (var x) n eq rewrite ↑-var x n with eq
  ... | ()
  ↑≡⊥ (Π<: T >: T₁ ∙ T₂) n ()

  repeat↑≡⊥ : ∀ T m n → repeat m (_↑ n) T ≡ ⊥ → T ≡ ⊥
  repeat↑≡⊥ T zero n eq                  = eq
  repeat↑≡⊥ T (suc m) n eq
    rewrite repeat↑≡⊥ T m n (↑≡⊥ _ _ eq) = refl

  <:⊥ : ∀ {Γ T} → Γ ⊢ T <: ⊥ → LBEnv Γ → T ≡ ⊥
  <:⊥ ⊥<: lbe                      = refl
  <:⊥ refl lbe                     = refl
  <:⊥ (tran S<:T T<:⊥) lbe
    rewrite <:⊥ T<:⊥ lbe           = <:⊥ S<:T lbe
  <:⊥ (var₂ {Γ} {n} B∈Γ) lbe
    with lookupOpt Γ n | inspect (lookupOpt Γ) n
  <:⊥ (var₂ {_} {n} ()) lbe | nothing | _
  <:⊥ (var₂ {_} {n} B∈Γ) lbe | just (S , U) | [ eq ]
    with lookupOpt-All eq lbe | repeat (suc n) (_↑ 0) S
       | inspect (repeat (suc n) (_↑ 0)) S | B∈Γ
  ...  | _ , S≢⊥ | _ | [ e ] | refl = ⊥-elim $ S≢⊥ (repeat↑≡⊥ S (suc n) 0 e)

module Undec where
  open import FsubMinus
  open import FsubMinus2
  open FsubMinus.FsubMinus
    renaming (Env to Env′ ; _↑_ to _⇑_ ; Π<:_∙_ to Π<:′_∙_)
    hiding (env-lookup)
  open LBEnvProperties

  infix 5 ⟦_⟧ ⟪_⟫
  
  ⟦_⟧ : Ftyp → Typ
  ⟦ ⊤ ⟧          = ⊤
  ⟦ var x ⟧      = var x
  ⟦ Π<:′ S ∙ U ⟧ = Π<: ⟦ S ⟧ ∙ ⟦ U ⟧

  ⟪_⟫ : Env′ → Env
  ⟪ [] ⟫    = []
  ⟪ T ∷ Γ ⟫ = (⟦ T ⟧ , ⊥) ∷ ⟪ Γ ⟫

  data Image : Typ → Set where
    ⇒⊤   : Image ⊤
    ⇒var : ∀ x → Image (var x)
    ⇒Π   : ∀ {S U} → Image S → Image U → Image (Π<: S ∙ U)

  module ⟦⟧-Bijective where
    open import Function.Bijection
    open import Function.Surjection
    open import Function.Equality

    Typᵢ : Set
    Typᵢ = Σ Typ Image

    ⟦⟧-image : ∀ T → Image ⟦ T ⟧
    ⟦⟧-image ⊤            = ⇒⊤
    ⟦⟧-image (var x)      = ⇒var x
    ⟦⟧-image (Π<:′ S ∙ U) = ⇒Π (⟦⟧-image S) (⟦⟧-image U)

    ⟦⟧-func : ≡.setoid Ftyp ⟶ ≡.setoid Typᵢ
    ⟦⟧-func   = record
      { _⟨$⟩_ = < ⟦_⟧ , ⟦⟧-image >
      ; cong  = ≡.cong < ⟦_⟧ , ⟦⟧-image >
      }

    ⟦⟧-injective : ∀ {S U} → ⟦ S ⟧ ≡ ⟦ U ⟧ → S ≡ U
    ⟦⟧-injective {⊤} {⊤} refl          = refl
    ⟦⟧-injective {⊤} {var _} ()
    ⟦⟧-injective {⊤} {Π<:′ _ ∙ _} ()
    ⟦⟧-injective {var x} {⊤} ()
    ⟦⟧-injective {var x} {var .x} refl = refl
    ⟦⟧-injective {var x} {Π<:′ _ ∙ _} ()
    ⟦⟧-injective {Π<:′ S ∙ U} {⊤} ()
    ⟦⟧-injective {Π<:′ S ∙ U} {var x} ()
    ⟦⟧-injective {Π<:′ S₁ ∙ U₁} {Π<:′ S₂ ∙ U₂} eq
      with ⟦ S₁ ⟧ | ⟦ S₂ ⟧ | ⟦ U₁ ⟧ | ⟦ U₂ ⟧
         | ⟦⟧-injective {S₁} {S₂} | ⟦⟧-injective {U₁} {U₂}
    ⟦⟧-injective {Π<:′ S₁ ∙ U₁} {Π<:′ S₂ ∙ U₂} refl
         | _ | _ | _ | _ | rec₁ | rec₂ = cong₂ Π<:′_∙_ (rec₁ refl) (rec₂ refl)

    ⟦⟧-func-injective : ∀ {S U} → (Typᵢ ∋ (⟦ S ⟧ , ⟦⟧-image S)) ≡ (⟦ U ⟧ , ⟦⟧-image U) → S ≡ U
    ⟦⟧-func-injective {S} {U} eq
      with ⟦ S ⟧ | ⟦⟧-image S | ⟦ U ⟧ | ⟦⟧-image U
         | ⟦⟧-injective {S} {U}
    ⟦⟧-func-injective {S} {U} refl
         | _ | _ | _ | _ | inj = inj refl

    infix 5 ⟦_⟧⁻¹
    ⟦_⟧⁻¹ : Typᵢ → Ftyp
    ⟦ _ , ⇒⊤ ⟧⁻¹     = ⊤
    ⟦ _ , ⇒var x ⟧⁻¹ = var x
    ⟦ _ , ⇒Π S U ⟧⁻¹ = Π<:′ ⟦ -, S ⟧⁻¹ ∙ ⟦ -, U ⟧⁻¹

    ⟦⟧-func-inv : ≡.setoid Typᵢ ⟶ ≡.setoid Ftyp
    ⟦⟧-func-inv = record
      { _⟨$⟩_   = ⟦_⟧⁻¹
      ; cong    = ≡.cong ⟦_⟧⁻¹
      }

    ⟦⟧-left-inverse-⟦⟧⁻¹ : ∀ {T} (iT : Image T) → (⟦ ⟦ -, iT ⟧⁻¹ ⟧ , ⟦⟧-image ⟦ -, iT ⟧⁻¹) ≡ (Typᵢ ∋ (-, iT))
    ⟦⟧-left-inverse-⟦⟧⁻¹ ⇒⊤       = refl
    ⟦⟧-left-inverse-⟦⟧⁻¹ (⇒var x) = refl
    ⟦⟧-left-inverse-⟦⟧⁻¹ (⇒Π S U)
      with ⟦ ⟦ -, S ⟧⁻¹ ⟧         | ⟦ ⟦ -, U ⟧⁻¹ ⟧ 
         | ⟦⟧-image ⟦ -, S ⟧⁻¹    | ⟦⟧-image ⟦ -, U ⟧⁻¹
         | ⟦⟧-left-inverse-⟦⟧⁻¹ S | ⟦⟧-left-inverse-⟦⟧⁻¹ U
    ...  | _ | _ | _ | _ | refl | refl = refl


    ⟦⟧-bijective : Bijective ⟦⟧-func
    ⟦⟧-bijective   = record
      { injective  = ⟦⟧-func-injective
      ; surjective = record
        { from             = ⟦⟧-func-inv
        ; right-inverse-of = λ { (_ , T) → ⟦⟧-left-inverse-⟦⟧⁻¹ T }
        }
      }

  open ⟦⟧-Bijective using (⟦⟧-image ; ⟦⟧-injective) public

  ⟦⟧≢⊥ : ∀ T → ⟦ T ⟧ ≢ ⊥
  ⟦⟧≢⊥ T with ⟦ T ⟧ | ⟦⟧-image T
  ⟦⟧≢⊥ T | _ | ⇒⊤     = λ ()
  ⟦⟧≢⊥ T | _ | ⇒var _ = λ ()
  ⟦⟧≢⊥ T | _ | ⇒Π _ _ = λ ()

  ⟪⟫-LBEnv : ∀ Γ → LBEnv ⟪ Γ ⟫
  ⟪⟫-LBEnv []      = []
  ⟪⟫-LBEnv (T ∷ Γ) = (refl , ⟦⟧≢⊥ T) ∷ (⟪⟫-LBEnv Γ)

  ⟦⟧-↑-comm : ∀ T n → ⟦ T ⟧ ↑ n ≡ ⟦ T ⇑ n ⟧
  ⟦⟧-↑-comm ⊤ n = refl
  ⟦⟧-↑-comm (var x) n with n ≤? x
  ... | yes _   = refl
  ... | no _    = refl
  ⟦⟧-↑-comm (Π<:′ S ∙ U) n
    rewrite ⟦⟧-↑-comm S n | ⟦⟧-↑-comm U (suc n)
                = refl

  <:∈⇒env-lookup : ∀ {n T Γ} → n <: T ∈ Γ → env-lookup ⟪ Γ ⟫ n ≡ just (⟦ T ⟧ , ⊥)
  <:∈⇒env-lookup (hd {T})
    rewrite ⟦⟧-↑-comm T 0 = refl
  <:∈⇒env-lookup (tl {n} {T} {_} {Γ} T∈Γ)
       with lookupOpt ⟪ Γ ⟫ n | <:∈⇒env-lookup T∈Γ
  ...     | nothing | ()
  ...     | just (T′ , rep⊥) | eq
       with repeat n (_↑ 0) T′ ↑ 0 | repeat n (_↑ 0) rep⊥ ↑ 0
  ...     | a | b with eq
  ...                | refl
    rewrite ⟦⟧-↑-comm T 0 = refl

  F<:⇒F<:> : ∀ {Γ S U} → Γ ⊢F S <: U → ⟪ Γ ⟫ ⊢ ⟦ S ⟧ <: ⟦ U ⟧
  F<:⇒F<:> ftop               = <:⊤
  F<:⇒F<:> fvrefl             = refl
  F<:⇒F<:> (fbinds T∈Γ T<:U)  = tran (var₂ (<:∈⇒env-lookup T∈Γ)) (F<:⇒F<:> T<:U)
  F<:⇒F<:> (fall S′<:S U<:U′) = Π<: (F<:⇒F<:> S′<:S) ⊥<: (F<:⇒F<:> U<:U′)

  env-lookup⇒<:∈ : ∀ {Γ n Γ′ S U} →
                     env-lookup Γ′ n ≡ just (S , U) →
                     Γ′ ≡ ⟪ Γ ⟫ →
                     U ≡ ⊥ × ∃ λ S′ → S ≡ ⟦ S′ ⟧ × n <: S′ ∈ Γ
  env-lookup⇒<:∈ {[]} {n} {[]} () refl
  env-lookup⇒<:∈ {[]} {n} {x ∷ Γ′} S,U∈Γ′ ()
  env-lookup⇒<:∈ {_ ∷ Γ} {n} {[]} S,U∈Γ′ ()
  env-lookup⇒<:∈ {S ∷ Γ} {zero} {.(⟦ S ⟧ , ⊥) ∷ .(⟪ Γ ⟫)} refl refl
    rewrite ⟦⟧-↑-comm S 0  = refl , S ⇑ 0 , refl , hd
  env-lookup⇒<:∈ {T ∷ Γ} {suc n} {.(⟦ T ⟧ , ⊥) ∷ .(⟪ Γ ⟫)} S,U∈Γ′ refl
    with lookupOpt ⟪ Γ ⟫ n | env-lookup⇒<:∈ {Γ} {n} {⟪ Γ ⟫}
  env-lookup⇒<:∈ {T ∷ Γ} {suc n} {.(⟦ T ⟧ , ⊥) ∷ .(⟪ Γ ⟫)} () refl | nothing | rec
  env-lookup⇒<:∈ {T ∷ Γ} {suc n} {.(⟦ T ⟧ , ⊥) ∷ .(⟪ Γ ⟫)} refl refl
    | just (S , U) | rec with rec refl refl
  ...                       | eq , S′ , eqS′ , S′∈Γ
    rewrite repeat↑≡⊥ U (suc n) 0 eq
          | repeat-⊥↑ (2 + n) 0
          | eqS′
          | ⟦⟧-↑-comm S′ 0 = refl , S′ ⇑ 0 , refl , tl S′∈Γ

  F<:>′⇒F<: : ∀ {Γ′ S′ U′} → Γ′ ⊢′ S′ <: U′ →
             ∀ {Γ S U} →
               Γ′ ≡ ⟪ Γ ⟫ → S′ ≡ ⟦ S ⟧ → U′ ≡ ⟦ U ⟧ →
               Γ ⊢F S <: U
  F<:>′⇒F<: <:⊤ {Γ} {S} {⊤} eqΓ eqS refl = ftop
  F<:>′⇒F<: <:⊤ {Γ} {S} {var x} eqΓ eqS ()
  F<:>′⇒F<: <:⊤ {Γ} {S} {Π<:′ _ ∙ _} eqΓ eqS ()
  F<:>′⇒F<: ⊥<: {Γ} {S} {U} eqΓ eqS eqU
    with ⟦⟧-image S
  ... | Si rewrite sym eqS with Si
  ... | ()
  F<:>′⇒F<: refl {Γ} {S} {U} eqΓ eqS eqU
    with ⟦⟧-injective (≡.trans (sym eqS) eqU)
  ... | refl                             = <:-refl Γ S
  F<:>′⇒F<: (var₁ B∈Γ S′<:U′) {Γ} {S} {⊤} eqΓ eqS ()
  F<:>′⇒F<: (var₁ B∈Γ S′<:U′) {Γ} {S} {var x} refl eqS refl
    with env-lookup⇒<:∈ {Γ} B∈Γ refl
  ... | refl , _ rewrite <:⊥ (<:′⇒<: S′<:U′) (⟪⟫-LBEnv Γ)
      with ⟦⟧-image S
  ...    | Si rewrite sym eqS with Si
  ...    | ()
  F<:>′⇒F<: (var₁ B∈Γ S′<:U′) {Γ} {S} {Π<:′ _ ∙ _} eqΓ eqS ()
  F<:>′⇒F<: (var₂ B∈Γ S′<:U′) {Γ} {⊤} {U} eqΓ () eqU
  F<:>′⇒F<: (var₂ B∈Γ S′<:U′) {Γ} {var x} {U} refl refl eqU
    with env-lookup⇒<:∈ {Γ} B∈Γ refl
  ... | _ , S′ , refl , S′∈Γ             = fbinds S′∈Γ (F<:>′⇒F<: S′<:U′ refl refl eqU)
  F<:>′⇒F<: (var₂ B∈Γ S′<:U′) {Γ} {Π<:′ _ ∙ _} {U} eqΓ () eqU
  F<:>′⇒F<: (Π<: S₁′<:S₁ S₂<:S₂ U<:U′) {Γ} {⊤} {⊤} refl () eqU
  F<:>′⇒F<: (Π<: S₁′<:S₁ S₂<:S₂ U<:U′) {Γ} {⊤} {var _} refl () eqU
  F<:>′⇒F<: (Π<: S₁′<:S₁ S₂<:S₂ U<:U′) {Γ} {⊤} {Π<:′ _ ∙ _} refl () eqU
  F<:>′⇒F<: (Π<: S₁′<:S₁ S₂<:S₂ U<:U′) {Γ} {var _} {⊤} refl () eqU
  F<:>′⇒F<: (Π<: S₁′<:S₁ S₂<:S₂ U<:U′) {Γ} {var _} {var _} refl () eqU
  F<:>′⇒F<: (Π<: S₁′<:S₁ S₂<:S₂ U<:U′) {Γ} {var _} {Π<:′ _ ∙ _} refl () eqU
  F<:>′⇒F<: (Π<: S₁′<:S₁ S₂<:S₂ U<:U′) {Γ} {Π<:′ _ ∙ _} {⊤} refl refl ()
  F<:>′⇒F<: (Π<: S₁′<:S₁ S₂<:S₂ U<:U′) {Γ} {Π<:′ _ ∙ _} {var _} refl refl ()
  F<:>′⇒F<: (Π<: S₁′<:S₁ S₂<:S₂ U<:U′) {Γ} {Π<:′ S ∙ U} {Π<:′ S′ ∙ U′} refl refl refl
                                         = fall (F<:>′⇒F<: S₁′<:S₁ refl refl refl)
                                                (F<:>′⇒F<: U<:U′ refl refl refl)
  F<:>′⇒F<: (<:> B∈Γ U′<:U S<:S′) {Γ} {S} {U} refl eqS eqU
    with env-lookup⇒<:∈ {Γ} B∈Γ refl
  ... | refl , _ rewrite  <:⊥ (<:′⇒<: U′<:U) (⟪⟫-LBEnv Γ)
      with ⟦⟧-image S
  ...    | Si rewrite sym eqS with Si
  ...    | ()

  F<:>⇒F<: : ∀ {Γ S U} → ⟪ Γ ⟫ ⊢ ⟦ S ⟧ <: ⟦ U ⟧ → Γ ⊢F S <: U
  F<:>⇒F<: S<:U = F<:>′⇒F<: (<:⇒<:′ S<:U) refl refl refl
