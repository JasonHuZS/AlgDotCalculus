{-# OPTIONS --without-K --safe #-}

-- small-step subtyping
module FsubMinusStep where

open import Data.List as List
open import Data.Nat
open import Data.Maybe as Maybe
open import Data.Product
open import Data.Sum
open import Function
open import Data.Empty renaming (⊥ to False)

open import Relation.Nullary
open import Relation.Unary using () renaming (Decidable to UDecidable)
open import Relation.Binary using () renaming (Decidable to BDecidable)
open import Relation.Binary.PropositionalEquality as ≡
open import Relation.Binary.Construct.Closure.ReflexiveTransitive

open import Data.Nat.Properties as ℕₚ
open import Data.Maybe.Properties as Maybeₚ
open import Data.List.Properties as Listₚ

open import Utils
open import FsubMinus
open import FsubMinus2
open FsubMinus.FsubMinus

module MutualDef where

  infix 4 _⊢F_↗_ _⊢F_↘_ _⊢F′_<:_ _⊢F′_>:_
  
  mutual
  
    data _⊢F_↗_ : Env → Ftyp → Ftyp → Set where
      ↗⊤   : ∀ {Γ T} → T ≢ ⊤ → Γ ⊢F T ↗ ⊤
      ↗var : ∀ {Γ n T} → n <: T ∈ Γ → Γ ⊢F var n ↗ T
      ↗Π₁  : ∀ {Γ S₁ S₂} U →
               Γ ⊢F S₁ ↘ S₂ →
               Γ ⊢F Π<: S₁ ∙ U ↗ Π<: S₂ ∙ U
      ↗Π₂  : ∀ {Γ U₁ U₂} S →
               Γ ‣ S ! ⊢F U₁ ↗ U₂ →
               Γ ⊢F Π<: S ∙ U₁ ↗ Π<: S ∙ U₂
  
    data _⊢F_↘_ : Env → Ftyp → Ftyp → Set where
      ⊤↘   : ∀ {Γ T} → T ≢ ⊤ → Γ ⊢F ⊤ ↘ T
      var↘ : ∀ {Γ n T} → n <: T ∈ Γ → Γ ⊢F T ↘ var n
      Π↘₁  : ∀ {Γ S₁ S₂} U →
               Γ ⊢F S₁ ↗ S₂ →
               Γ ⊢F Π<: S₁ ∙ U ↘ Π<: S₂ ∙ U
      Π↘₂  : ∀ {Γ U₁ U₂} S →
               Γ ‣ S ! ⊢F U₁ ↘ U₂ →
               Γ ⊢F Π<: S ∙ U₁ ↘ Π<: S ∙ U₂
  
  -- subtyping relation is the reflexive transitive closure of small-step subtyping
  _⊢F′_<:_ : Env → Ftyp → Ftyp → Set
  Γ ⊢F′ S <: U = Star (Γ ⊢F_↗_) S U
  
  _⊢F′_>:_ : Env → Ftyp → Ftyp → Set
  Γ ⊢F′ S >: U = Star (Γ ⊢F_↘_) S U
  
  ⊤<:′ : ∀ {Γ T} → Γ ⊢F′ ⊤ <: T → T ≡ ⊤
  ⊤<:′ ε               = refl
  ⊤<:′ (↗⊤ S≢⊤ ◅ ⊤<:T) = ⊤<:′ ⊤<:T
  
  mutual
    ↗⇒<: : ∀ {Γ S U} → Γ ⊢F S ↗ U → Γ ⊢F S <: U
    ↗⇒<: (↗⊤ _)        = ftop
    ↗⇒<: (↗var T∈Γ)    = fbinds T∈Γ (<:-refl _ _)
    ↗⇒<: (↗Π₁ U S₁↘S₂) = fall (↘⇒<: S₁↘S₂) (<:-refl _ _)
    ↗⇒<: (↗Π₂ S U₁↗U₂) = fall (<:-refl _ _) (↗⇒<: U₁↗U₂)
  
    ↘⇒<: : ∀ {Γ S U} → Γ ⊢F S ↘ U → Γ ⊢F U <: S
    ↘⇒<: (⊤↘ _)        = ftop
    ↘⇒<: (var↘ T∈Γ)    = fbinds T∈Γ (<:-refl _ _)
    ↘⇒<: (Π↘₁ U S₁↗S₂) = fall (↗⇒<: S₁↗S₂) (<:-refl _ _)
    ↘⇒<: (Π↘₂ S U₁↘U₂) = fall (<:-refl _ _) (↘⇒<: U₁↘U₂)
  
  <:′⇒<: : ∀ {Γ S U} → Γ ⊢F′ S <: U → Γ ⊢F S <: U
  <:′⇒<: ε               = <:-refl _ _
  <:′⇒<: (S↗S′ ◅ S′<:′U) = <:-trans (↗⇒<: S↗S′) (<:′⇒<: S′<:′U)
  
  wrap-<:′-↗-b : ∀ {Γ S U₁ U₂} → Γ ‣ S ! ⊢F′ U₁ <: U₂ → Γ ⊢F′ Π<: S ∙ U₁ <: Π<: S ∙ U₂
  wrap-<:′-↗-b ε                = ε
  wrap-<:′-↗-b (U₁↗U′ ◅ U′<:U₂) = ↗Π₂ _ U₁↗U′ ◅ wrap-<:′-↗-b U′<:U₂
  
  wrap-<:′-↗-p : ∀ {Γ S₁ S₂} → Γ ⊢F′ S₁ >: S₂ → ∀ U → Γ ⊢F′ Π<: S₁ ∙ U <: Π<: S₂ ∙ U
  wrap-<:′-↗-p ε U                = ε
  wrap-<:′-↗-p (S₁↘S′ ◅ S′>:S₂) U = ↗Π₁ _ S₁↘S′ ◅ wrap-<:′-↗-p S′>:S₂ U
  
  wrap-<:′-↘-b : ∀ {Γ S U₁ U₂} → Γ ‣ S ! ⊢F′ U₁ >: U₂ → Γ ⊢F′ Π<: S ∙ U₁ >: Π<: S ∙ U₂
  wrap-<:′-↘-b ε                = ε
  wrap-<:′-↘-b (U₁↘U′ ◅ U′>:U₂) = Π↘₂ _ U₁↘U′ ◅ wrap-<:′-↘-b U′>:U₂
  
  wrap-<:′-↘-p : ∀ {Γ S₁ S₂} → Γ ⊢F′ S₁ <: S₂ → ∀ U → Γ ⊢F′ Π<: S₁ ∙ U >: Π<: S₂ ∙ U
  wrap-<:′-↘-p ε U                = ε
  wrap-<:′-↘-p (S₁↗S′ ◅ S′<:S₂) U = Π↘₁ U S₁↗S′ ◅ wrap-<:′-↘-p S′<:S₂ U
  
  mutual
    <:⇒<:′ : ∀ {Γ S U} → Γ ⊢F S <: U → Γ ⊢F′ S <: U
    <:⇒<:′ {_} {S} ftop with ≡⊤-dec S
    ... | yes refl           = ε
    ... | no S≢⊤             = ↗⊤ S≢⊤ ◅ ε
    <:⇒<:′ fvrefl            = ε
    <:⇒<:′ (fbinds T∈Γ T<:U) = ↗var T∈Γ ◅ <:⇒<:′ T<:U
    <:⇒<:′ (fall {_} {S} {S′} {U} {U′} S′<:S U<:U′)
      = wrap-<:′-↗-p (<:⇒>: S′<:S) U ◅◅ wrap-<:′-↗-b (<:⇒<:′ U<:U′)
  
    <:⇒>: : ∀ {Γ S U} → Γ ⊢F S <: U → Γ ⊢F′ U >: S
    <:⇒>: {_} {S} ftop with ≡⊤-dec S
    ... | yes refl          = ε
    ... | no S≢⊤            = ⊤↘ S≢⊤ ◅ ε
    <:⇒>: fvrefl            = ε
    <:⇒>: (fbinds T∈Γ T<:U) = <:⇒>: T<:U ◅◅ var↘ T∈Γ ◅ ε
    <:⇒>: (fall {_} {S} {S′} {U} {U′} S′<:S U<:U′)
      = wrap-<:′-↘-b (<:⇒>: U<:U′) ◅◅ wrap-<:′-↘-p (<:⇒<:′ S′<:S) U 
  
module InductiveDef where

  infix 4 _⊢F_↗_ _⊢F′_<:_
  data _⊢F_↗_ : Env → Ftyp → Ftyp → Set where
    ↗⊤   : ∀ {Γ T} → T ≢ ⊤ → Γ ⊢F T ↗ ⊤
    ↗var : ∀ {Γ n T} → n <: T ∈ Γ → Γ ⊢F var n ↗ T
    ↗Π₁  : ∀ {Γ S₁ S₂} U →
             Γ ⊢F S₂ ↗ S₁ →
             Γ ⊢F Π<: S₁ ∙ U ↗ Π<: S₂ ∙ U
    ↗Π₂  : ∀ {Γ U₁ U₂} S →
             Γ ‣ S ! ⊢F U₁ ↗ U₂ →
             Γ ⊢F Π<: S ∙ U₁ ↗ Π<: S ∙ U₂

  -- subtyping relation is the reflexive transitive closure of small-step subtyping
  _⊢F′_<:_ : Env → Ftyp → Ftyp → Set
  Γ ⊢F′ S <: U = Star (Γ ⊢F_↗_) S U

  ↗⇒<: : ∀ {Γ S U} → Γ ⊢F S ↗ U → Γ ⊢F S <: U
  ↗⇒<: (↗⊤ _)        = ftop
  ↗⇒<: (↗var T∈Γ)    = fbinds T∈Γ (<:-refl _ _)
  ↗⇒<: (↗Π₁ U S₂↗S₁) = fall (↗⇒<: S₂↗S₁) (<:-refl _ U)
  ↗⇒<: (↗Π₂ S U₁↗U₂) = fall (<:-refl _ S) (↗⇒<: U₁↗U₂)

  <:′⇒<: : ∀ {Γ S U} → Γ ⊢F′ S <: U → Γ ⊢F S <: U
  <:′⇒<: ε               = <:-refl _ _
  <:′⇒<: (S↗S′ ◅ S′<:′U) = <:-trans (↗⇒<: S↗S′) (<:′⇒<: S′<:′U)

  wrap-<:′-↗-b : ∀ {Γ S U₁ U₂} → Γ ‣ S ! ⊢F′ U₁ <: U₂ → Γ ⊢F′ Π<: S ∙ U₁ <: Π<: S ∙ U₂
  wrap-<:′-↗-b ε                = ε
  wrap-<:′-↗-b (U₁↗U′ ◅ U′<:U₂) = ↗Π₂ _ U₁↗U′ ◅ wrap-<:′-↗-b U′<:U₂
  
  wrap-<:′-↗-p : ∀ {Γ S₁ S₂} → Γ ⊢F′ S₂ <: S₁ → ∀ U → Γ ⊢F′ Π<: S₁ ∙ U <: Π<: S₂ ∙ U
  wrap-<:′-↗-p ε U                = ε
  wrap-<:′-↗-p (S′↗S₁ ◅ S₂<:S′) U = wrap-<:′-↗-p S₂<:S′ U ◅◅ ↗Π₁ U S′↗S₁ ◅ ε

  -- this translation implies that the form of transitive closure can be
  -- limited without losing any power.
  <:⇒<:′ : ∀ {Γ S U} → Γ ⊢F S <: U → Γ ⊢F′ S <: U
  <:⇒<:′ {_} {S} ftop with ≡⊤-dec S
  ... | yes refl           = ε
  ... | no S≢⊤             = ↗⊤ S≢⊤ ◅ ε
  <:⇒<:′ fvrefl            = ε
  <:⇒<:′ (fbinds T∈Γ T<:U) = ↗var T∈Γ ◅ <:⇒<:′ T<:U
  <:⇒<:′ (fall S₂<:S₁ U₁<:U₂)
    = wrap-<:′-↗-p (<:⇒<:′ S₂<:S₁) _ ◅◅ wrap-<:′-↗-b (<:⇒<:′ U₁<:U₂) 

  ¬n<:n∈Γ : ∀ {n Γ} → ¬ n <: var n ∈ Γ
  ¬n<:n∈Γ n∈Γ = aux n∈Γ refl
    where aux : ∀ {n T Γ} → n <: T ∈ Γ → T ≡ var n → False
          aux (hd {⊤}) ()
          aux (hd {var _}) ()
          aux (hd {Π<: _ ∙ _}) ()
          aux (tl {_} {⊤} T∈Γ) ()
          aux (tl {_} {var x} T∈Γ) refl = aux T∈Γ refl
          aux (tl {_} {Π<: _ ∙ _} T∈Γ) ()

  ↗-¬refl : ∀ {Γ T} → ¬ Γ ⊢F T ↗ T
  ↗-¬refl T↗T = aux T↗T refl
    where aux : ∀ {Γ S U} → Γ ⊢F S ↗ U → S ≡ U → False
          aux (↗⊤ S≢⊤) S≡⊤    = S≢⊤ S≡⊤
          aux (↗var U∈Γ) refl = ¬n<:n∈Γ U∈Γ
          aux (↗Π₁ U S↗U) S≡U with Π<:-injective S≡U
          ... | refl , _      = aux S↗U refl
          aux (↗Π₂ S S↗U) S≡U with Π<:-injective S≡U
          ... | _ , refl      = aux S↗U refl
