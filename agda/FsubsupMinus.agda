{-# OPTIONS --without-K --safe #-}

module FsubsupMinus where

open import Data.List as List
open import Data.List.All as All
open import Data.Nat as ℕ
open import Data.Maybe as Maybe
open import Data.Product as Σ
open import Data.Sum as ⊎
open import Data.Vec as Vec renaming (_∷_ to _‼_ ; [] to nil) hiding (_++_)
open import Function
open import Data.Empty renaming (⊥ to False)

open import Relation.Nullary
open import Relation.Binary.PropositionalEquality as ≡

open import Data.Nat.Properties as ℕₚ
open import Data.Maybe.Properties as Maybeₚ
open import Data.Product.Properties

open import Induction.Nat

open import Utils

infix 6 var_
infixr 6 Π<:_>:_∙_ Π<:_∙_

data Typ : Set where
  ⊤         : Typ
  ⊥         : Typ
  var_      : ℕ → Typ
  Π<:_>:_∙_ : Typ → Typ → Typ → Typ

Env : Set
Env = List (Typ × Typ)

infixl 7 _↑_
_↑_ : Typ → ℕ → Typ
⊤ ↑ n                 = ⊤
⊥ ↑ n                 = ⊥
(var x) ↑ n with n ≤? x
... | yes p           = var (suc x)
... | no ¬p           = var x
(Π<: S >: S′ ∙ U) ↑ n = Π<: S ↑ n >: S′ ↑ n ∙ U ↑ suc n

Π<:_∙_ : Typ → Typ → Typ
Π<:_∙_ = Π<:_>: ⊥ ∙_

env-lookup : Env → ℕ → Maybe (Typ × Typ)
env-lookup Γ n = Maybe.map (Σ.map up up) (lookupOpt Γ n)
  where up = repeat (suc n) (_↑ 0)

X<:? : Typ × Typ → Typ
X<:? = proj₁

X>:? : Typ × Typ → Typ
X>:? = proj₂

infix 6 _,_‼
_,_‼ : Typ → Typ → List (Typ × Typ)
S , U ‼ = (S , U) !

↑-idx : ℕ → ℕ → ℕ
↑-idx x n with n ≤? x
... | yes p = suc x
... | no ¬p = x

↑-var : ∀ x n → (var x) ↑ n ≡ var (↑-idx x n)
↑-var x n with n ≤? x
... | yes p = refl
... | no ¬p = refl

↑-↑-comm : ∀ T m n → m ≤ n → T ↑ m ↑ suc n ≡ T ↑ n ↑ m
↑-↑-comm ⊤ m n m≤n                                  = refl
↑-↑-comm ⊥ m n m≤n                                  = refl
↑-↑-comm (var x) m n m≤n with n ≤? x
... | yes n≤x
    rewrite ≤?-yes (≤-trans m≤n n≤x)
          | ≤?-yes n≤x
          | ≤?-yes (≤-step (≤-trans m≤n n≤x))       = refl
... | no n>x with m ≤? x
...             | yes m≤x rewrite proj₂ $ ≤?-no n>x = refl
...             | no m>x with suc n ≤? x
...                         | yes 1+n≤x             = ⊥-elim (m>x (≤-trans (≤-step m≤n) 1+n≤x))
...                         | no 1+n>x              = refl
↑-↑-comm (Π<: S >: S′ ∙ U) m n m≤n
  rewrite ↑-↑-comm S m n m≤n
        | ↑-↑-comm S′ m n m≤n
        | ↑-↑-comm U (suc m) (suc n) (s≤s m≤n)      = refl

infix 4 _⊢_<:_
data _⊢_<:_ : Env → Typ → Typ → Set where
  <:⊤  : ∀ {Γ T} → Γ ⊢ T <: ⊤
  ⊥<:  : ∀ {Γ T} → Γ ⊢ ⊥ <: T
  refl : ∀ {Γ T} → Γ ⊢ T <: T
  tran : ∀ {Γ S T U} → Γ ⊢ S <: T → Γ ⊢ T <: U → Γ ⊢ S <: U
  var₁ : ∀ {Γ n B} → env-lookup Γ n ≡ just B → Γ ⊢ X>:? B <: var n
  var₂ : ∀ {Γ n B} → env-lookup Γ n ≡ just B → Γ ⊢ var n <: X<:? B
  Π<:  : ∀ {Γ S₁ S₂ U S₁′ S₂′ U′} →
           Γ ⊢ S₁′ <: S₁ →
           Γ ⊢ S₂ <: S₂′ →
           Γ ‣ S₁′ , S₂′ ‼ ⊢ U <: U′ →
           Γ ⊢ Π<: S₁ >: S₂ ∙ U <: Π<: S₁′ >: S₂′ ∙ U′

Typ-measure : Typ → ℕ
Typ-measure ⊤                 = 1
Typ-measure ⊥                 = 1
Typ-measure (var _)           = 2
Typ-measure (Π<: S >: S′ ∙ U) = 1 + Typ-measure S + Typ-measure S′ + Typ-measure U

Typ-measure≥1 : ∀ T → Typ-measure T ≥ 1
Typ-measure≥1 ⊤                = s≤s z≤n
Typ-measure≥1 ⊥                = s≤s z≤n
Typ-measure≥1 (var x)          = s≤s z≤n
Typ-measure≥1 (Π<: _ >: _ ∙ _) = s≤s z≤n

Typ-measure-↑ : ∀ T n → Typ-measure (T ↑ n) ≡ Typ-measure T
Typ-measure-↑ ⊤ n                 = refl
Typ-measure-↑ ⊥ n                 = refl
Typ-measure-↑ (var x) n
  rewrite ↑-var x n               = refl
Typ-measure-↑ (Π<: S >: S′ ∙ U) n
  rewrite Typ-measure-↑ S n
        | Typ-measure-↑ S′ n
        | Typ-measure-↑ U (suc n) = refl

infix 4 _<:_>:_∈_
data _<:_>:_∈_ : ℕ → Typ → Typ → Env → Set where
  hd : ∀ {S U Γ} → 0 <: S ↑ 0 >: U ↑ 0 ∈ Γ ‣ S , U ‼
  tl : ∀ {n S U B Γ} →
         n <: S >: U ∈ Γ →
         suc n <: S ↑ 0 >: U ↑ 0 ∈ Γ ‣ B !

<:>∈⇒lookup : ∀ {n S U Γ} →
                n <: S >: U ∈ Γ →
                env-lookup Γ n ≡ just (S , U)
<:>∈⇒lookup hd      = refl
<:>∈⇒lookup {suc n} {_} {_} {_ ∷ Γ} (tl S,U∈Γ)
  with lookupOpt Γ n | <:>∈⇒lookup S,U∈Γ
... | nothing | ()
... | just _ | refl = refl

lookup⇒<:>∈ : ∀ {n S U Γ} →
                env-lookup Γ n ≡ just (S , U) →
                n <: S >: U ∈ Γ
lookup⇒<:>∈ {n} {_} {_} {[]} ()
lookup⇒<:>∈ {zero} {_} {_} {(S , U) ∷ Γ} refl           = hd
lookup⇒<:>∈ {suc n} {S} {U} {B ∷ Γ} S,U∈Γ
  with lookupOpt Γ n | λ {S} {U} → lookup⇒<:>∈ {n} {S} {U} {Γ = Γ}
lookup⇒<:>∈ {suc n} {S} {U} {B ∷ Γ} ()   | nothing | _
lookup⇒<:>∈ {suc n} {_} {_} {B ∷ Γ} refl | just _ | rec = tl (rec refl)

repeat-⊥↑ : ∀ n m → repeat n (_↑ m) ⊥ ≡ ⊥
repeat-⊥↑ zero m                          = refl
repeat-⊥↑ (suc n) m rewrite repeat-⊥↑ n m = refl

module EquivalentModel where
  infix 4 _⊢′_<:_
  data _⊢′_<:_ : Env → Typ → Typ → Set where
    <:⊤  : ∀ {Γ T} → Γ ⊢′ T <: ⊤
    ⊥<:  : ∀ {Γ T} → Γ ⊢′ ⊥ <: T
    refl : ∀ {Γ T} → Γ ⊢′ T <: T
    var₁ : ∀ {Γ n S U′ U} →
             env-lookup Γ n ≡ just (S , U) →
             Γ ⊢′ U′ <: U →
             Γ ⊢′ U′ <: var n
    var₂ : ∀ {Γ n S S′ U} →
             env-lookup Γ n ≡ just (S , U) →
             Γ ⊢′ S <: S′ →
             Γ ⊢′ var n <: S′
    Π<:  : ∀ {Γ S₁ S₂ U S₁′ S₂′ U′} →
             Γ ⊢′ S₁′ <: S₁ →
             Γ ⊢′ S₂ <: S₂′ →
             Γ ‣ S₁′ , S₂′ ‼ ⊢′ U <: U′ →
             Γ ⊢′ Π<: S₁ >: S₂ ∙ U <: Π<: S₁′ >: S₂′ ∙ U′
    <:>  : ∀ {Γ n S S′ U′ U} →
             env-lookup Γ n ≡ just (S , U) →
             Γ ⊢′ U′ <: U →
             Γ ⊢′ S <: S′ →
             Γ ⊢′ U′ <: S′

  infixl 7 _↑↑_ _⇑
  _↑↑_ : Typ × Typ → Env → Typ × Typ
  (S , U) ↑↑ Γ = S ↑ length Γ , U ↑ length Γ

  _⇑ : Env → Env
  [] ⇑      = []
  (B ∷ Γ) ⇑ = B ↑↑ Γ ∷ Γ ⇑

  ⇑-length : ∀ Γ → length Γ ≡ length (Γ ⇑)
  ⇑-length []      = refl
  ⇑-length (T ∷ Γ) = cong suc (⇑-length Γ)

  <:>∈-weaken-≤ : ∀ {n S U Γ} →
                    n <: S >: U ∈ Γ →
                  ∀ {Γ₁ Γ₂ B′} →
                    Γ ≡ Γ₁ ‣ Γ₂ →
                    length Γ₂ ≤ n →
                    suc n <: S ↑ length Γ₂ >: U ↑ length Γ₂ ∈ Γ₁ ‣ B′ ! ‣ Γ₂ ⇑
  <:>∈-weaken-≤ hd {Γ₁} {[]} refl z≤n = tl hd
  <:>∈-weaken-≤ hd {Γ₁} {x ∷ Γ₂} eqΓ ()
  <:>∈-weaken-≤ (tl S,U∈Γ) {Γ₁} {[]} refl z≤n = tl (tl S,U∈Γ)
  <:>∈-weaken-≤ (tl {_} {S} {U} S,U∈Γ) {Γ₁} {_ ∷ Γ₂} refl (s≤s l≤n)
    rewrite ↑-↑-comm S 0 (length Γ₂) z≤n
          | ↑-↑-comm U 0 (length Γ₂) z≤n = tl (<:>∈-weaken-≤ S,U∈Γ refl l≤n)
  
  <:>∈-weaken-> : ∀ {n S U Γ} →
                    n <: S >: U ∈ Γ →
                  ∀ {Γ₁ Γ₂ B′} →
                    Γ ≡ Γ₁ ‣ Γ₂ →
                    n < length Γ₂ →
                    n <: S ↑ length Γ₂ >: U ↑ length Γ₂ ∈ Γ₁ ‣ B′ ! ‣ Γ₂ ⇑
  <:>∈-weaken-> S,U∈Γ {_} {[]} eqΓ ()
  <:>∈-weaken-> (hd {S} {U}) {_} {_ ∷ Γ₂} refl (s≤s n<l)
    rewrite ↑-↑-comm S 0 (length Γ₂) n<l
          | ↑-↑-comm U 0 (length Γ₂) n<l = hd
  <:>∈-weaken-> (tl {_} {S} {U} S,U∈Γ) {_} {B ∷ Γ₂} refl (s≤s n<l)
    rewrite ↑-↑-comm S 0 (length Γ₂) z≤n
          | ↑-↑-comm U 0 (length Γ₂) z≤n = tl (<:>∈-weaken-> S,U∈Γ refl n<l)

  <:>∈-weaken : ∀ {n S U Γ} →
                  n <: S >: U ∈ Γ →
                ∀ Γ₁ Γ₂ B′ →
                  Γ ≡ Γ₁ ‣ Γ₂ →
                  ↑-idx n (length Γ₂) <: S ↑ length Γ₂ >: U ↑ length Γ₂ ∈ Γ₁ ‣ B′ ! ‣ Γ₂ ⇑
  <:>∈-weaken {n} S,U∈Γ _ Γ₂ _ eqΓ with length Γ₂ ≤? n
  ... | yes l≤n = <:>∈-weaken-≤ S,U∈Γ eqΓ l≤n
  ... | no l>n  = <:>∈-weaken-> S,U∈Γ eqΓ (≰⇒> l>n)

  env-lookup-weaken : ∀ {n B} Γ₁ Γ₂ B′ →
                        env-lookup (Γ₁ ‣ Γ₂) n ≡ just B →
                        env-lookup (Γ₁ ‣ B′ ! ‣ Γ₂ ⇑) (↑-idx n (length Γ₂)) ≡ just (B ↑↑ Γ₂)
  env-lookup-weaken Γ₁ Γ₂ B′ B∈Γ = <:>∈⇒lookup (<:>∈-weaken (lookup⇒<:>∈ B∈Γ) Γ₁ Γ₂ _ refl)                        

  <:′-weakening-gen : ∀ {Γ S U} →
                        Γ ⊢′ S <: U →
                      ∀ Γ₁ Γ₂ B →
                        Γ ≡ Γ₁ ‣ Γ₂ →
                        Γ₁ ‣ B ! ‣ Γ₂ ⇑ ⊢′ S ↑ length Γ₂ <: U ↑ length Γ₂
  <:′-weakening-gen <:⊤ Γ₁ Γ₂ B eqΓ         = <:⊤
  <:′-weakening-gen ⊥<: Γ₁ Γ₂ B eqΓ         = ⊥<:
  <:′-weakening-gen refl Γ₁ Γ₂ B eqΓ        = refl
  <:′-weakening-gen (var₁ {_} {n} B∈Γ U′<:U) Γ₁ Γ₂ B refl
    rewrite ↑-var n (length Γ₂)             = var₁ (env-lookup-weaken Γ₁ Γ₂ B B∈Γ)
                                                   (<:′-weakening-gen U′<:U Γ₁ Γ₂ B refl)
  <:′-weakening-gen (var₂ {_} {n} B∈Γ S<:S′) Γ₁ Γ₂ B refl
    rewrite ↑-var n (length Γ₂)             = var₂ (env-lookup-weaken Γ₁ Γ₂ B B∈Γ)
                                                   (<:′-weakening-gen S<:S′ Γ₁ Γ₂ B refl)
  <:′-weakening-gen (Π<: {S₁′ = S₁′} {S₂′} <:₁ <:₂ U<:U′) Γ₁ Γ₂ B eqΓ
                                            = Π<: (<:′-weakening-gen <:₁ Γ₁ Γ₂ B eqΓ)
                                                  (<:′-weakening-gen <:₂ Γ₁ Γ₂ B eqΓ)
                                                  (<:′-weakening-gen U<:U′ Γ₁ _ B (cong ((S₁′ , S₂′) ∷_) eqΓ))
  <:′-weakening-gen (<:> B∈Γ U′<:U S<:S′) Γ₁ Γ₂ B refl
                                            = <:> (env-lookup-weaken Γ₁ Γ₂ B B∈Γ)
                                                  (<:′-weakening-gen U′<:U Γ₁ Γ₂ B refl)
                                                  (<:′-weakening-gen S<:S′ Γ₁ Γ₂ B refl)

  <:′-weakening : ∀ {Γ₁ Γ₂ S U} B →
                    Γ₁ ‣ Γ₂ ⊢′ S <: U →
                    Γ₁ ‣ B ! ‣ Γ₂ ⇑ ⊢′ S ↑ length Γ₂ <: U ↑ length Γ₂
  <:′-weakening B S<:U = <:′-weakening-gen S<:U _ _ B refl

  <:′-weakening-hd : ∀ {Γ S U} S′ U′ →
                       Γ ⊢′ S <: U →
                       Γ ‣ S′ , U′ ‼ ⊢′ S ↑ 0 <: U ↑ 0
  <:′-weakening-hd S′ U′ = <:′-weakening {Γ₂ = []} (S′ , U′)

  <:′-weakening′ : ∀ {Γ S U} Γ′ →
               Γ ⊢′ S <: U →
               Γ ‣ Γ′ ⊢′ repeat (length Γ′) (_↑ 0) S <: repeat (length Γ′) (_↑ 0) U
  <:′-weakening′ [] S<:U               = S<:U
  <:′-weakening′ ((S′ , U′) ∷ Γ′) S<:U = <:′-weakening-hd S′ U′ (<:′-weakening′ Γ′ S<:U)

  module Transitivity where
    infix 4 _≺:[_]_
    data _≺:[_]_ : Env → ℕ → Env → Set where
      ≺[_,_]_,_ : ∀ {Γ S′ U′} S U →
                    Γ ⊢′ S <: S′ →
                    Γ ⊢′ U′ <: U → Γ ‣ S , U ‼ ≺:[ 0 ] Γ ‣ S′ , U′ ‼
      _∷_       : ∀ {Γ₁ n Γ₂} B → Γ₁ ≺:[ n ] Γ₂ → Γ₁ ‣ B ! ≺:[ suc n ] Γ₂ ‣ B !

    <:∈-find : ∀ {x S U Γ Γ′ n} →
                 x <: S >: U ∈ Γ →
                 Γ′ ≺:[ n ] Γ →
                 x ≡ n × (∃₂ λ S′ U′ → n <: S′ >: U′ ∈ Γ′ × Γ′ ⊢′ S′ <: S × Γ′ ⊢′ U <: U′) ⊎
                 x ≢ n × x <: S >: U ∈ Γ′
    <:∈-find hd (≺[ S′ , U′ ] S′<:S , U<:U′)               = inj₁ ( refl , S′ ↑ 0 , U′ ↑ 0 , hd
                                                                  , <:′-weakening-hd S′ U′ S′<:S , <:′-weakening-hd S′ U′ U<:U′)
    <:∈-find hd (.(_ , _) ∷ Γ′≺Γ)                          = inj₂ ((λ ()) , hd)
    <:∈-find (tl S,U∈Γ) (≺[ S , U ] S′<:S , U<:U′)         = inj₂ ((λ ()) , tl S,U∈Γ)
    <:∈-find (tl S,U∈Γ) ((T₁ , T₂) ∷ Γ′≺Γ) with <:∈-find S,U∈Γ Γ′≺Γ
    ... | inj₁ (refl , S′ , U′ , S′,U′∈Γ′ , S′<:S , U<:U′) = inj₁ ( refl , S′ ↑ 0 , U′ ↑ 0 , tl S′,U′∈Γ′
                                                                  , <:′-weakening-hd T₁ T₂ S′<:S , <:′-weakening-hd T₁ T₂ U<:U′)
    ... | inj₂ (x≢n , S,U∈Γ′)                              = inj₂ ((λ { refl → x≢n refl }) , tl S,U∈Γ′)

    <:∈-find′ : ∀ {x S U Γ Γ′ n} →
                  env-lookup Γ x ≡ just (S , U) →
                  Γ′ ≺:[ n ] Γ →
                  x ≡ n × (∃₂ λ S′ U′ → env-lookup Γ′ n ≡ just (S′ , U′) × Γ′ ⊢′ S′ <: S × Γ′ ⊢′ U <: U′) ⊎
                  x ≢ n × env-lookup Γ′ x ≡ just (S , U)
    <:∈-find′ S,U∈Γ Γ′≺Γ with <:∈-find (lookup⇒<:>∈ S,U∈Γ) Γ′≺Γ
    ... | inj₁ (x≡n , S′ , U′ , S′,U′∈Γ′ , S′<:S , U<:U′) = inj₁ (x≡n , S′ , U′ , <:>∈⇒lookup S′,U′∈Γ′ , S′<:S , U<:U′)
    ... | inj₂ (x≢n , S,U∈Γ′)                             = inj₂ (x≢n , <:>∈⇒lookup S,U∈Γ′)
    
    private
      trans-on : Typ → Set
      trans-on T = ∀ {Γ S U} → Γ ⊢′ S <: T → Γ ⊢′ T <: U → Γ ⊢′ S <: U

      narrow-on : Typ → Typ → Set
      narrow-on T₁ T₂ = ∀ {Γ Γ′ n S U} →
                          Γ ⊢′ S <: U →
                          Γ′ ≺:[ n ] Γ →
                          env-lookup Γ n ≡ just (T₁ , T₂) →
                          Γ′ ⊢′ S <: U

      case : Set
      case = Typ ⊎ (Typ × Typ)

      case-measure : case → ℕ
      case-measure (inj₁ T)       = Typ-measure T
      case-measure (inj₂ (S , U)) = Typ-measure S ⊔ Typ-measure U

      prop : case → Set
      prop (inj₁ T)       = trans-on T
      prop (inj₂ (S , U)) = narrow-on S U

    <:′-trans-rec : ∀ T → (∀ r → case-measure r < Typ-measure T → prop r) → trans-on T
    <:′-trans-rec ⊤ rec S<:T <:⊤                                     = S<:T
    <:′-trans-rec ⊤ rec S<:T refl                                    = S<:T
    <:′-trans-rec ⊤ rec S<:T (var₁ B∈Γ T<:U)                         = var₁ B∈Γ (<:′-trans-rec ⊤ rec S<:T T<:U)
    <:′-trans-rec ⊤ rec S<:T (<:> B∈Γ ⊤<:U′ T′<:U)                   = <:> B∈Γ (<:′-trans-rec ⊤ rec S<:T ⊤<:U′) T′<:U
    <:′-trans-rec ⊥ rec ⊥<: T<:U                                     = T<:U
    <:′-trans-rec ⊥ rec refl T<:U                                    = T<:U
    <:′-trans-rec ⊥ rec (var₂ B∈Γ S<:T) T<:U                         = var₂ B∈Γ (<:′-trans-rec ⊥ rec S<:T T<:U)
    <:′-trans-rec ⊥ rec (<:> B∈Γ U′<:U S<:S′) T<:U                   = <:> B∈Γ U′<:U (<:′-trans-rec ⊥ rec S<:S′ T<:U)
    <:′-trans-rec (var x) rec ⊥<: T<:U                               = ⊥<:
    <:′-trans-rec (var x) rec refl T<:U                              = T<:U
    <:′-trans-rec (var x) rec (var₁ B∈Γ S<:T) <:⊤                    = <:⊤
    <:′-trans-rec (var x) rec (var₁ B∈Γ S<:T) refl                   = var₁ B∈Γ S<:T
    <:′-trans-rec (var x) rec (var₁ B∈Γ S<:T) (var₁ B′∈Γ T<:U)       = var₁ B′∈Γ (<:′-trans-rec (var x) rec (var₁ B∈Γ S<:T) T<:U)
    <:′-trans-rec (var x) rec (var₁ B∈Γ S<:T) (var₂ B′∈Γ T<:U)
      with ≡.trans (≡.sym B∈Γ) B′∈Γ
    ... | refl                                                       = <:> B′∈Γ S<:T T<:U
    <:′-trans-rec (var x) rec (var₁ B∈Γ S<:T) (<:> B′∈Γ U′<:U S<:S′) = <:> B′∈Γ (<:′-trans-rec (var x) rec (var₁ B∈Γ S<:T) U′<:U) S<:S′
    <:′-trans-rec (var x) rec (var₂ B∈Γ S<:T) T<:U                   = var₂ B∈Γ (<:′-trans-rec (var x) rec S<:T T<:U)
    <:′-trans-rec (var x) rec (<:> B∈Γ U′<:U S<:S′) T<:U             = <:> B∈Γ U′<:U (<:′-trans-rec (var x) rec S<:S′ T<:U)    
    <:′-trans-rec (Π<: S₁ >: S₂ ∙ U) rec ⊥<: T<:U                    = ⊥<:
    <:′-trans-rec (Π<: S₁ >: S₂ ∙ U) rec refl T<:U                   = T<:U
    <:′-trans-rec (Π<: S₁ >: S₂ ∙ U) rec (var₂ B∈Γ S<:T) T<:U        = var₂ B∈Γ (<:′-trans-rec (Π<: S₁ >: S₂ ∙ U) rec S<:T T<:U)
    <:′-trans-rec (Π<: S₁ >: S₂ ∙ U) rec (<:> B∈Γ U′<:U S<:S′) T<:U  = <:> B∈Γ U′<:U (<:′-trans-rec (Π<: S₁ >: S₂ ∙ U) rec S<:S′ T<:U)

    <:′-trans-rec (Π<: S₁ >: S₂ ∙ U) rec (Π<: S₁<:S₁′ S₂′<:S₂ U<:U′) <:⊤
      = <:⊤
    <:′-trans-rec (Π<: S₁ >: S₂ ∙ U) rec (Π<: S₁<:S₁′ S₂′<:S₂ U<:U′) refl
      = Π<: S₁<:S₁′ S₂′<:S₂ U<:U′
    <:′-trans-rec (Π<: S₁ >: S₂ ∙ U) rec (Π<: S₁<:S₁′ S₂′<:S₂ U<:U′) (var₁ B∈Γ T<:U)
      = var₁ B∈Γ (<:′-trans-rec (Π<: S₁ >: S₂ ∙ U) rec (Π<: S₁<:S₁′ S₂′<:S₂ U<:U′) T<:U)
    <:′-trans-rec (Π<: S₁ >: S₂ ∙ U) rec (Π<: S₁<:S₁′ S₂′<:S₂ U<:U′) (<:> B∈Γ U′<:U S<:S′)
      = <:> B∈Γ (<:′-trans-rec (Π<: S₁ >: S₂ ∙ U) rec (Π<: S₁<:S₁′ S₂′<:S₂ U<:U′) U′<:U) S<:S′
    <:′-trans-rec (Π<: S₁ >: S₂ ∙ U) rec (Π<: S₁<:S₁′ S₂′<:S₂ U′<:U) (Π<: S₁″<:S₁ S₂<:S₂″ U<:U″)
      = Π<: (<:′-trans-rec S₁ (λ r r<S₁ → rec r (≤-step (≤-stepsʳ _ (≤-stepsʳ _ r<S₁))))
                           S₁″<:S₁ S₁<:S₁′)
            (<:′-trans-rec S₂ (λ r r<S₂ → rec r (≤-step (≤-stepsʳ _ (≤-stepsˡ (Typ-measure S₁) r<S₂))))
                           S₂′<:S₂ S₂<:S₂″)
            (<:′-trans-rec U (λ r r≤U → rec r (≤-step (≤-stepsˡ (Typ-measure S₁ + Typ-measure S₂) r≤U)))
                           (rec (inj₂ (S₁ ↑ 0 , S₂ ↑ 0)) 1+S₁⊔S₂≤1+Π U′<:U
                                (≺[ _ , _ ] S₁″<:S₁ , S₂<:S₂″) refl)
                           U<:U″)
      where open ≤-Reasoning
            1+S₁⊔S₂≤1+Π : suc (Typ-measure (S₁ ↑ 0) ⊔ Typ-measure (S₂ ↑ 0)) ≤
                          suc (Typ-measure S₁ + Typ-measure S₂ + Typ-measure U)
            1+S₁⊔S₂≤1+Π = s≤s $ begin
              Typ-measure (S₁ ↑ 0) ⊔ Typ-measure (S₂ ↑ 0)     ≡⟨ cong₂ _⊔_ (Typ-measure-↑ S₁ 0) (Typ-measure-↑ S₂ 0) ⟩
              Typ-measure S₁ ⊔ Typ-measure S₂                 ≤⟨ m⊔n≤m+n (Typ-measure S₁) _ ⟩
              Typ-measure S₁ + Typ-measure S₂                 ≤⟨ ≤-stepsʳ (Typ-measure U) ≤-refl ⟩
              Typ-measure S₁ + Typ-measure S₂ + Typ-measure U ∎

    <:′-narrow-rec : ∀ T₁ T₂ →
                       (∀ T → case-measure (inj₁ T) ≤ case-measure (inj₂ (T₁ , T₂)) → trans-on T) →
                       narrow-on T₁ T₂
    <:′-narrow-rec T₁ T₂ trans <:⊤ Γ′≺Γ B∈Γ  = <:⊤
    <:′-narrow-rec T₁ T₂ trans ⊥<: Γ′≺Γ B∈Γ  = ⊥<:
    <:′-narrow-rec T₁ T₂ trans refl Γ′≺Γ B∈Γ = refl
    <:′-narrow-rec T₁ T₂ trans (var₁ B′∈Γ S<:U) Γ′≺Γ B∈Γ
      with <:∈-find′ B′∈Γ Γ′≺Γ
    ...  | inj₁ (refl , S′ , U′ , S′,U′∈Γ′ , S′<:S , U<:U′)
         with ≡.trans (≡.sym B′∈Γ) B∈Γ
    ...     | refl            = trans T₂ (m≤n⇒m≤o⊔n _ ≤-refl)
                                         (<:′-narrow-rec T₁ T₂ trans S<:U Γ′≺Γ B∈Γ)
                                         (var₁ S′,U′∈Γ′ U<:U′)
    <:′-narrow-rec T₁ T₂ trans (var₁ B′∈Γ S<:U) Γ′≺Γ B∈Γ
        | inj₂ (x≢n , S,U∈Γ′) = var₁ S,U∈Γ′ (<:′-narrow-rec T₁ T₂ trans S<:U Γ′≺Γ B∈Γ)
    <:′-narrow-rec T₁ T₂ trans (var₂ B′∈Γ S<:U) Γ′≺Γ B∈Γ
      with <:∈-find′ B′∈Γ Γ′≺Γ
    ...  | inj₁ (refl , S′ , U′ , S′,U′∈Γ′ , S′<:S , U<:U′)
         with ≡.trans (≡.sym B′∈Γ) B∈Γ
    ...     | refl            = trans T₁ (m≤n⇒m≤n⊔o _ ≤-refl)
                                      (var₂ S′,U′∈Γ′ S′<:S)
                                      (<:′-narrow-rec T₁ T₂ trans S<:U Γ′≺Γ B∈Γ)
    <:′-narrow-rec T₁ T₂ trans (var₂ B′∈Γ S<:U) Γ′≺Γ B∈Γ
        | inj₂ (x≢n , S,U∈Γ′) = var₂ S,U∈Γ′ (<:′-narrow-rec T₁ T₂ trans S<:U Γ′≺Γ B∈Γ)
    <:′-narrow-rec T₁ T₂ trans {Γ} {Γ′} {n} (Π<: {S₁′ = S₁′} {S₂′} S₁′<:S₁ S₂<:S₂′ U<:U′) Γ′≺Γ B∈Γ
                              = Π<: (<:′-narrow-rec T₁ T₂ trans S₁′<:S₁ Γ′≺Γ B∈Γ)
                                    (<:′-narrow-rec T₁ T₂ trans S₂<:S₂′ Γ′≺Γ B∈Γ)
                                    (<:′-narrow-rec (T₁ ↑ 0) (T₂ ↑ 0)
                                                    (λ T T≤T₁⊔T₂ →
                                                       trans T (≤-trans T≤T₁⊔T₂ (≤-reflexive
                                                                        (≡.cong₂ _⊔_ (Typ-measure-↑ T₁ 0) (Typ-measure-↑ T₂ 0)))))
                                                    U<:U′ (_ ∷ Γ′≺Γ)
                                                    (<:>∈⇒lookup (tl {n} {B = S₁′ , S₂′} {Γ} (lookup⇒<:>∈ B∈Γ))))
    <:′-narrow-rec T₁ T₂ trans (<:> B′∈Γ U′<:U S<:S′) Γ′≺Γ B∈Γ
      with <:∈-find′ B′∈Γ Γ′≺Γ
    ...  | inj₁ (refl , _ , _ , S″,U″∈Γ′ , S″<:S , U<:U″)
         with ≡.trans (≡.sym B′∈Γ) B∈Γ
    ...     | refl                                  = <:> S″,U″∈Γ′
                                                          (trans T₂ (m≤n⇒m≤o⊔n _ ≤-refl)
                                                                    (<:′-narrow-rec T₁ T₂ trans U′<:U Γ′≺Γ B∈Γ) U<:U″)
                                                          (trans T₁ (m≤n⇒m≤n⊔o _ ≤-refl)
                                                                    S″<:S (<:′-narrow-rec T₁ T₂ trans S<:S′ Γ′≺Γ B∈Γ))
    <:′-narrow-rec T₁ T₂ trans (<:> B′∈Γ U′<:U S<:S′) Γ′≺Γ B∈Γ
        | inj₂ (x≢n , S,U∈Γ′)                       = <:> S,U∈Γ′
                                                          (<:′-narrow-rec T₁ T₂ trans U′<:U Γ′≺Γ B∈Γ)
                                                          (<:′-narrow-rec T₁ T₂ trans S<:S′ Γ′≺Γ B∈Γ)

    <:′-trans-narrow : ∀ r → prop r
    <:′-trans-narrow = wfRec prop aux
      where open Measure <-wellFounded case-measure
            aux : ∀ r → (∀ r′ → case-measure r′ < case-measure r → prop r′) → prop r
            aux (inj₁ T) rec         = <:′-trans-rec T rec
            aux (inj₂ (T₁ , T₂)) rec = <:′-narrow-rec T₁ T₂
                                                      λ T T<:⊔ →
                                                        <:′-trans-rec T λ r′ r′<T → rec r′ (≤-trans r′<T T<:⊔)

    <:′-trans : ∀ {T} → trans-on T
    <:′-trans {T} = <:′-trans-narrow (inj₁ T)

    <:′-narrow : ∀ {T₁ T₂} → narrow-on T₁ T₂
    <:′-narrow {T₁} {T₂} = <:′-trans-narrow (inj₂ (T₁ , T₂))

  open Transitivity using (<:′-trans ; <:′-narrow) public

  <:′⇒<: : ∀ {Γ S U} → Γ ⊢′ S <: U → Γ ⊢ S <: U
  <:′⇒<: <:⊤                        = <:⊤
  <:′⇒<: ⊥<:                        = ⊥<:
  <:′⇒<: refl                       = refl
  <:′⇒<: (var₁ B∈Γ S<:U)            = tran (<:′⇒<: S<:U) (var₁ B∈Γ)
  <:′⇒<: (var₂ B∈Γ S<:U)            = tran (var₂ B∈Γ) (<:′⇒<: S<:U)
  <:′⇒<: (Π<: S₁′<:S₁ S₂<:S₂ U<:U′) = Π<: (<:′⇒<: S₁′<:S₁) (<:′⇒<: S₂<:S₂) (<:′⇒<: U<:U′)
  <:′⇒<: (<:> B∈Γ U′<:U S<:S′)      = tran (tran (<:′⇒<: U′<:U) (var₁ B∈Γ)) (tran (var₂ B∈Γ) (<:′⇒<: S<:S′))

  <:⇒<:′ : ∀ {Γ S U} → Γ ⊢ S <: U → Γ ⊢′ S <: U
  <:⇒<:′ <:⊤                        = <:⊤
  <:⇒<:′ ⊥<:                        = ⊥<:
  <:⇒<:′ refl                       = refl
  <:⇒<:′ (tran S<:T T<:U)           = <:′-trans (<:⇒<:′ S<:T) (<:⇒<:′ T<:U)
  <:⇒<:′ (var₁ B∈Γ)                 = var₁ B∈Γ refl
  <:⇒<:′ (var₂ B∈Γ)                 = var₂ B∈Γ refl
  <:⇒<:′ (Π<: S₁′<:S₁ S₂<:S₂ U<:U′) = Π<: (<:⇒<:′ S₁′<:S₁) (<:⇒<:′ S₂<:S₂) (<:⇒<:′ U<:U′)
  
