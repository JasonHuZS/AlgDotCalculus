{-# OPTIONS --without-K --safe #-}

module DsubsubUndec where

open import Data.List as List
open import Data.List.All
open import Data.Nat as ℕ
open import Data.Maybe as Maybe
open import Data.Product
open import Data.Sum
open import Data.Empty renaming (⊥ to False)
open import Data.Unit using (tt) renaming (⊤ to True)
open import Function

open import Data.Nat.Properties as ℕₚ
open import Data.Maybe.Properties as Maybeₚ
open import Data.Product.Properties

open import Relation.Nullary
open import Relation.Unary
open import Relation.Binary.PropositionalEquality as ≡

open import Dsubsub
open import Utils
open EquivalentModel


module Undecidability where
  open import FsubMinus
  open import FsubMinus2
  open FsubMinus.FsubMinus renaming (Env to Env′ ; _↑_ to _⇑_) hiding (env-lookup)
  
  ⟦_⟧ : Ftyp → Typ
  ⟦ ⊤ ⟧         = ⊤
  ⟦ var x ⟧     = x ∙A
  ⟦ Π<: S ∙ U ⟧ = Π ⟨A<: ⟦ S ⟧ ⟩ ∙ ⟦ U ⟧

  ⟦⟧-covar : ∀ T → Covar ⟦ T ⟧
  ⟦⟧-covar ⊤           = cv⊤
  ⟦⟧-covar (var x)     = cv∙A x
  ⟦⟧-covar (Π<: S ∙ U) = cvΠ (⟦⟧-covar S) (⟦⟧-covar U)

  ⟪_⟫ : Env′ → Env
  ⟪ [] ⟫    = []
  ⟪ T ∷ Γ ⟫ = ⟨A<: ⟦ T ⟧ ⟩ ∷ ⟪ Γ ⟫

  ⟪⟫-contra : ∀ Γ → ContraEnv ⟪ Γ ⟫
  ⟪⟫-contra []      = []
  ⟪⟫-contra (T ∷ Γ) = ctt (⟦⟧-covar T) ∷ ⟪⟫-contra Γ

  covar-irrelevant : Irrelevant Covar
  covar-irrelevant cv⊤ cv⊤               = refl
  covar-irrelevant (cv∙A n) (cv∙A .n)    = refl
  covar-irrelevant (cvΠ S U) (cvΠ S′ U′) = cong₂ cvΠ (covar-irrelevant S S′) (covar-irrelevant U U′)

  module ⟦⟧-Bijectivity where
    open import Function.Bijection
    open import Function.Surjection
    open import Function.Equality

    Typᵢ : Set
    Typᵢ = Σ Typ Covar

    ⟦⟧-func : ≡.setoid Ftyp ⟶ ≡.setoid Typᵢ
    ⟦⟧-func   = record
      { _⟨$⟩_ = < ⟦_⟧ , ⟦⟧-covar >
      ; cong  = ≡.cong < ⟦_⟧ , ⟦⟧-covar >
      }

    ⟦⟧-injective : ∀ {S U} → ⟦ S ⟧ ≡ ⟦ U ⟧ → S ≡ U
    ⟦⟧-injective {⊤} {⊤} refl          = refl
    ⟦⟧-injective {⊤} {var _} ()
    ⟦⟧-injective {⊤} {Π<: _ ∙ _} ()
    ⟦⟧-injective {var x} {⊤} ()
    ⟦⟧-injective {var x} {var .x} refl = refl
    ⟦⟧-injective {var x} {Π<: _ ∙ _} ()
    ⟦⟧-injective {Π<: _ ∙ _} {⊤} ()
    ⟦⟧-injective {Π<: _ ∙ _} {var x} ()
    ⟦⟧-injective {Π<: S ∙ U} {Π<: S′ ∙ U′} eq
      with ⟦ S ⟧ | ⟦ S′ ⟧ | ⟦ U ⟧ | ⟦ U′ ⟧
         | ⟦⟧-injective {S} {S′} | ⟦⟧-injective {U} {U′}
    ⟦⟧-injective {Π<: S ∙ U} {Π<: S′ ∙ U′} refl
         | _ | _ | _ | _ | rec₁ | rec₂ = cong₂ Π<:_∙_ (rec₁ refl) (rec₂ refl)

    ⟦⟧-func-injective : ∀ {S U} → (Typᵢ ∋ (⟦ S ⟧ , ⟦⟧-covar S)) ≡ (⟦ U ⟧ , ⟦⟧-covar U) → S ≡ U
    ⟦⟧-func-injective {S} {U} eq
      with ⟦ S ⟧ | ⟦⟧-covar S | ⟦ U ⟧ | ⟦⟧-covar U
         | ⟦⟧-injective {S} {U}
    ⟦⟧-func-injective {S} {U} refl
         | _ | _ | _ | _ | inj = inj refl

    infix 5 ⟦_⟧⁻¹
    ⟦_⟧⁻¹ : Typᵢ → Ftyp
    ⟦ _ , cv⊤ ⟧⁻¹     = ⊤
    ⟦ _ , cv∙A n ⟧⁻¹  = var n
    ⟦ _ , cvΠ S U ⟧⁻¹ = Π<: ⟦ -, S ⟧⁻¹ ∙ ⟦ -, U ⟧⁻¹

    ⟦⟧-func-inv : ≡.setoid Typᵢ ⟶ ≡.setoid Ftyp
    ⟦⟧-func-inv = record
      { _⟨$⟩_   = ⟦_⟧⁻¹
      ; cong    = ≡.cong ⟦_⟧⁻¹
      }

    ⟦⟧-left-inverse-⟦⟧⁻¹ : ∀ {T} (iT : Covar T) → (⟦ ⟦ -, iT ⟧⁻¹ ⟧ , ⟦⟧-covar ⟦ -, iT ⟧⁻¹) ≡ (Typᵢ ∋ (-, iT))
    ⟦⟧-left-inverse-⟦⟧⁻¹ cv⊤           = refl
    ⟦⟧-left-inverse-⟦⟧⁻¹ (cv∙A x)      = refl
    ⟦⟧-left-inverse-⟦⟧⁻¹ (cvΠ S U)
      with ⟦ ⟦ -, S ⟧⁻¹ ⟧         | ⟦ ⟦ -, U ⟧⁻¹ ⟧ 
         | ⟦⟧-covar ⟦ -, S ⟧⁻¹    | ⟦⟧-covar ⟦ -, U ⟧⁻¹
         | ⟦⟧-left-inverse-⟦⟧⁻¹ S | ⟦⟧-left-inverse-⟦⟧⁻¹ U
    ...  | _ | _ | _ | _ | refl | refl = refl

    ⟦⟧-bijective : Bijective ⟦⟧-func
    ⟦⟧-bijective   = record
      { injective  = ⟦⟧-func-injective
      ; surjective = record
        { from             = ⟦⟧-func-inv
        ; right-inverse-of = λ { (_ , T) → ⟦⟧-left-inverse-⟦⟧⁻¹ T }
        }
      }

    open Bijective ⟦⟧-bijective
    
    ⟦⟧⁻¹-left-inverse-⟦⟧ : ∀ {T} (cT : Covar ⟦ T ⟧) → ⟦ -, cT ⟧⁻¹ ≡ T
    ⟦⟧⁻¹-left-inverse-⟦⟧ {T} cT with left-inverse-of T
    ... | eq rewrite covar-irrelevant cT (⟦⟧-covar T) = eq

  open ⟦⟧-Bijectivity using (⟦⟧-injective ; ⟦_⟧⁻¹ ; ⟦⟧-left-inverse-⟦⟧⁻¹ ; ⟦⟧⁻¹-left-inverse-⟦⟧) public

  ⟦⟦⟧⁻¹⟧⇒id : ∀ {T} (cT : Covar T) → ⟦ ⟦ -, cT ⟧⁻¹ ⟧ ≡ T
  ⟦⟦⟧⁻¹⟧⇒id cT = ,-injectiveˡ $ ⟦⟧-left-inverse-⟦⟧⁻¹ cT

  ⟦⟧-↑-comm : ∀ T n → ⟦ T ⟧ ↑ n ≡ ⟦ T ⇑ n ⟧
  ⟦⟧-↑-comm ⊤ n = refl
  ⟦⟧-↑-comm (var x) n with n ≤? x
  ... | yes _   = refl
  ... | no _    = refl
  ⟦⟧-↑-comm (Π<: S ∙ U) n
    rewrite ⟦⟧-↑-comm S n | ⟦⟧-↑-comm U (suc n)
                = refl

  repeat-⊥↑ : ∀ n m → repeat n (_↑ m) ⊥ ≡ ⊥
  repeat-⊥↑ zero m                          = refl
  repeat-⊥↑ (suc n) m rewrite repeat-⊥↑ n m = refl

  repeat-⊤↑ : ∀ n m → repeat n (_↑ m) ⊤ ≡ ⊤
  repeat-⊤↑ zero m                          = refl
  repeat-⊤↑ (suc n) m rewrite repeat-⊤↑ n m = refl

  repeat-∙A↑ : ∀ n m x → ∃ λ x′ → repeat n (_↑ m) (x ∙A) ≡ x′ ∙A
  repeat-∙A↑ zero m x = x , refl
  repeat-∙A↑ (suc n) m x with repeat-∙A↑ n m x
  ... | x′ , eq
      with m ≤? x′ | cong (_↑ m) eq
  ...    | yes m≤x′ | eq′ = suc x′ , eq′
  ...    | no m>x′  | eq′ = x′ , eq′

  repeat-Π↑ : ∀ n m S U → repeat n (_↑ m) (Π S ∙ U) ≡ Π repeat n (_↑ m) S ∙ repeat n (_↑ suc m) U
  repeat-Π↑ zero m S U                              = refl
  repeat-Π↑ (suc n) m S U rewrite repeat-Π↑ n m S U = refl

  repeat-⟨A<:⟩↑ : ∀ n m T → repeat n (_↑ m) ⟨A<: T ⟩ ≡ ⟨A<: repeat n (_↑ m) T ⟩
  repeat-⟨A<:⟩↑ zero m T                                = refl
  repeat-⟨A<:⟩↑ (suc n) m T rewrite repeat-⟨A<:⟩↑ n m T = refl

  repeat-↑-inv : ∀ n m T T′ → repeat n (_↑ m) T ≡ ⟨A<: T′ ⟩ → ∃ λ U → T ≡ ⟨A<: U ⟩ × T′ ≡ repeat n (_↑ m) U
  repeat-↑-inv n m ⊤ T′ eq
    rewrite repeat-⊤↑ n m     = case eq of (λ ())
  repeat-↑-inv n m ⊥ T′ eq
    rewrite repeat-⊥↑ n m     = case eq of (λ ())
  repeat-↑-inv n m (x ∙A) T′ eq with repeat-∙A↑ n m x
  ... | _ , eq′ rewrite eq′   = case eq of (λ ())
  repeat-↑-inv n m (Π S ∙ U) T′ eq
    rewrite repeat-Π↑ n m S U = case eq of (λ ())
  repeat-↑-inv n m ⟨A<: U ⟩ T′ eq
    rewrite repeat-⟨A<:⟩↑ n m U with eq
  ... | refl                  = U , refl , refl

  ↦∈⇒<:∈ : ∀ {n U Γ} → n ↦ ⟨A<: U ⟩ ∈ ⟪ Γ ⟫ → ∃ λ U′ → U ≡ ⟦ U′ ⟧ × n <: U′ ∈ Γ
  ↦∈⇒<:∈ U∈Γ = aux U∈Γ refl refl
    where aux : ∀ {n T Γ} →
                  n ↦ T ∈ Γ →
                ∀ {Γ′ U} →
                  T ≡ ⟨A<: U ⟩ →
                  Γ ≡ ⟪ Γ′ ⟫ →
                  ∃ λ U′ → U ≡ ⟦ U′ ⟧ × n <: U′ ∈ Γ′
          aux hd {[]} {U} eqT ()
          aux hd {T ∷ Γ′} {.(⟦ T ⟧ ↑ 0)} refl refl
            rewrite ⟦⟧-↑-comm T 0 = T ⇑ 0 , refl , hd
          aux (tl T∈Γ) {[]} {U} eqT ()
          aux (tl {T = ⊤} T∈Γ) {_ ∷ Γ′} {U} () refl
          aux (tl {T = ⊥} T∈Γ) {_ ∷ Γ′} {U} () refl
          aux (tl {T = _ ∙A} T∈Γ) {_ ∷ Γ′} {U} () refl
          aux (tl {T = Π _ ∙ _} T∈Γ) {_ ∷ Γ′} {U} () refl
          aux (tl {T = ⟨A<: U ⟩} T∈Γ) {_ ∷ Γ′} {.(U ↑ 0)} refl refl
            with aux T∈Γ refl refl
          ... | U′ , refl , U′∈Γ′
            rewrite ⟦⟧-↑-comm U′ 0 = U′ ⇑ 0 , refl , tl U′∈Γ′

  env-lookup⇒<:∈ : ∀ {Γ n T} (cT : Covar T) →
                     env-lookup ⟪ Γ ⟫ n ≡ just ⟨A<: T ⟩ →
                     n <: ⟦ -, cT ⟧⁻¹ ∈ Γ
  env-lookup⇒<:∈ {Γ} {n} cT T∈Γ′ with ↦∈⇒<:∈ {n} {_} {Γ} (lookup⇒↦∈ T∈Γ′)
  ... | U , refl , U∈Γ rewrite ⟦⟧⁻¹-left-inverse-⟦⟧ cT = U∈Γ  

  D<<:′⇒F<: : ∀ {Γ′ S U} → Γ′ ⊢′ S <: U →
              ∀ {Γ} →
                Γ′ ≡ ⟪ Γ ⟫ → (cS : Covar S) → (cU : Covar U) →
                Γ ⊢F ⟦ -, cS ⟧⁻¹ <: ⟦ -, cU ⟧⁻¹
  D<<:′⇒F<: <:⊤ {Γ′} eqΓ cS cv⊤                                = ftop
  D<<:′⇒F<: ⊥<: {Γ′} eqΓ () cU
  D<<:′⇒F<: refl {Γ′} eqΓ cS cU
    rewrite covar-irrelevant cS cU                             = <:-refl _ _
  D<<:′⇒F<: (bnd S<:U) {Γ′} eqΓ () cU
  D<<:′⇒F<: (sel T∈Γ ⊥<: ¬b S<:U) {Γ′} refl (cv∙A n) cU
    with lookupContraEnv T∈Γ (⟪⟫-contra Γ′)
  ... | ()
  D<<:′⇒F<: (sel T∈Γ refl ¬b S<:U) {Γ′} refl (cv∙A n) cU
    with lookupContraEnv T∈Γ (⟪⟫-contra Γ′)
  ... | ctt cS                                                 = fbinds (env-lookup⇒<:∈ cS T∈Γ) (D<<:′⇒F<: S<:U refl cS cU)
  D<<:′⇒F<: (sel T∈Γ (bnd T<:B) ¬b S<:U) {Γ′} refl (cv∙A n) cU = ⊥-elim ¬b
  D<<:′⇒F<: (sel T∈Γ (sel T∈Γ₁ T<:B x T<:B₁) ¬b S<:U) {Γ′} refl (cv∙A n) cU
    with lookupContraEnv T∈Γ (⟪⟫-contra Γ′)
  ... | ()
  D<<:′⇒F<: (Π<: refl U<:U′) {Γ′} eqΓ (cvΠ cS cU) (cvΠ cS′ cU′)
                                                               = fall (D<<:′⇒F<: refl eqΓ cS′ cS) (D<<:′⇒F<: U<:U′ (eq-cons-Γ _ eqΓ) cU cU′)
    where eq-cons-Γ : ∀ {Γ T} (cT : Covar T) → Γ ≡ ⟪ Γ′ ⟫ →
                        Γ ‣ ⟨A<: T ⟩ ! ≡ ⟨A<: ⟦ ⟦ T , cT ⟧⁻¹ ⟧ ⟩ ∷ ⟪ Γ′ ⟫
          eq-cons-Γ cT eqΓ′ rewrite ⟦⟦⟧⁻¹⟧⇒id cT               = cong (⟨A<: _ ⟩ ∷_) eqΓ′
  D<<:′⇒F<: (Π<: (bnd S′<:S) U<:U′) {Γ′} eqΓ (cvΠ cS cU) (cvΠ cS′ cU′)
                                                               = fall (D<<:′⇒F<: S′<:S eqΓ cS′ cS) (D<<:′⇒F<: U<:U′ (eq-cons-Γ _ eqΓ) cU cU′)
    where eq-cons-Γ : ∀ {Γ T} (cT : Covar T) → Γ ≡ ⟪ Γ′ ⟫ →
                        Γ ‣ ⟨A<: T ⟩ ! ≡ ⟨A<: ⟦ ⟦ T , cT ⟧⁻¹ ⟧ ⟩ ∷ ⟪ Γ′ ⟫
          eq-cons-Γ cT eqΓ′ rewrite ⟦⟦⟧⁻¹⟧⇒id cT               = cong (⟨A<: _ ⟩ ∷_) eqΓ′

  D<<:⇒F<: : ∀ {Γ S U} → ⟪ Γ ⟫ ⊢ ⟦ S ⟧ <: ⟦ U ⟧ → Γ ⊢F S <: U
  D<<:⇒F<: {_} {S} {U} S<:U
    with D<<:′⇒F<: (<:⇒<:′ S<:U) refl (⟦⟧-covar _) (⟦⟧-covar _)
  ... | S<:U′
    rewrite ⟦⟧⁻¹-left-inverse-⟦⟧ (⟦⟧-covar S)
          | ⟦⟧⁻¹-left-inverse-⟦⟧ (⟦⟧-covar U) = S<:U′

  <:∈⇒env-lookup : ∀ {n T Γ} → n <: T ∈ Γ → env-lookup ⟪ Γ ⟫ n ≡ just ⟨A<: ⟦ T ⟧ ⟩
  <:∈⇒env-lookup (hd {T})
    rewrite ⟦⟧-↑-comm T 0       = refl
  <:∈⇒env-lookup (tl {n} {T} {_} {Γ} T∈Γ)
    with lookupOpt ⟪ Γ ⟫ n | <:∈⇒env-lookup T∈Γ
  <:∈⇒env-lookup (tl {n} {T} {_} {Γ} T∈Γ) | nothing | ()
  <:∈⇒env-lookup (tl {n} {T} {_} {Γ} T∈Γ) | just T′ | eq
    rewrite sym (⟦⟧-↑-comm T 0) = cong just (cong (_↑ 0) (just-injective eq))

  F<:⇒D<<: : ∀ {Γ S U} → Γ ⊢F S <: U → ⟪ Γ ⟫ ⊢ ⟦ S ⟧ <: ⟦ U ⟧
  F<:⇒D<<: ftop               = <:⊤
  F<:⇒D<<: fvrefl             = refl
  F<:⇒D<<: (fbinds T∈Γ T<:U)  = sel (<:∈⇒env-lookup T∈Γ) (bnd (F<:⇒D<<: T<:U))
  F<:⇒D<<: (fall S′<:S U<:U′) = Π<: (bnd (F<:⇒D<<: S′<:S)) (F<:⇒D<<: U<:U′)
