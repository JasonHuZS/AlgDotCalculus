## Decidability Analysis in Agda

This directory contains Agda source files which proofs the undecidability of various
DOT calculi. This directory mainly corresponds to chapter 3 in the thesis, but also
contains undecidability proof of `D/\` in chapter 5. Not all contents in this
directory are discussed in the thesis.

The naming convention used here is De-Bruijn indices.


### Definitions

| Definition in thesis | file               | Correspondences                                            | Comments                                     |
|----------------------|--------------------|------------------------------------------------------------|----------------------------------------------|
| 3.1                  | DsubFull.agda      | `_⊢_∷_` and `_⊢_<:_`                                       |                                              |
| Lemma 3.1            | Dsub.agda          | `_⊢_<:_`                                                   | unravelled D<:                               |
| 3.6                  | FsubMinus.agda     | `FsubMinus._⊢F_<:_` and `⟦_⟧`, `⟪_⟫` in `FsubMinusToDsubR` | `⟦_⟧` and `⟪_⟫` are interpretation functions |
| 3.7                  | DsubDef.agda       | `Covar`                                                    |                                              |
| 3.8                  | DsubDef.agda       | `Contra`                                                   |                                              |
| 3.9                  | DsubDef.agda       | `ContraEnv`                                                |                                              |
| 3.10                 | DsubDef.agda       | `InvertibleEnv`                                            |                                              |
| 3.12                 | FsubMinusStep.agda | `_⊢F_↗_` in `InductiveDef`                                 |                                              |
| 3.14                 | FsubsupMinus.agda  | `_⊢_<:_`                                                   |                                              |
| 3.16                 | FsubsupMinus.agda  | `_⊢′_<:_`                                                  |                                              |
| 3.18                 | DsubEquiv.agda     | `_⊢′_<:_`                                                  |                                              |
| 3.19                 | DsubEquiv.agda     | `⟨A:⟩-layer`                                               |                                              |
| 3.20                 | Dsubsub.agda       | `_⊢_<:_`                                                   |                                              |
| 3.21                 | DsubsubAlt.agda    | `_⊢′_<:_`                                                  |                                              |
|----------------------|--------------------|------------------------------------------------------------|----------------------------------------------|
| 5.1                  | Dint.agda          | `_⊢_<:_`                                                   |                                              |


### Conclusions

| Conclusion in thesis | file               | Correspondences                                                             | Comments                                      |
|----------------------|--------------------|-----------------------------------------------------------------------------|-----------------------------------------------|
| Lemma 3.1            | DsubFull.agda      | `<:⇒<:′`                                                                    |                                               |
| Theorem 3.5          | FsubMinus.agda     | `minus⇒deterministic` and `deterministic⇒minus` in `DeterministicFsubSized` |                                               |
| Lemma 3.6            | DsubReduced.agda   | `⟦⟧-bijective`                                                              |                                               |
| Lemma 3.7            | DsubDef.agda       | `contraInvertible`                                                          |                                               |
|                      | DsubReduced.agda   | `⟪⟫-contraEnv`                                                              |                                               |
| Lemma 3.8, 3.9, 3.10 | DsubDef.agda       | properties in `InvertibleProperties`                                        |                                               |
| Theorem 3.11         | DsubNoTrans.agda   | `F<:⇒D<:` and `D<:⇒F<:`                                                     |                                               |
| Theorem 3.12         | FsubMinusStep.agda | `<:′⇒<:` and `<:⇒<:′` in `InductiveDef`                                     |                                               |
| Theorem 3.13         | FsubMinus2.agda    | `<:-trans` and `<:-narrow`                                                  |                                               |
| Theorem 3.17         | FsubsupMinus.agda  | `<:′-trans` and `<:′-narrow`                                                |                                               |
| Theorem 3.18         | FsubsupMinus.agda  | `<:′⇒<:` and `<:⇒<:′`                                                       |                                               |
| Theorem 3.19         | FsubsupMinus2.agda | `F<:⇒F<:>` and `F<:>⇒F<:`                                                   |                                               |
| Theorem 3.20         | DsubEquiv.agda     | `<:′-trans` and `<:′-narrow`                                                | `Transitivity` module contains the full proof |
| Theorem 3.21         | DsubEquiv.agda     | `<:′⇒<:` and `<:⇒<:′`                                                       |                                               |
| Theorem 3.22         | DsubEquiv.agda     | `F<:⇒D<:` and `D<:⇒F<:`                                                     |                                               |
| Theorem 3.23, 3.24   | DsubTermUndec.agda | `F<:⇒typing′` and `typing⇒F<:′`                                             |                                               |
| Theorem 3.25         | DsubsubAlt.agda    | `<:′-trans`                                                                 |                                               |
| Theorem 3.26         | DsubsubAlt.agda    | `<:⇒<:′` and `<:′⇒<:`                                                       |                                               |
| Theorem 3.27         | DsubsubAlt.agda    | `D<<:⇒F<:` and `F<:⇒D<<:`                                                   |                                               |
|----------------------|--------------------|-----------------------------------------------------------------------------|-----------------------------------------------|
| Theorem 5.1          | Dint.agda          | `<:-trans` and `<:-narrow`                                                  | `Transitivity` module contains the full proof |
| Theorem 5.2          | DintUndec.agda     | `D∧⇒F<:` and `F<:⇒D∧`                                                       |                                               |
