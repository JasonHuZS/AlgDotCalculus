{-# OPTIONS --without-K --safe #-}

module DsubsubAlt where

open import Data.List as List
open import Data.List.All
open import Data.Nat as ℕ
open import Data.Maybe as Maybe
open import Data.Product
open import Data.Sum
open import Data.Empty renaming (⊥ to False)
open import Data.Unit using (tt) renaming (⊤ to True)
open import Function

open import Data.Nat.Properties as ℕₚ
open import Data.Maybe.Properties as Maybeₚ
open import Relation.Nullary
open import Relation.Binary.PropositionalEquality as ≡

open import Induction.Nat

open import Dsubsub
open import Utils
open EquivalentModel using (_⇑ ; ↦∈-weaken′)

infix 4 _⊢′_<:_
data _⊢′_<:_ : Env → Typ → Typ → Set where
  <:⊤  : ∀ {Γ T} → Γ ⊢′ T <: ⊤
  ⊥<:  : ∀ {Γ T} → Γ ⊢′ ⊥ <: T
  refl : ∀ {Γ T} → Γ ⊢′ T <: T
  bnd  : ∀ {Γ U U′} →
           (U<:U′ : Γ ⊢′ U <: U′) →
           Γ ⊢′ ⟨A<: U ⟩ <: ⟨A<: U′ ⟩
  sel  : ∀ {Γ n T U} →
           (T∈Γ : env-lookup Γ n ≡ just T) →
           (T<: : Γ ⊢′ T <: ⟨A<: U ⟩) →
           Γ ⊢′ n ∙A <: U
  Π<:  : ∀ {Γ S U S′ U′} →
           Γ ⊢′ S′ <: S →
           Γ ‣ S′ ! ⊢′ U <: U′ →
           Γ ⊢′ Π S ∙ U <: Π S′ ∙ U′


D-measure : ∀ {Γ S U} → Γ ⊢′ S <: U → ℕ
D-measure <:⊤               = 1
D-measure ⊥<:               = 1
D-measure refl              = 1
D-measure (bnd S<:U)        = 1 + D-measure S<:U
D-measure (sel T∈Γ S<:U)    = 1 + D-measure S<:U
D-measure (Π<: S′<:S U<:U′) = 1 + D-measure S′<:S + D-measure U<:U′

D-deriv : Set
D-deriv = Σ (Env × Typ × Typ) λ { (Γ , S , U) → Γ ⊢′ S <: U}

env : D-deriv → Env
env ((Γ , _) , _) = Γ

typ₁ : D-deriv → Typ
typ₁ ((_ , S , U) , _) = S

typ₂ : D-deriv → Typ
typ₂ ((_ , S , U) , _) = U

D-measure-pack : D-deriv → ℕ
D-measure-pack (_ , D) = D-measure D

<:′-weakening-gen : ∀ {Γ S U} →
                      Γ ⊢′ S <: U →
                    ∀ Γ₁ Γ₂ T →
                      Γ ≡ Γ₁ ‣ Γ₂ →
                      Γ₁ ‣ T ! ‣ Γ₂ ⇑ ⊢′ S ↑ length Γ₂ <: U ↑ length Γ₂
<:′-weakening-gen <:⊤ Γ₁ Γ₂ T eqΓ               = <:⊤ 
<:′-weakening-gen ⊥<: Γ₁ Γ₂ T eqΓ               = ⊥<: 
<:′-weakening-gen refl Γ₁ Γ₂ T eqΓ              = refl 
<:′-weakening-gen (bnd S<:U) Γ₁ Γ₂ T eqΓ        = bnd (<:′-weakening-gen S<:U Γ₁ Γ₂ T eqΓ) 
<:′-weakening-gen (sel {_} {n} T∈Γ T<:B) Γ₁ Γ₂ T eqΓ
  rewrite ↑-var n (length Γ₂)                   = sel (↦∈-weaken′ T∈Γ Γ₁ Γ₂ T eqΓ)
                                                      (<:′-weakening-gen T<:B Γ₁ Γ₂ T eqΓ) 
<:′-weakening-gen (Π<: S′<:S U<:U′) Γ₁ Γ₂ T eqΓ = Π<: (<:′-weakening-gen S′<:S Γ₁ Γ₂ T eqΓ)
                                                      (<:′-weakening-gen U<:U′ Γ₁ (_ ∷ Γ₂) T (cong (_ ∷_) eqΓ))

<:′-weakening : ∀ {Γ₁ Γ₂ S U} T →
                  Γ₁ ‣ Γ₂ ⊢′ S <: U →
                  Γ₁ ‣ T ! ‣ Γ₂ ⇑ ⊢′ S ↑ length Γ₂ <: U ↑ length Γ₂
<:′-weakening T S<:U = <:′-weakening-gen S<:U _ _ T refl

<:′-weakening-hd : ∀ {Γ S U} T →
                     Γ ⊢′ S <: U →
                     Γ ‣ T ! ⊢′ S ↑ 0 <: U ↑ 0
<:′-weakening-hd T = <:′-weakening {Γ₂ = []} T

module Transitivity where    
  infix 4 _≺:[_]_
  data _≺:[_]_ : Env → ℕ → Env → Set where
    ≺[_,_] : ∀ {Γ U} S → Γ ⊢′ S <: U → Γ ‣ S ! ≺:[ 0 ] Γ ‣ U !
    _∷_    : ∀ {Γ₁ n Γ₂} T → Γ₁ ≺:[ n ] Γ₂ → Γ₁ ‣ T ! ≺:[ suc n ] Γ₂ ‣ T !

  <:∈-find : ∀ {x T Γ Γ′ n} →
               x ↦ T ∈ Γ →
               Γ′ ≺:[ n ] Γ →
               x ≡ n × (∃ λ T′ → n ↦ T′ ∈ Γ′ × Γ′ ⊢′ T′ <: T) ⊎ x ≢ n × x ↦ T ∈ Γ′
  <:∈-find hd ≺[ T′ , T′<:T ]           = inj₁ (refl , T′ ↑ 0 , hd , <:′-weakening-hd T′ T′<:T)
  <:∈-find hd (T ∷ Γ′≺:Γ)               = inj₂ ((λ ()) , hd)
  <:∈-find (tl T∈Γ) ≺[ T′ , T′<:T ]     = inj₂ ((λ ()) , tl T∈Γ)
  <:∈-find (tl T∈Γ) (S ∷ Γ′≺:Γ) with <:∈-find T∈Γ Γ′≺:Γ
  ... | inj₁ (x≡n , T′ , T′∈Γ′ , T′<:T) = inj₁ (cong suc x≡n , T′ ↑ 0 , tl T′∈Γ′ , <:′-weakening-hd S T′<:T)
  ... | inj₂ (x≢n , T∈Γ′)               = inj₂ (x≢n ∘ suc-injective , tl T∈Γ′)

  <:∈-find′ : ∀ {x T Γ Γ′ n} →
                env-lookup Γ x ≡ just T →
                Γ′ ≺:[ n ] Γ →
                x ≡ n × (∃ λ T′ → env-lookup Γ′ n ≡ just T′ × Γ′ ⊢′ T′ <: T) ⊎ x ≢ n × env-lookup Γ′ x ≡ just T
  <:∈-find′ T∈Γ Γ′≺Γ with <:∈-find (lookup⇒↦∈ T∈Γ) Γ′≺Γ
  ... | inj₁ (x≡n , T′ , T′∈Γ′ , T′<:T) = inj₁ (x≡n , T′ , ↦∈⇒lookup T′∈Γ′ , T′<:T)
  ... | inj₂ (x≢n , T∈Γ′)               = inj₂ (x≢n , ↦∈⇒lookup T∈Γ′)

  private
    trans-on : Typ → Set
    trans-on T = ∀ {Γ S U} → Γ ⊢′ S <: T → Γ ⊢′ T <: U → Γ ⊢′ S <: U

    narrow-on : Typ → Set
    narrow-on T = ∀ {Γ Γ′ n S U} →
                    Γ ⊢′ S <: U →
                    Γ′ ≺:[ n ] Γ →
                    env-lookup Γ n ≡ just T →
                    Γ′ ⊢′ S <: U

  ⟨A<:⟩-layer : Typ → ℕ → Typ
  ⟨A<:⟩-layer T zero    = T
  ⟨A<:⟩-layer T (suc n) = ⟨A<: ⟨A<:⟩-layer T n ⟩

  mutual
    <:′-trans-rec : ∀ T → (∀ T′ → Typ-measure T′ < Typ-measure T → trans-on T′ × narrow-on T′) → trans-on T
    <:′-trans-rec ⊤ rec S<:T <:⊤                           = <:⊤
    <:′-trans-rec ⊤ rec S<:T refl                          = S<:T
    <:′-trans-rec ⊥ rec ⊥<: T<:U                           = ⊥<:
    <:′-trans-rec ⊥ rec refl T<:U                          = T<:U
    <:′-trans-rec (n ∙A) rec ⊥<: T<:U                      = ⊥<:
    <:′-trans-rec (n ∙A) rec refl T<:U                     = T<:U
    <:′-trans-rec ⟨A<: T ⟩ rec ⊥<: T<:U                    = ⊥<:
    <:′-trans-rec ⟨A<: T ⟩ rec refl T<:U                   = T<:U
    <:′-trans-rec ⟨A<: T ⟩ rec (bnd S<:T) <:⊤              = <:⊤
    <:′-trans-rec ⟨A<: T ⟩ rec (bnd S<:T) refl             = bnd S<:T
    <:′-trans-rec ⟨A<: T ⟩ rec (bnd S<:T) (bnd T<:U)       = bnd (<:′-trans-rec T (λ T′ T′<T → rec T′ (≤-step T′<T)) S<:T T<:U)
    <:′-trans-rec (Π S′ ∙ U′) rec ⊥<: T<:U                 = ⊥<:
    <:′-trans-rec (Π S′ ∙ U′) rec refl T<:U                = T<:U
    <:′-trans-rec (Π S′ ∙ U′) rec (Π<: S′<:S″ U″<:U′) <:⊤  = <:⊤
    <:′-trans-rec (Π S′ ∙ U′) rec (Π<: S′<:S″ U″<:U′) refl = Π<: S′<:S″ U″<:U′

    <:′-trans-rec (Π S′ ∙ U′) rec (Π<: S′<:S″ U″<:U′) (Π<: S‴<:S′ U′<:U‴)
      = Π<: (<:′-trans-rec S′ (λ T′ T′<S′ → rec T′ (≤-step (≤-stepsʳ _ T′<S′))) S‴<:S′ S′<:S″)
            (<:′-trans-rec U′ (λ T′ T′<U′ → rec T′ (≤-step (≤-stepsˡ _ T′<U′)))
                           (proj₂ (rec (S′ ↑ 0) (s≤s $ ≤-stepsʳ _ $ ≤-reflexive (Typ-measure-↑ S′ 0)))
                                  U″<:U′
                                  ≺[ _ , S‴<:S′ ]
                                  refl)
                           U′<:U‴)
    <:′-trans-rec T rec (sel T′∈Γ T′<:B) T<:U              = sel T′∈Γ (⟨A<:⟩-traverse T rec T<:U T′<:B 0 refl)

    ⟨A<:⟩-traverse : ∀ T →
                       (∀ T′ → Typ-measure T′ < Typ-measure T → trans-on T′ × narrow-on T′) →
                     ∀ {Γ U} →
                       Γ ⊢′ T <: U →
                     ∀ {S T′} →
                       Γ ⊢′ S <: ⟨A<: T′ ⟩ →
                     ∀ n →
                       T′ ≡ ⟨A<:⟩-layer T n →
                       Γ ⊢′ S <: ⟨A<: ⟨A<:⟩-layer U n ⟩
    ⟨A<:⟩-traverse T rec T<:U ⊥<: n eqT′               = ⊥<:
    ⟨A<:⟩-traverse T rec T<:U refl zero refl           = bnd T<:U
    ⟨A<:⟩-traverse T rec T<:U refl (suc n) refl        = bnd (⟨A<:⟩-traverse T rec T<:U refl n refl)
    ⟨A<:⟩-traverse T rec T<:U (bnd S<:T) zero refl     = bnd (<:′-trans-rec T rec S<:T T<:U)
    ⟨A<:⟩-traverse T rec T<:U (bnd S<:Tn) (suc n) refl = bnd (⟨A<:⟩-traverse T rec T<:U S<:Tn n refl)
    ⟨A<:⟩-traverse T rec T<:U (sel T″∈Γ T″<:B) n refl  = sel T″∈Γ (⟨A<:⟩-traverse T rec T<:U T″<:B (suc n) refl)

  <:′-narrow-on : ∀ T → (∀ T′ → Typ-measure T′ ≡ Typ-measure T → trans-on T′) → narrow-on T
  <:′-narrow-on T trans <:⊤ Γ′≺Γ T∈Γ                  = <:⊤
  <:′-narrow-on T trans ⊥<: Γ′≺Γ T∈Γ                  = ⊥<:
  <:′-narrow-on T trans refl Γ′≺Γ T∈Γ                 = refl
  <:′-narrow-on T trans (bnd S<:U) Γ′≺Γ T∈Γ           = bnd (<:′-narrow-on T trans S<:U Γ′≺Γ T∈Γ)
  <:′-narrow-on T trans (sel T′∈Γ T′<:B) Γ′≺Γ T∈Γ
    with <:∈-find′ T′∈Γ Γ′≺Γ
  ...  | inj₁ (refl , T″ , T″∈Γ′ , T″<:T)
    rewrite just-injective (≡.trans (≡.sym T′∈Γ) T∈Γ) = sel T″∈Γ′ (trans T refl T″<:T (<:′-narrow-on T trans T′<:B Γ′≺Γ T∈Γ))
  ... | inj₂ (x≢n , T′∈Γ′)                            = sel T′∈Γ′ (<:′-narrow-on T trans T′<:B Γ′≺Γ T∈Γ)

  <:′-narrow-on T trans {Γ} {Γ′} {n} (Π<: {S′ = S′} S′<:S U<:U′) Γ′≺Γ T∈Γ
    = Π<: (<:′-narrow-on T trans S′<:S Γ′≺Γ T∈Γ)
          (<:′-narrow-on (T ↑ 0)
                         (λ T′ eq → trans T′ (≡.trans eq (Typ-measure-↑ T 0)))
                         U<:U′ (_ ∷ Γ′≺Γ)
                         (↦∈⇒lookup (tl {n} {T′ = S′} {Γ} (lookup⇒↦∈ T∈Γ))))

  <:′-trans-narrow : ∀ T → trans-on T × narrow-on T
  <:′-trans-narrow = wfRec _ aux
    where open Measure <-wellFounded Typ-measure
          aux : ∀ T → (∀ T′ → Typ-measure T′ < Typ-measure T → trans-on T′ × narrow-on T′) → trans-on T × narrow-on T
          aux T rec = <:′-trans-rec T rec
                    , <:′-narrow-on T (λ T′ T′≡T →
                                         <:′-trans-rec T′ λ T″ T″<T′ → rec T″ (≤-trans T″<T′ (≤-reflexive T′≡T)))

  <:′-trans : ∀ {T} → trans-on T
  <:′-trans {T} = proj₁ (<:′-trans-narrow T)

  <:′-narrow : ∀ {T} → narrow-on T
  <:′-narrow {T} = proj₂ (<:′-trans-narrow T)

open Transitivity using (<:′-trans ; <:′-narrow) public

<:⇒<:′ : ∀ {Γ S U} → Γ ⊢ S <: U → Γ ⊢′ S <: U
<:⇒<:′ <:⊤               = <:⊤
<:⇒<:′ ⊥<:               = ⊥<:
<:⇒<:′ refl              = refl
<:⇒<:′ (tran S<:T T<:U)  = <:′-trans (<:⇒<:′ S<:T) (<:⇒<:′ T<:U)
<:⇒<:′ (bnd S<:U)        = bnd (<:⇒<:′ S<:U)
<:⇒<:′ (sel T∈Γ T<:B)    = sel T∈Γ (<:⇒<:′ T<:B)
<:⇒<:′ (Π<: S′<:S U<:U′) = Π<: (<:⇒<:′ S′<:S) (<:⇒<:′ U<:U′)

<:′⇒<: : ∀ {Γ S U} → Γ ⊢′ S <: U → Γ ⊢ S <: U
<:′⇒<: <:⊤               = <:⊤
<:′⇒<: ⊥<:               = ⊥<:
<:′⇒<: refl              = refl
<:′⇒<: (bnd S<:U)        = bnd (<:′⇒<: S<:U)
<:′⇒<: (sel T∈Γ T<:B)    = sel T∈Γ (<:′⇒<: T<:B)
<:′⇒<: (Π<: S′<:S U<:U′) = Π<: (<:′⇒<: S′<:S) (<:′⇒<: U<:U′)

open import FsubMinus
open import FsubMinus2
open FsubMinus.FsubMinus renaming (Env to Env′ ; _↑_ to _⇑_) hiding (env-lookup)
open import DsubsubUndec
open Undecidability hiding (D<<:′⇒F<: ; D<<:⇒F<: ; F<:⇒D<<:)

D<<:′⇒F<: : ∀ {Γ′ S U} → Γ′ ⊢′ S <: U →
              ∀ {Γ} →
                Γ′ ≡ ⟪ Γ ⟫ → (cS : Covar S) → (cU : Covar U) →
                Γ ⊢F ⟦ -, cS ⟧⁻¹ <: ⟦ -, cU ⟧⁻¹
D<<:′⇒F<: <:⊤ {Γ} eqΓ cS cv⊤                   = ftop
D<<:′⇒F<: ⊥<: {Γ} eqΓ () cU
D<<:′⇒F<: refl {Γ} eqΓ cS cU
  rewrite covar-irrelevant cS cU               = <:-refl _ _
D<<:′⇒F<: (bnd S<:U) {Γ} eqΓ () cU
D<<:′⇒F<: (sel T∈Γ′ ⊥<:) {Γ} refl (cv∙A n) cU
  with lookupContraEnv T∈Γ′ (⟪⟫-contra Γ)
... | ()
D<<:′⇒F<: (sel T∈Γ′ refl) {Γ} refl (cv∙A n) cU = fbinds (env-lookup⇒<:∈ cU T∈Γ′) (<:-refl _ _)
D<<:′⇒F<: (sel T∈Γ′ (bnd U′<:U)) {Γ} refl (cv∙A n) cU
  with lookupContraEnv T∈Γ′ (⟪⟫-contra Γ)
... | ctt cU′                                  = fbinds (env-lookup⇒<:∈ cU′ T∈Γ′) (D<<:′⇒F<: U′<:U refl cU′ cU)
D<<:′⇒F<: (sel T∈Γ′ (sel _ _)) {Γ} refl (cv∙A n) cU
  with lookupContraEnv T∈Γ′ (⟪⟫-contra Γ)
... | ()
D<<:′⇒F<: (Π<: refl U<:U′) {Γ} eqΓ (cvΠ cS cU) (cvΠ cS′ cU′)
                                               = fall (D<<:′⇒F<: refl eqΓ cS′ cS) (D<<:′⇒F<: U<:U′ (eq-cons-Γ _ eqΓ) cU cU′)
  where eq-cons-Γ : ∀ {Γ′ T} (cT : Covar T) → Γ′ ≡ ⟪ Γ ⟫ →
                      Γ′ ‣ ⟨A<: T ⟩ ! ≡ ⟨A<: ⟦ ⟦ T , cT ⟧⁻¹ ⟧ ⟩ ∷ ⟪ Γ ⟫
        eq-cons-Γ cT eqΓ′ rewrite ⟦⟦⟧⁻¹⟧⇒id cT = cong (⟨A<: _ ⟩ ∷_) eqΓ′
D<<:′⇒F<: (Π<: (bnd S′<:S) U<:U′) {Γ} eqΓ (cvΠ cS cU) (cvΠ cS′ cU′)
                                               = fall (D<<:′⇒F<: S′<:S eqΓ cS′ cS) (D<<:′⇒F<: U<:U′ (eq-cons-Γ _ eqΓ) cU cU′)
  where eq-cons-Γ : ∀ {Γ′ T} (cT : Covar T) → Γ′ ≡ ⟪ Γ ⟫ →
                      Γ′ ‣ ⟨A<: T ⟩ ! ≡ ⟨A<: ⟦ ⟦ T , cT ⟧⁻¹ ⟧ ⟩ ∷ ⟪ Γ ⟫
        eq-cons-Γ cT eqΓ′ rewrite ⟦⟦⟧⁻¹⟧⇒id cT = cong (⟨A<: _ ⟩ ∷_) eqΓ′

D<<:⇒F<: : ∀ {Γ S U} → ⟪ Γ ⟫ ⊢ ⟦ S ⟧ <: ⟦ U ⟧ → Γ ⊢F S <: U
D<<:⇒F<: {_} {S} {U} S<:U
  with D<<:′⇒F<: (<:⇒<:′ S<:U) refl (⟦⟧-covar _) (⟦⟧-covar _)
... | S<:U′
  rewrite ⟦⟧⁻¹-left-inverse-⟦⟧ (⟦⟧-covar S)
        | ⟦⟧⁻¹-left-inverse-⟦⟧ (⟦⟧-covar U) = S<:U′

F<:⇒D<<: : ∀ {Γ S U} → Γ ⊢F S <: U → ⟪ Γ ⟫ ⊢ ⟦ S ⟧ <: ⟦ U ⟧
F<:⇒D<<: ftop               = <:⊤
F<:⇒D<<: fvrefl             = refl
F<:⇒D<<: (fbinds T∈Γ T<:U)  = sel (<:∈⇒env-lookup T∈Γ) (bnd (F<:⇒D<<: T<:U))
F<:⇒D<<: (fall S′<:S U<:U′) = Π<: (bnd (F<:⇒D<<: S′<:S)) (F<:⇒D<<: U<:U′)
