{-# OPTIONS --without-K --safe #-}

module DsubTermUndec where

open import Data.List as List
open import Data.List.All as All
open import Data.Nat as ℕ
open import Data.Maybe as Maybe
open import Data.Product
open import Data.Sum
open import Data.Empty renaming (⊥ to False)
open import Data.Vec as Vec renaming (_∷_ to _‼_ ; [] to nil) hiding (_++_)
open import Function

open import Data.Maybe.Properties as Maybeₚ
open import Data.Nat.Properties as ℕₚ
open import Relation.Nullary
open import Relation.Binary.PropositionalEquality as ≡

open import DsubDef
open import DsubFull
open import DsubReduced
open import Dsub renaming (_⊢_<:_ to _⊢′_<:_)
open import Utils
open import DsubEquiv
  renaming ( _⊢′_<:_ to _⊢″_<:_ ; <:⇒<:′ to <:′⇒<:″ ; <:′⇒<: to <:″⇒<:′ ; Π<: to Π<:″
           ; <:′-weakening-hd to <:″-weakening-hd)

-- to show D<: term typing is also undecidable, there needs to be a strengthening
-- property.

module ByApplication where
  
  data EContra : Typ → Set where
    ctt : ∀ {T} → Covar T → EContra ⟨A: ⊥ ⋯ T ⟩
    cΠ  : ∀ S U → EContra (Π S ∙ U)
    c⊤ : EContra ⊤
    c∙A : ∀ n → EContra (n ∙A)
  
  EContraEnv : Env → Set
  EContraEnv = All EContra
  
  covar⇒econtra : ∀ {T} → Covar T → EContra T
  covar⇒econtra cv⊤               = c⊤
  covar⇒econtra (cv∙A n)          = c∙A n
  covar⇒econtra (cvΠ {S} {U} _ _) = cΠ ⟨A: ⊥ ⋯ S ⟩ U
  
  econtra⇒contra : ∀ {Γ} → EContraEnv Γ → Σ Env ContraEnv
  econtra⇒contra []            = [] , []
  econtra⇒contra (ctt T ∷ eΓ) with econtra⇒contra eΓ
  ... | Γ′ , cΓ′               = -, ctt T ∷ cΓ′
  econtra⇒contra (cΠ S U ∷ eΓ) = econtra⇒contra eΓ
  econtra⇒contra (c⊤ ∷ eΓ)     = econtra⇒contra eΓ
  econtra⇒contra (c∙A n ∷ eΓ)  = econtra⇒contra eΓ
  
  contra⇒econtra : ∀ {Γ} → ContraEnv Γ → EContraEnv Γ
  contra⇒econtra []           = []
  contra⇒econtra (ctt T ∷ cΓ) = ctt T ∷ contra⇒econtra cΓ
  
  ↑-EContra : ∀ {T} n → EContra T → EContra (T ↑ n)
  ↑-EContra n (ctt T)  = ctt (↑-Covar n T)
  ↑-EContra n (cΠ S U) = cΠ (S ↑ n) (U ↑ suc n)
  ↑-EContra n c⊤       = c⊤
  ↑-EContra n (c∙A x)
    rewrite ↑-var x n  = c∙A _
  
  ↦∈EContraEnv : ∀ {Γ n T} → n ↦ T ∈ Γ → EContraEnv Γ → EContra T
  ↦∈EContraEnv hd (T ∷ eT)       = ↑-EContra 0 T
  ↦∈EContraEnv (tl T∈Γ) (_ ∷ eT) = ↑-EContra 0 (↦∈EContraEnv T∈Γ eT)
  
  lookupEContraEnv : ∀ {Γ n T} → env-lookup Γ n ≡ just T → EContraEnv Γ → EContra T
  lookupEContraEnv lk eT = ↦∈EContraEnv (lookup⇒↦∈ lk) eT
  
  lookupEContraBot : ∀ {Γ n} → EContraEnv Γ → ¬ env-lookup Γ n ≡ just ⊥
  lookupEContraBot all eq with lookupEContraEnv eq all
  ... | ()
  
  lookupEContraBndBot : ∀ {Γ n S} → EContraEnv Γ → ¬ env-lookup Γ n ≡ just ⟨A: S ⋯ ⊥ ⟩
  lookupEContraBndBot all eq with lookupEContraEnv eq all
  ... | ctt ()
  
  lookupEContraBndBnd : ∀ {Γ n T S U} → EContraEnv Γ →
                          ¬ env-lookup Γ n ≡ just ⟨A: T ⋯ ⟨A: S ⋯ U ⟩ ⟩
  lookupEContraBndBnd all eq with lookupEContraEnv eq all
  ... | ctt ()
  
  lookupEContra⊥Lb : ∀ {Γ n S U} → EContraEnv Γ →
                      env-lookup Γ n ≡ just ⟨A: S ⋯ U ⟩ → S ≡ ⊥
  lookupEContra⊥Lb all eq with lookupEContraEnv eq all
  ... | ctt _ = refl
  
  invertibleEcontra : InvertibleEnv EContraEnv
  invertibleEcontra = record
    { no-⊥       = lookupEContraBot
    ; no-bnd-⊥   = lookupEContraBndBot
    ; no-bnd-bnd = lookupEContraBndBnd
    ; ⊥-lb       = lookupEContra⊥Lb
    }
  
  open DsubInvProperties invertibleEcontra renaming (<:⇒<:ᵢ to <:′⇒<:ᵢ ; <:ᵢ⇒<: to <:ᵢ⇒<:′)
  
  -- following lemma is used to show that, in extended contravariant environment, the
  -- behavior is still quite regulated (as much as in contravariant environment).
  
  <:⊥-econtra : ∀ {Γ T} → Γ ⊢″ T <: ⊥ → EContraEnv Γ → T ≡ ⊥
  <:⊥-econtra T<:⊥ eΓ = <:⊥′ (<:″⇒<:′ T<:⊥) eΓ
  
  ⊤<:-econtra : ∀ {Γ T} → Γ ⊢″ ⊤ <: T → EContraEnv Γ → T ≡ ⊤
  ⊤<:-econtra ⊤<:T eΓ = ⊤<:′ (<:″⇒<:′ ⊤<:T) eΓ
  
  Π<:⟨A:⟩⇒⊥-econtra : ∀ {Γ S U S′ U′} → Γ ⊢″ Π S ∙ U <: ⟨A: S′ ⋯ U′ ⟩ → EContraEnv Γ → False
  Π<:⟨A:⟩⇒⊥-econtra Π<:⟨A:⟩ eΓ = Π<:⟨A:⟩⇒⊥ (<:′⇒<:ᵢ (<:″⇒<:′ Π<:⟨A:⟩) eΓ) eΓ
  
  ∙A<:⟨A:⟩⇒⊥-contra : ∀ {Γ x S U} → Γ ⊢″ x ∙A <: ⟨A: S ⋯ U ⟩ → EContraEnv Γ → False
  ∙A<:⟨A:⟩⇒⊥-contra ∙A<:⟨A:⟩ eΓ = ∙A<:⟨A:⟩⇒⊥ ((<:′⇒<:ᵢ (<:″⇒<:′ ∙A<:⟨A:⟩) eΓ)) eΓ
  
  ⟨A:⟩<:⟨A:⟩-econtra : ∀ {Γ S′ U U′} → Γ ⊢″ ⟨A: ⊥ ⋯ U ⟩ <: ⟨A: S′ ⋯ U′ ⟩ → EContraEnv Γ →
                        S′ ≡ ⊥
  ⟨A:⟩<:⟨A:⟩-econtra B<:B′ eΓ with ⟨A:⟩<:⟨A:⟩ (<:′⇒<:ᵢ (<:″⇒<:′ B<:B′) eΓ) eΓ
  ... | S<:⊥ , _ = <:⊥ S<:⊥ eΓ
  
  insert-econtra : ∀ {T} Γ₁ Γ₂ →
                     EContra T →
                     ContraEnv (Γ₁ ‣ Γ₂) →
                     EContraEnv (Γ₁ ‣ T ! ‣ Γ₂ ⇑)
  insert-econtra Γ₁ [] eT cΓ                 = eT ∷ contra⇒econtra cΓ
  insert-econtra Γ₁ (_ ∷ Γ₂) eT (ctt T ∷ cΓ) = (ctt (↑-Covar (length Γ₂) T)) ∷ insert-econtra Γ₁ Γ₂ eT cΓ
  
  insert-econtra′ : ∀ {T} Γ₁ Γ₂ →
                      EContra T →
                      EContraEnv (Γ₁ ‣ Γ₂) →
                      EContraEnv (Γ₁ ‣ T ! ‣ Γ₂ ⇑)
  insert-econtra′ Γ₁ [] eT eΓ             = eT ∷ eΓ
  insert-econtra′ Γ₁ (_ ∷ Γ₂) eT (T ∷ eΓ) = (↑-EContra (length Γ₂) T) ∷ insert-econtra′ Γ₁ Γ₂ eT eΓ
  
  ↦∈-strengthen-≤ : ∀ {n T Γ} →
                      suc n ↦ T ∈ Γ →
                    ∀ Γ₁ Γ₂ U →
                      Γ ≡ Γ₁ ‣ U ! ‣ Γ₂ ⇑ →
                      length Γ₂ ≤ n →
                      ∃ λ T′ → n ↦ T′ ∈ Γ₁ ‣ Γ₂ × T ≡ T′ ↑ length Γ₂
  ↦∈-strengthen-≤ (tl T∈Γ) Γ₁ [] U refl z≤n = -, T∈Γ , refl
  ↦∈-strengthen-≤ (tl T∈Γ) Γ₁ (_ ∷ Γ₂) U refl (s≤s l≤n) with ↦∈-strengthen-≤ T∈Γ Γ₁ Γ₂ U refl l≤n
  ... | T′ , T′∈Γ′ , refl = T′ ↑ 0 , tl T′∈Γ′ , sym (↑-↑-comm T′ 0 (length Γ₂) z≤n)
  
  ↦∈-strengthen-> : ∀ {n T Γ} →
                      n ↦ T ∈ Γ →
                    ∀ Γ₁ Γ₂ U →
                      Γ ≡ Γ₁ ‣ U ! ‣ Γ₂ ⇑ →
                      length Γ₂ > n →
                      ∃ λ T′ → n ↦ T′ ∈ Γ₁ ‣ Γ₂ × T ≡ T′ ↑ length Γ₂
  ↦∈-strengthen-> hd Γ₁ [] U refl ()
  ↦∈-strengthen-> hd Γ₁ (T ∷ Γ₂) U refl (s≤s l>n) = T ↑ 0 , hd , (sym (↑-↑-comm T 0 (length Γ₂) l>n))
  ↦∈-strengthen-> (tl T∈Γ) Γ₁ [] U refl ()
  ↦∈-strengthen-> (tl T∈Γ) Γ₁ (_ ∷ Γ₂) U refl (s≤s l>n) with ↦∈-strengthen-> T∈Γ Γ₁ Γ₂ U refl l>n
  ... | T′ , T′∈Γ′ , refl                         = T′ ↑ 0 , tl T′∈Γ′ , (sym (↑-↑-comm T′ 0 (length Γ₂) z≤n))
  
  ↦∈-strengthen : ∀ {n′ T Γ} →
                    n′ ↦ T ∈ Γ →
                  ∀ Γ₁ Γ₂ U n →
                    Γ ≡ Γ₁ ‣ U ! ‣ Γ₂ ⇑ →
                    n′ ≡ ↑-idx n (length Γ₂) →
                    ∃ λ T′ → n ↦ T′ ∈ Γ₁ ‣ Γ₂ × T ≡ T′ ↑ length Γ₂
  ↦∈-strengthen T∈Γ Γ₁ Γ₂ U n eqΓ eqn with length Γ₂ ≤? n
  ... | yes l≤n rewrite eqn = ↦∈-strengthen-≤ T∈Γ Γ₁ _ _ eqΓ l≤n
  ... | no l>n rewrite eqn  = ↦∈-strengthen-> T∈Γ Γ₁ _ _ eqΓ (≰⇒> l>n)
  
  ↦∈-strengthen′ : ∀ {T} Γ₁ Γ₂ U n →
                     env-lookup (Γ₁ ‣ U ! ‣ Γ₂ ⇑) (↑-idx n (length Γ₂)) ≡ just T →
                     ∃ λ T′ → env-lookup (Γ₁ ‣ Γ₂) n ≡ just T′ × T ≡ T′ ↑ length Γ₂
  ↦∈-strengthen′ Γ₁ Γ₂ U n T∈Γ′ with ↦∈-strengthen (lookup⇒↦∈ T∈Γ′) Γ₁ Γ₂ U n refl refl
  ... | T′ , T′∈Γ , eq = T′ , ↦∈⇒lookup T′∈Γ , eq
  
  <:″-ec-strengthen-gen : ∀ {Γ′ S′ U′} Γ₁ Γ₂ →
                            Γ′ ⊢″ S′ <: U′ →
                          ∀ S U T →
                            EContra T →
                            S′ ≡ S ↑ length Γ₂ →
                            U′ ≡ U ↑ length Γ₂ →
                            Γ′ ≡ Γ₁ ‣ T ! ‣ Γ₂ ⇑ →
                            EContraEnv (Γ₁ ‣ Γ₂) →
                            Covar S → Covar U → 
                            Γ₁ ‣ Γ₂ ⊢″ S <: U
  <:″-ec-strengthen-gen Γ₁ Γ₂ (⊤<:! ⊤<:U′) S U T eT eqS eqU eqΓ eΓ cS cU = ⊤<:! (<:″-ec-strengthen-gen Γ₁ Γ₂ ⊤<:U′ ⊤ U _ eT refl eqU eqΓ eΓ cv⊤ cU)
  <:″-ec-strengthen-gen Γ₁ Γ₂ (<:⊥! S′<:⊥) S U T eT eqS eqU refl eΓ cS cU
    rewrite <:⊥-econtra S′<:⊥ (insert-econtra′ _ _ eT eΓ)
          | ↑-⊥-inv {S} (sym eqS)                                        = case cS of λ ()
  <:″-ec-strengthen-gen Γ₁ Γ₂ refl S U T eT eqS eqU eqΓ eΓ cS cU
    rewrite ↑-injective {S} {U} (≡.trans (≡.sym eqS) eqU)                = refl
  <:″-ec-strengthen-gen Γ₁ Γ₂ (bnd S′<:S U<:U′) ⊤ U T eT () eqU eqΓ eΓ cS cU
  <:″-ec-strengthen-gen Γ₁ Γ₂ (bnd S′<:S U<:U′) ⊥ U T eT () eqU eqΓ eΓ cS cU
  <:″-ec-strengthen-gen Γ₁ Γ₂ (bnd S′<:S U<:U′) (n ∙A) U T eT eqS eqU eqΓ eΓ cS cU
    rewrite ↑-var n (length Γ₂)                                          = case eqS of λ ()
  <:″-ec-strengthen-gen Γ₁ Γ₂ (bnd S′<:S U<:U′) (Π _ ∙ _) U T eT () eqU eqΓ eΓ cS cU
  <:″-ec-strengthen-gen Γ₁ Γ₂ (bnd S′<:S U<:U′) ⟨A: _ ⋯ _ ⟩ U T eT refl eqU eqΓ eΓ () cU
  <:″-ec-strengthen-gen Γ₁ Γ₂ (Π<:″ S′<:S U<:U′) ⊤ U T eT () eqU eqΓ eΓ  cS cU
  <:″-ec-strengthen-gen Γ₁ Γ₂ (Π<:″ S′<:S U<:U′) ⊥ U T eT () eqU eqΓ eΓ  cS cU
  <:″-ec-strengthen-gen Γ₁ Γ₂ (Π<:″ S′<:S U<:U′) (n ∙A) U T eT eqS eqU eqΓ eΓ cS cU
    rewrite ↑-var n (length Γ₂)                                          = case eqS of λ ()
  <:″-ec-strengthen-gen Γ₁ Γ₂ (Π<:″ S′<:S U<:U′) (Π _ ∙ _) ⊤ T eT eqS () eqΓ eΓ cS cU
  <:″-ec-strengthen-gen Γ₁ Γ₂ (Π<:″ S′<:S U<:U′) (Π _ ∙ _) ⊥ T eT eqS () eqΓ eΓ cS cU
  <:″-ec-strengthen-gen Γ₁ Γ₂ (Π<:″ S′<:S U<:U′) (Π _ ∙ _) (n ∙A) T eT eqS eqU eqΓ eΓ  cS cU
    rewrite ↑-var n (length Γ₂)                                          = case eqU of λ ()
  <:″-ec-strengthen-gen Γ₁ Γ₂ (Π<:″ S′<:S U<:U′) (Π _ ∙ _) (Π _ ∙ _) T eT refl refl refl eΓ (cvΠ {S₁} cS cU) (cvΠ {S₂} cS′ cU′)
    with S₁ ↑ length Γ₂ | inspect (S₁ ↑_) (length Γ₂)
       | S₂ ↑ length Γ₂ | inspect (S₂ ↑_) (length Γ₂)
  ...  | S₁′ | [ eq₁ ] | S₂′ | [ eq₂ ]
       with S′<:S
  ...     | ⊤<:! ⊤<:S                                                    = case ⊤<:-econtra ⊤<:S (insert-econtra′ _ _ eT eΓ) of λ ()
  ...     | <:⊥! S′<:⊥                                                   = case <:⊥-econtra S′<:⊥ (insert-econtra′ _ _ eT eΓ) of λ ()
  ...     | refl rewrite ↑-injective {S₁} {S₂} (≡.trans eq₁ (≡.sym eq₂)) = Π<:″ refl
                                                                                (<:″-ec-strengthen-gen Γ₁ _ U<:U′ _ _ T eT
                                                                                                       refl refl (cong₂ _∷_ (cong ⟨A: ⊥ ⋯_⟩ (sym eq₂)) refl)
                                                                                                       (ctt cS′ ∷ eΓ) cU cU′)
  ...     | bnd _ S₂′<:S₁′                                               = Π<:″ (bnd refl
                                                                                      (<:″-ec-strengthen-gen Γ₁ Γ₂ S₂′<:S₁′ _ _ T eT
                                                                                                             (sym eq₂) (sym eq₁) refl
                                                                                                             eΓ cS′ cS))
                                                                                      (<:″-ec-strengthen-gen Γ₁ _ U<:U′ _ _ T eT
                                                                                                             refl refl (cong₂ _∷_ (cong ⟨A: ⊥ ⋯_⟩ (sym eq₂)) refl)
                                                                                                             (ctt cS′ ∷ eΓ) cU cU′)
  ...     | <:> T∈Γ′ T<:B _
          with insert-econtra′ Γ₁ _ eT eΓ | lookupEContraEnv T∈Γ′ (insert-econtra′ Γ₁ _ eT eΓ)
  ...        | eΓ′                        | c⊤                           = case ⊤<:-econtra T<:B eΓ′ of λ ()
  ...        | eΓ′                        | c∙A x                        = ⊥-elim (∙A<:⟨A:⟩⇒⊥-contra T<:B eΓ′)
  ...        | eΓ′                        | ctt _                        = case ⟨A:⟩<:⟨A:⟩-econtra T<:B eΓ′ of λ ()
  ...        | eΓ′                        | cΠ _ _                       = ⊥-elim (Π<:⟨A:⟩⇒⊥-econtra T<:B eΓ′)
  <:″-ec-strengthen-gen Γ₁ Γ₂ (Π<:″ S′<:S U<:U′) (Π _ ∙ _) ⟨A: _ ⋯ _ ⟩ T eT eqS () eqΓ eΓ cS cU
  <:″-ec-strengthen-gen Γ₁ Γ₂ (Π<:″ S′<:S U<:U′) ⟨A: _ ⋯ _ ⟩ U T eT () eqU eqΓ eΓ cS cU
  <:″-ec-strengthen-gen Γ₁ Γ₂ (sel₁ T∈Γ T<:B) S ⊤ T eT eqS () eqΓ eΓ cS cU
  <:″-ec-strengthen-gen Γ₁ Γ₂ (sel₁ T∈Γ T<:B) S ⊥ T eT eqS () eqΓ eΓ cS cU
  <:″-ec-strengthen-gen Γ₁ Γ₂ (sel₁ T∈Γ T<:B) S (x ∙A) T eT eqS eqU refl eΓ cS cU
    rewrite ↑-var x (length Γ₂)
    with eqS  | insert-econtra′ Γ₁ _ eT eΓ | lookupEContraEnv T∈Γ (insert-econtra′ Γ₁ _ eT eΓ)
  ...  | refl | eΓ′                        | c⊤                          = case ⊤<:-econtra T<:B eΓ′ of λ ()
  ...  | refl | eΓ′                        | c∙A y                       = ⊥-elim (∙A<:⟨A:⟩⇒⊥-contra T<:B eΓ′)
  ...  | refl | eΓ′                        | ctt _
    rewrite ↑-⊥-inv {S} (⟨A:⟩<:⟨A:⟩-econtra T<:B eΓ′)                    = case cS of λ ()
  ...  | refl | eΓ′                        | cΠ _ _                      = ⊥-elim (Π<:⟨A:⟩⇒⊥-econtra T<:B eΓ′)
  <:″-ec-strengthen-gen Γ₁ Γ₂ (sel₁ T∈Γ T<:B) S (Π _ ∙ _) T eT eqS () eqΓ eΓ cS cU
  <:″-ec-strengthen-gen Γ₁ Γ₂ (sel₁ T∈Γ T<:B) S ⟨A: _ ⋯ _ ⟩ T eT eqS () eqΓ eΓ cS cU
  <:″-ec-strengthen-gen Γ₁ Γ₂ (<:> T∈Γ T<:B T<:B′) S U T eT eqS eqU refl eΓ cS cU
    with eqS  | insert-econtra′ Γ₁ _ eT eΓ | lookupEContraEnv T∈Γ (insert-econtra′ Γ₁ _ eT eΓ)
  ...  | refl | eΓ′                        | c⊤                          = case ⊤<:-econtra T<:B eΓ′ of λ ()
  ...  | refl | eΓ′                        | c∙A y                       = ⊥-elim (∙A<:⟨A:⟩⇒⊥-contra T<:B eΓ′)
  ...  | refl | eΓ′                        | ctt _
    rewrite ↑-⊥-inv {S} (⟨A:⟩<:⟨A:⟩-econtra T<:B eΓ′)                    = case cS of λ ()
  ...  | refl | eΓ′                        | cΠ _ _                      = ⊥-elim (Π<:⟨A:⟩⇒⊥-econtra T<:B eΓ′)
  <:″-ec-strengthen-gen Γ₁ Γ₂ (sel₂ T∈Γ T<:B) ⊤ U T eT () eqU eqΓ eΓ cS cU
  <:″-ec-strengthen-gen Γ₁ Γ₂ (sel₂ T∈Γ T<:B) ⊥ U T eT () eqU eqΓ eΓ cS cU
  <:″-ec-strengthen-gen Γ₁ Γ₂ (sel₂ T∈Γ T<:B) (Π _ ∙ _) U T eT () eqU eqΓ eΓ cS cU
  <:″-ec-strengthen-gen Γ₁ Γ₂ (sel₂ T∈Γ T<:B) ⟨A: _ ⋯ _ ⟩ U T eT () eqU eqΓ eΓ cS cU
  <:″-ec-strengthen-gen Γ₁ Γ₂ (sel₂ T∈Γ T<:B) (x ∙A) U T eT eqS eqU refl eΓ cS cU
    rewrite ↑-var x (length Γ₂)
    with eqS  | insert-econtra′ Γ₁ _ eT eΓ | lookupEContraEnv T∈Γ (insert-econtra′ Γ₁ _ eT eΓ)
  ...  | refl | eΓ′                        | c⊤                          = case ⊤<:-econtra T<:B eΓ′ of λ ()
  ...  | refl | eΓ′                        | c∙A y                       = ⊥-elim (∙A<:⟨A:⟩⇒⊥-contra T<:B eΓ′)
  ...  | refl | eΓ′                        | cΠ _ _                      = ⊥-elim (Π<:⟨A:⟩⇒⊥-econtra T<:B eΓ′)
  ...  | refl | eΓ′                        | ctt cT rewrite ⟨A:⟩<:⟨A:⟩-econtra T<:B eΓ′
       with ↦∈-strengthen′ Γ₁ Γ₂ T x T∈Γ
  ...     | T′ , T′∈Γ , eq
          with lookupEContraEnv T′∈Γ eΓ | eq
  ...        | c⊤                       | ()
  ...        | c∙A y                    | _ rewrite ↑-var y (length Γ₂)  = case eq of λ ()
  ...        | cΠ _ _                   | ()
  ...        | ctt cU′                  | refl                           = sel₂ T′∈Γ (bnd refl (aux Γ₁ Γ₂ eT T<:B refl eqU eΓ cU′ cU))
    where aux : ∀ {T U T′ T″ S U′} Γ₁ Γ₂ →
                  EContra T″ →
                  Γ₁ ‣ T″ ! ‣ Γ₂ ⇑ ⊢″ ⟨A: ⊥ ⋯ T′ ⟩ <: ⟨A: S ⋯ U′ ⟩ →
                  T′ ≡ T ↑ length Γ₂ → U′ ≡ U ↑ length Γ₂ →
                  EContraEnv (Γ₁ ‣ Γ₂) → Covar T → Covar U →
                  Γ₁ ‣ Γ₂ ⊢″ T <: U
          aux Γ₁ Γ₂ eT″ (⊤<:! <:′) eqT eqU eΓ cT cU                      = case ⊤<:-econtra <:′ (insert-econtra′ _ _ eT″ eΓ) of λ ()
          aux Γ₁ Γ₂ eT″ (<:⊥! <:′) eqT eqU eΓ cT cU                      = case <:⊥-econtra <:′ (insert-econtra′ _ _ eT″ eΓ) of λ ()
          aux {T} {U} Γ₁ Γ₂ eT″ refl eqT eqU eΓ cT cU
            rewrite ↑-injective {T} {U} (≡.trans (≡.sym eqT) eqU)        = refl
          aux Γ₁ Γ₂ eT″ (bnd _ <:′) eqT eqU eΓ cT cU                     = <:″-ec-strengthen-gen Γ₁ Γ₂ <:′ _ _ _ eT″ eqT eqU refl eΓ cT cU
          aux Γ₁ Γ₂ eT″ (<:> T′∈Γ′ <:′ _) eqT eqU eΓ cT cU
            with insert-econtra′ Γ₁ _ eT″ eΓ | lookupEContraEnv T′∈Γ′ (insert-econtra′ Γ₁ _ eT″ eΓ)
          ...  | eΓ′                         | ctt _                     = case ⟨A:⟩<:⟨A:⟩-econtra <:′ eΓ′ of λ ()
          ...  | eΓ′                         | cΠ _ _                    = ⊥-elim (Π<:⟨A:⟩⇒⊥-econtra <:′ eΓ′)
          ...  | eΓ′                         | c⊤                        = case ⊤<:-econtra <:′ eΓ′ of λ ()
          ...  | eΓ′                         | c∙A x                     = ⊥-elim (∙A<:⟨A:⟩⇒⊥-contra <:′ eΓ′) 
  
  <:″-ec-strengthen : ∀ {S U T} Γ₁ Γ₂ →
                        Γ₁ ‣ T ! ‣ Γ₂ ⇑ ⊢″ S ↑ length Γ₂ <: U ↑ length Γ₂ →
                        EContraEnv (Γ₁ ‣ Γ₂) →
                        EContra T →  Covar S → Covar U → 
                        Γ₁ ‣ Γ₂ ⊢″ S <: U
  <:″-ec-strengthen Γ₁ Γ₂ S<:U eΓ T S U = <:″-ec-strengthen-gen Γ₁ Γ₂ S<:U _ _ _ T refl refl refl eΓ S U
  
  <:″-ec-strengthen-hd : ∀ {Γ S U T} →
                           Γ ‣ T ! ⊢″ S ↑ 0 <: U ↑ 0 →
                           EContraEnv Γ →
                           EContra T → Covar S → Covar U → 
                           Γ ⊢″ S <: U
  <:″-ec-strengthen-hd S<:U eΓ T S U = <:″-ec-strengthen _ [] S<:U eΓ T S U
  
  typing⇒<:-gen : ∀ {Γ T₁ T₂ U′} U →
                    Γ ‣ (Π T₁ ∙ U) ! ‣ T₂ ↑ 0 ! ⊢ 1 $$ 0 ∷ U′ →
                    ContraEnv Γ → Covar T₁ → Covar T₂ →
                    Γ ⊢ T₂ <: T₁
  typing⇒<:-gen {_} {T₁} U (Π-E x∷Π y∷T) cΓ cT₁ cT₂
    with var∷⇒<: y∷T
       | var∷⇒<: x∷Π
       | EContraEnv _ ∋ covar⇒econtra (↑-Covar 0 cT₂) ∷ cΠ T₁ U ∷ contra⇒econtra cΓ
  ...  | _ , refl , T₂<:S | Π′ , refl , Π′<:Π | eΓ′
       with <:ᵢ⇒<:′ $ Π<:Π (<:′⇒<:ᵢ Π′<:Π eΓ′) eΓ′ | ↑-Covar 0 cT₂
  ...     | S<:T₁ | cT₂′
          with <:″-ec-strengthen-hd (<:′⇒<:″ $ dtrans _ T₂<:S S<:T₁)
                                    (cΠ T₁ U ∷ contra⇒econtra cΓ)
                                    (covar⇒econtra cT₂′)
                                    cT₂′ (↑-Covar 0 cT₁)
  ...        | T₂<:T₁                      = <:′⇒<: $ <:″⇒<:′ $ <:″-ec-strengthen-hd T₂<:T₁ (contra⇒econtra cΓ) (cΠ T₁ U) cT₂ cT₁
  typing⇒<:-gen U (<:∷ xy∷U′ _) cΓ cT₁ cT₂ = typing⇒<:-gen U xy∷U′ cΓ cT₁ cT₂
  
  <:⇒typing-gen : ∀ {Γ T₁ T₂} U →
                    Γ ⊢ T₂ <: T₁ →
                    ∃ λ U′ → Γ ‣ (Π T₁ ∙ U) ! ‣ T₂ ↑ 0 ! ⊢ 1 $$ 0 ∷ U′
  <:⇒typing-gen U T₂<:T₁ with <:′⇒<:″ $ <:⇒<:′ T₂<:T₁
  ... | T₂<:T₁′ = -, Π-E (var∷ refl) (<:∷ (var∷ refl) (<:′⇒<: $ <:″⇒<:′ $ <:″-weakening-hd _ $ <:″-weakening-hd _ T₂<:T₁′))
  
  open import FsubMinus
  open FsubMinus.FsubMinus hiding (_↑_)
  open FsubMinusToDsubR
  open Undecidability
  
  typing⇒F<: : ∀ {Γ T₁ T₂ U′} U →
                 ⟪ Γ ⟫ ‣ (Π ⟦ T₁ ⟧ ∙ U) ! ‣ ⟦ T₂ ⟧ ↑ 0 ! ⊢ 1 $$ 0 ∷ U′ →
                 Γ ⊢F T₂ <: T₁
  typing⇒F<: U xy∷U′ = D<:⇒F<: $ <:⇒<:′ $ typing⇒<:-gen U xy∷U′ (⟪⟫-contraEnv _) (⟦⟧-covar _) (⟦⟧-covar _)
  
  F<:⇒typing : ∀ {Γ T₁ T₂} U →
                 Γ ⊢F T₂ <: T₁ →
                 ∃ λ U′ → ⟪ Γ ⟫ ‣ (Π ⟦ T₁ ⟧ ∙ U) ! ‣ ⟦ T₂ ⟧ ↑ 0 ! ⊢ 1 $$ 0 ∷ U′
  F<:⇒typing U T₂<:T₁ = <:⇒typing-gen U $ <:′⇒<: $ F<:⇒D<: T₂<:T₁
  
module DifferentProof where

  open import FsubMinus
  open FsubMinus.FsubMinus hiding (_↑_)
  open FsubMinusToDsubR
  open Undecidability

  F<:⇒typing′ : ∀ {Γ S U} →
                  Γ ⊢F S <: U →
                  ⟪ Γ ⟫ ⊢ val ⟨A= ⟦ S ⟧ ⟩ ∷ ⟨A: ⊥ ⋯ ⟦ U ⟧ ⟩
  F<:⇒typing′ S<:U = <:∷ ⟨A⟩-I (bnd ⊥<: (<:′⇒<: $ F<:⇒D<: S<:U))

  bnd<:⇒F<:′ : ∀ {Γ S U} →
                  ⟪ Γ ⟫ ⊢″ ⟨A: ⟦ S ⟧ ⋯ ⟦ S ⟧ ⟩ <: ⟨A: ⊥ ⋯ ⟦ U ⟧ ⟩ →
                  Γ ⊢F S <: U
  bnd<:⇒F<:′ {Γ} {S} {U} bnd<: = helper
    where open DsubInvProperties contraInvertible
          cΓ = ⟪⟫-contraEnv Γ
          helper : Γ ⊢F S <: U
          helper with ⟨A:⟩<:⟨A:⟩ (<:⇒<:ᵢ (<:″⇒<:′ bnd<:) cΓ) cΓ
          ... | _ , S<:U = D<:⇒F<: $ <:ᵢ⇒<: S<:U

  typing⇒F<:′ : ∀ {Γ S U} →
                  ⟪ Γ ⟫ ⊢ val ⟨A= ⟦ S ⟧ ⟩ ∷ ⟨A: ⊥ ⋯ ⟦ U ⟧ ⟩ →
                  Γ ⊢F S <: U
  typing⇒F<:′ typ = bnd<:⇒F<:′ $ <:′⇒<:″ $ <:⇒<:′ (helper typ refl refl)
    where helper : ∀ {Γ Γ′ S S′ T} →
                     Γ′ ⊢ val ⟨A= S′ ⟩ ∷ T →
                     Γ′ ≡ ⟪ Γ ⟫ → S′ ≡ ⟦ S ⟧ → 
                     ⟪ Γ ⟫ ⊢ ⟨A: ⟦ S ⟧ ⋯ ⟦ S ⟧ ⟩ <: T
          helper ⟨A⟩-I eqΓ refl = refl
          helper (<:∷ typ x) refl eqS = tran _ (helper typ refl eqS) x
