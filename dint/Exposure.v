Set Implicit Arguments.
Require Import Definitions.
Require Import Misc.

Inductive and_trav : typ -> typ -> Prop :=
| at_found : forall T, ~is_and T -> and_trav T T
| at_left : forall S U S',
    and_trav S S' ->
    and_trav (typ_and S U) S'
| at_right : forall S U U',
    and_trav U U' ->
    and_trav (typ_and S U) U'.
Hint Constructors and_trav.

Inductive exposure : env -> typ -> typ -> Prop :=
| ex_stop : forall G T,
    ~is_sel T ->
    exposure G T T
| ex_top : forall G T,
    exposure G T typ_top
| ex_bot : forall G T x A T0 T1,
    binds x T G ->
    and_trav T T0 ->
    exposure G T0 T1 ->
    and_trav T1 typ_bot ->
    exposure G (typ_sel (avar_f x) A) typ_bot
| ex_bnd : forall G T x A T0 T1 L U U0 U1,
    binds x T G ->
    and_trav T T0 ->
    exposure G T0 T1 ->
    and_trav T1 (typ_bnd A L U) ->
    and_trav U U0 ->
    exposure G U0 U1 ->
    exposure G (typ_sel (avar_f x) A) U1.
Hint Constructors exposure.

Inductive upcast_e : env -> avar -> typ_label -> typ -> Prop :=
| uce_top : forall G x A,
    upcast_e G x A typ_top
| uce_bot : forall G x A T T0 T1,
    binds x T G ->
    and_trav T T0 ->
    exposure G T0 T1 ->
    and_trav T1 typ_bot ->
    upcast_e G (avar_f x) A typ_bot
| uce_bnd : forall G x A T T0 T1 L U,
    binds x T G ->
    and_trav T T0 ->
    exposure G T0 T1 ->
    and_trav T1 (typ_bnd A L U) ->
    upcast_e G (avar_f x) A U.
Local Hint Constructors upcast_e.

Inductive downcast_e : env -> avar -> typ_label -> typ -> Prop :=
| dce_bot : forall G x A,
    downcast_e G x A typ_bot
| dce_top : forall G x A T T0 T1,
    binds x T G ->
    and_trav T T0 ->
    exposure G T0 T1 ->
    and_trav T1 typ_bot ->
    downcast_e G (avar_f x) A typ_top
| dce_bnd : forall G x A T T0 T1 L U,
    binds x T G ->
    and_trav T T0 ->
    exposure G T0 T1 ->
    and_trav T1 (typ_bnd A L U) ->
    downcast_e G (avar_f x) A L.
Local Hint Constructors downcast_e.

Section BasicProperties.

  Lemma exposure_not_sel : forall G S U,
    exposure G S U ->
    ~is_sel U.
  Proof.
    induction on exposure; routine.
  Qed.

  Lemma exposure_weakening_gen : forall G1 G2 S U,
      exposure (G1 ++ G2) S U ->
      forall G,
        exposure (G1 ++ G ++ G2) S U.
  Proof.
    dep induction on exposure; eroutine.
  Qed.

  Lemma exposure_weakening : forall G S U,
      exposure G S U ->
      forall G',
        exposure (G' ++ G) S U.
  Proof.
    intros. reassoc 2 with 0.
    eauto using exposure_weakening_gen.
  Qed.
  
  Lemma and_trav_subty : forall T T',
      and_trav T T' ->
      forall G,
        G ⊢ T <⦂ T'.
  Proof.
    induction on and_trav; eroutine.
  Qed.

  Lemma and_trav_fv : forall T T',
      and_trav T T' ->
      fv T' [<=] fv T.
  Proof. induction on and_trav; routine. Qed.

  Lemma and_trav_preserves_lc : forall T1 T2,
      and_trav T1 T2 ->
      forall n,
        lc_at n T1 ->
        lc_at n T2.
  Proof. induction on and_trav; routine. Qed.
    
End BasicProperties.

Ltac saturate_fv :=
  repeat match goal with
         | H : binds ?x ?T ?G, Hwf : wf_env ?G |- _ =>
           pose proof (binds_fv _ _ H Hwf); fail_if_dup
         | H : and_trav _ _ |- _ =>
           pose proof (and_trav_fv H); fail_if_dup
         end.

Lemma binds_lc : forall x T G,
    binds x T G ->
    wf_env G ->
    lc T.
Proof. induction G; routine. Qed.

Ltac saturate_lc :=
  repeat (match goal with
          | H : binds ?x ?T ?G, Hwf : wf_env ?G |- _ =>
            pose proof (binds_lc _ _ H Hwf); fail_if_dup
          | H : and_trav _ _ |- _ =>
            let H' := fresh "H" in
            rec_pose (and_trav_preserves_lc H) H'; [eassumption |]; fail_if_dup
          end; progressive_inversions).

Section MoreProperties.

  Lemma exposure_preserves_fv : forall G S U,
    exposure G S U ->
    wf_env G ->
    fv S [<=] dom G ->
    fv U [<=] dom G.
  Proof.
    induction on exposure; intros; set solve.
    saturate_fv. simpl in *; fold_cls.
    apply IHexposure2; trivial.  
    etransitivity; try eassumption.
    transitivity (fv T1); auto.
  Qed.
  
  Lemma exposure_preserves_lc : forall G S U,
      exposure G S U ->
      wf_env G ->
      lc S ->
      lc U.
  Proof.
    induction on exposure; routine.
    fold_cls. saturate_lc.
    rec_pose IHexposure1 Hrec1; trivial.
    saturate_lc.
    rec_pose IHexposure2 Hrec2; trivial.
  Qed.    
  
  Lemma exposure_strengthening : forall G1 G2 S U,
      exposure (G2 ++ G1) S U ->
      wf_env (G2 ++ G1) ->
      fv S [<=] dom G1 ->
      exposure G1 S U.
  Proof.
    dep induction on exposure;
      intros; auto.
    - simpl in H6.
      pose proof (wf_uniq H5).
      destruct_binds_hyp_uniq H2; [fsetdec |].
      clear_dups.
      pose proof H5. apply wf_deapp in H5.
      saturate_fv.
      rec_pose IHexposure Hrec; eauto.
    - simpl in H6.
      pose proof (wf_uniq H5).
      destruct_binds_hyp_uniq H3; [fsetdec |].
      clear_dups.
      pose proof H5. apply wf_deapp in H5.
      saturate_fv.
      rec_pose IHexposure1 Hrec1; eauto.
      eapply exposure_preserves_fv in Hrec1; eauto.
      rec_pose IHexposure2 Hrec2; eauto 10.
      simpl in *; fold_fv.
      set solve.
  Qed.

  Lemma upcast_e_preserves_fv : forall G x A T,
      upcast_e G x A T ->
      wf_env G ->
      fv x [<=] dom G ->
      fv T [<=] dom G.
  Proof.
    destr on upcast_e; intros; set solve.
    saturate_fv.
    apply exposure_preserves_fv in H1; auto.
    set solve.
  Qed.

  Lemma upcast_e_preserves_lc : forall G x A T,
      upcast_e G x A T ->
      wf_env G ->
      lc x ->
      lc T.
  Proof.
    destr on upcast_e; routine.
    saturate_lc.
    apply exposure_preserves_lc in H1; auto.
    saturate_lc.
    trivial.
  Qed.

  Lemma downcast_e_preserves_fv : forall G x A T,
      downcast_e G x A T ->
      wf_env G ->
      fv x [<=] dom G ->
      fv T [<=] dom G.
  Proof.
    destr on downcast_e; intros; set solve.
    saturate_fv.
    apply exposure_preserves_fv in H1; auto.
    set solve.
  Qed.

  Lemma downcast_e_preserves_lc : forall G x A T,
      downcast_e G x A T ->
      wf_env G ->
      lc x ->
      lc T.
  Proof.
    destr on downcast_e; routine.
    saturate_lc.
    apply exposure_preserves_lc in H1; auto.
    saturate_lc.
    trivial.
  Qed.
  
End MoreProperties.

Ltac saturate_fv ::=
  repeat match goal with
         | H : binds ?x ?T ?G, Hwf : wf_env ?G |- _ =>
           pose proof (binds_fv _ _ H Hwf); fail_if_dup
         | H : and_trav _ _ |- _ =>
           pose proof (and_trav_fv H); fail_if_dup
         | H : exposure ?G ?S ?U, Hwf : wf_env ?G |- _ =>
           let H' := fresh "H" in
           rec_pose (exposure_preserves_fv H Hwf) H'; [solve [set solve] |]; fail_if_dup
         | H : upcast_e ?G ?x ?A ?U, Hwf : wf_env ?G |- _ =>
           let H' := fresh "H" in
           rec_pose (upcast_e_preserves_fv H Hwf) H'; [solve [set solve] |]; fail_if_dup
         | H : downcast_e ?G ?x ?A ?U, Hwf : wf_env ?G |- _ =>
           let H' := fresh "H" in
           rec_pose (downcast_e_preserves_fv H Hwf) H'; [solve [set solve] |]; fail_if_dup
         end.

Ltac saturate_lc ::=
  repeat (match goal with
          | H : binds ?x ?T ?G, Hwf : wf_env ?G |- _ =>
            pose proof (binds_lc _ _ H Hwf); fail_if_dup
          | H : and_trav _ _ |- _ =>
            let H' := fresh "H" in
            rec_pose (and_trav_preserves_lc H) H'; [eassumption |]; fail_if_dup
          | H : exposure _ _ _ |- _ =>
            let H' := fresh "H" in
            rec_pose (exposure_preserves_lc H) H'; [eassumption | eassumption |]; fail_if_dup
          | H : upcast_e _ _ _ _ |- _ =>
            let H' := fresh "H" in
            rec_pose (upcast_e_preserves_lc H) H'; [eassumption | eassumption |]; fail_if_dup
          | H : downcast_e _ _ _ _ |- _ =>
            let H' := fresh "H" in
            rec_pose (downcast_e_preserves_lc H) H'; [eassumption | eassumption |]; fail_if_dup
          end; progressive_inversions).
