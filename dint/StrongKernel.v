Set Implicit Arguments.
Require Import Definitions.
Require Import Misc.
Require Import Exposure.
Require Import OpeSub.
Require Import ListRelations.
Require Import StructuralProperties.
Require Import OperationProperties.

Inductive subtysk : vars -> env -> typ -> typ -> env -> Prop :=
| ssk_refl : forall L G1 G2 x A, subtysk L G1 (typ_sel x A) (typ_sel x A) G2
| ssk_top : forall L G1 G2 T, subtysk L G1 T typ_top G2
| ssk_bot : forall L G1 G2 T, subtysk L G1 typ_bot T G2
| ssk_bnd : forall L G1 G2 A S T1 T2 U,
    subtysk L G2 S T1 G1 ->
    subtysk L G1 T2 U G2 ->
    subtysk L G1 (typ_bnd A T1 T2) (typ_bnd A S U) G2
| ssk_fld : forall L G1 G2 a T T',
    subtysk L G1 T T' G2 ->
    subtysk L G1 (typ_fld a T) (typ_fld a T') G2
| ssk_all : forall L G1 G2 T1 T2 U1 U2 x,
    x `notin` fv G1 `union` fv T1 `union` fv T2
      `union` fv U1 `union` fv U2 `union` fv G2 `union` L ->
    subtysk L G2 T2 T1 G1 ->
    subtysk (union L (union (singleton x) (fv T2)))
            (x ~ T1 ++ G1) (open x U1) (open x U2) (x ~ T2 ++ G2) ->
    subtysk L G1 (typ_all T1 U1) (typ_all T2 U2) G2
| ssk_sel1 : forall L G1 G2 x A T S,
    binds x T G2 ->
    subtysk L G2 T (typ_bnd A S typ_top) G1 ->
    subtysk L G1 S (typ_sel (avar_f x) A) G2
| ssk_sel2 : forall L G1 G2 x A T U,
    binds x T G1 ->
    subtysk L G1 T (typ_bnd A typ_bot U) G2 ->
    subtysk L G1 (typ_sel (avar_f x) A) U G2
| ssk_and11 : forall L G1 G2 S U T,
    subtysk L G1 S T G2 ->
    subtysk L G1 (typ_and S U) T G2
| ssk_and12 : forall L G1 G2 S U T,
    subtysk L G1 U T G2 ->
    subtysk L G1 (typ_and S U) T G2
| ssk_and2 : forall L G1 G2 T S U,
    subtysk L G1 T S G2 ->
    subtysk L G1 T U G2 ->
    subtysk L G1 T (typ_and S U) G2.
Hint Constructors subtysk.
Notation "[ L ] G1 ⊢k S <⦂ U k⊣ G2" := (subtysk L G1 S U G2) (at level 70).

Reserved Notation "[ L ] G1 >> T '<⦂p' U << G2" (at level 70).
Inductive bi_subty' : atoms -> env -> typ -> typ -> env -> Prop :=
| bs'_bot : forall L G1 T G2, [ L ] G1 >> typ_bot <⦂p T << G2
| bs'_top : forall L G1 T G2, [ L ] G1 >> T <⦂p typ_top << G2
| bs'_sel_refl : forall L G1 x G2 A, [ L ] G1 >> typ_sel x A <⦂p typ_sel x A << G2
| bs'_sel_left : forall L G1 x A G2 T U,
    upcast_e G1 x A T ->
    [ L ] G1 >> T <⦂p U << G2 ->
    [ L ] G1 >> typ_sel x A <⦂p U << G2
| bs'_sel_right : forall L G1 x A G2 T U,
    downcast_e G2 x A U ->
    [ L ] G1 >> T <⦂p U << G2 ->
    [ L ] G1 >> T <⦂p typ_sel x A << G2

| bs'_all : forall L G1 T1 U1 G2 T2 U2 x,
    x `notin` fv G1 `union` fv T1 `union` fv T2
      `union` fv U1 `union` fv U2 `union` fv G2 `union` L ->
    [ L ] G2 >> T2 <⦂p T1 << G1 ->
    [ L  `union` singleton x `union` fv T2 ]
      x ~ T1 ++ G1 >> open x U1 <⦂p open x U2 << x ~ T2 ++ G2 ->
    [ L ] G1 >> typ_all T1 U1 <⦂p typ_all T2 U2 << G2
| bs'_bnd : forall L G1 A S1 U1 S2 U2 G2,
    [ L ] G2 >> S2 <⦂p S1 << G1 ->
    [ L ] G1 >> U1 <⦂p U2 << G2 ->
    [ L ] G1 >> typ_bnd A S1 U1 <⦂p typ_bnd A S2 U2 << G2
| bs'_fld : forall L G1 a T T' G2,
    [ L ] G1 >> T <⦂p T' << G2 ->
    [ L ] G1 >> typ_fld a T <⦂p typ_fld a T' << G2
| bs'_and_left1 : forall L G1 G2 S U T,
    ~is_and T ->
    [ L ] G1 >> S <⦂p T << G2 ->
    [ L ] G1 >> typ_and S U <⦂p T << G2
| bs'_and_left2 : forall L G1 G2 S U T,
    ~is_and T ->
    [ L ] G1 >> U <⦂p T << G2 ->
    [ L ] G1 >> typ_and S U <⦂p T << G2
| bs'_and_right : forall L G1 G2 T S U,
    [ L ] G1 >> T <⦂p S << G2 ->
    [ L ] G1 >> T <⦂p U << G2 ->
    [ L ] G1 >> T <⦂p typ_and S U << G2
where "[ L ] G1 >> T '<⦂p' U << G2" := (bi_subty' L G1 T U G2)%type.
Local Hint Constructors bi_subty'.


Inductive subtysk' : vars -> env -> typ -> typ -> env -> nat -> Prop :=
| ssk'_refl : forall L G1 G2 x A, subtysk' L G1 (typ_sel x A) (typ_sel x A) G2 1
| ssk'_top : forall L G1 G2 T, subtysk' L G1 T typ_top G2 1
| ssk'_bot : forall L G1 G2 T, subtysk' L G1 typ_bot T G2 1
| ssk'_bnd : forall L G1 G2 A S T1 T2 U n1 n2,
    subtysk' L G2 S T1 G1 n1 ->
    subtysk' L G1 T2 U G2 n2 ->
    subtysk' L G1 (typ_bnd A T1 T2) (typ_bnd A S U) G2 (1 + n1 + n2)
| ssk'_fld : forall L G1 G2 a T T' n,
    subtysk' L G1 T T' G2 n ->
    subtysk' L G1 (typ_fld a T) (typ_fld a T') G2 (1 + n)
| ssk'_all : forall L G1 G2 T1 T2 U1 U2 x n1 n2,
    x `notin` fv G1 `union` fv T1 `union` fv T2
      `union` fv U1 `union` fv U2 `union` fv G2 `union` L ->
    subtysk' L G2 T2 T1 G1 n1 ->
    subtysk' (union L (union (singleton x) (fv T2)))
            (x ~ T1 ++ G1) (open x U1) (open x U2) (x ~ T2 ++ G2) n2 ->
    subtysk' L G1 (typ_all T1 U1) (typ_all T2 U2) G2 (1 + n1 + n2)
| ssk'_sel1 : forall L G1 G2 x A T S n,
    binds x T G2 ->
    subtysk' L G2 T (typ_bnd A S typ_top) G1 n ->
    subtysk' L G1 S (typ_sel (avar_f x) A) G2 (1 + n)
| ssk'_sel2 : forall L G1 G2 x A T U n,
    binds x T G1 ->
    subtysk' L G1 T (typ_bnd A typ_bot U) G2 n ->
    subtysk' L G1 (typ_sel (avar_f x) A) U G2 (1 + n)
| ssk'_and11 : forall L G1 G2 S U T n,
    subtysk' L G1 S T G2 n ->
    subtysk' L G1 (typ_and S U) T G2 (1 + n)
| ssk'_and12 : forall L G1 G2 S U T n,
    subtysk' L G1 U T G2 n ->
    subtysk' L G1 (typ_and S U) T G2 (1 + n)
| ssk'_and2 : forall L G1 G2 T S U n1 n2,
    subtysk' L G1 T S G2 n1 ->
    subtysk' L G1 T U G2 n2 ->
    subtysk' L G1 T (typ_and S U) G2 (1 + n1 + n2).
Local Hint Constructors subtysk'.
Notation "[ L , n ] G1 ⊢k S <⦂ U k⊣ G2" := (subtysk' L G1 S U G2 n) (at level 70).

Lemma subtysk_to_subtysk' : forall L G1 S U G2,
    [ L ] G1 ⊢k S <⦂ U k⊣ G2 ->
    exists n, [ L , n ] G1 ⊢k S <⦂ U k⊣ G2.
Proof.
  induction on subtysk; eroutine.
Qed.

Lemma subtysk'_to_subtysk : forall L G1 S U G2 n,
    [ L , n ] G1 ⊢k S <⦂ U k⊣ G2 ->
    [ L ] G1 ⊢k S <⦂ U k⊣ G2.
Proof.
  induction on subtysk'; eroutine.
Qed.

Lemma subtysk_equiv_subtysk' : forall L G1 S U G2,
    [ L ] G1 ⊢k S <⦂ U k⊣ G2 <->
    exists n, [ L , n ] G1 ⊢k S <⦂ U k⊣ G2.
Proof.
  split; auto using subtysk_to_subtysk'.
  intros. tidy_up.
  eauto using subtysk'_to_subtysk.
Qed.

Local Ltac wf_env :=
  lazymatch goal with
  | H : wf_env (_ ++ _) |- _ => apply wf_deapp in H; invert H; subst
  end.

Local Hint Constructors upcast_e downcast_e.
Local Hint Resolve exposure_weakening_gen.

Lemma bi_subty'_weakening_gen: forall L' G1 G2 S U G3 G4,
    [ L' ] G1 ++ G2 >> S <⦂p U << G3 ++ G4 ->
    forall G G' L,
      L' [=] L `union` fv G' `union` fv G ->
      [ L ] G1 ++ G ++ G2 >> S <⦂p U << G3 ++ G' ++ G4.
Proof.
  intros. dependent induction H; intros; eroutine.
  - dependent destruction H; eauto 7.
  - dependent destruction H; eauto 7.
  - eapply bs'_all with x.
    + change (union (dom (G1 ++ G2)) (fv_values fv_typ (G1 ++ G2)))
        with (fv (G1 ++ G2)) in *.
      change (union (dom (G3 ++ G4)) (fv_values fv_typ (G3 ++ G4)))
        with (fv (G3 ++ G4)) in *.
      change (union (dom G) (fv_values fv_typ G)) with (fv G) in *.
      change (union (dom G') (fv_values fv_typ G')) with (fv G') in *.
      repeat rewrite fv_union in *. fold_cls.
      fsetdec.
    + apply IHbi_subty'1; trivial.
      rewrite H2. clear H H2. fsetdec.
    + reassoc 4 with 2.
      replace (x ~ T2 ++ G3 ++ G' ++ G4) with
          ((x ~ T2 ++ G3) ++ G' ++ G4).
            
      apply IHbi_subty'2; try solve [routine].
      * rewrite H2. clear H H2. fsetdec.
      * simpl_env. trivial.
  - constructor.
    + apply IHbi_subty'1; trivial.
      rewrite H1. fsetdec.
    + apply IHbi_subty'2; trivial.
Qed.

Lemma bi_subty'_weakening_l: forall L G1 G2 S U G,
    [ L `union` fv G ] G1 >> S <⦂p U << G2 ->
    [ L ] G ++ G1 >> S <⦂p U << G2.
Proof.
  intros.
  change (G ++ G1) with (nil ++ G ++ G1).
  change G2 with (nil ++ nil ++ G2).
  eapply bi_subty'_weakening_gen; eauto.
  simpl. fsetdec.
Qed.
  
Lemma bi_subty'_weakening_r: forall L G1 G2 S U G,
    [ L `union` fv G ] G1 >> S <⦂p U << G2 ->
    [ L ] G1 >> S <⦂p U << G ++ G2.
Proof.
  intros.
  change (G ++ G2) with (nil ++ G ++ G2).
  change G1 with (nil ++ nil ++ G1).
  eapply bi_subty'_weakening_gen; eauto.
  simpl. fsetdec.
Qed.

Local Hint Extern 1 (_ < _) => lia.
Local Hint Extern 1 (_ <= _) => lia.

Program Fixpoint subtysk_refl T {measure (typ_struct_measure T)} :
  forall L G1 G2,
    [ L ] G1 ⊢k T <⦂ T k⊣ G2 := _.
Next Obligation.
  destruct T; eroutine at 6.
  pick_fresh x.
  apply ssk_all with x; auto.
  apply subtysk_refl; auto.
  autorewrite with measures; lia.
Qed.
Local Hint Resolve subtysk_refl.

Local Hint Constructors ope_sub.

Lemma subtysk_sound_gen : forall L G1 S U G2,
    [ L ] G1 ⊢k S <⦂ U k⊣ G2 ->
    forall G,
      fv G [<=] L ->
      ope_sub G1 G ->
      ope_sub G2 G ->
      uniq G ->
      G ⊢ S <⦂ U.
Proof.
  induction on subtysk; intros; auto.
  4,5:eauto.
  - eapply st_all; [eauto |].
    simpl in H0.
    rec_pose IHsubtysk1 HS; try eassumption.
    cofinite.
    apply open_subst_subty with x; auto.
    + simpl. solve_notin.
    + apply IHsubtysk2; simpl; auto.
      * clear H Fr. fsetdec.
      * solve_uniq.
  - eapply ope_narrow_var in H; try eassumption.
    destruct_conjs.
    eauto.
  - eapply ope_narrow_var in H; try eassumption.
    destruct_conjs.
    eauto.
Qed.

Theorem subtysk_sound : forall G S U,
    [ fv G ] G ⊢k S <⦂ U k⊣ G ->
    uniq G ->
    G ⊢ S <⦂ U.
Proof.
  eauto using subtysk_sound_gen, ope_sub_refl.
Qed.

Fixpoint bnd_layer (T : typ) (Ts : list (typ_label * typ)) : typ :=
  match Ts with
  | nil => T
  | cons (A, T') Ts =>
    typ_bnd A T' (bnd_layer T Ts)
  end.

Lemma sk_trans_on_top : forall L G1 T G2,
    [ L ] G1 ⊢k typ_top <⦂ T k⊣ G2 ->
    forall S,
      [ L ] G1 ⊢k S <⦂ T k⊣ G2
with layered_top_trans : forall L G1 T l U G2 A,
    [ L ] G1 ⊢k T <⦂ bnd_layer (typ_bnd A typ_top U) l k⊣ G2 ->
    forall S,
      [ L ] G1 ⊢k T <⦂ bnd_layer (typ_bnd A S U) l k⊣ G2.
Proof.
  - clear sk_trans_on_top.
    dep induction on subtysk; routine.
    econstructor; eauto.
    apply layered_top_trans with (l := nil).
    assumption.

  - clear layered_top_trans.
    intros. gen S.
    dependent induction H; intros; eauto.
    1-6,8:induction l; eroutine.

    econstructor; eauto.
    specialize (IHsubtysk (cons (A0, _) l) U _ eq_refl).
    simpl in IHsubtysk. auto.
Qed.
Local Hint Resolve sk_trans_on_top.

Lemma sk_trans_on_bot : forall L G1 T G2,
    [ L ] G1 ⊢k T <⦂ typ_bot k⊣ G2 ->
    forall U,
      [ L ] G1 ⊢k T <⦂ U k⊣ G2
with layered_bot_trans : forall L G1 T l S G2 A,
    [ L ] G1 ⊢k T <⦂ bnd_layer (typ_bnd A S typ_bot) l k⊣ G2 ->
    forall U,
      [ L ] G1 ⊢k T <⦂ bnd_layer (typ_bnd A S U) l k⊣ G2.
Proof.
  - clear sk_trans_on_bot.
    dep induction on subtysk; routine.
    econstructor; eauto.
    apply layered_bot_trans with (l := nil).
    assumption.

  - clear layered_bot_trans.
    intros. gen U.
    dependent induction H; intros; eauto.
    1-6,8:induction l; eroutine.

    econstructor; eauto.
    specialize (IHsubtysk (cons (_, _) l) S _ eq_refl).
    simpl in IHsubtysk. auto.
Qed.
Local Hint Resolve sk_trans_on_bot.

Program Fixpoint subtysk'_split_and_gen (n : nat) {measure n} :
  (forall L G1 T S U G2,
    [ L , n ] G1 ⊢k T <⦂ typ_and S U k⊣ G2 ->
    exists n1 n2,
      [ L , n1 ] G1 ⊢k T <⦂ S k⊣ G2 /\ n1 <= n /\
      [ L , n2 ] G1 ⊢k T <⦂ U k⊣ G2 /\ n2 <= n)
  /\ (forall L G1 T S U l G2,
        [ L , n ] G1 ⊢k T <⦂ bnd_layer (typ_and S U) l k⊣ G2 ->
       exists n1 n2,
         [ L , n1 ] G1 ⊢k T <⦂ bnd_layer S l k⊣ G2 /\ n1 <= n /\
         [ L , n2 ] G1 ⊢k T <⦂ bnd_layer U l k⊣ G2 /\ n2 <= n) := _.
Next Obligation.
  split.
  - intros. dependent destruction H; eauto 6.
    + change (typ_bnd A ?T (typ_and S U))
        with (bnd_layer (typ_and S U) ((A, T) :: nil)) in *.
      apply subtysk'_split_and_gen in H0; auto.
      tidy_up. eauto 8.
    + apply subtysk'_split_and_gen in H; auto.
      tidy_up; eauto 8.
    + apply subtysk'_split_and_gen in H; auto.
      tidy_up; eauto 8.

  - intros. dependent destruction H; eauto 6.
    1-2,4-6:induction l; eroutine.

    + destruct l; tidy_up.
      apply subtysk'_split_and_gen in H0; auto.
      tidy_up; eauto 8.
    + change (typ_bnd A ?S0 (bnd_layer (typ_and S U) l))
        with (bnd_layer (typ_and S U) ((A, S0) :: l)) in *.
      apply subtysk'_split_and_gen in H0; auto.
      tidy_up; eauto 8.
    + apply subtysk'_split_and_gen in H; auto.
      tidy_up; eauto 8.
    + apply subtysk'_split_and_gen in H; auto.
      tidy_up; eauto 8.
    + destruct l; tidy_up.
      eauto 6.
Qed.    

Lemma subtysk'_split_and : forall L n G1 T S U G2,
    [ L , n ] G1 ⊢k T <⦂ typ_and S U k⊣ G2 ->
    exists n1 n2,
      [ L , n1 ] G1 ⊢k T <⦂ S k⊣ G2 /\ n1 <= n /\
      [ L , n2 ] G1 ⊢k T <⦂ U k⊣ G2 /\ n2 <= n.
Proof.
  intros. apply subtysk'_split_and_gen.
  trivial.
Qed.

Lemma and_trav_to_subtysk : forall T T',
    and_trav T T' ->
    forall L G1 G2,
      [ L ] G1 ⊢k T <⦂ T' k⊣ G2.
Proof.
  induction on and_trav; eroutine.
Qed.

Lemma and_trav_to_subtysk2 : forall S U,
    and_trav S U ->
    forall L U' G1 G2,
      [ L ] G1 ⊢k U <⦂ U' k⊣ G2 ->
      [ L ] G1 ⊢k S <⦂ U' k⊣ G2.
Proof.
  induction on and_trav; eroutine.
Qed.

Lemma and_trav_to_subtysk_bnd1 : forall T A S U,
    and_trav T (typ_bnd A S U) ->
    forall L G1 G2 U',
      [ L ] G1 ⊢k U <⦂ U' k⊣ G2 ->
      [ L ] G1 ⊢k T <⦂ typ_bnd A S U' k⊣ G2.
Proof.
  dep induction on and_trav; eroutine.
Qed.

Lemma and_trav_to_subtysk_bnd2 : forall T A S U,
    and_trav T (typ_bnd A S U) ->
    forall L G1 G2 S',
      [ L ] G2 ⊢k S' <⦂ S k⊣ G1 ->
      [ L ] G1 ⊢k T <⦂ typ_bnd A S' U k⊣ G2.
Proof.
  dep induction on and_trav; eroutine.
Qed.

Local Hint Resolve and_trav_to_subtysk and_trav_to_subtysk2
      and_trav_to_subtysk_bnd1 and_trav_to_subtysk_bnd2.

Local Ltac solve_and_trav :=
  try match goal with
      | H : and_trav _ _ |- _ =>
        solve [eauto using sk_trans_on_bot]
      end.

Theorem exposure_to_subtysk : forall G S U,
    exposure G S U ->
    forall G' L U',
      [ L ] G ⊢k U <⦂ U' k⊣ G' ->
      [ L ] G ⊢k S <⦂ U' k⊣ G'.
Proof.
  induction on exposure; routine.
  - econstructor; try eassumption.
    eauto 10.
  - eapply ssk_sel2; try eassumption.
    eapply and_trav_to_subtysk2; eauto 3.
    apply IHexposure1.
    eapply and_trav_to_subtysk2; eauto.
Qed.
Local Hint Resolve exposure_to_subtysk.

Local Ltac ex_stop := apply ex_stop; auto.

Theorem bi_subty'_to_subtysk : forall L G1 S U G2,
    [ L ] G1 >> S <⦂p U << G2 ->
    [ L ] G1 ⊢k S <⦂ U k⊣ G2.
Proof.
  induction on bi_subty'; routine.
  - destruct H; auto.
    + econstructor. eauto.
      eauto 6.
    + econstructor. eassumption.
      repeat (eapply exposure_to_subtysk + eapply and_trav_to_subtysk2;
              [eassumption |]).
      eauto.
  - destruct H; auto.
    + econstructor. eauto.
      eauto 6.
    + econstructor. eassumption.
      repeat (eapply exposure_to_subtysk + eapply and_trav_to_subtysk2;
              [eassumption |]).
      eauto.
  - eauto.
Qed.

Program Fixpoint subtysk'_conversions n {measure n} : forall L G1 S U G2,
    [ L , n ] G1 ⊢k S <⦂ U k⊣ G2 ->
    [ L ] G1 >> S <⦂p U << G2 /\
    (forall A T1 T2,
        U = typ_bnd A T1 T2 ->
        exists S0 S1, and_trav S S0 /\
                 exposure G1 S0 S1 /\
                 (and_trav S1 typ_bot \/
                  exists T1' T2' n',
                    and_trav S1 (typ_bnd A T1' T2') /\
                    [ L ] G2 >> T1 <⦂p T1' << G1 /\
                    ([ L , n' ] G1 ⊢k T2' <⦂ T2 k⊣ G2) /\ n' <= n))
  := _.
Next Obligation.
  split; intros.
  - induction H; routine.
    + eapply bs'_all with x; auto.
    + clear IHsubtysk'.
      apply subtysk'_conversions in H0; auto.
      tidy_up.
      specialize (H1 _ _ _ eq_refl).
      tidy_up; eauto.
    + clear IHsubtysk'.
      apply subtysk'_conversions in H0; auto.
      tidy_up.
      specialize (H1 _ _ _ eq_refl).
      tidy_up; eauto.

      apply subtysk'_conversions in H10; auto.
      tidy_up; eauto.
      
    + clear IHsubtysk'.
      assert (forall T' n' L G1 S G2,
                 [L , n'] G1 ⊢k S <⦂ T' k⊣ G2 ->
                 n' <= n ->
                 [ L ] G1 >> typ_and S U <⦂p T' << G2). {
        induction T'; intros;
          try solve [apply bs'_and_left1; auto;
                     eapply subtysk'_conversions; try eassumption; eauto].
        apply subtysk'_split_and in H0.
        tidy_up. apply bs'_and_right; eauto.
      }
      eapply H0; eauto.
    + clear IHsubtysk'.
      assert (forall T' n' L G1 U G2,
                 [L , n'] G1 ⊢k U <⦂ T' k⊣ G2 ->
                 n' <= n ->
                 [ L ] G1 >> typ_and S U <⦂p T' << G2). {
        induction T'; intros;
          try solve [apply bs'_and_left2; auto;
                     eapply subtysk'_conversions; try eassumption; eauto].
        apply subtysk'_split_and in H0.
        tidy_up. apply bs'_and_right; eauto.
      }
      eapply H0; eauto.

  - destruct H; subst; progressive_inversions; eauto 20.
    + repeat eexists.
      * auto.
      * ex_stop.
      * right. repeat eexists; try eassumption; auto.
        eapply subtysk'_conversions; try eassumption.
        lia.
    + apply subtysk'_conversions in H1; try lia.
      tidy_up.
      specialize (H1 _ _ _ eq_refl).
      tidy_up.
      * eauto 12.
      * apply subtysk'_conversions in H10; auto.
        tidy_up. specialize (H12 _ _ _ eq_refl).
        tidy_up; eauto 16.

    + apply subtysk'_conversions in H; auto.
      tidy_up.
      specialize (H0 _ _ _ eq_refl).
      tidy_up; eauto 20.

    + apply subtysk'_conversions in H; auto.
      tidy_up.
      specialize (H0 _ _ _ eq_refl).
      tidy_up; eauto 20.
Qed.

Theorem subtysk_to_bi_subty' : forall L G1 S U G2,
    [ L ] G1 ⊢k S <⦂ U k⊣ G2 ->
    [ L ] G1 >> S <⦂p U << G2.
Proof.
  intros. apply subtysk_to_subtysk' in H.
  tidy_up. eapply subtysk'_conversions.
  eassumption.
Qed.
