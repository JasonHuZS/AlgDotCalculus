Set Implicit Arguments.
Require Import Definitions.
Require Import OperationProperties.
Require Import StructuralProperties.
Require Import ListRelations.

Inductive ope_sub : env -> env -> Prop :=
| os_nil : ope_sub nil nil
| os_drop : forall G1 G2 x T,
    ope_sub G1 G2 ->
    ope_sub G1 ((x, T) :: G2)
| os_keep : forall G1 G2 x T1 T2,
    ope_sub G1 G2 ->
    G2 ⊢ T1 <⦂ T2 ->
    ope_sub ((x, T2) :: G1) ((x, T1) :: G2).
Local Hint Constructors ope_sub.

Section OpeProperties.
  
  Hint Resolve weaken_subty.

  Lemma ope_narrow_var :
    forall G G' x T,
      ope_sub G G' ->
      binds x T G ->
      exists T', binds x T' G' /\ (G' ⊢ T' <⦂ T).
  Proof.
    induction on ope_sub; intros.
    - routine.
    - apply IHope_sub in H. tidy_up.
      simpl_env.
      eauto using weaken_subty.
    - tidy_up.
      + simpl_env.
        eauto using weaken_subty.
      + apply IHope_sub in H0.
        tidy_up. simpl_env.
        eauto using weaken_subty.
  Qed.
  
  Lemma ope_narrow_subty : forall G T U,
      G ⊢ T <⦂ U ->
      forall G',
        ope_sub G G' ->
        G' ⊢ T <⦂ U.
  Proof.
    induction on subty; intros; eauto 4.
    - eapply st_all. auto.
      cofinite.
      apply H0; simpl; auto.
    - rec_pose (ope_narrow_var _ _ H0 H) Hv.
      tidy_up. eauto.
    - rec_pose (ope_narrow_var _ _ H0 H) Hv.
      tidy_up. eauto.
  Qed.
  Hint Resolve ope_narrow_subty.
  
  Lemma ope_sub_trans : forall G1 G2 G3,
      ope_sub G1 G2 ->
      ope_sub G2 G3 ->
      ope_sub G1 G3.
  Proof.
    intros. gen G1. induction H0; intros; auto.
    invert H1; subst; eauto.
  Qed.

  Lemma ope_sub_refl : forall G,
      ope_sub G G.
  Proof. induction on env; routine. Qed.

  Lemma ope_sub_nil : forall G,
      ope_sub nil G.
  Proof. induction on env; routine. Qed.

  Lemma ope_sub_app_r : forall G G1 G2,
      ope_sub G G2 ->
      ope_sub G (G1 ++ G2).
  Proof. induction G1; routine. Qed.

  Hint Resolve ope_sub_refl.
  
  Lemma prefix_to_ope_sub : forall G1 G2,
      prefix G1 G2 ->
      ope_sub G1 G2.
  Proof. induction on @prefix; routine. Qed.
    
End OpeProperties.
