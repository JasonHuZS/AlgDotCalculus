Set Implicit Arguments.
Require Import Definitions.
Require Import Exposure.
Require Import Misc.
Require Import OperationProperties.
Require Import StructuralProperties.
Require Import ListRelations.
Require Import OpeSub.

Inductive revealing : env -> typ -> env -> typ -> Prop :=
| rv_stop : forall G T,
    ~is_sel T ->
    revealing G T G T
| rv_top : forall G T,
    revealing G T nil typ_top
| rv_bot : forall G1 G2 T x A G1' T0 T1,
    and_trav T T0 ->
    revealing G1 T0 G1' T1 ->
    and_trav T1 typ_bot ->
    revealing (G2 ++ x ~ T ++ G1) (typ_sel (avar_f x) A) nil typ_bot
| rv_bnd : forall G1 G2 T x A G1' G T0 T1 L U U0 U1,
    and_trav T T0 ->
    revealing G1 T0 G1' T1 ->
    and_trav T1 (typ_bnd A L U) ->
    and_trav U U0 ->
    revealing G1' U0 G U1 ->
    revealing (G2 ++ x ~ T ++ G1) (typ_sel (avar_f x) A) G U1.
Hint Constructors revealing.

Inductive upcast : env -> avar -> typ_label -> env -> typ -> Prop :=
| uc_top : forall G x A,
    upcast G x A nil typ_top
| uc_bot : forall G1 G2 x A T G1' T0 T1,
    and_trav T T0 ->
    revealing G1 T0 G1' T1 ->
    and_trav T1 typ_bot ->
    upcast (G2 ++ x ~ T ++ G1) (avar_f x) A nil typ_bot
| uc_bnd : forall G1 G2 x A T G1' T0 T1 L U,
    and_trav T T0 ->
    revealing G1 T0 G1' T1 ->
    and_trav T1 (typ_bnd A L U) ->
    upcast (G2 ++ x ~ T ++ G1) (avar_f x) A G1' U.
Local Hint Constructors upcast.

Inductive downcast : env -> avar -> typ_label -> env -> typ -> Prop :=
| dc_bot : forall G x A,
    downcast G x A nil typ_bot
| dc_top : forall G1 G2 x A T G1' T0 T1,
    and_trav T T0 ->
    revealing G1 T0 G1' T1 ->
    and_trav T1 typ_bot ->
    downcast (G2 ++ x ~ T ++ G1) (avar_f x) A nil typ_top
| dc_bnd : forall G1 G2 x A T G1' T0 T1 L U,
    and_trav T T0 ->
    revealing G1 T0 G1' T1 ->
    and_trav T1 (typ_bnd A L U) ->
    downcast (G2 ++ x ~ T ++ G1) (avar_f x) A G1' L.
Local Hint Constructors downcast.

Reserved Notation "[ L ] G1 >> T '<⦂' U << G2" (at level 70).
Inductive bi_subty : atoms -> env -> typ -> typ -> env -> Prop :=
| bs_bot : forall L G1 T G2, [ L ] G1 >> typ_bot <⦂ T << G2
| bs_top : forall L G1 T G2, [ L ] G1 >> T <⦂ typ_top << G2
| bs_sel_refl : forall L G1 x G2 A, [ L ] G1 >> typ_sel x A <⦂ typ_sel x A << G2
| bs_sel_left : forall L G1 x A G2 T U G1',
    upcast G1 x A G1' T ->
    [ L ] G1' >> T <⦂ U << G2 ->
    [ L ] G1 >> typ_sel x A <⦂ U << G2
| bs_sel_right : forall L G1 x A G2 T U G2',
    downcast G2 x A G2' U ->
    [ L ] G1 >> T <⦂ U << G2' ->
    [ L ] G1 >> T <⦂ typ_sel x A << G2

| bs_all : forall L G1 T1 U1 G2 T2 U2 x,
    x `notin` fv G1 `union` fv T1 `union` fv T2
      `union` fv U1 `union` fv U2 `union` fv G2 `union` L ->
    [ L ] G2 >> T2 <⦂ T1 << G1 ->
    [ L  `union` singleton x `union` fv T2 ]
      x ~ T1 ++ G1 >> open x U1 <⦂ open x U2 << x ~ T2 ++ G2 ->
    [ L ] G1 >> typ_all T1 U1 <⦂ typ_all T2 U2 << G2
| bs_bnd : forall L G1 A S1 U1 S2 U2 G2,
    [ L ] G2 >> S2 <⦂ S1 << G1 ->
    [ L ] G1 >> U1 <⦂ U2 << G2 ->
    [ L ] G1 >> typ_bnd A S1 U1 <⦂ typ_bnd A S2 U2 << G2
| bs_fld : forall L G1 a T T' G2,
    [ L ] G1 >> T <⦂ T' << G2 ->
    [ L ] G1 >> typ_fld a T <⦂ typ_fld a T' << G2
| bs_and_left1 : forall L G1 G2 S U T,
    ~is_and T ->
    [ L ] G1 >> S <⦂ T << G2 ->
    [ L ] G1 >> typ_and S U <⦂ T << G2
| bs_and_left2 : forall L G1 G2 S U T,
    ~is_and T ->
    [ L ] G1 >> U <⦂ T << G2 ->
    [ L ] G1 >> typ_and S U <⦂ T << G2
| bs_and_right : forall L G1 G2 T S U,
    [ L ] G1 >> T <⦂ S << G2 ->
    [ L ] G1 >> T <⦂ U << G2 ->
    [ L ] G1 >> T <⦂ typ_and S U << G2
where "[ L ] G1 >> T '<⦂' U << G2" := (bi_subty L G1 T U G2)%type.
Hint Constructors bi_subty.

Local Hint Resolve prefix_nil pf_refl pf_cons prefix_trans prefix_app.

Section RevealingSubsumesExposure.
  
  Lemma revealing_gives_prefix : forall G T G' U,
    revealing G T G' U ->
    prefix G' G.
  Proof.
    induction on revealing; routine.
    etransitivity; [ | apply prefix_app].
    apply pf_cons.
    eauto.
  Qed.
  
  Lemma revealing_sel_weakening : forall G x A D U,
      revealing G (typ_sel (avar_f x) A) D U ->
      forall G',
        revealing (G' ++ G) (typ_sel (avar_f x) A) D U.
  Proof.
    dep induction on revealing;
      try solve [routine]; intros.
    - simpl in *. contradiction.
    - reassoc 4 with 2. eauto.
    - reassoc 4 with 2. eauto.
  Qed.

  Lemma revealing_sel_weakening_prefix : forall G x A D U,
      revealing G (typ_sel (avar_f x) A) D U ->
      forall G',
        prefix G G' ->
        revealing G' (typ_sel (avar_f x) A) D U.
  Proof.
    intros. apply prefix_to_app in H0.
    tidy_up. apply revealing_sel_weakening.
    trivial.
  Qed.

  Lemma revealing_preserves_fv : forall G S D U,
      revealing G S D U ->
      wf_env G ->
      fv S [<=] dom G ->
      fv U [<=] dom D.
  Proof.
    induction on revealing; intros; set solve.
    saturate_fv.
    simpl in *; fold_fv.
    wf_env.
    rec_pose IHrevealing1 Hrec1; auto.
    apply revealing_gives_prefix in H3_.
    apply prefix_wf_env in H3_; trivial.
    rec_pose IHrevealing2 Hrec2; auto.
    apply revealing_gives_prefix in H3_0.
    fold_fv. set solve.
  Qed.    

  Lemma revealing_preserves_lc : forall G S D U,
      revealing G S D U ->
      wf_env G ->
      lc S ->
      lc U.
  Proof.
    induction on revealing; routine.
    simpl in *; fold_cls.
    wf_env.
    saturate_lc.
    rec_pose IHrevealing1 Hrec1; auto.
    apply revealing_gives_prefix in H3_.
    apply prefix_wf_env in H3_; trivial.
    saturate_lc.
    rec_pose IHrevealing2 Hrec2; auto.
  Qed.
  
  Theorem exposure_to_revealing_gen : forall G S U,
      exposure G S U ->
      wf_env G ->
      forall G',
        prefix G' G ->
        fv S [<=] dom G' ->
        exists D,
          revealing G' S D U.
  Proof.
    induction on exposure; try solve [eroutine]; intros.
    - simpl in H5.
      rewrite <- in_subset_singleton in H5.
      pose proof (wf_uniq H3).
      apply in_witness in H5.
      apply prefix_to_app in H4.
      destruct_conjs. subst.
      pose proof H.
      replace (H4 ++ H7 ++ [(x, H5)] ++ H8)
        with ((H4 ++ H7) ++ [(x, H5)] ++ H8) in H by (simpl_env; trivial).
      apply binds_mid_eq in H; simpl_env; trivial.
      subst.
      pose proof H3. do 2 apply wf_deapp in H3.
      invert H3; subst.
      saturate_fv.
      specialize (IHexposure ltac:(trivial) H8 ltac:(eauto) ltac:(set solve)).
      destruct_conjs.

      eexists.
      eapply rv_bot; eassumption.

    - simpl in H5.
      rewrite <- in_subset_singleton in H5.
      pose proof (wf_uniq H3).
      apply in_witness in H5.
      apply prefix_to_app in H4.
      destruct_conjs. subst.
      pose proof H.
      replace (H4 ++ H7 ++ [(x, H5)] ++ H8)
        with ((H4 ++ H7) ++ [(x, H5)] ++ H8) in H by (simpl_env; trivial).
      apply binds_mid_eq in H; simpl_env; trivial.
      subst.
      pose proof H3. do 2 apply wf_deapp in H3.
      invert H3; subst.
      saturate_fv.
      specialize (IHexposure1 ltac:(trivial) H8 ltac:(eauto) ltac:(set solve)).
      destruct_conjs.
      
      do 2 pose proof H20.
      apply revealing_gives_prefix in H20.
      apply revealing_preserves_fv in H21; auto.
      specialize (IHexposure2 ltac:(trivial) IHexposure1 ltac:(eauto) ltac:(set solve)).
      destruct_conjs.
      
      eexists.
      eapply rv_bnd; eassumption.
  Qed.

  Theorem exposure_to_revealing : forall G S U,
      exposure G S U ->
      wf_env G ->
      fv S [<=] dom G ->
      exists D,
        revealing G S D U.
  Proof.
    intros. eapply exposure_to_revealing_gen; eauto.
  Qed.

  Theorem revealing_to_exposure : forall G S D U,
      revealing G S D U ->
      exposure G S U.
  Proof.
    induction on revealing; routine.
    - pose proof (binds_for_sure G2 G1 x T).
      apply revealing_gives_prefix in H3.
      apply prefix_to_app in H3.
      tidy_up. 
      eapply ex_bot.
      + eassumption.
      + eassumption.
      + simpl_env.
        do 2 eapply exposure_weakening.
        eassumption.
      + eassumption.
    - pose proof (binds_for_sure G2 G1 x T).
      apply revealing_gives_prefix in H3_.
      apply prefix_to_app in H3_.
      apply revealing_gives_prefix in H3_0.
      apply prefix_to_app in H3_0.
      tidy_up.
      eapply ex_bnd.
      + eassumption.
      + eassumption.
      + simpl_env.
        do 2 eapply exposure_weakening.
        eassumption.
      + eassumption.
      + eassumption.
      + simpl_env.
        do 3 eapply exposure_weakening.
        eassumption.
  Qed.

  Theorem exposure_equiv_revealing : forall G S U,
      wf_env G ->
      fv S [<=] dom G ->
      exposure G S U <->
      exists D,
        revealing G S D U.
  Proof.
    split; intros.
    - apply exposure_to_revealing; trivial.
    - tidy_up. eapply revealing_to_exposure; eassumption.
  Qed.      
    
End RevealingSubsumesExposure.

Local Ltac by_weakening :=
  once ((reassoc 3 with 2 + reassoc 4 with 3); apply weaken_subty; eassumption).

Section RevealingProperties.

  Inductive revealing_termination : forall G T G' U,
    revealing G T G' U -> Prop :=
  | rt_stop : forall G T
                (not_sel : ~is_sel T),
      revealing_termination (rv_stop G T not_sel)
  | rt_top : forall G T,
      revealing_termination (rv_top G T)
  | rt_bot : forall G1 G2 T x A G1' T0 T1
                 (At : and_trav T T0)
                 (Rec : revealing G1 T0 G1' T1)
                 (At' : and_trav T1 typ_bot),
      length (G2 ++ x ~ T ++ G1) > length G1 ->
      revealing_termination (rv_bot G2 x A At Rec At')
  | rt_bnd : forall G1 G2 T (x : var) A G1' G T0 T1 L U U0 U1
               (At1 : and_trav T T0)
               (Rec1 : revealing G1 T0 G1' T1)
               (At2 : and_trav T1 (typ_bnd A L U))
               (At3 : and_trav U U0)
               (Rec2 : revealing G1' U0 G U1),
      length (G2 ++ x ~ T ++ G1) > length G1 ->
      length (G2 ++ x ~ T ++ G1) > length G1' ->
      revealing_termination (rv_bnd G2 x At1 Rec1 At2 At3 Rec2).
  Hint Constructors revealing_termination.  
  
  Theorem revealing_sound : forall G T G' U,
      revealing G T G' U ->
      (G ⊢ T <⦂ U) /\ ~is_sel U.
  Proof.
    induction on revealing.
    all:try lazymatch goal with
            | |- subty (?l2 ++ ?x ~ ?p ++ ?l1) _ _ /\ _ =>
              pose proof (binds_for_sure l2 l1 x p)
            end.
    all:try solve [repeat split; auto].
    all:destruct_conjs.
    - repeat split; auto.
      eapply st_sel2. eauto.
      eapply st_trans; [apply and_trav_subty; eauto |].
      eapply st_trans.
      + by_weakening.
      + eapply st_trans; [apply and_trav_subty; eauto |].
        instantiate (1 := typ_top). trivial.
    - repeat split; auto.
      eapply st_sel2. eauto.
      eapply st_trans; [apply and_trav_subty; eauto |].
      eapply st_trans.
      + by_weakening.
      + eapply st_trans; [apply and_trav_subty; eauto |].
        eapply st_bnd. apply st_refl.
        eapply st_trans; [apply and_trav_subty; eauto |].
        apply revealing_gives_prefix in H3_.
        apply prefix_to_app in H3_.
        tidy_up.
        by_weakening.
  Qed.

  Fixpoint revealing_terminates G S G' U (H : revealing G S G' U) {struct H} :
    revealing_termination H.
  Proof.
    destruct H; constructor;
      repeat rewrite app_length;
      simpl; try lia.
    apply revealing_gives_prefix in H.
    apply prefix_to_app in H.
    tidy_up.
    rewrite app_length.
    lia.
  Qed.
  
  Hint Resolve ope_list_to_nil.

  Ltac destruct_env_by x G :=
    let H := fresh "H" in
    assert (H : x `in` dom G) by set solve;
    apply in_witness in H; destruct_conjs;
    subst G.
  
  Lemma revealing_strengthening : forall G T D U,
    revealing G T D U ->
    forall G',
      ope_list G' G ->
      wf_env G' -> wf_env G ->
      fv T [<=] dom G' ->
      exists D',
        revealing G' T D' U /\ ope_list D' D.
  Proof.
    induction on revealing; intros; eauto.
    - destruct_env_by x G'.
      pose proof (wf_uniq H4).
      pose proof (ope_list_witness_uniq H1 H9).
      destruct_conjs. subst H6.
      wf_env. clear H4. wf_env.
      clear_dups. saturate_fv.
      destruct (IHrevealing H8); set solve.
      destruct_conjs.
      apply revealing_gives_prefix in H3.
      apply prefix_wf_env in H3; trivial.
      eauto. 

    - destruct_env_by x G'.
      pose proof (wf_uniq H4).
      pose proof (ope_list_witness_uniq H2 H9).
      destruct_conjs. subst H6.
      wf_env. clear H4. wf_env.
      clear_dups. saturate_fv.
      destruct (IHrevealing1 H8); set solve.
      destruct_conjs.
      apply revealing_gives_prefix in H3_.
      apply prefix_wf_env in H3_; trivial.
      
      do 2 pose proof H17.
      apply revealing_gives_prefix in H17.
      apply prefix_wf_env in H17; trivial.
      apply revealing_preserves_fv in H22; set solve.
      destruct (IHrevealing2 x0); set solve.
      destruct_conjs.
      eauto. 
  Qed.

  Hint Resolve ope_list_trans ope_list_append1 ope_list_append2.
  
  Lemma upcast_e_to_upcast : forall G x A T,
      upcast_e G x A T ->
      wf_env G ->
      fv x [<=] dom G ->
      exists D, upcast G x A D T.
  Proof.
    destr on upcast_e; intros; eauto.
    - destruct_env_by x G.
      pose proof (wf_uniq H3).            
      pose proof H. apply binds_mid_eq in H; trivial.
      subst H5. pose proof H3. wf_env.
      
      saturate_fv. 
      apply exposure_to_revealing in H1; set solve.
      destruct_conjs.
      eapply revealing_strengthening with (G' := H7) in H17;
        set solve; eauto.
      destruct_conjs.
      eauto.

    - destruct_env_by x G.
      pose proof (wf_uniq H3).            
      pose proof H. apply binds_mid_eq in H; trivial.
      subst H5. pose proof H3. wf_env.
      
      saturate_fv. 
      apply exposure_to_revealing in H1; set solve.
      destruct_conjs.
      eapply revealing_strengthening with (G' := H7) in H17;
        set solve; eauto.
      destruct_conjs.
      eauto.
  Qed.
  
  Lemma downcast_e_to_downcast : forall G x A T,
      downcast_e G x A T ->
      wf_env G ->
      fv x [<=] dom G ->
      exists D, downcast G x A D T.
  Proof.
    destr on downcast_e; intros; eauto.
    - destruct_env_by x G.
      pose proof (wf_uniq H3).            
      pose proof H. apply binds_mid_eq in H; trivial.
      subst H5. pose proof H3. wf_env.
      
      saturate_fv. 
      apply exposure_to_revealing in H1; set solve.
      destruct_conjs.
      eapply revealing_strengthening with (G' := H7) in H17;
        set solve; eauto.
      destruct_conjs.
      eauto.

    - destruct_env_by x G.
      pose proof (wf_uniq H3).            
      pose proof H. apply binds_mid_eq in H; trivial.
      subst H5. pose proof H3. wf_env.
      
      saturate_fv. 
      apply exposure_to_revealing in H1; set solve.
      destruct_conjs.
      eapply revealing_strengthening with (G' := H7) in H17;
        set solve; eauto.
      destruct_conjs.
      eauto.
  Qed.

  Hint Constructors upcast_e downcast_e.
  
  Lemma upcast_to_upcast_e : forall G x A D T,
      upcast G x A D T ->
      upcast_e G x A T.
  Proof.
    destr on upcast; auto.
    - pose proof H0.
      apply revealing_gives_prefix in H2.
      apply prefix_to_app in H2.
      apply revealing_to_exposure in H0.
      tidy_up. 
      eapply uce_bot; simpl_env.
      + apply binds_for_sure.
      + eassumption.
      + do 2 apply exposure_weakening.
        eassumption.
      + eassumption.

    - pose proof H0.
      apply revealing_gives_prefix in H2.
      apply prefix_to_app in H2.
      apply revealing_to_exposure in H0.
      tidy_up.
      eapply uce_bnd; simpl_env.
      + apply binds_for_sure.
      + eassumption.
      + do 2 apply exposure_weakening.
        eassumption.
      + eassumption.
  Qed.
  
  Lemma downcast_to_downcast_e : forall G x A D T,
      downcast G x A D T ->
      downcast_e G x A T.
  Proof.
    destr on downcast; auto.
    - pose proof H0.
      apply revealing_gives_prefix in H2.
      apply prefix_to_app in H2.
      apply revealing_to_exposure in H0.
      tidy_up. 
      eapply dce_top; simpl_env.
      + apply binds_for_sure.
      + eassumption.
      + do 2 apply exposure_weakening.
        eassumption.
      + eassumption.

    - pose proof H0.
      apply revealing_gives_prefix in H2.
      apply prefix_to_app in H2.
      apply revealing_to_exposure in H0.
      tidy_up.
      eapply dce_bnd; simpl_env.
      + apply binds_for_sure.
      + eassumption.
      + do 2 apply exposure_weakening.
        eassumption.
      + eassumption.
  Qed.

  Lemma upcast_e_equiv_upcast : forall G x A T,
      wf_env G ->
      fv x [<=] dom G ->
      upcast_e G x A T <->
      exists D, upcast G x A D T.
  Proof.
    split; intros.
    - auto using upcast_e_to_upcast.
    - tidy_up. eauto using upcast_to_upcast_e.
  Qed.

  Lemma downcast_e_equiv_downcast : forall G x A T,
      wf_env G ->
      fv x [<=] dom G ->
      downcast_e G x A T <->
      exists D, downcast G x A D T.
  Proof.
    split; intros.
    - auto using downcast_e_to_downcast.
    - tidy_up. eauto using downcast_to_downcast_e.
  Qed.

  Lemma upcast_strengthening : forall G x A D T,
      upcast G x A D T ->
      forall G',
        ope_list G' G ->
        wf_env G' -> wf_env G ->
        fv x [<=] dom G' ->
        exists D', upcast G' x A D' T /\ ope_list D' D.
  Proof.
    destruct 1; intros; eauto.
    - exists nil. split; auto.
      destruct_env_by x G'.
      pose proof (wf_uniq H4).
      pose proof (ope_list_witness_uniq H2 H9).
      destruct_conjs. subst.

      wf_env. clear H4. wf_env.
      clear_dups. saturate_fv.
      pose proof H0.
      apply revealing_strengthening with (G' := H8) in H0;
        set solve.
      apply revealing_gives_prefix in H11.
      apply prefix_wf_env in H11; trivial.
      destruct_conjs.
      eauto.

    - destruct_env_by x G'.
      pose proof (wf_uniq H4).
      pose proof (ope_list_witness_uniq H2 H9).
      destruct_conjs. subst.

      wf_env. clear H4. wf_env.
      clear_dups. saturate_fv.
      pose proof H0.
      apply revealing_strengthening with (G' := H8) in H0;
        set solve.
      apply revealing_gives_prefix in H11.
      apply prefix_wf_env in H11; trivial.
      destruct_conjs.
      eauto.
  Qed.

  Lemma downcast_strengthening : forall G x A D T,
      downcast G x A D T ->
      forall G',
        ope_list G' G ->
        wf_env G' -> wf_env G ->
        fv x [<=] dom G' ->
        exists D', downcast G' x A D' T /\ ope_list D' D.
  Proof.
    destruct 1; intros; eauto.
    - exists nil. split; auto.
      destruct_env_by x G'.
      pose proof (wf_uniq H4).
      pose proof (ope_list_witness_uniq H2 H9).
      destruct_conjs. subst.

      wf_env. clear H4. wf_env.
      clear_dups. saturate_fv.
      pose proof H0.
      apply revealing_strengthening with (G' := H8) in H0;
        set solve.
      apply revealing_gives_prefix in H11.
      apply prefix_wf_env in H11; trivial.
      destruct_conjs.
      eauto.

    - destruct_env_by x G'.
      pose proof (wf_uniq H4).
      pose proof (ope_list_witness_uniq H2 H9).
      destruct_conjs. subst.

      wf_env. clear H4. wf_env.
      clear_dups. saturate_fv.
      pose proof H0.
      apply revealing_strengthening with (G' := H8) in H0;
        set solve.
      apply revealing_gives_prefix in H11.
      apply prefix_wf_env in H11; trivial.
      destruct_conjs.
      eauto.
  Qed.

  Theorem upcast_gives_prefix : forall G x A G' U,
      upcast G x A G' U ->
      prefix G' G.
  Proof.
    destr on upcast; intros; auto.
    apply revealing_gives_prefix in H0.
    eauto.
  Qed.      
  
  Theorem upcast_preserves_wf : forall G x A G' U,
      upcast G x A G' U ->
      wf_env G ->
      fv x [<=] dom G ->
      wf_env G' /\ fv U [<=] dom G' /\ lc U.
  Proof.
    destr on upcast; intros.
    1-2:simpl; repeat split; set solve.

    wf_env. saturate_fv. saturate_lc.
    rec_pose (revealing_preserves_fv H0) Hfv1; set solve.
    rec_pose (revealing_preserves_lc H0) Hlc1; auto.
    
    saturate_lc.
    apply revealing_gives_prefix in H0.
    apply prefix_wf_env in H0; trivial.
    repeat split; set solve.
  Qed.
  
  Theorem downcast_gives_prefix : forall G x A G' U,
      downcast G x A G' U ->
      prefix G' G.
  Proof.
    destr on downcast; intros; auto.
    apply revealing_gives_prefix in H0.
    eauto.
  Qed.

  Theorem downcast_preserves_wf : forall G x A G' U,
      downcast G x A G' U ->
      wf_env G ->
      fv x [<=] dom G ->
      wf_env G' /\ fv U [<=] dom G' /\ lc U.
  Proof.
    destr on downcast; intros.
    1-2:simpl; repeat split; set solve.

    wf_env. saturate_fv. saturate_lc.
    rec_pose (revealing_preserves_fv H0) Hfv1; set solve.
    rec_pose (revealing_preserves_lc H0) Hlc1; auto.
    
    saturate_lc.
    apply revealing_gives_prefix in H0.
    apply prefix_wf_env in H0; trivial.
    repeat split; set solve.
  Qed.

  Section AndInductivePrinciple.

    Variable P : typ -> Prop.
    
    Variable CaseNotAnd : forall T, ~is_and T -> P T.
    Variable CaseAnd : forall S U, P S -> P U -> P (typ_and S U).

    Lemma typ_and_ind : forall T, P T.
    Proof.
      induction T; routine.
    Qed.

  End AndInductivePrinciple.
  
  Hint Extern 1 (_ <= _) =>
  repeat match goal with
         | T : typ |- _ =>
           pose proof (typ_struct_measure_ge_1 T);
             fail_if_dup
         end;
    lia.
  
  Lemma bi_subty_and_refl_aux : forall U S L G1 G2,
      (forall T L G1 G2,
          typ_struct_measure T <= typ_struct_measure U ->
          [ L ] G1 >> T <⦂ T << G2) ->
      (forall T, and_trav U T -> and_trav S T) ->
      [ L ] G1 >> S <⦂ U << G2.
  Proof.
    induction U using typ_and_ind; intros.
    - specialize (H1 U ltac:(simpl; eauto)).
      simpl in *. tidy_up.
      induction H1; auto.
    - rec_pose (IHU1 S) Hrec1; eroutine.
      rec_pose (IHU2 S) Hrec2; eroutine.
  Qed.

  Program Fixpoint bi_subty_refl T {measure (typ_struct_measure T)} :
    forall L G1 G2,
      [ L ] G1 >> T <⦂ T << G2 := _.
  Next Obligation.
    destruct T; routine.
    - pick_fresh x. econstructor; auto.
      apply bi_subty_refl. rewrite open_typ_same_measure.
      lia.
    - apply bs_and_right;
        apply bi_subty_and_refl_aux; eroutine.
  Qed.                    

  Lemma bi_subty_and_is_set : forall U L G1 G2 S,
      (forall T, and_trav U T -> and_trav S T) ->
      [ L ] G1 >> S <⦂ U << G2.
  Proof.
    eauto using bi_subty_and_refl_aux, bi_subty_refl.
  Qed.
  
  Hint Constructors ope_list.
  Hint Resolve ope_list_refl.
  Hint Extern 2 (lc_at _ _) => progressive_inversions; assumption.
  Hint Extern 2 (lc_typ_at _ _) => progressive_inversions; assumption.

  Theorem bi_subty_strengthening : forall L G T U D,
      [ L ] G >> T <⦂ U << D ->
      forall G' D',
        ope_list G' G ->
        wf_env G -> wf_env G' ->
        fv T [<=] dom G' -> lc T ->
        ope_list D' D ->
        wf_env D -> wf_env D' ->
        fv U [<=] dom D' -> lc U ->
        [ L ] G' >> T <⦂ U << D'.
  Proof.
    induction on bi_subty; intros; eauto 1.
    - pose proof H.
      apply upcast_strengthening with (G' := G') in H; trivial.
      destruct_conjs.
      eapply bs_sel_left; eauto.
      simpl in H3.
      pose proof (upcast_preserves_wf H12 H2 H3).
      destruct_conjs.
      pose proof (ope_list_dom H0).
      pose proof (upcast_preserves_wf H11 H1 ltac:(fsetdec)).
      destruct_conjs.
      apply IHbi_subty; trivial.

    - pose proof H.
      apply downcast_strengthening with (G' := D') in H; trivial.
      destruct_conjs.
      eapply bs_sel_right; try eassumption.
      simpl in H9.
      pose proof (downcast_preserves_wf H12 H8 H9).
      destruct_conjs.
      pose proof (ope_list_dom H6).
      pose proof (downcast_preserves_wf H11 H7 ltac:(fsetdec)).
      destruct_conjs.
      apply IHbi_subty; trivial.
      
    - pose proof (ope_list_dom H0).
      pose proof (ope_list_dom H5).
      pose proof (ope_list_fv_env_subset H0).
      pose proof (ope_list_fv_env_subset H5).

      apply bs_all with (x := x).
      + fsetdec.
      + apply IHbi_subty1; trivial.
        all:set solve.
      + apply IHbi_subty2; simpl in *;
          lazymatch goal with
          | |- ope_list _ _ => auto
          | |- lc_typ_at _ _ =>
            apply open_lc_typ; auto
          | |- wf_env _ =>
            constructor; trivial;
              [ simpl in *; fsetdec | simpl in *; fsetdec | auto ]
          | _ => idtac
          end.
        * pose proof (fv_open_typ U1 x 0).
          simpl in *.
          etransitivity; [ eassumption |].
          clear H. fsetdec.
        * pose proof (fv_open_typ U2 x 0).
          simpl in *.
          etransitivity; [ eassumption |].
          clear H. fsetdec.

    - progressive_inversions. simpl in *; fold_cls. 
      constructor.
      + apply IHbi_subty1; set solve.
      + apply IHbi_subty2; set solve.
    - progressive_inversions. simpl in *; fold_cls. 
      constructor.
      apply IHbi_subty; set solve.
      
    - apply bs_and_left1; trivial.
      apply IHbi_subty; set solve.
    - apply bs_and_left2; trivial.
      apply IHbi_subty; set solve.
    - apply bs_and_right; trivial.
      + apply IHbi_subty1; set solve.
      + apply IHbi_subty2; set solve.
  Qed.

End RevealingProperties.

Section Soundness.

  Hint Resolve ope_sub_refl ope_sub_nil ope_sub_trans ope_sub_app_r binds_for_sure.
  Hint Constructors ope_sub.

  Theorem upcast_sound : forall G x A G' U,
    upcast G x A G' U ->
    G ⊢ typ_sel x A <⦂ U.
  Proof.
    destr on upcast.
    all:try lazymatch goal with
            | |- subty (?l2 ++ ?x ~ ?p ++ ?l1) _ _ /\ _ =>
              pose proof (binds_for_sure l2 l1 x p)
            end.
    all:routine.
    - apply revealing_sound in H0. destruct_conjs.
      eapply st_sel2; eauto.
      eapply st_trans; [apply and_trav_subty; eauto |].
      eapply st_trans.
      + by_weakening.
      + eapply st_trans; [apply and_trav_subty; eauto |].
        instantiate (1 := typ_top). trivial.
    - apply revealing_sound in H0. destruct_conjs.
      eapply st_sel2; eauto.
      eapply st_trans; [apply and_trav_subty; eauto |].
      eapply st_trans.
      + by_weakening.
      + apply and_trav_subty; eauto.
  Qed.


  Theorem downcast_sound : forall G x A G' U,
    downcast G x A G' U ->
    G ⊢ U <⦂ typ_sel x A.
  Proof.
    destr on downcast.
    all:try lazymatch goal with
            | |- subty (?l2 ++ ?x ~ ?p ++ ?l1) _ _ /\ _ =>
              pose proof (binds_for_sure l2 l1 x p)
            end.
    all:routine.
    - apply revealing_sound in H0. destruct_conjs.
      eapply st_sel1; eauto.
      eapply st_trans; [apply and_trav_subty; eauto |].
      eapply st_trans.
      + by_weakening.
      + eapply st_trans; [apply and_trav_subty; eauto |].
        instantiate (1 := typ_bot). trivial.
    - apply revealing_sound in H0. destruct_conjs.
      eapply st_sel1; eauto.
      eapply st_trans; [apply and_trav_subty; eauto |].
      eapply st_trans.
      + by_weakening.
      + apply and_trav_subty; eauto.
  Qed.

  Hint Resolve ope_sub_trans.
  Hint Resolve prefix_to_ope_sub.
  
  Theorem ope_sub_bi_subty_sound : forall L G1 T U G2,
    [ L ] G1 >> T <⦂ U << G2 ->
    forall G,
      fv G [<=] L ->
      uniq G ->
      ope_sub G1 G ->
      ope_sub G2 G ->
      G ⊢ T <⦂ U.
  Proof.
    induction on bi_subty; intros; eauto 4.
    - pose proof (upcast_gives_prefix H).
      apply upcast_sound in H. destruct_conjs.
      eapply st_trans.
      + eapply ope_narrow_subty; eassumption.
      + apply IHbi_subty; eauto.
    - pose proof (downcast_gives_prefix H).
      apply downcast_sound in H. destruct_conjs.
      eapply st_trans.
      + apply IHbi_subty; eauto.
      + eapply ope_narrow_subty; eassumption.

    - eapply st_all; auto.
      assert (x `notin` fv G). {
        eapply notin_subset_relax. eassumption. auto.
      }
      cofinite. apply open_subst_subty with (x := x); trivial.
      + repeat (apply notin_union_3; auto).
      + auto.
      + apply IHbi_subty2.
        * assert (fv (x ~ T2 ++ G) [=] singleton x `union` fv G `union` fv T2). {
             simpl. set solve.
           }
           rewrite H5. set solve.
        * routine.
        * apply os_keep; auto.
        * apply os_keep; auto.
  Qed.

End Soundness.
  
Definition subty_measure (G : env) (T : typ) : nat :=
  env_measure G + typ_struct_measure T.
Arguments subty_measure G T/.

Section Termination.

  Lemma and_trav_typ_measure : forall S U,
      and_trav S U ->
      typ_struct_measure S >= typ_struct_measure U.
  Proof.
    induction on and_trav; simpl; lia.
  Qed.

  Local Ltac and_trav_measure :=
    repeat match goal with
           | H : and_trav _ _ |- _ =>
             apply and_trav_typ_measure in H
           end.
  
  Theorem revealing_measure : forall G T G' T',
      revealing G T G' T' ->
      subty_measure G T >= subty_measure G' T'.
  Proof.
    induction on revealing; simpl; try lia.
    - pose proof (typ_struct_measure_ge_1 T).
      lia.
    - and_trav_measure.
      simpl in *. autorewrite with measures.
      lia.
  Qed.
  
  Local Ltac revealing_measure :=
    repeat match goal with
           | H : revealing _ _ _ _ |- _ =>
             apply revealing_measure in H
           end.
  
  Theorem upcast_decreases_measure : forall G x A G' U,
      upcast G x A G' U ->
      subty_measure G (typ_sel x A) > subty_measure G' U.
  Proof.
    destr on upcast; simpl;
      autorewrite with measures; try lia.

    and_trav_measure.
    revealing_measure.
    simpl in *. lia.
  Qed.

  Theorem downcast_decreases_measure : forall G x A G' U,
      downcast G x A G' U ->
      subty_measure G (typ_sel x A) > subty_measure G' U.
  Proof.
    destr on downcast; simpl;
      autorewrite with measures; try lia.

    and_trav_measure.
    revealing_measure.
    simpl in *. lia.
  Qed.

  Inductive bi_subty_termination : forall L G1 S U G2, [ L ] G1 >> S <⦂ U << G2 -> Prop :=
  | bt_bot : forall L G1 T G2, bi_subty_termination (bs_bot L G1 T G2)
  | bt_top : forall L G1 T G2, bi_subty_termination (bs_top L G1 T G2)
  | bt_sel_refl : forall L G1 x G2 A, bi_subty_termination (bs_sel_refl L G1 x G2 A)
  | bt_sel_left : forall L G1 x A G2 T U G1'
                    (Uc : upcast G1 x A G1' T)
                    (Rec : [ L ] G1' >> T <⦂ U << G2),
      subty_measure G1' T + subty_measure G2 U <
      subty_measure G1 (typ_sel x A) + subty_measure G2 U ->
      bi_subty_termination (bs_sel_left Uc Rec)
  | bt_sel_right : forall L G1 x A G2 T U G2'
                     (Dc : downcast G2 x A G2' U)
                     (Rec : [ L ] G1 >> T <⦂ U << G2'),
      subty_measure G1 T + subty_measure G2' U <
      subty_measure G1 T + subty_measure G2 (typ_sel x A) ->
      bi_subty_termination (bs_sel_right Dc Rec)

  | bt_all : forall L G1 T1 U1 G2 T2 U2 x
               (F : x `notin` fv G1 `union` fv T1 `union` fv T2
                      `union` fv U1 `union` fv U2 `union` fv G2 `union` L)
               (Rec1 : [ L ] G2 >> T2 <⦂ T1 << G1)
               (Rec2 : [ L  `union` singleton x `union` fv T2 ]
                         x ~ T1 ++ G1 >> open x U1 <⦂ open x U2 << x ~ T2 ++ G2),
      subty_measure G2 T2 + subty_measure G1 T1 <
      subty_measure G1 (typ_all T1 U1) + subty_measure G2 (typ_all T2 U2) ->
      subty_measure (x ~ T1 ++ G1) (open x U1) + subty_measure (x ~ T2 ++ G2) (open x U2) <
      subty_measure G1 (typ_all T1 U1) + subty_measure G2 (typ_all T2 U2) ->
      bi_subty_termination (bs_all U1 U2 F Rec1 Rec2)
  | bt_bnd : forall L G1 A S1 U1 S2 U2 G2
               (Rec1 : [ L ] G2 >> S2 <⦂ S1 << G1)
               (Rec2 : [ L ] G1 >> U1 <⦂ U2 << G2),
      subty_measure G2 S2 + subty_measure G1 S1 <
      subty_measure G1 (typ_bnd A S1 U1) + subty_measure G2 (typ_bnd A S2 U2) ->
      subty_measure G1 U1 + subty_measure G2 U2 <
      subty_measure G1 (typ_bnd A S1 U1) + subty_measure G2 (typ_bnd A S2 U2) ->
      bi_subty_termination (bs_bnd A Rec1 Rec2)
  | bt_fld : forall L G1 a T T' G2
               (Rec : [ L ] G1 >> T <⦂ T' << G2),
      subty_measure G1 T + subty_measure G2 T' <
      subty_measure G1 (typ_fld a T) + subty_measure G2 (typ_fld a T') ->
      bi_subty_termination (bs_fld a Rec)
  | bt_and_left1 : forall L G1 G2 S U T
                     (NA : ~is_and T)
                     (Rec : [ L ] G1 >> S <⦂ T << G2),
      subty_measure G1 S + subty_measure G2 T <
      subty_measure G1 (typ_and S U) + subty_measure G2 T ->
      bi_subty_termination (bs_and_left1 U NA Rec)
  | bt_and_left2 : forall L G1 G2 S U T
                     (NA : ~is_and T)
                     (Rec : [ L ] G1 >> U <⦂ T << G2),
      subty_measure G1 U + subty_measure G2 T <
      subty_measure G1 (typ_and S U) + subty_measure G2 T ->
      bi_subty_termination (bs_and_left2 S NA Rec)
  | bt_and_right : forall L G1 G2 T S U
                     (Rec1 : [ L ] G1 >> T <⦂ S << G2)
                     (Rec2 : [ L ] G1 >> T <⦂ U << G2),
      subty_measure G1 T + subty_measure G2 S <
      subty_measure G1 T + subty_measure G2 (typ_and S U) ->
      subty_measure G1 T + subty_measure G2 U <
      subty_measure G1 T + subty_measure G2 (typ_and S U) ->
      bi_subty_termination (bs_and_right Rec1 Rec2).
      
  Local Hint Constructors bi_subty_termination.

  Hint Rewrite -> open_typ_same_measure : measures.
  
  Fixpoint bi_subty_terminates L G1 S U G2
           (D : [ L ] G1 >> S <⦂ U << G2) {struct D} :
    bi_subty_termination D.
  Proof.
    destruct D; constructor;
      lazymatch goal with
      | H : upcast _ _ _ _ _ |- _ =>
        apply upcast_decreases_measure in H
      | H : downcast _ _ _ _ _ |- _ =>
        apply downcast_decreases_measure in H
      | _ => idtac
      end;
      simpl in *; auto; try lia;
    autorewrite with measures.
    lia.
  Qed.

End Termination.
