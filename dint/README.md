## Algorithmic Analysis of D/\


This directory focuses on algorithmic analysis of D/\. It defines:

1. The declarative D/\,
1. Strong kernel D/\, and
1. Stare-at subtyping.

The naming convention in this directory is locally nameless representation with
cofinite quantification.

### Definitions

| Definition | File           | Correspondences                   | Comments |
|------------|----------------|-----------------------------------|----------|
| 5.1        | Definitions.v  | `subty`                           |          |
| 5.2        | StrongKernel.v | `subtysk`                         |          |
| 5.3        | Exposure.v     | `and_trav`                        |          |
| 5.4        | Revealing.v    | `revealing`, `upcast`, `downcast` |          |
| 5.5        | Revealing.v    | `bi_subty`                        |          |
| 5.6        | Misc.v         | `typ_struct_measure`              |          |

### Conclusions

| Conclusion       | File                      | Correspondences                                                                                       |
|------------------|---------------------------|-------------------------------------------------------------------------------------------------------|
| Lemma 5.3        | StrongKernel.v            | `subtysk_refl`                                                                                        |
| Lemma 5.4        | StrongKernel.v            | `sk_trans_on_top`                                                                                     |
| Lemma 5.5        | StrongKernel.v            | `sk_trans_on_bot`                                                                                     |
| Theorem 5.6      | StrongKernel.v            | `subtysk_sound_gen`                                                                                   |
| Theorem 5.7      | StrongKernel.v            | `subtysk_sound`                                                                                       |
| Lemma 5.8        | Revealing.v               | `and_trav_typ_measure`, `revealing_measure`, `upcast_decreases_measure`, `downcast_decreases_measure` |
| Theorem 5.9      | Revealing.v               | `bi_subty_terminates`                                                                                 |
| Theroem 5.10     | Revealing.v               | `bi_subty_refl`                                                                                       |
| Theorem 5.11     | Revealing.v               | `bi_subty_and_refl_aux`                                                                               |
| Lemma 5.12       | Revealing.v               | `revealing_gives_prefix`                                                                              |
| Lemma 5.13, 5.15 | Revealing.v               | `revealing_sound`                                                                                     |
| Lemma 5.14       | Revealing.v               | `revealing_terminates`                                                                                |
| Lemma 5.16       | Revealing.v               | `upcast_gives_prefix`, `downcast_gives_prefix`, `upcast_sound`, `downcast_sound`                      |
| Lemma 5.17, 5.18 | Revealing.v               | `ope_sub_bi_subty_sound`                                                                              |
| Lemma 5.19       | StrongKernel.v            | `and_trav_to_subtysk`                                                                                 |
| Lemma 5.20       | StrongKernel.v            | `exposure_to_subtysk`                                                                                 |
|                  | Revealing.v               | `revealing_to_exposure`                                                                               |
| Theorem 5.21     | RevealingToStrongKernel.v | `bi_subty_to_subtysk`                                                                                 |
| Theorem 5.22     | RevealingToStrongKernel.v | `subtysk_to_bi_subty`                                                                                 |

