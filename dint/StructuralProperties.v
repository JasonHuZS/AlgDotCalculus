Set Implicit Arguments.
Require Import Definitions.
Require Import OperationProperties.

Section StructuralProperties.

  Hint Resolve binds_weaken.
  
  Lemma weaken_subty_gen G S U: forall G1 G2,
      G1 ++ G2 ⊢ S <⦂ U -> G1 ++ G ++ G2 ⊢ S <⦂ U.
  Proof.
    intros. generalize dependent G.
    dependent induction H; intros; trivial; eauto 4.

    eapply st_all; eroutine at 4.
    reassoc 4 with 2. apply H1; routine.
  Qed.

  Definition weaken_subty G S U: forall G',
      G ⊢ S <⦂ U -> G' ++ G ⊢ S <⦂ U.
  Proof.
    intros. reassoc 2 with 0.
    apply weaken_subty_gen; simpl; trivial.
  Qed.
  
  Hint Resolve subst_open_comm_typ.
 
  Definition subst_env (z u: var) (G : env) : env := map (substi z u) G.

  Global Instance SubstiEnv : CanSubst env := { substi := subst_env }.

  Lemma free_all : forall x y T (G : env),
      y `notin` fv G ->
      binds x T G ->
      y `notin` fv T.
  Proof. induction on env; routine. Qed.

  Ltac fold_subst_fvar :=
    repeat match goal with
           | |- context[if ?x == ?y then ?z else ?x] =>
             change (if x == y then z else x) with (subst_fvar y z x)
           end.

  Ltac simp := simpl; fold_cls; fold_subst_fvar.

  Ltac fold_env :=
    lazymatch goal with
      |- context[?v ~ substi ?x ?y ?T ++ substi ?x ?y ?G] =>
      change (v ~ substi x y T ++ substi x y G)
    with (substi x y (v ~ T ++ G))
    end.
  
  Lemma binds_subst : forall G1 x y T G2 x0 T0,
      binds x0 T0 (G1 ++ x ~ T ++ G2) ->
      uniq (G1 ++ x ~ T ++ G2) ->
      x `notin` fv T `union` fv G2 ->
      y `notin` fv T `union` fv G2 `union` singleton x ->
      binds (subst_fvar x y x0) (substi x y T0) (substi x y G1 ++ y ~ T ++ G2).
  Proof.
    intros. destruct_binds_hyp_uniq H; tidy_up; fold_cls.
    - apply binds_app_2.
      apply binds_map_2. trivial.
    - apply binds_app_3. 
      rewrite subst_fresh_typ; auto.
    - rewrite subst_fresh_typ; auto.
      eapply free_all; try eassumption.
      simpl. solve_notin.
  Qed.

  Hint Resolve binds_subst.
  
  Lemma open_subst_subty_gen : forall x T S U G1 G2,
      G1 ++ x ~ T ++ G2 ⊢ S <⦂ U ->
      uniq (G1 ++ x ~ T ++ G2) ->
      x `notin` fv T `union` fv G2 ->
      forall y,
        y `notin` fv T `union` fv G2 `union` singleton x ->
        substi x y G1 ++ y ~ T ++ G2 ⊢ substi x y S <⦂ substi x y U.
  Proof.
    intros. gen y. dependent induction H; intros; auto.
    1:eauto.
    all:simp; eauto 3.
    - eapply st_bnd.
      + apply IHsubty1; trivial.
      + apply IHsubty2; trivial.
    - eapply st_fld.
      apply IHsubty; trivial.
    - eapply st_all.
      + apply IHsubty; auto.
      + cofinite.
        fold_substi; open_substi_transform.
        reassoc 4 with 2; fold_env.
        eapply H1; routine.
    - eapply st_sel1;
        [apply binds_subst | apply IHsubty]; auto.
    - eapply st_sel2;
        [apply binds_subst | apply IHsubty]; auto.
    - eapply st_and_i.
      + apply IHsubty1; trivial.
      + apply IHsubty2; trivial.
  Qed.      
      
  Lemma open_subst_subty : forall x S G U1 U2 y,
      uniq G ->
      x `notin` fv G `union` fv S `union` fv U1 `union` fv U2 ->
      y `notin` fv G `union` fv S `union` fv U1 `union` fv U2 `union` singleton x ->
      x ~ S ++ G ⊢ open x U1 <⦂ open x U2 ->
      y ~ S ++ G ⊢ open y U1 <⦂ open y U2.
  Proof.
    intros. reassoc 2 with 0.
    rewrite subst_intro_typ with (x := x) (t := U1) by auto.
    rewrite subst_intro_typ with (x := x) (t := U2) by auto.
    eapply open_subst_subty_gen with (G1 := nil); routine.
  Qed.
  
  Reserved Notation "G1 ⪯ G2" (at level 70).

  Inductive subenv: env -> env -> Prop :=
  | subenv_empty : nil ⪯ nil
  | subenv_grow: forall G G' x T T',
      G ⪯ G' ->
      uniq ((x, T) :: G) ->
      uniq ((x, T') :: G') ->
      G ⊢ T <⦂ T' ->
      (x, T) :: G ⪯ (x, T') :: G'
  where "G1 ⪯ G2" := (subenv G1 G2).
  Hint Constructors subenv.

  Lemma subenv_refl : forall G, uniq G -> G ⪯ G.
  Proof. induction G; eroutine. Qed.
  Local Hint Resolve subenv_refl.

  Lemma subenv_push : forall G1 G2 x T,
      G1 ⪯ G2 ->
      uniq ((x, T) :: G1) ->
      uniq ((x, T) :: G2) ->
      (x, T) :: G1 ⪯ (x, T) :: G2.
  Proof. induction G1; eroutine. Qed.
  Local Hint Resolve subenv_push.

  Local Hint Extern 1 =>
  match goal with
  | [ H : uniq _ |- _ ] => inversion H
  end.

  Local Hint Extern 1 =>
  match goal with
  | [ H : _ ⪯ _ |- _ ] => inversion H
  end.
  
  Lemma subenv_last: forall G x S U,
      G ⊢ S <⦂ U ->
      uniq ((x, S) :: G) ->
      (x, S) :: G ⪯ (x, U) :: G.
  Proof. routine. Qed.
  Hint Resolve subenv_last.

  Lemma subenv_implies_uniq : forall G1 G2,
      G1 ⪯ G2 -> uniq G1 /\ uniq G2.
  Proof. routine. Qed.

  Local Hint Extern 1 =>
  match goal with
  | [ |- _ ⊢ _ <⦂ _ ] => idtac
  end; reassoc 2 with 0.
  
  Local Hint Extern 1 => eapply weaken_subty.

  Lemma narrow_binds : forall G G',
      G' ⪯ G -> 
      forall x T,
        binds x T G ->
        exists T', binds x T' G' /\ (G' ⊢ T' <⦂ T).
  Proof.
    induction on subenv; routine.
    - eexists; split; eauto.
    - apply IHsubenv in H3.
      tidy_up.
      eexists; split; eauto.
  Qed.
  
  Local Ltac uniq_env :=
    match goal with
    | [ H : _ ⪯ _ |- _ ] =>
      let H0 := fresh "H" in
      pose proof (subenv_implies_uniq H) as H0;
      destruct H0
    end.

  Local Ltac solve_by_cofinite H :=
    cofinite; apply H;
    [ solve_notin |
      constructor; trivial;
      constructor; trivial; solve_notin].
  
  Lemma narrow_subty_gen : forall G S U,
      G ⊢ S <⦂ U ->
      forall G',
        G' ⪯ G ->
        G' ⊢ S <⦂ U.
  Proof.
      induction on subty; routine; uniq_env.
      + eapply st_trans;
          [apply IHsubty1 | apply IHsubty2]; trivial.
      + eapply st_all;
          [routine | solve_by_cofinite H0].
      + pose proof (narrow_binds H0 _ _ H).
        tidy_up.
        eapply st_sel1; try eassumption.
        eapply st_trans; try eassumption.
        eauto.
      + pose proof (narrow_binds H0 _ _ H).
        tidy_up.
        eapply st_sel2; try eassumption.
        eapply st_trans; try eassumption.
        eauto.
  Qed.
  
  Lemma narrow_subty : forall x S1 S2 G U1 U2,
      x ~ S2 ++ G ⊢ U1 <⦂ U2 ->
      x `notin` dom G ->
      uniq G ->
      G ⊢ S1 <⦂ S2 ->
      x ~ S1 ++ G ⊢ U1 <⦂ U2.
  Proof.
    intros. eapply narrow_subty_gen.
    apply H.
    constructor; trivial.
    auto.
    all:solve_uniq.
  Qed.
  
End StructuralProperties.

Hint Resolve subenv_refl subenv_push subenv_last.
