Set Implicit Arguments.
Require Import Definitions.
Require Import Misc.
Require Import Exposure.
Require Import Revealing.
Require Import StrongKernel.
Require Import OperationProperties.
Require Import ListRelations.

Local Hint Constructors bi_subty' upcast_e downcast_e.
Local Remove Hints ex_top : core.
Local Hint Resolve revealing_to_exposure exposure_weakening.

Lemma revealing_fv_bound : forall G S G' U,
    revealing G S G' U ->
    fv U [<=] fv G `union` fv S.
Proof.
  induction on revealing; set solve.

  apply revealing_gives_prefix in H3_.
  apply prefix_to_app in H3_.
  apply revealing_gives_prefix in H3_0.
  apply prefix_to_app in H3_0.
  destruct_conjs. subst.
  repeat rewrite fv_union in *.
  saturate_fv. 
  simpl in *. fsetdec.
Qed.

Lemma bi_subty_to_bi_subty' : forall L' G1 S U G2,
    [ L' ] G1 >> S <⦂ U << G2 ->
    forall L,
      L `union` fv G1 `union` fv G2 `union` fv S `union` fv U [<=] L' ->
    [ L ] G1 >> S <⦂p U << G2.
Proof.
  induction on bi_subty; intros; eauto.
  - destruct H; eauto.
    + econstructor; eauto.
      rewrite <- (app_nil_2 _ G).
      apply bi_subty'_weakening_l.
      apply IHbi_subty.
      change (fv nil) with (union empty empty).
      fsetdec.
      
    + econstructor.
      * eapply uce_bot.
        -- eauto.
        -- eassumption.
        -- eauto.
        -- eassumption.
      * rewrite <- (app_nil_2 _ G1).
        repeat apply bi_subty'_weakening_l.
        apply IHbi_subty.
        repeat rewrite fv_union in *.
        change (fv nil) with (union empty empty).
        fsetdec.

    + econstructor.
      * eapply uce_bnd.
        -- eauto.
        -- eassumption.
        -- eauto.
        -- eassumption.
      * pose proof (revealing_gives_prefix H1).
        apply prefix_to_app in H3.
        pose proof (revealing_gives_prefix H1).
        apply prefix_to_app in H5.
        destruct_conjs. subst. 
        repeat apply bi_subty'_weakening_l.
        apply IHbi_subty.
        apply revealing_fv_bound in H1.
        repeat rewrite fv_union in *.
        saturate_fv.
        assert (fv U0 [<=] L). {
          simpl in *. fsetdec.
        }
        fsetdec.

  - destruct H; eauto.
    + econstructor; eauto.
      rewrite <- (app_nil_2 _ G).
      apply bi_subty'_weakening_r.
      apply IHbi_subty.
      change (fv nil) with (union empty empty).
      fsetdec.
      
    + econstructor.
      * eapply dce_top.
        -- eauto.
        -- eassumption.
        -- eauto.
        -- eassumption.
      * rewrite <- (app_nil_2 _ G0).
        repeat apply bi_subty'_weakening_r.
        apply IHbi_subty.
        repeat rewrite fv_union in *.
        change (fv nil) with (union empty empty).
        fsetdec.

    + econstructor.
      * eapply dce_bnd.
        -- eauto.
        -- eassumption.
        -- eauto.
        -- eassumption.
      * pose proof (revealing_gives_prefix H1).
        apply prefix_to_app in H3.
        pose proof (revealing_gives_prefix H1).
        apply prefix_to_app in H5.
        destruct_conjs. subst. 
        repeat apply bi_subty'_weakening_r.
        apply IHbi_subty.
        apply revealing_fv_bound in H1.
        repeat rewrite fv_union in *.
        saturate_fv.
        assert (fv L1 [<=] L). {
          simpl in *. fsetdec.
        }
        fsetdec.

  - apply bs'_all with x.
    + fsetdec.
    + apply IHbi_subty1. clear H.
      simpl in *. fsetdec.
    + apply IHbi_subty2. clear H.
      repeat rewrite fv_union.
      pose proof (fv_open_typ U1 x 0).
      pose proof (fv_open_typ U2 x 0).
      simpl in *. fsetdec.
  - constructor.
    + apply IHbi_subty1. simpl in *.
      fsetdec.
    + apply IHbi_subty2. simpl in *.
      fsetdec.

  - apply bs'_and_left1; trivial.
    apply IHbi_subty. simpl in *.
    fsetdec.
  - apply bs'_and_left2; trivial.
    apply IHbi_subty. simpl in *.
    fsetdec.
  - apply bs'_and_right.
    + apply IHbi_subty1. simpl in *.
      fsetdec.
    + apply IHbi_subty2. simpl in *.
      fsetdec.      
Qed.    

Local Hint Constructors lc_typ_at.
Local Hint Resolve ope_list_trans ope_list_refl ope_list_to_nil ope_list_append2.

Lemma bi_subty'_to_bi_subty : forall L G1 S U G2,
    [ L ] G1 >> S <⦂p U << G2 ->
    wf_env G1 -> wf_env G2 ->
    fv S [<=] dom G1 -> lc S ->
    fv U [<=] dom G2 -> lc U ->
    [ L ] G1 >> S <⦂ U << G2.
Proof.
  induction on bi_subty'; intros; try solve [routine].
  - destruct H.
    + econstructor.
      * apply uc_top.
      * rec_pose IHbi_subty' H; set solve.
        eapply bi_subty_strengthening with (G := G);
          eroutine.
    + pose proof H. 
      apply binds_app in H; destruct_conjs; subst.
      pose proof H0. wf_env.
      saturate_fv.
      do 2 (apply exposure_strengthening in H8; set solve).
      apply exposure_to_revealing in H8; set solve.
      destruct_conjs. 
      eapply bs_sel_left; eauto.
      eapply uc_bot; eassumption.

    + pose proof H. 
      apply binds_app in H; destruct_conjs; subst.
      pose proof H0. wf_env.
      saturate_fv.
      do 2 (apply exposure_strengthening in H8; set solve).
      apply exposure_to_revealing in H8; set solve.
      destruct_conjs.
      
      do 2 pose proof H21.
      apply revealing_gives_prefix in H21.
      apply prefix_to_app in H21.
      apply revealing_preserves_fv in H22; set solve.
      destruct_conjs. subst.
      pose proof H18. apply wf_deapp in H18.

      eapply bs_sel_left.
      * eapply uc_bnd; eassumption.
      * simpl in H14. fold_fv.
        saturate_lc.
        pose proof H23.
        apply revealing_preserves_lc in H23; trivial.
        saturate_lc.
        apply revealing_preserves_lc in H25; trivial.
        rec_pose IHbi_subty' Hrec; set solve.

        eapply bi_subty_strengthening; eauto.

  - destruct H.
    + econstructor.
      * apply dc_bot.
      * rec_pose IHbi_subty' H; set solve.
        eapply bi_subty_strengthening with (D := G);
          eroutine.
    + pose proof H. 
      apply binds_app in H; destruct_conjs; subst.
      pose proof H1. wf_env.
      saturate_fv.
      do 2 (apply exposure_strengthening in H8; set solve).
      apply exposure_to_revealing in H8; set solve.
      destruct_conjs. 
      eapply bs_sel_right; eauto.
      eapply dc_top; eassumption.

    + pose proof H. 
      apply binds_app in H; destruct_conjs; subst.
      pose proof H1. wf_env.
      saturate_fv.
      do 2 (apply exposure_strengthening in H8; set solve).
      apply exposure_to_revealing in H8; set solve.
      destruct_conjs.
      
      do 2 pose proof H21.
      apply revealing_gives_prefix in H21.
      apply prefix_to_app in H21.
      apply revealing_preserves_fv in H22; set solve.
      destruct_conjs. subst.
      pose proof H18. apply wf_deapp in H18.

      eapply bs_sel_right.
      * eapply dc_bnd; eassumption.
      * simpl in H14. fold_fv.
        saturate_lc.
        pose proof H23.
        apply revealing_preserves_lc in H23; trivial.
        saturate_lc.
        apply revealing_preserves_lc in H25; trivial.
        rec_pose IHbi_subty' Hrec; set solve.

        eapply bi_subty_strengthening; eauto.

  - eapply bs_all with x; simpl in *; progressive_inversions; fold_cls.
    + set solve.
    + auto.
    + apply IHbi_subty'2.
      all:auto using open_lc_typ.
      all:try constructor; auto.
      * pose proof (fv_open_typ U1 x 0).
        etransitivity; [ eassumption |].
        set solve.
      * pose proof (fv_open_typ U2 x 0).
        etransitivity; [ eassumption |].
        set solve.
Qed.

Lemma bi_subty_to_subtysk : forall L G1 S U G2,
    [ L `union` fv G1 `union` fv G2 `union` fv S `union` fv U ]
      G1 >> S <⦂ U << G2 ->
    [ L ] G1 ⊢k S <⦂ U k⊣ G2.
Proof.
  intros.
  eauto using bi_subty'_to_subtysk, bi_subty_to_bi_subty'.
Qed.

Theorem subtysk_to_bi_subty : forall L G1 S U G2,
    [ L ] G1 ⊢k S <⦂ U k⊣ G2 ->
    wf_env G1 -> wf_env G2 ->
    fv S [<=] dom G1 -> lc S ->
    fv U [<=] dom G2 -> lc U ->
    [ L ] G1 >> S <⦂ U << G2.
Proof.
  intros.
  auto using subtysk_to_bi_subty', bi_subty'_to_bi_subty.
Qed.

Lemma bi_subty_cong_names : forall L G1 S U G2,
    [ L ] G1 >> S <⦂ U << G2 ->
    forall L',
      L [=] L' ->
      [ L' ] G1 >> S <⦂ U << G2.
Proof.
  induction on bi_subty; intros; eauto.
  eapply bs_all with x; auto.
  - fsetdec.
  - apply IHbi_subty2.
    clear H. fsetdec.
Qed.

Theorem subtysk_equiv_bi_subty : forall L G1 S U G2,
    wf_env G1 -> wf_env G2 ->
    fv S [<=] dom G1 -> lc S ->
    fv U [<=] dom G2 -> lc U ->
    [ L `union` fv G1 `union` fv G2 `union` fv S `union` fv U ]
      G1 ⊢k S <⦂ U k⊣ G2 <->
    [ L `union` fv G1 `union` fv G2 `union` fv S `union` fv U ]
      G1 >> S <⦂ U << G2.
Proof.
  split; intros.
  - auto using subtysk_to_bi_subty.
  - apply bi_subty_to_subtysk; trivial.
    eapply bi_subty_cong_names; eauto.
    fsetdec.
Qed.
