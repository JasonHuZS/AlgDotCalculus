DEPS::=lib/metalib/Metalib lib/coq-ext-lib
ALL_DEPS::=${DEPS} lib/Coq-Equations
PROJECTS::=dsub dart mu-dart

.PHONY: clean deps

${PROJECTS}: ${ALL_DEPS} share
	cd $@ && ${MAKE}

.submodules-pulled:
	git submodule init
	git submodule update
	touch .submodules-pulled

share:${DEPS} lib/Coq-Equations

lib/Coq-Equations: .submodules-pulled
	cd $@ && coq_makefile -f _CoqProject -o Makefile && ${MAKE}

${DEPS} share: .submodules-pulled
	cd $@ && ${MAKE}

deps: ${ALL_DEPS}

CWD=$(shell pwd)
clean:
	for d in ${ALL_DEPS}; do cd ${CWD}; cd "$$d" && ${MAKE} clean; done
