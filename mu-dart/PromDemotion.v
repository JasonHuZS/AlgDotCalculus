Set Implicit Arguments.
Require Import Definitions.
Require Import TypeCheckSpecs.
Require Import TypingHelpers.
Require Import Helpers.
Require Import Measures.
Require Import ObjectTraversal.
Require Import SubstitutionLaws.
Require Import OperationProperties.
Require Import Weakening.
Require Import SubtypingMeasures.
Require Import Exposure.

Require Import ExtLib.Structures.Functor.

Section PromDemotion.
  
  Inductive mode : Set := pro | de.

  Definition oppode (m : mode) : mode :=
    match m with
    | pro => de
    | de => pro
    end.

  Definition weak_case (m : mode) :=
    match m with
    | pro => typ_top
    | de => typ_bot
    end.
  
  Inductive prode_obj : mode -> decs -> list typ_label -> var -> typ -> typ -> Prop :=
  | pdo_top : forall m DS l x, prode_obj m DS l x typ_top typ_top
  | po_top : forall DS l x T, prode_obj pro DS l x T typ_top
  | pdo_bot : forall m DS l x, prode_obj m DS l x typ_bot typ_bot
  | do_bot : forall DS l x T, prode_obj de DS l x T typ_bot
  | pdo_seln : forall m DS l x n A,
      prode_obj m DS l x (typ_sel (avar_b n) A) (typ_sel (avar_b n) A)  
  | pdo_sel_neq : forall m DS l (x y : var) A,
      x <> y ->
      prode_obj m DS l x (typ_sel y A) (typ_sel y A)

  | po_sel : forall (DS : decs) (l : list typ_label) (x : var) A l' T T',
      In A l ->
      lIn A (ldom DS) ->
      expo_mu l x A DS l' T ->
      prode_obj pro DS l' x T T' ->
      prode_obj pro DS l x (typ_sel x A) T'
  | do_sel : forall (DS : decs) (l : list typ_label) (x : var) A l' T T',
      In A l ->
      lIn A (ldom DS) ->
      impo_mu l x A DS l' T ->
      prode_obj de DS l' x T T' ->
      prode_obj de DS l x (typ_sel x A) T'

  | po_all : forall DS l x T U T' U',
      prode_obj de DS l x T T' ->
      prode_obj pro DS l x U U' ->
      prode_obj pro DS l x (typ_all T U) (typ_all T' U')
  | do_all : forall DS l x T U T' U',
      prode_obj pro DS l x T T' ->
      prode_obj de DS l x U U' ->
      prode_obj de DS l x (typ_all T U) (typ_all T' U')

  | pdo_rcd : forall m DSi l x DS DS',
      prode_obj_rcd m DSi l x DS DS' ->
      prode_obj m DSi l x (typ_rcd DS) (typ_rcd DS')
                
  | pdo_obj : forall m DSi l x DS,
      x `notin` fv DS ->
      prode_obj m DSi l x (typ_obj DS) (typ_obj DS)

  with
  prode_obj_rcd : mode -> decs -> list typ_label -> var -> decs -> decs -> Prop :=
  | pdor_nil : forall m DSi l x, prode_obj_rcd m DSi l x decs_nil decs_nil
  | pdor_cons1 : forall m DSi l x DS DS' a T T',
      prode_obj_rcd m DSi l x DS DS' ->
      prode_obj m DSi l x T T' ->
      prode_obj_rcd m DSi l x (decs_cons (dec_trm a T) DS)
                    (decs_cons (dec_trm a T') DS')
  | pdor_cons2 : forall m DSi l x DS DS' A T U T' U',
      prode_obj_rcd m DSi l x DS DS' ->
      prode_obj (oppode m) DSi l x T T' ->
      prode_obj m DSi l x U U' ->
      prode_obj_rcd m DSi l x (decs_cons (dec_typ A T U) DS)
                   (decs_cons (dec_typ A T' U') DS').
  Hint Constructors prode_obj prode_obj_rcd.
  
  Theorem prode_obj_open_consistent : forall m DS l x T T',
      prode_obj m DS l x T T' ->
      forall n y,
        y <> x ->
        lc DS ->
        prode_obj m DS l x (open_rec n y T) (open_rec n y T')
    with prode_obj_rcd_open_consistent : forall m DSi l x DS DS',
        prode_obj_rcd m DSi l x DS DS' ->
        forall n y,
          y <> x ->
          lc DSi ->
          prode_obj_rcd m DSi l x (open_rec n y DS) (open_rec n y DS').
  Proof.
    - clear prode_obj_open_consistent.
      induction on prode_obj; intros; simpl; auto.
      + simpl. destruct_eq; auto.
      + specialize (IHprode_obj n y H2 H3).
        pose proof (expo_mu_preserves_lc H1 H3).
        erewrite <- open_lc_le_typ with (x := y) (m := n) in H1; eauto.
        lia.
      + specialize (IHprode_obj n y H2 H3).
        pose proof (impo_mu_preserves_lc H1 H3).
        erewrite <- open_lc_le_typ with (x := y) (m := n) in H1; eauto.
        lia.
      + constructor. pose proof (fv_open_decs DS y (S n)).
        fsetdec.                            
        
    - clear prode_obj_rcd_open_consistent.
      induction on prode_obj_rcd; intros; simpl; auto.
  Qed.
  
  Program Fixpoint prode_obj_sound m DS l T n
          {measure (n + pc_mu_measure DS l + typ_struct_measure T)} :
    (forall x T' G,
        n = 1 ->
        prode_obj m DS l x T T' ->
        lc DS ->
        G ⊢ trm_var x ⦂ typ_rcd DS ->
        match m with
        | pro => G ⊢ T <⦂ T'
        | de => G ⊢ T' <⦂ T
        end) /\
    (forall x DSs DSs' G,
        n = 0 ->
        T = typ_rcd DSs ->
        prode_obj_rcd m DS l x DSs DSs' ->
        lc DS ->
        G ⊢ trm_var x ⦂ typ_rcd DS ->
        match m with
        | pro => G ⊢ typ_rcd DSs <⦂ typ_rcd DSs'
        | de => G ⊢ typ_rcd DSs' <⦂ typ_rcd DSs
        end) := _.
  Next Obligation.
    split; destruct m; intros; subst.
    - inversion H0; simpl in *; auto; subst.
      + pose proof H4.
        apply expo_mu_is_correct with (G := G) in H4; trivial.
        specialize (prode_obj_sound pro DS l' T0 1).
        simpl in prode_obj_sound.
        eapply prode_obj_sound with (G := G) in H5; eauto.
        apply expo_mu_shrinks_measure in H6; trivial.
        lia.
      + pose proof (prode_obj_sound de DS l T0 1).
        simpl in H4.
        apply H4 with (G := G) in H; trivial. 2: lia.
        clear H4.
        eapply subtyp_all; trivial.
        cofinite.
        pose proof (prode_obj_sound pro DS l (open x0 U) 1).
        simpl in H4. eapply H4; eauto.
        * rewrite open_typ_same_measure. lia.
        * apply prode_obj_open_consistent; trivial.
          auto.
      + specialize (prode_obj_sound pro DS l (typ_rcd DS0) 0).
        simpl in prode_obj_sound.
        eapply prode_obj_sound; eauto.

    - inversion H0; simpl in *; auto; subst.
      + pose proof H4.
        apply impo_mu_is_correct with (G := G) in H4; trivial.
        specialize (prode_obj_sound de DS l' T0 1).
        simpl in prode_obj_sound.
        eapply prode_obj_sound with (G := G) in H5; eauto.
        apply impo_mu_shrinks_measure in H6; trivial.
        lia.
      + pose proof (prode_obj_sound pro DS l T0 1).
        simpl in H4.
        apply H4 with (G := G) in H; trivial. 2: lia.
        clear H4.
        eapply subtyp_all; trivial.
        cofinite.
        pose proof (prode_obj_sound de DS l (open x0 U) 1).
        simpl in H4. eapply H4; eauto.
        * rewrite open_typ_same_measure. lia.
        * apply prode_obj_open_consistent; trivial.
          auto.
      + specialize (prode_obj_sound de DS l (typ_rcd DS0) 0).
        simpl in prode_obj_sound.
        eapply prode_obj_sound; eauto.

    - inversion H1; simpl in *; auto; subst.
      + list_reasoning.
        apply decs_append_resp_subtyp.
        * eapply subtyp_fld.
          specialize (prode_obj_sound pro DS l T 1).
          simpl in prode_obj_sound.
          eapply prode_obj_sound; eauto. lia.
        * specialize (prode_obj_sound pro DS l (typ_rcd DS0) 0).
          simpl in prode_obj_sound.
          eapply prode_obj_sound; eauto. lia.

      + list_reasoning.
        apply decs_append_resp_subtyp. eapply subtyp_typ.
        * specialize (prode_obj_sound de DS l T 1).
          simpl in prode_obj_sound.
          eapply prode_obj_sound; eauto. lia.
        * specialize (prode_obj_sound pro DS l U 1).
          simpl in prode_obj_sound.
          eapply prode_obj_sound; eauto. lia.
        * specialize (prode_obj_sound pro DS l (typ_rcd DS0) 0).
          simpl in prode_obj_sound.
          eapply prode_obj_sound; eauto. lia.

    - inversion H1; simpl in *; auto; subst.
      + list_reasoning.
        apply decs_append_resp_subtyp.
        * eapply subtyp_fld.
          specialize (prode_obj_sound de DS l T 1).
          simpl in prode_obj_sound.
          eapply prode_obj_sound; eauto. lia.
        * specialize (prode_obj_sound de DS l (typ_rcd DS0) 0).
          simpl in prode_obj_sound.
          eapply prode_obj_sound; eauto. lia.

      + list_reasoning.
        apply decs_append_resp_subtyp. eapply subtyp_typ.
        * specialize (prode_obj_sound pro DS l T 1).
          simpl in prode_obj_sound.
          eapply prode_obj_sound; eauto. lia.
        * specialize (prode_obj_sound de DS l U 1).
          simpl in prode_obj_sound.
          eapply prode_obj_sound; eauto. lia.
        * specialize (prode_obj_sound de DS l (typ_rcd DS0) 0).
          simpl in prode_obj_sound.
          eapply prode_obj_sound; eauto. lia.          
  Qed.

  Theorem prode_obj_fv_specs : forall m DS l x T T',
      prode_obj m DS l x T T' ->
      fv T' [<=] fv DS `union` fv T /\ x `notin` fv T'
    with prode_obj_rcd_fv_specs : forall m DSi l x DS DS',
        prode_obj_rcd m DSi l x DS DS' ->
        fv DS' [<=] fv DSi `union` fv DS /\ x `notin` fv DS'.
  Proof.
    - clear prode_obj_fv_specs.
      induction on prode_obj.
      1-6,12:simpl; split; auto; set solve.
      all:destruct_conjs.
      + pose proof (expo_mu_fv_bound H1).
        split; trivial. fsetdec.
      + pose proof (impo_mu_fv_bound H1).
        split; trivial. fsetdec.
      + simpl. fold_cls. split; auto.
        fsetdec.
      + simpl. fold_cls. split; auto.
        fsetdec.
      + apply prode_obj_rcd_fv_specs in H.
        destruct_conjs. split; auto.

    - clear prode_obj_rcd_fv_specs.
      induction on prode_obj_rcd; auto.
      + apply prode_obj_fv_specs in H.
        destruct_conjs.
        simpl in *. split; auto.
        fsetdec.
      + apply prode_obj_fv_specs in H.
        apply prode_obj_fv_specs in H0.
        destruct_conjs.
        simpl in *. split; auto.
        fsetdec.
  Qed.

  Theorem prode_obj_preserves_lc : forall m DS l x T T',
      prode_obj m DS l x T T' ->
      lc DS ->
      forall n, lc_at n T ->
           lc_at n T'
    with prode_obj_rcd_preserves_lc : forall m DSi l x DS DS',
        prode_obj_rcd m DSi l x DS DS' ->
        lc DSi ->
        forall n, lc_at n DS ->
             lc_at n DS'.
  Proof.
    - clear prode_obj_preserves_lc.
      induction on prode_obj; intros; simpl; auto.
      all:fold_lc_at.
      + apply expo_mu_preserves_lc in H1; trivial.
        eapply lc_relax_typ. eauto. lia.
      + apply impo_mu_preserves_lc in H1; trivial.
        eapply lc_relax_typ. eauto. lia.
      + progressive_inversions.
        apply IHprode_obj1 in H4; trivial.
        apply IHprode_obj2 in H5; trivial.
        constructor; trivial.
      + progressive_inversions.
        apply IHprode_obj1 in H4; trivial.
        apply IHprode_obj2 in H5; trivial.
        constructor; trivial.
      + constructor. progressive_inversions.
        eapply prode_obj_rcd_preserves_lc; eauto.

    - clear prode_obj_rcd_preserves_lc.
      induction on prode_obj_rcd; intros; simpl; auto.
      all:progressive_inversions.
      + eapply prode_obj_preserves_lc in H; eauto.
        repeat constructor; auto.
        apply IHprode_obj_rcd; trivial.
      + eapply prode_obj_preserves_lc in H; eauto.
        eapply prode_obj_preserves_lc in H0; eauto.
        repeat constructor; auto.
        apply IHprode_obj_rcd; trivial.
  Qed.
  
  Definition expo_mu_fu l x A DS (tup : list typ_label * typ)
             (H : expo_mu_func l x A DS = tup) :
    {l' & { T |
            In A l ->
            lIn A (ldom DS) ->
            pc_mu_measure DS l > pc_mu_measure DS l' + typ_struct_measure T } }.
  Proof.
    refine (existT _ (fst tup) (exist _ (snd tup) _)).
    abstract (intros; destruct tup;
              apply expo_mu_func_resp_spec in H;
              simpl; eapply expo_mu_shrinks_measure; eauto).
  Defined.

  Lemma expo_mu_fu_correct : forall l x A DS tup H l' T pf,
      @expo_mu_fu l x A DS tup H = existT _ l' (exist _ T pf) ->
      expo_mu_func l x A DS = (l', T).
  Proof.
    intros. unfold expo_mu_fu in H0. tidy_up.
    apply surjective_pairing.
  Qed.

  Definition expo_mu_fd (l : list typ_label) x A (DS : decs) :
    {l' & { T |
            In A l ->
            lIn A (ldom DS) ->
            pc_mu_measure DS l > pc_mu_measure DS l' + typ_struct_measure T } } :=
    @expo_mu_fu l x A DS (expo_mu_func l x A DS) eq_refl.

  Definition impo_mu_fu l x A DS (tup : list typ_label * typ)
             (H : impo_mu_func l x A DS = tup) :
    {l' & { T |
            In A l ->
            lIn A (ldom DS) ->
            pc_mu_measure DS l > pc_mu_measure DS l' + typ_struct_measure T } }.
  Proof.
    refine (existT _ (fst tup) (exist _ (snd tup) _)).
    abstract (intros; destruct tup;
              apply impo_mu_func_resp_spec in H;
              simpl; eapply impo_mu_shrinks_measure; eauto).
  Defined.

  Lemma impo_mu_fu_correct : forall l x A DS tup H l' T pf,
      @impo_mu_fu l x A DS tup H = existT _ l' (exist _ T pf) ->
      impo_mu_func l x A DS = (l', T).
  Proof.
    intros. unfold impo_mu_fu in H0. tidy_up.
    apply surjective_pairing.
  Qed.

  Definition impo_mu_fd (l : list typ_label) x A (DS : decs) :
    {l' & { T |
            In A l ->
            lIn A (ldom DS) ->
            pc_mu_measure DS l > pc_mu_measure DS l' + typ_struct_measure T } } :=
    @impo_mu_fu l x A DS (impo_mu_func l x A DS) eq_refl.

  Local Obligation Tactic := intros; simpl.
  
  Equations motion_obj (m : mode) (DS : decs) (l : list typ_label)
            (x : var) (T : typ) : typ
    by wf (pc_mu_measure DS l + typ_struct_measure T) lt :=
    {
      motion_obj m DS l x typ_top := typ_top;
      motion_obj m DS l x typ_bot := typ_bot;
      motion_obj m DS l x (typ_sel (avar_b n) A) := typ_sel n A;
      
      motion_obj m DS l x (typ_sel (avar_f y) A) with x == y => {
      | left _ with In_dec A l => {
        | left _ with LabelAssocList.KeySetProperties.In_dec A (ldom DS) => {
          | left _ with m => {
            | pro with expo_mu_fd l x A DS => {
              | (existT _ l' (exist _ T' pf)) => motion_obj pro DS l' x T'
              };
            | de with impo_mu_fd l x A DS => {
              | (existT _ l' (exist _ T' pf)) => motion_obj de DS l' x T'
              }
            };
          | right _ => weak_case m
          };
        | right _ => weak_case m
        };
      | right _ => typ_sel y A
      };

      motion_obj m DS l x (typ_all T U) with motion_obj (oppode m) DS l x T => {
      | T' with pick_fresh_var => {
        | exist _ z _ with motion_obj m DS l x U => {
          | U' => typ_all T' U'
          }
        }
      };

      motion_obj m DSi l x (typ_rcd DS) :=
        let motion_rcd (m' : mode) (DS' : decs)
                       (H : decs_struct_measure DS' <= decs_struct_measure DS)
            : decs := _ in
        typ_rcd (motion_rcd m DS _);

      motion_obj m DS' l x (typ_obj DS) with
          AtomSetProperties.In_dec x (fv DS) => {
      | left _ => weak_case m;
      | right _ => typ_obj DS
      }
    }.

  Next Obligation.
    rec_pose pf Hrec. lia.
  Qed.
  Next Obligation.
    rec_pose pf Hrec. lia.
  Qed.
  Next Obligation. lia. Qed.
  Next Obligation. lia. Qed.
  Next Obligation.
    induction DS'.
    - exact (decs_nil).
    - destruct d.
      + refine (decs_cons (dec_typ t (motion_obj (oppode m') DSi l x t0 _)
                                   (motion_obj m' DSi l x t1 _))
                          (IHDS' _)).
        all:abstract (simpl in *; lia).
      + refine (decs_cons (dec_trm t (motion_obj m' DSi l x t0 _))
                          (IHDS' _)).
        all:abstract (simpl in *; lia).
  Defined.
  Next Obligation. auto. Qed.

  Local Obligation Tactic := program_simpl.

  Fixpoint motion_obj_rcd (m : mode) (DSi : decs) (l : list typ_label)
           (x : var) (DS : decs) : decs :=
    match DS with
    | decs_nil => decs_nil
    | decs_cons (dec_typ A T U) DS' =>
      decs_cons (dec_typ A (motion_obj (oppode m) DSi l x T)
                         (motion_obj m DSi l x U))
                (motion_obj_rcd m DSi l x DS')
    | decs_cons (dec_trm a T) DS' =>
      decs_cons (dec_trm a (motion_obj m DSi l x T))
                (motion_obj_rcd m DSi l x DS')
    end.

  Lemma prode_obligation_5_equiv : forall DSi l x DS DS' m' H,
      tail_of DS' DS ->
      motion_obj_obligations_obligation_5 DSi l x DS
        (fun (m0 : mode) (DS0 : decs) (l0 : list typ_label) 
           (x0 : atom) (T0 : typ)
           (_ : pc_mu_measure DS0 l0 + typ_struct_measure T0 <
                pc_mu_measure DSi l + typ_struct_measure ({< DS >})) =>
         motion_obj m0 DS0 l0 x0 T0) m' DS' H
        = motion_obj_rcd m' DSi l x DS'.
  Proof.
    intros. induction DS'; intros; simpl; trivial.      
    destruct d; simpl.
    all:rewrite IHDS'; trivial;
      eapply tail_of_decons; eassumption.
  Qed.

  Program Fixpoint motion_obj_func_sound_wrt_spec m n DSi l T
          {measure (n + pc_mu_measure DSi l + typ_struct_measure T)} :
    forall x T',
      (n = 1 ->
       motion_obj m DSi l x T = T' ->
       prode_obj m DSi l x T T') /\      
      (forall DS DS',
          n = 0 ->
          T = typ_rcd DS ->
          motion_obj_rcd m DSi l x DS = DS' ->
          prode_obj_rcd m DSi l x DS DS') := _.
  Next Obligation.
    split; intros; subst.
    - funelim (motion_obj m DSi l x T).
      
      all:subst; auto.
      2-3,7:destruct m; simpl; auto.
      + rewrite prode_obligation_5_equiv; trivial.
        specialize (motion_obj_func_sound_wrt_spec m 0 DS l (typ_rcd d)).
        constructor.
        eapply motion_obj_func_sound_wrt_spec; trivial.
        lia.
      + clear H. remember (expo_mu_func l a t DS) as exp.
        destruct exp. simpl in *. symmetry in Heqexp.
        apply expo_mu_func_resp_spec in Heqexp.
        eapply po_sel; trivial.
        * eassumption.
        * specialize (motion_obj_func_sound_wrt_spec pro 1 DS l0 t0).
          simpl in motion_obj_func_sound_wrt_spec.
          apply expo_mu_shrinks_measure in Heqexp; trivial.
          eapply motion_obj_func_sound_wrt_spec; trivial.
          lia.
      + clear H. remember (impo_mu_func l a t DS) as exp.
        destruct exp. simpl in *. symmetry in Heqexp.
        apply impo_mu_func_resp_spec in Heqexp.
        eapply do_sel; trivial.
        * eassumption.
        * specialize (motion_obj_func_sound_wrt_spec de 1 DS l0 t0).
          simpl in motion_obj_func_sound_wrt_spec.
          apply impo_mu_shrinks_measure in Heqexp; trivial.
          eapply motion_obj_func_sound_wrt_spec; trivial.
          lia.
      + clear Hind Hind0 Heq.
        destruct m; simpl.
        * constructor. 
          -- specialize (motion_obj_func_sound_wrt_spec de 1 DS l t0).
             simpl in motion_obj_func_sound_wrt_spec.
             eapply motion_obj_func_sound_wrt_spec; trivial.
             lia.
          -- specialize (motion_obj_func_sound_wrt_spec pro 1 DS l t1).
             simpl in motion_obj_func_sound_wrt_spec.
             eapply motion_obj_func_sound_wrt_spec; trivial.
             lia.
        * constructor. 
          -- specialize (motion_obj_func_sound_wrt_spec pro 1 DS l t0).
             simpl in motion_obj_func_sound_wrt_spec.
             eapply motion_obj_func_sound_wrt_spec; trivial.
             lia.
          -- specialize (motion_obj_func_sound_wrt_spec de 1 DS l t1).
             simpl in motion_obj_func_sound_wrt_spec.
             eapply motion_obj_func_sound_wrt_spec; trivial.
             lia.

    - destruct DS; auto.
      destruct d.
      + simpl. constructor.
        * specialize (motion_obj_func_sound_wrt_spec m 0 DSi l (typ_rcd DS)).
          simpl in motion_obj_func_sound_wrt_spec.
          eapply motion_obj_func_sound_wrt_spec; trivial.
          lia.
        * specialize (motion_obj_func_sound_wrt_spec (oppode m) 1 DSi l t0).
          simpl in motion_obj_func_sound_wrt_spec.
          eapply motion_obj_func_sound_wrt_spec; trivial.
          lia.
        * specialize (motion_obj_func_sound_wrt_spec m 1 DSi l t1).
          simpl in motion_obj_func_sound_wrt_spec.
          eapply motion_obj_func_sound_wrt_spec; trivial.
          lia.
      + simpl. constructor.
        * specialize (motion_obj_func_sound_wrt_spec m 0 DSi l (typ_rcd DS)).
          simpl in motion_obj_func_sound_wrt_spec.
          eapply motion_obj_func_sound_wrt_spec; trivial.
          lia.
        * specialize (motion_obj_func_sound_wrt_spec m 1 DSi l t0).
          simpl in motion_obj_func_sound_wrt_spec.
          eapply motion_obj_func_sound_wrt_spec; trivial.
          lia.
  Qed.  

  Lemma promotion_obj_func_shape_invar : forall DS l x T T',
      motion_obj pro DS l x T = T' ->
      promo_shape T T'.
  Proof.
    intros. destruct T; unfold promo_shape.
    all:simp motion_obj in H; auto; simpl in *.
    - exact tt.
    - remember (motion_obj de DS l x T1) as T1'.
      destruct (fresh_vars
           (union (singleton x)
              (union (singleton x)
                 (union (fv_typ T1)
                    (union (fv_typ T2) (union (fv_typ T1') (fv_decs DS))))))).
      simpl in H. unfold motion_obj_unfold_clause_5_clause_1_clause_1 in H.
      eauto.
    - eauto.
    - destruct (AtomSetProperties.In_dec x (fv_decs d)); simpl in *; auto.
  Qed.

  Lemma demotion_obj_func_shape_invar : forall DS l x T T',
      motion_obj de DS l x T = T' ->
      demo_shape T T'.
  Proof.
    intros. destruct T; unfold demo_shape.
    all:simp motion_obj in H; auto; simpl in *.
    - exact tt.
    - remember (motion_obj de DS l x T1) as T1'.
      destruct (fresh_vars
           (union (singleton x)
              (union (singleton x)
                 (union (fv_typ T1)
                    (union (fv_typ T2)
                       (union (fv_typ (motion_obj pro DS l x T1)) (fv_decs DS))))))).
      simpl in H. unfold motion_obj_unfold_clause_5_clause_1_clause_1 in H.
      eauto.
    - eauto.
    - destruct (AtomSetProperties.In_dec x (fv_decs d)); simpl in *; auto.
  Qed.

  Definition motion_measure : relation {_ : env & typ} :=
    lprod (@length (var * typ)) typ_struct_measure.

  Instance WfMotionMeasure : WellFounded motion_measure.
  Proof.
    unfold motion_measure. apply lprod_wf.
  Defined.
  
  Local Ltac term_proof :=
    repeat rewrite app_length;
    try rewrite open_typ_same_measure; simpl; lia.

  Local Hint Extern 5 (_ < _) => term_proof.

  Local Obligation Tactic := intros; simpl.
  
  Fixpoint intersect_dec (l : list var) (xs : atoms) {struct l} : 
    {x | x `in` xs } + {Forall (fun x => x `notin` xs) l}.
  Proof.
    refine (match l with
            | nil => inright _
            | cons x l' =>
              match AtomSetProperties.In_dec x xs with
              | left pf => inleft (exist _ x pf)
              | right H =>
                match intersect_dec l' xs with
                | inleft fnd => inleft fnd
                | inright Hind => inright _
                end
              end
            end).
    - abstract (constructor).
    - abstract (auto).
  Defined.
    
  Inductive prodemotion : mode -> env -> typ -> list var -> typ -> Prop :=
  | pd_top : forall m G V, prodemotion m G typ_top V typ_top
  | pr_top : forall G T V, prodemotion pro G T V typ_top
  | pd_bot : forall m G V, prodemotion m G typ_bot V typ_bot
  | de_bot : forall G T V, prodemotion de G T V typ_bot

  | pd_seln : forall m G n A V,
      prodemotion m G (typ_sel (avar_b n) A) V (typ_sel (avar_b n) A)
  | pd_sel_neq : forall m G (x : var) A V,
      ~In x V ->
      prodemotion m G (typ_sel x A) V (typ_sel x A)

  | pr_sel1 : forall G1 G2 (x : var) A T V,
      exposure G1 T typ_bot ->
      prodemotion pro (G2 ++ x ~ T ++ G1) (typ_sel x A) V typ_bot
  | pr_sel2 : forall G1 G2 (x : var) (A : typ_label) T DS L U V U',
      exposure G1 T (typ_rcd DS) ->
      lbinds A (dec_typ A L U) DS ->
      prodemotion pro G1 U V U' ->
      prodemotion pro (G2 ++ x ~ T ++ G1) (typ_sel x A) V U'
  | pr_sel3 : forall G1 G2 (x : var) (A : typ_label) T DS V U U',
      exposure G1 T (typ_obj DS) ->
      prode_obj pro (open x DS) (collect_ty_labs DS) x (typ_sel x A) U ->
      prodemotion pro G1 U V U' ->
      prodemotion pro (G2 ++ x ~ T ++ G1) (typ_sel x A) V U'

  | de_sel1 : forall G1 G2 (x : var) A T V,
      exposure G1 T typ_bot ->
      prodemotion de (G2 ++ x ~ T ++ G1) (typ_sel x A) V typ_top
  | de_sel2 : forall G1 G2 (x : var) (A : typ_label) T DS L U V L',
      exposure G1 T (typ_rcd DS) ->
      lbinds A (dec_typ A L U) DS ->
      prodemotion de G1 L V L' ->
      prodemotion de (G2 ++ x ~ T ++ G1) (typ_sel x A) V L'
  | de_sel3 : forall G1 G2 (x : var) (A : typ_label) T DS V L L',
      exposure G1 T (typ_obj DS) ->
      prode_obj de (open x DS) (collect_ty_labs DS) x (typ_sel x A) L ->
      prodemotion de G1 L V L' ->
      prodemotion de (G2 ++ x ~ T ++ G1) (typ_sel x A) V L'
                  
  | pd_all : forall m G T U T' U' V,
      prodemotion (oppode m) G T V T' ->
      prodemotion m G U V U' ->
      prodemotion m G (typ_all T U) V (typ_all T' U')

  | pd_rcd : forall m G DS DS' V,
      prodemotion_rcd m G DS V DS' ->
      prodemotion m G (typ_rcd DS) V (typ_rcd DS')
  | pd_obj : forall m G DS V,
      not_capture (typ_obj DS) V ->
      prodemotion m G (typ_obj DS) V (typ_obj DS)

  with
  prodemotion_rcd : mode -> env -> decs -> list var -> decs -> Prop :=
  | pdr_nil : forall m G V, prodemotion_rcd m G decs_nil V decs_nil
  | pdr_cons1 : forall m G DS DS' T T' a V,
      prodemotion_rcd m G DS V DS' ->
      prodemotion m G T V T' ->
      prodemotion_rcd m G (decs_cons (dec_trm a T) DS)
                      V (decs_cons (dec_trm a T') DS')
  | pdr_cons2 : forall m G DS DS' A T U T' U' V,
      prodemotion_rcd m G DS V DS' ->
      prodemotion (oppode m) G T V T' ->
      prodemotion m G U V U' ->
      prodemotion_rcd m G (decs_cons (dec_typ A T U) DS)
                      V (decs_cons (dec_typ A T' U') DS').
  Hint Constructors prodemotion prodemotion_rcd.
  
  Local Ltac by_weakening :=
    reassoc 4 with 3; apply weaken_subtyp; eassumption.

  Local Ltac wf_env :=
    lazymatch goal with
    | H : wf_env (_ ++ _) |- _ => apply wf_deapp in H; invert H; subst
    end.

  Local Hint Resolve wf_uniq.
  
  Theorem prodemotion_open_consistent : forall m G T V T',
      prodemotion m G T V T' ->
      wf_env G ->
      forall n y,
        y `notin` fv G ->
        ~In y V ->
        prodemotion m G (open_rec n y T) V (open_rec n y T')
  with prodemotion_rcd_open_consistent : forall m G DS V DS',
      prodemotion_rcd m G DS V DS' ->
      wf_env G ->
      forall n y,
        y `notin` fv G ->
        ~In y V ->
        prodemotion_rcd m G (open_rec n y DS) V (open_rec n y DS').
  Proof.
    - clear prodemotion_open_consistent.
      induction on prodemotion; intros; simpl; auto.
      + destruct_eq; auto.
      + constructor; trivial.
      + econstructor; eauto.
        wf_env. rec_pose IHprodemotion Hrec.
        * repeat rewrite fv_union in *. simpl in *.
          solve_notin.
        * instantiate (1 := n) in Hrec.
          apply exposure_find_lc in H; trivial.
          apply lc_at_binds_decs with (n := 0) in H0. 2:routine.
          progressive_inversions. 
          rewrite <- (@open_lc_le_typ U 0 n y); trivial.
          lia.
      + eapply pr_sel3; eauto.
        wf_env. rec_pose IHprodemotion Hrec.
        * repeat rewrite fv_union in *. simpl in *.
          solve_notin.
        * instantiate (1 := n) in Hrec.
          apply exposure_find_lc in H; trivial.
          apply prode_obj_preserves_lc with (n := 0) in H0; auto.
          2:repeat constructor.
          progressive_inversions.
          rewrite <- (@open_lc_le_typ U 0 n y); trivial.
          lia.

      + constructor; trivial.
      + econstructor; eauto.
        wf_env. rec_pose IHprodemotion Hrec.
        * repeat rewrite fv_union in *. simpl in *.
          solve_notin.
        * instantiate (1 := n) in Hrec.
          apply exposure_find_lc in H; trivial.
          apply lc_at_binds_decs with (n := 0) in H0. 2:routine.
          progressive_inversions.
          rewrite <- (@open_lc_le_typ L 0 n y); trivial.
          lia.
      + eapply de_sel3; eauto.
        wf_env. rec_pose IHprodemotion Hrec.
        * repeat rewrite fv_union in *. simpl in *.
          solve_notin.
        * instantiate (1 := n) in Hrec.
          apply exposure_find_lc in H; trivial.
          apply prode_obj_preserves_lc with (n := 0) in H0; auto.
          2:repeat constructor.
          progressive_inversions.
          rewrite <- (@open_lc_le_typ L 0 n y); trivial.
          lia.

      + constructor. unfold not_capture in *.
        rewrite Forall_forall in *.
        intros. specialize (H _ H3).
        assert (x <> y). {
          eapply notin_neq; eauto.
        }
        simpl in *. pose proof (fv_open_decs DS y (S n)).
        fsetdec.

    - clear prodemotion_rcd_open_consistent.
      induction on prodemotion_rcd; intros; auto.
      + simpl. constructor; auto.
      + simpl. constructor; auto.
  Qed.

  Lemma not_capture_top : forall V,
      not_capture typ_top V.
  Proof.
    intros. unfold not_capture. rewrite Forall_forall.
    intros. simpl. auto.
  Qed.

  Lemma not_capture_sel_n : forall V n A,
      not_capture (typ_sel (avar_b n) A) V.
  Proof.
    intros. unfold not_capture. rewrite Forall_forall.
    intros. simpl. auto.
  Qed.

  Lemma prode_obj_sound_specialized : forall m DS l x T T' G,
      prode_obj m DS l x T T' ->
      lc DS ->
      G ⊢ trm_var x ⦂ typ_rcd DS ->
      match m with
      | pro => G ⊢ T <⦂ T'
      | de => G ⊢ T' <⦂ T
      end.
  Proof.
    intros.
    pose proof (prode_obj_sound m DS l T 1).
    simpl in H2. destruct H2.
    destruct m; simpl in *.
    - eapply H2; eauto.
    - eapply H2; eauto.
  Qed.
  
  Local Hint Resolve not_capture_bot not_capture_top not_capture_sel_n.

  Program Fixpoint prodemotion_sound (tup : {_ : env & typ})
          {measure tup ( motion_measure )} :
    let (G, T) := tup in
    forall m V T',
      prodemotion m G T V T' ->
      wf_env G ->
      match m with
      | pro => G ⊢ T <⦂ T'
      | de => G ⊢ T' <⦂ T
      end := _.
  
  Obligation 2.
  unfold MR. apply lprod_wf.
  Defined.
  Next Obligation.
    destruct tup. intros.
    destruct H; simpl.
    1,3,5,6,15:destruct m; simpl.
    1-12:simpl; auto.

    - apply exposure_sound in H.
      eapply subtyp_sel2.
      eapply ty_sub. eauto.
      eapply subtyp_trans.
      + by_weakening.
      + instantiate (1 := typ_top). trivial.
    - pose proof (exposure_sound H). wf_env. 
      specialize (prodemotion_sound (existT _ G1 U)).
      simpl in prodemotion_sound.
      rec_pose prodemotion_sound Hrec.
      + left. simpl. term_proof.
      + simpl in *. eapply subtyp_sel2.
        eapply ty_sub. eauto.
        eapply subtyp_trans.
        * by_weakening.
        * eapply subtyp_trans. eapply lbinds_subtyp. eassumption.
           eapply subtyp_typ. eauto.
           by_weakening.
    - pose proof (exposure_sound H). wf_env.
      apply exposure_find_lc in H; trivial.
      specialize (prodemotion_sound (existT _ G1 U)).
      simpl in prodemotion_sound.
      apply prode_obj_sound_specialized with (G := x ~ T ++ G1) in H1.
      2:discharge.
      rec_pose prodemotion_sound Hrec.
      + left. simpl. term_proof.
      + eapply subtyp_trans.
        * apply weaken_subtyp. eassumption.
        * by_weakening.
      + apply ty_mu_elim. eapply ty_sub. eauto.
        auto.

    - apply exposure_sound in H.
      eapply subtyp_sel1.
      eapply ty_sub. eauto.
      eapply subtyp_trans.
      + by_weakening.
      + instantiate (1 := typ_top). trivial.
    - pose proof (exposure_sound H). wf_env. 
      specialize (prodemotion_sound (existT _ G1 L)).
      simpl in prodemotion_sound.
      rec_pose prodemotion_sound Hrec.
      + left. simpl. term_proof.
      + simpl in *. eapply subtyp_sel1.
        eapply ty_sub. eauto.
        eapply subtyp_trans.
        * by_weakening.
        * eapply subtyp_trans. eapply lbinds_subtyp. eassumption.
           eapply subtyp_typ.
           by_weakening. eauto.
    - pose proof (exposure_sound H). wf_env.
      apply exposure_find_lc in H; trivial.
      specialize (prodemotion_sound (existT _ G1 L)).
      simpl in prodemotion_sound.
      apply prode_obj_sound_specialized with (G := x ~ T ++ G1) in H1.
      2:discharge.
      rec_pose prodemotion_sound Hrec.
      + left. simpl. term_proof.
      + simpl in *. eapply subtyp_trans.
        * by_weakening.
        * apply weaken_subtyp. eassumption.
      + apply ty_mu_elim. eapply ty_sub. eauto.
        auto.

    - pose proof (prodemotion_sound (existT _ G T)).
      simpl in H2.
      assert (motion_measure (existT (fun _ : env => typ) G T)
                             (existT (fun _ : env => typ) G (all( T) U))).
      {
        right. simpl. lia.
      }
      specialize (H2 H3 (oppode m) V T' H H0).
      clear H3. destruct m; simpl in *.
      + eapply subtyp_all. trivial.
        cofinite.
        pose proof (prodemotion_sound (existT _ G (open x U))).
        simpl in H3.
        assert (motion_measure (existT (fun _ : env => typ) G (open_rec_typ 0 x U))
                               (existT (fun _ : env => typ) G (all( T) U))). {
          right. simpl. rewrite open_typ_same_measure. lia.
        }
        specialize (H3 H4 pro V (open x U')).
        simpl in H3. clear H4.
        apply weaken_subtyp. apply H3; trivial.
        apply prodemotion_open_consistent; auto.
        rewrite In_is_in. auto.
      + eapply subtyp_all. trivial.
        cofinite.
        pose proof (prodemotion_sound (existT _ G (open x U))).
        simpl in H3.
        assert (motion_measure (existT (fun _ : env => typ) G (open_rec_typ 0 x U))
                               (existT (fun _ : env => typ) G (all( T) U))). {
          right. simpl. rewrite open_typ_same_measure. lia.
        }
        specialize (H3 H4 de V (open x U')).
        simpl in H3. clear H4.
        apply weaken_subtyp. apply H3; trivial.
        apply prodemotion_open_consistent; auto.
        rewrite In_is_in. auto.                

    - assert (forall DDS DDS',
                 prodemotion_rcd m G DDS V DDS' ->
                 decs_struct_measure DDS <= decs_struct_measure DS ->
                 match m as m' return (m' = m -> Prop) with
                 | pro => fun _ : pro = m => G ⊢ {<DDS >} <⦂ {<DDS' >}
                 | de => fun _ : de = m => G ⊢ {<DDS' >} <⦂ {<DDS >}
                 end eq_refl). {
        induction on prodemotion_rcd; intros.
        - destruct m; auto.
        - specialize (IHprodemotion_rcd prodemotion_sound H H0).
          destruct m; simpl in *.
          + list_reasoning.
            apply decs_append_resp_subtyp.
            * eapply subtyp_fld.
              specialize (prodemotion_sound (existT _ G T)).
              apply prodemotion_sound
                with (m := pro) (V := V) (T' := T') in H0; trivial.
              right. simpl in *. lia.
            * apply IHprodemotion_rcd.
              simpl in *. lia.
          + list_reasoning.
            apply decs_append_resp_subtyp.
            * eapply subtyp_fld.
              specialize (prodemotion_sound (existT _ G T)).
              apply prodemotion_sound
                with (m := de) (V := V) (T' := T') in H0; trivial.
              right. simpl in *. lia.
            * apply IHprodemotion_rcd.
              simpl in *. lia.

        - specialize (IHprodemotion_rcd prodemotion_sound H H0).
          destruct m; simpl in *.
          + list_reasoning.
            apply decs_append_resp_subtyp. eapply subtyp_typ.
            * specialize (prodemotion_sound (existT _ G T)).
              apply prodemotion_sound
                with (m := de) (V := V) (T' := T') in H0; trivial.
              right. simpl in *. lia.
            * specialize (prodemotion_sound (existT _ G U)).
              apply prodemotion_sound
                with (m := pro) (V := V) (T' := U') in H0; trivial.
              right. simpl in *. lia.
            * apply IHprodemotion_rcd.
              simpl in *. lia.
          + list_reasoning.
            apply decs_append_resp_subtyp. eapply subtyp_typ.
            * specialize (prodemotion_sound (existT _ G T)).
              apply prodemotion_sound
                with (m := pro) (V := V) (T' := T') in H0; trivial.
              right. simpl in *. lia.
            * specialize (prodemotion_sound (existT _ G U)).
              apply prodemotion_sound
                with (m := de) (V := V) (T' := U') in H0; trivial.
              right. simpl in *. lia.
            * apply IHprodemotion_rcd.
              simpl in *. lia.
      }
      apply H1; trivial.
  Qed.

  Theorem prodemotion_fv_specs : forall m G T V T',
      prodemotion m G T V T' ->
      fv T' [<=] fv G `union` fv T /\ not_capture T' V
    with prodemotion_rcd_fv_specs : forall m G DS V DS',
      prodemotion_rcd m G DS V DS' ->
      fv (typ_rcd DS') [<=] fv G `union` fv (typ_rcd DS) /\ not_capture (typ_rcd DS') V.
  Proof.
    - clear prodemotion_fv_specs.
      induction on prodemotion.
      1-7,10: simpl; split; auto; set solve.
      all:destruct_conjs.
      2-5:split; trivial.
      + unfold not_capture. rewrite Forall_forall.
        intros. simpl. apply notin_singleton_2.
        eapply notin_neq in H0. 2:eauto.
        congruence.
      + apply exposure_preserves_fv in H.
        repeat rewrite fv_union.
        apply fv_decs_shrinks in H0. simpl in *.
        fsetdec.
      + apply exposure_preserves_fv in H.
        repeat rewrite fv_union.
        apply prode_obj_fv_specs in H0. destruct_conjs.
        pose proof (fv_open_decs DS x 0).
        simpl in *. fsetdec.

      + apply exposure_preserves_fv in H.
        repeat rewrite fv_union.
        apply fv_decs_shrinks in H0. simpl in *.
        fsetdec.
      + apply exposure_preserves_fv in H.
        repeat rewrite fv_union.
        apply prode_obj_fv_specs in H0. destruct_conjs.
        pose proof (fv_open_decs DS x 0).
        simpl in *. fsetdec.

      + split.
        * simpl in *. fsetdec.
        * unfold not_capture in *. rewrite Forall_forall in *.
          intros. specialize (H2 _ H3).
          specialize (H0 _ H3).
          simpl. solve_notin.
      + eauto.
      + split; trivial. fsetdec.

    - clear prodemotion_rcd_fv_specs.
      induction on prodemotion_rcd; simpl; destruct_conjs.
      + split; set solve.
        unfold not_capture. simpl. rewrite Forall_forall.
        intros. auto.
      + eapply prodemotion_fv_specs in H. destruct_conjs.
        split.
        * simpl in *. fsetdec.
        * unfold not_capture in *. simpl. rewrite Forall_forall in *.
          intros. specialize (H1 _ H3). specialize (H2 _ H3).
          auto.
      + eapply prodemotion_fv_specs in H.
        eapply prodemotion_fv_specs in H0.
        destruct_conjs. split.
        * simpl in *. fsetdec.
        * unfold not_capture in *. simpl. rewrite Forall_forall in *.
          intros. specialize (H2 _ H6). specialize (H3 _ H6).
          specialize (H5 _ H6).
          auto.
  Qed.

  Theorem prodemotion_preserves_lc : forall m G T V T',
      prodemotion m G T V T' ->
      wf_env G ->
      forall n,
        lc_at n T ->
        lc_at n T'
    with prodemotion_rcd_preserves_lc : forall m G DS V DS',
      prodemotion_rcd m G DS V DS' ->
      wf_env G ->
      forall n,
        lc_at n DS ->
        lc_at n DS'.
  Proof.
    - clear prodemotion_preserves_lc.
      induction on prodemotion; intros; auto.
      1-3,6:constructor.
      + wf_env. apply exposure_find_lc in H; trivial.
        eapply lc_at_binds_decs in H0. 2:routine.
        progressive_inversions.
        apply IHprodemotion in H13; trivial.
        eapply lc_relax_typ. eauto.
        lia.
      + wf_env. apply exposure_find_lc in H; trivial.
        apply prode_obj_preserves_lc with (n := n) in H0; trivial.
        auto.
        apply open_lc_decs. routine.

      + wf_env. apply exposure_find_lc in H; trivial.
        eapply lc_at_binds_decs in H0. 2:routine.
        progressive_inversions.
        apply IHprodemotion in H11; trivial.
        eapply lc_relax_typ. eauto.
        lia.
      + wf_env. apply exposure_find_lc in H; trivial.
        apply prode_obj_preserves_lc with (n := n) in H0; trivial.
        auto.
        apply open_lc_decs. routine.

      + tidy_up.
        constructor; auto.
      + tidy_up. constructor. eauto.

    - clear prodemotion_rcd_preserves_lc.
      induction on prodemotion_rcd; intros; auto.
      + tidy_up. constructor; eauto.
      + tidy_up. constructor; eauto.
  Qed.


  Equations motion (m : mode) (G : env) (T : typ) (V : list var) : typ
    by wf (existT _ G T) (motion_measure) :=
    {
      motion m G typ_top V := typ_top;
      motion m G typ_bot V := typ_bot;
      motion m G (typ_sel (avar_b n) A) V := (typ_sel (avar_b n) A);
      
      motion m G (typ_all T U) V with motion (oppode m) G T V => {
      | T' with motion m G U V => {
        | U' => typ_all T' U'
        }
      };
      
      motion m G (typ_obj DS) V with intersect_dec V (fv DS) => {
      | inright _ => (typ_obj DS);
      | inleft _ => weak_case m
      };
      
      motion m G (typ_rcd DS) V :=
        let helper (DS' : decs)
                   (H : decs_struct_measure DS' <= decs_struct_measure DS)
            : decs := _ in
        typ_rcd (helper DS _);

      motion m G (typ_sel (avar_f x) A) V with In_dec x V => {
      | left _ with split_at x G => {
        | None => weak_case m;
        | Some (existT _ T (existT _ G1 _)) with exposure_func G1 T => {
          | typ_bot =>
            match m with
            | pro => typ_bot
            | de => typ_top
            end;
          | typ_rcd DS with decs_typ_lookup A DS => {
            | None => weak_case m;
            | Some (pair L U) with m => {
              | pro => motion m G1 U V;
              | de => motion m G1 L V
              }
            };
          | typ_obj DS with
                motion_obj m (open x DS) (collect_ty_labs DS) x (typ_sel x A) => {
            | U => motion m G1 U V
            };
          | _ => weak_case m
          }
        };
      | right _ =>
        typ_sel (avar_f x) A
      }
    }.

  Next Obligation.
    tidy_up. left. simpl. term_proof.
  Qed.
  Next Obligation.
    tidy_up. left. simpl. term_proof.
  Qed.
  Next Obligation.
    tidy_up. left. simpl. term_proof.
  Qed.
  Next Obligation.
    right. simpl. lia.
  Qed.
  Next Obligation.
    right. simpl. lia.
  Qed.
  Next Obligation.
    induction DS'.
    - exact (decs_nil).
    - destruct d.
      + refine (decs_cons (dec_typ t (motion (oppode m) G t0 V _)
                                   (motion m G t1 V _))
                          (IHDS' _)).
        3:abstract (simpl in *; lia).
        all:abstract (right; simpl in *; lia).
      + refine (decs_cons (dec_trm t (motion m G t0 V _))
                          (IHDS' _)).
        abstract (right; simpl in *; lia).
        abstract (simpl in *; lia).
  Defined.
  Next Obligation. auto. Qed.

  Local Obligation Tactic := program_simpl.

  Fixpoint motion_rcd (m : mode) (G : env) (DS : decs) (V : list var) : decs :=
    match DS with
    | decs_nil => decs_nil
    | decs_cons (dec_typ A T U) DS' =>
      decs_cons (dec_typ A (motion (oppode m) G T V) (motion m G U V))
                (motion_rcd m G DS' V)
    | decs_cons (dec_trm a T) DS' =>
      decs_cons (dec_trm a (motion m G T V))
                (motion_rcd m G DS' V)
    end.

  Lemma motion_obligation_6_equiv : forall DS' m G DS V pf,
      tail_of DS' DS ->
      motion_obligations_obligation_6 m V
        (fun (m0 : mode) (G0 : env) (T0 : typ) (V1 : list atom)
           (_ : motion_measure (existT (fun _ : env => typ) G0 T0)
                  (existT (fun _ : env => typ) G ({< DS >}))) => 
         motion m0 G0 T0 V1) DS' pf = motion_rcd m G DS' V.
  Proof.
    induction DS'; intros; trivial.
    destruct d; simpl; trivial.
    - rewrite IHDS'; trivial. eapply tail_of_decons. eassumption.
    - rewrite IHDS'; trivial. eapply tail_of_decons. eassumption.
  Qed.

  Lemma motion_obj_func_sound_specialized : forall m DS l x T T',
      motion_obj m DS l x T = T' ->
      prode_obj m DS l x T T'.
  Proof.
    intros.
    pose proof (motion_obj_func_sound_wrt_spec m 1 DS l T x T').
    apply H0; trivial.
  Qed.

  Program Fixpoint motion_func_sound_wrt_spec (tup : {_ : env & typ})
          {measure tup ( motion_measure )} :
    let (G, T) := tup in
    forall m V T',
      motion m G T V = T' ->
      prodemotion m G T V T' := _.
  Next Obligation.
    intros. rename tup into G. rename X into T.
    funelim (motion m G T V).
    all:try (rewrite <- Heqcall in *; clear Heqcall). 
    1-3,5:subst; constructor; auto.
    all:try solve [tidy_up; destruct m; constructor; auto].
    - rewrite motion_obligation_6_equiv in *; trivial.
      assert (forall DS DS',
                 motion_rcd m G DS V = DS' ->
                 decs_struct_measure DS <= decs_struct_measure d ->
                 prodemotion_rcd m G DS V DS'). {
        induction DS; simpl; intros; subst; auto.
        destruct d0.
        + constructor.
          * apply IHDS; auto. simpl in *. lia.
          * specialize (motion_func_sound_wrt_spec (existT _ G t0)).
            simpl in motion_func_sound_wrt_spec.
            eapply motion_func_sound_wrt_spec; trivial.
            right. simpl in *. lia.
          * specialize (motion_func_sound_wrt_spec (existT _ G t1)).
            simpl in motion_func_sound_wrt_spec.
            eapply motion_func_sound_wrt_spec; trivial.
            right. simpl in *. lia.
        + constructor.
          * apply IHDS; auto. simpl in *. lia.
          * specialize (motion_func_sound_wrt_spec (existT _ G t0)).
            simpl in motion_func_sound_wrt_spec.
            eapply motion_func_sound_wrt_spec; trivial.
            right. simpl in *. lia.
      }
      subst. constructor. auto.

    - apply exposure_func_sound_wrt_spec in Heq.
      tidy_up. destruct m; constructor; auto.
    - tidy_up. apply exposure_func_sound_wrt_spec in Heq0.
      apply decs_typ_lookup_sound1 in Heq.
      eapply pr_sel2; eauto.
      specialize (motion_func_sound_wrt_spec (existT _ x0 t1)).
      simpl in motion_func_sound_wrt_spec.
      eapply motion_func_sound_wrt_spec; trivial.
      left. simpl. term_proof.

    - tidy_up. apply exposure_func_sound_wrt_spec in Heq0.
      apply decs_typ_lookup_sound1 in Heq.
      eapply de_sel2; eauto.
      specialize (motion_func_sound_wrt_spec (existT _ x0 t0)).
      simpl in motion_func_sound_wrt_spec.
      eapply motion_func_sound_wrt_spec; trivial.
      left. simpl. term_proof.

    - tidy_up. apply exposure_func_sound_wrt_spec in Heq.
      remember (motion_obj m (open_rec_decs 0 a d0) (collect_ty_labs d0) a (a ⋅ t))
        as mo.
      symmetry in Heqmo.
      apply motion_obj_func_sound_specialized in Heqmo.
      destruct m.
      + eapply pr_sel3; eauto.
        specialize (motion_func_sound_wrt_spec (existT _ x0 mo)).
        simpl in motion_func_sound_wrt_spec.
        eapply motion_func_sound_wrt_spec; trivial.
        left. simpl. term_proof.
      + eapply de_sel3; eauto.
        specialize (motion_func_sound_wrt_spec (existT _ x0 mo)).
        simpl in motion_func_sound_wrt_spec.
        eapply motion_func_sound_wrt_spec; trivial.
        left. simpl. term_proof.

    - subst. constructor.
      + specialize (motion_func_sound_wrt_spec (existT _ G t0)).
        simpl in motion_func_sound_wrt_spec.
        eapply motion_func_sound_wrt_spec; trivial.
        right. simpl. lia.
      + specialize (motion_func_sound_wrt_spec (existT _ G t1)).
        simpl in motion_func_sound_wrt_spec.
        eapply motion_func_sound_wrt_spec; trivial.
        right. simpl. lia.
  Qed.
  Next Obligation.
    unfold MR. apply lprod_wf.
  Qed.

  Lemma promotion_func_shape_invar : forall G T V T',
      motion pro G T V = T' ->
      promo_shape T T'.
  Proof.
    intros. destruct T; unfold promo_shape.
    all:simp motion in H; eauto.
    - exact tt.
    - destruct (intersect_dec V (fv d)); simpl in *; auto.
  Qed.

  Lemma demotion_func_shape_invar : forall G T V T',
      motion de G T V = T' ->
      demo_shape T T'.
  Proof.
    intros. destruct T; unfold demo_shape.
    all:simp motion in H; eauto.
    - exact tt.
    - destruct (intersect_dec V (fv d)); simpl in *; auto.
  Qed.

End PromDemotion.