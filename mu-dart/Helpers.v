Require Import Definitions.

Theorem fv_decs_shrinks : forall l D (DS : decs),
    lbinds l D DS ->
    fv D [<=] fv DS.
Proof. induction on decs; routine. Qed.    

Lemma fv_deapp : forall (G1 G2 : env),
    fv G2 [<=] fv (G1 ++ G2).
Proof.
  induction G1; simpl; auto.
  intros. destruct_conjs. specialize (IHG1 G2).
  simpl in *. set simpl in *.
  split; fsetdec.
Qed.

Theorem lc_at_binds_decs : forall l D (DS : decs) n,
    lbinds l D DS ->
    lc_at n DS ->
    lc_at n D.
Proof. induction on decs; routine. Qed.

Theorem Forall_lbinds_decs : forall l D (DS : decs) P,
    lbinds l D DS ->
    Forall P DS ->
    P (l, D).
Proof.
  induction on decs; routine.
  apply IHdecs; assumption.
Qed.

Theorem lbinds_decs_on_typ : forall A A' T U (DS : decs),
    lbinds A (dec_typ A' T U) DS ->
    A = A'.
Proof. induction on decs; routine. Qed.

Theorem lbinds_decs_on_trm : forall a a' T (DS : decs),
    lbinds a (dec_trm a' T) DS ->
    a = a'.
Proof. induction on decs; routine. Qed.

Theorem lbinds_trm_on_typ : forall (a : trm_label) A T U (DS : decs),
    lbinds a (dec_typ A T U) DS ->
    False.
Proof. induction on decs; routine. Qed.

Theorem lbinds_typ_on_trm : forall (A : typ_label) a T (DS : decs),
    lbinds A (dec_trm a T) DS ->
    False.
Proof. induction on decs; routine. Qed.

Theorem lbinds_defs_on_typ : forall A A' T (DS : defs),
    lbinds A (def_typ A' T) DS ->
    A = A'.
Proof. induction on defs; routine. Qed.

Theorem lbinds_defs_on_trm : forall a a' T (DS : defs),
    lbinds a (def_trm a' T) DS ->
    a = a'.
Proof. induction on defs; routine. Qed.

Theorem lbinds_def_trm_on_typ : forall (a : trm_label) A T (DS : defs),
    lbinds a (def_typ A T) DS ->
    False.
Proof. induction on defs; routine. Qed.

Theorem lbinds_def_typ_on_trm : forall (A : typ_label) a T (DS : defs),
    lbinds A (def_trm a T) DS ->
    False.
Proof. induction on defs; routine. Qed.

Theorem lbinds_subtyp :
  forall G (DS : decs) l D,
    lbinds l D DS ->
    G ⊢ {< DS >} <⦂ {< decs_cons D decs_nil >}.
Proof.
  induction on decs; routine.
  - destruct H0; trivial.
    list_reasoning. eapply subtyp_drop2. routine.
  - list_reasoning. eapply subtyp_trans.
    apply subtyp_drop1; routine.
    eapply IHdecs. eassumption.
Qed.

Theorem subtyp_drop1_gen : forall G DS1 DS2,
    G ⊢ ({< append' DS1 DS2 >}) <⦂ ({< DS2 >}).
Proof.
  destruct DS1.
  - routine.
  - intros. apply subtyp_drop1. routine.
Qed.

Theorem decs_append_nil : forall DS,
    append' DS decs_nil = DS.
Proof. induction on decs; routine. Qed.

Theorem subtyp_drop2_gen : forall G DS1 DS2,
    G ⊢ ({< append' DS1 DS2 >}) <⦂ ({< DS1 >}).
Proof.
  destruct DS2.
  - rewrite decs_append_nil. routine.
  - intros. apply subtyp_drop2. routine.
Qed.

Theorem decs_append_resp_subtyp : forall G DS1 DS2 DS1' DS2',
    G ⊢ ({< DS1 >}) <⦂ {< DS1' >} ->
    G ⊢ ({< DS2 >}) <⦂ {< DS2' >} ->
    G ⊢ ({< append' DS1 DS2 >}) <⦂ {< append' DS1' DS2' >}.
Proof.
  intros. apply subtyp_merge.
  - eapply subtyp_trans. apply subtyp_drop2_gen. trivial.
  - eapply subtyp_trans. apply subtyp_drop1_gen. trivial.
Qed.

Theorem decs_nil_top_of_rcd : forall G DS,
    G ⊢ ({< DS >}) <⦂ typ_rcd decs_nil.
Proof.
  intros. change DS with (append' decs_nil DS).
  apply subtyp_drop2_gen.
Qed.

Theorem decs_subtyp_iter : forall G DS DS',
    Forall (fun D => G ⊢ {< DS >} <⦂ {< decs_cons D decs_nil >}) (to_list DS') ->
    G ⊢ {< DS >} <⦂ {< DS' >}.
Proof.
  intros. induction DS'.
  - apply decs_nil_top_of_rcd.
  - tidy_up. list_reasoning. apply subtyp_merge; auto.
Qed.

Theorem decs_subtyp_is_iter : forall G DS DS',
    G ⊢ {< DS >} <⦂ {< DS' >} ->
    Forall (fun D => G ⊢ {< DS >} <⦂ {< decs_cons D decs_nil >}) (to_list DS').
Proof.
  intros. induction DS'; routine.
  constructor; list_reasoning.
  + eapply subtyp_trans. eassumption.
    apply subtyp_drop2_gen.
  + apply IHDS'. eapply subtyp_trans.
    eassumption. apply subtyp_drop1_gen.
Qed.

Theorem decs_bind_typ : forall G DS l D x,
    G ⊢ trm_var x ⦂ ({< DS >}) ->
    lbinds l D DS ->
    G ⊢ trm_var x ⦂ {< decs_cons D decs_nil >}.
Proof.
  induction on decs; routine.
  - destruct H0. trivial.
    eapply ty_sub. eassumption.
    list_reasoning. eapply subtyp_drop2. routine.
  - eapply IHdecs; routine.
    eapply ty_sub. eassumption.
    list_reasoning. eapply subtyp_drop1. routine.
Qed.  

Lemma lookup_trm_lc_decs : forall (a : trm_label) U (DS : decs),
    lbinds a (a ∷ U) DS ->
    lc DS ->
    lc U.
Proof. induction on decs; routine. Qed.

Lemma lookup_trm_closed_decs : forall (G : env) (a : trm_label) U (DS : decs),
    lbinds a (a ∷ U) DS ->
    fv DS [<=] dom G ->
    fv U [<=] dom G.
Proof. induction on decs; routine. Qed.

Lemma open_rec_append' : forall n x (DS1 DS2 : decs),
    open_rec n x (append' DS1 DS2) = append' (open_rec n x DS1) (open_rec n x DS2).
Proof.
  induction DS1; intros; simpl; auto.
  rewrite IHDS1. simpl. trivial.
Qed.

Hint Extern 1 => match goal with
                | H : _ [=] empty |- _ => rewrite H in *
                | H : empty [=] _ |- _ => rewrite <- H in *
                end : meta_ext.

Local Ltac saturate_set :=
  repeat match goal with
         | H : union _ _ [=] empty |- _ =>
           apply union_empty_3 in H; destruct_conjs
         end.

Local Ltac false :=
  let Contra := fresh "Contra" in
  simpl; right; intro Contra; saturate_set; firstorder.

Local Ltac true := solve [left; set solve].

Definition check_fv_avar (x : avar) : {fv x [=] empty} + {~fv x [=] empty}.
Proof.
  destruct x; simpl.
  - left. set solve.
  - right. intro Contra. destruct (Contra a).
    eapply empty_iff. apply H. set solve.
Defined.

Fixpoint check_fv_typ (T : typ) : {fv T [=] empty} + {~fv T [=] empty}
with check_fv_dec (D : dec) : {fv D [=] empty} + {~fv D [=] empty}
with check_fv_decs (DS : decs) : {fv DS [=] empty} + {~fv DS [=] empty}.
Proof.
  - destruct T eqn:?; intros; simpl.
    all:try solve [true].
    all:try apply check_fv_decs.
    + apply check_fv_avar.
    + destruct (check_fv_typ t1), (check_fv_typ t2).
      true. all:false.
  - destruct D.
    + destruct (check_fv_typ t0), (check_fv_typ t1).
      true. all:false.
    + destruct (check_fv_typ t0).
      true. false.
  - destruct DS.
    + true.
    + destruct (check_fv_dec d), (check_fv_decs DS).
      true. all:false.
Defined.
Local Hint Resolve check_fv_typ.

Fixpoint check_fv_trm (t : trm) : {fv t [=] empty} + {~fv t [=] empty}
with check_fv_val (vl : val) : {fv vl [=] empty} + {~fv vl [=] empty}
with check_fv_def (d : def) : {fv d [=] empty} + {~fv d [=] empty}
with check_fv_defs (ds : defs) : {fv ds [=] empty} + {~fv ds [=] empty}.
Proof.
  - destruct t eqn:?; simpl.
    + apply check_fv_avar.
    + apply check_fv_val.
    + apply check_fv_avar.
    + destruct (check_fv_avar a), (check_fv_avar a0).
      true. all:false.
    + destruct o; try destruct (check_fv_typ t0);
        destruct (check_fv_trm t0_1), (check_fv_trm t0_2).
      all:try true.
      all:false.      
  - destruct vl eqn:?; simpl.
    + destruct (check_fv_decs d), (check_fv_defs d0).
      true. all:false.
    + destruct (check_fv_typ t), (check_fv_trm t0).
      true. all:false.
  - destruct d; simpl; auto.
  - destruct ds; simpl.
    + true.
    + destruct (check_fv_def d), (check_fv_defs ds).
      true. all:false.
Defined.

Local Hint Constructors lc_avar_at lc_typ_at lc_dec_at lc_decs_at.
Local Hint Constructors lc_trm_at lc_val_at lc_def_at lc_defs_at.

Local Ltac compare_nat n :=
  repeat match goal with
         | n' : nat |- _ => different n n';
                        destruct (le_lt_dec (S n') n);
                        fail_if_dup
         end.

Local Ltac yes := solve [simpl; left; auto].

Local Ltac no :=
  let Contra := fresh "Contra" in
  simpl; right; intro Contra; invert Contra; tidy_up; firstorder lia.

Definition check_lc_at_avar n (x : avar) : {lc_at n x} + {~lc_at n x}.
Proof.
  destruct x.
  - compare_nat n. yes. no.
  - yes.
Defined.
  
Fixpoint check_lc_at_typ n (T : typ) : {lc_at n T} + {~lc_at n T}
with check_lc_at_dec n (D : dec) : {lc_at n D} + {~lc_at n D}
with check_lc_at_decs n (DS : decs) : {lc_at n DS} + {~lc_at n DS}.
Proof.
  - destruct T eqn:?.
    all:try yes.
    + destruct (check_lc_at_avar n a).
      yes. no.
    + destruct (check_lc_at_typ n t1), (check_lc_at_typ (S n) t2).
      yes. all:no.
    + destruct (check_lc_at_decs n d).
      yes. no.
    + destruct (check_lc_at_decs (S n) d).
      yes. no.
  - destruct D.
    + destruct (check_lc_at_typ n t0), (check_lc_at_typ n t1).
      yes. all:no.
    + destruct (check_lc_at_typ n t0).
      yes. no.
  - destruct DS.
    + yes.
    + destruct (check_lc_at_dec n d), (check_lc_at_decs n DS).
      yes. all:no.
Defined.

Fixpoint check_lc_at_trm n (t : trm) : {lc_at n t} + {~lc_at n t}
with check_lc_at_val n (vl : val) : {lc_at n vl} + {~lc_at n vl}
with check_lc_at_def n (d : def) : {lc_at n d} + {~lc_at n d}
with check_lc_at_defs n (ds : defs) : {lc_at n ds} + {~lc_at n ds}.
Proof.
  - destruct t eqn:?.
    + destruct (check_lc_at_avar n a).
      yes. no.
    + destruct (check_lc_at_val n v).
      yes. no.
    + destruct (check_lc_at_avar n a).
      yes. no.
    + destruct (check_lc_at_avar n a), (check_lc_at_avar n a0).
      yes. all:no.
    + destruct o; try destruct (check_lc_at_typ n t0);
        destruct (check_lc_at_trm n t0_1), (check_lc_at_trm (S n) t0_2).
      all:try yes. all:no.
  - destruct vl.
    + destruct (check_lc_at_decs (S n) d), (check_lc_at_defs (S n) d0).
      yes. all:no.
    + destruct (check_lc_at_typ n t), (check_lc_at_trm (S n) t0).
      yes. all:no.
  - destruct d.
    + destruct (check_lc_at_typ n t0).
      yes. no.
    + destruct (check_lc_at_trm n t0).
      yes. no.
  - destruct ds.
    + yes.
    + destruct (check_lc_at_def n d), (check_lc_at_defs n ds).
      yes. all:no.
Defined.

(** decision procedure for deciding domain uniqueness. *)
Definition defs_luniq_dec (ds : defs) : {luniq ds} + {~luniq ds}.
Proof.
  induction ds.
  - left. routine.
  - destruct IHds.
    + simpl.
      destruct (LabelAssocList.KeySetProperties.In_dec (lab_def_of d) (ldom ds)).
      * right. intro Contra.
        lsolve_uniq.
      * left. lsolve_uniq.
    + right.  intro Contra. inversion Contra.
      subst. intuition.
Defined.

Definition decs_luniq_dec (DS : decs) : {luniq DS} + {~luniq DS}.
Proof.
  induction DS.
  - left. routine.
  - destruct IHDS.
    + simpl.
      destruct (LabelAssocList.KeySetProperties.In_dec (lab_dec_of d) (ldom DS)).
      * right. intro Contra.
        lsolve_uniq.
      * left. lsolve_uniq.
    + right.  intro Contra. inversion Contra.
      subst. intuition.
Defined.

Inductive uniq_dom_typ : typ -> Prop :=
| udt_top : uniq_dom_typ typ_top
| udt_bot : uniq_dom_typ typ_bot
| udt_sel : forall {x A}, uniq_dom_typ (typ_sel x A)
| udt_all : forall {T U}, uniq_dom_typ T ->
                     uniq_dom_typ U ->
                     uniq_dom_typ (typ_all T U)
| udt_rcd : forall {DS}, uniq_dom_decs DS ->
                    luniq DS ->
                    uniq_dom_typ (typ_rcd DS)
| udt_obj : forall {DS}, uniq_dom_decs DS ->
                    luniq DS ->
                    uniq_dom_typ (typ_obj DS)
with
uniq_dom_dec : dec -> Prop :=
| udd_typ : forall {X T U}, uniq_dom_typ T ->
                       uniq_dom_typ U ->
                       uniq_dom_dec (dec_typ X T U)
| udd_trm : forall {x T}, uniq_dom_typ T ->
                     uniq_dom_dec (dec_trm x T)
with
uniq_dom_decs : decs -> Prop :=
| udd_nil : uniq_dom_decs decs_nil
| udd_cons : forall {D DS}, uniq_dom_dec D ->
                       uniq_dom_decs DS ->
                       uniq_dom_decs (decs_cons D DS).

Inductive uniq_dom_trm : trm -> Prop :=
| udt_var : forall {x}, uniq_dom_trm (trm_var x)
| udt_val : forall {vl}, uniq_dom_val vl -> uniq_dom_trm (trm_val vl)
| udt_tsel : forall {x A}, uniq_dom_trm (trm_sel x A)
| udt_app : forall {x y}, uniq_dom_trm (trm_app x y)
| udt_let1 : forall {t1 t2}, uniq_dom_trm t1 ->
                       uniq_dom_trm t2 ->
                       uniq_dom_trm (trm_let None t1 t2)
| udt_let2 : forall {T t1 t2},
    uniq_dom_typ T ->
    uniq_dom_trm t1 ->
    uniq_dom_trm t2 ->
    uniq_dom_trm (trm_let (Some T) t1 t2)
with
uniq_dom_val : val -> Prop :=
| udv_obj : forall {DS ds}, uniq_dom_decs DS ->
                       luniq DS ->
                       uniq_dom_defs ds ->
                       luniq ds ->
                       uniq_dom_val (val_obj DS ds)
| udv_lam : forall {T t}, uniq_dom_typ T ->
                     uniq_dom_trm t ->
                     uniq_dom_val (val_lam T t)
with
uniq_dom_def : def -> Prop :=
| udf_typ : forall {X T}, uniq_dom_typ T ->
                     uniq_dom_def (def_typ X T)
| udf_trm : forall {x t}, uniq_dom_trm t ->
                     uniq_dom_def (def_trm x t)
with
uniq_dom_defs : defs -> Prop :=
| udf_nil : uniq_dom_defs defs_nil
| udf_cons : forall {d ds}, uniq_dom_def d ->
                       uniq_dom_defs ds ->
                       uniq_dom_defs (defs_cons d ds).
Local Hint Constructors uniq_dom_typ uniq_dom_dec uniq_dom_decs.
Local Hint Constructors uniq_dom_trm uniq_dom_val uniq_dom_def uniq_dom_defs.

Fixpoint uniq_dom_typ_dec (x : typ) : {uniq_dom_typ x} + {~uniq_dom_typ x}
with uniq_dom_dec_dec (x : dec) : {uniq_dom_dec x} + {~uniq_dom_dec x}
with uniq_dom_decs_dec (x : decs) : {uniq_dom_decs x} + {~uniq_dom_decs x}.
Proof.
  - clear uniq_dom_typ_dec. induction x; routine.
    all:try no.
    all:destruct (decs_luniq_dec d), (uniq_dom_decs_dec d).
    all:try no.
    all:yes.
  - induction x.
    + destruct (uniq_dom_typ_dec t0), (uniq_dom_typ_dec t1).
      yes. all:no.
    + destruct (uniq_dom_typ_dec t0). yes. no.
  - clear uniq_dom_decs_dec. induction x; routine.
    + destruct (uniq_dom_dec_dec d). yes. no.
    + no.
Defined.

Fixpoint uniq_dom_trm_dec (x : trm) : {uniq_dom_trm x} + {~uniq_dom_trm x}
with uniq_dom_val_dec (x : val) : {uniq_dom_val x} + {~uniq_dom_val x}
with uniq_dom_def_dec (x : def) : {uniq_dom_def x} + {~uniq_dom_def x}
with uniq_dom_defs_dec (x : defs) : {uniq_dom_defs x} + {~uniq_dom_defs x}.
Proof.
  - clear uniq_dom_trm_dec. induction x; routine.
    all:try no.
    destruct (uniq_dom_val_dec v).
    yes. no.
    destruct o; try destruct (uniq_dom_typ_dec t).
    yes. no. yes.
  - induction x.
    + destruct (decs_luniq_dec d), (uniq_dom_decs_dec d),
        (defs_luniq_dec d0), (uniq_dom_defs_dec d0).
      all:try no.
      yes.
    + destruct (uniq_dom_typ_dec t), (uniq_dom_trm_dec t0).
      yes. all:no.
  - induction x.
    + destruct (uniq_dom_typ_dec t0). yes. no.
    + destruct (uniq_dom_trm_dec t0). yes. no.
  - clear uniq_dom_defs_dec. induction x; routine.
    + destruct (uniq_dom_def_dec d). yes. no.
    + no.
Defined.

Lemma uniq_dom_val_decons : forall d ds D DS,
    uniq_dom_val ([decs_cons D DS]{ defs_cons d ds }) ->
    uniq_dom_val ([ DS ]{ ds }).
Proof. routine. Qed.

Local Ltac by_ind T :=
  lazymatch reverse goal with
  | H : context[T] |- _ => clear H
  end; induction on T; try solve [eroutine].

Lemma decs_open_same_ldom : forall (DS : decs) n z,
    ldom (open_rec n z DS) = ldom DS.
Proof using.
  induction on decs.
  - routine.
  - destruct d; routine.
Qed.

Lemma defs_open_same_ldom : forall (ds : defs) n z,
    ldom (open_rec n z ds) = ldom ds.
Proof using.
  induction on defs.
  - routine.
  - destruct d; routine.
Qed.

Lemma luniq_decs_open : forall (DS : decs) n z, luniq DS -> luniq (open_rec n z DS).
Proof.
  induction on decs; routine.
  constructor. auto. rewrite decs_open_same_ldom.
  destruct d; simpl; trivial.
Qed.

Lemma luniq_defs_open : forall (ds : defs) n z, luniq ds -> luniq (open_rec n z ds).
Proof.
  induction on defs; routine.
  constructor. auto. rewrite defs_open_same_ldom.
  destruct d; simpl; trivial.
Qed.

Local Hint Resolve luniq_decs_open luniq_defs_open.

Lemma uniq_dom_typ_open : forall (x : typ) n z, uniq_dom_typ x -> uniq_dom_typ (open_rec n z x)
with uniq_dom_dec_open : forall (x : dec) n z, uniq_dom_dec x -> uniq_dom_dec (open_rec n z x)
with uniq_dom_decs_open : forall (x : decs) n z, uniq_dom_decs x -> uniq_dom_decs (open_rec n z x).
Proof.
  - by_ind typ. 
  - by_ind dec.
  - by_ind decs.
Qed.

Local Hint Resolve uniq_dom_typ_open uniq_dom_decs_open.

Lemma uniq_dom_trm_open : forall (x : trm) n z, uniq_dom_trm x -> uniq_dom_trm (open_rec n z x)
with uniq_dom_val_open : forall (x : val) n z, uniq_dom_val x -> uniq_dom_val (open_rec n z x)
with uniq_dom_def_open : forall (x : def) n z, uniq_dom_def x -> uniq_dom_def (open_rec n z x)
with uniq_dom_defs_open : forall (x : defs) n z, uniq_dom_defs x -> uniq_dom_defs (open_rec n z x).
Proof.
  - by_ind trm. destruct o; routine.
  - by_ind val. routine.
  - by_ind def.
  - by_ind defs.
Qed.

Lemma uniq_dom_decs_lbinds_typ : forall DS (A : typ_label) T U,
    uniq_dom_decs DS ->
    lbinds A (dec_typ A T U) DS ->
    uniq_dom_typ T /\ uniq_dom_typ U.
Proof.
  induction on uniq_dom_decs; intros; tidy_up; auto.
Qed.
