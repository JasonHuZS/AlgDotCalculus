Set Implicit Arguments.
Require Import Definitions.

Hint Constructors lc_avar_at lc_typ_at lc_dec_at lc_decs_at
     lc_trm_at lc_val_at lc_def_at lc_defs_at.

Local Ltac prove_by_ind T := induction on T; eroutine.

Section LcOpenClosingProperties.

  Local Notation lc_relax T :=
    (forall (v : T) n m, lc_at n v -> n <= m -> lc_at m v).

  Local Hint Extern 1 => lia.
  
  Lemma lc_relax_avar : lc_relax avar.
  Proof using. induction 1; routine. Qed.
  Hint Resolve lc_relax_avar.

  Lemma lc_relax_typ : lc_relax typ
  with lc_relax_dec : lc_relax dec
  with lc_relax_decs : lc_relax decs.
  Proof using.
    - clear lc_relax_typ.
      intros. gen m.
      induction H; eroutine.
    - clear lc_relax_dec. induction 1; eroutine.
    - clear lc_relax_decs. induction 1; eroutine.
  Qed.
  Hint Resolve lc_relax_typ lc_relax_decs.

  Lemma lc_relax_trm : lc_relax trm
  with lc_relax_val : lc_relax val
  with lc_relax_def : lc_relax def
  with lc_relax_defs : lc_relax defs.
  Proof using.
    - clear lc_relax_trm.
      intros. gen m.
      induction H; eroutine.
    - clear lc_relax_val. induction 1; eroutine.
    - clear lc_relax_def.
      induction 1; eroutine.
      constructor. eapply lc_relax_typ; eassumption.
    - clear lc_relax_defs. induction 1; eroutine.
  Qed.


  Local Notation open_lc T :=
    (forall (v : T) n x, lc_at (S n) v -> lc_at n (open_rec n x v)).
  
  Lemma open_lc_avar : open_lc avar.
  Proof using.
    intros. invert H.
    - destruct_eq; routine.
    - routine.
  Qed.
  Hint Resolve open_lc_avar.

  Ltac solve_open_lc H :=
    simpl in H; dependent induction H; routine.
  
  Lemma open_lc_typ : open_lc typ
  with open_lc_dec : open_lc dec
  with open_lc_decs : open_lc decs.
  Proof using.
    - clear open_lc_typ. intros. solve_open_lc H.
    - clear open_lc_dec. intros. solve_open_lc H.
    - clear open_lc_decs. intros. solve_open_lc H.
  Qed.
  Hint Resolve open_lc_typ open_lc_decs.

  Lemma open_lc_trm : open_lc trm
  with open_lc_val : open_lc val
  with open_lc_def : open_lc def
  with open_lc_defs : open_lc defs.
  Proof using.
    - clear open_lc_trm. intros. solve_open_lc H.
    - clear open_lc_val. intros. solve_open_lc H.
    - clear open_lc_def.
      intros. solve_open_lc H.
      constructor. apply open_lc_typ. assumption.
    - clear open_lc_defs. intros. solve_open_lc H.
  Qed. 

  Local Notation open_lc_le T :=
    (forall (v : T) n m x, lc_at n v -> m >= n -> open_rec m x v = v).

  Lemma open_lc_le_avar : open_lc_le avar.
  Proof using. destr on avar; routine. Qed.
  Hint Resolve open_lc_le_avar.

  Lemma open_lc_le_typ : open_lc_le typ
  with open_lc_le_dec : open_lc_le dec
  with open_lc_le_decs : open_lc_le decs.
  Proof using.
    - clear open_lc_le_typ. prove_by_ind typ.
    - clear open_lc_le_dec. prove_by_ind dec.
    - clear open_lc_le_decs. prove_by_ind decs.
  Qed.
  Hint Resolve open_lc_le_typ open_lc_le_decs.
  
  Lemma open_lc_le_trm : open_lc_le trm
  with open_lc_le_val : open_lc_le val
  with open_lc_le_def : open_lc_le def
  with open_lc_le_defs : open_lc_le defs.
  Proof using.
    - clear open_lc_le_trm. prove_by_ind trm.
      destruct o; eroutine.
    - clear open_lc_le_val. prove_by_ind val.
    - clear open_lc_le_def. prove_by_ind def.
    - clear open_lc_le_defs. prove_by_ind defs.
  Qed.
      
  Local Notation open_lc_inv T :=
    (forall (v : T) n x, lc_at n (open_rec n x v) -> lc_at (S n) v).
  
  Lemma open_lc_inv_avar : open_lc_inv avar.
  Proof using.
    intros. simpl in H. invert H.
    - destruct v; routine.
    - destruct v; routine.
  Qed.
  Hint Resolve open_lc_inv_avar.

  Lemma open_lc_inv_typ : open_lc_inv typ
  with open_lc_inv_dec : open_lc_inv dec
  with open_lc_inv_decs : open_lc_inv decs.
  Proof using.
    - clear open_lc_inv_typ. prove_by_ind typ.
    - clear open_lc_inv_dec. prove_by_ind dec.
    - clear open_lc_inv_decs. prove_by_ind decs.
  Qed.
  Hint Resolve open_lc_inv_typ open_lc_inv_decs.

  Lemma open_lc_inv_trm : open_lc_inv trm
  with open_lc_inv_val : open_lc_inv val
  with open_lc_inv_def : open_lc_inv def
  with open_lc_inv_defs : open_lc_inv defs.
  Proof using.
    - clear open_lc_inv_trm. prove_by_ind trm.
      destruct o; eroutine.
    - clear open_lc_inv_val. prove_by_ind val.
    - clear open_lc_inv_def. prove_by_ind def.
      constructor. eapply open_lc_inv_typ. eassumption.
    - clear open_lc_inv_defs. prove_by_ind defs.
  Qed.

  Local Notation open_left_inv T :=
    (forall (v : T) n x, lc_at n v -> open_rec n x (close_rec n x v) = v).

  Lemma open_left_inv_avar : open_left_inv avar.
  Proof using. induction 1; routine. Qed.
  Hint Resolve open_left_inv_avar.

  Lemma open_left_inv_typ : open_left_inv typ
  with open_left_inv_dec : open_left_inv dec
  with open_left_inv_decs : open_left_inv decs.
  Proof using.
    - clear open_left_inv_typ. induction 1; routine.
    - clear open_left_inv_dec. induction 1; routine.
    - clear open_left_inv_decs. induction 1; routine.
  Qed.
  Hint Resolve open_left_inv_typ open_left_inv_decs.

  Lemma open_left_inv_trm : open_left_inv trm
  with open_left_inv_val : open_left_inv val
  with open_left_inv_def : open_left_inv def
  with open_left_inv_defs : open_left_inv defs.
  Proof using.
    - clear open_left_inv_trm. induction 1; routine.
    - clear open_left_inv_val. induction 1; routine.
    - clear open_left_inv_def. induction 1; routine.
    - clear open_left_inv_defs. induction 1; routine.
  Qed.

  Local Notation close_lc T :=
    (forall (v : T) n x, lc_at n v -> lc_at (S n) (close_rec n x v)).
  
  Lemma close_lc_avar : close_lc avar.
  Proof using. prove_by_ind avar. Qed.
  Hint Resolve close_lc_avar.
  
  Lemma close_lc_typ : close_lc typ
  with close_lc_dec : close_lc dec
  with close_lc_decs : close_lc decs.
  Proof using.
    - clear close_lc_typ. prove_by_ind typ.
    - clear close_lc_dec. prove_by_ind dec.
    - clear close_lc_decs. prove_by_ind decs.
  Qed.
  Hint Resolve close_lc_typ close_lc_decs.

  Lemma close_lc_trm : close_lc trm
  with close_lc_val : close_lc val
  with close_lc_def : close_lc def
  with close_lc_defs : close_lc defs.
  Proof using.
    - clear close_lc_trm. prove_by_ind trm.
      destruct o; eroutine.
    - clear close_lc_val. prove_by_ind val.
    - clear close_lc_def. prove_by_ind def.
      constructor. apply close_lc_typ. assumption.
    - clear close_lc_defs. prove_by_ind defs.
  Qed.

End LcOpenClosingProperties.


Section FvOpen.

  Local Notation fv_open T :=
    (forall (v : T) x n, fv (open_rec n x v) [<=] singleton x \u fv v).
  
  Lemma fv_open_avar : fv_open avar.
  Proof using.
    induction on avar; intros; simpl.
    - progressive_destructions; set solve.
    - set solve.
  Qed.
  Hint Resolve fv_open_avar.

  Local Ltac union_pf :=
    autorewrite with meta_ext;
    repeat (split; set solve);
    etransitivity; try solve [eauto]; set solve.

  Local Ltac solve_it T :=
    induction on T;
    intros; simpl;
    try lazymatch goal with
        | o : option _ |- _ => destruct o; simpl
        end;
    set solve; fold_cls; union_pf.
  
  Lemma fv_open_typ : fv_open typ
  with fv_open_dec : fv_open dec
  with fv_open_decs : fv_open decs.
  Proof using.
    - clear fv_open_typ. solve_it typ.
    - clear fv_open_dec. solve_it dec.
    - clear fv_open_decs. solve_it decs.
  Qed.
  Hint Resolve fv_open_typ fv_open_decs.

  Lemma fv_open_trm : fv_open trm
  with fv_open_val : fv_open val
  with fv_open_def : fv_open def
  with fv_open_defs : fv_open defs.
  Proof using.
    - clear fv_open_trm. solve_it trm.
    - clear fv_open_val. solve_it val.
    - clear fv_open_def. solve_it def.
    - clear fv_open_defs. solve_it defs.
  Qed.
  
  Local Notation fv_open_grow T :=
    (forall (v : T) x n, fv v [<=] fv (open_rec n x v)).

  Lemma fv_open_grow_avar : fv_open_grow avar.
  Proof using.
    induction on avar; intros; set solve.
  Qed.
  Hint Resolve fv_open_grow_avar.

  Lemma fv_open_grow_typ : fv_open_grow typ
  with fv_open_grow_dec : fv_open_grow dec
  with fv_open_grow_decs : fv_open_grow decs.
  Proof using.
    - clear fv_open_grow_typ. solve_it typ.
    - clear fv_open_grow_dec. solve_it dec.
    - clear fv_open_grow_decs. solve_it decs.
  Qed.
  Hint Resolve fv_open_grow_typ fv_open_grow_decs.
  
  Lemma fv_open_grow_trm : fv_open_grow trm
  with fv_open_grow_val : fv_open_grow val
  with fv_open_grow_def : fv_open_grow def
  with fv_open_grow_defs : fv_open_grow defs.
  Proof using.
    - clear fv_open_grow_trm. solve_it trm.
    - clear fv_open_grow_val. solve_it val.
    - clear fv_open_grow_def. solve_it def.
    - clear fv_open_grow_defs. solve_it defs.
  Qed.
  
End FvOpen.

Section FvClose.

  Local Notation fv_close_self T :=
    (forall (v : T) x n, x `notin` fv (close_rec n x v)).

  Lemma fv_close_self_avar : fv_close_self avar.
  Proof using. intros. destruct v; routine. Qed.

  Hint Resolve AtomSetProperties.not_in_union fv_close_self_avar.

  Local Ltac by_ind T :=
    induction on T; intros; simpl; auto.
  
  Lemma fv_close_self_typ : fv_close_self typ
  with fv_close_self_dec : fv_close_self dec
  with fv_close_self_decs : fv_close_self decs.
  Proof using.
    - clear fv_close_self_typ. by_ind typ.
    - clear fv_close_self_dec. by_ind dec.
    - clear fv_close_self_decs. by_ind decs.
  Qed.

  Local Ltac pf_union := apply AtomSetProperties.not_in_union.
  Hint Resolve fv_close_self_typ fv_close_self_decs.
  
  Lemma fv_close_self_trm : fv_close_self trm
  with fv_close_self_val : fv_close_self val
  with fv_close_self_def : fv_close_self def
  with fv_close_self_defs : fv_close_self defs.
  Proof using.
    - clear fv_close_self_trm. by_ind trm.
      destruct o; simpl; auto.
    - clear fv_close_self_val. by_ind val.
    - clear fv_close_self_def. by_ind def.
    - clear fv_close_self_defs. by_ind defs.
  Qed.

  Local Notation fv_close T :=
    (forall (v : T) x n, fv (close_rec n x v) [<=] fv v).

  Lemma fv_close_avar : fv_close avar.
  Proof using.
    intros. destruct v; auto.
    destruct_eq; set solve.
  Qed.

  Local Ltac union_pf :=
    set simpl;
    repeat (split; set solve);
    etransitivity; try eassumption; set solve.
  Hint Resolve fv_close_avar.

  Local Ltac by_ind' T :=
    by_ind T;
    try lazymatch goal with
        | o : option _ |- _ => destruct o; simpl
        end;
    union_pf.
  
  Lemma fv_close_typ : fv_close typ
  with fv_close_dec : fv_close dec
  with fv_close_decs : fv_close decs.
  Proof using.
    - clear fv_close_typ. by_ind' typ.
    - clear fv_close_dec. by_ind' dec.
    - clear fv_close_decs. by_ind' decs.
  Qed.
  Hint Resolve fv_close_typ fv_close_decs.
  
  Lemma fv_close_trm : fv_close trm
  with fv_close_val : fv_close val
  with fv_close_def : fv_close def
  with fv_close_defs : fv_close defs.
  Proof using.
    - clear fv_close_trm. by_ind' trm.
    - clear fv_close_val. by_ind' val.
    - clear fv_close_def. by_ind' def.
    - clear fv_close_defs. by_ind' defs.
  Qed.

  Local Notation fv_add_close T :=
    (forall (v : T) x (G : env) n, fv v [<=] add x (dom G) ->
                              fv (close_rec n x v) [<=] dom G).
  
  Lemma fv_add_close_avar : fv_add_close avar.
  Proof using.
    intros. destruct v; simpl in *.
    - set solve.
    - destruct_eq. set solve.
      rewrite AtomSetProperties.add_union_singleton in H.
      apply AtomSetImpl.union_1 with (x := a) in H.
      destruct H.
      + apply AtomSetImpl.singleton_1 in H.
        congruence.
      + autounfold. simpl. intros.
        apply AtomSetImpl.singleton_1 in H0.
        subst. trivial.
      + apply AtomSetImpl.singleton_2. trivial.
  Qed.
  Hint Resolve fv_add_close_avar.

  Remove Hints fv_close_typ fv_close_decs.

  Local Ltac boom T :=
    by_ind T;
    try lazymatch goal with
        | o : option _ |- _ => destruct o; simpl
        end;
    set solve; simpl in *;
    set simpl; repeat (split; set solve);
    auto; ctx_app; set solve.
  
  Lemma fv_add_close_typ : fv_add_close typ
  with fv_add_close_dec : fv_add_close dec
  with fv_add_close_decs : fv_add_close decs.
  Proof using.
    - clear fv_add_close_typ. boom typ.
    - clear fv_add_close_dec. boom dec.
    - clear fv_add_close_decs. boom decs.
  Qed.
  Hint Resolve fv_add_close_typ fv_add_close_decs.
  
  Lemma fv_add_close_trm : fv_add_close trm
  with fv_add_close_val : fv_add_close val
  with fv_add_close_def : fv_add_close def
  with fv_add_close_defs : fv_add_close defs.
  Proof using.
    - clear fv_add_close_trm. boom trm.
    - clear fv_add_close_val. boom val.
    - clear fv_add_close_def. boom def.
    - clear fv_add_close_defs. boom defs.
  Qed.

  Local Notation notin_fv_close T :=
    (forall (v : T) n x , x `notin` fv v -> close_rec n x v = v).

  Lemma notin_fv_close_avar : notin_fv_close avar.
  Proof.
    intros. destruct v; simpl; trivial.
    simpl in *. destruct_eq.
  Qed.
  Local Hint Resolve notin_fv_close_avar.

  Lemma notin_fv_close_typ : notin_fv_close typ
  with notin_fv_close_dec : notin_fv_close dec
  with notin_fv_close_decs : notin_fv_close decs.
  Proof.
    - clear notin_fv_close_typ. prove_by_ind typ.
    - clear notin_fv_close_dec. prove_by_ind dec.
    - clear notin_fv_close_decs. prove_by_ind decs.
  Qed.
  Local Hint Resolve notin_fv_close_typ notin_fv_close_decs.


  Lemma notin_fv_close_trm : notin_fv_close trm
  with notin_fv_close_val : notin_fv_close val
  with notin_fv_close_def : notin_fv_close def
  with notin_fv_close_defs : notin_fv_close defs.
  Proof.
    - clear notin_fv_close_trm. prove_by_ind trm.
      destruct o; routine.
    - clear notin_fv_close_val. prove_by_ind val.
    - clear notin_fv_close_def. prove_by_ind def.
    - clear notin_fv_close_defs. prove_by_ind defs.
  Qed.  
  
End FvClose.
