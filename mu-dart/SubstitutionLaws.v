Require Import Definitions.
Require Import Substitution.
Require Import Weakening.

Section SubstProps.

  Lemma substi_resp_append' : forall x y (DS1 DS2 : decs),
    substi x y $ append' DS1 DS2 =
    append' (substi x y DS1) $ substi x y DS2.
  Proof using. induction on decs; routine. Qed.
  Hint Resolve substi_resp_append'.

  Lemma substi_resp_rcd : forall x y (DS1 DS2 : decs),
      substi x y ({< append' DS1 DS2 >}) =
      {< append' (substi x y DS1) (substi x y DS2) >}.
  Proof using. routine hinted list_reasoning; fold_substi. Qed.

  Lemma substi_resp_not_empty : forall x y (DS : decs),
      not_empty DS ->
      not_empty (substi x y DS).
  Proof using. destr on decs; routine. Qed.
  
  Lemma same_ldom : forall x y (ds : defs),
      ldom (subst_defs x y ds) = ldom ds.
  Proof using.
    induction on defs.
    - routine.
    - destruct d; routine.
  Qed.

  Lemma substi_resp_luniq : forall x y (ds : defs),
      luniq ds ->
      luniq (subst_defs x y ds).
  Proof using.
    induction on defs.
    - routine.
    - destruct d; routine;
        constructor; try rewrite same_ldom; routine.
  Qed.
  Hint Resolve substi_resp_luniq.

  Lemma substi_resp_lbinds : forall x y l D (DS : decs),
      lbinds l D DS ->
      lbinds l (substi x y D) (substi x y DS).
  Proof using.
    induction on decs.
    - routine.
    - destruct d; routine;
        right; apply IHdecs; assumption.
  Qed.
  
  Lemma substi_resp_wf_lab_def : forall x y l d,
      wf_lab_def (l, d) ->
      wf_lab_def (l, substi x y d).
  Proof using. destr on def; routine. Qed.
  Hint Resolve substi_resp_wf_lab_def.  

  Lemma substi_resp_all_wf_lab_def : forall x y (ds : defs),
      Forall wf_lab_def ds ->
      Forall wf_lab_def (substi x y ds).
  Proof using.
    induction on defs.
    - routine.
    - destruct d; routine.
  Qed.
  Hint Resolve substi_resp_all_wf_lab_def.

  Lemma substi_resp_wf_defs :forall x y ds,
      wf_defs ds ->
      wf_defs (substi x y ds).
  Proof using.
    induction on defs.
    - routine.
    - destruct d; routine.
      all:constructor; try rewrite same_ldom; routine.
  Qed.
  
End SubstProps.
Hint Resolve substi_resp_wf_defs.
Hint Resolve substi_resp_not_empty.
Hint Resolve substi_resp_lbinds.


Lemma free_all : forall x y T (G : env),
    y `notin` fv G ->
    binds x T G ->
    y `notin` fv T.
Proof. induction on env; routine. Qed.
Hint Resolve free_all.


(** This lemma is crazily difficult, as it requires direct reasoning of 
 * the relation between [substi] and [typ_obj], and it seems like lots of
 * complexity is embedded inside of this lemma.
 *)
Section SubstRules.
  Variable y : atom.
  Variable S : typ.

  Local Notation subst_for ctor :=
    (forall G t T,
        ctor G t T ->
        forall G1 G2 x,
          G = G2 ++ x ~ S ++ G1 ->
          uniq G ->
          x `notin` fv G1 ->
          substi x y G2 ++ G1 ⊢ trm_var y ⦂ substi x y S ->
          ctor (substi x y G2 ++ G1) (substi x y t) (substi x y T)).

  (** this tactic solves goal that can be proved directly from the context. *)
  Ltac finalize := solve [match goal with [H : _ |- _] => eapply H; eauto end].

  (** this tactic solves cases where requires variable lookup
    * in a weakened environment. *)
  Ltac solve_tac1 :=
    simpl; simpl_env;
    (eapply weaken_trm || eapply weaken_subtyp); try eassumption;
    simplify; trivial; solve_uniq.

  (** this tactic transforms goals that are the results of openning. *)
  Ltac closure :=
    try cofinite; fold_substi; fold_open_rec; open_substi_transform.

  Ltac comm_subst_open :=
    repeat match goal with
           | |- context[?f _ (subst_fvar _ _ _) (substi _ _ _)] =>
             lazymatch f with
             | context[open_rec] => idtac
             end;
             exrewrite <- subst_open_comm_all
           end.
  
  (** massage the context to a unified form. *)
  Ltac xform_ctx :=
    comm_subst_open;
    match goal with
    | |- context[?v ~ substi ?x ?y ?T ++ substi ?x ?y ?G] =>
      change (v ~ substi x y T ++ substi x y G)
        with (substi x y (v ~ T ++ G))
    | |- context[?v ~ (μ{ substi ?x ?y ?T }) ++ substi ?x ?y ?G] =>
      change (v ~ (μ{ substi x y T }) ++ substi x y G)
        with (substi x y (v ~ (μ{ T }) ++ G))
    end.

  (** same princicple as above. massage the context to a unified form. *)
  Ltac xform_rcd :=
    match goal with
    | |- context[decs_cons ?a (dec_trm ?T) (substi ?x ?y ?DS)] =>
      is_evar T;
      exvar typ ltac:(fun T' => unify T (substi x y T');
      replace (decs_cons a (dec_trm (substi x y T')) (substi x y DS))
        with (substi x y (decs_cons a (dec_trm T') DS)) by reflexivity)
    | |- context[decs_cons ?A (dec_typ ?T1 ?T2) (substi ?x ?y ?DS)] =>
      is_evar T1; is_evar T2;
      exvar typ ltac:(fun T1' => unify T1 (substi x y T1');
      exvar typ ltac:(fun T2' => unify T2 (substi x y T2');
      replace (decs_cons A (dec_typ (substi x y T1') (substi x y T2'))
                         (substi x y DS))
        with (substi x y (decs_cons A (dec_typ T1' T2') DS)) by reflexivity))
    end;
      rewrite <- substi_resp_rcd.

  (** this tactic solves the cases which will lead to weakening. *)
  Ltac solve_tac2 := solve [match goal with [H : _ |- _] => eapply H; eauto end;
                            solve_tac1].

  (** this tactic solves the cases which introduces closures. *)
  Ltac solve_tac3 := closure; reassoc 3 with 2; xform_ctx; solve_tac2.
  
  Hint Resolve get_to_binds.
  Lemma subst_trm_var : forall G1 G2 x0 y x T S,
      binds x0 T (G2 ++ x ~ S ++ G1) ->
      uniq (G2 ++ x ~ S ++ G1) ->
      x `notin` fv G1 ->
      substi x y G2 ++ G1 ⊢ trm_var y ⦂ substi x y S ->
      substi x y G2 ++ G1 ⊢ trm_var (subst_fvar x y x0) ⦂ substi x y T.
  Proof.
    intros. pose proof (binds_to_get _ _ H H0).
    repeat lazymatch goal with
           | H : get _ (_ ++ _) = _ |- _ =>
             apply get_uniq_app in H;
               [ | solve_uniq];
               destruct H; destruct_conjs
           end; tidy_up.
    - constructor. apply binds_app_2. apply binds_map_2.
      auto.
    - trivial.
    - exrewrite subst_fresh_typ.
      + auto.
      + eapply free_all;
          [| apply get_to_binds; eassumption].
        simpl. solve_notin.
  Qed.

  (** this tactic proves the goal where we need to show [substi] respects [lbinds]. *)
  Ltac boom_lbinds :=
    fold_substi;
    match goal with
    | |- lbinds _ (dec_trm (substi ?x ?y ?T)) _ =>
      change (dec_trm (substi x y T)) with (substi x y (dec_trm T))
    | |- lbinds _ (dec_typ (substi ?x ?y ?S) ?U) _ =>
      is_evar U;
      exvar typ ltac:(fun U' =>
      unify U (substi x y U');
      replace (dec_typ (substi x y S) (substi x y U'))
        with (substi x y (dec_typ S U')) by reflexivity)
    | |- lbinds _ (dec_typ ?S (substi ?x ?y ?U)) _ =>
      is_evar S;
      exvar typ ltac:(fun S' =>
      unify S (substi x y S');
      replace (dec_typ (substi x y S') (substi x y U))
        with (substi x y (dec_typ S' U)) by reflexivity)
    end; eauto.

  (** solves various kinds of goals. *)
  Ltac boom :=
    try match goal with
        | |- forall _, _ `notin` _ -> _ => solve_tac3
        | |- context[typ_obj] => fold_substi; try xform_rcd; finalize
        | _ =>
          fold_substi;
          repeat match goal with
                 | |- context[if ?x == ?y then ?z else ?x] =>
                   change (if x == y then z else x) with (subst_fvar y z x)
                 end;
          comm_subst_open;
          eauto 4; try finalize; boom_lbinds
        end;
    routine.

  (** preparation tactic. *)
  Ltac prep :=
    intros; subst; simpl; fold_substi;
    try (list_reasoning; repeat rewrite substi_resp_append');
    try exrewrite subst_open_comm_typ_dec_decs.

  Lemma subst_trm_gen : subst_for ty_trm
  with subst_def_gen : subst_for ty_def
  with subst_defs_gen : subst_for ty_defs
  with subst_subtyp_gen : subst_for subtyp.
  Proof.
    - clear subst_trm_gen.
      induction on ty_trm; prep;
        try solve [eapply subst_trm_var; eauto].
      par:try solve [typing undec; boom].
    - clear subst_def_gen.
      induction on ty_def; prep; constructor; boom.
    - clear subst_defs_gen.
      induction on ty_defs; prep; constructor; boom.
    - clear subst_subtyp_gen.
      induction on subtyp; prep.
      par:try solve [typing undec; boom].
      + typing undec. 
        instantiate (1 := substi x0 y T).
        change (substi x0 y G2 ++ G1 ⊢ substi x0 y (trm_var x)
                       ⦂ substi x0 y ({<decs_cons (A ∈ S0 ⋯ T) decs_nil >})).
        eapply subst_trm_gen; eauto.
      + typing undec.
        instantiate (1 := substi x0 y S0).
        change (substi x0 y G2 ++ G1 ⊢ substi x0 y (trm_var x)
                       ⦂ substi x0 y ({<decs_cons (A ∈ S0 ⋯ T) decs_nil >})).
        eapply subst_trm_gen; eauto.
  Qed.
  
  (** specialization. *)
  Section Specialization.
  
    Local Notation subst_for' ctor :=
      (forall G t T x,
          ctor (x ~ S ++ G) t T ->
          uniq (x ~ S ++ G) ->
          x `notin` fv G ->
          G ⊢ trm_var y ⦂ substi x y S ->
          ctor G (substi x y t) (substi x y T)).

    Ltac boom :=
      intros;
      match goal with
      | |- _ ?G _ (substi ?x ?y _) =>
        change G with (substi x y nil ++ G);
        (eapply subst_trm_gen || eapply subst_def_gen ||
         eapply subst_defs_gen || eapply subst_subtyp_gen); eauto
      end.
    
    Lemma subst_ty_trm : subst_for' ty_trm.
    Proof using. boom. Qed.

    Lemma subst_ty_def : subst_for' ty_def.
    Proof using. boom. Qed.
    
    Lemma subst_ty_defs : subst_for' ty_defs.
    Proof using. boom. Qed.

    Lemma subst_subtyp : subst_for' subtyp.
    Proof using. boom. Qed.
    
  End Specialization.
End SubstRules.

Section Renaming.

  Ltac prelude :=
    intros;
    try erewrite subst_intro_typ;
    try erewrite subst_intro_trm;
    try erewrite subst_intro_def;
    try erewrite subst_intro_defs;
    try erewrite subst_intro_dec;
    try erewrite subst_intro_decs.

  Ltac fin :=
    try (eapply subst_ty_trm || eapply subst_ty_defs);
    routine;
    try (simpl; fold_substi; fold_open_rec;
         erewrite subst_fresh_typ
         || erewrite subst_fresh_dec
         || erewrite subst_fresh_decs);
    routine.

  Ltac boom := prelude; fin.
  
  Lemma renaming_trm : forall G z t T U (x : var),
      uniq G ->
      z `notin` fv G `union` fv T `union` fv U `union` fv t ->
      z ~ U ++ G ⊢ open z t ⦂ open z T ->
      G ⊢ trm_var x ⦂ U ->
      G ⊢ open x t ⦂ open x T.
  Proof using. boom. Qed.

  Lemma renaming_defs : forall G z DS ds (x : var),
      uniq G ->
      z `notin` fv G `union` fv DS `union` fv ds ->
      z ~ (μ{ DS }) ++ G ⊩[open z ds ⦂ open z DS ] ->
      G ⊢ trm_var x ⦂ μ{ DS } ->
      G ⊩[open x ds ⦂ open x DS].
  Proof using. boom. Qed.

  Lemma renaming_subtyp : forall G z T1 T2 U (x : var),
      uniq G ->
      z `notin` fv G `union` fv T1 `union` fv T2 `union` fv U ->
      z ~ U ++ G ⊢ open z T1 <⦂ open z T2 ->
      G ⊢ trm_var x ⦂ U ->
      G ⊢ open x T1 <⦂ open x T2.
  Proof using.
    intros.
    erewrite subst_intro_typ.
    erewrite subst_intro_typ with (t := T2).
    eapply subst_subtyp.
    all:try eauto.
    routine.
    erewrite subst_fresh_typ; routine.
  Qed.
  
  Lemma renaming_fresh : forall L G T u U (x : var),
      uniq G ->
      (forall x, x `notin` L -> x ~ T ++ G ⊢ open x u ⦂ U) ->
      G ⊢ trm_var x ⦂ T ->
      G ⊢ open x u ⦂ U.
  Proof using. intros;
    let z := fresh in
    pick_fresh z; prelude;
      try instantiate (1 := z);
      try eexrewrite <- subst_fresh_typ;
      fin.
  Qed.

End Renaming.

Lemma open_subst_trm : forall x S G t U y,
    uniq G ->
    x `notin` fv G `union` fv S `union` fv U `union` fv t ->
    y `notin` fv G `union` fv S `union` fv U `union` fv t `union` singleton x ->
    x ~ S ++ G ⊢ open x t ⦂ open x U ->
    y ~ S ++ G ⊢ open y t ⦂ open y U.
Proof.
  intros.
  apply renaming_trm with (z := x) (U := S); routine.
  simpl_env.
  apply weaken_trm_gen. trivial.
Qed.

Lemma open_subst_subtyp : forall x S G U1 U2 y,
    uniq G ->
    x `notin` fv G `union` fv S `union` fv U1 `union` fv U2 ->
    y `notin` fv G `union` fv S `union` fv U1 `union` fv U2 `union` singleton x ->
    x ~ S ++ G ⊢ open x U1 <⦂ open x U2 ->
    y ~ S ++ G ⊢ open y U1 <⦂ open y U2.
Proof.
  intros.
  apply renaming_subtyp with (z := x) (U := S); routine.
  simpl_env.
  apply weaken_subtyp_gen. trivial.
Qed.

Lemma open_subst_defs : forall x y G ds DS,
    uniq G ->
    x `notin` fv G `union` fv ds `union` fv DS ->
    y `notin` fv G `union` fv ds `union` fv DS `union` singleton x ->
    x ~ μ{DS} ++ G ⊩[open x ds ⦂ open x DS] ->
    y ~ μ{DS} ++ G ⊩[open y ds ⦂ open y DS].
Proof.
  intros.
  apply renaming_defs with (z := x); routine.
  simpl_env.
  apply weaken_defs_gen. trivial.
Qed.