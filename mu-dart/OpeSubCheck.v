Set Implicit Arguments.
Require Import Definitions.
Require Import PreciseTyping.
Require Import OpeSub.
Require Import Helpers.
Require Import TypeCheckSpecs.
Require Import Weakening.
Require Import TypingHelpers.
Require Import OperationProperties.

Local Hint Constructors convertible ope_sub ope_decs.

Lemma ope_decs_is_subtyp : forall G DS1 DS2,
    ope_decs DS1 DS2 ->
    G ⊢ typ_rcd DS1 <⦂ typ_rcd DS2.
Proof.
  induction on ope_decs; intros; auto.
  - list_reasoning. apply decs_append_resp_subtyp; trivial.
  - list_reasoning.
    eapply subtyp_trans. apply subtyp_drop1; routine.
    eassumption.
Qed.

Lemma ope_decs_open_rec_invar : forall n x DS1 DS2,
    ope_decs DS1 DS2 ->
    ope_decs (open_rec n x DS1) (open_rec n x DS2).
Proof.
  induction on ope_decs; intros; routine.
Qed.

Lemma ope_decs_is_convertible : forall G x DS1 DS2,
    ope_decs DS1 DS2 ->
    convertible G x (typ_obj DS1) (typ_obj DS2).
Proof.
  intros. apply cv_mu.
  apply ope_decs_is_subtyp.
  apply ope_decs_open_rec_invar.
  trivial.
Qed.

Goal forall x DS1 DS2,
    ope_sub (x ~ typ_obj DS1) (x ~ typ_obj (append' DS1 DS2)).
Proof.
  intros. apply os_keep; trivial.
  apply cv_mu. repeat rewrite open_rec_append'.
  apply subtyp_drop2_gen.
Qed.

Fixpoint remove_typ_lab (DS : decs) (A : typ_label) : decs :=
  match DS with
  | decs_nil => decs_nil
  | decs_cons (dec_typ B T U) DS' =>
    if A == B then remove_typ_lab DS' A else
      decs_cons (dec_typ B T U) (remove_typ_lab DS' A)
  | decs_cons (dec_trm a T) DS' =>
    decs_cons (dec_trm a T) (remove_typ_lab DS' A)
  end.

Inductive expo_mu : decs -> var -> typ_label -> (* inputs *)
                    typ -> decs -> (* outputs *)
                    Prop :=
| expo_top : forall x (A : typ_label) (DS : decs),
    A `lnotin` ldom DS ->
    expo_mu DS x A typ_top DS
| expo_sel : forall x y L (A B : typ_label) (DS : decs),
    lbinds A (dec_typ A L (typ_sel y B)) (open x DS) ->
    avar_f x <> y ->
    expo_mu DS x A (typ_sel y B) (remove_typ_lab DS A)
| expo_stop : forall x (A : typ_label) L T (DS : decs),
    lbinds A (dec_typ A L T) (open x DS) ->
    ~is_sel T ->
    expo_mu DS x A T (remove_typ_lab DS A)
| expo_recur : forall (x : var) (A B : typ_label) L (DS : decs) T DS',
    lbinds A (dec_typ A L (typ_sel x B)) (open x DS) ->
    expo_mu (remove_typ_lab DS A) x B T DS' ->
    expo_mu DS x A T DS'.
Local Hint Constructors expo_mu.

Lemma remove_typ_lab_gives_ope : forall DS A,
    ope_decs DS (remove_typ_lab DS A).
Proof.
  induction DS; intros; routine.
  destruct d; routine.
Qed.
Local Hint Resolve remove_typ_lab_gives_ope.

Local Hint Resolve ope_decs_refl ope_decs_trans.

Lemma expo_mu_gives_ope_decs : forall DS x A T DS',
    expo_mu DS x A T DS' ->
    ope_decs DS DS'.
Proof.
  induction on expo_mu; eroutine.
Qed.
Local Hint Resolve expo_mu_gives_ope_decs.

Lemma open_rec_remove_tys_lab_comm : forall DS n x A,
    open_rec n x (remove_typ_lab DS A) = remove_typ_lab (open_rec n x DS) A.
Proof.
  induction DS; routine.
  destruct d; routine.
Qed.

Lemma expo_mu_sound : forall DS x A T DS',
    expo_mu DS x A T DS' ->
    forall G,
      G ⊢ trm_var x ⦂ typ_rcd (open x DS) ->
      G ⊢ typ_sel x A <⦂ T.
Proof.
  dep induction on expo_mu; routine.
  1-2:eapply decs_bind_typ in H; try eassumption;
    eauto.
  eapply decs_bind_typ in H; try eassumption.
  eapply subtyp_trans.
  - eapply subtyp_sel2. eassumption.
  - apply IHexpo_mu; trivial.
    eapply ty_sub. eapply H0.
    fold_cls. rewrite open_rec_remove_tys_lab_comm.
    apply ope_decs_is_subtyp.
    auto.
Qed.

Theorem expo_mu_effect : forall DS x A T DS',
    expo_mu DS x A T DS' ->
    ~is_sel T \/ (forall y B, T = typ_sel y B -> avar_f x <> y).
Proof.
  induction on expo_mu; intros; auto.
  right. routine.
Qed.

Theorem ope_decs_preserves_lc : forall DS DS',
    ope_decs DS DS' ->
    forall n,
      lc_at n DS ->
      lc_at n DS'.
Proof.
  induction on ope_decs; routine.
Qed.

Theorem expo_mu_preserves_lc : forall DS x A T DS',
    expo_mu DS x A T DS' ->
    lc_at 1 DS ->
    lc T.
Proof.
  induction on expo_mu; intros; auto.
  - constructor.
  - apply lc_at_binds_decs with (n := 0) in H; auto.
    routine.
  - apply lc_at_binds_decs with (n := 0) in H; auto.
    routine.
  - apply IHexpo_mu. eapply ope_decs_preserves_lc.
    2:eassumption.
    auto.
Qed.

Theorem ope_decs_shrinks_fv : forall DS DS',
    ope_decs DS DS' ->
    fv DS' [<=] fv DS.
Proof.
  induction on ope_decs; set solve.
Qed.

Theorem expo_mu_fv_bound : forall DS x A T DS',
    expo_mu DS x A T DS' ->
    fv T [<=] singleton x `union` fv DS.
Proof.
  induction on expo_mu; intros; auto.
  - set solve.
  - apply fv_decs_shrinks in H.
    pose proof (fv_open_decs DS x 0).
    set solve.
  - apply fv_decs_shrinks in H.
    pose proof (fv_open_decs DS x 0).
    set solve.
  - pose proof (remove_typ_lab_gives_ope DS A).
    apply ope_decs_shrinks_fv in H0.
    fsetdec.
Qed.

Theorem exposure_mu_preserves_wf : forall (G : env) x A DS T DS',
    expo_mu DS x A T DS' ->
    fv DS [<=] dom G -> x `in` dom G -> lc_at 1 DS -> 
    fv T [<=] dom G /\ lc T.
Proof.
  intros. split.
  - apply expo_mu_fv_bound in H. fsetdec.
  - eapply expo_mu_preserves_lc. eauto. trivial.
Qed.

Inductive revealing : env -> typ -> (* inputs *)
                      env -> typ -> (* outputs *)
                      Prop :=
| rv_stop : forall G T,
    ~is_sel T ->
    revealing G T G T
| rv_top : forall G T,
    revealing G T nil typ_top
| rv_bot : forall G1 G2 G1' T x A,
    revealing G1 T G1' typ_bot ->
    revealing (G2 ++ x ~ T ++ G1) (typ_sel x A) nil typ_bot
| rv_rcd : forall G1 G2 T G1' G1'' x (A : typ_label) (DS : decs) L U U',
    revealing G1 T G1' (typ_rcd DS) ->
    lbinds A (dec_typ A L U) DS ->
    revealing G1' U G1'' U' ->
    revealing (G2 ++ x ~ T ++ G1) (typ_sel x A) G1'' U'
| rv_mu1 : forall G1 G2 G1' T (x : var) (A : typ_label) (DS DS' : decs) U,
    revealing G1 T G1' (typ_obj DS) ->
    lIn A (ldom DS) ->
    expo_mu DS x A U DS' ->
    ~is_sel U ->
    revealing (G2 ++ x ~ T ++ G1) (typ_sel x A) (x ~ (typ_obj DS') ++ G1') U
| rv_mu2 : forall G1 G2 G1' G1'' T (x y : var) (A B : typ_label) (DS DS' : decs) U,
    revealing G1 T G1' (typ_obj DS) ->
    lIn A (ldom DS) ->
    expo_mu DS x A (typ_sel y B) DS' ->
    revealing G1' (typ_sel y B) G1'' U ->
    revealing (G2 ++ x ~ T ++ G1) (typ_sel x A) G1'' U.
Local Hint Constructors revealing.

Local Hint Resolve ope_sub_refl ope_sub_nil_any ope_sub_trans ope_sub_app_r.

Local Ltac by_weakening :=
  once ((reassoc 4 with 3 + reassoc 5 with 4); apply weaken_subtyp; eassumption).

Theorem revealing_sound : forall G T G' U,
    revealing G T G' U ->
    G ⊢ T <⦂ U /\ ~is_sel U /\ ope_sub G' G.
Proof.
  induction on revealing.
  all:try lazymatch goal with
          | |- subtyp (?l2 ++ ?x ~ ?p ++ ?l1) _ _ /\ _ =>
            pose proof (binds_for_sure l2 l1 x p)
          end.
  all:try solve [repeat split; auto].
  all:destruct_conjs.
  - repeat split; auto.
    eapply subtyp_sel2.
    eapply ty_sub. eauto.
    eapply subtyp_trans.
    + by_weakening.
    + instantiate (1 := typ_top). trivial.
  - repeat split; auto.
    + eapply subtyp_sel2.
      eapply ty_sub. eapply decs_bind_typ; try eassumption.
      * eapply ty_sub. eauto. by_weakening.
      * eapply subtyp_typ. eauto.
        eapply ope_narrow_subtyp_gen in H1; [| eassumption].
        by_weakening.
    + eauto.
  - repeat split; auto.
    + apply weaken_subtyp.
      eapply expo_mu_sound; try eassumption.
      apply ty_mu_elim. eapply ty_sub. eauto.
      apply weaken_subtyp. trivial.
    + apply ope_sub_app_r. apply os_keep; trivial.
      eapply cv_trans.
      * apply cv_sub. eassumption.
      * apply cv_mu. apply expo_mu_gives_ope_decs in H0.
        apply ope_decs_is_subtyp.
        apply ope_decs_open_rec_invar.
        trivial.
  - repeat split; auto.
    + apply weaken_subtyp. eapply subtyp_trans.
      * eapply expo_mu_sound; try eassumption.
        apply ty_mu_elim. eapply ty_sub. eauto.
        apply weaken_subtyp. trivial.
      * apply weaken_subtyp. eapply ope_narrow_subtyp_gen; eauto.
    + eauto.
Qed.

Ltac wf_env :=
  lazymatch goal with
  | H : wf_env (_ ++ _) |- _ => apply wf_deapp in H; invert H; subst
  end.    

Ltac fv_next := etransitivity; [eassumption |].
Ltac fv_shrink := etransitivity; [ | apply fv_deapp].
Ltac fv_solve := repeat fv_next; repeat fv_shrink; auto.

Local Hint Resolve wf_nil fv_deapp.
Theorem revealing_preserves_wf : forall G T G' U,
    revealing G T G' U ->
    wf_env G ->
    fv T [<=] dom G -> lc T ->
    wf_env G' /\ fv G' [<=] fv G /\ fv U [<=] dom G' /\ lc U.
Proof.
  induction on revealing; intros.
  1-3:simpl; repeat split; set solve.
  - wf_env.
    destruct IHrevealing1; auto.
    pose proof H.
    apply lc_at_binds_decs with (n := 0) in H; [ | routine].
    apply fv_decs_shrinks in H5.
    pose proof (revealing_sound H3_). destruct_conjs.
    destruct IHrevealing2; auto.
    + simpl in *. auto.
    + routine.
    + destruct_conjs. repeat (split; trivial).
      fv_solve.
      
  - wf_env. destruct IHrevealing; trivial.
    destruct_conjs. pose proof (fv_open_decs DS x 0).
    pose proof H0.
    apply exposure_mu_preserves_wf with (G := (x ~ μ{DS} ++ G1')) in H0.
    3:simpl; auto. 3:routine.
    destruct_conjs. repeat (split; trivial).
    + constructor; trivial.
      * apply revealing_sound in H3.
        destruct_conjs. apply ope_sub_dom in H18.
        apply expo_mu_gives_ope_decs in H15.
        apply ope_decs_shrinks_fv in H15.
        simpl in *. fsetdec.
      * apply expo_mu_gives_ope_decs in H15.
        apply ope_decs_shrinks_fv in H15.
        simpl in *. fsetdec.
      * apply expo_mu_gives_ope_decs in H15.
        apply ope_decs_preserves_lc with (n := 1) in H15.
        all:routine.
    + apply expo_mu_gives_ope_decs in H15.
      apply ope_decs_shrinks_fv in H15.
      assert (fv (x ~ typ_obj DS' ++ G1') [=] singleton x `union` fv G1'). {
        simpl. set simpl. fsetdec.
      }
      rewrite H17. apply union_conj.
      split; [set solve |].
      fv_solve.
    + simpl in *. fsetdec.

  - wf_env. destruct IHrevealing1; auto. destruct_conjs.
    pose proof H0. apply expo_mu_effect in H0.
    destruct H0; [simpl in *; contradiction |].
    specialize (H0 _ _ eq_refl).
    apply exposure_mu_preserves_wf with (G := (x ~ T ++ G1')) in H12; auto.
    3,4:routine.
    destruct_conjs. destruct IHrevealing2; auto.
    + simpl in *.
      eapply exclude_add; try eassumption. congruence.
    + destruct_conjs. repeat (split; trivial).
      fv_solve.
    + simpl in *. fsetdec.
Qed.

