Set Implicit Arguments.
Require Import Definitions.
Require Import TypeCheckSpecs.
Require Import Helpers.
Require Import TypingHelpers.
Require Import SubtypingMeasures.

Require Import ListRelations.

(** model of how exposure should work for a mu type
  *
  * this relation (expo_mu As x A DS Bs T) represents 
  *  that x.A can expose to T, having Bs label remains,
  *    if we are allowed to look up labels in As.
  * 
  * In typing context, we expect DS is associated with x.
  *)
Inductive expo_mu : list typ_label -> avar -> typ_label -> decs -> (* inputs *)
                    list typ_label -> typ -> (* outputs *)
                    Prop :=
| expo_top1 : forall As x (A : typ_label) (DS : decs),
    A `lnotin` ldom DS ->
    expo_mu As x A DS As typ_top
| expo_top2 : forall As x (A B : typ_label) (DS : decs),
    ~In A As ->
    expo_mu As x A DS As typ_top
| expo_sel : forall As x y L (A B : typ_label) (DS : decs),
    In A As ->
    lbinds A (dec_typ A L (typ_sel y B)) DS ->
    x <> y ->
    expo_mu As x A DS (In_remove A As) (typ_sel y B)
| expo_stop : forall As x (A : typ_label) L T (DS : decs),
    In A As ->
    lbinds A (dec_typ A L T) DS ->
    ~is_sel T ->
    expo_mu As x A DS (In_remove A As) T
| expo_recur : forall As x (A B : typ_label) L (DS : decs) Bs T,
    In A As ->
    lbinds A (dec_typ A L (typ_sel x B)) DS ->
    expo_mu (In_remove A As) x B DS Bs T ->
    expo_mu As x A DS Bs T.
Local Hint Constructors expo_mu.

(** model of how imposure should work for a mu type
 * 
 * it's a dual for [expo_mu].
 *)
Inductive impo_mu : list typ_label -> avar -> typ_label -> decs -> (* inputs *)
                    list typ_label -> typ -> (* outputs *)
                    Prop :=
| impo_bot1 : forall As x (A : typ_label) (DS : decs),
    A `lnotin` ldom DS ->
    impo_mu As x A DS As typ_bot
| impo_bot2 : forall As x (A B : typ_label) (DS : decs),
    ~In A As ->
    impo_mu As x A DS As typ_bot
| impo_sel : forall As x y U (A B : typ_label) (DS : decs),
    In A As ->
    lbinds A (dec_typ A (typ_sel y B) U) DS ->
    x <> y ->
    impo_mu As x A DS (In_remove A As) (typ_sel y B)
| impo_stop : forall As x (A : typ_label) U T (DS : decs),
    In A As ->
    lbinds A (dec_typ A T U) DS ->
    ~is_sel T ->
    impo_mu As x A DS (In_remove A As) T
| impo_recur : forall As x (A B : typ_label) U (DS : decs) Bs T,
    In A As ->
    lbinds A (dec_typ A (typ_sel x B) U) DS ->
    impo_mu (In_remove A As) x B DS Bs T ->
    impo_mu As x A DS Bs T.
Local Hint Constructors impo_mu.

Local Hint Resolve remove_length.
Local Hint Extern 2 (_ <= _) => apply remove_length; assumption.

(** NOW, let's actually define this mu exposure function. *)

Equations(noind) expo_mu_func (As : list typ_label) (x : avar)
         (A : typ_label) (DS : decs) :
  (list typ_label * typ) by wf (length As) lt :=
  {
    expo_mu_func As x A DS with In_dec A As => {
    | left _ with decs_typ_lookup A DS => {
      | None => (As, typ_top);
      | Some (pair T1 T2) with is_sel_dec T2 => {
        | inleft (existT _ y (exist _ B _)) with x == y => {
          | left _ => expo_mu_func (In_remove A As) x B DS;
          | right _ => (In_remove A As, typ_sel y B)
          };
        | inright _ => (In_remove A As, T2)
        }
      };
    | right _ => (As, typ_top)
    }
  }.

Arguments expo_mu_func As x A DS : simpl never.

Ltac expo_mu_func_hyp H :=
  simp expo_mu_func in H;
  lazymatch type of H with
  | context [In_dec ?A ?As] =>
    destruct (In_dec A As) eqn:?; tidy_up;
    try lazymatch type of H with
        | context [decs_typ_lookup ?A ?DS] =>
          destruct (decs_typ_lookup A DS) eqn:?; tidy_up;
          try lazymatch type of H with
              | context [is_sel_dec ?T2] =>
                destruct (is_sel_dec T2) eqn:?; tidy_up;
                try lazymatch type of H with
                    | context [?x == ?y] =>
                      destruct (x == y) eqn:?; tidy_up
                    end
              end
        end
  end.

Ltac expo_mu_func_g :=
  simp expo_mu_func;
  lazymatch goal with
  | |- context [In_dec ?A ?As] =>
    destruct (In_dec A As) eqn:?; tidy_up;
    try lazymatch goal with
        | |- context [decs_typ_lookup ?A ?DS] =>
          destruct (decs_typ_lookup A DS) eqn:?; tidy_up;
          try lazymatch goal with
              | |- context [is_sel_dec ?T2] =>
                destruct (is_sel_dec T2) eqn:?; tidy_up;
                try lazymatch goal with
                    | |- context [?x == ?y] =>
                      destruct (x == y) eqn:?; tidy_up
                    end
              end
        end
  end.

Tactic Notation "expo_mu_func" "in" hyp(H) := expo_mu_func_hyp H.
Tactic Notation "expo_mu_func" := expo_mu_func_g.

Section ExposureMuProperties.
  
  Local Hint Resolve ope_list_refl ope_list_trans In_remove_ope_list.

  Lemma expo_mu_gives_ope : forall As x A DS Bs T,
      expo_mu As x A DS Bs T ->
      ope_list Bs As.
  Proof. induction on expo_mu; eauto. Qed.

  Local Hint Resolve decs_typ_lookup_sound1 decs_typ_lookup_sound2.
  
  Program Fixpoint expo_mu_func_resp_spec As {measure (length As)} : forall x A DS Bs T,
      expo_mu_func As x A DS = (Bs, T) ->
      expo_mu As x A DS Bs T := _.
  Next Obligation. expo_mu_func in H; eauto. Qed.
  
  Lemma expo_mu_func_gives_ope : forall As x A DS Bs T,
      expo_mu_func As x A DS = (Bs, T) ->
      ope_list Bs As.
  Proof.
    intros. apply expo_mu_func_resp_spec in H.
    eapply expo_mu_gives_ope; eassumption.
  Qed.
  
  (** at last, we end with proving the model is intended. *)
  Lemma expo_mu_is_correct : forall As (x : var) A DS Bs T,
      expo_mu As x A DS Bs T ->
      forall G,
        G ⊢ trm_var x ⦂ {< DS >} ->
        G ⊢ typ_sel x A <⦂ T.
  Proof.
    dep induction on expo_mu; intros; auto.    
    1-2:eapply decs_bind_typ in H2; try eassumption;
      eauto.
    eapply decs_bind_typ in H1; try eassumption;
      eauto.
  Qed.

  Theorem expo_mu_effect : forall As x A DS Bs T,
      expo_mu As x A DS Bs T ->
      ~is_sel T \/ (forall y B, T = typ_sel y B -> x <> y).
  Proof.
    induction on expo_mu; intros; auto.
    right. routine.
  Qed.

  Theorem expo_mu_lbinds : forall As x A DS Bs T,
      expo_mu As x A DS Bs T ->
      uniq_dom_decs DS ->
      uniq_dom_typ T.
  Proof.
    induction on expo_mu; trivial; intros;
      try constructor.
    eapply uniq_dom_decs_lbinds_typ; eauto.
  Qed.

  Theorem expo_mu_preserves_lc : forall As x A DS Bs T,
      expo_mu As x A DS Bs T ->
      lc DS ->
      lc T.
  Proof.
    induction on expo_mu; intros; auto.
    1-2:constructor.
    - apply lc_at_binds_decs with (n := 0) in H0; auto.
      routine.
    - apply lc_at_binds_decs with (n := 0) in H0; auto.
      routine.
  Qed.

  Theorem expo_mu_fv_bound : forall As x A DS Bs T,
      expo_mu As x A DS Bs T ->
      fv T [<=] fv DS.
  Proof.
    induction on expo_mu; intros; auto.
    1-2:set solve.
    - apply fv_decs_shrinks in H0. set solve.
    - apply fv_decs_shrinks in H0. set solve.
  Qed.
  
  Theorem exposure_mu_preserves_wf : forall (G : env) As x A DS Bs T,
      expo_mu As x A DS Bs T ->
      fv DS [<=] dom G -> lc DS ->
      fv T [<=] dom G /\ lc T.
  Proof.
    induction on expo_mu; intros; auto.
    1-2:simpl; split; set solve.
    all:pose proof H1 as Hb.
    all:apply lc_at_binds_decs with (n := 0) in H1; auto.
    all:apply fv_decs_shrinks in Hb; auto.
    all:simpl in *; split; routine.
  Qed.

  Program Fixpoint expo_mu_func_return_binds_or_top As {measure (length As)} :
    forall x A DS Bs T,
      expo_mu_func As x A DS = (Bs, T) ->
      T = typ_top \/ exists (B : typ_label) L, lbinds B (dec_typ B L T) DS := _.
  Next Obligation.
    expo_mu_func in H; eauto.
    eapply expo_mu_func_return_binds_or_top; eauto.
    pose proof (remove_length _ _ i). lia.
  Qed.

  Theorem expo_mu_lnotin : forall As x A DS Bs U,
      expo_mu As x A DS Bs U ->
      A `lnotin` ldom DS ->
      As = Bs /\ U = typ_top.
  Proof.
    induction on expo_mu; auto; intros.
    all:lazymatch goal with
        | H : lbinds _ _ _ |- _ =>
          apply LabelAssocList.binds_In in H; contradiction
        end.
  Qed.

  Theorem expo_mu_shrinks_measure : forall As x A DS Bs U,
      expo_mu As x A DS Bs U ->
      In A As ->
      lIn A (ldom DS) ->
      pc_mu_measure DS As > pc_mu_measure DS Bs + typ_struct_measure U.
  Proof.
    dep induction on expo_mu; intros; simpl in *;
      try contradiction.
    all:try solve [edestruct pc_mu_measure_lbinds; eauto; simpl in *; lia].

    invert H5; subst.
    all:try solve [edestruct pc_mu_measure_lbinds; eauto; simpl in *; lia].
    - edestruct pc_mu_measure_lbinds; eauto.
      edestruct pc_mu_measure_lbinds. 1:exact H0.
      eauto. lia.
    - apply LabelAssocList.binds_In in H4.
      specialize (IHexpo_mu H3 H4).
      edestruct pc_mu_measure_lbinds. 1:exact H0.
      eauto. lia.
    - apply LabelAssocList.binds_In in H4.
      specialize (IHexpo_mu H3 H4).
      edestruct pc_mu_measure_lbinds. 1:exact H0.
      eauto. lia.
  Qed.
  
End ExposureMuProperties.

Equations(noind) impo_mu_func (As : list typ_label) (x : avar)
         (A : typ_label) (DS : decs) :
  (list typ_label * typ) by wf (length As) lt :=
  {
    impo_mu_func As x A DS with In_dec A As => {
    | left _ with decs_typ_lookup A DS => {
      | None => (As, typ_bot);
      | Some (pair T1 T2) with is_sel_dec T1 => {
        | inleft (existT _ y (exist _ B _)) with x == y => {
          | left _ => impo_mu_func (In_remove A As) x B DS;
          | right _ => (In_remove A As, typ_sel y B)
          };
        | inright _ => (In_remove A As, T1)
        }
      };
    | right _ => (As, typ_bot)
    }
  }. 
Arguments impo_mu_func As x A DS : simpl never.

Ltac impo_mu_func_hyp H :=
  simp impo_mu_func in H;
  lazymatch type of H with
  | context [In_dec ?A ?As] =>
    destruct (In_dec A As) eqn:?; tidy_up;
    try lazymatch type of H with
        | context [decs_typ_lookup ?A ?DS] =>
          destruct (decs_typ_lookup A DS) eqn:?; tidy_up;
          try lazymatch type of H with
              | context [is_sel_dec ?T2] =>
                destruct (is_sel_dec T2) eqn:?; tidy_up;
                try lazymatch type of H with
                    | context [?x == ?y] =>
                      destruct (x == y) eqn:?; tidy_up
                    end
              end
        end
  end.

Ltac impo_mu_func_g :=
  simp impo_mu_func;
  lazymatch goal with
  | |- context [In_dec ?A ?As] =>
    destruct (In_dec A As) eqn:?; tidy_up;
    try lazymatch goal with
        | |- context [decs_typ_lookup ?A ?DS] =>
          destruct (decs_typ_lookup A DS) eqn:?; tidy_up;
          try lazymatch goal with
              | |- context [is_sel_dec ?T2] =>
                destruct (is_sel_dec T2) eqn:?; tidy_up;
                try lazymatch goal with
                    | |- context [?x == ?y] =>
                      destruct (x == y) eqn:?; tidy_up
                    end
              end
        end
  end.

Tactic Notation "impo_mu_func" "in" hyp(H) := impo_mu_func_hyp H.
Tactic Notation "impo_mu_func" := impo_mu_func_g.

Section ImposureMuProperties.
  
  Local Hint Resolve ope_list_refl ope_list_trans In_remove_ope_list.

  Lemma impo_mu_gives_ope : forall As x A DS Bs T,
      impo_mu As x A DS Bs T ->
      ope_list Bs As.
  Proof. induction on impo_mu; eauto. Qed.

  Local Hint Resolve decs_typ_lookup_sound1 decs_typ_lookup_sound2.
  
  Program Fixpoint impo_mu_func_resp_spec As {measure (length As)} : forall x A DS Bs T,
      impo_mu_func As x A DS = (Bs, T) ->
      impo_mu As x A DS Bs T := _.
  Next Obligation. impo_mu_func in H; eauto. Qed.
  
  Lemma impo_mu_func_gives_ope : forall As x A DS Bs T,
      impo_mu_func As x A DS = (Bs, T) ->
      ope_list Bs As.
  Proof.
    intros. apply impo_mu_func_resp_spec in H.
    eapply impo_mu_gives_ope; eassumption.
  Qed.
  
  (** at last, we end with proving the model is intended. *)
  Lemma impo_mu_is_correct : forall As (x : var) A DS Bs T,
      impo_mu As x A DS Bs T ->
      forall G,
        G ⊢ trm_var x ⦂ {< DS >} ->
        G ⊢ T <⦂ typ_sel x A.
  Proof.
    dep induction on impo_mu; intros; auto.    
    1-2:eapply decs_bind_typ in H2; try eassumption;
      eauto.
    eapply decs_bind_typ in H1; try eassumption;
      eauto.
  Qed.

  Theorem impo_mu_effect : forall As x A DS Bs T,
      impo_mu As x A DS Bs T ->
      ~is_sel T \/ (forall y B, T = typ_sel y B -> x <> y).
  Proof.
    induction on impo_mu; intros; auto.
    right. routine.
  Qed.

  Theorem impo_mu_lbinds : forall As x A DS Bs T,
      impo_mu As x A DS Bs T ->
      uniq_dom_decs DS ->
      uniq_dom_typ T.
  Proof.
    induction on impo_mu; trivial; intros;
      try constructor.
    apply uniq_dom_decs_lbinds_typ in H0; trivial.
    apply H0.
  Qed.

  Theorem impo_mu_preserves_lc : forall As x A DS Bs T,
      impo_mu As x A DS Bs T ->
      lc DS ->
      lc T.
  Proof.
    induction on impo_mu; intros; auto.
    1-2:constructor.
    - apply lc_at_binds_decs with (n := 0) in H0; auto.
      routine.
    - apply lc_at_binds_decs with (n := 0) in H0; auto.
      routine.
  Qed.

  Theorem impo_mu_fv_bound : forall As x A DS Bs T,
      impo_mu As x A DS Bs T ->
      fv T [<=] fv DS.
  Proof.
    induction on impo_mu; intros; auto.
    1-2:set solve.
    - apply fv_decs_shrinks in H0. set solve.
    - apply fv_decs_shrinks in H0. set solve.
  Qed.
  
  Theorem imposure_mu_preserves_wf : forall (G : env) As x A DS Bs T,
      impo_mu As x A DS Bs T ->
      fv DS [<=] dom G -> lc DS ->
      fv T [<=] dom G /\ lc T.
  Proof.
    induction on impo_mu; intros; auto.
    1-2:simpl; split; set solve.
    all:pose proof H1 as Hb.
    all:apply lc_at_binds_decs with (n := 0) in H1; auto.
    all:apply fv_decs_shrinks in Hb; auto.
    all:simpl in *; split; routine.
  Qed.

  Program Fixpoint impo_mu_func_return_binds_or_top As {measure (length As)} :
    forall x A DS Bs T,
      impo_mu_func As x A DS = (Bs, T) ->
      T = typ_bot \/ exists (B : typ_label) U, lbinds B (dec_typ B T U) DS := _.
  Next Obligation.
    impo_mu_func in H; eauto.
    eapply impo_mu_func_return_binds_or_top; eauto.
    pose proof (remove_length _ _ i). lia.
  Qed.

  Theorem impo_mu_lnotin : forall As x A DS Bs U,
      impo_mu As x A DS Bs U ->
      A `lnotin` ldom DS ->
      As = Bs /\ U = typ_bot.
  Proof.
    induction on impo_mu; auto; intros.
    all:lazymatch goal with
        | H : lbinds _ _ _ |- _ =>
          apply LabelAssocList.binds_In in H; contradiction
        end.
  Qed.

  Theorem impo_mu_shrinks_measure : forall As x A DS Bs U,
      impo_mu As x A DS Bs U ->
      In A As ->
      lIn A (ldom DS) ->
      pc_mu_measure DS As > pc_mu_measure DS Bs + typ_struct_measure U.
  Proof.
    dep induction on impo_mu; intros; simpl in *;
      try contradiction.
    all:try solve [edestruct pc_mu_measure_lbinds; eauto; simpl in *; lia].

    invert H5; subst.
    all:try solve [edestruct pc_mu_measure_lbinds; eauto; simpl in *; lia].
    - edestruct pc_mu_measure_lbinds; eauto.
      edestruct pc_mu_measure_lbinds. 1:exact H0.
      eauto. lia.
    - apply LabelAssocList.binds_In in H4.
      specialize (IHimpo_mu H3 H4).
      edestruct pc_mu_measure_lbinds. 1:exact H0.
      eauto. lia.
    - apply LabelAssocList.binds_In in H4.
      specialize (IHimpo_mu H3 H4).
      edestruct pc_mu_measure_lbinds. 1:exact H0.
      eauto. lia.
  Qed.
  
End ImposureMuProperties.
