Set Implicit Arguments.
Require Import Definitions.
Require Import TypeCheckSpecs.
Require Import Helpers.
Require Import TypingHelpers.
Require Import ObjectTraversal.
Require Import Measures.
Require Import Weakening.
Require Import OperationProperties.
Require Import Exposure.

(** models of imposure operation *)
Inductive imposure : env -> typ -> typ -> Prop :=
| imp_stop : forall G T,
    ~is_sel T ->
    imposure G T T
| imp_bot : forall G T,
    imposure G T typ_bot
| imp_top : forall G1 G2 T x A,
    exposure G1 T typ_bot ->
    imposure (G2 ++ x ~ T ++ G1) (typ_sel x A) typ_top
| imp_rcd : forall G1 G2 T x (A : typ_label) (DS : decs) L U L',
    exposure G1 T (typ_rcd DS) ->
    lbinds A (dec_typ A L U) DS ->
    imposure G1 L L' ->
    imposure (G2 ++ x ~ T ++ G1) (typ_sel x A) L'
| imp_mu : forall G1 G2 T (x : var) (A : typ_label) (DS : decs) l' L L',
    exposure G1 T (typ_obj DS) ->
    impo_mu (collect_ty_labs DS) x A (open x DS) l' L ->
    imposure G1 L L' ->
    imposure (G2 ++ x ~ T ++ G1) (typ_sel x A) L'.
Local Hint Constructors imposure.

Section ImposureProperties.

  Local Ltac by_weakening :=
    reassoc 4 with 3; apply weaken_subtyp; eassumption.

  Theorem imposure_sound : forall G T U,
      imposure G T U ->
      G ⊢ U <⦂ T.
  Proof.
    induction on imposure; trivial.
    all:lazymatch goal with
        | |- subtyp (?l2 ++ ?x ~ ?p ++ ?l1) _ _ =>
          pose proof (binds_for_sure l2 l1 x p)
        end.
    - eapply subtyp_sel1.
      eapply ty_sub. eauto.
      eapply subtyp_trans.
      + apply exposure_sound in H. by_weakening.
      + instantiate (1 := typ_top). trivial.
    - eapply subtyp_sel1.
      eapply ty_sub. eapply decs_bind_typ; try eassumption.
      + eapply ty_sub. eauto.
        apply exposure_sound in H.
        by_weakening.
      + eapply subtyp_typ. eauto. eauto.
    - eapply subtyp_trans. by_weakening.
      eapply impo_mu_is_correct; try eassumption.
      apply ty_mu_elim. eapply ty_sub. eauto.
      apply exposure_sound in H.
      by_weakening.
  Qed.

  Theorem imposure_not_sel : forall G T U,
      imposure G T U ->
      ~is_sel U.
  Proof. induction on imposure; simpl; auto. Qed.    

  Local Ltac wf_env :=
    lazymatch goal with
    | H : wf_env (_ ++ _) |- _ => apply wf_deapp in H; invert H; subst
    end.
  
  Theorem imposure_preserves_wf : forall G T U,
      imposure G T U ->
      wf_env G ->
      fv T [<=] dom G -> lc T ->
      fv U [<=] dom G /\ lc U.
  Proof.
    induction on imposure; intros; auto.
    1-2:simpl; split; set solve.
    - wf_env.
      apply exposure_preserves_wf in H; trivial.
      destruct H. pose proof H0.
      apply lc_at_binds_decs with (n := 0) in H0; [ | routine].
      apply fv_decs_shrinks in H6.
      simpl in *. 
      destruct IHimposure; auto. routine.
      split; trivial.
      set simpl. simpl. fsetdec.
    - wf_env. apply exposure_preserves_wf in H; trivial.
      destruct H. pose proof H0.
      apply impo_mu_effect in H6.
      apply imposure_mu_preserves_wf with (G := x ~ T ++ G1) in H0; auto.
      destruct (is_sel_dec L); destruct_conjs.
      + subst. destruct H6; simpl in H6; [intuition |].
        specialize (H6 _ _ eq_refl).
        destruct IHimposure; auto.
        * progressive_inversions.
          invert H15; subst. lia.
          simpl in *. eapply exclude_add; try eassumption.
          congruence.
        * split; trivial.
          set simpl. simpl. fsetdec.

      + inversion H2.
        all:subst; simpl in *; try contradiction.
        * split; trivial. 
          set simpl in *. simpl. fsetdec.
        * split; set solve.
  Qed.
  
End ImposureProperties.

Local Ltac impo_obli := rewrite app_length; simpl; lia.
Local Obligation Tactic := program_simpl; impo_obli.

Equations imposure_func (G : env) (T : typ) : typ by wf (length G) lt :=
  {
    imposure_func G T with is_sel_dec T => {
    | inleft (existT _ (avar_b _) (exist _ A _)) => typ_bot;
    | inleft (existT _ (avar_f x) (exist _ A _)) with split_at x G => {
      | None => typ_bot;
      | Some (existT _ T' (existT _ G1 _)) with exposure_func G1 T' => {
        | typ_bot => typ_top;
        | typ_rcd DS with decs_typ_lookup A DS => {
          | None => typ_bot;
          | Some (pair L U) => imposure_func G1 L
          };
        | typ_obj DS
            with impo_mu_func (collect_ty_labs DS) x A (open x DS) => {
          | (pair _ L) => imposure_func G1 L
          };
        | _ => typ_bot
        }
      };
    | inright _ => T
    }
  }.

Local Obligation Tactic := program_simpl.
Arguments imposure_func G T : simpl never.

Section ImposureProperties2.
  
  Theorem imposure_func_sound_wrt_spec : forall G T U,
    imposure_func G T = U ->
    imposure G T U.
  Proof.
    intros. funelim (imposure_func G T).
    all:rewrite <- Heqcall; clear Heqcall.
    all:simpl in *; try congruence. 
    all:try solve [tidy_up; auto]; subst.
    - tidy_up. constructor.
      apply exposure_func_sound_wrt_spec. trivial.
    - tidy_up. econstructor; eauto.
      + apply exposure_func_sound_wrt_spec. eassumption.
      + apply decs_typ_lookup_sound1. eassumption.
    - apply impo_mu_func_resp_spec in Heq.
      tidy_up. eapply imp_mu; eauto.
      apply exposure_func_sound_wrt_spec. trivial.
  Qed.

  Theorem imposure_func_sound : forall G T U,
      imposure_func G T = U ->
      G ⊢ U <⦂ T /\ ~is_sel U.
  Proof.
    intros. apply imposure_func_sound_wrt_spec in H. split.
    - apply imposure_sound. trivial.
    - eapply imposure_not_sel. eassumption.
  Qed.
  
End ImposureProperties2.

