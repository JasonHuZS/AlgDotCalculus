Require Import Definitions.

Fixpoint typ_struct_measure (T : typ) :=
  match T with
  | typ_top => 1
  | typ_bot => 1
  | typ_sel _ _ => 2
  | typ_all T U => 1 + typ_struct_measure T + typ_struct_measure U
  | typ_rcd DS => 1 + decs_struct_measure DS
  | typ_obj DS => 2 + decs_struct_measure DS
  end
with
dec_struct_measure (D : dec) :=
  match D with
  | dec_typ _ T U => 1 + typ_struct_measure T + typ_struct_measure U
  | dec_trm _ T => 1 + typ_struct_measure T
  end
with
decs_struct_measure (DS : decs) :=
  match DS with
  | decs_nil => 1
  | decs_cons D DS' => 1 + dec_struct_measure D + decs_struct_measure DS'
  end.

Fixpoint trm_struct_measure (t : trm) :=
  match t with
  | trm_var _ => 1
  | trm_val vl => 1 + val_struct_measure vl
  | trm_sel _ _ => 1
  | trm_app _ _ => 2
  | trm_let None s t => 1 + trm_struct_measure s + trm_struct_measure t
  | trm_let (Some T) s t => 1 + typ_struct_measure T +
                           trm_struct_measure s + trm_struct_measure t
  end
with
val_struct_measure (vl : val) :=
  match vl with
  | val_obj DS ds => 1 + decs_struct_measure DS + defs_struct_measure ds
  | val_lam T t => 1 + typ_struct_measure T + trm_struct_measure t
  end
with
def_struct_measure (d : def) :=
  match d with
  | def_typ _ T => 1 + typ_struct_measure T
  | def_trm _ t => 1 + trm_struct_measure t
  end
with
defs_struct_measure (ds : defs) :=
  match ds with
  | defs_nil => 1
  | defs_cons d ds => 1 + def_struct_measure d + defs_struct_measure ds
  end.

Local Ltac simplify :=
  intros; cbn in *; try lia;
  try lazymatch goal with
      | o : option _ |- _ => destruct o; simpl
      end.

Local Ltac finish :=
  repeat match goal with
         | H : context[forall _, _ = _] |- _ =>
           rewrite H
         end;
  reflexivity.

Local Ltac crush_sm T :=
  lazymatch goal with
  | H : context[T] |- _ => clear H
  end; induction on T; simplify; finish.

Lemma open_typ_same_measure : forall T k u,
    typ_struct_measure $ open_rec_typ k u T = typ_struct_measure T
with open_dec_same_measure : forall T k u,
    dec_struct_measure $ open_rec_dec k u T = dec_struct_measure T
with open_decs_same_measure : forall T k u,
    decs_struct_measure $ open_rec_decs k u T = decs_struct_measure T.
Proof.
  - crush_sm typ.
  - crush_sm dec.
  - crush_sm decs.
Qed.

Lemma open_trm_same_measure : forall t k u,
    trm_struct_measure $ open_rec_trm k u t = trm_struct_measure t
with open_val_same_measure : forall vl k u,
    val_struct_measure $ open_rec_val k u vl = val_struct_measure vl
with open_def_same_measure : forall T k u,
    def_struct_measure $ open_rec_def k u T = def_struct_measure T
with open_defs_same_measure : forall T k u,
    defs_struct_measure $ open_rec_defs k u T = defs_struct_measure T.
Proof.
  all:pose proof open_typ_same_measure; pose proof open_decs_same_measure.
  - crush_sm trm.
  - crush_sm val.
  - crush_sm def.
  - crush_sm defs.
Qed.

Lemma close_typ_same_measure : forall T k u,
    typ_struct_measure $ close_rec_typ k u T = typ_struct_measure T
with close_dec_same_measure : forall T k u,
    dec_struct_measure $ close_rec_dec k u T = dec_struct_measure T
with close_decs_same_measure : forall T k u,
    decs_struct_measure $ close_rec_decs k u T = decs_struct_measure T.
Proof.
  - crush_sm typ.
  - crush_sm dec.
  - crush_sm decs.
Qed.

Lemma typ_struct_measure_neq_0 : forall T,
    typ_struct_measure T <> 0.
Proof. destruct T; simpl; lia. Qed.

Lemma dec_struct_measure_neq_0 : forall D,
    dec_struct_measure D <> 0.
Proof. destruct D; simpl; lia. Qed.

Lemma decs_struct_measure_neq_0 : forall DS,
    decs_struct_measure DS <> 0.
Proof. destruct DS; simpl; lia. Qed.

Lemma defs_struct_measure_neq_0 : forall ds,
    defs_struct_measure ds <> 0.
Proof. destruct ds; simpl; lia. Qed.

Lemma decs_measure_lbinds_typ : forall (A : typ_label) L U (DS : decs),
    lbinds A (dec_typ A L U) DS ->
    typ_struct_measure L < decs_struct_measure DS /\
    typ_struct_measure U < decs_struct_measure DS.
Proof.
  induction DS; intros; tidy_up.
  - split; lia.
  - destruct IHDS; trivial.
    split; lia.
Qed.

Lemma decs_measure_lbinds_trm : forall (a : trm_label) T (DS : decs),
    lbinds a (dec_trm a T) DS ->
    typ_struct_measure T < decs_struct_measure DS.
Proof.
  induction DS; intros; tidy_up.
  - lia.
  - apply IHDS in H. lia.
Qed.
