Set Implicit Arguments.

Require Import Definitions.
Require Export Coq.Setoids.Setoid.
Require Import Measures.
Require Import Helpers.

Inductive typ_equiv : typ -> typ -> Prop :=
| top_eq : typ_equiv typ_top typ_top
| bot_eq : typ_equiv typ_bot typ_bot
| sel_eq : forall {a T}, typ_equiv (a ⋅ T) (a ⋅ T)
| all_eq : forall {T1 T2 U1 U2},
    typ_equiv T1 T2 -> typ_equiv U1 U2 ->
    typ_equiv (typ_all T1 U1) (typ_all T2 U2)
| rcd_eq : forall {DS1 DS2},
    decs_equiv DS1 DS2 -> typ_equiv (typ_rcd DS1) (typ_rcd DS2)
| obj_eq : forall {DS}, typ_equiv (typ_obj DS) (typ_obj DS)
with
dec_equiv : dec -> dec -> Prop :=
| dtyp_eq : forall {A T1 T2 U1 U2},
    typ_equiv T1 T2 -> typ_equiv U1 U2 ->
    dec_equiv (dec_typ A T1 U1) (dec_typ A T2 U2)
| dtrm_eq : forall {a T1 T2},
    typ_equiv T1 T2 -> 
    dec_equiv (dec_trm a T1) (dec_trm a T2)
with
decs_equiv : decs -> decs -> Prop :=
| refl_eq : forall {DS}, decs_equiv DS DS
| prep_eq : forall {D1 D2 DS1 DS2},
    dec_equiv D1 D2 -> decs_equiv DS1 DS2 ->
    decs_equiv (decs_cons D1 DS1) (decs_cons D2 DS2)
| swap_eq : forall {D1 D1' D2 D2' DS1 DS2},
    dec_equiv D1 D1' -> dec_equiv D2 D2' ->
    decs_equiv DS1 DS2 ->
    decs_equiv (decs_cons D1 (decs_cons D2 DS1)) (decs_cons D2' (decs_cons D1' DS2))
| tran_eq : forall {DS1 DS2 DS3},
    decs_equiv DS1 DS2 ->
    decs_equiv DS2 DS3 ->
    decs_equiv DS1 DS3.
Local Hint Constructors typ_equiv dec_equiv decs_equiv.

Local Ltac by_ind T :=
  lazymatch reverse goal with
  | H : context[T] |- _ => clear H
  end; induction on T; try solve [eroutine].

Section EquivalenceProperties.

  Lemma typ_eq_refl : forall T, typ_equiv T T
  with dec_eq_refl : forall D, dec_equiv D D
  with decs_eq_refl : forall DS, decs_equiv DS DS.
  Proof.
    - by_ind typ.
    - by_ind dec.
    - constructor.
  Qed.

  Lemma typ_eq_sym : forall T1 T2, typ_equiv T1 T2 -> typ_equiv T2 T1
  with dec_eq_sym : forall D1 D2, dec_equiv D1 D2 -> dec_equiv D2 D1
  with decs_eq_sym : forall DS1 DS2, decs_equiv DS1 DS2 -> decs_equiv DS2 DS1.
  Proof.
    - by_ind typ_equiv.
    - by_ind dec_equiv.
    - by_ind decs_equiv.
  Qed.

  Lemma typ_eq_trans : forall T1 T2 T3, typ_equiv T1 T2 -> typ_equiv T2 T3 -> typ_equiv T1 T3
  with dec_eq_trans : forall D1 D2 D3, dec_equiv D1 D2 -> dec_equiv D2 D3 -> dec_equiv D1 D3
  with decs_eq_trans : forall DS1 DS2 DS3, decs_equiv DS1 DS2 -> decs_equiv DS2 DS3 -> decs_equiv DS1 DS3.
  Proof.
    - intros. gen T3. revert H. by_ind typ_equiv.
    - by_ind dec_equiv.
    - by_ind decs_equiv.
  Qed.

  Global Instance TypEquiv : Equivalence typ_equiv | 1 :=
    {
      Equivalence_Reflexive := typ_eq_refl;
      Equivalence_Symmetric := typ_eq_sym;
      Equivalence_Transitive := typ_eq_trans
    }.

  Global Instance DecEquiv : Equivalence dec_equiv | 1 :=
    {
      Equivalence_Reflexive := dec_eq_refl;
      Equivalence_Symmetric := dec_eq_sym;
      Equivalence_Transitive := dec_eq_trans
    }.
  
  Global Instance DecsEquiv : Equivalence decs_equiv | 1 :=
    {
      Equivalence_Reflexive := decs_eq_refl;
      Equivalence_Symmetric := decs_eq_sym;
      Equivalence_Transitive := decs_eq_trans
    }.
  
End EquivalenceProperties.

Definition equiv {A} {R : relation A} `{Equivalence A R} : relation A := R.

Notation "x ≈ y" := (equiv x y) (at level 70).
Notation "C [ x ≈ y ]" := (@equiv _ _ C x y) (at level 70).
Notation "x !≈ y" := (~equiv x y) (at level 70).
Notation "C [ x !≈ y ]" := (~@equiv _ _ C x y) (at level 70).

Definition ty_equivalence G T U : Prop :=
  G ⊢ T <⦂ U /\ G ⊢ U <⦂ T.

Notation "G ⊢ T ≊ U" :=
  (ty_equivalence G T U)(at level 70, T at level 79) : type_scope.

Section SubtypRelations.
  
  Context {G : env}.
  
  Global Instance PreoderSubtyp : PreOrder (subtyp G) | 2 :=
    {
      PreOrder_Reflexive := (subtyp_refl G);
      PreOrder_Transitive := (@subtyp_trans G)
    }.

  Arguments ty_equivalence G T U/.
  
  Global Instance InducedEquiv : Equivalence (ty_equivalence G) | 2 := { }.
  Proof.
    - routine. 
    - routine. all:apply H.
    - routine; tidy_up; etransitivity; eassumption.
  Qed.
  
  Global Instance InducedPartial : @PartialOrder _ (ty_equivalence G) InducedEquiv
                                                 (subtyp G) PreoderSubtyp | 2 := { }.
  Proof. repeat split; routine. Qed.

  Lemma append_ty_equiv_cong : forall DS1 DS2 DS1' DS2',
      G ⊢ {< DS1 >} ≊ {< DS1' >} ->
      G ⊢ {< DS2 >} ≊ {< DS2' >} ->
      G ⊢ {< append' DS1 DS2 >} ≊ {< append' DS1' DS2' >}.
  Proof. split; apply decs_append_resp_subtyp; routine. Qed.

  Definition append_decs DS1 DS2 := {< append' DS1 DS2 >}.
  
End SubtypRelations.  

Section TypingMorphism.

  Local Notation decs_equivalence G DS1 DS2 := (G ⊢ {< DS1 >} ≊ {< DS2 >}).
  
  Global Add Parametric Morphism G : append_decs
      with signature ((fun DS1 DS2 => decs_equivalence G DS1 DS2)
                        ==> (fun DS1 DS2 => decs_equivalence G DS1 DS2)
                        ==> ty_equivalence G)
        as append_mor.
  Proof. intros. apply append_ty_equiv_cong; assumption. Qed.

End TypingMorphism.

Section ConvenientProperties.
  
  Arguments equiv _ _ _ x y/.
  
  Lemma typ_open_equiv : forall (T U : typ) n x, T ≈ U -> open_rec n x T ≈ open_rec n x U
  with dec_open_equiv : forall (D1 D2 : dec) n x, D1 ≈ D2 -> open_rec n x D1 ≈ open_rec n x D2
  with decs_open_equiv : forall (DS1 DS2 : decs) n x, DS1 ≈ DS2 -> open_rec n x DS1 ≈ open_rec n x DS2.
  Proof.
    - intros. gen n H. by_ind typ_equiv.
    - by_ind dec_equiv.
    - by_ind decs_equiv.
  Qed.

  Local Ltac fin := simpl; try solve [repeat (f_equal; auto)].
  
  Lemma typ_equiv_same_measure : forall (T U : typ), T ≈ U -> typ_struct_measure T = typ_struct_measure U
  with dec_equiv_same_measure : forall (T U : dec), T ≈ U -> dec_struct_measure T = dec_struct_measure U
  with decs_equiv_same_measure : forall (T U : decs),
      T ≈ U -> decs_struct_measure T = decs_struct_measure U.
  Proof.
    - by_ind typ_equiv; fin.
    - by_ind dec_equiv; fin.
    - by_ind decs_equiv; fin.
      + apply dec_equiv_same_measure in H.
        apply dec_equiv_same_measure in H0. lia.
      + lia.
  Qed.

  Global Add Parametric Morphism n x : (@open_rec typ _ n x)
      with signature (equiv ==> equiv) as ty_open_rec_morph.
  Proof. intros. apply typ_open_equiv. trivial. Qed.

  Global Add Parametric Morphism : typ_struct_measure
      with signature (equiv ==> eq) as typ_measure_morph.
  Proof. intros. apply typ_equiv_same_measure. trivial. Qed.

  Definition decs_length (DS : decs) := length (to_list DS).
  Arguments decs_length DS/.
  
  Lemma decs_equiv_same_length : forall (DS1 DS2 : decs),
      DS1 ≈ DS2 ->
      decs_length DS1 = decs_length DS2.
  Proof.
    induction on decs_equiv; simpl in *; trivial.
    - f_equal. apply IHdecs_equiv.
    - do 2 f_equal. apply IHdecs_equiv.
    - congruence.
  Qed.

  Lemma decs_length_1 : forall DS,
      decs_length DS = 1 ->
      exists D, DS = decs_cons D decs_nil.
  Proof.
    induction on decs; routine.
    destruct H; routine.
    eexists. reflexivity.
  Qed.      
  
  Lemma decs_singleton_dec : forall D D',
      decs_cons D decs_nil ≈ decs_cons D' decs_nil ->
      D ≈ D'.
  Proof.
    intros. cbv in H. dependent induction H.
    - reflexivity.
    - trivial.
    - pose proof (decs_equiv_same_length H).
      simpl in H1. symmetry in H1.
      pose proof (decs_length_1 _ H1).
      destruct H2. subst.
      etransitivity.
      apply IHdecs_equiv1; reflexivity.
      apply IHdecs_equiv2; reflexivity.
  Qed.
  
End ConvenientProperties.


Section TypRelations.  
  
  Local Definition typ_equiv_resp_measure T U := typ_struct_measure T + typ_struct_measure U.
  Arguments typ_equiv_resp_measure T U/.
  
  Program Fixpoint typ_equiv_resp T U {measure (typ_equiv_resp_measure T U)} :
    forall G, T ≈ U -> G ⊢ T ≊ U := _.
  Next Obligation.
    simpl in H; destruct H; try solve [routine].
    - simpl in *. pose proof (typ_equiv_resp T1 T2 ltac:(lia) G H). tidy_up.
      split; eapply subtyp_all; try assumption; cofinite.
      + eapply typ_equiv_resp.
        * repeat rewrite open_typ_same_measure; lia.
        * rewrite H0. reflexivity.
      + eapply typ_equiv_resp.
        * repeat rewrite open_typ_same_measure; lia.
        * rewrite H0. reflexivity.
    - assert (dec_equiv_resp : forall D1 D2,
                 dec_struct_measure D1 + dec_struct_measure D2 <
                 typ_equiv_resp_measure (typ_rcd DS1) (typ_rcd DS2) ->
                 D1 ≈ D2 ->
                 G ⊢ (typ_rcd (decs_cons D1 decs_nil)) ≊ (typ_rcd (decs_cons D2 decs_nil))). {
        induction on dec_equiv; simpl in *.
        - pose proof (typ_equiv_resp T1 T2 ltac:(lia) G H0).
          pose proof (typ_equiv_resp U1 U2 ltac:(lia) G H1).
          split.
          * eapply subtyp_typ. apply H3. apply H4.
          * eapply subtyp_typ. apply H3. apply H4.
        - pose proof (typ_equiv_resp T1 T2 ltac:(lia) G H0).
          split.
          * eapply subtyp_fld. apply H1.
          * eapply subtyp_fld. apply H1.
      }
      
      assert (decs_equiv_resp : forall DS1' DS2',
                 decs_struct_measure DS1' + decs_struct_measure DS2' <
                 typ_equiv_resp_measure (typ_rcd DS1) (typ_rcd DS2) ->
                 DS1' ≈ DS2' ->
                 G ⊢ (typ_rcd DS1') ≊ (typ_rcd DS2')). {
        induction on decs_equiv; simpl in *.
        - reflexivity.
        - specialize (IHdecs_equiv ltac:(lia)). list_reasoning.
          apply append_ty_equiv_cong; trivial.
          apply dec_equiv_resp; trivial. lia.
        - specialize (IHdecs_equiv ltac:(lia)).
          change (decs_cons ?D1 (decs_cons ?D2 ?DS))
            with (append' (decs_cons D1 (decs_cons D2 decs_nil)) DS).
          apply append_ty_equiv_cong; trivial.
          apply dec_equiv_resp in H0; try lia.
          apply dec_equiv_resp in H1; try lia.
          list_reasoning. split.
          + apply subtyp_merge.
            etransitivity; [apply subtyp_drop1 |]; routine.
            etransitivity; [apply subtyp_drop2 |]; routine.
          + apply subtyp_merge.
            etransitivity; [apply subtyp_drop1 |]; routine.
            etransitivity; [apply subtyp_drop2 |]; routine.
        - apply decs_equiv_same_measure in H3_.
          apply decs_equiv_same_measure in H3_0.
          rewrite <- H3_0 in *. rewrite H3_ in *.
          etransitivity. apply IHdecs_equiv1; trivial.
          apply IHdecs_equiv2; trivial.
      }
      simpl in *. apply decs_equiv_resp. lia.
      trivial.
  Qed.

  Global Add Parametric Morphism G : (ty_equivalence G)
      with signature (equiv ==> equiv ==> iff) as typ_equiv_morph.
  Proof.
    split; intros.
    - etransitivity.
      apply typ_equiv_resp. symmetry. eassumption.
      etransitivity. eassumption. apply typ_equiv_resp. assumption.
    - etransitivity.
      apply typ_equiv_resp. eassumption.
      etransitivity. eassumption. apply typ_equiv_resp.
      symmetry. assumption.
  Qed.

  Lemma decs_equiv_resp : forall G DS1 DS2,
      DS1 ≈ DS2 -> G ⊢ (typ_rcd DS1) ≊ (typ_rcd DS2).
  Proof.
    intros. apply typ_equiv_resp.
    constructor. trivial.
  Qed.

  Global Add Parametric Morphism G : (fun DS1 DS2 => G ⊢ (typ_rcd DS1) ≊ (typ_rcd DS2))
      with signature (equiv ==> equiv ==> iff) as decs_equiv_morph.
  Proof.
    intros. apply (decs_equiv_resp G) in H.
    apply (decs_equiv_resp G) in H0.
    split; intros.
    - etransitivity. rewrite <- H. reflexivity.
      rewrite H1. trivial.
    - etransitivity. apply H.
      rewrite H1. rewrite H0. reflexivity.
  Qed.
  
End TypRelations.

Section Decidabilities.

  Local Ltac yes := solve [left; subst; constructor; simpl; auto].
  Local Ltac no :=
    let Contra := fresh "Contra" in
    solve [right; intro Contra;
           invert Contra; progressive_inversions; routine].

  Lemma nil_equiv_nil : forall DS,
      decs_nil ≈ DS -> DS = decs_nil.
  Proof.
    intros. cbv in H. dependent induction H; auto.
  Qed.
  
  Lemma false_nil_equiv_cons : forall D DS,
      decs_nil ≈ decs_cons D DS -> False.
  Proof.
    intros. apply nil_equiv_nil in H. congruence.
  Qed.
  Hint Resolve false_nil_equiv_cons.

  Lemma all_nils : forall DS1 DS2,
      decs_nil ≈ append' DS1 DS2 -> DS1 = decs_nil /\ DS2 = decs_nil.
  Proof.
    destruct DS1, DS2; intros; simpl in *; auto.
    all:exfalso; eapply false_nil_equiv_cons; eassumption.
  Qed.
  
  Lemma shift_decs_equiv : forall D DS1 DS2,
      (decs_cons D (append' DS1 DS2)) ≈ (append' DS1 (decs_cons D DS2)).
  Proof.      
    induction DS1; intros; simpl in *.
    - reflexivity.
    - etransitivity.
      + eapply swap_eq; reflexivity.
      + constructor. reflexivity.
        apply IHDS1.
  Qed.

  Lemma shift_decs_equiv_gen : forall D1 D2 DS1 DS2,
      D1 ≈ D2 ->
      (decs_cons D1 (append' DS1 DS2)) ≈ (append' DS1 (decs_cons D2 DS2)).
  Proof.
    intros. rewrite <- shift_decs_equiv.
    constructor; trivial.
  Qed.
  
  Lemma decs_equiv_resp_In : forall D (DS1 DS2 : decs),
      In D (to_list DS1) ->
      DS1 ≈ DS2 ->
      exists D', D ≈ D' /\ In D' (to_list DS2).
  Proof.
    intros. gen D. induction H0; intros.
    - eexists. eroutine. reflexivity.
    - simpl in H1. destruct H1; tidy_up.
      + eexists. eroutine.
      + destruct (IHdecs_equiv D); trivial.
        eroutine.
    - simpl in H2. destruct H2; tidy_up.
      + eroutine.
      + eroutine.
      + destruct (IHdecs_equiv D); trivial.
        eroutine.
    - destruct (IHdecs_equiv1 D); trivial.
      tidy_up. destruct (IHdecs_equiv2 x); trivial.
      tidy_up.
      eexists. split.
      etransitivity. eassumption. eassumption.
      trivial.
  Qed.

  Lemma decs_cons_equiv_head : forall D DS DS',
      decs_cons D DS ≈ DS' ->
      exists D', D ≈ D' /\ In D' (to_list DS').
  Proof.
    intros.
    eapply decs_equiv_resp_In; [ | eassumption].
    routine.
  Qed.
  
  Lemma decs_equiv_drop_eq : forall DS1 DS3 DS2 DS4 D D',
      append' DS1 (decs_cons D DS2) = append' DS3 (decs_cons D' DS4) ->
      dec_equiv D D' ->
      append' DS1 DS2 ≈ append' DS3 DS4.
  Proof.
    induction on decs; intros.
    - destruct DS3; routine.
      reflexivity.
      rewrite shift_decs_equiv_gen. reflexivity. trivial.
    - destruct DS3; simpl in *.
      + tidy_up. rewrite shift_decs_equiv_gen. reflexivity.
        symmetry. trivial.
      + tidy_up. constructor. reflexivity.
        eapply IHdecs. apply H4. trivial.
  Qed.

  Lemma decs_equiv_drop_mid : forall DS1 DS2 DS3 DS4 D D',
      decs_equiv (append' DS1 (decs_cons D DS2)) $ append' DS3 (decs_cons D' DS4) ->
      dec_equiv D D' ->
      append' DS1 DS2 ≈ append' DS3 DS4.
  Proof.
    intros. dependent induction H.
    - eapply decs_equiv_drop_eq; eassumption.
    - destruct DS1, DS3; tidy_up; trivial.
      + rewrite shift_decs_equiv.
        etransitivity. eassumption.
        change (decs_append' ?DS1 ?DS2) with (append' DS1 DS2).
        rewrite <- shift_decs_equiv.
        apply shift_decs_equiv_gen.
        etransitivity. symmetry. eassumption.
        trivial.
      + etransitivity; [ | eassumption].
        apply shift_decs_equiv_gen.
        etransitivity. eassumption.
        symmetry. trivial.
      + constructor. trivial.
        eapply IHdecs_equiv; trivial.
    - destruct DS1, DS3; tidy_up; trivial.
      + constructor; trivial.
        etransitivity. eassumption.
        etransitivity; [ | eassumption]. symmetry. trivial.
      + destruct DS3; tidy_up. constructor; trivial.
        constructor; trivial.
        rewrite H1. symmetry. apply shift_decs_equiv_gen.
        etransitivity. symmetry. eassumption.
        trivial.
      + destruct DS1; tidy_up; trivial.
        constructor; trivial.
        constructor; trivial. rewrite <- H1.
        apply shift_decs_equiv_gen.
        etransitivity. eassumption. symmetry. trivial.
      + destruct DS1, DS3; tidy_up.
        * constructor; trivial.
          etransitivity. eassumption.
          etransitivity. symmetry. apply H2. trivial.
        * etransitivity; [ | eapply swap_eq; reflexivity].
          constructor. eassumption.
          rewrite H1. rewrite shift_decs_equiv_gen; try reflexivity.
          etransitivity. symmetry. eassumption. trivial.
        * etransitivity; [eapply swap_eq; reflexivity |].
          constructor. eassumption.
          rewrite <- H1. apply shift_decs_equiv_gen.
          etransitivity. eassumption. symmetry. trivial.
        * etransitivity. eapply swap_eq; reflexivity.
          do 2 (constructor; try assumption).
          eapply IHdecs_equiv; try reflexivity. trivial.
    - assert (In D (to_list (append' DS1 (decs_cons D DS2)))). {
        change (append' ?X ?Y) with (append X Y).
        rewrite append_sound. apply in_or_app.
        right. simpl. routine.
      }
      pose proof (decs_equiv_resp_In D H2 H).
      destruct_conjs. apply in_split in H5. destruct_conjs.
      pose proof (from_to_iso DS6).
      rewrite H7 in H8. rewrite <- append_complete in H8.
      symmetry in H8. simpl in *.
      pose proof (IHdecs_equiv1 _ _ _ _ _ _ eq_refl H8 H4).
      rewrite H9. rewrite IHdecs_equiv2; try eassumption; try reflexivity.
      etransitivity. symmetry. eassumption.
      trivial.
  Qed.

  Lemma decs_equiv_drop_cons : forall DS1 DS2 D D',
      decs_equiv (decs_cons D DS1) (decs_cons D' DS2) ->
      dec_equiv D D' ->
      DS1 ≈ DS2.
  Proof.
    intros. change DS1 with (append' decs_nil DS1).
    change DS2 with (append' decs_nil DS2).
    eapply decs_equiv_drop_mid; simpl; eassumption.
  Qed.

  Lemma decs_equiv_drop_one_side : forall D DS DS',
      decs_equiv (decs_cons D DS) DS' ->
      exists DS1 D' DS2, dec_equiv D D' /\
                    DS' = append' DS1 (decs_cons D' DS2) /\
                    DS ≈ append' DS1 DS2.
  Proof.
    intros.
    pose proof (decs_cons_equiv_head H).
    destruct_conjs.
    apply in_split in H2. destruct_conjs.
    pose proof (from_to_iso DS').
    rewrite H4 in H5. rewrite <- append_complete in H5.
    rewrite <- H5 in H. simpl in H.
    change (decs_append ?X ?Y) with (append' X Y) in H.
    change (decs_cons D DS) with (append' decs_nil (decs_cons D DS)) in H.
    eapply decs_equiv_drop_mid in H; trivial.
    simpl in H. repeat eexists.
    all:try eassumption.
    symmetry. apply H5.
  Qed.
  
  Lemma decs_equiv_refute : forall DS1 DS2 D,
      ~decs_equiv DS1 DS2 ->
      ~decs_equiv (decs_cons D DS1) (decs_cons D DS2).
  Proof.
    intros. intro Contra. apply H.
    eapply decs_equiv_drop_cons.
    eassumption. reflexivity.
  Qed.
  
  Fixpoint typ_equiv_dec (x y : typ) : {x ≈ y} + {x !≈ y}
  with dec_equiv_dec (x y : dec) : {x ≈ y} + {x !≈ y}
  with decs_equiv_dec (x y : decs) : {x ≈ y} + {x !≈ y}.
  Proof.
    - destruct x, y.
      all:try no || yes.
      + destruct (a == a0), (t == t0).
        yes. all:no.
      + destruct (typ_equiv_dec x1 y1), (typ_equiv_dec x2 y2).
        yes. all:no.
      + destruct (decs_equiv_dec d d0).
        yes. no.
      + destruct (d == d0).
        yes. no.
    - destruct x, y; try no.
      + destruct (t == t2), (typ_equiv_dec t0 t3), (typ_equiv_dec t1 t4).
        yes. all:no.
      + destruct (t == t1), (typ_equiv_dec t0 t2).
        yes. all:no.
    - destruct x.
      + destruct y. yes.
        right. intro Contra. eapply false_nil_equiv_cons.
        eassumption.
      + assert (loop : forall DS,
                   { DS1 & { d' & {DS2 | d ≈ d' /\
                                         DS = append' DS1 (decs_cons d' DS2)} } } +
                   {Forall (fun d' => d !≈ d') (to_list DS)}). {
          induction on decs.
          - right. constructor.
          - destruct (dec_equiv_dec d d0).
            + left. exists decs_nil, d0, H. split; trivial.
            + destruct IHdecs; progressive_destructions.
              * left. subst. exists (decs_cons d0 s).
                repeat eexists. eassumption.
              * right. simpl. constructor; trivial.
        }
        destruct (loop y).
        * destruct_conjs. subst.
          destruct (decs_equiv_dec x (append' s X0)).
          -- left. etransitivity; [ | apply shift_decs_equiv].
             constructor; trivial.
          -- right. intro Contra.
             eapply decs_equiv_refute. eassumption.
             etransitivity. eassumption. symmetry.
             apply shift_decs_equiv_gen. trivial.
        * right. intro Contra.
          pose proof (decs_cons_equiv_head Contra).
          destruct_conjs.
          rewrite Forall_forall in f.
          apply f in H1. intuition.
  Defined.

End Decidabilities.