Set Implicit Arguments.
Require Import Definitions.
Require Import InertTypes Helpers.

Inductive ope_decs : decs -> decs -> Prop :=
| od_nil : ope_decs decs_nil decs_nil
| od_keep : forall D DS DS', ope_decs DS DS' -> ope_decs (decs_cons D DS) (decs_cons D DS')
| od_drop : forall D DS DS', ope_decs DS DS' -> ope_decs (decs_cons D DS) DS'.
Hint Constructors ope_decs.

Inductive precise_flow : env -> atom -> typ -> typ -> Prop :=
| pf_binds : forall G x T, binds x T G -> precise_flow G x T T
| pf_open : forall G x T DS, precise_flow G x T (μ{DS}) ->
                        precise_flow G x T ({< open x DS >})
| pf_ope : forall G x T DS DS',
    precise_flow G x T ({< DS >}) ->
    ope_decs DS DS' ->
    precise_flow G x T ({< DS' >}).
Hint Constructors precise_flow.

Lemma ope_decs_refl : forall DS,
    ope_decs DS DS.
Proof. induction on decs; routine. Qed.
Local Hint Resolve ope_decs_refl.

Lemma ope_decs_to_nil : forall DS,
    ope_decs DS decs_nil.
Proof. induction on decs; auto. Qed.
Local Hint Resolve ope_decs_to_nil.

Lemma ope_decs_trans : forall DS1 DS2 DS3,
    ope_decs DS1 DS2 ->
    ope_decs DS2 DS3 ->
    ope_decs DS1 DS3.
Proof.
  intros. gen DS3.
  induction H; routine.
  invert H0; routine.
Qed.

Lemma ope_decs_append1 : forall DS1 DS2,
    ope_decs (append' DS1 DS2) DS1.
Proof. induction on decs; routine. Qed.

Lemma ope_decs_append2 : forall DS1 DS2,
    ope_decs (append' DS1 DS2) DS2.
Proof. induction on decs; routine. Qed.

Local Hint Resolve ope_decs_refl ope_decs_trans.
Local Hint Resolve ope_decs_append1 ope_decs_append2.

Lemma precise_flow_rcd_inert : forall G x T DS,
    inert_env G ->
    precise_flow G x T ({< DS >}) ->
    exists DS', T = μ{ DS' } /\ ope_decs (open x DS') DS.
Proof.
  dep induction on precise_flow.
  - exfalso. eapply inert_not_rcd; eauto.
  - eroutine.
  - edestruct IHprecise_flow; trivial; try reflexivity;
      eroutine.
Qed.

Lemma ope_In : forall DS D DS',
    ope_decs DS DS' ->
    In D (to_list DS') ->
    In D (to_list DS).
Proof. induction on ope_decs; routine. Qed.

Lemma In_is_lbinds : forall D (DS : decs),
    In D (to_list DS) ->
    lbinds (lab_dec_of D) D DS.
Proof.
  induction on decs; routine.
  right. apply IHdecs. apply H1.
Qed.

Lemma lbinds_is_In : forall D (DS : decs),
    lbinds (lab_dec_of D) D DS ->
    In D (to_list DS).
Proof. induction on decs; routine. Qed.


Lemma ope_lbinds : forall DS (a : trm_label) T DS',
    ope_decs DS DS' ->
    lbinds a (dec_trm a T) DS' ->
    lbinds a (dec_trm a T) DS.
Proof.
  induction on ope_decs; routine.
  all:right; apply IHope_decs; eassumption.
Qed.

Lemma precise_decs_ope : forall G x DS DS',
    precise_flow G x (typ_rcd DS) (typ_rcd DS') ->
    ope_decs DS DS'.
Proof.
  dep induction on precise_flow; eroutine.
Qed.

Lemma precise_flow_append1 : forall G x T DS1 DS2,
    precise_flow G x T ({< append' DS1 DS2 >}) ->
    precise_flow G x T ({< DS1 >}).
Proof. eroutine. Qed.

Lemma precise_flow_append2 : forall G x T DS1 DS2,
    precise_flow G x T ({< append' DS1 DS2 >}) ->
    precise_flow G x T ({< DS2 >}).
Proof. eroutine. Qed.

Lemma precise_flow_rcd_inv : forall G x DS T,
    precise_flow G x ({< DS >}) T ->
    exists DS', T = {< DS' >} /\ ope_decs DS DS'.
Proof.
  dep induction on precise_flow; eroutine.
  edestruct IHprecise_flow; eauto. eroutine.
Qed.

Lemma ope_is_subtyp : forall G DS1 DS2,
    ope_decs DS1 DS2 ->
    G ⊢ {< DS1 >} <⦂ {< DS2 >}.
Proof.
  induction on ope_decs; eroutine.
  - list_reasoning. 
    apply decs_append_resp_subtyp; auto.
  - list_reasoning. eapply subtyp_trans.
    apply subtyp_drop1. routine.
    trivial.
Qed.

Lemma precise_flow_is_typ : forall G x T U,
    precise_flow G x T U ->
    G ⊢ trm_var x ⦂ U.
Proof.
  induction on precise_flow; eroutine.
  eapply ty_sub. eauto.
  apply ope_is_subtyp. trivial.
Qed.

Lemma precise_flow_is_binds : forall G x U T,
    precise_flow G x U T ->
    binds x U G.
Proof. induction on precise_flow; routine. Qed.

Lemma ope_singleton : forall DS D,
    ope_decs DS (decs_cons D decs_nil) ->
    In D (to_list DS).
Proof.
  intros. eapply ope_In; eauto.
  simpl. eauto.
Qed.
  
Lemma lin_search_to_precise_flow : forall G x T DS D,
    In D (to_list DS) ->
    precise_flow G x T ({< DS >}) ->
    precise_flow G x T ({< decs_cons D decs_nil >}).
Proof.
  induction on decs; routine.
  - eapply pf_ope; eauto.
  - apply IHdecs; auto.
    eapply pf_ope; eauto.
Qed.

Lemma search_to_precise_flow : forall G x DS D,
    binds x (μ{DS}) G ->
    In D (to_list $ open x DS) ->
    precise_flow G x (μ{DS}) ({< decs_cons D decs_nil >}).
Proof.
  intros. eapply lin_search_to_precise_flow; eauto.
Qed.

Ltac pf_int :=
  match goal with
  | H : inert_env ?G,
    Pf : precise_flow ?G _ _ (typ_rcd _) |- _ =>
    let H1 := fresh "H" in
    let H2 := fresh "H" in
    pose proof (precise_flow_is_binds Pf) as H1;
    pose proof (precise_flow_rcd_inert H Pf) as H2;
    destruct_conjs;
    clear Pf
  end.

Ltac to_lbinds :=
  match goal with
  | H : ope_decs _ (decs_cons _ decs_nil) |- _ =>
    apply ope_singleton in H;
    apply In_is_lbinds in H
  end.
