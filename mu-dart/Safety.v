Require Export Definitions.
Require Export Substitution.
Require Export CanonicalForms.
Require Export OperationalSemantics.
Require Export SubstitutionLaws.
Require Export InertTypes.
Require Export Weakening.
Require Export Narrowing.
Require Import Helpers.

Inductive sta_trm_typ : sta * trm -> typ -> Prop :=
| sta_trm_typ_c : forall G s t T,
    inert_env G ->
    well_typed G s ->
    G ⊢ t ⦂ T ->
    sta_trm_typ (s, t) T.

Hint Constructors sta_trm_typ.

Notation "'⊢' t '⦂' T" := (sta_trm_typ t T) (at level 40, t at level 59).

Section Preservation.        

  Hint Constructors Forall inert_typ.
    
  Lemma val_init_inert : forall G v T,
    G ⊢ trm_val v ⦂ T ->
    exists T', G ⊢ trm_val v ⦂ T' /\
          G ⊢ T' <⦂ T /\
          inert_typ T'.
  Proof.
    dep induction on ty_trm.
    - eroutine.
    - eexists; eroutine.
      pick_fresh x. assert (x `notin` L) by auto.
      specialize (H _ H0).
      apply ty_defs_inert in H.
      apply open_decs_invert_inert in H; auto.
      apply luniq_defs_open. trivial.
    - edestruct IHty_trm; try reflexivity. eroutine.
  Qed.

  Lemma well_typed_same_dom : forall G s,
      well_typed G s ->
      dom G = dom s.
  Proof. unfold well_typed. routine. Qed.

  Lemma open_resp_wf_defs : forall x (ds : defs),
      wf_defs ds ->
      wf_defs (open x ds).
  Proof.
    induction on defs; routine.
    constructor; auto.
    rewrite defs_open_same_ldom.
    destruct d; routine.
  Qed.
  
  Ltac claim l :=
    exists l; simpl; split; [ |split].
  
  Ltac is_nil := let n := uconstr:(nil) in claim n; auto.
      
  Ltac prep_let :=
    lazymatch goal with
    | H : opred _ _ _ _ |- _ => invert H; subst
    end;
    progressive_inversions.

  Local Ltac resolve_let H :=
    edestruct H; try eassumption;
    destruct_conjs;
    match goal with
    | _ : inert_env ?G' |- _ =>
      solve [claim G'; auto;
             (eapply ty_let1 || eapply ty_let2); try eassumption;
             cofinite; apply weaken_trm_gen; auto]
    end.
  
  Lemma gen_preservation : forall G s t T,
    well_typed G s ->
    inert_env G ->
    G ⊢ t ⦂ T ->
    forall s' t',
      [s] t ↦ [s'] t' ->
      exists G',
        inert_env G' /\
        well_typed (G' ++ G) s' /\
        G' ++ G ⊢ t' ⦂ T.
  Proof.
    induction on ty_trm; try solve [routine]; intros.
    - exists nil. recover_inert_env.
      pose proof H3. unfold well_typed in H3.
      edestructs canonical_form_all.
      progressive_destructions;
        repeat split;  try eassumption; eauto.
      simpl.
      fold_open_rec.

      pick_fresh y;
        eapply renaming_trm;
        try eassumption;
        try instantiate (1 := y).
      auto.
      pose proof (binds_unique _ _ _ _ _ H5 H13 u0).
      inversion H0. subst.
      auto.
    - edestruct canonical_form_obj; try eassumption.
      destruct_conjs. progressive_inversions. is_nil.
      unfold well_typed in H3.
      pose proof (binds_unique _ _ _ _ _ H6 H13 ltac:(routine)).
      invert H11. subst.
      apply open_resp_wf_defs with (x := x) in H8.
      pose proof (wf_defs_lookup _ _ _ _ H8 H7 H16).
      subst. trivial.
    - destruct t; prep_let.
      par:try resolve_let IHty_trm.
      + is_nil. eapply renaming_fresh; routine.
      + pose proof (val_init_inert _ _ _ H5).
        assert (x `notin` dom G) by (erewrite well_typed_same_dom; eroutine).
        destruct_conjs. claim (x ~ H8).
        * routine. repeat constructor. trivial.
        * apply well_typed_push; trivial.
        * eapply renaming_fresh; auto.
          cofinite. simpl_env. apply weaken_trm_gen.
          eapply narrow_trm; auto.
    - destruct t; prep_let.
      par:try resolve_let IHty_trm.
      + is_nil. eapply renaming_fresh; routine.
      + pose proof (val_init_inert _ _ _ H5).
        assert (x `notin` dom G) by (erewrite well_typed_same_dom; eroutine).
        destruct_conjs. claim (x ~ H8).
        * routine. repeat constructor. trivial.
        * apply well_typed_push; trivial.
        * eapply renaming_fresh; auto.
          cofinite. simpl_env. apply weaken_trm_gen.
          eapply narrow_trm; auto.
    - edestruct IHty_trm; try eassumption.
      destruct_conjs.
      claim x; auto. eapply ty_sub; try eassumption.
      apply weaken_subtyp. trivial.
  Qed.

  Theorem presevation : forall s s' t t' T,
      ⊢ (s, t) ⦂ T ->
      [s] t ↦ [s'] t' ->
      ⊢ (s', t') ⦂ T.
  Proof.
    intros. tidy_up.
    edestruct gen_preservation; try eassumption.
    eroutine.
  Qed.

End Preservation.

Lemma var_typing_implies_avar_f: forall G a T,
    G ⊢ trm_var a ⦂ T ->
    exists x, a = avar_f x.
Proof.
  intros; dependent induction H; eauto.
Qed.

Ltac boom H :=
  edestruct H; try eassumption; eroutine.

Theorem progress: forall s t T,
    ⊢ (s, t) ⦂ T ->
    nf t \/ exists s' t', [s] t ↦ [s'] t'.
Proof.
  intros. tidy_up.
  induction H5; eauto.
  - edestruct canonical_form_all; try eassumption.
    destruct_conjs. right. eroutine.
  - edestruct canonical_form_obj; try eassumption.
    destruct_conjs. right. eroutine.
  - right. destruct t.
    par:try solve [boom IHty_trm].
    + edestruct var_typing_implies_avar_f; try eassumption.
      subst. eroutine.
    + pick_fresh z.
      exists (z ~ v ++ s), (open z u).
      constructor. auto.
  - right. destruct t.
    par:try solve [boom IHty_trm].
    + edestruct var_typing_implies_avar_f; try eassumption.
      subst. eroutine.
    + pick_fresh z.
      exists (z ~ v ++ s), (open z u).
      constructor. auto.
Qed.