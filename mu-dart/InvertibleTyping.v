Set Implicit Arguments.
Require Import Definitions TightTyping.
Require Import InertTypes.
Require Import PreciseTyping.

Reserved Notation "G '⊢##' x '⦂' T" (at level 70, T at level 79).

Inductive ty_var_inv : env -> atom -> typ -> Prop :=
| ty_v_inv : forall G x T, binds x T G -> G ⊢## x ⦂ T
| ty_obj_drop1_inv : forall G x (DS1 DS2 : decs),
    not_empty DS1 ->
    G ⊢## x ⦂ {< append' DS1 DS2 >} ->
    G ⊢## x ⦂ {< DS2 >}
| ty_obj_drop2_inv : forall G x (DS1 DS2 : decs),
    not_empty DS2 ->
    G ⊢## x ⦂ {< append' DS1 DS2 >} ->
    G ⊢## x ⦂ {< DS1 >}
| ty_obj_merge_inv : forall G x (DS1 DS2 : decs),
    G ⊢## x ⦂ {< DS1 >} ->
    G ⊢## x ⦂ {< DS2 >} ->
    G ⊢## x ⦂ {< append' DS1 DS2 >}

| ty_obj_fld_inv : forall G x a T U,
    G ⊢# T <⦂ U ->
    G ⊢## x ⦂ {< decs_cons (dec_trm a T) decs_nil >} ->
    G ⊢## x ⦂ {< decs_cons (dec_trm a U) decs_nil >}
| ty_obj_typ_inv : forall G (x : atom) A S1 T1 S2 T2,
    G ⊢## x ⦂ {< decs_cons (dec_typ A S1 T1) decs_nil >} ->
    G ⊢# S2 <⦂ S1 ->
    G ⊢# T1 <⦂ T2 ->
    G ⊢## x ⦂ {< decs_cons (dec_typ A S2 T2) decs_nil >}

| ty_mu_intro_inv : forall x G DS,
    G ⊢## x ⦂ {< open x DS >} ->
    G ⊢## x ⦂ μ{ DS }
| ty_mu_elim_inv : forall x G DS,
    G ⊢## x ⦂ μ{ DS } ->
    G ⊢## x ⦂ {< open x DS >}

| ty_sel_inv : forall G x y S A U,
    G ⊢## x ⦂ S ->
    precise_flow G y U ({< decs_cons (dec_typ A S S) decs_nil >}) ->
    G ⊢## x ⦂ y ⋅ A
| ty_all_inv: forall L G x S1 T1 S2 T2,
    G ⊢## x ⦂ all(S1) T1 ->
    G ⊢# S2 <⦂ S1 ->
    (forall x, x `notin` L ->
       x ~ S2 ++ G ⊢ open x T1 <⦂ open x T2) ->
    G ⊢## x ⦂ all(S2) T2
| ty_top_inv : forall G x T,
    G ⊢## x ⦂ T ->
    G ⊢## x ⦂ ⊤
where "G ⊢## x ⦂ T" := (ty_var_inv G x T).
Hint Constructors ty_var_inv.

Reserved Notation "G '⊢##v' x '⦂' T" (at level 70, T at level 79).
Inductive ty_val_inv : env -> val -> typ -> Prop :=
| ty_objv_v : forall L G ds DS,
    (forall x, x `notin` L ->
          x ~ μ{ DS } ++ G ⊩[ open x ds ⦂ open x DS ]) ->
    wf_defs ds ->
    G ⊢##v [DS]{ ds } ⦂ μ{ DS }
| ty_allv_v : forall L G T t U,    
    (forall x, x `notin` L ->
          x ~ T ++ G ⊢ open x t ⦂ open x U) ->
    G ⊢##v λ( T ){ t } ⦂ all( T ) U
| ty_all_inv_v: forall L G v S1 T1 S2 T2,
    G ⊢##v v ⦂ all(S1) T1 ->
    G ⊢# S2 <⦂ S1 ->
    (forall x, x `notin` L ->
       x ~ S2 ++ G ⊢ open x T1 <⦂ open x T2) ->
    G ⊢##v v ⦂ all(S2) T2
      
| ty_sel_inv_v : forall G v y U A T,
    G ⊢##v v ⦂ T ->
    precise_flow G y U ({< decs_cons (dec_typ A T T) decs_nil >}) ->
    G ⊢##v v ⦂ y ⋅ A 

| ty_top_inv_v : forall G v T,
    G ⊢##v v ⦂ T ->
    G ⊢##v v ⦂ ⊤
where "G ⊢##v v ⦂ T" := (ty_val_inv G v T).
Hint Constructors ty_val_inv.

Tactic Notation "inv" "typing" "undec" "1" :=
  match goal with
  | [H : _ ⊢## _ ⦂ _ ⋅ _ |- _ ] => invert H
  | _ => idtac
  end.

Section InvertibleTyping.

  Local Hint Resolve ope_decs_refl ope_decs_trans ope_decs_to_nil.
  Local Hint Resolve ope_decs_append1 ope_decs_append2.

  Ltac pf_tac :=
    from_inert_env;
    repeat pf_int;
    from_inert_env; progressive_inversions;
    repeat to_lbinds;
    prove from inert.
  
  Lemma invertible_typing_closure_tight: forall G x T U,
      inert_env G ->
      G ⊢## x ⦂ T ->
      G ⊢# T <⦂ U ->
      G ⊢## x ⦂ U.
  Proof using.
    induction on subtypt;
      inv typing undec 1;
      eprove from inert at 6.
    pf_tac.
  Qed.
  Hint Resolve invertible_typing_closure_tight.

  Lemma tight_to_invertible : forall G (x : var) U,
      inert_env G ->
      G ⊢# trm_var x ⦂ U ->
      G ⊢## x ⦂ U.
  Proof using. dep induction on tyt_trm; eauto. Qed.

  Lemma invertible_typing_closure_tight_v: forall G v T U,
      inert_env G ->
      G ⊢##v v ⦂ T ->
      G ⊢# T <⦂ U ->
      G ⊢##v v ⦂ U.
  Proof using.
    induction on subtypt; try solve [eprove from inert at 6].
    invert H4. pf_tac.
  Qed.
  Hint Resolve invertible_typing_closure_tight_v.

  Lemma tight_to_invertible_v : forall G v T,
      inert_env G ->
      G ⊢# trm_val v ⦂ T ->
      G ⊢##v v ⦂ T.
  Proof using. dep induction on tyt_trm; eauto. Qed.

  Local Hint Resolve precise_flow_append1 precise_flow_append2.  
  Local Ltac destr_ind H :=
    tidy_up; edestruct H; auto; try reflexivity;
    [ solve_lbinds
    | destruct_conjs; eauto 10].

  Local Hint Resolve ope_In ope_lbinds.

  Lemma invertible_find_decs : forall G x (a : trm_label) T DS,
      inert_env G ->
      G ⊢## x ⦂ {< DS >} ->
      lbinds a (dec_trm a T) DS ->
      exists T' DS',
        binds x (typ_obj DS') G /\
        lbinds a (dec_trm a T') (open x DS') /\
        G ⊢# T' <⦂ T
  with invertible_find_mu : forall G x (a : trm_label) T DS,
      inert_env G ->
      G ⊢## x ⦂ μ{ DS } ->
      lbinds a (dec_trm a T) (open x DS) ->
      exists T' DS',
        binds x (typ_obj DS') G /\
        lbinds a (dec_trm a T') (open x DS') /\
        G ⊢# T' <⦂ T.
  Proof.
    - clear invertible_find_decs.
      intros. gen T H. dependent induction H0; intros.
      par: try solve [destr_ind IHty_var_inv].
      + exfalso. eapply inert_not_rcd; eassumption.
      + list_reasoning.
        apply LabelAssocList.binds_app_1 in H1. simpl in H1.
        destruct H1.
        * destr_ind IHty_var_inv1.
        * destr_ind IHty_var_inv2.
      + tidy_up.
      + eapply invertible_find_mu; eauto.
    - clear invertible_find_mu.
      intros. gen T H. dependent induction H0; intros; eauto.
  Qed.

  Lemma invertible_one_dec : forall G x (a : trm_label) T,
      G ⊢## x ⦂ {< decs_cons (dec_trm a T) decs_nil >} ->
      inert_env G ->
      exists T' DS',
        binds x (typ_obj DS') G /\
        lbinds a (dec_trm a T') (open x DS') /\
        G ⊢# T' <⦂ T.
  Proof.
    intros. eapply invertible_find_decs; eroutine.
    routine.
  Qed.

  Lemma invertible_val_rcd : forall G v DS,
      G ⊢##v v ⦂ typ_rcd DS ->
      exists ds, v = val_obj DS ds.
  Proof. dep induction on ty_val_inv; eauto. Qed.
  
  (* Following lemma is quite a bit difficult to deal with,
   * and it seems to expose a different style of undecidability,
   * which requires more customization. 
   *)
  
  Ltac boom :=
    destruct_lbinds;
    try match goal with
        | [H : _ |- _ ] =>
          guess_is_ind_hyp H;
          eapply H; eauto;
          solve_lbinds
        end.

  Local Hint Resolve lbinds_is_In.
  Lemma inert_intuitive_subtyping_decs : forall G x DS A,
      inert_env G ->
      G ⊢## x ⦂ {< DS >} ->
      forall S U,
      lbinds (label_typ A) (dec_typ A S U) DS ->
      G ⊢# S <⦂ x ⋅ A /\ G ⊢# x ⋅ A <⦂ U
  with inert_intuitive_subtyping_obj : forall G x DS A,
      inert_env G ->
      G ⊢## x ⦂ μ{ DS } ->
      forall S U,
        lbinds (label_typ A) (dec_typ A S U) (open x DS) ->
        G ⊢# S <⦂ x ⋅ A /\ G ⊢# x ⋅ A <⦂ U.
  Proof using.
    - clear inert_intuitive_subtyping_decs.
      dep induction on ty_var_inv; intros.
      exfalso. eapply inert_not_rcd; eassumption.
      1-5:clear inert_intuitive_subtyping_obj; boom.
      1-2:match goal with
          | [H : lbinds _ _ (to_list' (decs_cons _ decs_nil)) |- _] =>
            cbn in H; progressive_destructions
          end.
      + split; econstructor; try eassumption; boom.
      + eapply inert_intuitive_subtyping_obj; eauto.
    - clear inert_intuitive_subtyping_obj.
      dep induction on ty_var_inv; intros.
      + clear inert_intuitive_subtyping_decs.
        from_inert_env. from_inert_obj.
        split;
          [ eapply subtypt_sel1
          | eapply subtypt_sel2 ];
          apply search_to_precise_flow; eauto.
      + eapply inert_intuitive_subtyping_decs; eauto.
  Qed.

End InvertibleTyping.
Local Hint Resolve tight_to_invertible.

Lemma general_to_tight_typ : forall G t T,
    inert_env G ->
    G ⊢ t ⦂ T ->
    G ⊢# t ⦂ T
with general_to_tight_subtyp : forall G S U,
    inert_env G ->
    G ⊢ S <⦂ U ->
    G ⊢# S <⦂ U.
Proof.
  - clear general_to_tight_typ.
    induction on ty_trm; eroutine at 6.
  - clear general_to_tight_subtyp.
    induction on subtyp; eroutine at 6.
    all:eapply inert_intuitive_subtyping_decs;
      try apply tight_to_invertible;
      try apply general_to_tight_typ;
      auto; eauto;
        solve_lbinds.
Qed.
