Set Implicit Arguments.
Require Import Definitions.
Require Import TypeCheckSpecs.
Require Import Helpers.
Require Import TypingHelpers.
Require Import ObjectTraversal.
Require Import Measures.
Require Import Weakening.
Require Import OperationProperties.


(** models of exposure operation *)
Inductive exposure : env -> typ -> typ -> Prop :=
| exp_stop : forall G T,
    ~is_sel T ->
    exposure G T T
| exp_top : forall G T,
    exposure G T typ_top
| exp_bot : forall G1 G2 T x A,
    exposure G1 T typ_bot ->
    exposure (G2 ++ x ~ T ++ G1) (typ_sel x A) typ_bot
| exp_rcd : forall G1 G2 T x (A : typ_label) (DS : decs) L U U',
    exposure G1 T (typ_rcd DS) ->
    lbinds A (dec_typ A L U) DS ->
    exposure G1 U U' ->
    exposure (G2 ++ x ~ T ++ G1) (typ_sel x A) U'
| exp_mu : forall G1 G2 T (x : var) (A : typ_label) (DS : decs) l' U U',
    exposure G1 T (typ_obj DS) ->
    expo_mu (collect_ty_labs DS) x A (open x DS) l' U ->
    exposure G1 U U' ->
    exposure (G2 ++ x ~ T ++ G1) (typ_sel x A) U'.
Local Hint Constructors exposure.

Section ExposureProperties.

  Local Ltac by_weakening :=
    reassoc 4 with 3; apply weaken_subtyp; eassumption.

  Theorem exposure_sound : forall G T U,
      exposure G T U ->
      G ⊢ T <⦂ U.
  Proof.
    induction on exposure; trivial.
    all: lazymatch goal with
         | |- subtyp (?l2 ++ ?x ~ ?p ++ ?l1) _ _ =>
           pose proof (binds_for_sure l2 l1 x p)
         end.
    - eapply subtyp_sel2.
      eapply ty_sub. eauto.
      eapply subtyp_trans.
      + by_weakening.
      + instantiate (1 := typ_top). trivial.
    - eapply subtyp_sel2.
      eapply ty_sub. eapply decs_bind_typ; try eassumption.
      + eapply ty_sub. eauto. by_weakening.
      + eapply subtyp_typ. eauto.
        by_weakening.
    - eapply subtyp_trans.
      eapply expo_mu_is_correct; try eassumption.
      + apply ty_mu_elim. eapply ty_sub. eauto.
        by_weakening.
      + by_weakening.
  Qed.

  Theorem exposure_not_sel : forall G T U,
      exposure G T U ->
      ~is_sel U.
  Proof. induction on exposure; simpl; auto. Qed.    

  Local Ltac wf_env :=
    lazymatch goal with
    | H : wf_env (_ ++ _) |- _ => apply wf_deapp in H; invert H; subst
    end.

  Lemma exposure_find_lc : forall G T U,
      exposure G T U ->
      wf_env G -> lc T ->
      lc U.
  Proof.
    induction on exposure; intros; simpl; auto.
    - tidy_up. wf_env. apply IHexposure2; trivial.
      rec_pose IHexposure1 Hrec.
      apply lc_at_binds_decs with (n := 0) in H; routine.
    - tidy_up. wf_env.
      rec_pose IHexposure1 Hrec.
      apply expo_mu_preserves_lc in H; trivial.
      apply IHexposure2; trivial.
      apply open_lc_decs. routine.
  Qed.

  Lemma exposure_preserves_fv : forall G T U,
      exposure G T U ->
      fv U [<=] fv G `union` fv T.
  Proof.
    induction on exposure; simpl in *.
    1-3:set solve.
    - change (fv U' [<=] fv (G2 ++ x ~ T ++ G1) `union` singleton x).
      change (union (dom G1) (fv_values fv_typ G1)) with (fv G1) in *.
      repeat rewrite fv_union.
      apply fv_decs_shrinks in H. simpl in *.
      fsetdec.
    - change (fv U' [<=] fv (G2 ++ x ~ T ++ G1) `union` singleton x).
      change (union (dom G1) (fv_values fv_typ G1)) with (fv G1) in *.
      repeat rewrite fv_union.
      apply expo_mu_fv_bound in H. pose proof (fv_open_decs DS x 0).
      simpl in *. fsetdec.
  Qed.    
  
  Theorem exposure_preserves_wf : forall G T U,
      exposure G T U ->
      wf_env G ->
      fv T [<=] dom G -> lc T ->
      fv U [<=] dom G /\ lc U.
  Proof.
    induction on exposure; intros; auto.
    1-2:simpl; split; set solve.
    - wf_env. destruct IHexposure1; auto.
      pose proof H.
      apply lc_at_binds_decs with (n := 0) in H; [ | routine].
      apply fv_decs_shrinks in H5.
      simpl in *. 
      destruct IHexposure2; auto. routine.
      split; trivial.
      set simpl. simpl. fsetdec.
    - apply wf_deapp in H0. invert H0; subst.
      destruct IHexposure1; auto.
      pose proof H.
      apply expo_mu_effect in H5.
      apply exposure_mu_preserves_wf with (G := x ~ T ++ G1) in H; auto.
      destruct (is_sel_dec U); destruct_conjs.
      + subst. destruct H5; simpl in H5; [intuition |].
        specialize (H5 _ _ eq_refl).
        destruct IHexposure2; auto.
        * progressive_inversions.
          invert H15; subst. lia.
          simpl in *. eapply exclude_add; try eassumption.
          congruence.
        * split; trivial.
          set simpl. simpl. fsetdec.

      + inversion H2_0.
        all:subst; simpl in *; try contradiction.
        * split; trivial. 
          set simpl in *. simpl. fsetdec.
        * split; set solve.
  Qed.
  
End ExposureProperties.

Local Ltac expo_obli := rewrite app_length; simpl; lia.
Local Obligation Tactic := program_simpl; expo_obli.

Equations exposure_func (G : env) (T : typ) : typ by wf (length G) lt :=
  {
    exposure_func G T with is_sel_dec T => {
    | inleft (existT _ (avar_b _) (exist _ A _)) => typ_top;
    | inleft (existT _ (avar_f x) (exist _ A _)) with split_at x G => {
      | None => typ_top;
      | Some (existT _ T' (existT _ G1 _)) with exposure_func G1 T' => {
        | typ_bot => typ_bot;
        | typ_rcd DS with decs_typ_lookup A DS => {
          | None => typ_top;
          | Some (pair _ U) => exposure_func G1 U
          };
        | typ_obj DS
            with expo_mu_func (collect_ty_labs DS) x A (open x DS) => {
          | (pair _ U) => exposure_func G1 U
          };
        | _ => typ_top
        }
      };
    | inright _ => T
    }
  }.

Local Obligation Tactic := program_simpl.
Arguments exposure_func G T : simpl never.

Section ExposureProperties2.
  
  Theorem exposure_func_sound_wrt_spec : forall G T U,
    exposure_func G T = U ->
    exposure G T U.
  Proof.
    intros. funelim (exposure_func G T).
    all:rewrite <- Heqcall in *; clear Heqcall.
    all:simpl in *; try congruence. 
    all:try solve [tidy_up; auto]; subst.
    - tidy_up. constructor.
      rewrite Heq in *. trivial.
    - tidy_up. rewrite Heq0 in *.
      eapply exp_rcd; eauto.
      apply decs_typ_lookup_sound1. eassumption.
    - apply expo_mu_func_resp_spec in Heq.
      tidy_up. eapply exp_mu; eauto.
      rewrite Heq0 in *. trivial.
  Qed.

  Theorem exposure_func_sound : forall G T U,
      exposure_func G T = U ->
      G ⊢ T <⦂ U /\ ~is_sel U.
  Proof.
    intros. apply exposure_func_sound_wrt_spec in H. split.
    - apply exposure_sound. trivial.
    - eapply exposure_not_sel. eassumption.
  Qed.
  
End ExposureProperties2.
  
