Set Implicit Arguments.
Require Import Definitions.
Require Import TypeCheckSpecs.
Require Import OperationProperties.
Require Import Helpers.
Require Import SubstitutionLaws.
Require Import Measures.
Require Import TypingHelpers.
Require Import Weakening.

From Equations Require Import Equations.

Ltac discharge := simpl in *; auto; fold_cls; auto.

Ltac rev_neq H := revert H; fold any not.

Local Hint Resolve expo_resp_subtyp impo_resp_subtyp.
Local Hint Resolve prom_resp_subtyp dem_resp_subtyp.

Module Type DecisionEnv.

  Context (expo : env -> typ -> typ -> Prop).
  Context `{isExposure : IsExposure expo}.
  Notation "G ⊢E T ⇑ U" := (expo G T U) (at level 70).
  Context (exposurer : env -> typ -> typ).
  Context `{isExpoImpl : IsExpoImpl expo exposurer}.

  Context (impo : env -> typ -> typ -> Prop).
  Context `{isImposure : IsImposure impo}.
  Notation "G ⊢I T ⇓ U" := (impo G T U) (at level 70).
  Context (imposurer : env -> typ -> typ).
  Context `{isImpoImpl : IsImpoImpl impo imposurer}.

  Context (prom : env -> typ -> list var -> typ -> Prop).
  Context `{isPromotion : IsPromotion prom}.
  Notation "G ⊢P T // V ⇒ U" := (prom G T V U) (at level 70).
  Context (promoter : env -> typ -> list var -> typ).
  Context `{isPromImpl : IsPromImpl prom promoter}.

  Context (dem : env -> typ -> list var -> typ -> Prop).
  Context `{isDemotion : IsDemotion dem}.
  Notation "G ⊢D T // V ⇒ U" := (dem G T V U) (at level 70).
  Context (demoter : env -> typ -> list var -> typ).
  Context `{isDemImpl : IsDemImpl dem demoter}.

  Context (subtyper : env -> typ -> typ -> Tc unit).
  Context `{isSubtyper : IsSubtyper subtyper}.

End DecisionEnv.

Module TyperVarCase (Env : DecisionEnv).

  Import Env.

  Existing Instances isExposure isExpoImpl.
  Existing Instances isImposure isImpoImpl.
  Existing Instances isPromotion isPromImpl.
  Existing Instances isDemotion isDemImpl.
  Existing Instances isSubtyper.
    
  Ltac imposurer_wf :=
    match goal with
    | H : imposurer _ _ = _ |- _ =>
      apply func_resp_impo in H;
      apply impo_preserves_wf in H; trivial;
      destruct_conjs
    end.
  
  Ltac exposurer_wf :=
    match goal with
    | H : exposurer _ _ = _ |- _ =>
      apply func_resp_expo in H;
      apply expo_preserves_wf in H; trivial;
      destruct_conjs
    end.

  Ltac promoter_wf :=
    match goal with
    | H : promoter _ _ _ = _ |- _ =>
      apply func_resp_prom in H;
      apply prom_preserves_wf in H; trivial;
      destruct_conjs
    end.
  
  Ltac demoter_wf :=
    match goal with
    | H : demoter _ _ _ = _ |- _ =>
      apply func_resp_dem in H;
      apply dem_preserves_wf in H; trivial;
      destruct_conjs
    end.

  Ltac wf_tac :=
    repeat (wf_lookup || imposurer_wf || exposurer_wf || promoter_wf || demoter_wf).
  
  Inductive assignable : env -> var -> typ -> Prop :=
  | asn_to_mu : forall G x U DS,
      G ⊢I U ⇓ (μ{ DS }) ->
      assignable G x (typ_rcd (open x DS)) ->
      assignable G x U
  | asn_bind_mu : forall G x T DS U,
      binds x T G ->
      G ⊢E T ⇑ μ{DS} ->
      G ⊢ {< open x DS >} <⦂ U ->
      assignable G x U
  | asn_bind : forall G x T U,
      binds x T G ->
      G ⊢ T <⦂ U ->
      assignable G x U.
  Hint Constructors assignable.
  
  Theorem assignable_sound : forall G x T,
      assignable G x T ->
      G ⊢ trm_var x ⦂ T.
  Proof.
    induction on assignable.
    - eapply ty_sub.
      2:eapply impo_resp_subtyp; eassumption.
      auto.
    - eapply ty_sub.
      2:eassumption.
      apply ty_mu_elim.
      eapply ty_sub.
      constructor. eassumption.
      eapply subtyp_trans.
      apply expo_resp_subtyp. eassumption.
      trivial.
    - eapply ty_sub.
      constructor. eassumption.
      trivial.
  Qed.

  Local Hint Resolve binds_In.
  Theorem assignable_in_dom : forall G x T,
      assignable G x T ->
      x `in` dom G.
  Proof. induction on assignable; eauto. Qed.    
  
  (** TODO: synthesis is tricky. in the case of path type, it's probably the
   * best to keep following the path and stop at a point where V is escaped.
   *)
  Inductive syn_var : env -> var -> list var -> typ -> Prop :=
  | syn_stop : forall G x T V,
      binds x T G ->
      not_capture T V ->
      syn_var G x V T
  | syn_mu : forall G x T DS V DS',
      binds x T G ->
      G ⊢E T ⇑ μ{DS} ->
      G ⊢P {< open x DS >} // (In_remove x V) ⇒ {< DS' >} ->
      syn_var G x V (μ{ close x DS' })
  | syn_binds : forall G x T V U,
      binds x T G ->
      G ⊢P T // V ⇒ U ->
      syn_var G x V U.
  Hint Constructors syn_var.

  Local Hint Resolve binds_In.
  Theorem syn_var_preserves_wf : forall G x V T,
      syn_var G x V T ->
      wf_env G ->
      fv T [<=] dom G /\ lc T.
  Proof.
    induction on syn_var; intros.
    - rec_pose (wf_lookup _ _ H) Hx.
      trivial.
    - rec_pose (wf_lookup _ _ H) Hx.
      destruct_conjs.
      apply expo_preserves_wf in H0; eauto.
      destruct_conjs.
      destruct (@wf_typ_open G x DS); eauto.
      apply prom_preserves_wf in H1; trivial.
      destruct_conjs.
      apply wf_typ_close; routine.
    - eapply prom_preserves_wf; try eassumption.
      all:eapply wf_lookup; try eassumption.
  Qed.

  Theorem syn_var_sound : forall G x V T,
      syn_var G x V T ->
      wf_env G ->
      G ⊢ trm_var x ⦂ T.
  Proof.
    intros. destruct H.
    - eauto.
    - apply ty_mu_intro. eapply ty_sub.
      eapply ty_mu_elim. eapply ty_sub. eauto.
      apply expo_resp_subtyp. eassumption.
      rewrite open_left_inv_decs. 
      eapply prom_resp_subtyp; trivial. eassumption.

      rec_pose (wf_lookup _ _ H) Hx; trivial.
      destruct_conjs.
      apply expo_preserves_wf in H1; eauto.
      destruct_conjs.
      destruct (@wf_typ_open G x DS); eauto.
      apply prom_preserves_wf in H2; trivial.
      routine.
    - apply prom_resp_subtyp in H1.
      eapply ty_sub. apply ty_var. eassumption.
      all:trivial.
  Qed.
  
  Theorem syn_var_not_capture : forall G x V T,
      syn_var G x V T ->
      not_capture T V.
  Proof.
    intros. destruct H.
    - assumption.
    - apply prom_remove_vars in H1. unfold not_capture in *.
      eapply remove_from_list; try eassumption.
      apply fv_close_decs.
      apply fv_close_self_decs.
    - eapply prom_remove_vars. eassumption.
  Qed.

  (** NOW, let's get implementations started. *)
  Definition var_ch_subtyp_fail := "var checker: subtyper failed".
  
  Definition assn_func (G : env) (x : var) (U : typ) : TcE unit :=
    Tx <- option_to_TcE (get x G) ;;
    let Ti := imposurer G U in
    match is_obj_dec Ti with
    | inleft (exist _ DS _) =>
      let Te := exposurer G Tx in
      match is_obj_dec Te with
      | inleft (exist _ DS' _) =>
        inr $ subtyper G (typ_rcd (open x DS')) (typ_rcd (open x DS))
            !!!> var_ch_subtyp_fail
      | inright _ =>
        inr $ subtyper G Tx (typ_rcd (open x DS))
            !!!> var_ch_subtyp_fail
      end
    | inright _ =>
      let Te := exposurer G Tx in
      match is_obj_dec Te with
      | inleft (exist _ DS _) =>
        inr $ subtyper G (typ_rcd (open x DS)) U
            !!!> var_ch_subtyp_fail
      | inright _ =>
        inr $ subtyper G Tx U
            !!!> var_ch_subtyp_fail
      end
    end.

  Import ProofPackage.

  Theorem assn_func_resp_wf : forall G x U,
      wf_env G ->
      x `in` dom G ->
      fv U [<=] dom G -> lc U ->
      assn_func G x U <> inl err_case.
  Proof.
    intros. unfold assn_func. simpl. intro Contra.
    execute Contra.
    apply get_for_sure in H0; routine.
  Qed.
  
  Local Ltac asn_bind_mu :=
    eapply asn_bind_mu;
    [ eauto | | ];
    [ apply func_resp_expo; eassumption
    | apply func_resp_subtyp; auto ].

  Local Ltac asn_bind :=
    eapply asn_bind;
    [ eauto |];
    apply func_resp_subtyp; auto.

  Local Ltac asn_to_mu :=
    eapply asn_to_mu;
    [eapply func_resp_impo; eassumption
    |].
  
  Local Hint Resolve get_to_binds.
  Lemma assn_func_sound_wrt_assignable : forall G x T,
      assn_func G x T = inr (inr tt) ->
      uniq G ->
      assignable G x T.
  Proof.
    intros. unfold assn_func in H. simpl in H.
    execute H; destruct u.
    - asn_to_mu. asn_bind_mu.
    - asn_to_mu. asn_bind.
    - asn_bind_mu.
    - asn_bind.
  Qed.

  Theorem assn_func_sound : forall G x T,
      assn_func G x T = inr (inr tt) ->
      uniq G ->
      G ⊢ trm_var x ⦂ T.
  Proof.
    intros. apply assignable_sound.
    apply assn_func_sound_wrt_assignable;
      trivial.
  Qed.
  
  (** we need following three functions to provably discharge a goal
   *  that we don't want.
   *)
  Definition syn_var_func_aux' (x : var) (DS : typ) :
    match DS with
    | typ_rcd _ => typ
    | _ => Type
    end.
  Proof.
    refine (match DS as DS0
                  return DS0 = DS ->
                         match DS0 with
                         | typ_rcd _ => typ
                         | _ => Type
                         end with
            | typ_rcd DS => fun _ => typ_obj (close x DS)
            | _ => fun _ => False
            end eq_refl).
  Defined.

  Definition syn_var_unifier (x : var) (DS : typ) :
    is_rcd DS -> typ.
  Proof.
    intros. pose proof (syn_var_func_aux' x DS).
    destruct DS.
    all:simpl in *; try abstract contradiction.
    exact X.
  Defined.

  Lemma unifier_applied_to_Tc : forall x T pf,
      @syn_var_unifier x T pf =
      match T with
      | typ_rcd DS => typ_obj (close x DS)
      | _ => typ_top
      end.
  Proof.
    destruct T; simpl; intros; auto.
    all:contradiction.
  Qed.
  
  (* the error message complicates the proof here. just leave it. *)
  Definition syn_var_func_aux (G : env) (x : var) (DS : decs) (V : list var)
             (R : typ)
             (H : promoter G (typ_rcd (open x DS)) (In_remove x V) = R)
    : typ.
  Proof.
    refine (@syn_var_unifier x R _).
    
    abstract (apply func_prom_shape_invar in H;
              simpl in *; routine).
  Defined.
  
  Lemma aux_applied_to_rcd : forall G x DS T V pf,
      @syn_var_func_aux G x DS V T pf =
      match T with
      | typ_rcd DS => typ_obj (close x DS)
      | _ => typ_top
      end.
  Proof.
    intros. unfold syn_var_func_aux.
    apply unifier_applied_to_Tc. 
  Qed.
  
  Definition syn_var_func (G : env) (x : var) (V : list var) : TcE typ :=
    Tx <- option_to_TcE (get x G) ;;
    match not_capture_dec Tx V with
    | left _ => ret Tx
    | right _ =>
      let Tx' := exposurer G Tx in
      match is_obj_dec Tx' with
      | inleft (exist _ DS _) =>
        ret $ @syn_var_func_aux G x DS V
            (promoter G (typ_rcd (open x DS)) (In_remove x V))
            eq_refl
      | inright _ =>
        ret $ promoter G Tx V
      end
    end.

  Lemma syn_var_func_sound_wrt_syn_var : forall G x V T,
      syn_var_func G x V = inr (inr T) ->
      syn_var G x V T.
  Proof.
    intros. unfold syn_var_func in H. simpl in H.
    execute H; tidy_up.
    - apply syn_stop; eauto.
    - rewrite aux_applied_to_rcd. fold_cls.
      remember (promoter G (typ_rcd (open x x0)) (In_remove x V)) as T0.
      destruct (is_rcd_dec T0).
      + progressive_destruction. rewrite H.
        eapply syn_mu; eauto.
        * apply func_resp_expo. eauto.
        * apply func_resp_prom. congruence.
      + symmetry in HeqT0.
        apply func_prom_shape_invar in HeqT0.
        simpl in *. tidy_up. simpl in *.
        contradiction.
    - eapply syn_binds. eauto.
      apply func_resp_prom. trivial.
  Qed.
      
  Theorem syn_var_func_sound : forall G x V T,
      syn_var_func G x V = inr (inr T) ->
      wf_env G ->
      G ⊢ trm_var x ⦂ T.
  Proof.
    intros. eapply syn_var_sound; trivial.
    apply syn_var_func_sound_wrt_syn_var.
    eassumption.
  Qed.

  Theorem syn_var_func_preserves_wf : forall G x V T,
      syn_var_func G x V = inr (inr T) ->
      wf_env G ->
      fv T [<=] dom G /\ lc T.
  Proof.
    intros. eapply syn_var_preserves_wf; trivial.
    apply syn_var_func_sound_wrt_syn_var.
    eassumption.
  Qed.

  Theorem syn_var_func_not_capture : forall G x V T,
      syn_var_func G x V = inr (inr T) ->
      wf_env G ->
      not_capture T V.
  Proof.
    intros. eapply syn_var_not_capture; trivial.
    apply syn_var_func_sound_wrt_syn_var.
    eassumption.
  Qed.

  Theorem syn_var_fun_resp_wf : forall G x V,
      wf_env G ->
      x `in` dom G ->
      syn_var_func G x V <> inl err_case.
  Proof.
    intros. unfold syn_var_func. intro Contra.
    execute Contra.
    apply get_for_sure in H0; routine.
  Qed.

End TyperVarCase.

Module Typer (Env : DecisionEnv).
  
  Include TyperVarCase(Env).
  Import Env.

  Existing Instances isExposure isExpoImpl.
  Existing Instances isImposure isImpoImpl.
  Existing Instances isPromotion isPromImpl.
  Existing Instances isDemotion isDemImpl.
  Existing Instances isSubtyper.
  
  Reserved Notation "G '⊢⇑' t '//' V '⦂' T" (at level 70, t at level 79).
  Reserved Notation "G '⊢⇓' t '⦂' U" (at level 70, t at level 79).
  Reserved Notation "G ⊢⇓[ ds ⦂ DS ]" (at level 70, DS at level 79).
  
  Inductive ty_synthesis : env -> trm -> list var -> typ -> Prop :=
  | ty_syn_var : forall G x V T,
      syn_var G x V T -> G ⊢⇑ trm_var x // V ⦂ T

  | ty_syn_obj : forall x G ds DS V T,
      x `notin` fv G `union` fv ds `union` fv DS ->
      x ~ μ{DS} ++ G ⊢⇓[ open x ds ⦂ open x DS ] ->
      G ⊢P μ{DS} // V ⇒ T ->
      wf_defs ds ->
      G ⊢⇑ trm_val ([DS]{ ds }) // V ⦂ T

  | ty_syn_fun : forall x G T t V T' U,
      x `notin` fv G `union` fv T `union` fv t
        `union` AtomSetProperties.of_list V ->
      x ~ T ++ G ⊢⇑ open x t // V ⦂ U ->
      G ⊢D T // V ⇒ T' ->
      G ⊢⇑ trm_val (λ(T){ t }) // V ⦂ (typ_all T' (close x U))

  | ty_syn_sel1 : forall G x (a : trm_label) T DS U V U',
      binds x T G ->
      G ⊢E T ⇑ μ{DS} ->
      lbinds a (dec_trm a U) (open x DS) ->
      G ⊢P U // V ⇒ U' ->
      G ⊢⇑ trm_sel x a // V ⦂ U'
  | ty_syn_sel2 : forall G x (a : trm_label) T DS U V U',
      binds x T G ->
      G ⊢E T ⇑ {< DS >} ->
      lbinds a (dec_trm a U) DS ->
      G ⊢P U // V ⇒ U' ->
      G ⊢⇑ trm_sel x a // V ⦂ U'
  | ty_syn_sel3 : forall G x (a : trm_label) T V,
      binds x T G ->
      G ⊢E T ⇑ typ_bot ->
      G ⊢⇑ trm_sel x a // V ⦂ typ_bot

  | ty_syn_app1 : forall G x (y : var) T U1 U2 V U',
      binds x T G ->
      G ⊢E T ⇑ typ_all U1 U2 ->
      G ⊢⇓ trm_var y ⦂ U1 ->
      G ⊢P (open y U2) // V ⇒ U' ->
      G ⊢⇑ trm_app x y // V ⦂ U'
  | ty_syn_app2 : forall G x y T U V,
      binds x T G ->
      G ⊢E T ⇑ typ_bot ->
      binds y U G ->
      G ⊢⇑ trm_app x y // V ⦂ typ_bot

  | ty_syn_let1 : forall x G t1 t2 V T1 T2,
      G ⊢⇑ t1 // nil ⦂ T1 ->
      x `notin` fv G `union` fv t1 `union` fv t2
        `union` AtomSetProperties.of_list V `union` fv T1 ->
      x ~ T1 ++ G ⊢⇑ open x t2 // (x :: V) ⦂ T2 ->
      G ⊢⇑ lett t1 inn t2 // V ⦂ T2
  | ty_syn_let2 : forall x G t1 t2 V T1 T2,
      G ⊢⇓ t1 ⦂ T1 ->
      x `notin` fv G `union` fv t1 `union` fv t2
        `union` AtomSetProperties.of_list V `union` fv T1 ->
      x ~ T1 ++ G ⊢⇑ open x t2 // (x :: V) ⦂ T2 ->
      G ⊢⇑ lett t1 as T1 inn t2 // V ⦂ T2
        
  where "G ⊢⇑ t // V ⦂ T" := (ty_synthesis G t V T)%type
  with
  ty_check : env -> trm -> typ -> Prop :=
  | ty_ch_var : forall G x T,
      assignable G x T -> G ⊢⇓ trm_var x ⦂ T
  | ty_ch_obj : forall x G ds DS T,
      x `notin` fv G `union` fv ds `union` fv DS `union` fv T ->
      x ~ μ{DS} ++ G ⊢⇓[ open x ds ⦂ open x DS ] ->
      G ⊢ μ{DS} <⦂ T ->
      wf_defs ds ->
      G ⊢⇓ trm_val ([DS]{ ds }) ⦂ T

  | ty_ch_fun1 : forall x G T t Tc T' U,
      x `notin` fv G `union` fv T `union` fv t `union` fv Tc
        `union` fv T' `union` fv U ->
      G ⊢I Tc ⇓ (typ_all T' U) ->
      G ⊢ T' <⦂ T ->
      x ~ T ++ G ⊢⇓ open x t ⦂ open x U ->
      G ⊢⇓ trm_val (λ(T){ t }) ⦂ Tc 
  | ty_ch_fun2 : forall x G T t U T',
      x `notin` fv G `union` fv T `union` fv t `union` fv T' ->
      x ~ T ++ G ⊢⇑ open x t // nil ⦂ U ->
      G ⊢ typ_all T (close x U) <⦂ T' ->
      G ⊢⇓ trm_val (λ(T){ t }) ⦂ T'

  | ty_ch_sel1 : forall G x (a : trm_label) T DS U U',
      binds x T G ->
      G ⊢E T ⇑ μ{DS} ->
      lbinds a (dec_trm a U) (open x DS) ->
      G ⊢ U <⦂ U' ->
      G ⊢⇓ trm_sel x a ⦂ U'
  | ty_ch_sel2 : forall G x (a : trm_label) T DS U U',
      binds x T G ->
      G ⊢E T ⇑ {< DS >} ->
      lbinds a (dec_trm a U) DS ->
      G ⊢ U <⦂ U' ->
      G ⊢⇓ trm_sel x a ⦂ U'
  | ty_ch_sel3 : forall G x (a : trm_label) T U,
      binds x T G ->
      G ⊢E T ⇑ typ_bot ->
      G ⊢⇓ trm_sel x a ⦂ U

  | ty_ch_app1 : forall G x (y : var) T U1 U2 U',
      binds x T G ->
      G ⊢E T ⇑ typ_all U1 U2 ->
      G ⊢⇓ trm_var y ⦂ U1 ->
      G ⊢ (open y U2) <⦂ U' ->
      G ⊢⇓ trm_app x y ⦂ U'
  | ty_ch_app2 : forall G x y T T' U,
      binds x T G ->
      G ⊢E T ⇑ typ_bot ->
      binds y T' G ->
      G ⊢⇓ trm_app x y ⦂ U

  | ty_ch_let1 : forall x G t1 t2 T1 T2,
      G ⊢⇑ t1 // nil ⦂ T1 ->
      x `notin` fv G `union` fv t1 `union` fv t2
        `union` fv T1 `union` fv T2 ->
      x ~ T1 ++ G ⊢⇓ open x t2 ⦂ T2 ->
      G ⊢⇓ lett t1 inn t2  ⦂ T2
  | ty_ch_let2 : forall x G t1 t2 T1 T2,
      G ⊢⇓ t1 ⦂ T1 ->
      x `notin` fv G `union` fv t1 `union` fv t2
        `union` fv T1 `union` fv T2 ->
      x ~ T1 ++ G ⊢⇓ open x t2 ⦂ T2 ->
      G ⊢⇓ lett t1 as T1 inn t2 ⦂ T2
        
  where "G ⊢⇓ t ⦂ T" := (ty_check G t T)%type
  with
  defs_check : env -> defs -> decs -> Prop :=
  | ch_nil : forall G, G ⊢⇓[ defs_nil ⦂ decs_nil ]
  | ch_dtrm : forall G ds DS a t T,
      G ⊢⇓[ ds ⦂ DS ] ->
      G ⊢⇓ t ⦂ T ->
      G ⊢⇓[ defs_cons (def_trm a t) ds ⦂ decs_cons (dec_trm a T) DS ]
  | ch_dtyp : forall G ds DS A T,
      G ⊢⇓[ ds ⦂ DS ] ->
      G ⊢⇓[ defs_cons (def_typ A T) ds ⦂ decs_cons (dec_typ A T T) DS ]  
  where "G ⊢⇓[ ds ⦂ DS ]" := (defs_check G ds DS)%type.
  Local Hint Constructors ty_synthesis ty_check defs_check.

  Lemma ty_check_var_in_dom : forall G (x : var) T,
      G ⊢⇓ trm_var x ⦂ T ->
      x `in` dom G.
  Proof.
    intros. invert H. subst.
    eapply assignable_in_dom.
    eassumption.
  Qed.

  (** we need this relation in order to loop over all defs and decs *)
  Inductive def_dec_maps : defs -> decs -> Set :=
  | ddc_nil : def_dec_maps defs_nil decs_nil
  | ddc_trm : forall a t T ds DS,
      def_dec_maps ds DS ->
      def_dec_maps (defs_cons (def_trm a t) ds) (decs_cons (dec_trm a T) DS)
  | ddc_typ : forall A T ds DS,
      def_dec_maps ds DS ->
      def_dec_maps (defs_cons (def_typ A T) ds) (decs_cons (dec_typ A T T) DS).
  Hint Constructors def_dec_maps.

  Local Ltac yes :=
    solve [subst; left; simpl in *; auto].

  Local Ltac no :=
    let Contra := fresh "Contra" in
    solve [right; intro Contra; invert Contra; subst; intuition].

  (** we have to show def_dec_maps is a decidable relation. *)
  Definition def_dec_maps_dec ds DS : def_dec_maps ds DS + {def_dec_maps ds DS -> False}.
  Proof.
    gen DS.
    induction ds; destr on decs;
      try no.
    - yes.
    - destruct (IHds DS); try no.
      destruct d, d0; try no.
      + destruct (t == t1), (t0 == t2), (t2 == t3); try no.
        yes.
      + destruct (t == t1).
        yes. no.
  Defined.

  Inductive tail_of : defs -> defs -> Prop :=
  | to_refl : forall ds, tail_of ds ds
  | to_prep : forall D ds ds', tail_of ds ds' -> tail_of ds (defs_cons D ds').
  Hint Constructors tail_of.

  Lemma nil_tail : forall ds,
      tail_of defs_nil ds.
  Proof. induction on defs; routine. Qed.

  Lemma decons_tail : forall D ds ds',
      tail_of (defs_cons D ds) ds' ->
      tail_of ds ds'.
  Proof.
    dep induction on tail_of; auto.
    specialize (IHtail_of _ _ eq_refl).
    constructor. trivial.
  Qed.

  Lemma lower_cost_tail : forall a t ds ds',
      tail_of (defs_cons (def_trm a t) ds) ds' ->
      trm_struct_measure t <= defs_struct_measure ds'.
  Proof.
    dep induction on tail_of; simpl; try lia.
    specialize (IHtail_of _ _ _ eq_refl). lia.
  Qed.
  
  Local Ltac prove_termination :=
    try rewrite open_trm_same_measure; lia.
  
  Definition tm_result (tm : typing_mode) : Set :=
    match tm with
    | inl _ => typ
    | inr _ => unit
    end.

  Local Obligation Tactic := program_simpl; try prove_termination.

  (** tricky thing. mutual recursion on a measure over a trm. *)
  Equations ty_syn_ch_func (G : env) (t : trm) (tm : typing_mode) :
    TcE (tm_result tm) by wf (trm_struct_measure t) lt :=
    {
      (** ERROR CASES related to De Bruijn indices,
       * we discharge them later by well-formedness
       *)
      ty_syn_ch_func G (trm_var (avar_b n)) tm := err_out;
      ty_syn_ch_func G (trm_sel (avar_b n) _) tm := err_out;
      ty_syn_ch_func G (trm_app (avar_b n) _) tm := err_out;
      ty_syn_ch_func G (trm_app _ (avar_b n)) tm := err_out;

      (** TYPE SYNTHESIS DIRECTION *)
      ty_syn_ch_func G (trm_var (avar_f x)) (inl V) := syn_var_func G x V;
      
      ty_syn_ch_func G (trm_val (val_lam T t)) (inl V)
        with pick_fresh_var => {
      | exist _ x _ =>
        U <- ty_syn_ch_func (x ~ T ++ G) (open x t) (inl V)
          !!!> "synthesizer: recursive call failed";;
          ret (typ_all (demoter G T V) (close x U))
      };

      ty_syn_ch_func G (trm_sel (avar_f x) a) (inl V) :=
        Tx <- option_to_TcE (get x G) ;;
           match exposurer G Tx with
           | typ_bot => ret typ_bot
           | typ_rcd DS =>
             match decs_trm_lookup a DS with
             | Some U =>
               ret $ promoter G U V
             | None =>
               errete "synthesizer: lookup decs failed in sel case"
             end

           | typ_obj DS =>
             match (decs_trm_lookup a (open x DS)) with
             | Some U =>
               ret $ promoter G U V
             | None =>
               errete "synthesizer: lookup decs failed in sel case"
             end
           | _ =>
             errete "synthesizer: exposure results in a wrong type in sel case"
           end;

      ty_syn_ch_func G (trm_app (avar_f x) (avar_f y)) (inl V) :=
        Tx <- option_to_TcE (get x G) ;;
           match exposurer G Tx with
           | typ_bot =>
             option_to_TcE (get y G) ;;
                           ret typ_bot
           | typ_all U1 U2 =>
             (** we call check case here *)
             ty_syn_ch_func G (trm_var y) (inr U1)
                            !!!> "synthesizer: applicant type check failed in app case" ;;
                            ret $ promoter G (open y U2) V
           | _ =>
             errete "synthesizer: exposure results in a wrong type in app case"
           end;

      ty_syn_ch_func G (trm_let None t1 t2) (inl V)
        with ty_syn_ch_func G t1 (inl nil) => {
      | inl e => inl e;
      | inr (inl msgs) =>
        inr (inl ("synthesizer: failed to synthesize the binding term in let case"
                    :: msgs));
      | inr (inr T1) with pick_fresh_var => {
        | exist _ x _ =>
          ty_syn_ch_func (x ~ T1 ++ G) (open x t2) (inl (x :: V))
                         !!!> "synthesizer: failed to synthesize the body in let case"
        }
      };
      ty_syn_ch_func G (trm_let (Some T) t1 t2) (inl V)
        with ty_syn_ch_func G t1 (inr T) => {
      | inl e => inl e;
      | inr (inl msgs) =>
        inr (inl ("synthesizer: failed to check the binding term in let case"
                    :: msgs));
      | inr (inr _) with pick_fresh_var => {
        | exist _ x _ =>
          ty_syn_ch_func (x ~ T ++ G) (open x t2) (inl (x :: V))
                         !!!> "synthesizer: failed to synthesize the body in let case"
        }
      };

      (** TYPE CHECK DIRECTION *)
      ty_syn_ch_func G (trm_var (avar_f x)) (inr T) := assn_func G x T;

      ty_syn_ch_func G (trm_val (val_lam T t)) (inr Tk)
        with imposurer G Tk => {
      | Tki with is_all_dec Tki => {
        | inleft (existT _ T' (exist _ U _)) with pick_fresh_var => {
          | exist _ x _ =>
            ty_syn_ch_func (x ~ T ++ G) (open x t) (inr (open x U))
                           !!!> "checker: check body failed in function case" ;;
                           inr $ subtyper G T' T
                           !!!> "checker: subtyper failed in function case"
          };
        
        | inright _ with pick_fresh_var => {
          | exist _ x _ => 
            U <- ty_syn_ch_func (x ~ T ++ G) (open x t) (inl nil)
              !!!> "checker: recursive call failed in function case" ;;
              inr $ subtyper G (typ_all T (close x U)) Tk
              !!!> "checker: subtyper failed in function case"
          }
        }

      };

      ty_syn_ch_func G (trm_sel (avar_f x) a) (inr T) :=
        Tx <- option_to_TcE (get x G) ;;
           match exposurer G Tx with
           | typ_bot => ret tt

           | typ_rcd DS =>
             match decs_trm_lookup a DS with
             | Some U =>
               inr $ subtyper G U T
                   !!!> "checker: subtyper failed after decs lookup"
             | None =>
               errete "checker: lookup decs failed in sel case"
             end

           | typ_obj DS =>
             match (decs_trm_lookup a (open x DS)) with
             | Some U =>
               inr $ subtyper G U T
                   !!!> "checker: subtyper failed after decs lookup"
             | None =>
               errete "subtyper: lookup decs failed in sel case"
             end
           | _ =>
             errete "subtyper: exposure results in a wrong type in sel case"
           end;
      
      ty_syn_ch_func G (trm_app (avar_f x) (avar_f y)) (inr T) :=
        Tx <- option_to_TcE (get x G) ;;
           match exposurer G Tx with
           | typ_bot =>
             option_to_TcE (get y G) ;; ret tt
                           
           | typ_all U1 U2 =>
             ty_syn_ch_func G (trm_var y) (inr U1)
                            !!!> "checker: applicant type mismatch in app case";;
                            inr $ subtyper G (open y U2) T
                            !!!> "checker: subtyping result type failed in app case"

           | _ =>
             errete "checker: exposure results in a wrong type in app case"
           end;

      ty_syn_ch_func G (trm_let None t1 t2) (inr Tk)
        with ty_syn_ch_func G t1 (inl nil) => {
      | inl e => inl e;
      | inr (inl msgs) =>
        inr (inl ("checker: failed to synthesize the binding term in let case"
                    :: msgs));
      | inr (inr T1) with pick_fresh_var => {
        | exist _ x _ =>
          ty_syn_ch_func (x ~ T1 ++ G) (open x t2) (inr Tk)
                         !!!> "checker: failed to check the body in let case"
        }
      };
      ty_syn_ch_func G (trm_let (Some T) t1 t2) (inr Tk)
        with ty_syn_ch_func G t1 (inr T) => {
      | inl e => inl e;
      | inr (inl msgs) =>
        inr (inl ("checker: failed to check the binding term in let case"
                    :: msgs));
      | inr (inr _) with pick_fresh_var => {
        | exist _ x _ =>
          ty_syn_ch_func (x ~ T ++ G) (open x t2) (inr Tk)
                         !!!> "checker: failed to check the body in let case"
        }
      };

      (** OBJECT CASE
         *
         * we need to pull it out to be its own case, because we have to
         * tame recursion.
         *)
        ty_syn_ch_func G (trm_val (val_obj DS ds)) tm :=
          let ty_defs_func x {ds' DS'} (G' : env) (m : def_dec_maps ds' DS')
                      (tl : tail_of ds' (open x ds)) : TcE unit :=
              _ in
          let (x, _) := fresh_vars (ltac:(atoms_in_env)
                                    `union` match tm with
                                            | inl V => AtomSetProperties.of_list V
                                            | inr T => fv T
                                            end) in
          match def_dec_maps_dec (open x ds) (open x DS) with
          | inleft m =>
            ty_defs_func x (x ~ μ{DS} ++ G) m (to_refl _)
              !!!> "checker: object check failed" ;;
            let R : TcE (tm_result tm) :=
                match tm with
                | inl V =>
                  ret $ promoter G (μ{DS}) V
                | inr T =>
                  inr $ subtyper G (μ{DS}) T
                           !!!> "checker: subtyper failed in obejct case"
                end in
            R
          | inright _ =>
            errete "checker: defs and decs do not match well in object case"
          end
    }.
  
  Next Obligation.
    induction m.
    - exact (ret tt).
    - assert (trm_struct_measure t <
              S (S (decs_struct_measure DS + defs_struct_measure ds))). {
        pose proof (lower_cost_tail tl).
        rewrite open_defs_same_measure in H.
        lia.
      }
      exact
      (ty_syn_ch_func G' t (inr T) H !!!> "def checker: failed to check term" ;;
                      (IHm (decons_tail tl)) !!!> "def checker: recursive call failed").
    - apply IHm. eapply decons_tail. eassumption.
  Defined.

  Local Obligation Tactic := program_simpl.

  Fixpoint ty_defs_func {ds DS} (G : env) (m : def_dec_maps ds DS) : TcE unit :=
    match m with
    | ddc_nil => ret tt
    | @ddc_trm a t T ds DS rec =>
      ty_syn_ch_func G t (inr T) !!!> "def checker: failed to check term" ;;
      ty_defs_func G rec !!!> "def checker: recursive call failed"
    | @ddc_typ A T ds DS rec =>
      ty_defs_func G rec
    end.

  Definition ty_syn_ch_obj_func (G : env) (DS : decs) (ds : defs)
             (tm : typing_mode) : TcE (tm_result tm) :=
    let (x, _) := fresh_vars (ltac:(atoms_in_env)
                                    `union` match tm with
                                            | inl V => AtomSetProperties.of_list V
                                            | inr T => fv T
                                            end) in
    match def_dec_maps_dec (open x ds) (open x DS) with
    | inleft m =>
      ty_defs_func (x ~ μ{DS} ++ G) m
        !!!> "checker: object check failed" ;;
      let R : TcE (tm_result tm) :=
          match tm with
          | inl V =>
            ret $ promoter G (μ{DS}) V
          | inr T =>
            inr $ subtyper G (μ{DS}) T
                     !!!> "checker: subtyper failed in obejct case"
          end in
      R
    | inright _ =>
      errete "checker: defs and decs do not match well in object case"
    end.

  Definition equations_gen_obligation_obj (DS : decs) (ds : defs) :=
    ty_syn_ch_func_obligations_obligation_1 
            (fun (G0 : env) (t0 : trm) (tm2 : typing_mode)
               (_ : trm_struct_measure t0 < trm_struct_measure (trm_val ([DS ]{ ds})))
             => ty_syn_ch_func G0 t0 tm2).

  Lemma obj_discharge_obligation : forall DS ds G' ds' DS' x
                                     (m : def_dec_maps ds' DS')
                                     (tl : tail_of ds' (open x ds)),
      ty_defs_func G' m = equations_gen_obligation_obj DS ds x G' m tl.
  Proof.
    intros. unfold equations_gen_obligation_obj.
    unfold ty_syn_ch_func_obligations_obligation_1.
    induction m; simpl; trivial.
    destruct (ty_syn_ch_func G' t (inr T)); simpl; trivial.
    rewrite <- IHm. trivial.
  Qed.

  Lemma fold_obj_obligation : forall DS ds,
      equations_gen_obligation_obj DS ds =
      ty_syn_ch_func_obligations_obligation_1 
            (fun (G0 : env) (t0 : trm) (tm2 : typing_mode)
               (_ : trm_struct_measure t0 < trm_struct_measure (trm_val ([DS ]{ ds})))
             => ty_syn_ch_func G0 t0 tm2).
  Proof. reflexivity. Qed.
  
  Lemma ty_syn_ch_obj_case_eq : forall G DS ds tm,
      ty_syn_ch_func G (trm_val (val_obj DS ds)) tm = ty_syn_ch_obj_func G DS ds tm.
  Proof.
    intros. simp ty_syn_ch_func.
    unfold ty_syn_ch_obj_func. simpl.
    repeat lazymatch goal with
           | |- match ?expr with _ => _ end
               = match ?expr with _ => _ end =>
             destruct expr eqn:?; simpl; try congruence
           | |- (let _ := ?expr in _) = (let _ := ?expr in _) =>
             destruct expr eqn:?; simpl; try congruence
           end.
    rewrite <- fold_obj_obligation.
    rewrite <- obj_discharge_obligation. reflexivity.
  Qed.
  
  Import ProofPackage.

  Lemma uniq_dom_tail_of : forall d ds ds',
      tail_of (defs_cons d ds) ds' ->
      uniq_dom_defs ds' ->
      uniq_dom_def d.
  Proof.
    dep induction on tail_of; intros; eroutine.
  Qed.
  
  Lemma tail_of_preserves_wf : forall (G : env) d ds ds',
      tail_of (defs_cons d ds) ds' ->
      fv ds' [<=] dom G -> lc ds' ->
      fv d [<=] dom G /\ lc d.
  Proof.
    dep induction on tail_of; intros.
    - routine.
    - eapply IHtail_of; eauto.
      all:routine.
  Qed.

  Inductive tail_of_d : decs -> decs -> Prop :=
  | tod_refl : forall DS, tail_of_d DS DS
  | tod_prep : forall D DS DS', tail_of_d DS DS' -> tail_of_d DS (decs_cons D DS').
  Hint Constructors tail_of_d.

  Lemma decons_tail_d : forall D DS DS',
      tail_of_d (decs_cons D DS) DS' ->
      tail_of_d DS DS'.
  Proof.
    dep induction on tail_of_d; auto.
    specialize (IHtail_of_d _ _ eq_refl).
    constructor. trivial.
  Qed.

  Lemma tail_of_d_preserves_wf : forall (G : env) d DS DS',
      tail_of_d (decs_cons d DS) DS' ->
      fv DS' [<=] dom G -> lc DS' ->
      fv d [<=] dom G /\ lc d.
  Proof.
    dep induction on tail_of_d; intros.
    - routine.
    - eapply IHtail_of_d; eauto.
      all:routine.
  Qed.
  
  Local Hint Resolve decons_tail decons_tail_d.
  Local Hint Resolve binds_In get_to_binds.
  Local Hint Resolve decs_trm_lookup_sound1.
  Local Hint Resolve decs_trm_lookup_lc decs_trm_lookup_fv.
  Local Hint Resolve uniq_dom_trm_open.

  Local Hint Extern 5 (_ < _) => rewrite open_trm_same_measure; lia.
  
  Local Ltac psplit := split; [ | split].

  Local Ltac prom_not_capture :=
    eapply prom_remove_vars; apply func_resp_prom;
    eassumption.
  
  Program Fixpoint ty_syn_ch_func_sound_wrt_rules
          t tm {measure (trm_struct_measure t)}
    : forall G,
      uniq G ->
      uniq_dom_trm t ->
      match tm with
      | inl V =>
        forall T,
          ty_syn_ch_func G t (inl V) = inr (inr T) ->
          G ⊢⇑ t // V ⦂ T
      | inr T =>
        ty_syn_ch_func G t (inr T) = inr (inr tt) ->
        G ⊢⇓ t ⦂ T
      end := _.
  Next Obligation.

    assert (obj_case: forall DS ds,
               t = trm_val (val_obj DS ds) ->
               forall x G' ds' DS' (m : def_dec_maps ds' DS'),
                 tail_of ds' (open x ds) ->
                 ty_defs_func G' m = inr (inr tt) ->
                 uniq G' ->
                 G' ⊢⇓[ ds' ⦂ DS' ]). {
      induction m; intros; simpl in *; eauto.
      execute H3. destruct u.
      constructor.
      - apply IHm; eauto.
      - rec_pose (ty_syn_ch_func_sound_wrt_rules t0 (inr T)) Hrec; trivial.
        + apply lower_cost_tail in H2.
          rewrite open_defs_same_measure in H2.
          lia.
        + rec_pose (uniq_dom_defs_open ds 0 x) Huniq.
          eapply uniq_dom_tail_of in Huniq; eauto.
          routine.
    }
    
    destruct tm; [unfold program_branch_0 | unfold program_branch_1]; intros.
    all:clear program_branch_0 program_branch_1.
    - funelim (ty_syn_ch_func G t (inl l));
        repeat lazymatch goal with
               | H : ty_syn_ch_func _ _ _ = _ |- _ =>
                 simp ty_syn_ch_func in H
               end;
        try rewrite Heq0 in *; simpl in *; try rewrite Heq in *;
          simpl in *; try congruence.
      all:try rewrite <- Heqcall in *.
      + constructor.
        apply syn_var_func_sound_wrt_syn_var. trivial.
      + execute_steps 2 H2. rewrite <- fold_obj_obligation in *.
        rewrite <- obj_discharge_obligation in *.
        execute H2. destruct u.
        apply obj_case with (x := x) in Heqt; auto.
        econstructor.
        * instantiate (1 := x). simpl.
          fsetdec.
        * assumption.
        * eapply func_resp_prom. tidy_up. trivial.
        * routine.

      + execute H1; progressive_inversions.
        * eapply ty_syn_sel3; eauto.
          apply func_resp_expo. eassumption.
        * eapply ty_syn_sel2; eauto.
          -- apply func_resp_expo. eassumption.
          -- eapply func_resp_prom. trivial.
        * eapply ty_syn_sel1; eauto.
          -- apply func_resp_expo. eassumption.
          -- eapply func_resp_prom. trivial.

      + execute H2; progressive_inversions.
        * eapply ty_syn_app2; eauto.
          apply func_resp_expo. eassumption.
        * eapply ty_syn_app1; eauto.
          -- apply func_resp_expo. eassumption.
          -- destruct u.
             pose proof (ty_syn_ch_func_sound_wrt_rules (trm_var a0) (inr t0_1)).
             simpl in H2. apply H2; auto.
             constructor.
          -- apply func_resp_prom. trivial.

      + execute H2; progressive_inversions.
        constructor.
        * simpl. fsetdec.
        * pose proof (ty_syn_ch_func_sound_wrt_rules (open x t0) (inl l)).
          simpl in H2. apply H2; auto.
        * apply func_resp_dem. trivial.

      + destruct u. monads simpl in *.
        rec_pose (ty_syn_ch_func_sound_wrt_rules t0 (inr t)) Hrec1.
        econstructor; trivial.
        * instantiate (1 := x). simpl. fsetdec.
        * pose proof (ty_syn_ch_func_sound_wrt_rules (open x t1) (inl (x :: l))).
          simpl in H3. 
          apply H3; auto. routine.
          
      + monads simpl in *.
        rec_pose (ty_syn_ch_func_sound_wrt_rules t0 (inl nil)) Hrec1.
        econstructor; try eassumption.
        * instantiate (1 := x). simpl. fsetdec.
        * pose proof (ty_syn_ch_func_sound_wrt_rules (open x t1) (inl (x :: l))).
          simpl in H3. 
          apply H3; auto. routine.

    - funelim (ty_syn_ch_func G t (inr t0));
        repeat lazymatch goal with
               | H : ty_syn_ch_func _ _ _ = _ |- _ =>
                 simp ty_syn_ch_func in H
               end;
        try rewrite Heq0 in *; simpl in *; try rewrite Heq in *;
          simpl in *; try congruence; destruct_conjs.
      all:try rewrite <- Heqcall in *.
      (* all:simpl in *; try congruence; destruct_conjs. *)
      + constructor.
        apply assn_func_sound_wrt_assignable; trivial.
      + execute H2. rewrite <- fold_obj_obligation in *.
        rewrite <- obj_discharge_obligation in *.
        destruct u.
        apply obj_case with (x := x) in Heqt; auto.
        econstructor.
        * instantiate (1 := x). simpl. fsetdec.
        * assumption.
        * apply func_resp_subtyp; routine.
        * routine.

      + execute H1.
        * eapply ty_ch_sel3; eauto.
          eapply func_resp_expo. trivial.
        * eapply ty_ch_sel2; eauto.
          -- eapply func_resp_expo. trivial.
          -- apply func_resp_subtyp; routine.
        * eapply ty_ch_sel1; eauto.
          -- eapply func_resp_expo. trivial.
          -- apply func_resp_subtyp; routine.
          
      + execute H2.
        * eapply ty_ch_app2; eauto.
          eapply func_resp_expo. trivial.
        * eapply ty_ch_app1. eauto.
          -- eapply func_resp_expo. eassumption.
          -- destruct u.
             rec_pose (ty_syn_ch_func_sound_wrt_rules (trm_var a0) (inr t1_1)) Hrec.
             1:repeat constructor.
             trivial.
          -- apply func_resp_subtyp; routine.

      + execute H2; progressive_inversions.
        eapply ty_ch_fun1.
        2:eapply func_resp_impo; eassumption.
        2:apply func_resp_subtyp; trivial.
        -- instantiate (1 := x1). simpl. fsetdec.
        -- destruct u.
           pose proof (ty_syn_ch_func_sound_wrt_rules (open x1 t0)
                                                      (inr (open x1 x0))).
           simpl in H2. apply H2; auto.

      + execute H2; progressive_inversions. eapply ty_ch_fun2.
        3:apply func_resp_subtyp; eassumption.
        -- simpl. fsetdec.
        -- pose proof (ty_syn_ch_func_sound_wrt_rules (open x t0) (inl nil)).
           simpl in H2. apply H2; auto.

      + execute H2; progressive_inversions. destruct u.
        rec_pose (ty_syn_ch_func_sound_wrt_rules t0 (inr t)) Hrec1.
        eapply ty_ch_let2; trivial.
        * instantiate (1 := x). simpl. fsetdec.
        * pose proof (ty_syn_ch_func_sound_wrt_rules (open x t1) (inr t2)).
          simpl in H3. apply H3; auto.

      + execute H2; progressive_inversions.
        pose proof (ty_syn_ch_func_sound_wrt_rules t0 (inl nil)).
        simpl in H3. apply H3 in Heq0; auto; try lia.
        eapply ty_ch_let1; try eassumption.
        * instantiate (1 := x). simpl. fsetdec.
        * pose proof (ty_syn_ch_func_sound_wrt_rules (open x t1) (inr t)).
          simpl in H4. apply H4; auto.
  Qed.

  (** follow we prove the theory of bi-directional checking is sound wrt
   * to the language.
   *)
  
  Local Hint Resolve syn_var_sound syn_var_preserves_wf syn_var_not_capture.
  Local Hint Resolve not_capture_bot.
  Local Hint Resolve lookup_trm_lc_decs lookup_trm_closed_decs.
  Local Hint Resolve prom_remove_vars dem_remove_vars.
  Local Hint Resolve open_lc_trm wf_uniq.
  
  Local Hint Extern 1 (not_capture _ _) => eapply prom_remove_vars; eassumption.
  
  Local Ltac promo :=
    eapply ty_sub; [ | eapply prom_resp_subtyp; eauto ].

  Local Ltac expo :=
    eapply ty_sub; [ | eapply expo_resp_subtyp; eauto ].

  Local Ltac impo :=
    eapply ty_sub; [ | eapply impo_resp_subtyp; eauto ].
  
  Local Ltac bind_decs :=
    eapply decs_bind_typ; [ | eauto].

  Local Ltac ty_sub :=
    eapply ty_sub; [ | eassumption ].

  (** this is the same as proving the algorithm is correct. *)
  Theorem synthesis_sound : forall G t V T,
      G ⊢⇑ t // V ⦂ T ->
      wf_env G ->
      fv t [<=] dom G -> lc t ->
      G ⊢ t ⦂ T /\ not_capture T V /\ fv T [<=] dom G /\ lc T
    with check_sound : forall G t T,
        G ⊢⇓ t ⦂ T ->
        wf_env G ->
        fv t [<=] dom G -> lc t ->
        fv T [<=] dom G -> lc T ->
        G ⊢ t ⦂ T
    with defs_check_sound : forall G ds DS,
        G ⊢⇓[ ds ⦂ DS ] ->
        wf_env G ->
        fv ds [<=] dom G -> lc ds -> 
        fv DS [<=] dom G -> lc DS -> 
        G ⊩[ ds ⦂ DS ].
  Proof.
    - clear synthesis_sound.
      induction on ty_synthesis; intros.
      + eauto 6.
      + repeat split.
        3-4:apply prom_preserves_wf in H1; routine.
        * promo. econstructor; trivial.
          cofinite.
          apply open_subst_defs with (x := x).
          2-3:simpl in *; try fsetdec.
          all:auto.
        * apply prom_remove_vars in H1. trivial.

      + destruct IHty_synthesis; auto.
        destruct_conjs. repeat split.
        * eapply ty_sub.
          2:eapply subtyp_all; [ eapply dem_resp_subtyp | cofinite]; eauto.
          econstructor. cofinite.
          pose proof (@fv_close_self_typ U x 0).
          pose proof (fv_close_typ U x 0).
          apply open_subst_trm with (x := x).
          2-3:simpl in *; solve_notin.
          -- auto.
          -- rewrite open_left_inv_typ; auto.
        * apply dem_remove_vars in H0.
          unfold not_capture in *. rewrite Forall_forall in *.
          intros. simpl.
          specialize (H0 _ H9). specialize (H6 _ H9).
          pose proof (fv_close_typ U x 0).
          solve_notin.
        * apply dem_preserves_wf in H0.
          2-4:routine.
          destruct_conjs. simpl in *.
          pose proof (fv_close_typ U x 0).
          pose proof (@fv_close_self_typ U x 0).
          fsetdec.
        * apply dem_preserves_wf in H0.
          2-4:routine. destruct_conjs.
          constructor. routine.
          apply close_lc_typ. trivial.

      + psplit; auto.
        * promo. constructor.
          bind_decs. apply ty_mu_elim.
          expo. eauto.
        * assert (x `in` dom G). {
            eapply binds_In. eauto.
          }
          apply wf_lookup in H; trivial. destruct_conjs.
          apply expo_preserves_wf in H0; trivial. destruct_conjs.
          pose proof H1.
          apply lookup_trm_closed_decs with (G := G) in H1; auto.
          apply lookup_trm_lc_decs in H9; auto.
          apply prom_preserves_wf in H2; auto.

      + psplit; auto.
        * promo. constructor.
          bind_decs. expo. eauto.
        * assert (x `in` dom G). {
            eapply binds_In. eauto.
          }
          apply wf_lookup in H; trivial. destruct_conjs.
          apply expo_preserves_wf in H0; trivial. destruct_conjs.
          pose proof H1.
          apply lookup_trm_closed_decs with (G := G) in H1; auto.
          invert H8. subst.
          apply lookup_trm_lc_decs in H9; auto.
          apply prom_preserves_wf in H2; auto.

      + psplit; auto.
        * eapply ty_sub; [ |eauto].
          constructor. eapply ty_sub.
          eauto. eapply subtyp_trans.
          eapply expo_resp_subtyp. eassumption.
          auto.
        * simpl; split; auto. set solve.
          
      + psplit; auto.
        * promo. econstructor.
          expo. eauto.
          apply wf_lookup in H; trivial. destruct_conjs.
          apply expo_preserves_wf in H0; auto. destruct_conjs.
          eapply check_sound; try solve [routine].
        * apply wf_lookup in H; trivial. destruct_conjs.
          apply expo_preserves_wf in H0; auto. destruct_conjs.
          apply ty_check_var_in_dom in H1.
          apply prom_preserves_wf in H2; auto.

      + psplit; auto.
        * change typ_bot with (open y typ_bot).
          econstructor; eauto.
          eapply ty_sub. expo. eauto. trivial.
        * simpl; split; auto. set solve.

      + destruct IHty_synthesis1; discharge. routine.
        destruct_conjs.
        destruct IHty_synthesis2; auto. destruct_conjs.
        repeat split; trivial.
        * econstructor. eassumption. cofinite.
          rewrite <- (@open_lc_le_typ T2 0 0 x0); auto.
          apply open_subst_trm with (x := x).
          1:auto. 1:progressive_inversions; solve_notin.
          1:solve_notin.
          rewrite open_lc_le_typ with (n := 0); auto.
        * routine.
        * simpl in H9. set simpl in *.
          eapply disjoined_singleton. eassumption.
          invert H8. trivial.

      + destruct IHty_synthesis; auto.
        destruct_conjs.
        repeat split; trivial.
        * econstructor. apply check_sound; routine.
          cofinite. 
          rewrite <- (@open_lc_le_typ T2 0 0 x0); auto.
          apply open_subst_trm with (x := x).
          1:auto. 1:progressive_inversions; simpl in *; solve_notin.
          1:solve_notin.
          rewrite open_lc_le_typ with (n := 0); auto.
        * routine.
        * simpl in H7. set simpl in *.
          eapply disjoined_singleton. eassumption.
          invert H6. trivial.

    - clear check_sound.
      induction on ty_check; intros.
      + apply assignable_sound. trivial.
      + ty_sub. econstructor; trivial.
        cofinite. apply open_subst_defs with (x := x); auto.
        
      + impo. eapply ty_sub.
        * econstructor. cofinite.
          apply open_subst_trm with (x := x).
          4:apply IHty_check; auto.
          1:auto. 1-2:simpl in *; solve_notin.
          -- apply impo_preserves_wf in H0;
               destruct_conjs; discharge.
          -- apply open_lc_typ.
             apply impo_preserves_wf in H0;
               destruct_conjs; discharge.
             routine.
        * eapply subtyp_all. assumption.
          cofinite. trivial.
      + ty_sub. econstructor. cofinite.
        pose proof (fv_close_typ U x 0).
        pose proof (@fv_close_self_typ U x 0).
        apply open_subst_trm with (x := x).
        2-3:simpl in *; solve_notin.
        * auto.
        * apply synthesis_sound with (V := nil) in H0; auto.
          destruct_conjs.
          rewrite open_left_inv_typ; auto.
      + ty_sub. constructor.
        bind_decs. apply ty_mu_elim.
        expo. auto.
      + ty_sub. constructor.
        bind_decs. expo. auto.
      + eapply ty_sub. instantiate (1 := typ_bot).
        2:trivial.
        constructor. eapply ty_sub.
        expo. all:auto.

      + ty_sub. econstructor.
        * expo. auto.
        * apply wf_lookup in H; auto. destruct_conjs.
          apply expo_preserves_wf in H0; auto. destruct_conjs.
          apply ty_check_var_in_dom in H2.
          apply IHty_check; try solve [routine].
      + eapply ty_sub.
        * econstructor; [| eauto].
          eapply ty_sub. expo. auto.
          instantiate (1 := typ_bot). trivial.
        * simpl. trivial.

      + apply synthesis_sound in H; eauto.
        2,3:routine. destruct_conjs.
        econstructor. eassumption.
        cofinite.
        rewrite <- (@open_lc_le_typ T2 0 0 x0); auto.        
        apply open_subst_trm with (x := x).
        1:auto. 1-2:progressive_inversions; simpl in *; solve_notin.
        rewrite open_lc_le_typ with (n := 0); auto.
        assert (dom G [<=] union (singleton x) (dom G)) by set solve.
        apply IHty_check; discharge.
      + rec_pose IHty_check1 Ht1.
        econstructor. assumption.
        cofinite.
        rewrite <- (@open_lc_le_typ T2 0 0 x0); auto.
        apply open_subst_trm with (x := x).
        1:auto. 1-2:progressive_inversions; simpl in *; solve_notin.
        rewrite open_lc_le_typ with (n := 0); auto.
        assert (dom G [<=] union (singleton x) (dom G)) by set solve.
        apply IHty_check2; discharge.

    - clear defs_check_sound.
      induction on defs_check; intros; auto.
      + repeat constructor; routine.
      + routine.
  Qed.

  Theorem ty_syn_func_sound_wrt_rules :
    forall G t V T,
      wf_env G ->
      uniq_dom_trm t ->
      fv t [<=] dom G -> lc t ->
      ty_syn_ch_func G t (inl V) = inr (inr T) ->
      G ⊢⇑ t // V ⦂ T /\ not_capture T V /\ fv T [<=] dom G /\ lc T.
  Proof.
    intros.
    rec_pose (@ty_syn_ch_func_sound_wrt_rules t (inl V) G) Hresp.
    rec_pose (@synthesis_sound G t V T) Hsound.
    routine.
  Qed.

  Theorem ty_ch_func_sound_wrt_rules :
    forall G t T,
      wf_env G ->
      uniq_dom_trm t ->
      fv t [<=] dom G -> lc t ->
      fv T [<=] dom G -> lc T ->
      ty_syn_ch_func G t (inr T) = inr (inr tt) ->
      G ⊢⇓ t ⦂ T.
  Proof.
    intros.
    rec_pose (@ty_syn_ch_func_sound_wrt_rules t (inr T) G) Hresp.
    routine.
  Qed.
  
  Local Ltac lk_wf :=
    lazymatch goal with
    | _ : get ?a ?G = None |- _ =>
      assert (a `in` dom G) by routine
    end.

  Theorem ty_syn_ch_func_wf : forall G t tm,
      wf_env G ->
      fv t [<=] dom G -> lc t ->
      uniq_dom_trm t ->
      tm_constraint G tm ->
      ty_syn_ch_func G t tm <> inl err_case.
  Proof.
    intros G0 t0 tm0.
    pose proof (fun_elim (f := ty_syn_ch_func)) as H.
    apply H; clear H.
    all:intros; unfold tm_constraint in *; try congruence.
    all:destruct_conjs; reveal_err; simpl.
    all:try solve [progressive_inversions].
    all:try lazymatch goal with
            | H : _ = inl err_case |- inl err_case <> inl err_case =>
              intro Ok; rev_neq H;
                apply Hind; routine
            end.

    - apply syn_var_fun_resp_wf; routine.
    - apply assn_func_resp_wf; routine.

    - intro Contra. execute Contra.
      rewrite <- fold_obj_obligation in *.
      rewrite <- obj_discharge_obligation in *.
      assert (obj_rec : forall G' DS' ds' (m : def_dec_maps ds' DS'),
                 tail_of ds' (open x d0) ->
                 tail_of_d DS' (open x d) ->
                 wf_env G' ->
                 fv ds' [<=] dom G' -> lc ds' ->
                 fv DS' [<=] dom G' -> lc DS' ->
                 ty_defs_func G' m <> inl err_case). {
        induction m; intros; simpl in *; try congruence; intro Contra'.
        - execute Contra'.
          + rev_neq Heqt1.
            eapply H with (x2 := inr T); auto; try eassumption.

            pose proof (lower_cost_tail H5).
            rewrite open_defs_same_measure in H12.
            lia.
            all:routine.
            rec_pose (uniq_dom_defs_open d0 0 x) Huniq.
            eapply uniq_dom_tail_of in Huniq; eauto.
            routine.
          + apply IHm; auto. eapply decons_tail. eassumption.
            all:routine.
            eapply decons_tail_d. eassumption.
        - apply IHm; auto. eapply decons_tail. eassumption.
          all:routine.
          eapply decons_tail_d. eassumption.
      }
      rev_neq Heqt. apply obj_rec; discharge.

    - intro Contra. execute Contra.
      lk_wf.
      apply get_for_sure in H4; routine.
    - intro Contra. execute Contra.
      lk_wf. apply get_for_sure in H5; routine.

    - intro Contra. execute Contra.
      all:try lk_wf.
      + apply get_for_sure in H5; routine.
      + apply get_for_sure in H5; routine.
      + wf_tac. rev_neq Heqt0.
        apply H; discharge.
        * routine.
        * constructor.
        * split; discharge. routine.
    - intro Contra. execute Contra.
      all:try lk_wf.
      + apply get_for_sure in H6; routine.
      + apply get_for_sure in H6; routine.
      + wf_tac. rev_neq Heqt0.
        apply H; discharge.
        * routine.
        * constructor.
        * split; discharge. routine.

    - intro Contra. execute Contra.
      + rev_neq Heqt2.
        apply H; discharge.
        apply uniq_dom_trm_open. routine.

    - intro Contra. execute Contra.
      rev_neq Heqt3. fold_open_rec.
      apply H; discharge.
      + apply uniq_dom_trm_open. routine.
      + destruct_conjs. wf_tac. auto.

    - intro Contra. execute Contra.
      rev_neq Heqt3.
      apply H; discharge.
      apply uniq_dom_trm_open. routine.

    - intro Contra. execute Contra.
      rev_neq Contra. wf_tac. destruct u.
      apply H; auto.
      apply uniq_dom_trm_open. routine.

    - intro Contra. execute Contra.
      rev_neq Contra. wf_tac. destruct u.
      apply H; auto.
      + apply uniq_dom_trm_open. routine.
      + split; discharge.

    - intro Contra. execute Contra.
      rev_neq Contra. wf_tac.
      apply ty_syn_func_sound_wrt_rules in Heq0; auto.
      2-4:routine.
      apply H; auto.
      apply uniq_dom_trm_open. routine.

    - intro Contra. execute Contra.
      rev_neq Contra. wf_tac.
      apply ty_syn_func_sound_wrt_rules in Heq0; auto.
      2-4:routine.
      apply H; auto.
      + apply uniq_dom_trm_open. routine.
      + split; discharge.
  Qed.

  (** NOW, LET'S CONNECT THEM!!!! *)

  (** First, let's assert soundness. *)
  
  Theorem ty_syn_func_sound :
    forall G t V T,
      wf_env G ->
      uniq_dom_trm t ->
      fv t [<=] dom G -> lc t ->
      ty_syn_ch_func G t (inl V) = inr (inr T) ->
      G ⊢ t ⦂ T /\ not_capture T V /\ fv T [<=] dom G /\ lc T.
  Proof.
    intros. eapply synthesis_sound; trivial.
    destruct (@ty_syn_func_sound_wrt_rules G t V T); trivial.
  Qed.

  Theorem ty_ch_func_sound :
    forall G t T,
      wf_env G ->
      uniq_dom_trm t ->
      fv t [<=] dom G -> lc t ->
      fv T [<=] dom G -> lc T ->
      ty_syn_ch_func G t (inr T) = inr (inr tt) ->
      G ⊢ t ⦂ T.
  Proof.
    intros. eapply check_sound; trivial.
    rec_pose (@ty_ch_func_sound_wrt_rules G t T) Hs; trivial.
  Qed.  
  
  (** We then define a more general function with quantifiers.
   * at this point, we've discharged all err_case.
   *)

  Definition ty_syn_func_unifier (R : TcE typ) :
    R <> inl err_case ->
    Tc typ.
  Proof.
    intros. destruct R.
    - destruct e. congruence.
    - exact s.
  Defined.

  Lemma ty_syn_func_unifier_inv : forall R pf r,
      @ty_syn_func_unifier R pf = r ->
      R = inr r.
  Proof.
    intros. destruct R; simpl in *.
    - destruct e. congruence.
    - congruence.
  Qed.
  
  Definition ty_syn_func (G : env) (t : trm) (V : list var) :
    wf_env G ->
    fv t [<=] dom G -> lc t ->
    uniq_dom_trm t ->
    Tc typ.
  Proof.
    intros. refine (@ty_syn_func_unifier (ty_syn_ch_func G t (inl V)) _).
    apply ty_syn_ch_func_wf with (tm := inl V); simpl; trivial.
  Defined.
  
  (** then we define the true front end synthesizer. *)

  Definition ty_checker' (t : trm) :
    fv t [=] empty -> lc t -> uniq_dom_trm t -> Tc typ.
  Proof.
    intros. apply (@ty_syn_func nil t nil); trivial.
    - apply wf_nil.
    - set solve.
  Defined.
  
  Definition ty_checker (t : trm) : Tc typ :=
    match check_fv_trm t, check_lc_at_trm 0 t, uniq_dom_trm_dec t with
    | left fvbp, left lcp, left udt => ty_checker' fvbp lcp udt
    | right nfvbp, _, _ => erret "sanity: input term is not closed."
    | _, right nlcp, _ => erret "sanity: input term is not locally closed."
    | _, _, right nudt => erret "sanity: input term does not have unique domain."
    end.

  (** We end with proving [ty_checker] is sound. *)
  
  Theorem ty_checker_sound : forall t T,
      ty_checker t = inr T ->
      nil ⊢ t ⦂ T /\ fv T [=] empty /\ lc T.
  Proof.
    unfold ty_checker. unfold erret.
    intros. execute H. unfold ty_checker' in H.
    unfold ty_syn_func in H.
    apply ty_syn_func_unifier_inv in H.
    apply ty_syn_func_sound in H; trivial.
    - routine. fsetdec.
    - apply wf_nil.
    - set solve.
  Qed.
  
End Typer.
