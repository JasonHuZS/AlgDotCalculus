Set Implicit Arguments.
Require Import Definitions.

Require Export SubEnv.
Require Import Weakening.

Section NarrowingPrep.

  Local Hint Extern 1 =>
  match goal with
  | [ |- _ ⊢ _ <⦂ _ ] => idtac
  | [ |- _ ⊢ _ ⦂ _ ] => idtac
  end; reassoc 2 with 0.
  
  Local Hint Extern 1 => eapply weaken_trm.
  
  Lemma narrow_var :
    forall G G' x T,
      G' ⪯ G ->
      binds x T G ->
      G' ⊢ trm_var x ⦂ T.
  Proof.
    induction on subenv; routine.
    eapply ty_sub.
    - constructor. routine.
    - simpl_env.
      apply weaken_subtyp.
      trivial.
  Qed.
  
End NarrowingPrep.

Section Narrowing.

  Local Notation narrowing ctor :=
    (forall G t T, ctor G t T -> forall G',
          G' ⪯ G ->
          ctor G' t T).

  Local Ltac by_ind T :=
    lazymatch goal with
    | H : context[T] |- _ => clear H
    end;
    induction on T; eroutine.
  
  Lemma narrow_trm_gen : narrowing ty_trm
  with narrow_def_gen : narrowing ty_def
  with narrow_defs_gen : narrowing ty_defs
  with narrow_subtyp_gen : narrowing subtyp.
  Proof.
    - by_ind ty_trm;
        try (eapply narrow_var; eassumption).
      all:typing undec;
        match goal with
        | [ H : _ ⪯ _ |- _ ] => pose proof (subenv_implies_uniq H)
        end;
        eroutine at 6.
    - by_ind ty_def.
    - by_ind ty_defs.
    - by_ind subtyp.
      typing undec. routine.
      cofinite. eapply H0; auto.
      pose proof (subenv_implies_uniq H1).
      apply subenv_push; routine.
  Qed.
End Narrowing.

Lemma narrow_trm : forall G x t T U U',
  x `notin` dom G ->
  uniq G ->
  x ~ U ++ G ⊢ t ⦂ T ->
  G ⊢ U' <⦂ U ->
  x ~ U' ++ G ⊢ t ⦂ T.
Proof.
  intros. reassoc 2 with 0.
  eapply narrow_trm_gen. eassumption.
  simpl. apply subenv_last; trivial.
  auto.
Qed.

Lemma narrow_subtyp : forall G x T T' U U',
  x `notin` dom G ->
  uniq G ->
  x ~ U ++ G ⊢ T' <⦂ T ->
  G ⊢ U' <⦂ U ->
  x ~ U' ++ G ⊢ T' <⦂ T.
Proof.
  intros. reassoc 2 with 0.
  eapply narrow_subtyp_gen. eassumption.
  simpl. apply subenv_last; trivial.
  auto.
Qed.