Set Implicit Arguments.
From Equations Require Export Equations.

Require Import Definitions.
Require Import OperationProperties.
Require Export ListRelations.
Require Import Substitution.
Require Import Helpers.
Require Import OpeSub.

Require Import Coq.Logic.Decidable.

(** module level tactics *)

Ltac mass_discharge :=
  simpl; try eassumption; trivial;
  try lia;
  progressive_inversions;
  autorewrite with meta_ext;
  simpl in *;
  routine.

Ltac subgoal T tac :=
  let H := fresh "H" in
  unshelve evar (H : T);
  [ try solve [mass_discharge]
  | let H' := eval unfold H in H in
         clear H; tac H'].

Ltac rec_pose t name :=
  let rec go t' :=
      let T := type of t' in
      let sT := eval simpl in T in
  lazymatch sT with
  | ?T -> ?IGNORE =>
    subgoal T ltac:(fun gl => go constr:(t' gl))
  | forall x : ?T, ?IGNORE =>
    exvar T ltac:(fun x => go constr:(t' x))
  | _ => pose proof t' as name
  end in
      go t.

Ltac fv_trans := etransitivity; try eassumption; set solve.

Ltac rev_neq H := revert H; fold any not.
  
Ltac discharge := simpl in *; auto; fold_cls; auto.

Ltac expose_eq :=
  repeat match goal with
         | H1 : ?expr = _, H2 : ?expr = _ |- _ =>
           rewrite H1 in *; progressive_inversions
         end.

Ltac decons H := change (?t :: ?l) with (one t ++ l) in H.

(** proofs start here *)

(** predicates to help reduce case analysis. *)
Section Predicates.

  Definition is_top (T : typ) :=
    match T with
    | typ_top => True
    | _ => False
    end.

  Definition is_top_dec (T : typ) : {is_top T} + {~is_top T}.
  Proof. destruct T; simpl; auto. Defined.

  Definition is_bot (T : typ) :=
    match T with
    | typ_bot => True
    | _ => False
    end.

  Definition is_bot_dec (T : typ) : {is_bot T} + {~is_bot T}.
  Proof. destruct T; simpl; auto. Defined.

  Definition is_sel (T : typ) :=
    match T with
    | typ_sel _ _ => True
    | _ => False
    end.

  Definition is_sel_dec (T : typ) : { x & {A | T = typ_sel x A} } + {~is_sel T}.
  Proof. destruct T; simpl; eauto. Defined.

  Definition is_all (T : typ) :=
    match T with
    | typ_all _ _ => True
    | _ => False
    end.

  Definition is_all_dec (T : typ) : {S & { U | T = typ_all S U } } + {~is_all T}.
  Proof. destruct T; simpl; eauto. Defined.
  
  Definition is_rcd (T : typ) :=
    match T with
    | typ_rcd _ => True
    | _ => False
    end.

  Definition is_rcd_dec (T : typ) : {DS | T = typ_rcd DS } + {~is_rcd T}.
  Proof. destruct T; simpl; eauto. Defined.

  Definition is_obj (T : typ) :=
    match T with
    | typ_obj _ => True
    | _ => False
    end.

  Definition is_obj_dec (T : typ) : {DS | T = typ_obj DS } + {~is_obj T}.
  Proof. destruct T; simpl; eauto. Defined.

End Predicates.

Section Precords.

  Inductive precord : Set :=
  | pc_top : precord
  | pc_bot : precord
  | pc_sel (x : avar) (A : typ_label)
  | pc_all (T U : typ)
  | pc_rcd (DS : decs)
  | pc_mu (DS : decs) (l : list typ_label).

  Notation permits := (list (var * precord)).

  Definition is_pc_mu (T : precord) : Prop :=
    match T with
    | pc_mu _ _ => True
    | _ => False
    end.

  Definition is_pc_sel (T : precord) : Prop :=
    match T with
    | pc_sel x T => True
    | _ => False
    end.

  Definition is_pc_top (T : precord) : Prop :=
    match T with
    | pc_top => True
    | _ => False
    end.
  
  Definition is_pc_bot (T : precord) : Prop :=
    match T with
    | pc_bot => True
    | _ => False
    end.

  Definition is_pc_rcd (T : precord) : Prop :=
    match T with
    | pc_rcd _ => True
    | _ => False
    end.

  Definition is_pc_all (T : precord) : Prop :=
    match T with
    | pc_all _ _ => True
    | _ => False
    end.
  
  Definition is_pc_top_dec (T : precord) : {is_pc_top T} + {~is_pc_top T}.
  Proof. destruct T; simpl; auto. Defined.

  Definition is_pc_bot_dec (T : precord) : {is_pc_bot T} + {~is_pc_bot T}.
  Proof. destruct T; simpl; auto. Defined.
  
  Definition is_pc_sel_dec (p : precord) : {x & {A | p = pc_sel x A} } + {~is_pc_sel p}.
  Proof. destruct p; eauto. Defined.

  Definition is_pc_all_dec (T : precord) : {S & { U | T = pc_all S U } } + {~is_pc_all T}.
  Proof. destruct T; simpl; eauto. Defined.

  Definition is_pc_rcd_dec (T : precord) : {DS | T = pc_rcd DS } + {~is_pc_rcd T}.
  Proof. destruct T; simpl; eauto. Defined.
  
  Definition is_pc_mu_dec (p : precord) : {DS & {l | p = pc_mu DS l} } + {~is_pc_mu p}.
  Proof. destruct p; eauto. Defined.
  
  Definition typ_of (p : precord) : typ :=
    match p with
    | pc_top => typ_top
    | pc_bot => typ_bot
    | pc_sel x A => typ_sel x A
    | pc_all T U => typ_all T U
    | pc_rcd DS => typ_rcd DS
    | pc_mu DS l => typ_obj DS
    end.

    Fixpoint collect_ty_labs (DS : decs) :=
    match DS with
    | decs_nil => nil
    | decs_cons (dec_typ A _ _) DS' => A :: collect_ty_labs DS'
    | decs_cons D DS' => collect_ty_labs DS'
    end.
    
  Definition from_typ (T : typ) : precord :=
    match T with
    | typ_top => pc_top
    | typ_bot => pc_bot
    | typ_sel x A => pc_sel x A
    | typ_all T U => pc_all T U
    | typ_rcd DS => pc_rcd DS
    | typ_obj DS => pc_mu DS (collect_ty_labs DS)
    end.

  Definition mprecords (p1 p2 : precord) : precord :=
    match is_pc_sel_dec p1 with
    | inleft _ => p2
    | inright _ => p1
    end.

  Lemma mprecords_no_sel : forall p1 p2,
      ~is_pc_sel p2 ->
      ~is_pc_sel (mprecords p1 p2).
  Proof. destruct p1, p2; intros; auto. Qed.
  
  Lemma pc_sel_is_sel : forall U,
      is_pc_sel (from_typ U) <-> is_sel U.
  Proof. destruct U; simpl; intuition. Qed.

  Definition update_precord (p : precord) (l : list typ_label) : precord :=
    match p with
    | pc_mu DS _ => pc_mu DS l
    | _ => p
    end.

  Definition perm_of (p : precord) : option (list typ_label) :=
    match p with
    | pc_mu _ l => Some l
    | _ => None
    end.

  Definition to_env (P : permits) : env := map typ_of P.
  Arguments to_env P/.

  Lemma to_env_app : forall P1 P2,
      to_env (P1 ++ P2) = to_env P1 ++ to_env P2.
  Proof. simpl. intros. apply map_app. Qed.

  Lemma to_env_dom : forall P,
      dom (to_env P) = dom P.
  Proof. induction on list; routine. Qed.

  Lemma to_env_permits_uniq : forall P,
      uniq P <-> uniq (to_env P).
  Proof.
    induction on list; routine; tidy_up; auto.
  Qed.

  Lemma precord_same_typ_after_update : forall p l,
      typ_of (update_precord p l) = typ_of p.
  Proof. destr on precord; routine. Qed.

  Lemma typ_of_left_inv : forall T,
      typ_of (from_typ T) = T.
  Proof. destruct T; routine. Qed.
  
  Definition open_rec_precord (n : nat) (x : var) (p : precord) : precord :=
    match p with
    | pc_top => pc_top
    | pc_bot => pc_bot
    | pc_sel y A => pc_sel (open_rec n x y) A
    | pc_all T U => pc_all (open_rec n x T) (open_rec (S n) x U)
    | pc_rcd DS => pc_rcd (open_rec n x DS)
    | pc_mu DS l => pc_mu (open_rec (S n) x DS) l
    end.

  Global Instance OpenPrecord : CanOpen precord := { open_rec := open_rec_precord }.

  Definition close_rec_precord (n : nat) (x : var) (p : precord) : precord :=
    match p with
    | pc_top => pc_top
    | pc_bot => pc_bot
    | pc_sel y A => pc_sel (close_rec n x y) A
    | pc_all T U => pc_all (close_rec n x T) (close_rec (S n) x U)
    | pc_rcd DS => pc_rcd (close_rec n x DS)
    | pc_mu DS l => pc_mu (close_rec (S n) x DS) l
    end.
  
  Global Instance ClosePrecord : CanClose precord
    := { close_rec := close_rec_precord }.

  Definition fv_precord (T : precord) : atoms := fv (typ_of T).
  Arguments fv_precord T/.

  Global Instance FvPrecord : HasFv precord := { fv := fv_precord }.

  Lemma fv_typ_of_equiv : forall T,
      fv (typ_of T) [=] fv T.
  Proof. destruct T; simpl; set solve. Qed.

  Lemma fv_from_typ_equiv : forall T,
      fv (from_typ T) [=] fv T.
  Proof. destruct T; simpl; set solve. Qed.
  
  Inductive lc_pc_at : nat -> precord -> Prop :=
  | lc_pc_top : forall {n}, lc_pc_at n pc_top
  | lc_pc_bot : forall {n}, lc_pc_at n pc_bot
  | lc_pc_sel : forall {n v A}, lc_at n v -> lc_pc_at n (pc_sel v A)
  | lc_pc_all : forall {n T U}, lc_at n T -> lc_at (S n) U ->
                            lc_pc_at n (pc_all T U)
  | lc_pc_rcd : forall {n DS}, lc_at n DS -> lc_pc_at n (pc_rcd DS)
  | lc_pc_obj : forall {n DS l}, lc_at (S n) DS -> lc_pc_at n (pc_mu DS l).
  Hint Constructors lc_pc_at.
  
  Global Instance LcPrecord : LC precord := { lc_at := lc_pc_at }.

  Lemma lc_at_equiv_from_typ : forall n T,
      lc_at n (from_typ T) <-> lc_at n T.
  Proof.
    destruct T; simpl.
    all:split; routine.
  Qed.

  Lemma lc_at_equiv_typ_of : forall n T,
      lc_at n (typ_of T) <-> lc_at n T.
  Proof.
    destruct T; simpl.
    all:split; routine.
  Qed.
  
  Definition subst_precord (x y : var) (p : precord) : precord :=
    match p with
    | pc_top => pc_top
    | pc_bot => pc_bot
    | pc_sel z A => pc_sel (substi x y z) A
    | pc_all T U => pc_all (substi x y T) (substi x y U)
    | pc_rcd DS => pc_rcd (substi x y DS)
    | pc_mu DS l => pc_mu (substi x y DS) l
    end.

  Global Instance SubstPrecord : CanSubst precord := { substi := subst_precord }.

  Global Instance FvPermits : HasFv permits :=
    { fv := fun e => dom e `union` fv_values fv e }.
  
  Lemma fold_right_map : forall A B C D (l : list (var * A)) (p : B -> D) (f : A -> B) c (b : C),
      fold_right (fun (tup : var * A) acc => c acc $ let (_, a) := tup in p (f a)) b l
      = fold_right (fun (tup : var * B) acc => c acc $ let (_, a) := tup in p a) b (map f l).
  Proof.
    induction l; intros; simpl; auto.
    tidy_up. rewrite IHl. auto.
  Qed.
  
  Lemma fv_permits : forall P,
      fv P = fv (to_env P).
  Proof.
    induction on list; intros; auto.
    destruct a. simpl. rewrite to_env_dom.
    f_equal. unfold fv_precord. f_equal. unfold fv_values.
    apply fold_right_map.
  Qed.
  
  Lemma fv_permits_deapp : forall (P1 P2 : permits),
      fv P2 [<=] fv (P1 ++ P2).
  Proof.
    intros. repeat rewrite fv_permits.
    rewrite to_env_app. apply fv_deapp.
  Qed.
  
End Precords.
Hint Rewrite -> to_env_dom : meta_ext.
Hint Constructors lc_pc_at.
Arguments fv_precord T/.

Notation permits := (list (var * precord))%type.

Ltac gather_atoms ::=
  let A := gather_atoms_with (fun P : permits => fv P) in
  let L := default_gather_atoms in
  constr:(L `union` A).

Section WellFormedEnv.
  
  Inductive wf_env : env -> Prop :=
  | wf_nil : wf_env nil
  | wf_cons : forall {x T G}, x \notin fv T \u fv G ->
                         fv T [<=] dom G ->
                         lc T ->
                         wf_env G ->
                         wf_env (x ~ T ++ G).
  Hint Constructors wf_env.

  Lemma wf_decons : forall x T G, wf_env (x ~ T ++ G) -> x \notin fv T.
  Proof. routine. Qed.

  Lemma wf_deapp : forall G1 G2, wf_env (G1 ++ G2) -> wf_env G2.
  Proof. induction on env; routine. Qed.

  Lemma wf_uniq : forall G, wf_env G -> uniq G.
  Proof. induction on env; routine. Qed.

  Lemma wf_var_in : forall G1 G2 v,
      wf_env (G1 ++ G2) ->
      v `in` dom G2 ->
           v `notin` dom G1.
  Proof.
    induction G1; intros.
    - routine.
    - tidy_up. repeat rewrite dom_app in *.
      destruct_notin.
      rewrite AtomSetProperties.add_union_singleton.
      intro Contra.
      apply AtomSetImpl.union_1 in Contra.
      destruct Contra.
      + apply AtomSetImpl.singleton_1 in H1. subst.
        intuition.
      + eapply IHG1; eassumption.
  Qed.        

End WellFormedEnv.

Section WellFormedStructures.

  Theorem wf_lookup : forall G x T,
      binds x T G ->
      wf_env G ->
      fv T [<=] dom G /\ lc T.
  Proof.
    induction on env; intros.
    - contradiction.
    - destruct H0; progressive_destructions.
      + constructor; set solve.
      + edestruct IHlist; try eassumption.
        constructor; set solve.
  Qed.
  
  Theorem wf_typ_open : forall (G : env) x DS,
      x `in` dom G ->
      fv (μ{DS}) [<=] dom G ->
      lc (μ{DS}) ->
      fv ({< open x DS >}) [<=] dom G /\ lc ({< open x DS >}).
  Proof.
    intros. split.
    - pose proof (fv_open_decs DS x 0).
      etransitivity. eassumption.
      set simpl. split; set solve.
    - constructor. apply open_lc_decs. routine.
  Qed.

  Theorem wf_typ_close : forall (G : env) x DS,
      fv ({< DS >}) [<=] dom G ->
      lc ({< DS >}) ->
      fv (μ{ close x DS }) [<=] dom G /\ lc (μ{ close x DS }).
  Proof.
    intros. split.
    - pose proof (fv_close_decs DS x 0).
      etransitivity. eassumption.
      set solve.
    - constructor. apply close_lc_decs. routine.
  Qed.
  
End WellFormedStructures.

Ltac wf_lookup :=
  match goal with
  | _ : wf_env ?G, H : get _ ?G = Some _ |- _ =>
    apply get_to_binds in H;
    apply wf_lookup in H; trivial;
    destruct_conjs
  end.

Inductive tail_of : decs -> decs -> Prop :=
| to_refl : forall DS, tail_of DS DS
| to_cons : forall DS' DS D,
    tail_of DS' DS ->
    tail_of DS' (decs_cons D DS).
Hint Constructors tail_of.

Lemma tail_of_lbinds : forall D DS DS',
    tail_of DS' DS ->
    lbinds (lab_dec_of D) D DS' ->
    lbinds (lab_dec_of D) D DS.
Proof.
  induction on tail_of; auto; intros.
  right. apply IHtail_of. assumption.
Qed.

Lemma tail_of_decons : forall D DS DS',
    tail_of (decs_cons D DS') DS ->
    tail_of DS' DS.
Proof. dep induction on tail_of; eauto. Qed.

Section Specifications.

  (** T does not capture a list of vars *)
  Definition not_capture (T : typ) (vs : list var) :=
    Forall (fun v => v `notin` fv T) vs.
  Arguments not_capture T vs/.
  
  Definition not_capture_dec T V : {not_capture T V} + {~not_capture T V}.
  Proof.
    apply Forall_dec. intros.
    apply Sumbool.sumbool_not.
    destruct (AtomSetProperties.In_dec x (fv T)).
    - left. unfold not.
      rewrite not_not_iff; trivial.
      left. trivial.
    - auto.
  Qed.   
  
  Lemma not_capture_bot : forall V,
      not_capture typ_bot V.
  Proof.
    induction on list; unfold not_capture; simpl;
      try solve [routine].
    constructor; trivial.
  Qed.
  
  Definition promo_shape (S U : typ) : Set :=
    match S with
    | typ_top => U = typ_top
    | typ_bot => U = typ_bot
    | typ_sel _ _ => unit
    | typ_all _ _ => {S & {T | U = typ_all S T} }
    | typ_rcd _ => {DS' | U = typ_rcd DS'}
    | typ_obj DS => U = typ_obj DS \/ U = typ_top
    end.

  Definition demo_shape (S U : typ) : Set :=
    match S with
    | typ_top => U = typ_top
    | typ_bot => U = typ_bot
    | typ_sel _ _ => unit
    | typ_all _ _ => {S & {T | U = typ_all S T} }
    | typ_rcd _ => {DS' | U = typ_rcd DS'}
    | typ_obj DS => U = typ_obj DS \/ U = typ_bot
    end.

  (** typing mode
   *
   * in synthesis mode, we want to remove a set of variables from the resulting type
   * in check mode, we want to make sure the witness give precisely the same type.
   *)
  Definition typing_mode : Set := (list var) + typ.

  Definition tm_constraint (G : env) (tm : typing_mode) :=
    match tm with
    | inl _ => True
    | inr T => fv T [<=] dom G /\ lc T 
    end.
  Arguments tm_constraint G tm/.

End Specifications.

Section Characteristics.
  
  Class IsExposure (expo : env -> typ -> typ -> Prop) : Set :=
    {
      expo_resp_subtyp : forall G T U, expo G T U -> G ⊢ T <⦂ U;
      expo_not_sel : forall G T U, expo G T U -> ~is_sel U;
      expo_preserves_wf : forall G T U,
          expo G T U ->
          wf_env G ->
          fv T [<=] dom G ->
          lc T ->
          fv U [<=] dom G /\ lc U
    }.

  Class IsExpoImpl (expo : env -> typ -> typ -> Prop) (f : env -> typ -> typ) : Set :=
    {
      func_resp_expo : forall G T U,
        f G T = U ->
        expo G T U
    }.
  
  Class IsImposure (impo : env -> typ -> typ -> Prop) : Set :=
    {
      impo_resp_subtyp : forall G T U, impo G T U -> G ⊢ U <⦂ T;
      impo_not_sel : forall G T U, impo G T U -> ~is_sel U;
      impo_preserves_wf : forall G T U,
          impo G T U ->
          wf_env G ->
          fv T [<=] dom G ->
          lc T ->
          fv U [<=] dom G /\ lc U
    }.

  Class IsImpoImpl (impo : env -> typ -> typ -> Prop) (f : env -> typ -> typ) : Set :=
    {
      func_resp_impo : forall G T U,
        f G T = U ->
        impo G T U
    }.
  
  Class IsPromotion (prom : env -> typ -> list var -> typ -> Prop) : Type :=
    {
      prom_resp_subtyp : forall G T V U,
        prom G T V U ->
        wf_env G ->
        G ⊢ T <⦂ U;
      prom_remove_vars : forall G T V U,
          prom G T V U ->
          not_capture U V;
      prom_preserves_wf : forall G T V U,
          prom G T V U ->
          wf_env G ->
          fv T [<=] dom G ->
          lc T ->
          fv U [<=] dom G /\ lc U;
    }.

  Class IsPromImpl (prom : env -> typ -> list var -> typ -> Prop)
        (f : env -> typ -> list var -> typ) : Set :=
    {
      func_resp_prom : forall G T V U,
        f G T V = U ->
        prom G T V U;
      func_prom_shape_invar : forall G T V U,
        f G T V = U ->
        promo_shape T U
    }.

  Class IsDemotion (dem : env -> typ -> list var -> typ -> Prop) : Type :=
    {
      dem_resp_subtyp : forall G T V U,
        dem G T V U ->
        wf_env G ->
        G ⊢ U <⦂ T;
      dem_remove_vars : forall G T V U,
          dem G T V U ->
          not_capture U V;
      dem_preserves_wf : forall G T V U,
          dem G T V U ->
          wf_env G ->
          fv T [<=] dom G ->
          lc T ->
          fv U [<=] dom G /\ lc U
    }.

  Class IsDemImpl (dem : env -> typ -> list var -> typ -> Prop)
        (f : env -> typ -> list var -> typ) : Set :=
    {
      func_resp_dem : forall G T V U,
        f G T V = U ->
        dem G T V U;
      func_dem_shape_invar : forall G T V U,
          f G T V = U ->
          demo_shape T U
    }.

  Class IsEval (eval : permits -> precord -> permits -> precord -> Prop) : Set :=
    {
      eval_resp_subtyp : forall P T P' U,
        eval P T P' U ->
        to_env P ⊢ typ_of T <⦂ typ_of U;
      eval_not_sel : forall P T P' U,
          eval P T P' U ->
          ~is_pc_sel U;
      eval_ope_sub : forall P T P' U,
          eval P T P' U ->
          ope_sub (to_env P') (to_env P);
      eval_preserves_wf : forall P T P' U,
          eval P T P' U ->
          wf_env (to_env P) ->
          fv T [<=] dom P -> lc T ->
          wf_env (to_env P') /\ fv P' [<=] fv P /\ fv U [<=] dom P' /\ lc U
    }.

  Class IsEvalImpl (eval : permits -> precord -> permits -> precord -> Prop)
        (f : permits -> precord -> permits * precord) : Set :=
    {
      func_resp_eval : forall P T P' T',
        f P T = (P', T') ->
        eval P T P' T'
    }.
    
  Class IsSubtyper (f : env -> typ -> typ -> Tc unit) : Type :=
    {
      func_resp_subtyp : forall G T U,
        f G T U = (inr tt) ->
        uniq G ->
        G ⊢ T <⦂ U
    }.
  
End Characteristics.

Module ProofPackage.

  Hint Extern 5 (_ `notin` _) => fsetdec.
  Hint Extern 5 (wf_env (_ ++ _)) => constructor; set solve; routine.
  Hint Extern 5 (wf_env (_ :: _)) => constructor; set solve; routine.
  
  Hint Extern 5 =>
  lazymatch goal with
  | |- context [fv (open ?x ?t)] =>
    try pose proof (fv_open_trm t x 0);
      try pose proof (fv_open_typ t x 0);
      try pose proof (fv_open_defs t x 0);
      try pose proof (fv_open_decs t x 0);
      fv_trans
  end.
  Hint Extern 5 => apply open_lc_defs; routine.
  Hint Extern 5 => apply open_lc_decs; routine.
  Hint Extern 5 => apply open_lc_trm; routine.
  Hint Extern 5 => apply open_lc_typ; routine.

End ProofPackage.
