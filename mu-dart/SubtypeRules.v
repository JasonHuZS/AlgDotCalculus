Set Implicit Arguments.
Require Import Definitions.
Require Import TypeCheckSpecs.
Require Import TypingHelpers.
Require Import SubstitutionLaws.
Require Import Helpers.
Require Import OpeSub.
Require Import Measures.
Require Import SubtypingMeasures.
Require Import Revealing.
Require Import Weakening.
Require Import OperationProperties.

Inductive upcast : permits -> avar -> typ_label -> (* inputs *)
                   permits -> typ -> (* outputs *)
                   Prop :=
| uc_top : forall P x A,
    upcast P x A nil typ_top
| uc_bot : forall P1 P2 P1' T (x : var) A,
    revealing P1 T P1' pc_bot ->
    upcast (P2 ++ x ~ T ++ P1) x A nil typ_bot
| uc_rcd : forall P1 P2 T P1' (x : var) (A : typ_label) (DS : decs) L U,
    revealing P1 T P1' (pc_rcd DS) ->
    lbinds A (dec_typ A L U) DS ->
    upcast (P2 ++ x ~ T ++ P1) x A P1' U
| uc_mu : forall P1 P2 P1' T (x : var) (A : typ_label) (DS : decs) l L U,
    revealing P1 T P1' (pc_mu DS l) ->
    In A l ->
    lbinds A (dec_typ A L U) (open x DS) ->
    upcast (P2 ++ x ~ T ++ P1) x A (x ~ (pc_mu DS (In_remove A l)) ++ P1') U.

Local Ltac by_weakening :=
  reassoc 4 with 3; apply weaken_subtyp; eassumption.

Section UpcastProperties.

  Local Hint Constructors upcast.
  
  Theorem upcast_decreases_measure : forall P x A P' U,
    upcast P x A P' U ->
    subtyp_measure P (typ_sel x A) > subtyp_measure P' U.
  Proof.
    intros. dependent induction H; subst; simpl.
    all:try lazymatch goal with
            | H : revealing _ _ _ _ |- _ =>
              let H' := fresh "H'" in
              pose proof H as H';
                apply revealing_maintain_reveal_measure in H';
                simpl in H'
            end.
    all:permits_simpl; try lia.
    - apply revealing_maintain_reveal_measure in H. simpl in H.
      apply decs_measure_lbinds_typ in H0. destruct H0.
      lia.
    - change (total ?l) with (permits_weight P1').
      edestruct pc_mu_measure_lbinds; try eassumption.
      repeat rewrite pc_mu_measure_open_invar in *.
      lia.
  Qed.

  Hint Resolve ope_sub_refl ope_sub_nil_any ope_sub_trans ope_sub_app_r.
  Hint Constructors ope_sub.

  Theorem upcast_sound : forall P x A P' U,
      upcast P x A P' U ->
      to_env P ⊢ typ_sel x A <⦂ U /\ ope_sub (to_env P') (to_env P).
  Proof.
    induction on upcast.
    all:repeat rewrite to_env_app in *.
    all:change (to_env (?x ~ ?T)) with (x ~ typ_of T).
    all:try lazymatch goal with
            | |- subtyp (?l2 ++ ?x ~ ?p ++ ?l1) _ _ /\ _ =>
              pose proof (binds_for_sure l2 l1 x p)
            end.
    all:repeat rewrite typ_of_left_inv in *; destruct_conjs.
    all:try solve [repeat split; auto].
    - apply revealing_sound in H. destruct_conjs.
      repeat split; auto.
      eapply subtyp_sel2.
      eapply ty_sub. eauto.
      eapply subtyp_trans.
      + by_weakening.
      + instantiate (1 := typ_top). trivial.

    - apply revealing_sound in H. destruct_conjs.
      repeat split; auto.
      eapply subtyp_sel2.
      eapply decs_bind_typ; try eassumption.
      eapply ty_sub. eauto. by_weakening.
    - apply revealing_sound in H. destruct_conjs.
      repeat split; auto.
      + eapply subtyp_sel2.
        eapply decs_bind_typ; try eassumption.
        apply ty_mu_elim.
        eapply ty_sub. eauto. by_weakening.
      + simpl. auto.
  Qed.

  Hint Resolve wf_nil fv_deapp.
  Theorem upcast_preserves_wf : forall P (x : var) A P' U,
      upcast P x A P' U ->
      wf_env (to_env P) ->
      x `in` dom P ->
      wf_env (to_env P') /\ fv P' [<=] fv P /\ fv U [<=] dom P' /\ lc U.
  Proof.
    induction on upcast; intros.
    all:repeat rewrite to_env_app in *.
    all:change (to_env (?x ~ ?T)) with (x ~ typ_of T).
    1-2:simpl; repeat split; set solve.
    - wf_env. simpl_pc.
      apply revealing_preserves_wf in H; trivial.
      destruct_conjs.
      pose proof H1.
      apply lc_at_binds_decs with (n := 0) in H1; [ | routine].
      apply fv_decs_shrinks in H11.
      repeat split; trivial.
      + fv_solve.
      + simpl in *. auto.
      + routine.
      
    - wf_env. simpl_pc.
      apply revealing_preserves_wf in H; trivial.
      destruct_conjs. pose proof (fv_open_decs DS x 0).
      rewrite <- fv_typ_of_equiv in H6. 
      rewrite <- lc_at_equiv_typ_of in H7. simpl typ_of in *.
      pose proof H2.
      apply lc_at_binds_decs with (n := 0) in H2; [ | routine].
      apply fv_decs_shrinks in H13.
      repeat split; trivial.
      + constructor; trivial.
        * simpl_pc. simpl in *. fsetdec.
        * simpl_pc; trivial.
      + assert (fv (x ~ pc_mu DS (In_remove A l) ++ P1') [=] singleton x `union` fv P1'). {
          simpl. set simpl. fsetdec.
        }
        rewrite H14. apply union_conj.
        split; [set solve |].
        fv_solve.
      + simpl in *. fsetdec.
      + routine.
  Qed.

  Definition upcast_func (P : permits) (x : avar) (A : typ_label) : permits * typ :=
    match x with
    | avar_b _ => (nil, typ_top)
    | avar_f x =>
      match split_at x P with
      | None => (nil, typ_top)
      | Some (existT _ T' (existT _ P1 _)) =>
        let '(existT _ P' (exist _ U _)) := reveal_func P1 T' in
        match U with
        | pc_bot => (nil, typ_bot)
        | pc_rcd DS =>
          match decs_typ_lookup A DS with
          | None => (nil, typ_top)
          | Some (_, U') => (P', U')
          end
        | pc_mu DS l =>

          match In_dec A l with
          | right _ => (nil, typ_top)
          | left _ =>
            match decs_typ_lookup A (open x DS) with
            | None => (nil, typ_top)
            | Some (_, U') => 
              (x ~ pc_mu DS (In_remove A l) ++ P', U')
            end
          end
        | _ => (nil, typ_top)
        end
      end
    end.

  Theorem upcast_func_sound_wrt_spec : forall P x A P' T,
      upcast_func P x A = (P', T) ->
      upcast P x A P' T.
  Proof.
    intros. unfold upcast_func in H.
    execute H.
    all:progressive_inversions; auto.
    - apply reveal_func_sound_wrt_spec in Heqs2.
      tidy_up. eapply uc_bot. eauto.
    - apply reveal_func_sound_wrt_spec in Heqs2.
      tidy_up. eapply uc_rcd; eauto.
      apply decs_typ_lookup_sound1. eassumption.
    - apply reveal_func_sound_wrt_spec in Heqs2.
      tidy_up. eapply uc_mu; eauto.
      apply decs_typ_lookup_sound1. eassumption.
  Qed.

  Lemma upcast_func_decreases_measure : forall P x A P' T,
      upcast_func P x A = (P', T) ->
      subtyp_measure P (typ_sel x A) > subtyp_measure P' T.
  Proof.
    intros. apply upcast_func_sound_wrt_spec in H.
    apply upcast_decreases_measure. trivial.
  Qed.

  
  Definition upcast_f_u P (x : avar) (A : typ_label) (tup : permits * typ)
             (H : upcast_func P x A = tup) :
    { P'' & {T'' |
             subtyp_measure P (typ_sel x A) > subtyp_measure P'' T'' } }.
  Proof.
    refine (existT _ (fst tup) (exist _ (snd tup) _)).
    abstract (destruct tup; simpl fst; simpl snd;
              apply upcast_func_decreases_measure; trivial).
  Defined.

  Lemma upcast_f_u_eq : forall P x A tup pf P'' T'' pf',
      @upcast_f_u P x A tup pf = existT _ P'' (exist _ T'' pf') ->
      P'' = fst tup /\ T'' = snd tup.
  Proof.
    intros. unfold upcast_f_u in H. tidy_up.
    auto.
  Qed.
  
  Definition upcast_f_dep (P : permits) (x : avar) (A : typ_label) : 
    { P' & {T' |
            subtyp_measure P (typ_sel x A) > subtyp_measure P' T' } }
    := @upcast_f_u P x A (upcast_func P x A) eq_refl.
  
  Lemma upcast_f_dep_eq_upcast_f : forall P x A P' T' pf,
      upcast_f_dep P x A = existT _ P' (exist _ T' pf) ->
      upcast_func P x A = (P', T').
  Proof.
    intros. unfold upcast_f_dep in H.
    apply upcast_f_u_eq in H.
    destruct (upcast_func P x A); tidy_up.
    auto.
  Qed.
  
End UpcastProperties.


Inductive downcast : permits -> avar -> typ_label -> (* inputs *)
                   permits -> typ -> (* outputs *)
                   Prop :=
| dc_bot : forall P x A,
    downcast P x A nil typ_bot
| dc_top : forall P1 P2 P1' T (x : var) A,
    revealing P1 T P1' pc_bot ->
    downcast (P2 ++ x ~ T ++ P1) x A nil typ_top
| dc_rcd : forall P1 P2 T P1' (x : var) (A : typ_label) (DS : decs) L U,
    revealing P1 T P1' (pc_rcd DS) ->
    lbinds A (dec_typ A L U) DS ->
    downcast (P2 ++ x ~ T ++ P1) x A P1' L
| dc_mu : forall P1 P2 P1' T (x : var) (A : typ_label) (DS : decs) l L U,
    revealing P1 T P1' (pc_mu DS l) ->
    In A l ->
    lbinds A (dec_typ A L U) (open x DS) ->
    downcast (P2 ++ x ~ T ++ P1) x A (x ~ (pc_mu DS (In_remove A l)) ++ P1') L.

Section DowncastProperties.

  Local Hint Constructors downcast.
  
  Theorem downcast_decreases_measure : forall P x A P' U,
    downcast P x A P' U ->
    subtyp_measure P (typ_sel x A) > subtyp_measure P' U.
  Proof.
    intros. dependent induction H; subst; simpl.
    all:try lazymatch goal with
            | H : revealing _ _ _ _ |- _ =>
              let H' := fresh "H'" in
              pose proof H as H';
                apply revealing_maintain_reveal_measure in H';
                simpl in H'
            end.
    all:permits_simpl; try lia.
    - apply revealing_maintain_reveal_measure in H. simpl in H.
      apply decs_measure_lbinds_typ in H0. destruct H0.
      lia.
    - change (total ?l) with (permits_weight P1').
      edestruct pc_mu_measure_lbinds; try eassumption.
      repeat rewrite pc_mu_measure_open_invar in *.
      lia.
  Qed.

  Hint Resolve ope_sub_refl ope_sub_nil_any ope_sub_trans ope_sub_app_r.
  Hint Constructors ope_sub.

  Theorem downcast_sound : forall P x A P' L,
      downcast P x A P' L ->
      to_env P ⊢ L <⦂ typ_sel x A /\ ope_sub (to_env P') (to_env P).
  Proof.
    induction on downcast.
    all:repeat rewrite to_env_app in *.
    all:change (to_env (?x ~ ?T)) with (x ~ typ_of T).
    all:try lazymatch goal with
            | |- subtyp (?l2 ++ ?x ~ ?p ++ ?l1) _ _ /\ _ =>
              pose proof (binds_for_sure l2 l1 x p)
            end.
    all:repeat rewrite typ_of_left_inv in *; destruct_conjs.
    all:try solve [repeat split; auto].
    - apply revealing_sound in H. destruct_conjs.
      repeat split; auto.
      eapply subtyp_sel1.
      eapply ty_sub. eauto.
      eapply subtyp_trans.
      + by_weakening.
      + instantiate (1 := typ_top). trivial.

    - apply revealing_sound in H. destruct_conjs.
      repeat split; auto.
      eapply subtyp_sel1.
      eapply decs_bind_typ; try eassumption.
      eapply ty_sub. eauto. by_weakening.
    - apply revealing_sound in H. destruct_conjs.
      repeat split; auto.
      + eapply subtyp_sel1.
        eapply decs_bind_typ; try eassumption.
        apply ty_mu_elim.
        eapply ty_sub. eauto. by_weakening.
      + simpl. auto.
  Qed.

  Hint Resolve wf_nil fv_deapp.
  Theorem downcast_preserves_wf : forall P (x : var) A P' U,
      downcast P x A P' U ->
      wf_env (to_env P) ->
      x `in` dom P ->
      wf_env (to_env P') /\ fv P' [<=] fv P /\ fv U [<=] dom P' /\ lc U.
  Proof.
    induction on downcast; intros.
    all:repeat rewrite to_env_app in *.
    all:change (to_env (?x ~ ?T)) with (x ~ typ_of T).
    1-2:simpl; repeat split; set solve.
    - wf_env. simpl_pc.
      apply revealing_preserves_wf in H; trivial.
      destruct_conjs.
      pose proof H1.
      apply lc_at_binds_decs with (n := 0) in H1; [ | routine].
      apply fv_decs_shrinks in H11.
      repeat split; trivial.
      + fv_solve.
      + simpl in *. auto.
      + routine.
      
    - wf_env. simpl_pc.
      apply revealing_preserves_wf in H; trivial.
      destruct_conjs. pose proof (fv_open_decs DS x 0).
      rewrite <- fv_typ_of_equiv in H6. 
      rewrite <- lc_at_equiv_typ_of in H7. simpl typ_of in *.
      pose proof H2.
      apply lc_at_binds_decs with (n := 0) in H2; [ | routine].
      apply fv_decs_shrinks in H13.
      repeat split; trivial.
      + constructor; trivial.
        * simpl_pc. simpl in *. fsetdec.
        * simpl_pc; trivial.
      + assert (fv (x ~ pc_mu DS (In_remove A l) ++ P1') [=] singleton x `union` fv P1'). {
          simpl. set simpl. fsetdec.
        }
        rewrite H14. apply union_conj.
        split; [set solve |].
        fv_solve.
      + simpl in *. fsetdec.
      + routine.
  Qed.

  Definition downcast_func (P : permits) (x : avar) (A : typ_label) : permits * typ :=
    match x with
    | avar_b _ => (nil, typ_bot)
    | avar_f x =>
      match split_at x P with
      | None => (nil, typ_bot)
      | Some (existT _ T' (existT _ P1 _)) =>
        let '(existT _ P' (exist _ U _)) := reveal_func P1 T' in
        match U with
        | pc_bot => (nil, typ_top)
        | pc_rcd DS =>
          match decs_typ_lookup A DS with
          | None => (nil, typ_bot)
          | Some (L', _) => (P', L')
          end
        | pc_mu DS l =>

          match In_dec A l with
          | right _ => (nil, typ_bot)
          | left _ =>
            match decs_typ_lookup A (open x DS) with
            | None => (nil, typ_bot)
            | Some (L', _) =>
              (x ~ pc_mu DS (In_remove A l) ++ P', L')
            end
          end
        | _ => (nil, typ_bot)
        end
      end
    end.

  Theorem downcast_func_sound_wrt_spec : forall P x A P' T,
      downcast_func P x A = (P', T) ->
      downcast P x A P' T.
  Proof.
    intros. unfold downcast_func in H.
    execute H.
    all:progressive_inversions; auto.
    - apply reveal_func_sound_wrt_spec in Heqs2.
      tidy_up. eapply dc_top. eauto.
    - apply reveal_func_sound_wrt_spec in Heqs2.
      tidy_up. eapply dc_rcd; eauto.
      apply decs_typ_lookup_sound1. eassumption.
    - apply reveal_func_sound_wrt_spec in Heqs2.
      tidy_up. eapply dc_mu; eauto.
      apply decs_typ_lookup_sound1. eassumption.
  Qed.

  Lemma downcast_func_decreases_measure : forall P x A P' T,
      downcast_func P x A = (P', T) ->
      subtyp_measure P (typ_sel x A) > subtyp_measure P' T.
  Proof.
    intros. apply downcast_func_sound_wrt_spec in H.
    apply downcast_decreases_measure. trivial.
  Qed.
  
  Definition downcast_f_u P (x : avar) (A : typ_label) (tup : permits * typ)
             (H : downcast_func P x A = tup) :
    { P'' & {T'' |
             subtyp_measure P (typ_sel x A) > subtyp_measure P'' T'' } }.
  Proof.
    refine (existT _ (fst tup) (exist _ (snd tup) _)).
    abstract (destruct tup; simpl fst; simpl snd;
              apply downcast_func_decreases_measure; trivial).
  Defined.

  Lemma downcast_f_u_eq : forall P x A tup pf P'' T'' pf',
      @downcast_f_u P x A tup pf = existT _ P'' (exist _ T'' pf') ->
      P'' = fst tup /\ T'' = snd tup.
  Proof.
    intros. unfold downcast_f_u in H. tidy_up.
    auto.
  Qed.
  
  Definition downcast_f_dep (P : permits) (x : avar) (A : typ_label) : 
    { P' & {T' |
            subtyp_measure P (typ_sel x A) > subtyp_measure P' T' } }
    := @downcast_f_u P x A (downcast_func P x A) eq_refl.
  
  Lemma downcast_f_dep_eq_downcast_f : forall P x A P' T' pf,
      downcast_f_dep P x A = existT _ P' (exist _ T' pf) ->
      downcast_func P x A = (P', T').
  Proof.
    intros. unfold downcast_f_dep in H.
    apply downcast_f_u_eq in H.
    destruct (downcast_func P x A); tidy_up.
    auto.
  Qed.
  
End DowncastProperties.

Reserved Notation "[ L ] P1 >> T '<⦂' U << P2" (at level 70).
Reserved Notation "[ L ] P1 >>[ DS1 '<⦂' DS2 ]<< P2" (at level 70).

Inductive bi_subtyp : atoms -> permits -> typ -> typ -> permits -> Prop :=
| bs_bot : forall L P1 T P2, [ L ] P1 >> typ_bot <⦂ T << P2
| bs_top : forall L P1 T P2, [ L ] P1 >> T <⦂ typ_top << P2
| bs_sel_refl : forall L P1 x A P2, [ L ] P1 >> typ_sel x A <⦂ typ_sel x A << P2

| bs_sel_left : forall L P1 x A P2 T U P1',
    upcast P1 x A P1' T ->
    [ L ] P1' >> T <⦂ U << P2 ->
    [ L ] P1 >> typ_sel x A <⦂ U << P2
| bs_sel_right : forall L P1 x A P2 T U P2',
    downcast P2 x A P2' U ->
    [ L ] P1 >> T <⦂ U << P2' ->
    [ L ] P1 >> T <⦂ typ_sel x A << P2

| bs_all : forall L P1 T1 U1 P2 T2 U2 x,
    x `notin` fv P1 `union` fv T1 `union` fv T2
      `union` fv U1 `union` fv U2 `union` fv P2 `union` L ->
    [ L ] P2 >> T2 <⦂ T1 << P1 ->
    [ L  `union` singleton x `union` fv T2 ]
      x ~ from_typ T1 ++ P1 >> open x U1 <⦂ open x U2 << x ~ from_typ T2 ++ P2 ->
    [ L ] P1 >> typ_all T1 U1 <⦂ typ_all T2 U2 << P2
| bs_rcd : forall L P1 DS1 DS2 P2,
    [ L ] P1 >>[ DS1 <⦂ DS2 ]<< P2 ->
    [ L ] P1 >> typ_rcd DS1 <⦂ typ_rcd DS2 << P2
| bs_obj : forall L P1 DS P2,
    [ L ] P1 >> typ_obj DS <⦂ typ_obj DS << P2
where "[ L ] P1 >> T '<⦂' U << P2" := (bi_subtyp L P1 T U P2)%type

with
bi_decs_subtyp :  atoms ->permits -> decs -> decs -> permits -> Prop :=
| bd_nil : forall L P1 DS P2, [ L ] P1 >>[ DS <⦂ decs_nil ]<< P2
| bd_cons1 : forall L P1 (DS1 DS2 : decs) (a : trm_label) T1 T2 P2,
    [ L ] P1 >> T1 <⦂ T2 << P2 ->
    lbinds a (dec_trm a T1) DS1 ->
    [ L ] P1 >>[ DS1 <⦂ DS2 ]<< P2 ->
    [ L ] P1 >>[ DS1 <⦂ decs_cons (dec_trm a T2) DS2 ]<< P2
| bd_cons2 : forall L P1 (DS1 DS2 : decs) (A : typ_label) S1 U1 S2 U2 P2,
    [ L ] P2 >> S2 <⦂ S1 << P1 ->
    [ L ] P1 >> U1 <⦂ U2 << P2 ->
    lbinds A (dec_typ A S1 U1) DS1 ->
    [ L ] P1 >>[ DS1 <⦂ DS2 ]<< P2 ->
    [ L ] P1 >>[ DS1 <⦂ decs_cons (dec_typ A S2 U2) DS2 ]<< P2
where "[ L ] P1 >>[ DS1 '<⦂' DS2 ]<< P2" := (bi_decs_subtyp L P1 DS1 DS2 P2)%type.
Hint Constructors bi_subtyp bi_decs_subtyp.

Local Hint Resolve ope_sub_nil_any ope_sub_trans ope_sub_refl ope_sub_deapp.

Theorem ope_sub_bi_subtyp_sound : forall L P1 T U P2,
    [ L ] P1 >> T <⦂ U << P2 ->
    forall G,
      fv G [<=] L ->
      uniq G ->
      ope_sub (to_env P1) G ->
      ope_sub (to_env P2) G ->
      G ⊢ T <⦂ U
  with ope_sub_bi_decs_subtyp_sound : forall L P1 DS1 DS2 P2,
      [ L ] P1 >>[ DS1 <⦂ DS2 ]<< P2 ->
      forall G,
        fv G [<=] L ->
        uniq G ->
        ope_sub (to_env P1) G ->
        ope_sub (to_env P2) G ->
        G ⊢ typ_rcd DS1 <⦂ typ_rcd DS2.
Proof.
  - clear ope_sub_bi_subtyp_sound.
    induction on bi_subtyp; intros; auto.
    + pose proof (upcast_sound H).
      destruct_conjs.
      eapply subtyp_trans.
      * eapply ope_narrow_subtyp_gen; eauto.
      * apply IHbi_subtyp; auto.
        eapply ope_sub_trans; eauto.
    + pose proof (downcast_sound H).
      destruct_conjs.
      eapply subtyp_trans.
      * apply IHbi_subtyp; auto.
        eapply ope_sub_trans; eauto.
      * eapply ope_narrow_subtyp_gen; eauto.

    + eapply subtyp_all; auto.
      assert (x `notin` fv G). {
        eapply notin_subset_relax. eassumption. auto.
      }
      cofinite. apply open_subst_subtyp with (x := x); trivial.
      * repeat (apply notin_union_3; auto).
      * auto.
      * apply IHbi_subtyp2.
        -- assert (fv (x ~ T2 ++ G) [=] singleton x `union` fv G `union` fv T2). {
             simpl. set solve.
           }
           rewrite H5. set solve.
        -- constructor; trivial. simpl in *. solve_notin.
        -- simpl. rewrite typ_of_left_inv.
           apply os_keep; auto.
        -- simpl. rewrite typ_of_left_inv.
           apply os_keep; auto.
    + eauto.
      
  - clear ope_sub_bi_decs_subtyp_sound.
    induction on bi_decs_subtyp; intros; auto.
    + apply decs_nil_top_of_rcd.
    + list_reasoning. apply subtyp_merge; auto.
      eapply subtyp_trans.
      * eapply lbinds_subtyp. eassumption.
      * apply subtyp_fld. eauto.
    + list_reasoning. apply subtyp_merge; auto.
      eapply subtyp_trans.
      * eapply lbinds_subtyp. eassumption.
      * apply subtyp_typ; eauto.
Qed.

Definition bsubtyp (P : permits) (T U : typ) : Prop :=
  uniq P /\ [ fv P ] P >> T <⦂ U << P.
Arguments bsubtyp P T U/.

Notation "P ⊢B T <⦂ U" := (bsubtyp P T U)%type (at level 70).
Local Hint Resolve ope_sub_refl.

Theorem bsubtyp_sound : forall P T U,
    P ⊢B T <⦂ U ->
    to_env P ⊢ T <⦂ U.
Proof.
  simpl. intros. tidy_up.
  eapply ope_sub_bi_subtyp_sound; eauto.
  - rewrite <- fv_permits. change (fv P [<=] fv P).
    set solve.
  - rewrite <- to_env_permits_uniq. trivial.
Qed.

Program Fixpoint bi_subtyp_refls (T : typ) n {measure (n + typ_struct_measure T)} :
  (forall L P1 P2,
      n = 1 ->
      ([ L ] P1 >> T <⦂ T << P2)) /\
  (forall L P1 P2 DS DS',
      n = 0 ->
      T = typ_rcd DS' ->
      tail_of DS' DS ->
      [ L ] P1 >>[ DS <⦂ DS' ]<< P2) := _.
Next Obligation.
  split; intros; subst.
  - destruct T; try constructor.
    + pick_fresh x. econstructor.
      * eauto.
      * eapply bi_subtyp_refls; eauto.
        simpl. lia.
      * eapply bi_subtyp_refls; eauto.
        rewrite open_typ_same_measure. simpl. lia.
    + eapply bi_subtyp_refls; eauto.
      lia.
  - destruct DS'; auto.
    destruct d.
    + rec_pose (tail_of_lbinds (t ∈ t0 ⋯ t1) H1) Hlbinds.
      simpl in *. econstructor. 3:eassumption.
      1-2:eapply bi_subtyp_refls; eauto; simpl; lia.
      eapply bi_subtyp_refls; eauto.
      simpl. lia.
      eapply tail_of_decons. eassumption.
    + rec_pose (tail_of_lbinds (t ∷ t0) H1) Hlbinds.
      simpl in *. econstructor. 2:eassumption.
      * eapply bi_subtyp_refls; eauto. lia.
      * eapply bi_subtyp_refls; eauto.
        simpl. lia.
        eapply tail_of_decons. eassumption.
Qed.

Theorem bi_subtyp_refl : forall T L P1 P2,
    [ L ] P1 >> T <⦂ T << P2.
Proof. intros. eapply bi_subtyp_refls. eauto. Qed.

Definition eq_typ_sel (x : avar) (A : typ_label) (y : avar) (B : typ_label)
  : {x = y /\ A = B} + {x <> y \/ A <> B}.
Proof.
  destruct (x == y), (A == B); subst; eauto.
Defined.

Equations bi_subtyper (L : atoms)
          (P1 : permits) (T1 : typ)
          (T2 : typ) (P2 : permits) : Tc unit
  by wf (subtyp_measure P1 T1 + subtyp_measure P2 T2) lt :=
  {
    bi_subtyper L _ typ_bot _ _ := ret tt;
    bi_subtyper L _ _ typ_top _ := ret tt;
    bi_subtyper L P1 (typ_sel x A) (typ_sel y B) P2
      with eq_typ_sel x A y B => {
    | left _ => ret tt;
    | right _ with upcast_f_dep P1 x A => {
      | existT _ P1' (exist _ T1' _) =>
        bi_subtyper L P1' T1' (typ_sel y B) P2
      }
    };
    bi_subtyper L P1 (typ_sel x A) T2 P2
      with upcast_f_dep P1 x A => {
    | existT _ P1' (exist _ T1' _) =>
      bi_subtyper L P1' T1' T2 P2
    };
    bi_subtyper L P1 T1 (typ_sel y B) P2
      with downcast_f_dep P2 y B => {
    | existT _ P2' (exist _ T2' _) =>
      bi_subtyper L P1 T1 T2' P2'
    };

    bi_subtyper L P1 (typ_all T1 U1) (typ_all T2 U2) P2
      with bi_subtyper L P2 T2 T1 P1 => {
    | inl msgs => inl ("subtyper: input subtype failed in function case" :: msgs);
    | inr _ with pick_fresh_var => {
      | exist _ x _ =>
        bi_subtyper (L `union` singleton x `union` fv T2)
                    (x ~ from_typ T1 ++ P1) (open x U1)
                    (open x U2) (x ~ from_typ T2 ++ P2)
                    !!> "subtyper: output subtype failed in function case"
      }
    };

    bi_subtyper L P1 (typ_rcd DS1) (typ_rcd DS2) P2 :=
      let bi_decs_subtyper (DS2' : decs)
                           (H : decs_struct_measure DS2' <= decs_struct_measure DS2) :
            Tc unit := _ in
      bi_decs_subtyper DS2 _;
    
    bi_subtyper L P1 (typ_obj DS1) (typ_obj DS2) P2 with DS1 == DS2 => {
    | left _ => ret tt;
    | right _ => erret "subtyper: object is not in the typing lattice"
    };
    
    bi_subtyper L _ _ _ _ := erret "subtyper: unable to discover subtype relation."
  }.

Next Obligation. lia. Qed.
Next Obligation. lia. Qed.
Next Obligation. lia. Qed.
Next Obligation. lia. Qed.
Next Obligation. lia. Qed.
Next Obligation. lia. Qed.
Next Obligation. lia. Qed.
Next Obligation. lia. Qed.
Next Obligation.
  pose proof subtyp_measure_all_case.
  simpl in H. do 2 rewrite H with (x := x).
  lia.
Qed.
Next Obligation. lia. Qed.
Next Obligation.
  induction DS2'.
  - exact (ret tt).
  - destruct d; simpl in *.
    + refine (match decs_typ_lookup_d t DS1 with
              | inright _ => erret "subtyper: typ label is not found"
              | inleft (existT _ S1 (exist _ U1 _)) =>
                bi_subtyper L P2 t0 S1 P1 _ ;;
                            bi_subtyper L P1 U1 t1 P2 _ ;;
                            IHDS2' _
              end).
      1-2:abstract (apply decs_measure_lbinds_typ in b; destruct_conjs; lia).
      abstract(lia).
    + refine (match decs_trm_lookup_d t DS1 with
              | inright _ => erret "subtyper: trm label is not found"
              | inleft (exist _ T1 _) =>
                bi_subtyper L P1 T1 t0 P2 _ ;;
                            IHDS2' _
              end).
      * abstract (apply decs_measure_lbinds_trm in b; lia).
      * abstract (lia).
Defined.
Next Obligation. lia. Qed.

Fixpoint bi_decs_subtyper (L : atoms)
         (P1 : permits) (DS1 DS2 : decs) (P2 : permits) : Tc unit :=
  match DS2 with
  | decs_nil => ret tt
  | decs_cons (dec_trm a T2) DS2' =>
    match decs_trm_lookup a DS1 with
    | None => erret "subtyper: trm label is not found"
    | Some T1 =>
      bi_subtyper L P1 T1 T2 P2 ;;
                  bi_decs_subtyper L P1 DS1 DS2' P2
    end
  | decs_cons (dec_typ A S2 U2) DS2' =>
    match decs_typ_lookup A DS1 with
    | None => erret "subtyper: typ label is not found"
    | Some (S1, U1) =>
      bi_subtyper L P2 S2 S1 P1 ;;
                  bi_subtyper L P1 U1 U2 P2 ;;
                  bi_decs_subtyper L P1 DS1 DS2' P2
    end
  end.

Lemma obligation_11_equiv : forall DS2' L P1 DS1 DS2 P2 pf,
    tail_of DS2' DS2 ->
    bi_subtyper_obligations_obligation_11 L P1 DS1 DS2 P2
                              (fun (L0 : atoms) (P5 : permits) (T4 T5 : typ) 
                                   (P6 : permits)
                                   (_ : permits_weight P5 + typ_struct_measure T4 +
                                        (permits_weight P6 + typ_struct_measure T5) <
                                        permits_weight P1 + typ_struct_measure ({< DS1 >}) +
                                        (permits_weight P2 + typ_struct_measure ({< DS2 >}))) =>
                                 bi_subtyper L0 P5 T4 T5 P6) DS2' pf =
    bi_decs_subtyper L P1 DS1 DS2' P2.
Proof.
  intros. induction DS2'; intros; simpl; trivial.
  destruct d; simpl.
  - destruct (decs_typ_lookup_d t DS1) eqn:?.
    + destruct_conjs. pose proof Heqs.
      apply decs_typ_lookup_d_correct1 in Heqs.
      rewrite Heqs. rewrite IHDS2'; trivial.
      eapply tail_of_decons. eassumption.
    + apply decs_typ_lookup_d_correct2 in Heqs.
      rewrite Heqs. trivial.
  - destruct (decs_trm_lookup_d t DS1) eqn:?.
    + destruct_conjs. apply decs_trm_lookup_d_correct1 in Heqs.
      rewrite Heqs. rewrite IHDS2'; trivial.
      eapply tail_of_decons. eassumption.
    + apply decs_trm_lookup_d_correct2 in Heqs.
      rewrite Heqs. trivial.
Qed.

Lemma bi_decs_subtyper_corresponds : forall L P1 DS1 DS2 P2,
    bi_subtyper L P1 (typ_rcd DS1) (typ_rcd DS2) P2
    = bi_decs_subtyper L P1 DS1 DS2 P2.
Proof.
  intros. simp bi_subtyper.
  simpl. apply obligation_11_equiv. trivial.
Qed.

Local Hint Resolve surjective_pairing.
Local Ltac rem_eval H :=
  lazymatch type of H with
  | context [fst ?f] =>
    let Heq := fresh "p" in
    remember f as p;
    lazymatch goal with
    | H : p = _ |- _ =>
      symmetry in H
    end;
    destruct p; simpl in *
  end.

Local Ltac solve_term :=
  match goal with
  | H : _ = _ |- _ =>
    (apply downcast_func_decreases_measure in H ||
     apply upcast_func_decreases_measure in H);
    simpl in *; lia
  end.

Local Ltac solve_sel H f :=
  rem_eval H;
  eapply f in H; eauto;
  try solve_term;
  once ((eapply bs_sel_left + eapply bs_sel_right); try eassumption;
        (apply downcast_func_sound_wrt_spec || apply upcast_func_sound_wrt_spec); trivial).

Program Fixpoint bi_subtyper_sound_wrt_spec P1 T1 T2 P2 n
        {measure (n + subtyp_measure P1 T1 + subtyp_measure P2 T2)} : forall L,
    (n = 1 ->
     bi_subtyper L P1 T1 T2 P2 = inr tt ->
     [ L ] P1 >> T1 <⦂ T2 << P2) /\
    (forall DS1 DS2,
        n = 0 ->
        T1 = typ_rcd DS1 -> T2 = typ_rcd DS2 ->
        bi_decs_subtyper L P1 DS1 DS2 P2 = inr tt ->
        [ L ] P1 >>[ DS1 <⦂ DS2 ]<< P2) := _.
Next Obligation.
  split; intros; subst.
  - funelim (bi_subtyper L P1 T1 T2 P2).
    all:simp bi_subtyper in H0.
    all:try rewrite Heq0 in *; simpl in *;
      try rewrite Heq in *; simpl in *; subst; trivial.
    all:unfold erret in *; try congruence.
    all:try solve_sel H0 bi_subtyper_sound_wrt_spec.
    
    + rewrite obligation_11_equiv in H0; trivial.
      constructor.
      eapply bi_subtyper_sound_wrt_spec; eauto.
      lia.
    + destruct_conjs. subst. auto.
    + monads simpl in *. destruct u.
      econstructor.
      * instantiate (1 := x). simpl. solve_notin.
      * eapply bi_subtyper_sound_wrt_spec; eauto.
        simpl. lia.
      * eapply bi_subtyper_sound_wrt_spec; eauto.
        pose proof subtyp_measure_all_case.
        simpl in H1. simpl. do 2 rewrite H1 with (x := x).
        lia.

  - induction DS2; simpl in H2; auto.
    execute H2.
    + destruct u, u0.
      pose proof (decs_typ_lookup_sound1 _ _ Heqo). pose proof H.
      apply decs_measure_lbinds_typ in H. destruct_conjs.
      econstructor.
      * eapply bi_subtyper_sound_wrt_spec; eauto.
        simpl. lia.
      * eapply bi_subtyper_sound_wrt_spec; eauto.
        simpl. lia.
      * trivial.
      * apply IHDS2; trivial. intros.
        apply bi_subtyper_sound_wrt_spec. lia.
    + destruct u.
      pose proof (decs_trm_lookup_sound1 _ _ Heqo). pose proof H.
      apply decs_measure_lbinds_trm in H. 
      econstructor.
      * eapply bi_subtyper_sound_wrt_spec; eauto.
        simpl. lia.
      * trivial.
      * apply IHDS2; trivial. intros.
        apply bi_subtyper_sound_wrt_spec. lia.
Qed.

Definition from_env (G : env) : permits := map from_typ G.

Lemma to_env_left_inv : forall G,
    to_env (from_env G) = G.
Proof.
  induction G; simpl; auto.
  tidy_up. rewrite IHG. rewrite typ_of_left_inv.
  trivial.
Qed.

Definition subtyper (G : env) (T1 T2 : typ) : Tc unit :=
  let P := from_env G in
  bi_subtyper (fv P) P T1 T2 P.

Theorem subtyper_sound : forall G T1 T2,
    subtyper G T1 T2 = inr tt ->
    uniq G ->
    G ⊢ T1 <⦂ T2.
Proof.
  intros. rewrite <- (to_env_left_inv G).
  apply bsubtyp_sound.
  unfold bsubtyp. split.
  - rewrite <- to_env_left_inv in H0.
    rewrite <- to_env_permits_uniq in H0.
    trivial.
  - unfold subtyper in H.
    eapply bi_subtyper_sound_wrt_spec in H; eauto.
Qed.
