## Algorithmic Analysis of mu-Dependent And Record Types (muDART)

Slogan: `muDART = DART + mu`.

This directory focuses on algorithmic analysis of muDART. It defines:

1. The declarative muDART, and
1. Stare-at subtyping.

The naming convention in this directory is locally nameless representation with
cofinite quantification.

### Definitions

| Definition | File                | Correspondences                         |
|------------|---------------------|-----------------------------------------|
| 6.1        | Definitions.v       | `ty_trm`, `ty_def`, `ty_defs`, `subtyp` |
| 6.2        | SubtypeRules.v      | `bi_subtyp`                             |
| 6.3        | OpeSubCheck.v       | `expo_mu`                               |
| 6.4        | OpeSubCheck.v       | `revealing`                             |
|            | SubtypeRules.v      | `upcast`, `downcast`                    |
| 6.5        | OpeSub.v            | `convertible`                           |
| 6.6        | OpeSub.v            | `ope_sub`                               |
| 6.7        | SubtypingMeasures.v | `Fixpoint`s                             |
| 6.8        | TypingRules.v       | `assignable`, `syn_var`                 |
|------------|---------------------|-----------------------------------------|
| A.1        | Exposure.v          | `exposure`                              |
| A.2        | Imposure.v          | `imposure`                              |
| A.3        | ObjectTraversal.v   | `impo_mu`                               |
| A.4        | PromDemotion.v      | `prodemotion`                           |
| A.5        | PromDemotion.v      | `prode_obj`                             |
| A.6        | TypingRules.v       | `ty_synthesis`, `ty_check`              |
|            |                     |                                         |

### Conclusions

| Conclusion               | File              | Correspondences                                                                  |
|--------------------------|-------------------|----------------------------------------------------------------------------------|
| Lemma 6.1                | SubtypeRules.v    | `bi_subtyp_refls`                                                                |
| Lemma 6.2                | OpeSubCheck.v     | `expo_mu_sound`                                                                  |
| Lemma 6.3                | OpeSubCheck.v     | `expo_mu_effect`                                                                 |
| Lemma 6.4                | OpeSubCheck.v     | `expo_mu_gives_ope_decs`                                                         |
| Lemma 6.6                | OpeSub.v          | `convertible_var`                                                                |
| Lemma 6.7                | OpeSubCheck.v     | `ope_decs_is_convertible`                                                        |
| Theorem 6.8              | OpeSub.v          | `ope_narrow_trm_gen` `ope_narrow_subtyp_gen`                                     |
| Lemma 6.9                | OpeSub.v          | `ope_sub_convertible`                                                            |
| Lemma 6.10               | OpeSub.v          | `ope_sub_refl`, `ope_sub_trans`                                                  |
| Theorem 6.11, 6.12, 6.14 | OpeSubCheck.v     | `revealing_sound`                                                                |
| Theorem 6.13             | OpeSubCheck.v     | `revealing_preserves_wf`                                                         |
| Theorem 6.16             | SubtypeRules.v    | `upcast_sound`, `downcast_sound`, `upcast_preserves_wf`, `downcast_preserves_wf` |
| Theorem 6.17             | SubtypeRules.v    | `ope_sub_bi_subtyp_sound`                                                        |
| Theorem 6.18             | SubtypeRules.v    | `bsubtyp_sound`                                                                  |
| Lemma 6.19               | ObjectTraversal.v | `expo_mu_shrinks_measure`                                                        |
| Lemma 6.20               | Revealing.v       | `revealing_decreases_measure`                                                    |
|                          | SubtypeRules.v    | `upcast_decreases_measure`, `downcast_decreases_measure`                         |
| Lemma 6.22               | TypingRules.v     | `assignable_sound`                                                               |
| Lemma 6.23               | TypingRules.v     | `syn_var_sound`                                                                  |
| Lemma 6.24               | TypingRules.v     | `syn_var_not_capture`                                                            |
|--------------------------|-------------------|----------------------------------------------------------------------------------|
| Lemma A.1                | Exposure.v        | `exposure_sound`, `exposure_not_sel`                                             |
| Lemma A.2                | Imposure.v        | `imposure_sound`, `imposure_not_sel`                                             |
| Lemma A.3                | ObjectTraversal.v | `impo_mu_is_correct`                                                             |
| Lemma A.4                | ObjectTraversal.v | `impo_mu_effect`                                                                 |
| Lemma A.5                | ObjectTraversal.v | `impo_mu_gives_ope`                                                              |
| Lemma A.6                | PromDemotion.v    | `prodemotion_sound`, `prodemotion_fv_specs`                                      |
| Lemma A.7                | PromDemotion.v    | `prode_obj_fv_specs`                                                             |
| Lemma A.8                | PromDemotion.v    | `prode_obj_sound`                                                                |
| Lemma A.9                | TypingRules.v     | `check_sound`                                                                    |
| Lemma A.10, A.11         | TypingRules.v     | `synthesis_sound`                                                                |

### File structure

For performance purposes, the proofs are scattered in multiple files (though it still
takes more than 10 minutes to compile this folder on my fastest machine). Following
serves as an explanation of what the files are.

* `Definitions.v`, the definition of the language and related concepts.
* A number of structural properties are proved in following files. It should be
  straightforward from their names what they prove: `Substitutionlaws.v`,
  `Narrowing.v`, `Weakening.v`.
* Following files contain an enumeration of properties of the operations:
  `Operationproperties.v`, `Substitution.v` and `SubEnv.v`.
* `Helpers.v` contains random helper lemmas.
* `Equivalence.v` contains the definition and properties of relaxed equivalence
  relation between types.
* `Measures.v` contains definitions and properties of some measures.
* `Safety.v` contains the safety proof of the calculus.
* `TypeCheckspecs.v` contains the specifications needed for the implementations.
* `ContextTraversal.v` contains the theories and functions that traverse objects and
  contexts.
* `TypingRules.v` contains the theories and functions that relate to typer.
