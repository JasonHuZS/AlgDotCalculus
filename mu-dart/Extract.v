Require Export TypeCheck.
Require Export StdSetup.
Require Export Definitions.

Extraction Inline def_rect defs_rec defs_rect.
Extraction Inline dec_rec dec_rect decs_rec decs_rect.
Extraction Inline typ_rect typ_rec.
Extraction Inline avar_rect avar_rec.
Extraction Inline sumbool_rect.
