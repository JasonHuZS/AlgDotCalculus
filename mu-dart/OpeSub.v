Set Implicit Arguments.
Require Import Definitions.
Require Import Weakening.

Inductive convertible : env -> var -> typ -> typ -> Prop :=
| cv_sub : forall G x T U,
    G ⊢ T <⦂ U ->
    convertible G x T U
| cv_mu : forall G x DS1 DS2,
    G ⊢ typ_rcd (open x DS1) <⦂ typ_rcd (open x DS2) ->
    convertible G x (typ_obj DS1) (typ_obj DS2)
| cv_trans : forall G x T1 T2 T3,
    convertible G x T1 T2 ->
    convertible G x T2 T3 ->
    convertible G x T1 T3.
Local Hint Constructors convertible.    
Hint Resolve cv_sub.

Inductive ope_sub : env -> env -> Prop :=
| os_nil : ope_sub nil nil
| os_drop : forall G1 G2 x T,
    ope_sub G1 G2 ->
    ope_sub G1 ((x, T) :: G2)
| os_keep : forall G1 G2 x T1 T2,
    ope_sub G1 G2 ->
    convertible G2 x T1 T2 ->
    ope_sub ((x, T2) :: G1) ((x, T1) :: G2).
Local Hint Constructors ope_sub.

Lemma convertible_refl : forall G x T, 
    convertible G x T T.
Proof. routine. Qed.
Local Hint Resolve convertible_refl.

Lemma convertible_var : forall G x T U,
    convertible G x T U ->
    G ⊢ trm_var x ⦂ T ->
    G ⊢ trm_var x ⦂ U.
Proof.
  induction on convertible; intros; eauto.
Qed.
Local Hint Resolve convertible_var.

Lemma convertible_weaken : forall G1 G2 x T U,
    convertible G1 x T U ->
    convertible (G2 ++ G1) x T U.
Proof.
  induction on convertible; eauto.
Qed.
Local Hint Resolve convertible_weaken.

Lemma ope_sub_nil_any : forall G,
    ope_sub nil G.
Proof. induction G; routine. Qed.

Lemma ope_sub_refl : forall G,
    ope_sub G G.
Proof. induction G; routine. Qed.

Lemma ope_sub_cons_id : forall x T G G',
    ope_sub G G' ->
    ope_sub (x ~ T ++ G) (x ~ T ++ G').
Proof. intros; apply os_keep; auto. Qed.
Local Hint Resolve ope_sub_cons_id.

Lemma ope_sub_decons : forall x T G G',
    ope_sub ((x, T) :: G) G' ->
    ope_sub G G'.
Proof. dep induction on ope_sub; eauto. Qed.
Local Hint Resolve ope_sub_decons.

Lemma ope_sub_deapp : forall G1 G2 G',
    ope_sub (G1 ++ G2) G' ->
    ope_sub G2 G'.
Proof. induction G1; eroutine. Qed.

Lemma ope_sub_app_r : forall G G1 G2,
    ope_sub G G2 ->
    ope_sub G (G1 ++ G2).
Proof. induction G1; routine. Qed.

Lemma ope_sub_dom : forall G G',
    ope_sub G G' ->
    dom G [<=] dom G'.
Proof.
  induction on ope_sub; auto.
  - routine.
  - simpl. fsetdec.
Qed.

Lemma ope_narrow_var : forall G G' x T,
    ope_sub G G' ->
    binds x T G ->
    G' ⊢ trm_var x ⦂ T.
Proof.
  induction on ope_sub; intros.
  - routine.
  - apply IHope_sub in H.
    reassoc 2 with 1. apply weaken_trm.
    trivial.
  - tidy_up.
    + eapply convertible_var. 2:eauto.
      simpl_env. apply convertible_weaken.
      trivial.
    + apply IHope_sub in H0.
      reassoc 2 with 1. apply weaken_trm.
      trivial.
Qed.      

Lemma ope_narrow_trm_gen : forall G t T,
    G ⊢ t ⦂ T ->
    forall G',
      ope_sub G G' ->
      G' ⊢ t ⦂ T
with ope_narrow_def_gen : forall G d D,
  G ⊩ d ⦂ D ->
  forall G',
    ope_sub G G' ->
    G' ⊩ d ⦂ D
with ope_narrow_defs_gen : forall G ds DS,
  G ⊩[ ds ⦂ DS ] ->
  forall G',
    ope_sub G G' ->
    G' ⊩[ ds ⦂ DS ]
with ope_narrow_subtyp_gen : forall G T U,
    G ⊢ T <⦂ U ->
    forall G',
      ope_sub G G' ->
      G' ⊢ T <⦂ U.
Proof.
  - clear ope_narrow_trm_gen.
    induction on ty_trm; intros; auto.
    + eapply ope_narrow_var; eauto.
    + econstructor. cofinite.
      apply H0; auto. 
    + eauto.
    + econstructor; trivial.
      cofinite. eapply ope_narrow_defs_gen; eauto.
    + econstructor. eauto.
      cofinite. apply H0; eauto.
    + econstructor. eauto.
      cofinite. apply H0; eauto.
    + eapply ty_sub. eauto.
      eapply ope_narrow_subtyp_gen; eauto.

  - clear ope_narrow_def_gen.
    induction on ty_def; eroutine.

  - clear ope_narrow_defs_gen.
    induction on ty_defs; eroutine.

  - clear ope_narrow_subtyp_gen.
    induction on subtyp; intros; auto.
    1,3,4:eauto.
    eapply subtyp_all. auto. cofinite. auto.
Qed.                                
Local Hint Resolve ope_narrow_subtyp_gen.

Lemma ope_sub_convertible : forall G1 x T U,
    convertible G1 x T U ->
    forall G2,
      ope_sub G1 G2 ->
      convertible G2 x T U.
Proof.
  induction on convertible; eauto.
Qed.                                                 
Local Hint Resolve ope_sub_convertible.

Lemma ope_sub_trans : forall G1 G2 G3,
    ope_sub G1 G2 ->
    ope_sub G2 G3 ->
    ope_sub G1 G3.
Proof.
  intros. gen G1. induction H0; intros; auto.
  invert H1; subst; eauto.
Qed.
