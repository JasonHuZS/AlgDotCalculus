Set Implicit Arguments.
Require Import Definitions.
Require Import TypeCheckSpecs.
Require Import Exposure.
Require Import Imposure.
Require Import PromDemotion.
Require Import TypingRules.
Require Import SubtypeRules.

Lemma wf_env_fv_bound : forall G,
    wf_env G ->
    fv G [<=] dom G.
Proof.
  induction on wf_env; set solve.
Qed.

Module DecisionEnvImpl <: DecisionEnv.

  Definition expo := exposure.

  Instance isExposure : IsExposure expo := { }.
  Proof.
    - apply exposure_sound.
    - apply exposure_not_sel.
    - apply exposure_preserves_wf.
  Qed.

  Definition exposurer := exposure_func.

  Instance isExpoImpl : IsExpoImpl expo exposurer := { }.
  Proof. apply exposure_func_sound_wrt_spec. Qed.

  Definition impo := imposure.

  Instance isImposure : IsImposure impo := { }.
  Proof.
    - apply imposure_sound.
    - apply imposure_not_sel.
    - apply imposure_preserves_wf.
  Qed.

  Definition imposurer := imposure_func.

  Instance isImpoImpl : IsImpoImpl impo imposurer := { }.
  Proof. apply imposure_func_sound_wrt_spec. Qed.

  Definition prom := prodemotion pro.

  Instance isPromotion : IsPromotion prom := { }.
  Proof.
    - intros. pose proof (prodemotion_sound (existT _ G T)).
      simpl in H1.
      eapply H1 with (m := pro); trivial.
      eassumption.
    - eapply prodemotion_fv_specs.
    - intros. split.
      + pose proof (prodemotion_fv_specs H).
        destruct_conjs.
        pose proof (wf_env_fv_bound H0).
        fsetdec.
      + eapply prodemotion_preserves_lc; eauto.
  Qed.

  Definition promoter := motion pro.

  Instance isPromImpl : IsPromImpl prom promoter := { }.
  Proof.
    - intros. pose proof (motion_func_sound_wrt_spec (existT _ G T)).
      simpl in *. apply H0. trivial.
    - apply promotion_func_shape_invar.
  Qed.

  Definition dem := prodemotion de.

  Instance isDemotion : IsDemotion dem := { }.
  Proof.
    - intros. pose proof (prodemotion_sound (existT _ G T)).
      simpl in H1.
      eapply H1 with (m := de); trivial.
      eassumption.
    - eapply prodemotion_fv_specs.
    - intros. split.
      + pose proof (prodemotion_fv_specs H).
        destruct_conjs.
        pose proof (wf_env_fv_bound H0).
        fsetdec.
      + eapply prodemotion_preserves_lc; eauto.
  Qed.

  Definition demoter := motion de.

  Instance isDemImpl : IsDemImpl dem demoter := { }.
  Proof.
    - intros. pose proof (motion_func_sound_wrt_spec (existT _ G T)).
      simpl in *. apply H0. trivial.
    - apply demotion_func_shape_invar.
  Qed.

  Definition subtyper := SubtypeRules.subtyper.

  Instance isSubtyper : IsSubtyper subtyper := { }.
  Proof. apply subtyper_sound. Qed.
  
End DecisionEnvImpl.

Module TyperImpl := Typer DecisionEnvImpl.