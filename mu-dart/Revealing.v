Set Implicit Arguments.
Require Import Definitions.
Require Import TypeCheckSpecs.
Require Import Helpers.
Require Import TypingHelpers.
Require Import ObjectTraversal.
Require Import Measures.
Require Import Weakening.
Require Import OperationProperties.
Require Import SubtypingMeasures.
Require Import OpeSub.

Inductive revealing : permits -> precord -> (* inputs *)
                     permits -> precord -> (* outputs *)
                     Prop :=
| rv_stop : forall P T,
    ~is_pc_sel T ->
    revealing P T P T
| rv_top : forall P T,
    revealing P T nil pc_top
| rv_bot : forall P1 P2 P1' T x A,
    revealing P1 T P1' pc_bot ->
    revealing (P2 ++ x ~ T ++ P1) (pc_sel x A) nil pc_bot
| rv_rcd : forall P1 P2 T P1' P1'' x (A : typ_label) (DS : decs) L U U',
    revealing P1 T P1' (pc_rcd DS) ->
    lbinds A (dec_typ A L U) DS ->
    revealing P1' (from_typ U) P1'' U' ->
    revealing (P2 ++ x ~ T ++ P1) (pc_sel x A) P1'' U'
| rv_mu1 : forall P1 P2 P1' T (x : var) (A : typ_label) (DS : decs) l l' U,
    revealing P1 T P1' (pc_mu DS l) ->
    In A l ->
    lIn A (ldom DS) ->
    expo_mu l x A (open x DS) l' U ->
    ~is_sel U ->
    revealing (P2 ++ x ~ T ++ P1) (pc_sel x A) (x ~ (pc_mu DS l') ++ P1') (from_typ U)
| rv_mu2 : forall P1 P2 P1' P1'' T (x y : var) (A B : typ_label) (DS : decs) l l' U,
    revealing P1 T P1' (pc_mu DS l) ->
    In A l ->
    lIn A (ldom DS) ->
    expo_mu l x A (open x DS) l' (typ_sel y B) ->
    revealing P1' (pc_sel y B) P1'' U ->
    revealing (P2 ++ x ~ T ++ P1) (pc_sel x A) P1'' U.
Local Hint Constructors revealing.

Ltac permits_simpl :=
  repeat rewrite permits_weight_app in *;
  repeat rewrite permits_weight_cons in *;
  repeat rewrite precord_measure_from_typ in *.

Ltac wf_env :=
  lazymatch goal with
  | H : wf_env (_ ++ _) |- _ => apply wf_deapp in H; invert H; subst
  end.

Ltac fv_next := etransitivity; [eassumption |].
Ltac fv_shrink := etransitivity; [ | apply fv_permits_deapp].
Ltac fv_solve := repeat fv_next; repeat fv_shrink; auto.
Ltac simpl_pc :=
  repeat rewrite fv_typ_of_equiv in *;
  repeat rewrite fv_from_typ_equiv in *;
  repeat rewrite to_env_dom in *;
  repeat rewrite lc_at_equiv_typ_of in *;
  repeat rewrite lc_at_equiv_from_typ in *;
  repeat rewrite <- fv_permits in *.

Section RevealingProperties.

  Theorem revealing_maintain_reveal_measure : forall P T P' U,
      revealing P T P' U ->
      reveal_measure P T >= reveal_measure P' U.
  Proof.
    induction on revealing; auto; simpl in *; try lia.
    all:permits_simpl; try lia.
    - pose proof (precord_measure_ge_1 T). lia.
    - apply decs_measure_lbinds_typ in H. destruct H. lia.
    - lazymatch goal with
      | |- context [total ?l] =>
        change (total l) with (permits_weight P1')
      end.
      apply expo_mu_shrinks_measure in H1; trivial.
      + repeat rewrite pc_mu_measure_open_invar in *.
        lia.
      + rewrite decs_open_same_ldom. trivial.
  Qed.

  Theorem revealing_decreases_measure : forall P x A P' U,
      revealing P (pc_sel x A) P' U ->
      subtyp_measure P (typ_sel x A) > subtyp_measure P' (typ_of U).
  Proof.
    dep induction on revealing; subst; simpl.
    all:try lazymatch goal with
            | H : revealing _ _ _ _ |- _ =>
              let H' := fresh "H'" in
              pose proof H as H';
                apply revealing_maintain_reveal_measure in H';
                simpl in H'
            end.
    all:permits_simpl; try lia.
    - simpl in *. contradiction.
    - apply revealing_maintain_reveal_measure in H4_. simpl in H4_.
      apply decs_measure_lbinds_typ in H. destruct H.
      destruct (is_sel_dec U).
      + destruct_conjs. subst. specialize (IHrevealing2 _ _ eq_refl).
        simpl in *. lia.
      + destruct U; simpl in *; try contradiction.
        all:invert H4_0; subst; simpl in *; try lia.
    - change (total ?l) with (permits_weight P1').
      apply expo_mu_shrinks_measure in H5; trivial.
      + repeat rewrite pc_mu_measure_open_invar in *.
        rewrite typ_of_left_inv. lia.
      + rewrite decs_open_same_ldom. trivial.
    - apply revealing_maintain_reveal_measure in H4_. 
      specialize (IHrevealing2 _ _ eq_refl). simpl in *.
      lia.
  Qed.
  
  Hint Resolve ope_sub_refl ope_sub_nil_any ope_sub_trans ope_sub_app_r.
  Hint Constructors ope_sub.
  Local Ltac by_weakening :=
    once ((reassoc 4 with 3 + reassoc 5 with 4); apply weaken_subtyp; eassumption).
  
  Theorem revealing_sound : forall P T P' U,
      revealing P T P' U ->
      to_env P ⊢ typ_of T <⦂ typ_of U /\ ~is_pc_sel U /\ ope_sub (to_env P') (to_env P).
  Proof.
    induction on revealing.
    all:repeat rewrite to_env_app in *.
    all:change (to_env (?x ~ ?T)) with (x ~ typ_of T).
    all:try lazymatch goal with
            | |- subtyp (?l2 ++ ?x ~ ?p ++ ?l1) _ _ /\ _ =>
              pose proof (binds_for_sure l2 l1 x p)
            end.
    all:repeat rewrite typ_of_left_inv in *; destruct_conjs.
    all:try solve [repeat split; auto].
    - repeat split; auto.
      eapply subtyp_sel2.
      eapply ty_sub. eauto.
      eapply subtyp_trans.
      + by_weakening.
      + instantiate (1 := typ_top). trivial.
    - repeat split; auto.
      + eapply subtyp_sel2.
        eapply ty_sub. eapply decs_bind_typ; try eassumption.
        * eapply ty_sub. eauto. by_weakening.
        * eapply subtyp_typ. eauto.
          eapply ope_narrow_subtyp_gen in H1; [| eassumption].
          by_weakening.
      + eauto.
    - rewrite pc_sel_is_sel. repeat split; auto.
      + apply weaken_subtyp.
        eapply expo_mu_is_correct; try eassumption.
        apply ty_mu_elim. eapply ty_sub. eauto.
        apply weaken_subtyp. trivial.
      + simpl. auto.
    - repeat split; auto.
      + apply weaken_subtyp. eapply subtyp_trans.
        * eapply expo_mu_is_correct; try eassumption.
          apply ty_mu_elim. eapply ty_sub. eauto.
          apply weaken_subtyp. trivial.
        * apply weaken_subtyp. eapply ope_narrow_subtyp_gen; eauto.
      + eauto.
  Qed.
  
  Hint Resolve wf_nil fv_deapp.
  Theorem revealing_preserves_wf : forall P T P' U,
      revealing P T P' U ->
      wf_env (to_env P) ->
      fv T [<=] dom P -> lc T ->
      wf_env (to_env P') /\ fv P' [<=] fv P /\ fv U [<=] dom P' /\ lc U.
  Proof.
    induction on revealing; intros.
    all:repeat rewrite to_env_app in *.
    all:change (to_env (?x ~ ?T)) with (x ~ typ_of T).
    1-3:simpl; repeat split; set solve.
    - wf_env. simpl_pc.
      destruct IHrevealing1; auto.
      pose proof H.
      apply lc_at_binds_decs with (n := 0) in H; [ | routine].
      apply fv_decs_shrinks in H5. 
      pose proof (revealing_sound H3_). destruct_conjs.
      change (pc_rcd DS) with (from_typ (typ_rcd DS)) in *.
      simpl_pc. destruct IHrevealing2; auto.
      + simpl in *. auto.
      + routine.
      + destruct_conjs. repeat (split; trivial).
        fv_solve.
      
    - wf_env. simpl_pc. destruct IHrevealing; trivial.
      destruct_conjs. pose proof (fv_open_decs DS x 0).
      rewrite <- fv_typ_of_equiv in H9. 
      rewrite <- lc_at_equiv_typ_of in H14. simpl typ_of in *.
      apply exposure_mu_preserves_wf with (G := (x ~ μ{DS} ++ to_env P1')) in H1.
      destruct_conjs. repeat (split; trivial).
      + constructor; trivial.
        * apply revealing_sound in H3.
          destruct_conjs. apply ope_sub_dom in H18.
          simpl_pc. simpl in *. fsetdec.
        * simpl_pc. trivial.
      + assert (fv (x ~ pc_mu DS l' ++ P1') [=] singleton x `union` fv P1'). {
          simpl. set simpl. fsetdec.
        }
        rewrite H17. apply union_conj.
        split; [set solve |].
        fv_solve.
      + simpl in *. simpl_pc. trivial.
      + simpl. simpl_pc. fsetdec.
      + apply open_lc_decs. routine.

    - wf_env. simpl_pc. destruct IHrevealing1; auto. destruct_conjs.
      rewrite <- fv_typ_of_equiv in H7. 
      rewrite <- lc_at_equiv_typ_of in H12. simpl typ_of in *.
      pose proof H1. apply expo_mu_effect in H1.
      destruct H1; [simpl in *; contradiction |].
      specialize (H1 _ _ eq_refl).
      apply exposure_mu_preserves_wf with (G := to_env (x ~ T ++ P1')) in H13; auto.
      destruct_conjs. destruct IHrevealing2; auto.
      + progressive_inversions. simpl in *.
        simpl_pc.
        eapply exclude_add; try eassumption. congruence.
      + routine.
      + destruct_conjs. repeat (split; trivial).
        fv_solve.
  Qed.
  
End RevealingProperties.

Local Ltac reveal_obli := repeat rewrite app_length; simpl; lia.
Local Obligation Tactic := program_simpl; reveal_obli.

Equations reveal_func (P : permits) (T : precord) :
  {P' : permits & { U : precord | length P' <= length P } } by wf (length P) lt :=
  {
    reveal_func P T with is_pc_sel_dec T => {
    | inleft (existT _ (avar_b _) _) => existT _ nil (exist _ pc_top _);
    | inleft (existT _ (avar_f x) (exist _ A _)) with split_at x P => {
      | None => existT _ nil (exist _ pc_top _);
      | Some (existT _ T' (existT _ P1 _)) with reveal_func P1 T' => {
        | existT _ P1' (exist _ U _) with U => {
          | pc_bot => existT _ nil (exist _ pc_bot _);
          | pc_rcd DS with decs_typ_lookup A DS => {
            | None => existT _ nil (exist _ pc_top _);
            | Some (pair _ U') with reveal_func P1' (from_typ U') => {
              | existT _ P1'' (exist _ U'' _) =>
                existT _ P1'' (exist _ U'' _)
              }
            };
          
          | pc_mu DS l with In_dec A l => {
            | right _ => existT _ nil (exist _ pc_top _);
            | left _ with LabelAssocList.KeySetProperties.In_dec A (ldom DS) => {
              | right _ => existT _ nil (exist _ pc_top _);
              | left _ with expo_mu_func l x A (open x DS) => {
                | (pair l' U') with is_sel_dec U' => {
                  | inleft (existT _ (avar_b _) _) =>
                    existT _ nil (exist _ pc_top _);
                  | inleft (existT _ (avar_f y) (exist _ B _))
                      with reveal_func P1' (pc_sel y B) => {
                    | existT _ P1'' (exist _ U'' _) =>
                      existT _ P1'' (exist _ U'' _)
                    } ;
                  | inright _ =>
                    existT _ (x ~ pc_mu DS l' ++ P1') (exist _  (from_typ U') _)
                  }
                }
              }
            };
                            
          | _ => existT _ nil (exist _ pc_top _)

          }
        }
      };
    | inright _ => existT _ P (exist _ T _)
    }
  }.

Local Obligation Tactic := program_simpl.

Section RevealFuncProperties.

  Theorem reveal_func_sound_wrt_spec : forall P T P' T' pf,
    reveal_func P T = existT _ P' (exist _ T' pf) ->
    revealing P T P' T'.
  Proof.
    intros. funelim (reveal_func P T).
    all:rewrite <- Heqcall in *; clear Heqcall.
    all:progressive_inversions; auto.
    - tidy_up. eapply rv_bot. eauto.
    - tidy_up. eapply rv_rcd; eauto.
      apply decs_typ_lookup_sound1. eassumption.
    - tidy_up. eapply rv_mu1; eauto.
      apply expo_mu_func_resp_spec. trivial.
    - tidy_up. eapply rv_mu2; eauto.
      apply expo_mu_func_resp_spec. eassumption.
  Qed.

End RevealFuncProperties.