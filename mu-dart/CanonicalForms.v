Require Import Definitions.
Require Import Narrowing.
Require Import Weakening.
Require Import InertTypes.
Require Import InvertibleTyping.
Require Import TightTyping.
Require Import Substitution.
Require Import SubstitutionLaws.

Lemma type_means_binds : forall (x : var) T G,
    G ⊢ trm_var x ⦂ T ->
    exists T', binds x T' G.
Proof. dep induction on ty_trm; eroutine. Qed.


Definition well_typed (G : env) (s : sta) : Prop :=
  uniq G /\ uniq s /\ (dom G = dom s) /\
  (forall x T v, binds x T G -> binds x v s ->
            G ⊢ trm_val v ⦂ T).

Section WellTypedness.
  Hint Unfold well_typed.
  
  Lemma nil_is_well_typed : well_typed nil nil.
  Proof. routine; contradiction. Qed.

  Hint Resolve binds_dom_contradiction.
  Lemma well_typed_push : forall G s x T v,
      well_typed G s ->
      x `notin` dom G ->
      G ⊢ trm_val v ⦂ T ->
      well_typed (x ~ T ++ G) (x ~ v ++ s).
  Proof.
    routine;
    match goal with
    | [ H : dom ?G = _ , H1 : _ `notin` dom ?G |- _] =>
      pose proof H1; rewrite H in H1; auto
    end;
    reassoc 2 with 1;
    progressive_destructions;
    solve [eapply weaken_trm; try eassumption; solve [auto] || eauto]
    || (exfalso; eauto).
  Qed.
  
End WellTypedness.
Hint Resolve nil_is_well_typed.

Local Ltac instan_routine :=
  try cofinite;
  repeat eexists;
  solve [eroutine hinted econstructor;
         try (eapply narrow_trm || eapply narrow_subtyp; [ctx_app|])].

Local Ltac by_cofinite :=
  subst; progressive_inversions; repeat eexists; eauto;
  cofinite; (eapply subtyp_trans || eapply ty_sub);
  [eapply narrow_subtyp || eapply narrow_trm | ]; auto; eroutine.

Section InvertibleProperties.
  Hint Unfold well_typed.

  Hint Resolve binds_In.
  
  Lemma corresponding_types: forall G s x T,
    well_typed G s ->
    binds x T G ->
    (exists v, binds x v s /\
          G ⊢ trm_val v ⦂ T).
  Proof.
    Local Hint Extern 1 (_ `in` dom _) => match goal with
                                         | [H : dom _ = dom _ |- _ ] => rewrite <- H
                                         end.
    
    eroutine by idtac; match goal with [s : sta |- _] =>
                                       edestruct binds_In_inv with (E := s)
                       end.
  Qed.

  Ltac destruct_ind :=
    match goal with
    | [ H : _ |- _ ] =>
      guess_is_ind_hyp H;
      edestruct H;
      try reflexivity;
      try eassumption;
      progressive_destructions
    end.

  Hint Resolve tight_to_general_subtyp tight_to_general_trm.
  Ltac boom := try destruct_ind; instan_routine || by_cofinite.
  
  Lemma invertible_var_typ_all_to_binds : forall G (x : var) T U,
      uniq G ->
      G ⊢## x ⦂ all(T) U ->
      (exists L T' U',
          binds x (all(T') U') G /\
          G ⊢# T <⦂ T' /\
          (forall y, y `notin` L -> y ~ T ++ G ⊢ open y U' <⦂ open y U)).
  Proof using. dep induction on ty_var_inv; boom. Qed.
  
End InvertibleProperties.

Section CanonicalForms.

  Hint Resolve tight_to_general_subtyp.
  Hint Resolve general_to_tight_typ.
  Hint Resolve tight_to_invertible.
  
  Lemma var_typ_all_to_binds : forall G (x : var) T U,
      inert_env G ->
      G ⊢ trm_var x ⦂ all(T) U ->
      (exists L T' U',
          binds x (all(T') U') G /\
          G ⊢ T <⦂ T' /\
          (forall y, y `notin` L -> y ~ T ++ G ⊢ open y U' <⦂ open y U)).
  Proof using.
    intros;
    edestructs invertible_var_typ_all_to_binds; eauto 7.
  Qed.

  Hint Resolve tight_to_invertible_v.
  Lemma val_typ_all_to_lambda: forall G v T U,
      inert_env G ->
      G ⊢ trm_val v ⦂ all(T) U ->
      (exists L T' t,
          v = val_lam T' t /\
          G ⊢ T <⦂ T' /\
          (forall y, y `notin` L -> y ~ T ++ G ⊢ open y t ⦂ open y U)).
  Proof using.
    intros.
    apply general_to_tight_typ in H0; trivial.
    apply tight_to_invertible_v in H0; trivial.
    dependent induction H0.
    - instan_routine.
    - edestruct IHty_val_inv; try reflexivity; trivial.
      destruct_conjs. by_cofinite.
  Qed.

  Lemma canonical_form_all : forall G s (x : var) T U,
      inert_env G ->
      well_typed G s ->
      G ⊢ trm_var x ⦂ all(T) U ->
      (exists L T' t,
          binds x (λ(T'){ t }) s /\
          G ⊢ T <⦂ T' /\
          (forall y, y `notin` L -> y ~ T ++ G ⊢ open y t ⦂ open y U)).
  Proof using.
    intros;
      edestructs var_typ_all_to_binds corresponding_types val_typ_all_to_lambda.
    by_cofinite.
  Qed.

  Lemma var_rcd_one_dec : forall G (x : var) (a : trm_label) T,
      inert_env G ->
      G ⊢ trm_var x ⦂ {< decs_cons (dec_trm a T) decs_nil >} ->
      exists T' DS',
        binds x (typ_obj DS') G /\
        lbinds a (dec_trm a T') (open x DS') /\
        G ⊢ T' <⦂ T.
  Proof.
    intros.
    edestruct (type_means_binds x). eassumption.
    apply general_to_tight_typ in H0; trivial.
    apply tight_to_invertible in H0; trivial.
    apply invertible_one_dec in H0; trivial.
    destruct_conjs.
    repeat eexists; try eassumption.
    apply tight_to_general_subtyp. trivial.
  Qed.

  Local Ltac destr_hyp H :=
    tidy_up; edestruct H; try reflexivity;
    [ list_reasoning;
      try (eapply lbinds_app_l + eapply lbinds_app_r + idtac); solve [eroutine]
    | repeat eexists; eroutine].

  Lemma ty_defs_find : forall G ds DS (a : trm_label) T,
      G ⊩[ ds ⦂ DS ] ->
      lbinds a (a ∷ T) DS ->
      exists t,
        lbinds a (def_trm a t) ds /\ G ⊢ t ⦂ T.
  Proof.
    induction on ty_defs; eroutine.
    destruct IHty_defs; eroutine.
  Qed.
  
  Lemma invertible_v_obj_lbinds : forall G ds DS (a : trm_label) T (x : var),
      uniq G ->
      G ⊢##v [ DS ]{ ds } ⦂ μ{DS} ->
      G ⊢ trm_var x ⦂ μ{DS} ->
      lbinds a (a ∷ T) (open x DS) ->
      exists t,
        lbinds a (def_trm a t) (open x ds) /\ G ⊢ t ⦂ T /\ wf_defs ds.
  Proof.
    intros. tidy_up. pick_fresh y.
    assert (y `notin` L) by auto.
    specialize (H6 _ H3).
    eapply renaming_defs in H6; eauto.
    eapply ty_defs_find in H6; eauto.
    eroutine.
  Qed.
  
  Lemma canonical_form_obj : forall G s (x : var) a T,
      inert_env G ->
      well_typed G s ->
      G ⊢ trm_var x ⦂ {<decs_cons (a ∷ T) decs_nil >} ->
      exists ds DS t,
        binds x (val_obj DS ds) s /\
        lbinds a (def_trm a t) (open x ds) /\
        wf_defs ds /\
        G ⊢ t ⦂ T.
  Proof.
    intros. destruct (var_rcd_one_dec G x a T); trivial.
    destruct_conjs.
    pose proof (corresponding_types _ _ _ _ H0 H3).
    destruct_conjs.
    apply general_to_tight_typ in H8; trivial.
    apply tight_to_invertible_v in H8; trivial.
    tidy_up. eapply invertible_v_obj_lbinds in H8; eauto.
    tidy_up; repeat eexists; try eassumption.
    eauto.
  Qed.

  Lemma wf_defs_lookup : forall ds (a : trm_label) t1 t2,
      wf_defs ds ->
      lbinds a (def_trm a t1) ds ->
      lbinds a (def_trm a t2) ds ->
      t1 = t2.
  Proof.
    intros. tidy_up.
    pose proof (LabelAssocList.binds_unique _ _ _ _ _ H0 H1 H).
    routine.
  Qed.
  
End CanonicalForms.