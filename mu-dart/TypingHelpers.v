Set Implicit Arguments.
Require Import Definitions.
Require Import Helpers.

From Equations Require Import Equations.

Section DecsTypLookup.

  Fixpoint decs_typ_lookup (A : typ_label) (DS : decs) : option (typ * typ) :=
    match DS with
    | decs_nil => None
    | decs_cons (dec_trm _ _) DS => decs_typ_lookup A DS
    | decs_cons (dec_typ B T1 T2) DS =>
      match A == B with
      | left eq => Some (T1, T2)
      | right neq => decs_typ_lookup A DS
      end
    end.

  Lemma decs_typ_lookup_sound1 : forall (A : typ_label) (DS : decs) T1 T2,
      decs_typ_lookup A DS = Some (T1, T2) ->
      lbinds A (dec_typ A T1 T2) DS.
  Proof.
    induction on decs; routine.
    execute H1; routine.
    all: right; apply IHdecs; trivial.
  Qed.

  Lemma decs_typ_lookup_sound2 : forall (A : typ_label) (DS : decs),
      decs_typ_lookup A DS = None ->
      A `lnotin` ldom DS.
  Proof.
    induction on decs; routine.
    execute H1; simpl in *.
    all: apply IHdecs in H1;
      apply LabelSetNotin.notin_add_3; routine.
  Qed.
  
  Lemma decs_typ_lookup_complete : forall (A : typ_label) (DS : decs) T1 T2,
      lbinds A (dec_typ A T1 T2) DS ->
      luniq DS ->
      decs_typ_lookup A DS = Some (T1, T2).
  Proof.
    induction on decs; routine.
    execute; auto.
    apply LabelAssocList.binds_In in H1.
    contradiction.
  Qed.

  Lemma decs_typ_lookup_lc : forall (A : typ_label) (DS : decs) T1 T2,
      decs_typ_lookup A DS = Some (T1, T2) ->
      lc DS ->
      lc T1 /\ lc T2.
  Proof.
    intros. apply decs_typ_lookup_sound1 in H.
    apply lc_at_binds_decs with (n := 0) in H; routine.
  Qed.

  Lemma decs_typ_lookup_fv : forall (G : env) (A : typ_label) (DS : decs) T1 T2,
      decs_typ_lookup A DS = Some (T1, T2) ->
      fv DS [<=] dom G ->
      fv T1 [<=] dom G /\ fv T2 [<=] dom G.
  Proof.
    intros. apply decs_typ_lookup_sound1 in H.
    apply fv_decs_shrinks in H. routine.
  Qed.
  
End DecsTypLookup.

Section DecsTrmLookup.

  Fixpoint decs_trm_lookup (a : trm_label) (DS : decs) : option typ :=
    match DS with
    | decs_nil => None
    | decs_cons (dec_trm b T) DS =>
      match a == b with
      | left _ => Some T
      | right _ => decs_trm_lookup a DS
      end
    | decs_cons (dec_typ _ _ _) DS =>
      decs_trm_lookup a DS
    end.

  Lemma decs_trm_lookup_sound1 : forall (a : trm_label) (DS : decs) T,
      decs_trm_lookup a DS = Some T ->
      lbinds a (dec_trm a T) DS.
  Proof.
    induction on decs; routine.
    execute H1; routine.
    all:right; apply IHdecs; auto.
  Qed.

  Lemma decs_trm_lookup_sound2 : forall (a : trm_label) (DS : decs),
      decs_trm_lookup a DS = None ->
      a `lnotin` ldom DS.
  Proof.
    induction on decs; routine.
    execute H1; auto.
    all: apply IHdecs in H1;
      apply LabelSetNotin.notin_add_3; routine.
  Qed.
  
  Lemma decs_trm_lookup_complete : forall (a : trm_label) (DS : decs) T,
      lbinds a (dec_trm a T) DS ->
      luniq DS ->
      decs_trm_lookup a DS = Some T.
  Proof.
    induction on decs; routine.
    execute; auto.
    apply LabelAssocList.binds_In in H1.
    contradiction.
  Qed.
  
  Lemma decs_trm_lookup_lc : forall (a : trm_label) (DS : decs) T,
      decs_trm_lookup a DS = Some T ->
      lc DS ->
      lc T.
  Proof.
    intros. apply decs_trm_lookup_sound1 in H.
    apply lc_at_binds_decs with (n := 0) in H; routine.
  Qed.

  Lemma decs_trm_lookup_fv : forall (G : env) (a : trm_label) (DS : decs) T,
      decs_trm_lookup a DS = Some T ->
      fv DS [<=] dom G ->
      fv T [<=] dom G.
  Proof.
    intros. apply decs_trm_lookup_sound1 in H.
    apply lookup_trm_closed_decs with (G := G) in H; trivial.
  Qed.
  
End DecsTrmLookup.

Definition decs_typ_lookup_u (A : typ_label) (DS : decs) (tup : option (typ * typ))
           (H : decs_typ_lookup A DS = tup) :
  {T1 & {T2 | lbinds A (dec_typ A T1 T2) DS } } +
  { A `lnotin` ldom DS }.
Proof.
  refine (match tup as o
                return decs_typ_lookup A DS = o
                       -> {T1 & {T2 | lbinds A (dec_typ A T1 T2) DS } } +
                          { A `lnotin` ldom DS } with
          | Some t => fun H => inleft (existT _ (fst t) (exist _ (snd t) _))
          | None => fun H => inright _
          end H).
  - abstract (destruct t; simpl; apply decs_typ_lookup_sound1; trivial).
  - abstract (apply decs_typ_lookup_sound2; trivial).
Defined.

Lemma decs_typ_lookup_u_sound1 : forall A DS tup H T1 T2 pf,
    @decs_typ_lookup_u A DS tup H = inleft (existT _ T1 (exist _ T2 pf)) ->
    tup = Some (T1, T2).
Proof. intros. destruct tup; routine. Qed.

Lemma decs_typ_lookup_u_sound2 : forall A DS tup H pf,
    @decs_typ_lookup_u A DS tup H = inright pf ->
    tup = None.
Proof. intros. destruct tup; routine. Qed.

Definition decs_typ_lookup_d (A : typ_label) (DS : decs) :
  {T1 & {T2 | lbinds A (dec_typ A T1 T2) DS } }  +
  { A `lnotin` ldom DS } := decs_typ_lookup_u A DS eq_refl.

Lemma decs_typ_lookup_d_correct1 : forall A DS T1 T2 pf,
    decs_typ_lookup_d A DS = inleft (existT _ T1 (exist _ T2 pf)) ->
    decs_typ_lookup A DS = Some (T1, T2).
Proof.
  intros. unfold decs_typ_lookup_d in H.
  apply decs_typ_lookup_u_sound1 in H. trivial.
Qed.

Lemma decs_typ_lookup_d_correct2 : forall A DS pf,
    decs_typ_lookup_d A DS = inright pf ->
    decs_typ_lookup A DS = None.
Proof.
  intros. unfold decs_typ_lookup_d in H.
  apply decs_typ_lookup_u_sound2 in H. trivial.
Qed.

Definition decs_trm_lookup_u (a : trm_label) (DS : decs) (op : option typ)
           (H : decs_trm_lookup a DS = op) :
  {T  | lbinds a (dec_trm a T) DS } +
  { a `lnotin` ldom DS }.
Proof.
  refine (match op as o
                return decs_trm_lookup a DS = o ->
                       {T  | lbinds a (dec_trm a T) DS } +
                       { a `lnotin` ldom DS } with
          | Some T => fun H => inleft (exist _ T _)
          | None => fun H => inright _
          end H).
  - abstract (apply decs_trm_lookup_sound1; trivial).
  - abstract (apply decs_trm_lookup_sound2; trivial).
Defined.

Lemma decs_trm_lookup_u_sound1 : forall a DS op H T pf,
    @decs_trm_lookup_u a DS op H = inleft (exist _ T pf) ->
    op = Some T.
Proof. intros. destruct op; routine. Qed.

Lemma decs_trm_lookup_u_sound2 : forall a DS op H pf,
    @decs_trm_lookup_u a DS op H = inright pf ->
    op = None.
Proof. intros. destruct op; routine. Qed.

Definition decs_trm_lookup_d (a : trm_label) (DS : decs) :
  {T  | lbinds a (dec_trm a T) DS } +
  { a `lnotin` ldom DS } := decs_trm_lookup_u a DS eq_refl.

Lemma decs_trm_lookup_d_correct1 : forall a DS T pf,
    decs_trm_lookup_d a DS = inleft (exist _ T pf) ->
    decs_trm_lookup a DS = Some T.
Proof.
  intros. unfold decs_trm_lookup_d in H.
  apply decs_trm_lookup_u_sound1 in H. trivial.
Qed.

Lemma decs_trm_lookup_d_correct2 : forall a DS pf,
    decs_trm_lookup_d a DS = inright pf ->
    decs_trm_lookup a DS = None.
Proof.
  intros. unfold decs_trm_lookup_d in H.
  apply decs_trm_lookup_u_sound2 in H. trivial.
Qed.
