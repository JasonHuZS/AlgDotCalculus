Set Implicit Arguments.
Require Import Definitions.
Require Import TypeCheckSpecs.
Require Export Measures.

Definition total (ns : list nat) : nat :=
  fold_right plus 0 ns.

Lemma total_app : forall ns1 ns2,
    total (ns1 ++ ns2) = total ns1 + total ns2.
Proof.
  unfold total.
  induction on list; simpl in *; intros; trivial.
  rewrite IHlist. lia.
Qed.

Fixpoint pc_mu_measure (DS : decs) (l : list typ_label) : nat :=
  match DS with
  | decs_nil => 1
  | decs_cons (dec_typ A T U) DS' =>
    if In_dec A l then
      2 + typ_struct_measure T + typ_struct_measure U + pc_mu_measure DS' l
    else pc_mu_measure DS' l
  | decs_cons (dec_trm a T) DS' =>
    2 + typ_struct_measure T + pc_mu_measure DS' l
  end.

Definition precord_measure (T : precord) : nat :=
  match T with
  | pc_top => 1
  | pc_bot => 1
  | pc_sel _ _ => 2
  | pc_all T U =>
    1 + typ_struct_measure T + typ_struct_measure U
  | pc_rcd DS => 1 + decs_struct_measure DS
  | pc_mu DS l => 2 + pc_mu_measure DS l
  end.

Definition permits_weight (P : permits) : nat :=
  total (List.map (fun (tup : (var * precord)) =>
                     let (_, T) := tup in precord_measure T) P).
Arguments permits_weight P : simpl never.

Lemma permits_weight_cons : forall x T P,
    permits_weight ((x, T) :: P) = permits_weight P + precord_measure T.
Proof.
  unfold permits_weight. intros. simpl. lia.
Qed.

Lemma permits_weight_app : forall P1 P2,
    permits_weight (P1 ++ P2) = permits_weight P1 + permits_weight P2.
Proof.
  induction P1; intros; auto.
  simpl. destruct a. repeat rewrite permits_weight_cons.
  rewrite IHP1. lia.
Qed.

Definition reveal_measure (P : permits) (T : precord) : nat :=
  permits_weight P + precord_measure T.
Arguments reveal_measure P T/.

Definition subtyp_measure (P : permits) (T : typ) : nat :=
  permits_weight P + typ_struct_measure T.
Arguments subtyp_measure P T/.

Lemma pc_mu_measure_labs_gen : forall DS l,
    included (collect_ty_labs DS) l ->
    pc_mu_measure DS l = decs_struct_measure DS.
Proof.
  induction DS; trivial; intros.
  destruct d; simpl.
  destruct (In_dec t l); simpl in *.
  - rewrite IHDS; trivial.
    rewrite Forall_forall in *. intros. apply H.
    right. trivial.
  - exfalso. apply n. rewrite Forall_forall in *.
    apply H. left. trivial.
  - rewrite IHDS; trivial.
Qed.

Lemma pc_mu_measure_labs : forall DS,
    pc_mu_measure DS (collect_ty_labs DS) = decs_struct_measure DS.
Proof.
  intros. apply pc_mu_measure_labs_gen.
  rewrite Forall_forall. intros. trivial.
Qed.

Lemma precord_measure_from_typ : forall T,
    precord_measure (from_typ T) = typ_struct_measure T.
Proof.
  induction T; simpl; trivial.
  rewrite pc_mu_measure_labs. trivial.
Qed.

Lemma subtyp_measure_all_case : forall P T U x,
    subtyp_measure P (typ_all T U)
    = 1 + subtyp_measure (x ~ from_typ T ++ P) (open x U).
Proof.
  intros. simpl. rewrite permits_weight_cons.
  rewrite precord_measure_from_typ.
  rewrite open_typ_same_measure. lia.
Qed.

Lemma pc_mu_measure_lnotin : forall (A : typ_label) (DS : decs) l,
    A `lnotin` ldom DS ->
    pc_mu_measure DS (In_remove A l) = pc_mu_measure DS l.
Proof.
  induction DS; intros; simpl; trivial.
  - destruct d.
    + simpl in *. assert (A `lnotin` ldom DS) by auto.
      eapply IHDS in H0. rewrite H0.
      destruct (In_dec t (In_remove A l)), (In_dec t l); trivial.
      * apply in_before_removal in i. contradiction.
      * apply eq_after_removal in n; trivial. subst.
        exfalso. apply H. auto.
    + simpl in *. assert (A `lnotin` ldom DS) by auto.
      eapply IHDS in H0. rewrite H0. trivial.
Qed.

Lemma pc_mu_measure_remove_lab : forall (A : typ_label) (DS : decs) l,
    pc_mu_measure DS (In_remove A l) <= pc_mu_measure DS l.
Proof.
  induction DS; intros.
  - routine.
  - destruct d; simpl.
    + destruct (In_dec t (In_remove A l)), (In_dec t l); trivial.
      * specialize (IHDS l). lia.
      * apply in_before_removal in i. contradiction.
      * specialize (IHDS l). lia.
    + specialize (IHDS l). lia.
Qed.
  
Lemma pc_mu_measure_lbinds : forall (A : typ_label) L U (DS : decs) l,
    lbinds A (dec_typ A L U) DS ->
    In A l ->
    pc_mu_measure DS (In_remove A l) + typ_struct_measure L < pc_mu_measure DS l /\
    pc_mu_measure DS (In_remove A l) + typ_struct_measure U < pc_mu_measure DS l.
Proof.
  induction DS; intros; tidy_up.
  - destruct (In_dec A l); try contradiction.
    pose proof (In_removed A l).
    destruct (In_dec A (In_remove A l)); try contradiction.
    pose proof (pc_mu_measure_remove_lab A DS l).
    split; lia.
  - edestruct IHDS; eauto.
    pose proof (pc_mu_measure_remove_lab A DS l).
    destruct d; simpl.
    + destruct (In_dec t (In_remove A l)), (In_dec t l); try lia.
      apply in_before_removal in i. contradiction.
    + lia.
Qed.

Lemma pc_mu_measure_open_invar : forall n x DS l,
    pc_mu_measure (open_rec n x DS) l = pc_mu_measure DS l.
Proof.
  induction DS; simpl; intros; auto.
  repeat rewrite IHDS. 
  destruct d; simpl;
    repeat rewrite open_typ_same_measure; trivial.
Qed.    

Lemma precord_measure_ge_1 : forall T,
    precord_measure T >= 1.
Proof. destruct T; simpl; lia. Qed.