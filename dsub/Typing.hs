{-# OPTIONS_GHC -cpp -XMagicHash #-}
{- For Hugs, use the option -F"cpp -P -traditional" -}

module Typing where

import qualified Prelude

#ifdef __GLASGOW_HASKELL__
import qualified GHC.Base
#else
-- HUGS
import qualified IOExts
#endif

#ifdef __GLASGOW_HASKELL__
unsafeCoerce :: a -> b
unsafeCoerce = GHC.Base.unsafeCoerce#
#else
-- HUGS
unsafeCoerce :: a -> b
unsafeCoerce = IOExts.unsafeCoerce
#endif

#ifdef __GLASGOW_HASKELL__
type Any = GHC.Base.Any
#else
-- HUGS
type Any = ()
#endif

__ :: any
__ = Prelude.error "Logical or arity value used"

false_rect :: a1
false_rect =
  Prelude.error "absurd case"

data Unit =
   Tt

data Bool =
   True
 | False

andb :: Bool -> Bool -> Bool
andb b1 b2 =
  case b1 of {
   True -> b2;
   False -> False}

negb :: Bool -> Bool
negb b =
  case b of {
   True -> False;
   False -> True}

data Nat =
   O
 | S Nat

nat_rect :: a1 -> (Nat -> a1 -> a1) -> Nat -> a1
nat_rect f f0 n =
  case n of {
   O -> f;
   S n0 -> f0 n0 (nat_rect f f0 n0)}

data Option a =
   Some a
 | None

data Sum a b =
   Inl a
 | Inr b

data Prod a b =
   Pair a b

fst :: (Prod a1 a2) -> a1
fst p =
  case p of {
   Pair x _ -> x}

snd :: (Prod a1 a2) -> a2
snd p =
  case p of {
   Pair _ y -> y}

data List a =
   Nil
 | Cons a (List a)

list_rect :: a2 -> (a1 -> (List a1) -> a2 -> a2) -> (List a1) -> a2
list_rect f f0 l =
  case l of {
   Nil -> f;
   Cons y l0 -> f0 y l0 (list_rect f f0 l0)}

length :: (List a1) -> Nat
length l =
  case l of {
   Nil -> O;
   Cons _ l' -> S (length l')}

app :: (List a1) -> (List a1) -> List a1
app l m =
  case l of {
   Nil -> m;
   Cons a l1 -> Cons a (app l1 m)}

type Sig a = a
  -- singleton inductive, whose constructor was exist
  
data SigT a p =
   ExistT a p

projT1 :: (SigT a1 a2) -> a1
projT1 x =
  case x of {
   ExistT a _ -> a}

projT2 :: (SigT a1 a2) -> a2
projT2 x =
  case x of {
   ExistT _ h -> h}

data Sumbool =
   Left
 | Right

sumbool_rect :: (() -> a1) -> (() -> a1) -> Sumbool -> a1
sumbool_rect f f0 s =
  case s of {
   Left -> f __;
   Right -> f0 __}

flip :: (a1 -> a2 -> a3) -> a2 -> a1 -> a3
flip f x y =
  f y x

max :: Nat -> Nat -> Nat
max n m =
  case n of {
   O -> m;
   S n' -> case m of {
            O -> n;
            S m' -> S (max n' m')}}

eq_dec :: Nat -> Nat -> Sumbool
eq_dec n =
  nat_rect (\m -> case m of {
                   O -> Left;
                   S _ -> Right}) (\_ iHn m ->
    case m of {
     O -> Right;
     S m0 -> iHn m0}) n

in_dec :: (a1 -> a1 -> Sumbool) -> a1 -> (List a1) -> Sumbool
in_dec h a l =
  list_rect Right (\a0 _ iHl ->
    let {s = h a0 a} in case s of {
                         Left -> Left;
                         Right -> iHl}) l

rev :: (List a1) -> List a1
rev l =
  case l of {
   Nil -> Nil;
   Cons x l' -> app (rev l') (Cons x Nil)}

map :: (a1 -> a2) -> (List a1) -> List a2
map f l =
  case l of {
   Nil -> Nil;
   Cons a t -> Cons (f a) (map f t)}

fold_left :: (a1 -> a2 -> a1) -> (List a2) -> a1 -> a1
fold_left f l a0 =
  case l of {
   Nil -> a0;
   Cons b t -> fold_left f t (f a0 b)}

fold_right :: (a2 -> a1 -> a1) -> a1 -> (List a2) -> a1
fold_right f a0 l =
  case l of {
   Nil -> a0;
   Cons b t -> f b (fold_right f a0 t)}

le_lt_dec :: Nat -> Nat -> Sumbool
le_lt_dec n m =
  nat_rect (\_ -> Left) (\_ iHn m0 ->
    case m0 of {
     O -> Right;
     S m1 -> sumbool_rect (\_ -> Left) (\_ -> Right) (iHn m1)}) n m

type EqDec a = a -> a -> Sumbool

nat_eq_eqdec :: EqDec Nat
nat_eq_eqdec =
  eq_dec

type EqDec_eq a = a -> a -> Sumbool

eq_dec0 :: (EqDec_eq a1) -> a1 -> a1 -> Sumbool
eq_dec0 eqDec_eq =
  eqDec_eq

eqDec_eq_of_EqDec :: (EqDec a1) -> EqDec_eq a1
eqDec_eq_of_EqDec h =
  h

type Atom = Nat

type T = Atom

eq_dec1 :: Nat -> Nat -> Sumbool
eq_dec1 =
  eq_dec

nat_list_max :: (List Nat) -> Nat
nat_list_max xs =
  list_rect O (\x _ h0 -> max x h0) xs

atom_fresh_for_list :: (List Nat) -> Nat
atom_fresh_for_list xs =
  let {s = nat_list_max xs} in S s

fresh :: (List Atom) -> Nat
fresh =
  atom_fresh_for_list

fresh_not_in :: ()
fresh_not_in =
  __

nat_of :: Atom -> Atom
nat_of x =
  x

type Eq = ()

eq_equiv :: ()
eq_equiv =
  __

eq_refl :: ()
eq_refl =
  __

eq_sym :: ()
eq_sym =
  __

eq_trans :: ()
eq_trans =
  __

type T0 = Atom

eq_dec2 :: Atom -> Atom -> Sumbool
eq_dec2 =
  eq_dec1

type T1 = Atom

eq_dec3 :: Atom -> Atom -> Sumbool
eq_dec3 =
  eq_dec1

type Elt = Atom

type T2 = List Elt

empty :: T2
empty =
  Nil

is_empty :: T2 -> Bool
is_empty l =
  case l of {
   Nil -> True;
   Cons _ _ -> False}

mem :: Elt -> T2 -> Bool
mem x s =
  case s of {
   Nil -> False;
   Cons y l -> case eq_dec3 x y of {
                Left -> True;
                Right -> mem x l}}

add :: Elt -> T2 -> T2
add x s =
  case s of {
   Nil -> Cons x Nil;
   Cons y l -> case eq_dec3 x y of {
                Left -> s;
                Right -> Cons y (add x l)}}

singleton :: Elt -> T2
singleton x =
  Cons x Nil

remove :: Elt -> T2 -> T2
remove x s =
  case s of {
   Nil -> Nil;
   Cons y l -> case eq_dec3 x y of {
                Left -> l;
                Right -> Cons y (remove x l)}}

fold :: (Elt -> a1 -> a1) -> T2 -> a1 -> a1
fold f =
  fold_left (flip f)

union :: T2 -> T2 -> T2
union s =
  fold add s

diff :: T2 -> T2 -> T2
diff s s' =
  fold remove s' s

inter :: T2 -> T2 -> T2
inter s s' =
  fold (\x s0 -> case mem x s' of {
                  True -> add x s0;
                  False -> s0}) s Nil

subset :: T2 -> T2 -> Bool
subset s s' =
  is_empty (diff s s')

equal :: T2 -> T2 -> Bool
equal s s' =
  andb (subset s s') (subset s' s)

filter :: (Elt -> Bool) -> T2 -> T2
filter f s =
  case s of {
   Nil -> Nil;
   Cons x l ->
    case f x of {
     True -> Cons x (filter f l);
     False -> filter f l}}

for_all :: (Elt -> Bool) -> T2 -> Bool
for_all f s =
  case s of {
   Nil -> True;
   Cons x l -> case f x of {
                True -> for_all f l;
                False -> False}}

exists_ :: (Elt -> Bool) -> T2 -> Bool
exists_ f s =
  case s of {
   Nil -> False;
   Cons x l -> case f x of {
                True -> True;
                False -> exists_ f l}}

partition :: (Elt -> Bool) -> T2 -> Prod T2 T2
partition f s =
  case s of {
   Nil -> Pair Nil Nil;
   Cons x l ->
    case partition f l of {
     Pair s1 s2 ->
      case f x of {
       True -> Pair (Cons x s1) s2;
       False -> Pair s1 (Cons x s2)}}}

cardinal :: T2 -> Nat
cardinal =
  length

elements :: T2 -> List Elt
elements s =
  s

choose :: T2 -> Option Elt
choose s =
  case s of {
   Nil -> None;
   Cons x _ -> Some x}

isok :: (List Elt) -> Bool
isok l =
  case l of {
   Nil -> True;
   Cons a l0 -> andb (negb (mem a l0)) (isok l0)}

type T3 = Atom

eq_dec4 :: Atom -> Atom -> Sumbool
eq_dec4 =
  eq_dec3

type Elt0 = Atom

type T_ = T2
  -- singleton inductive, whose constructor was Mkt
  
this :: T_ -> T2
this t =
  t

type T4 = T_

mem0 :: Elt0 -> T4 -> Bool
mem0 x s =
  mem x (this s)

add0 :: Elt0 -> T4 -> T4
add0 x s =
  add x (this s)

remove0 :: Elt0 -> T4 -> T4
remove0 x s =
  remove x (this s)

singleton0 :: Elt0 -> T4
singleton0 =
  singleton

union0 :: T4 -> T4 -> T4
union0 s s' =
  union (this s) (this s')

inter0 :: T4 -> T4 -> T4
inter0 s s' =
  inter (this s) (this s')

diff0 :: T4 -> T4 -> T4
diff0 s s' =
  diff (this s) (this s')

equal0 :: T4 -> T4 -> Bool
equal0 s s' =
  equal (this s) (this s')

subset0 :: T4 -> T4 -> Bool
subset0 s s' =
  subset (this s) (this s')

empty0 :: T4
empty0 =
  empty

is_empty0 :: T4 -> Bool
is_empty0 s =
  is_empty (this s)

elements0 :: T4 -> List Elt0
elements0 s =
  elements (this s)

choose0 :: T4 -> Option Elt0
choose0 s =
  choose (this s)

fold0 :: (Elt0 -> a1 -> a1) -> T4 -> a1 -> a1
fold0 f s =
  fold f (this s)

cardinal0 :: T4 -> Nat
cardinal0 s =
  cardinal (this s)

filter0 :: (Elt0 -> Bool) -> T4 -> T4
filter0 f s =
  filter f (this s)

for_all0 :: (Elt0 -> Bool) -> T4 -> Bool
for_all0 f s =
  for_all f (this s)

exists_0 :: (Elt0 -> Bool) -> T4 -> Bool
exists_0 f s =
  exists_ f (this s)

partition0 :: (Elt0 -> Bool) -> T4 -> Prod T4 T4
partition0 f s =
  let {p = partition f (this s)} in Pair (fst p) (snd p)

eq_dec5 :: T4 -> T4 -> Sumbool
eq_dec5 s0 s'0 =
  let {b = equal s0 s'0} in case b of {
                             True -> Left;
                             False -> Right}

type Elt1 = Atom

type T5 = T4

empty1 :: T5
empty1 =
  empty0

is_empty1 :: T5 -> Bool
is_empty1 =
  is_empty0

mem1 :: Elt1 -> T5 -> Bool
mem1 =
  mem0

add1 :: Elt1 -> T5 -> T5
add1 =
  add0

singleton1 :: Elt1 -> T5
singleton1 =
  singleton0

remove1 :: Elt1 -> T5 -> T5
remove1 =
  remove0

union1 :: T5 -> T5 -> T5
union1 =
  union0

inter1 :: T5 -> T5 -> T5
inter1 =
  inter0

diff1 :: T5 -> T5 -> T5
diff1 =
  diff0

eq_dec6 :: T5 -> T5 -> Sumbool
eq_dec6 =
  eq_dec5

equal1 :: T5 -> T5 -> Bool
equal1 =
  equal0

subset1 :: T5 -> T5 -> Bool
subset1 =
  subset0

fold1 :: (Elt1 -> a1 -> a1) -> T5 -> a1 -> a1
fold1 =
  fold0

for_all1 :: (Elt1 -> Bool) -> T5 -> Bool
for_all1 =
  for_all0

exists_1 :: (Elt1 -> Bool) -> T5 -> Bool
exists_1 =
  exists_0

filter1 :: (Elt1 -> Bool) -> T5 -> T5
filter1 =
  filter0

partition1 :: (Elt1 -> Bool) -> T5 -> Prod T5 T5
partition1 =
  partition0

cardinal1 :: T5 -> Nat
cardinal1 =
  cardinal0

elements1 :: T5 -> List Elt1
elements1 =
  elements0

choose1 :: T5 -> Option Elt1
choose1 =
  choose0

eqb :: Atom -> Atom -> Bool
eqb x y =
  case eq_dec4 x y of {
   Left -> True;
   Right -> False}

atom_fresh :: T5 -> Atom
atom_fresh l =
  atom_fresh_for_list (elements1 l)

eqDec_of_X :: EqDec Atom
eqDec_of_X =
  eq_dec1

eqDec_eq_of_X :: EqDec_eq Atom
eqDec_eq_of_X =
  eqDec_eq_of_EqDec eqDec_of_X

eqb0 :: Atom -> Atom -> Bool
eqb0 x y =
  case eq_dec1 x y of {
   Left -> True;
   Right -> False}

eqb1 :: Atom -> Atom -> Bool
eqb1 x y =
  case eq_dec1 x y of {
   Left -> True;
   Right -> False}

eqb2 :: Atom -> Atom -> Bool
eqb2 x y =
  case eq_dec1 x y of {
   Left -> True;
   Right -> False}

in_dec0 :: Elt1 -> T5 -> Sumbool
in_dec0 x s =
  case mem1 x s of {
   True -> Left;
   False -> Right}

of_list :: (List Elt1) -> T5
of_list l =
  fold_right add1 empty1 l

to_list :: T5 -> List Elt1
to_list =
  elements1

fold_rec :: (Elt1 -> a1 -> a1) -> a1 -> T5 -> (T5 -> () -> a2) -> (Elt1 -> a1
            -> T5 -> T5 -> () -> () -> () -> a2 -> a2) -> a2
fold_rec f i s pempty pstep =
  let {l = rev (elements1 s)} in
  let {pstep' = \x a s' s'' x0 -> pstep x a s' s'' __ __ __ x0} in
  list_rect (\_ _ s0 _ -> pempty s0 __) (\a l0 iHl pstep'0 _ s0 _ ->
    pstep'0 a (fold_right f i l0) (of_list l0) s0 __ __ __
      (iHl (\x a0 s' s'' _ _ _ x0 -> pstep'0 x a0 s' s'' __ __ __ x0) __
        (of_list l0) __)) l (\x a s' s'' _ _ _ x0 -> pstep' x a s' s'' x0) __
    s __

fold_rec_bis :: (Elt1 -> a1 -> a1) -> a1 -> T5 -> (T5 -> T5 -> a1 -> () -> a2
                -> a2) -> a2 -> (Elt1 -> a1 -> T5 -> () -> () -> a2 -> a2) ->
                a2
fold_rec_bis f i s pmorphism pempty pstep =
  fold_rec f i s (\s' _ -> pmorphism empty1 s' i __ pempty)
    (\x a s' s'' _ _ _ x0 ->
    pmorphism (add1 x s') s'' (f x a) __ (pstep x a s' __ __ x0))

fold_rec_nodep :: (Elt1 -> a1 -> a1) -> a1 -> T5 -> a2 -> (Elt1 -> a1 -> ()
                  -> a2 -> a2) -> a2
fold_rec_nodep f i s x x0 =
  fold_rec_bis f i s (\_ _ _ _ x1 -> x1) x (\x1 a _ _ _ x2 -> x0 x1 a __ x2)

fold_rec_weak :: (Elt1 -> a1 -> a1) -> a1 -> (T5 -> T5 -> a1 -> () -> a2 ->
                 a2) -> a2 -> (Elt1 -> a1 -> T5 -> () -> a2 -> a2) -> T5 ->
                 a2
fold_rec_weak f i x x0 x1 s =
  fold_rec_bis f i s x x0 (\x2 a s' _ _ x3 -> x1 x2 a s' __ x3)

fold_rel :: (Elt1 -> a1 -> a1) -> (Elt1 -> a2 -> a2) -> a1 -> a2 -> T5 -> a3
            -> (Elt1 -> a1 -> a2 -> () -> a3 -> a3) -> a3
fold_rel f g i j s rempty rstep =
  let {l = rev (elements1 s)} in
  let {rstep' = \x a b x0 -> rstep x a b __ x0} in
  list_rect (\_ -> rempty) (\a l0 iHl rstep'0 ->
    rstep'0 a (fold_right f i l0) (fold_right g j l0) __
      (iHl (\x a0 b _ x0 -> rstep'0 x a0 b __ x0))) l (\x a b _ x0 ->
    rstep' x a b x0)

set_induction :: (T5 -> () -> a1) -> (T5 -> T5 -> a1 -> Elt1 -> () -> () ->
                 a1) -> T5 -> a1
set_induction x x0 s =
  fold_rec (\_ _ -> Tt) Tt s x (\x1 _ s' s'' _ _ _ x2 ->
    x0 s' s'' x2 x1 __ __)

set_induction_bis :: (T5 -> T5 -> () -> a1 -> a1) -> a1 -> (Elt1 -> T5 -> ()
                     -> a1 -> a1) -> T5 -> a1
set_induction_bis x x0 x1 s =
  fold_rec_bis (\_ _ -> Tt) Tt s (\s0 s' _ _ x2 -> x s0 s' __ x2) x0
    (\x2 _ s' _ _ x3 -> x1 x2 s' __ x3)

cardinal_inv_2 :: T5 -> Nat -> Elt1
cardinal_inv_2 s _ =
  let {l = elements1 s} in case l of {
                            Nil -> false_rect;
                            Cons e _ -> e}

cardinal_inv_2b :: T5 -> Elt1
cardinal_inv_2b s =
  let {n = cardinal1 s} in
  let {x = \x -> cardinal_inv_2 s x} in
  case n of {
   O -> false_rect;
   S n0 -> x n0}

eqb3 :: Atom -> Atom -> Bool
eqb3 x y =
  case eq_dec1 x y of {
   Left -> True;
   Right -> False}

one :: a1 -> List a1
one item =
  Cons item Nil

dom :: (List (Prod Atom a1)) -> T5
dom e =
  case e of {
   Nil -> empty1;
   Cons p e' -> case p of {
                 Pair x _ -> add1 x (dom e')}}

get :: Atom -> (List (Prod Atom a1)) -> Option a1
get x e =
  case e of {
   Nil -> None;
   Cons p f ->
    case p of {
     Pair y c ->
      case eq_dec0 eqDec_eq_of_X x y of {
       Left -> Some c;
       Right -> get x f}}}

map0 :: (a1 -> a2) -> (List (Prod Atom a1)) -> List (Prod Atom a2)
map0 f e =
  map (\b -> case b of {
              Pair x a -> Pair x (f a)}) e

alist_ind :: a2 -> (Atom -> a1 -> (List (Prod Atom a1)) -> a2 -> a2) -> (List
             (Prod Atom a1)) -> a2
alist_ind x x0 xs =
  list_rect x (\a0 xs0 iHxs -> case a0 of {
                                Pair x1 a -> x0 x1 a xs0 iHxs}) xs

binds_dec :: Atom -> a1 -> (List (Prod Atom a1)) -> (a1 -> a1 -> Sumbool) ->
             Sumbool
binds_dec x a e x0 =
  in_dec (\x1 y ->
    case x1 of {
     Pair x2 x3 ->
      case y of {
       Pair t0 a1 ->
        sumbool_rect (\_ ->
          sumbool_rect (\_ -> Left) (\_ -> Right) (x0 x3 a1)) (\_ -> Right)
          (eq_dec1 x2 t0)}}) (Pair x a) e

binds_lookup :: Atom -> (List (Prod Atom a1)) -> Sum a1 ()
binds_lookup x e =
  alist_ind (Inr __) (\x1 a1 _ x0 ->
    case x0 of {
     Inl s -> Inl s;
     Inr _ ->
      let {s = eq_dec1 x x1} in case s of {
                                 Left -> Inl a1;
                                 Right -> Inr __}}) e

type CanOpen a =
  Nat -> Atom -> a -> a
  -- singleton inductive, whose constructor was Build_CanOpen
  
open_rec :: (CanOpen a1) -> Nat -> Atom -> a1 -> a1
open_rec canOpen =
  canOpen

type CanClose a =
  Nat -> Atom -> a -> a
  -- singleton inductive, whose constructor was Build_CanClose
  
close_rec :: (CanClose a1) -> Nat -> Atom -> a1 -> a1
close_rec canClose =
  canClose

type Typ_label = Nat
  -- singleton inductive, whose constructor was typ_lab
  
type Trm_label = Nat
  -- singleton inductive, whose constructor was trm_lab
  
eqDecTypLab :: EqDec_eq Typ_label
eqDecTypLab x y =
  sumbool_rect (\_ -> Left) (\_ -> Right) (eq_dec x y)

eqDecTrmLab :: EqDec_eq Trm_label
eqDecTrmLab x y =
  sumbool_rect (\_ -> Left) (\_ -> Right) (eq_dec x y)

data Label =
   Label_typ Typ_label
 | Label_trm Trm_label

type T6 = Label

eq_dec7 :: T6 -> T6 -> Sumbool
eq_dec7 x y =
  case x of {
   Label_typ x0 ->
    case y of {
     Label_typ t1 ->
      sumbool_rect (\_ -> Left) (\_ -> Right) (eq_dec0 eqDecTypLab x0 t1);
     Label_trm _ -> Right};
   Label_trm x0 ->
    case y of {
     Label_typ _ -> Right;
     Label_trm t1 ->
      sumbool_rect (\_ -> Left) (\_ -> Right) (eq_dec0 eqDecTrmLab x0 t1)}}

type T7 = Label

eq_dec8 :: Label -> Label -> Sumbool
eq_dec8 =
  eq_dec7

type T8 = Label

eq_dec9 :: Label -> Label -> Sumbool
eq_dec9 =
  eq_dec7

type Elt2 = Label

type T9 = List Elt2

empty2 :: T9
empty2 =
  Nil

is_empty2 :: T9 -> Bool
is_empty2 l =
  case l of {
   Nil -> True;
   Cons _ _ -> False}

mem2 :: Elt2 -> T9 -> Bool
mem2 x s =
  case s of {
   Nil -> False;
   Cons y l -> case eq_dec9 x y of {
                Left -> True;
                Right -> mem2 x l}}

add2 :: Elt2 -> T9 -> T9
add2 x s =
  case s of {
   Nil -> Cons x Nil;
   Cons y l -> case eq_dec9 x y of {
                Left -> s;
                Right -> Cons y (add2 x l)}}

singleton2 :: Elt2 -> T9
singleton2 x =
  Cons x Nil

remove2 :: Elt2 -> T9 -> T9
remove2 x s =
  case s of {
   Nil -> Nil;
   Cons y l ->
    case eq_dec9 x y of {
     Left -> l;
     Right -> Cons y (remove2 x l)}}

fold2 :: (Elt2 -> a1 -> a1) -> T9 -> a1 -> a1
fold2 f =
  fold_left (flip f)

union2 :: T9 -> T9 -> T9
union2 s =
  fold2 add2 s

diff2 :: T9 -> T9 -> T9
diff2 s s' =
  fold2 remove2 s' s

inter2 :: T9 -> T9 -> T9
inter2 s s' =
  fold2 (\x s0 -> case mem2 x s' of {
                   True -> add2 x s0;
                   False -> s0}) s Nil

subset2 :: T9 -> T9 -> Bool
subset2 s s' =
  is_empty2 (diff2 s s')

equal2 :: T9 -> T9 -> Bool
equal2 s s' =
  andb (subset2 s s') (subset2 s' s)

filter2 :: (Elt2 -> Bool) -> T9 -> T9
filter2 f s =
  case s of {
   Nil -> Nil;
   Cons x l ->
    case f x of {
     True -> Cons x (filter2 f l);
     False -> filter2 f l}}

for_all2 :: (Elt2 -> Bool) -> T9 -> Bool
for_all2 f s =
  case s of {
   Nil -> True;
   Cons x l -> case f x of {
                True -> for_all2 f l;
                False -> False}}

exists_2 :: (Elt2 -> Bool) -> T9 -> Bool
exists_2 f s =
  case s of {
   Nil -> False;
   Cons x l -> case f x of {
                True -> True;
                False -> exists_2 f l}}

partition2 :: (Elt2 -> Bool) -> T9 -> Prod T9 T9
partition2 f s =
  case s of {
   Nil -> Pair Nil Nil;
   Cons x l ->
    case partition2 f l of {
     Pair s1 s2 ->
      case f x of {
       True -> Pair (Cons x s1) s2;
       False -> Pair s1 (Cons x s2)}}}

cardinal2 :: T9 -> Nat
cardinal2 =
  length

elements2 :: T9 -> List Elt2
elements2 s =
  s

choose2 :: T9 -> Option Elt2
choose2 s =
  case s of {
   Nil -> None;
   Cons x _ -> Some x}

isok0 :: (List Elt2) -> Bool
isok0 l =
  case l of {
   Nil -> True;
   Cons a l0 -> andb (negb (mem2 a l0)) (isok0 l0)}

type T10 = Label

eq_dec10 :: Label -> Label -> Sumbool
eq_dec10 =
  eq_dec9

type Elt3 = Label

type T_0 = T9
  -- singleton inductive, whose constructor was Mkt
  
this0 :: T_0 -> T9
this0 t =
  t

type T11 = T_0

mem3 :: Elt3 -> T11 -> Bool
mem3 x s =
  mem2 x (this0 s)

add3 :: Elt3 -> T11 -> T11
add3 x s =
  add2 x (this0 s)

remove3 :: Elt3 -> T11 -> T11
remove3 x s =
  remove2 x (this0 s)

singleton3 :: Elt3 -> T11
singleton3 =
  singleton2

union3 :: T11 -> T11 -> T11
union3 s s' =
  union2 (this0 s) (this0 s')

inter3 :: T11 -> T11 -> T11
inter3 s s' =
  inter2 (this0 s) (this0 s')

diff3 :: T11 -> T11 -> T11
diff3 s s' =
  diff2 (this0 s) (this0 s')

equal3 :: T11 -> T11 -> Bool
equal3 s s' =
  equal2 (this0 s) (this0 s')

subset3 :: T11 -> T11 -> Bool
subset3 s s' =
  subset2 (this0 s) (this0 s')

empty3 :: T11
empty3 =
  empty2

is_empty3 :: T11 -> Bool
is_empty3 s =
  is_empty2 (this0 s)

elements3 :: T11 -> List Elt3
elements3 s =
  elements2 (this0 s)

choose3 :: T11 -> Option Elt3
choose3 s =
  choose2 (this0 s)

fold3 :: (Elt3 -> a1 -> a1) -> T11 -> a1 -> a1
fold3 f s =
  fold2 f (this0 s)

cardinal3 :: T11 -> Nat
cardinal3 s =
  cardinal2 (this0 s)

filter3 :: (Elt3 -> Bool) -> T11 -> T11
filter3 f s =
  filter2 f (this0 s)

for_all3 :: (Elt3 -> Bool) -> T11 -> Bool
for_all3 f s =
  for_all2 f (this0 s)

exists_3 :: (Elt3 -> Bool) -> T11 -> Bool
exists_3 f s =
  exists_2 f (this0 s)

partition3 :: (Elt3 -> Bool) -> T11 -> Prod T11 T11
partition3 f s =
  let {p = partition2 f (this0 s)} in Pair (fst p) (snd p)

eq_dec11 :: T11 -> T11 -> Sumbool
eq_dec11 s0 s'0 =
  let {b = equal2 s0 s'0} in case b of {
                              True -> Left;
                              False -> Right}

type Elt4 = Label

type T12 = T11

empty4 :: T12
empty4 =
  empty3

is_empty4 :: T12 -> Bool
is_empty4 =
  is_empty3

mem4 :: Elt4 -> T12 -> Bool
mem4 =
  mem3

add4 :: Elt4 -> T12 -> T12
add4 =
  add3

singleton4 :: Elt4 -> T12
singleton4 =
  singleton3

remove4 :: Elt4 -> T12 -> T12
remove4 =
  remove3

union4 :: T12 -> T12 -> T12
union4 =
  union3

inter4 :: T12 -> T12 -> T12
inter4 =
  inter3

diff4 :: T12 -> T12 -> T12
diff4 =
  diff3

eq_dec12 :: T12 -> T12 -> Sumbool
eq_dec12 =
  eq_dec11

equal4 :: T12 -> T12 -> Bool
equal4 =
  equal3

subset4 :: T12 -> T12 -> Bool
subset4 =
  subset3

fold4 :: (Elt4 -> a1 -> a1) -> T12 -> a1 -> a1
fold4 =
  fold3

for_all4 :: (Elt4 -> Bool) -> T12 -> Bool
for_all4 =
  for_all3

exists_4 :: (Elt4 -> Bool) -> T12 -> Bool
exists_4 =
  exists_3

filter4 :: (Elt4 -> Bool) -> T12 -> T12
filter4 =
  filter3

partition4 :: (Elt4 -> Bool) -> T12 -> Prod T12 T12
partition4 =
  partition3

cardinal4 :: T12 -> Nat
cardinal4 =
  cardinal3

elements4 :: T12 -> List Elt4
elements4 =
  elements3

choose4 :: T12 -> Option Elt4
choose4 =
  choose3

eqb4 :: Label -> Label -> Bool
eqb4 x y =
  case eq_dec10 x y of {
   Left -> True;
   Right -> False}

eqDec_of_X0 :: EqDec Label
eqDec_of_X0 =
  eq_dec7

eqDec_eq_of_X0 :: EqDec_eq Label
eqDec_eq_of_X0 =
  eqDec_eq_of_EqDec eqDec_of_X0

eqb5 :: Label -> Label -> Bool
eqb5 x y =
  case eq_dec7 x y of {
   Left -> True;
   Right -> False}

eqb6 :: Label -> Label -> Bool
eqb6 x y =
  case eq_dec7 x y of {
   Left -> True;
   Right -> False}

eqb7 :: Label -> Label -> Bool
eqb7 x y =
  case eq_dec7 x y of {
   Left -> True;
   Right -> False}

in_dec1 :: Elt4 -> T12 -> Sumbool
in_dec1 x s =
  case mem4 x s of {
   True -> Left;
   False -> Right}

of_list0 :: (List Elt4) -> T12
of_list0 l =
  fold_right add4 empty4 l

to_list0 :: T12 -> List Elt4
to_list0 =
  elements4

fold_rec0 :: (Elt4 -> a1 -> a1) -> a1 -> T12 -> (T12 -> () -> a2) -> (Elt4 ->
             a1 -> T12 -> T12 -> () -> () -> () -> a2 -> a2) -> a2
fold_rec0 f i s pempty pstep =
  let {l = rev (elements4 s)} in
  let {pstep' = \x a s' s'' x0 -> pstep x a s' s'' __ __ __ x0} in
  list_rect (\_ _ s0 _ -> pempty s0 __) (\a l0 iHl pstep'0 _ s0 _ ->
    pstep'0 a (fold_right f i l0) (of_list0 l0) s0 __ __ __
      (iHl (\x a0 s' s'' _ _ _ x0 -> pstep'0 x a0 s' s'' __ __ __ x0) __
        (of_list0 l0) __)) l (\x a s' s'' _ _ _ x0 -> pstep' x a s' s'' x0)
    __ s __

fold_rec_bis0 :: (Elt4 -> a1 -> a1) -> a1 -> T12 -> (T12 -> T12 -> a1 -> ()
                 -> a2 -> a2) -> a2 -> (Elt4 -> a1 -> T12 -> () -> () -> a2
                 -> a2) -> a2
fold_rec_bis0 f i s pmorphism pempty pstep =
  fold_rec0 f i s (\s' _ -> pmorphism empty4 s' i __ pempty)
    (\x a s' s'' _ _ _ x0 ->
    pmorphism (add4 x s') s'' (f x a) __ (pstep x a s' __ __ x0))

fold_rec_nodep0 :: (Elt4 -> a1 -> a1) -> a1 -> T12 -> a2 -> (Elt4 -> a1 -> ()
                   -> a2 -> a2) -> a2
fold_rec_nodep0 f i s x x0 =
  fold_rec_bis0 f i s (\_ _ _ _ x1 -> x1) x (\x1 a _ _ _ x2 -> x0 x1 a __ x2)

fold_rec_weak0 :: (Elt4 -> a1 -> a1) -> a1 -> (T12 -> T12 -> a1 -> () -> a2
                  -> a2) -> a2 -> (Elt4 -> a1 -> T12 -> () -> a2 -> a2) ->
                  T12 -> a2
fold_rec_weak0 f i x x0 x1 s =
  fold_rec_bis0 f i s x x0 (\x2 a s' _ _ x3 -> x1 x2 a s' __ x3)

fold_rel0 :: (Elt4 -> a1 -> a1) -> (Elt4 -> a2 -> a2) -> a1 -> a2 -> T12 ->
             a3 -> (Elt4 -> a1 -> a2 -> () -> a3 -> a3) -> a3
fold_rel0 f g i j s rempty rstep =
  let {l = rev (elements4 s)} in
  let {rstep' = \x a b x0 -> rstep x a b __ x0} in
  list_rect (\_ -> rempty) (\a l0 iHl rstep'0 ->
    rstep'0 a (fold_right f i l0) (fold_right g j l0) __
      (iHl (\x a0 b _ x0 -> rstep'0 x a0 b __ x0))) l (\x a b _ x0 ->
    rstep' x a b x0)

set_induction0 :: (T12 -> () -> a1) -> (T12 -> T12 -> a1 -> Elt4 -> () -> ()
                  -> a1) -> T12 -> a1
set_induction0 x x0 s =
  fold_rec0 (\_ _ -> Tt) Tt s x (\x1 _ s' s'' _ _ _ x2 ->
    x0 s' s'' x2 x1 __ __)

set_induction_bis0 :: (T12 -> T12 -> () -> a1 -> a1) -> a1 -> (Elt4 -> T12 ->
                      () -> a1 -> a1) -> T12 -> a1
set_induction_bis0 x x0 x1 s =
  fold_rec_bis0 (\_ _ -> Tt) Tt s (\s0 s' _ _ x2 -> x s0 s' __ x2) x0
    (\x2 _ s' _ _ x3 -> x1 x2 s' __ x3)

cardinal_inv_0 :: T12 -> Nat -> Elt4
cardinal_inv_0 s _ =
  let {l = elements4 s} in case l of {
                            Nil -> false_rect;
                            Cons e _ -> e}

cardinal_inv_2b0 :: T12 -> Elt4
cardinal_inv_2b0 s =
  let {n = cardinal4 s} in
  let {x = \x -> cardinal_inv_0 s x} in
  case n of {
   O -> false_rect;
   S n0 -> x n0}

eqb8 :: Label -> Label -> Bool
eqb8 x y =
  case eq_dec7 x y of {
   Left -> True;
   Right -> False}

one0 :: a1 -> List a1
one0 item =
  Cons item Nil

dom0 :: (List (Prod Label a1)) -> T12
dom0 e =
  case e of {
   Nil -> empty4;
   Cons p e' -> case p of {
                 Pair x _ -> add4 x (dom0 e')}}

get0 :: Label -> (List (Prod Label a1)) -> Option a1
get0 x e =
  case e of {
   Nil -> None;
   Cons p f ->
    case p of {
     Pair y c ->
      case eq_dec0 eqDec_eq_of_X0 x y of {
       Left -> Some c;
       Right -> get0 x f}}}

map1 :: (a1 -> a2) -> (List (Prod Label a1)) -> List (Prod Label a2)
map1 f e =
  map (\b -> case b of {
              Pair x a -> Pair x (f a)}) e

alist_ind0 :: a2 -> (Label -> a1 -> (List (Prod Label a1)) -> a2 -> a2) ->
              (List (Prod Label a1)) -> a2
alist_ind0 x x0 xs =
  list_rect x (\a0 xs0 iHxs -> case a0 of {
                                Pair x1 a -> x0 x1 a xs0 iHxs}) xs

binds_dec0 :: Label -> a1 -> (List (Prod Label a1)) -> (a1 -> a1 -> Sumbool)
              -> Sumbool
binds_dec0 x a e x0 =
  in_dec (\x1 y ->
    case x1 of {
     Pair x2 x3 ->
      case y of {
       Pair t0 a1 ->
        sumbool_rect (\_ ->
          sumbool_rect (\_ -> Left) (\_ -> Right) (x0 x3 a1)) (\_ -> Right)
          (eq_dec7 x2 t0)}}) (Pair x a) e

binds_lookup0 :: Label -> (List (Prod Label a1)) -> Sum a1 ()
binds_lookup0 x e =
  alist_ind0 (Inr __) (\x1 a1 _ x0 ->
    case x0 of {
     Inl s -> Inl s;
     Inr _ ->
      let {s = eq_dec7 x x1} in case s of {
                                 Left -> Inl a1;
                                 Right -> Inr __}}) e

data Monad m =
   Build_Monad (() -> Any -> m) (() -> () -> m -> (Any -> m) -> m)

ret :: (Monad a1) -> a2 -> a1
ret monad x =
  case monad of {
   Build_Monad ret0 _ -> unsafeCoerce ret0 __ x}

bind :: (Monad a1) -> a1 -> (a2 -> a1) -> a1
bind monad x x0 =
  case monad of {
   Build_Monad _ bind0 -> unsafeCoerce bind0 __ __ x x0}

data PMonad m =
   Build_PMonad (() -> Any -> Any -> m) (() -> () -> Any -> m -> (Any -> m)
                                        -> m)

type MonP m x = Any

pbind :: (PMonad a1) -> (MonP a1 a3) -> a1 -> (a2 -> a1) -> a1
pbind pMonad pu x x0 =
  case pMonad of {
   Build_PMonad _ pbind0 -> unsafeCoerce pbind0 __ __ pu x x0}

pMonad_Monad :: (Monad a1) -> PMonad a1
pMonad_Monad m =
  Build_PMonad (\_ -> unsafeCoerce (\_ x -> ret m x)) (\_ _ ->
    unsafeCoerce (\_ c f -> bind m c f))

data Err0 =
   Err Any
 | Err_cons Err0 Any

data Result a =
   Res_ret a
 | Err_ret Err0

resultMonad :: Monad (Result Any)
resultMonad =
  Build_Monad (\_ x -> Res_ret x) (\_ _ retA bmap ->
    case retA of {
     Res_ret a ->
      let {r = bmap a} in
      case r of {
       Res_ret b -> Res_ret b;
       Err_ret e -> Err_ret (Err_cons e a)};
     Err_ret e -> Err_ret e})

eret :: a2 -> Result a1
eret e =
  Err_ret (Err (unsafeCoerce e))

get_witness :: Atom -> (List (Prod Atom a1)) -> a1 -> SigT
               (List (Prod Atom a1)) (List (Prod Atom a1))
get_witness v l r =
  list_rect (\_ _ -> false_rect) (\a l0 iHl r0 _ ->
    case a of {
     Pair a0 a1 ->
      let {s = eq_dec0 eqDec_eq_of_X v a0} in
      case s of {
       Left -> ExistT Nil l0;
       Right ->
        let {h = iHl r0 __} in
        case h of {
         ExistT h0 x -> ExistT (app (one0 (Pair a0 a1)) h0) x}}}) l r __

get_for_sure :: (List (Prod Atom a1)) -> Atom -> a1
get_for_sure l x =
  list_rect (\_ _ -> false_rect) (\a _ iHl x0 _ ->
    case a of {
     Pair a0 t0 ->
      let {s = eq_dec0 eqDec_eq_of_X x0 a0} in
      case s of {
       Left -> t0;
       Right -> iHl x0 __}}) l x __

data Avar =
   Avar_b Nat
 | Avar_f Atom

avar_rect :: (Nat -> a1) -> (Atom -> a1) -> Avar -> a1
avar_rect f f0 a =
  case a of {
   Avar_b x -> f x;
   Avar_f x -> f0 x}

avar_rec :: (Nat -> a1) -> (Atom -> a1) -> Avar -> a1
avar_rec =
  avar_rect

eqAvar :: EqDec_eq Avar
eqAvar x y =
  avar_rec (\n x0 ->
    case x0 of {
     Avar_b n0 -> sumbool_rect (\_ -> Left) (\_ -> Right) (eq_dec n n0);
     Avar_f _ -> Right}) (\a x0 ->
    case x0 of {
     Avar_b _ -> Right;
     Avar_f a0 -> sumbool_rect (\_ -> Left) (\_ -> Right) (eq_dec1 a a0)}) x
    y

fv_values :: (a1 -> T5) -> (List (Prod Atom a1)) -> T5
fv_values f l =
  fold_right (\b a -> union1 a (case b of {
                                 Pair _ t -> f t})) empty1 l

data Typ =
   Typ_top
 | Typ_bot
 | Typ_sel Avar
 | Typ_all Typ Typ
 | Typ_bnd Typ Typ

typ_rect :: a1 -> a1 -> (Avar -> a1) -> (Typ -> a1 -> Typ -> a1 -> a1) ->
            (Typ -> a1 -> Typ -> a1 -> a1) -> Typ -> a1
typ_rect f f0 f1 f2 f3 t =
  case t of {
   Typ_top -> f;
   Typ_bot -> f0;
   Typ_sel a -> f1 a;
   Typ_all t0 t1 ->
    f2 t0 (typ_rect f f0 f1 f2 f3 t0) t1 (typ_rect f f0 f1 f2 f3 t1);
   Typ_bnd t0 t1 ->
    f3 t0 (typ_rect f f0 f1 f2 f3 t0) t1 (typ_rect f f0 f1 f2 f3 t1)}

typ_rec :: a1 -> a1 -> (Avar -> a1) -> (Typ -> a1 -> Typ -> a1 -> a1) -> (Typ
           -> a1 -> Typ -> a1 -> a1) -> Typ -> a1
typ_rec =
  typ_rect

eqDecTyp :: EqDec_eq Typ
eqDecTyp x y =
  typ_rec (\x0 -> case x0 of {
                   Typ_top -> Left;
                   _ -> Right}) (\x0 ->
    case x0 of {
     Typ_bot -> Left;
     _ -> Right}) (\a x0 ->
    case x0 of {
     Typ_sel a0 ->
      sumbool_rect (\_ -> Left) (\_ -> Right) (eq_dec0 eqAvar a a0);
     _ -> Right}) (\_ h _ h0 x0 ->
    case x0 of {
     Typ_all t1 t2 ->
      sumbool_rect (\_ -> sumbool_rect (\_ -> Left) (\_ -> Right) (h0 t2))
        (\_ -> Right) (h t1);
     _ -> Right}) (\_ h _ h0 x0 ->
    case x0 of {
     Typ_bnd t1 t2 ->
      sumbool_rect (\_ -> sumbool_rect (\_ -> Left) (\_ -> Right) (h0 t2))
        (\_ -> Right) (h t1);
     _ -> Right}) x y

data Trm =
   Trm_var Avar
 | Trm_val Val
 | Trm_app Avar Avar
 | Trm_let Trm Trm
data Val =
   Val_bnd Typ
 | Val_lam Typ Trm

open_rec_avar :: Nat -> Atom -> Avar -> Avar
open_rec_avar k u a =
  case a of {
   Avar_b i ->
    case eq_dec0 (eqDec_eq_of_EqDec nat_eq_eqdec) k i of {
     Left -> Avar_f u;
     Right -> Avar_b i};
   Avar_f x -> Avar_f x}

open_rec_typ :: Nat -> Atom -> Typ -> Typ
open_rec_typ k u t =
  case t of {
   Typ_sel x -> Typ_sel (open_rec_avar k u x);
   Typ_all t0 u0 -> Typ_all (open_rec_typ k u t0) (open_rec_typ (S k) u u0);
   Typ_bnd t0 u0 -> Typ_bnd (open_rec_typ k u t0) (open_rec_typ k u u0);
   x -> x}

open_rec_trm :: Nat -> Atom -> Trm -> Trm
open_rec_trm k u t =
  case t of {
   Trm_var x -> Trm_var (open_rec_avar k u x);
   Trm_val v -> Trm_val (open_rec_val k u v);
   Trm_app x y -> Trm_app (open_rec_avar k u x) (open_rec_avar k u y);
   Trm_let s t0 -> Trm_let (open_rec_trm k u s) (open_rec_trm (S k) u t0)}

open_rec_val :: Nat -> Atom -> Val -> Val
open_rec_val k u vl =
  case vl of {
   Val_bnd t -> Val_bnd (open_rec_typ k u t);
   Val_lam t t0 -> Val_lam (open_rec_typ k u t) (open_rec_trm (S k) u t0)}

openTyp :: CanOpen Typ
openTyp =
  open_rec_typ

openTrm :: CanOpen Trm
openTrm =
  open_rec_trm

close_rec_avar :: Nat -> Atom -> Avar -> Avar
close_rec_avar k u a =
  case a of {
   Avar_b n -> Avar_b n;
   Avar_f x ->
    case eq_dec0 eqDec_eq_of_X x u of {
     Left -> Avar_b k;
     Right -> Avar_f x}}

close_rec_typ :: Nat -> Atom -> Typ -> Typ
close_rec_typ k u t =
  case t of {
   Typ_sel x -> Typ_sel (close_rec_avar k u x);
   Typ_all t0 u0 -> Typ_all (close_rec_typ k u t0) (close_rec_typ (S k) u u0);
   Typ_bnd t0 u0 -> Typ_bnd (close_rec_typ k u t0) (close_rec_typ k u u0);
   x -> x}

closeTyp :: CanClose Typ
closeTyp =
  close_rec_typ

fv_avar :: Avar -> T5
fv_avar v =
  case v of {
   Avar_b _ -> empty1;
   Avar_f x -> singleton1 x}

fv_typ :: Typ -> T5
fv_typ t =
  case t of {
   Typ_sel a -> fv_avar a;
   Typ_all t0 t1 -> union1 (fv_typ t0) (fv_typ t1);
   Typ_bnd t0 t1 -> union1 (fv_typ t0) (fv_typ t1);
   _ -> empty1}

fv_trm :: Trm -> T5
fv_trm t =
  case t of {
   Trm_var a -> fv_avar a;
   Trm_val v -> fv_val v;
   Trm_app x y -> union1 (fv_avar x) (fv_avar y);
   Trm_let t0 t1 -> union1 (fv_trm t0) (fv_trm t1)}

fv_val :: Val -> T5
fv_val vl =
  case vl of {
   Val_bnd t -> fv_typ t;
   Val_lam t t0 -> union1 (fv_typ t) (fv_trm t0)}

data ExpoStuck =
   Stuck_err Atom Typ

exposure_impl :: (List (Prod Atom Typ)) -> Typ -> Result
                 (SigT Typ
                 (SigT (List (Prod Atom Typ)) (List (Prod Atom Typ))))
exposure_impl g s =
  let {
   fix_F_sub x =
     let {g0 = projT1 x} in
     let {s0 = projT2 x} in
     let {exposure_impl0 = \g1 s1 -> fix_F_sub (ExistT g1 s1)} in
     (\_ _ _ ->
     case s0 of {
      Typ_sel a ->
       case a of {
        Avar_b _ -> false_rect;
        Avar_f a0 ->
         let {s1 = get_for_sure g0 a0} in
         let {h2 = get_witness a0 g0 s1} in
         case h2 of {
          ExistT h3 x0 ->
           let {recur = exposure_impl0 x0 s1 __ __ __} in
           pbind (pMonad_Monad (unsafeCoerce resultMonad)) __ recur (\x1 ->
             case x1 of {
              ExistT x2 x3 ->
               case x3 of {
                ExistT x4 x5 ->
                 case x2 of {
                  Typ_bot -> Res_ret (ExistT Typ_bot (ExistT h3
                   (app (one (Pair a0 s1)) (app x4 x5))));
                  Typ_bnd _ t2 ->
                   let {recur0 = exposure_impl0 x0 t2 __ __ __} in
                   pbind (pMonad_Monad (unsafeCoerce resultMonad)) __ recur0
                     (\x6 ->
                     case x6 of {
                      ExistT x7 _ -> Res_ret (ExistT x7 (ExistT h3 (Cons
                       (Pair a0 s1) (app x4 x5))))});
                  x6 -> eret (Stuck_err a0 x6)}}})}};
      x0 -> Res_ret (ExistT x0 (ExistT Nil g0))})}
  in fix_F_sub (ExistT g s) __ __ __

promo_demo_func_obligation_1 :: (List (Prod Atom Typ)) -> Atom -> Typ ->
                                ((List (Prod Atom Typ)) -> Atom -> Typ -> ()
                                -> () -> () -> () -> Prod (Result Typ)
                                (Result Typ)) -> Prod (Result Typ)
                                (Result Typ)
promo_demo_func_obligation_1 g x s promo_demo0 =
  case s of {
   Typ_sel a ->
    case a of {
     Avar_b _ -> false_rect;
     Avar_f a0 ->
      let {s0 = eq_dec0 eqDec_eq_of_X x a0} in
      case s0 of {
       Left ->
        let {s1 = get_for_sure g a0} in
        let {h2 = get_witness a0 g s1} in
        case h2 of {
         ExistT _ x0 ->
          let {recur = exposure_impl x0 s1} in
          Pair
          (pbind (pMonad_Monad (unsafeCoerce resultMonad)) __
            (unsafeCoerce recur) (\x1 ->
            case x1 of {
             ExistT x2 _ ->
              case x2 of {
               Typ_bot -> Res_ret Typ_bot;
               Typ_bnd _ t2 -> Res_ret t2;
               x3 -> eret (Stuck_err a0 x3)}}))
          (pbind (pMonad_Monad (unsafeCoerce resultMonad)) __
            (unsafeCoerce recur) (\x1 ->
            case x1 of {
             ExistT x2 _ ->
              case x2 of {
               Typ_bot -> Res_ret Typ_top;
               Typ_bnd t1 _ -> Res_ret t1;
               x3 -> eret (Stuck_err a0 x3)}}))};
       Right -> Pair (Res_ret (Typ_sel (Avar_f a0))) (Res_ret (Typ_sel
        (Avar_f a0)))}};
   Typ_all s1 s2 ->
    let {recur = promo_demo0 g x s1 __ __ __ __} in
    case recur of {
     Pair r r0 -> Pair
      (pbind (pMonad_Monad (unsafeCoerce resultMonad)) __ r0 (\x0 ->
        let {
         s0 = atom_fresh
                (union1 (singleton1 x)
                  (union1 (dom g)
                    (union1 (fv_values fv_typ g)
                      (union1 (fv_typ s1) (union1 (fv_typ s2) (fv_typ x0))))))}
        in
        let {
         x1 = promo_demo0 (app (one (Pair s0 x0)) g) x
                (open_rec openTyp O s0 s2) __ __ __ __}
        in
        case x1 of {
         Pair r1 _ ->
          pbind (pMonad_Monad (unsafeCoerce resultMonad)) __ r1 (\x2 ->
            Res_ret (Typ_all x0 (close_rec closeTyp O s0 x2)))}))
      (pbind (pMonad_Monad (unsafeCoerce resultMonad)) __ r (\x0 ->
        let {
         s0 = atom_fresh
                (union1 (singleton1 x)
                  (union1 (dom g)
                    (union1 (fv_values fv_typ g)
                      (union1 (fv_typ s1) (union1 (fv_typ s2) (fv_typ x0))))))}
        in
        let {
         x1 = promo_demo0 (app (one (Pair s0 s1)) g) x
                (open_rec openTyp O s0 s2) __ __ __ __}
        in
        case x1 of {
         Pair _ r2 ->
          pbind (pMonad_Monad (unsafeCoerce resultMonad)) __ r2 (\x2 ->
            Res_ret (Typ_all x0 (close_rec closeTyp O s0 x2)))}))};
   Typ_bnd s1 s2 ->
    let {x0 = promo_demo0 g x s2 __ __ __ __} in
    let {x1 = promo_demo0 g x s1 __ __ __ __} in
    case x1 of {
     Pair r r0 ->
      case x0 of {
       Pair r1 r2 -> Pair
        (pbind (pMonad_Monad (unsafeCoerce resultMonad)) __ r1 (\x2 ->
          pbind (pMonad_Monad (unsafeCoerce resultMonad)) __ r0 (\y ->
            Res_ret (Typ_bnd y x2))))
        (pbind (pMonad_Monad (unsafeCoerce resultMonad)) __ r2 (\x2 ->
          pbind (pMonad_Monad (unsafeCoerce resultMonad)) __ r (\y -> Res_ret
            (Typ_bnd y x2))))}};
   x0 -> Pair (Res_ret x0) (Res_ret x0)}

promo_demo_func :: (SigT (List (Prod Atom Typ)) (SigT Atom Typ)) -> Prod
                   (Result Typ) (Result Typ)
promo_demo_func recarg =
  let {
   fix_F_sub x =
     let {g = projT1 x} in
     let {x0 = projT1 (projT2 x)} in
     let {s = projT2 (projT2 x)} in
     let {promo_demo0 = \g0 x1 s0 -> fix_F_sub (ExistT g0 (ExistT x1 s0))} in
     (\_ _ _ ->
     promo_demo_func_obligation_1 g x0 s (\g0 x1 s0 _ ->
       promo_demo0 g0 x1 s0))}
  in fix_F_sub recarg __ __ __

promo_demo :: (List (Prod Atom Typ)) -> Atom -> Typ -> Prod (Result Typ)
              (Result Typ)
promo_demo g x s =
  promo_demo_func (ExistT g (ExistT x s))

promotion :: (List (Prod Atom Typ)) -> Atom -> Typ -> Result Typ
promotion g x s =
  let {p = promo_demo g x s} in case p of {
                                 Pair r _ -> r}

data SubtypUndec =
   St_undec Typ Typ

data FuncInputTypeMismatch =
   St_all_mis Typ Typ

subty_dec :: (List (Prod Atom Typ)) -> Typ -> Typ -> Result ()
subty_dec g t u =
  let {
   fix_F_sub x =
     let {g0 = projT1 x} in
     let {t0 = projT1 (projT2 x)} in
     let {u0 = projT2 (projT2 x)} in
     let {subty_dec0 = \g1 t1 u1 -> fix_F_sub (ExistT g1 (ExistT t1 u1))} in
     (\_ _ _ _ _ ->
     case t0 of {
      Typ_top ->
       case u0 of {
        Typ_top -> Res_ret __;
        x0 -> eret (St_undec Typ_top x0)};
      Typ_bot -> Res_ret __;
      Typ_sel a ->
       case u0 of {
        Typ_top -> Res_ret __;
        Typ_bot -> eret (St_undec (Typ_sel a) Typ_bot);
        Typ_sel a0 ->
         case a0 of {
          Avar_b _ -> false_rect;
          Avar_f a1 ->
           case a of {
            Avar_b _ -> false_rect;
            Avar_f a2 ->
             let {s = eq_dec0 eqDec_eq_of_X a2 a1} in
             case s of {
              Left -> Res_ret __;
              Right ->
               let {s0 = get_for_sure g0 a2} in
               let {h6 = get_witness a2 g0 s0} in
               case h6 of {
                ExistT _ x0 ->
                 let {h12 = exposure_impl x0 s0} in
                 pbind (pMonad_Monad (unsafeCoerce resultMonad)) __
                   (unsafeCoerce h12) (\x1 ->
                   case x1 of {
                    ExistT x2 _ ->
                     case x2 of {
                      Typ_bot -> Res_ret __;
                      Typ_bnd _ t2 ->
                       let {
                        x3 = subty_dec0 g0 t2 (Typ_sel (Avar_f a1)) __ __ __
                               __ __}
                       in
                       pbind (pMonad_Monad (unsafeCoerce resultMonad)) __ x3
                         (\_ -> Res_ret __);
                      x3 -> eret (Stuck_err a2 x3)}})}}}};
        Typ_all t1 t2 ->
         case a of {
          Avar_b _ -> false_rect;
          Avar_f a0 ->
           let {s = get_for_sure g0 a0} in
           let {h6 = get_witness a0 g0 s} in
           case h6 of {
            ExistT _ x0 ->
             let {h12 = exposure_impl x0 s} in
             pbind (pMonad_Monad (unsafeCoerce resultMonad)) __
               (unsafeCoerce h12) (\x1 ->
               case x1 of {
                ExistT x2 _ ->
                 case x2 of {
                  Typ_bot -> Res_ret __;
                  Typ_bnd _ t4 ->
                   let {x3 = subty_dec0 g0 t4 (Typ_all t1 t2) __ __ __ __ __}
                   in
                   pbind (pMonad_Monad (unsafeCoerce resultMonad)) __ x3
                     (\_ -> Res_ret __);
                  x3 -> eret (Stuck_err a0 x3)}})}};
        Typ_bnd t1 t2 ->
         case a of {
          Avar_b _ -> false_rect;
          Avar_f a0 ->
           let {s = get_for_sure g0 a0} in
           let {h6 = get_witness a0 g0 s} in
           case h6 of {
            ExistT _ x0 ->
             let {h12 = exposure_impl x0 s} in
             pbind (pMonad_Monad (unsafeCoerce resultMonad)) __
               (unsafeCoerce h12) (\x1 ->
               case x1 of {
                ExistT x2 _ ->
                 case x2 of {
                  Typ_bot -> Res_ret __;
                  Typ_bnd _ t4 ->
                   let {x3 = subty_dec0 g0 t4 (Typ_bnd t1 t2) __ __ __ __ __}
                   in
                   pbind (pMonad_Monad (unsafeCoerce resultMonad)) __ x3
                     (\_ -> Res_ret __);
                  x3 -> eret (Stuck_err a0 x3)}})}}};
      Typ_all t1 t2 ->
       case u0 of {
        Typ_top -> Res_ret __;
        Typ_sel a ->
         case a of {
          Avar_b _ -> false_rect;
          Avar_f a0 ->
           let {s = get_for_sure g0 a0} in
           let {h6 = get_witness a0 g0 s} in
           case h6 of {
            ExistT _ x0 ->
             let {h12 = exposure_impl x0 s} in
             pbind (pMonad_Monad (unsafeCoerce resultMonad)) __
               (unsafeCoerce h12) (\x1 ->
               case x1 of {
                ExistT x2 _ ->
                 case x2 of {
                  Typ_bot -> Res_ret __;
                  Typ_bnd t3 _ ->
                   let {x3 = subty_dec0 g0 (Typ_all t1 t2) t3 __ __ __ __ __}
                   in
                   pbind (pMonad_Monad (unsafeCoerce resultMonad)) __ x3
                     (\_ -> Res_ret __);
                  x3 -> eret (Stuck_err a0 x3)}})}};
        Typ_all t3 t4 ->
         let {s = eq_dec0 eqDecTyp t1 t3} in
         case s of {
          Left ->
           let {
            s0 = atom_fresh
                   (union1 (dom g0)
                     (union1 (fv_values fv_typ g0)
                       (union1 (fv_typ t2) (union1 (fv_typ t3) (fv_typ t4)))))}
           in
           let {
            x0 = subty_dec0 (app (one (Pair s0 t3)) g0)
                   (open_rec openTyp O s0 t2) (open_rec openTyp O s0 t4) __
                   __ __ __ __}
           in
           pbind (pMonad_Monad (unsafeCoerce resultMonad)) __ x0 (\_ ->
             Res_ret __);
          Right -> eret (St_all_mis t1 t3)};
        x0 -> eret (St_undec (Typ_all t1 t2) x0)};
      Typ_bnd t1 t2 ->
       case u0 of {
        Typ_top -> Res_ret __;
        Typ_sel a ->
         case a of {
          Avar_b _ -> false_rect;
          Avar_f a0 ->
           let {s = get_for_sure g0 a0} in
           let {h6 = get_witness a0 g0 s} in
           case h6 of {
            ExistT _ x0 ->
             let {h12 = exposure_impl x0 s} in
             pbind (pMonad_Monad (unsafeCoerce resultMonad)) __
               (unsafeCoerce h12) (\x1 ->
               case x1 of {
                ExistT x2 _ ->
                 case x2 of {
                  Typ_bot -> Res_ret __;
                  Typ_bnd t3 _ ->
                   let {x3 = subty_dec0 g0 (Typ_bnd t1 t2) t3 __ __ __ __ __}
                   in
                   pbind (pMonad_Monad (unsafeCoerce resultMonad)) __ x3
                     (\_ -> Res_ret __);
                  x3 -> eret (Stuck_err a0 x3)}})}};
        Typ_bnd t3 t4 ->
         let {x0 = subty_dec0 g0 t3 t1 __ __ __ __ __} in
         let {x1 = subty_dec0 g0 t2 t4 __ __ __ __ __} in
         pbind (pMonad_Monad (unsafeCoerce resultMonad)) __ x0 (\_ ->
           pbind (pMonad_Monad (unsafeCoerce resultMonad)) __ x1 (\_ ->
             Res_ret __));
        x0 -> eret (St_undec (Typ_bnd t1 t2) x0)}})}
  in fix_F_sub (ExistT g (ExistT t u)) __ __ __ __ __

data NotBoundToFuncTypes =
   Ty_nfunc Atom Typ

typing_impl :: (List (Prod Atom Typ)) -> Trm -> Result Typ
typing_impl g t =
  let {
   fix_F_sub x =
     let {g0 = projT1 x} in
     let {t0 = projT2 x} in
     let {typing_impl0 = \g1 t1 -> fix_F_sub (ExistT g1 t1)} in
     (\_ _ _ ->
     case t0 of {
      Trm_var a ->
       case a of {
        Avar_b _ -> false_rect;
        Avar_f a0 -> let {s = get_for_sure g0 a0} in Res_ret s};
      Trm_val v ->
       case v of {
        Val_bnd t1 -> Res_ret (Typ_bnd t1 t1);
        Val_lam t1 t2 ->
         let {
          s = atom_fresh
                (union1 (dom g0)
                  (union1 (fv_values fv_typ g0)
                    (union1 (fv_typ t1) (fv_trm t2))))}
         in
         let {
          x0 = typing_impl0 (app (one (Pair s t1)) g0)
                 (open_rec openTrm O s t2) __ __ __}
         in
         pbind (pMonad_Monad (unsafeCoerce resultMonad)) __ x0 (\x1 ->
           Res_ret (Typ_all t1 (close_rec closeTyp O s x1)))};
      Trm_app a a0 ->
       case a of {
        Avar_b _ -> false_rect;
        Avar_f a1 ->
         case a0 of {
          Avar_b _ -> false_rect;
          Avar_f a2 ->
           let {s = get_for_sure g0 a1} in
           let {h4 = get_witness a1 g0 s} in
           case h4 of {
            ExistT _ x0 ->
             let {h10 = exposure_impl x0 s} in
             pbind (pMonad_Monad (unsafeCoerce resultMonad)) __
               (unsafeCoerce h10) (\x1 ->
               case x1 of {
                ExistT x2 _ ->
                 let {s0 = get_for_sure g0 a2} in
                 case x2 of {
                  Typ_bot -> Res_ret (open_rec openTyp O a2 Typ_bot);
                  Typ_all t1 t2 ->
                   let {x3 = subty_dec g0 s0 t1} in
                   pbind (pMonad_Monad (unsafeCoerce resultMonad)) __
                     (unsafeCoerce x3) (\_ -> Res_ret
                     (open_rec openTyp O a2 t2));
                  x3 -> eret (Ty_nfunc a1 x3)}})}}};
      Trm_let t1 t2 ->
       let {x0 = typing_impl0 g0 t1 __ __ __} in
       pbind (pMonad_Monad (unsafeCoerce resultMonad)) __ x0 (\x1 ->
         let {
          s = atom_fresh
                (union1 (dom g0)
                  (union1 (fv_values fv_typ g0)
                    (union1 (fv_typ x1) (union1 (fv_trm t1) (fv_trm t2)))))}
         in
         let {
          x2 = typing_impl0 (app (one (Pair s x1)) g0)
                 (open_rec openTrm O s t2) __ __ __}
         in
         pbind (pMonad_Monad (unsafeCoerce resultMonad)) __ x2 (\x3 ->
           let {x4 = promotion (app (one (Pair s x1)) g0) s x3} in
           pbind (pMonad_Monad (unsafeCoerce resultMonad)) __ x4 (\x5 ->
             Res_ret x5)))})}
  in fix_F_sub (ExistT g t) __ __ __

typecheck' :: Trm -> Result Typ
typecheck' t =
  typing_impl Nil t

data HasFreeVars =
   Has_fv_typ Atom Typ
 | Has_fv_trm Atom Trm
 | Has_fv_val Atom Val

check_fv_typ :: Typ -> Result ()
check_fv_typ t =
  case t of {
   Typ_sel a ->
    case a of {
     Avar_b _ -> Res_ret __;
     Avar_f a0 -> eret (Has_fv_typ a0 (Typ_sel (Avar_f a0)))};
   Typ_all t1 t2 ->
    let {x = check_fv_typ t1} in
    pbind (pMonad_Monad (unsafeCoerce resultMonad)) __ x (\_ ->
      let {x0 = check_fv_typ t2} in
      pbind (pMonad_Monad (unsafeCoerce resultMonad)) __ x0 (\_ -> Res_ret
        __));
   Typ_bnd t1 t2 ->
    let {x = check_fv_typ t1} in
    pbind (pMonad_Monad (unsafeCoerce resultMonad)) __ x (\_ ->
      let {x0 = check_fv_typ t2} in
      pbind (pMonad_Monad (unsafeCoerce resultMonad)) __ x0 (\_ -> Res_ret
        __));
   _ -> Res_ret __}

check_fv_trm :: Trm -> Result ()
check_fv_trm t =
  case t of {
   Trm_var a ->
    case a of {
     Avar_b _ -> Res_ret __;
     Avar_f a0 -> eret (Has_fv_trm a0 (Trm_var (Avar_f a0)))};
   Trm_val v ->
    let {x = check_fv_val v} in
    pbind (pMonad_Monad (unsafeCoerce resultMonad)) __ x (\_ -> Res_ret __);
   Trm_app a a0 ->
    case a of {
     Avar_b n ->
      case a0 of {
       Avar_b _ -> Res_ret __;
       Avar_f a1 -> eret (Has_fv_trm a1 (Trm_app (Avar_b n) (Avar_f a1)))};
     Avar_f a1 ->
      case a0 of {
       Avar_b n -> eret (Has_fv_trm a1 (Trm_app (Avar_f a1) (Avar_b n)));
       Avar_f a2 -> eret (Has_fv_trm a2 (Trm_app (Avar_f a1) (Avar_f a2)))}};
   Trm_let t0_1 t0_2 ->
    let {x = check_fv_trm t0_1} in
    pbind (pMonad_Monad (unsafeCoerce resultMonad)) __ x (\_ ->
      let {x0 = check_fv_trm t0_2} in
      pbind (pMonad_Monad (unsafeCoerce resultMonad)) __ x0 (\_ -> Res_ret
        __))}

check_fv_val :: Val -> Result ()
check_fv_val vl =
  case vl of {
   Val_bnd t ->
    let {x = check_fv_typ t} in
    pbind (pMonad_Monad (unsafeCoerce resultMonad)) __ x (\_ -> Res_ret __);
   Val_lam t t0 ->
    let {x = check_fv_typ t} in
    pbind (pMonad_Monad (unsafeCoerce resultMonad)) __ x (\_ ->
      let {x0 = check_fv_trm t0} in
      pbind (pMonad_Monad (unsafeCoerce resultMonad)) __ x0 (\_ -> Res_ret
        __))}

data NotClosed =
   Nc_at_typ Nat Typ Nat
 | Nc_at_trm Nat Trm Nat
 | Nc_at_val Nat Val Nat

check_lc_at_typ :: Nat -> Typ -> Result ()
check_lc_at_typ n t =
  case t of {
   Typ_sel a ->
    case a of {
     Avar_b n0 ->
      let {s = le_lt_dec (S n0) n} in
      case s of {
       Left -> Res_ret __;
       Right -> eret (Nc_at_typ n (Typ_sel (Avar_b n0)) (S n0))};
     Avar_f _ -> Res_ret __};
   Typ_all t1 t2 ->
    let {x = check_lc_at_typ n t1} in
    pbind (pMonad_Monad (unsafeCoerce resultMonad)) __ x (\_ ->
      let {x0 = check_lc_at_typ (S n) t2} in
      pbind (pMonad_Monad (unsafeCoerce resultMonad)) __ x0 (\_ -> Res_ret
        __));
   Typ_bnd t1 t2 ->
    let {x = check_lc_at_typ n t1} in
    pbind (pMonad_Monad (unsafeCoerce resultMonad)) __ x (\_ ->
      let {x0 = check_lc_at_typ n t2} in
      pbind (pMonad_Monad (unsafeCoerce resultMonad)) __ x0 (\_ -> Res_ret
        __));
   _ -> Res_ret __}

check_lc_at_trm :: Nat -> Trm -> Result ()
check_lc_at_trm n t =
  case t of {
   Trm_var a ->
    case a of {
     Avar_b n0 ->
      let {s = le_lt_dec (S n0) n} in
      case s of {
       Left -> Res_ret __;
       Right -> eret (Nc_at_trm n (Trm_var (Avar_b n0)) (S n0))};
     Avar_f _ -> Res_ret __};
   Trm_val v ->
    let {x = check_lc_at_val n v} in
    pbind (pMonad_Monad (unsafeCoerce resultMonad)) __ x (\_ -> Res_ret __);
   Trm_app a a0 ->
    case a of {
     Avar_b n0 ->
      case a0 of {
       Avar_b n1 ->
        let {s = le_lt_dec (S n1) n} in
        case s of {
         Left ->
          let {s0 = le_lt_dec (S n0) n} in
          case s0 of {
           Left -> Res_ret __;
           Right ->
            eret (Nc_at_trm n (Trm_app (Avar_b n0) (Avar_b n1)) (S n0))};
         Right ->
          let {s0 = le_lt_dec (S n0) n} in
          case s0 of {
           Left ->
            eret (Nc_at_trm n (Trm_app (Avar_b n0) (Avar_b n1)) (S n1));
           Right ->
            eret (Nc_at_trm n (Trm_app (Avar_b n0) (Avar_b n1))
              (max (S n0) (S n1)))}};
       Avar_f a1 ->
        let {s = le_lt_dec (S n0) n} in
        case s of {
         Left -> Res_ret __;
         Right -> eret (Nc_at_trm n (Trm_app (Avar_b n0) (Avar_f a1)) (S n0))}};
     Avar_f a1 ->
      case a0 of {
       Avar_b n0 ->
        let {s = le_lt_dec (S n0) n} in
        case s of {
         Left -> Res_ret __;
         Right -> eret (Nc_at_trm n (Trm_app (Avar_f a1) (Avar_b n0)) (S n0))};
       Avar_f _ -> Res_ret __}};
   Trm_let t0_1 t0_2 ->
    let {x = check_lc_at_trm n t0_1} in
    pbind (pMonad_Monad (unsafeCoerce resultMonad)) __ x (\_ ->
      let {x0 = check_lc_at_trm (S n) t0_2} in
      pbind (pMonad_Monad (unsafeCoerce resultMonad)) __ x0 (\_ -> Res_ret
        __))}

check_lc_at_val :: Nat -> Val -> Result ()
check_lc_at_val n vl =
  case vl of {
   Val_bnd t ->
    let {x = check_lc_at_typ n t} in
    pbind (pMonad_Monad (unsafeCoerce resultMonad)) __ x (\_ -> Res_ret __);
   Val_lam t t0 ->
    let {x = check_lc_at_typ n t} in
    pbind (pMonad_Monad (unsafeCoerce resultMonad)) __ x (\_ ->
      let {x0 = check_lc_at_trm (S n) t0} in
      pbind (pMonad_Monad (unsafeCoerce resultMonad)) __ x0 (\_ -> Res_ret
        __))}

typecheck :: Trm -> Result Typ
typecheck t =
  pbind (pMonad_Monad (unsafeCoerce resultMonad)) __
    (unsafeCoerce check_fv_trm t) (\_ ->
    pbind (pMonad_Monad (unsafeCoerce resultMonad)) __
      (unsafeCoerce check_lc_at_trm O t) (\_ -> typecheck' t))

