## Algorithmic Analysis of D<:

This directory focuses on algorithmic analysis of D<:. It defines:

1. The declarative D<:,
1. Kernel D<:,
1. Strong kernel D<:, 
1. Step subtyping, and
1. Stare-at subtyping.

The naming convention in this directory is locally nameless representation with
cofinite quantification.

### Sparse notes on formalization

* Some conclusions are results of equivalences of intermediate languages which are not
  mentioned in the thesis.
* Step subtyping and Stare-at subtyping are modelled as program traces. In addition to
  every input/output, the relations also rely on a name store, very often represented
  by `L`, which means when a free name is drawn from a supply, the name must not be in
  `L`. This is a realistic treatment when free names are necessary in a program trace.
* There are theorems in the thesis which require to count the steps of derivation. In
  this case, separate relations are defined. Each of such relations copies its
  corresponding original relation, and additionally has a step counter, usually
  represented by `n`. Then this relation is shown to be equivalent to the original
  one. That is, if the original relation is `R ⊆ X × Y`, then define `R′ ⊆ X × Y × ℕ`,
  such that
  ```
  (x, y) ∈ R ⇔ ∃ n, (x, y, n) ∈ R′
  ```

### Definitions

| Definition | File                  | Correspondences                         | Comments                          |
|------------|-----------------------|-----------------------------------------|-----------------------------------|
| 3.1        | Definitions.v         | `typing` and `subty`                    | Definition of the declarative D<: |
| 4.1        | Kernel.v              | `subtykn`                               |                                   |
| 4.2        | Exposure.v            | `stp_subty`                             |                                   |
| 4.3        | Exspoure.v            | `exposure`, `upcast_e` and `downcast_e` |                                   |
| 4.6        | StrongKernel.v        | `subtysk`                               |                                   |
| 4.8        | Exposure.v            | `wf_search_impl`                        |                                   |
| 4.9        | Comm.v                | `bi_subty`                              |                                   |
| 4.10       | Comm.v                | `revealing`, `upcast`, `downcast`       |                                   |
| 4.11       | share/ListRelations.v | `prefix`                                |                                   |
| 4.12       | Misc.v                | `typ_struct_measure`                    |                                   |

### Conclusions

| Conclusion         | File           | Correspondences                                                                                    |
|--------------------|----------------|----------------------------------------------------------------------------------------------------|
| Lemma 4.3          | Exposure.v     | `stp_subty_sound`                                                                                  |
| Lemma 4.4          | Exposure.v     | `stp_subty_terminates`                                                                             |
| Lemma 4.6          | Exposure.v     | `exposure_sound`                                                                                   |
| Lemma 4.12         | Kernel.v       | `subtykn_refl`                                                                                     |
| Lemma 4.13, 4.14   | Kernel.v       | `trans_on_top`                                                                                     |
| Lemma 4.15         | Kernel.v       | `trans_on_bot`                                                                                     |
| Lemma 4.16         | Kernel.v       | `exposure'_to_subtykn`                                                                             |
| Theorem 4.17       | Kernel.v       | `stp_subty_to_subtykn`                                                                             |
| Theorem 4.18       | Kernel.v       | `subtykn_to_stp_subty`, `subtykn_to_stp_subty'`, `stp_subty'_to_stp_subty`, `subtykn'_conversions` |
| Lemma 4.19         | StrongKernel.v | `subtysk_refl`                                                                                     |
| Theorem 4.20       | StrongKernel.v | `subtykn_to_subtysk`                                                                               |
| Theorem 4.21, 4.25 | StrongKernel.v | `subtysk_sound`                                                                                    |
| Lemma 4.22         | OpeSub.v       | `ope_sub_refl`                                                                                     |
| Lemma 4.23         | OpeSub.v       | `ope_sub_trans`                                                                                    |
| Theorem 4.24       | OpeSub.v       | `ope_narrow_subty`                                                                                 |
| Theorem 4.26       | Comm.v         | `bi_subty_refl`                                                                                    |
| Lemma 4.27         | Exposure.v     | `revealing_gives_prefix`                                                                           |
| Lemma 4.28, 4.29   | Comm.v         | `revealing_sound`                                                                                  |
| Lemma 4.30         | Exposure.v     | `prefix_wf_env`                                                                                    |
| Lemma 4.31         | Comm.v         | `revealing_preserves_wf`                                                                           |
| Lemma 4.32         | Comm.v         | `upcast_sound`, `upcast_preserves_wf`, `downcast_sound`, `downcast_preserves_wf`                   |
|                    | Expoure.v      | `upcast_gives_prefix`, `downcast_gives_prefix`                                                     |
| Theorem 4.33       | Comm.v         | `ope_sub_bi_subty_sound`                                                                           |
| Theorem 4.34       | Comm.v         | `bsubtyp_sound`                                                                                    |
| Lemma 4.35         | Comm.v         | `revealing_terminates`                                                                             |
| Lemma 4.36         | Comm.v         | `revealing_measure`, `upcast_decreases_measure`, `downcast_decreases_measure`                      |
| Theorem 4.37       | Comm.v         | `bi_subty_terminates`                                                                              |
| Lemma 4.38         | StrongKernel.v | `sk_trans_on_top`                                                                                  |
| Lemma 4.39         | StrongKernel.v | `sk_trans_on_bot`                                                                                  |
| Lemma 4.40         | StrongKernel.v | `revealing_to_subtysk`, `exposure'_to_subtysk`                                                     |
| Lemma 4.41         | StrongKernel.v | `bi_subty_to_subtysk`                                                                              |
| Theorem 4.42       | Exposure.v     | `bi_subty_strengthening`                                                                           |
| Theorem 4.43       | StrongKernel.v | `subtysk_to_bi_subty`, `subtysk_to_bi_subty'`, `bi_subty'_to_bi_subty`, `subtysk'_conversions`     |


