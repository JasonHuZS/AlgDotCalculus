Set Implicit Arguments.
Require Import Definitions.
Require Import OperationProperties.
Require Import StructuralProperties.
Require Import Misc.

Section MeasuresAndFacts.
  
  Local Notation denv := (list typ).

  Definition denv_fv (D : denv) : atoms :=
    fold_right (fun T fvs => fvs \u fv T) {} D.

  Instance FvDenv : HasFv denv := { fv := denv_fv }.
  
  Definition env_weight (G : env) : nat :=
    fold_right (fun (tup : (var * typ)) w =>
                  let (_, T) := tup in w + typ_struct_measure T) 0 G.

  Definition denv_weight (D : denv) : nat :=
    fold_right (fun T w => w + typ_struct_measure T) 0 D.

  Local Notation wf_tup := (env * denv * typ)%type.
  
  Definition wf_measure (tup : wf_tup) : nat :=
    let '(G, D, T) := tup in env_weight G + denv_weight D + typ_struct_measure T.

  Lemma typ_struct_measure_neq_0 : forall T,
      typ_struct_measure T <> 0.
  Proof. destruct T; simpl; lia. Qed.
  
  Function wf_search_impl' (tup : wf_tup) {measure wf_measure tup} : nat :=
    let '(G, D, T) := tup in
    match T with
    | typ_top => 1
    | typ_bot => 1
    | typ_sel x =>
      match x with
      | avar_b n =>
        match D with
        | nil => 0
        | cons T' D =>
          match n with
          | O => 1 + wf_search_impl' (G, D, T')
          | S n' => wf_search_impl' (G, D, typ_sel (avar_b n'))
          end
        end
      | avar_f v =>
        match G with
        | nil => 0
        | cons (v', T') G' =>
          if v == v'
          then 1 + wf_search_impl' (G', nil, T')
          else wf_search_impl' (G', D, typ_sel (avar_f v))
        end

      end
    | typ_all T U =>
      1 + wf_search_impl' (G, T :: D, U)
    | typ_bnd T U =>
      1 + wf_search_impl' (G, D, T) + wf_search_impl' (G, D, U)
    end.
  Proof.
    all:intros; simpl; try lia.
    all:pose proof (typ_struct_measure_neq_0 T'); lia.
  Qed.
  
  (** now, we need to proceed on proving properties on wf_search_impl'.
   *
   * we can still transform some problems to structural recursion.
   *)
  
  Definition wf_search_impl G D T := wf_search_impl' (G, D, T).
  Arguments wf_search_impl G D T/.

  Definition wf_search_measure G T : nat := wf_search_impl G nil T.
  Arguments wf_search_measure G T/.

  Local Ltac mass_discharge :=
    simpl; trivial; try eassumption; try lia; set solve; try fsetdec.
  
  Ltac ev_wf := unfold wf_search_impl;
                rewrite wf_search_impl'_equation;
                trivial; try congruence.
  
  Ltac untup :=
    repeat lazymatch goal with
           | _ : context[wf_search_impl' (?G, ?D, ?T)] |- _ =>
             change (wf_search_impl' (G, D, T))
               with (wf_search_impl G D T) in *
           | |- context[wf_search_impl' (?G, ?D, ?T)] =>
             change (wf_search_impl' (G, D, T))
               with (wf_search_impl G D T)
           end.

  Fixpoint wf_search_fv G (D : denv) v : nat :=
    match G with
    | nil => 0
    | cons (v', T') G' =>
      if v == v'
      then 1 + wf_search_impl G' nil T'
      else wf_search_fv G' D v
    end.

  Lemma fv_search_equiv : forall G D v,
      wf_search_fv G D v = wf_search_impl G D (typ_sel $ avar_f v).
  Proof.
    induction G; intros; simpl; ev_wf; routine.
  Qed.

  Lemma fv_ignores_D : forall G D v,
      wf_search_fv G D v = wf_search_fv G nil v.
  Proof. induction G; routine. Qed.
  
  Fixpoint wf_search_bv G D n : nat :=
    match D with
    | nil => 0
    | cons T' D' =>
      match n with
      | O => 1 + wf_search_impl G D' T'
      | S n' => wf_search_bv G D' n'
      end
    end.

  Lemma bv_search_equiv : forall G D n,
      wf_search_bv G D n = wf_search_impl G D (typ_sel $ avar_b n).
  Proof.
    induction D; intros; simpl.
    - ev_wf.
    - destruct n.
      + rewrite (wf_search_impl'_equation (_, _, _ _)).
        trivial.
      + ev_wf. apply IHD.
  Qed.

  Lemma bv_search_idx : forall G D1 D2 T,
      wf_search_bv G (D1 ++ [T] ++ D2) (length D1) =
      1 + wf_search_impl G D2 T.
  Proof.
    induction D1; intros; simpl.
    - trivial.
    - simpl in IHD1. rewrite IHD1. trivial.
  Qed.
  
  Definition wf_search_avar G D v : nat :=
    match v with
    | avar_b n => wf_search_bv G D n
    | avar_f v => wf_search_fv G D v
    end.

  Lemma avar_search_equiv : forall G D v,
      wf_search_avar G D v = wf_search_impl G D (typ_sel v).
  Proof.
    intros. destruct v; simpl.
    - apply bv_search_equiv.
    - apply fv_search_equiv.
  Qed.
  
  Lemma wf_search_fv_weaken : forall G1 G2 D v,
      wf_env (G1 ++ G2) ->
      v `in` dom G2 ->
      wf_search_fv G2 D v = wf_search_fv (G1 ++ G2) D v.
  Proof.
    induction G1; intros; trivial.
    destruct_conjs. simpl. destruct_eq.
    - exfalso. eapply wf_var_in; mass_discharge.
    - apply IHG1; routine.
  Qed.

  Definition wf_weaken_measure (D : denv) (T : typ) : nat :=
    denv_weight D + typ_struct_measure T.
  Arguments wf_weaken_measure D T/.
  
  Program Fixpoint wf_search_weaken_gen D T {measure (wf_weaken_measure D T)} :
    forall G1 G2,
      wf_env (G1 ++ G2) ->
      fv T [<=] dom G2 ->
      fv D [<=] dom G2 ->
      wf_search_impl' (G2, D, T) = wf_search_impl' (G1 ++ G2, D, T) := _.
  Next Obligation.
    destruct T; try solve [do 2 ev_wf].
    - do 2 ev_wf. destruct a.
      + destruct D. trivial.
        destruct n.
        * simpl in H1.
          erewrite wf_search_weaken_gen; mass_discharge.
        * pose proof (typ_struct_measure_neq_0 t).
          erewrite wf_search_weaken_gen; mass_discharge.
      + assert (a `in` dom G2) by set solve.
        pose proof (wf_search_fv_weaken G1 G2 D H H2).
        do 2 rewrite fv_search_equiv in H3.
        simpl in H3.
        do 2 rewrite wf_search_impl'_equation in H3.
        apply H3.
    - simpl in H0.
      do 2 rewrite (wf_search_impl'_equation (_, D, _)).
      erewrite wf_search_weaken_gen; mass_discharge.
    - simpl in H0.
      do 2 rewrite (wf_search_impl'_equation (_, _, (_ _ _))).
      do 2 (rewrite wf_search_weaken_gen with (G1 := G1) (G2 := G2);
            mass_discharge).
  Qed.
          
  Theorem wf_search_weaken : forall G1 G2 T,
      wf_env (G1 ++ G2) ->
      fv T [<=] dom G2 ->
      wf_search_measure G2 T = wf_search_measure (G1 ++ G2) T.
  Proof.
    intros. apply wf_search_weaken_gen; mass_discharge.
  Qed.

  Lemma wf_lookup_lt : forall G1 G2 x T,
      wf_env (G1 ++ x ~~ T ++ G2) ->
      fv T [<=] dom G2 ->
      wf_search_measure G2 T <
      wf_search_measure (G1 ++ x ~~ T ++ G2) (typ_sel $ avar_f x).
  Proof.
    intros. rewrite <- wf_search_weaken; trivial.
    simpl. rewrite (wf_search_impl'_equation (_, _, _ _)).
    destruct_eq. lia.
    set solve.
  Qed.      

  
  Inductive incr_lc : list typ -> Prop :=
  | lc_nil : incr_lc nil
  | lc_cons : forall T D, lc_at (length D) T -> incr_lc D -> incr_lc (T :: D).
  Hint Constructors incr_lc.

  Fixpoint open_rec_D v (D : denv) :=
    match D with
    | nil => nil
    | cons T D' => open_rec (length D') v T :: open_rec_D v D'
    end.
  
  Program Fixpoint wf_open_shift_gen D U {measure (wf_weaken_measure D U)} :
    forall G n T x,
      wf_env G ->
      fv U [<=] dom G ->
      fv D [<=] dom G ->
      incr_lc (D ++ [T]) ->
      length D = n ->
      x `notin` fv G \u fv D \u fv T \u fv U ->
      lc_at (S n) U ->
      wf_search_impl' (G, D ++ [T], U) =
      wf_search_impl' (x ~~ T ++ G, open_rec_D x D, open_rec n x U) := _.
  Next Obligation.
    destruct U.
    - do 2 ev_wf.
    - do 2 ev_wf.
    - untup. rewrite <- avar_search_equiv.
      destruct a; simpl; ev_wf.
      + rewrite bv_search_equiv. ev_wf.
        destruct_eq.
        * destruct_eq.
          destruct D; trivial.

          simpl. untup.
          rewrite <- bv_search_equiv.
          change [T] with ([T] ++ nil).
          rewrite bv_search_idx. trivial.
        * progressive_inversions.
          assert (n < length D) by lia.
          destruct D.
          -- simpl in *. lia.
          -- simpl. simpl in H1.
             progressive_inversions.
             destruct n.
             ++ f_equal. apply wf_open_shift_gen; mass_discharge.
                rewrite app_length in H10. simpl in H10.
                rewrite Nat.add_comm in H10. trivial.
             ++ simpl in H3. assert (n < length D) by lia.

                pose proof (typ_struct_measure_neq_0 t).
                erewrite wf_open_shift_gen with (x := x); trivial.
                erewrite open_lc_le_typ.
                all:mass_discharge.
                all:repeat constructor.
                lia.
      + simpl. destruct_eq.
        * simpl in *. solve_notin.
        * untup. rewrite <- fv_search_equiv.
          rewrite fv_ignores_D.
          rewrite fv_ignores_D with (D := _ _ D).
          trivial.
    - ev_wf. rewrite (wf_search_impl'_equation (_ :: _, _, _)).
      simpl in *. progressive_destructions.
      f_equal. change (U1 :: D ++ [T]) with ((U1 :: D) ++ [T]).
      erewrite wf_open_shift_gen with (x := x); mass_discharge.
      constructor; trivial.
      rewrite app_length. simpl. rewrite Nat.add_comm.
      trivial.
    - ev_wf. rewrite (wf_search_impl'_equation (_ :: _, _, _)).
      simpl in *. progressive_destructions.
      do 2 f_equal.
      all:apply wf_open_shift_gen; mass_discharge.
  Qed.

  Lemma wf_open_shift : forall G T U x,
      wf_env G ->
      fv T [<=] dom G ->
      fv U [<=] dom G ->
      x `notin` fv G \u fv T \u fv U ->
      lc T ->
      lc_at 1 U ->
      wf_search_impl G [T] U =
      wf_search_impl (x ~~ T ++ G) nil (open x U).
  Proof.
    intros. eapply wf_open_shift_gen with (D := nil); trivial.
    all:progressive_destructions; simpl in *; mass_discharge.
    repeat constructor. trivial.
  Qed.

End MeasuresAndFacts.
Arguments wf_search_impl G D T/.
Arguments wf_search_measure G T/.

Ltac ev_wf := unfold wf_search_impl;
              rewrite wf_search_impl'_equation;
              trivial; try congruence.

Ltac untup :=
  repeat lazymatch goal with
         | _ : context[wf_search_impl' (?G, ?D, ?T)] |- _ =>
           change (wf_search_impl' (G, D, T))
             with (wf_search_impl G D T) in *
         | |- context[wf_search_impl' (?G, ?D, ?T)] =>
           change (wf_search_impl' (G, D, T))
             with (wf_search_impl G D T)
         end.

Inductive ExpoStuck : Set :=
| stuck_err : atom -> typ -> ExpoStuck.
Notation "'!err!expo_stuck' 'avar' n 'bound' 'to' t" := (stuck_err n t) (at level 10, only printing).

(** the specification of exposure, annotated individually.
 * 
 * G : the original context
 * G1 : potential extra context
 * G2 : the witness context of the subtyping relation
 * S : the input type
 * U : the result type
 *)
Definition exposure_spec (G G1 G2 : env) (S U : typ) : Prop :=
  (G2 ⊢ S <⦂ U) /\             (* witness of subtyping *)
  G1 ++ G2 = G /\              (* composition invariance *)
  fv U [<=] dom G2 /\
  (* free variables of output type is bound by context. *)
  lc U /\                      (* result type of locally closed. *)
  wf_search_measure G U <= wf_search_measure G S /\
  (* preserves measure for subtyping algorithm. *)
  match U with                (* result type is non-dependent. *)
  | typ_sel _ => False
  | _ => True
  end.

Local Ltac ref := apply res_ret; repeat eexists; repeat split; unfold exposure_spec.

Local Ltac st_refl := ref; apply st_refl || simpl in *;
                      lazymatch goal with
                      | |- ?G1 ++ _ = ?G =>
                        lazymatch type of G with list ?T => unify G1 (@nil T) end;
                        simpl; reflexivity
                      | _ => idtac
                      end; auto.

Local Ltac solve_by_weaken := 
  reassoc 3 with 2; apply weaken_subty; try eassumption;
  simpl_env; destruct_uniq; auto.

Local Ltac recurse t :=
  let n := fresh "recur" in
  pose proof t as n;
  refine (x <- n ;; _);
  unfold exposure_spec in *;
  destruct_conjs;
  simpl in *.

Local Ltac precise_typ_pf a t tail Hname :=
  let H := fresh "H" in
  assert (H : get a (a ~~ t ++ tail) = Some t) by routine;
  apply get_to_binds in H;
  pose proof (ty_var H) as Hname;
  eapply ty_sub in Hname.

Local Ltac destr a x :=
  destruct x eqn:?;
           lazymatch goal with
           | _ : x = typ_bot |- _ => idtac
           | _ : x = typ_bnd _ _ |- _ => idtac
           | _ : x = ?t |- _ => exact (eret $ stuck_err a t)
           end.

(** this implementation gives a super type of S in the given context.
 * in the failure case, it also gives an explanation on why.
 * 
 * it actually is able to show that the subtyping holds in a potentially smaller
 * context than the original one.
 *)
Program Fixpoint exposure_impl G S {measure (length G)} :
  wf_env G ->                  (* env wellformed-ness *)
  fv S [<=] dom G ->            (* free variable in S needs to be captured by context. *)
  lc S ->                      (* S needs to be closed *)
  Result { U : typ & { G1 & { G2 | exposure_spec G G1 G2 S U } } } := _.
Next Obligation.
  refine (match S as S0 return
                fv S0 [<=] dom G -> lc S0 ->
                Result { U : typ & { G1 & { G2 | exposure_spec G G1 G2 S0 U } } } with
          | typ_sel a => fun subset lcS => _
          | _ => fun subset lc => ltac:(st_refl)
          end H0 H1).

  destruct a; [simpl in lcS; exfalso; progressive_inversions |].
  assert (ainG : a `in` dom G) by set solve.
  destruct (get_for_sure G ainG) as [T Heqo].
  pose proof (get_witness _ _ Heqo). destruct_conjs.
  assert (lenX : length X < length G). {
    rewrite <- H3. repeat rewrite app_length.
    simpl. lia.
  }

  assert (wfX : wf_env X).
  { subst G. eapply wf_deapp. instantiate (1 := H2 ++ a ~~ T). simpl_env. trivial. }
  assert (wfapp : wf_env (a ~~ T ++ X)). {
    subst. eapply wf_deapp. eassumption.
  }
  assert (fvT : fv T [<=] dom X).
  { invert wfapp. trivial. }
  assert (lcT : lc T).
  { invert wfapp. trivial. }
  
  recurse (exposure_impl X T lenX wfX fvT lcT).
  destr a x.
  + ref; subst.
    
    let H0 := fresh "H" in
    precise_typ_pf a T (X0 ++ X1) H0;
      [ eapply st_sel2; apply H0 |].
    do 2 instantiate (1 := typ_bot).
    eapply st_trans.

    * solve_by_weaken.
    * constructor.
    * reflexivity.
    * set solve.
    * trivial.
    * pose proof (wf_lookup_lt H2 (X0 ++ X1) a T H fvT).
      pose proof (wf_search_weaken (H2 ++ a ~~ T) (X0 ++ X1)).
      rewrite app_assoc in H5.
      pose proof (H5 typ_bot H ltac:(set solve)).
      specialize (H5 T H fvT).
      simpl in *. lia.
    * trivial.

  + simpl in H6. fold_fv.
    assert (fv t2 [<=] dom X). {
      etransitivity. apply AtomSetProperties.union_subset_2.
      etransitivity. eassumption.
      subst X. set solve.
    }
    assert (lc t2) by routine.

    recurse (exposure_impl X t2 lenX wfX H10 H11). subst.
    ref. eapply st_trans.
    eapply st_sel2. eapply ty_sub. constructor.
    * eroutine.
    * solve_by_weaken.
    * rewrite <- H13 in *. solve_by_weaken.
    * rewrite <- H13. set solve.
    * trivial.
    * rewrite <- H13 in *.
      rewrite wf_search_impl'_equation in H8.

      pose proof (wf_search_weaken (H2 ++ a ~~ T) (X2 ++ X3)).
      rewrite app_assoc in H3.
      pose proof (H3 x H ltac:(set solve)).
      pose proof (H3 T H ltac:(set solve)).
      pose proof (wf_lookup_lt H2 (X2 ++ X3) a T H fvT).
      simpl in *. lia.
    * trivial.
Defined.

Local Ltac unpack := repeat (split || eexists || apply res_ret).

Local Ltac prode_refl :=
  unpack; apply st_refl || solve_notin.

Local Ltac recurse t ::=
  let n := fresh "recur" in
  pose proof t as n;
  destruct_conjs;
  simpl in *.

Local Ltac ref_body b :=
  let x := fresh "x" in refine (x <- b ;; _); destruct_conjs.

Local Ltac precise_typ a t tail pf :=
  let H0 := fresh "H" in
  precise_typ_pf a t tail H0;
  [ eapply pf;
    eapply weaken_typing; apply H0 |].

Ltac subset_fv S :=
  lazymatch goal with
  | H : wf_env (_ :: _) |- ?a `notin` _ =>
    let Contra := fresh "Contra" in
    invert H; subst;
    autorewrite with meta_ext in *;
    intro Contra;
    match goal with
    | H1 : a `notin` _ |- _ =>
      apply H1; enough (a `in` fv S); set solve
    end
  end.

Local Ltac solve_decl :=
  destruct_conjs;
  unpack;
  [eapply st_bnd; eassumption |
   simpl; solve_notin |
   routine |
   simpl; set solve].

(** specifictions for promotion and demotion. *)

(** 
 * G : the context
 * x : the variable to remove
 * S : the input type
 * U : the result type
 *)
Definition promo_spec G x (S U : typ) :=
  subty G S U /\                 (* U is a super type of S *)
  x `notin` fv U /\              (* x is indeed remvoed *)
  lc U /\                        (* U remains closed *)
  fv U [<=] dom G.               (* free variables are bound. *)

Definition demo_spec G x (S U : typ) :=
  subty G U S /\
  x `notin` fv U /\
  lc U /\
  fv U [<=] dom G.

(** it's tricky that promotion and demotion are mutually recursive.
 * 
 * luckily, they are dual, so we can as well compute them at the same time,
 * hence the signature returns a tuple of results.
 *)
Program Fixpoint promo_demo G x S {measure (typ_struct_measure S)} :
  wf_env G ->                    (* environment well-formed *)
  fv S [<=] dom G ->              (* free vars of input are bound. *)
  lc S ->                        (* input type is closed *)
  Result { U1 | promo_spec G x S U1 } *
  Result { U2 | demo_spec G x S U2 } := _.
Next Obligation.
  unfold promo_spec in *; unfold demo_spec in *.
  destruct S.
  - prode_refl.
  - prode_refl.
  - destruct a. simpl in H1; exfalso; progressive_inversions.

    destruct (x == a); [ subst x | prode_refl ].

    assert (ainG : a `in` dom G).
    { apply H0. simpl. apply AtomSetImpl.singleton_2. trivial. }
    destruct (get_for_sure G ainG) as [T Heqo].

    pose proof (get_witness _ _ Heqo). destruct_conjs.
    assert (lenX : length X < length G). {
      rewrite <- H3. repeat rewrite app_length.
      simpl. lia.
    }
    assert (wfX : wf_env X).
    { subst G. eapply wf_deapp.
      instantiate (1 := H2 ++ a ~~ T). simpl_env. trivial. }
    assert (wfapp : wf_env (a ~~ T ++ X)). {
      subst. eapply wf_deapp. eassumption.
    }
    assert (fvT : fv T [<=] dom X).
    { invert wfapp; trivial. }
    assert (lcT : lc T).
    { invert wfapp; trivial. }

    recurse (exposure_impl wfX fvT lcT).
    unfold exposure_spec in recur.
    split; ref_body recur.
    
    all:destr a x.
    + ref; subst.
      
      precise_typ a T (X0 ++ X1) @st_sel2.
      do 2 instantiate (1 := typ_bot).
      eapply st_trans.
      * solve_by_weaken.
      * constructor.
      * solve_notin.
      * trivial.
      * set solve.
        
    + assert (fv t2 [<=] dom X1).
      { simpl in H6. etransitivity; [ | eassumption]; set solve. }

      ref; subst.
      * precise_typ a T (X0 ++ X1) @st_sel2.
        solve_by_weaken.
      * assert (a `notin` dom X1).
        { invert wfapp. simpl in H11. set solve. }
        eapply notin_subset_relax; eassumption.
      * routine.
      * simpl_env. etransitivity; [eassumption |].
        set solve.

    + ref; subst.
      precise_typ a T (X0 ++ X1) @st_sel1.
      do 2 instantiate (1 := typ_top).

      eapply st_trans.
      * solve_by_weaken.
      * constructor.
      * solve_notin.
      * trivial.
      * set solve.

    + assert (fv t1 [<=] dom X1).
      { simpl in H6. etransitivity; [ | eassumption]; set solve. }

      ref; subst.
      * precise_typ a T (X0 ++ X1) @st_sel1.
        solve_by_weaken.
      * assert (a `notin` dom X1).
        { invert wfapp. simpl in H11. set solve. }
        eapply notin_subset_relax; eassumption.
      * routine.
      * simpl_env. etransitivity; [eassumption |].
        set solve.
  -
    assert (wf1 : typ_struct_measure S1 <
                  typ_struct_measure (typ_all S1 S2)) by (simpl; lia).
    assert (wf2 : typ_struct_measure S2 <
                  typ_struct_measure (typ_all S1 S2)) by (simpl; lia).
    simpl in H0. fold_fv.
    assert (fvS1 : fv S1 [<=] dom G) by set solve.
    assert (fvS2 : fv S2 [<=] dom G) by set solve.
    assert (lcS1 : lc S1) by routine.
    assert (lcS2 : lc_at 1 S2).
    { invert H1. assumption. }
    
    recurse (promo_demo G x S1 wf1 H fvS1 lcS1).
    split.
    + ref_body r0.
      pick_fresh z.
      rewrite <- (open_typ_same_measure _ 0 z) in wf2 at 1.
      assert (wf_env (z ~~ x0 ++ G)).
      { constructor; routine. }
      assert (fv (open z S2) [<=] dom (z ~~ x0 ++ G)). {
        etransitivity. apply fv_open_typ. simpl.
        rewrite AtomSetProperties.add_union_singleton.
        set solve.
      }
      assert (lc (open z S2)) by (apply open_lc_typ; trivial).
      
      pose proof (promo_demo (z ~~ x0 ++ G) x (open z S2) wf2 H6 H7 H8).
      destruct X. ref_body r1.
      ref.
      * eapply st_all. eassumption.
        cofinite.
        pose proof (@fv_close_self_typ x1 z 0).
        assert (x2 `notin` fv (close z x1)). {
          eapply notin_subset_relax.
          apply fv_close_typ. auto.
        }

        erewrite <- open_left_inv_typ in H9.
        assert (uniq G) by (apply wf_uniq; trivial).
        eapply open_subst_subty in H9.

        -- eassumption.
        -- trivial.
        -- solve_notin.
        -- solve_notin.
        -- trivial.
      * assert (x `notin` fv (close z x1)). {
          eapply notin_subset_relax.
          apply fv_close_typ. auto.
        }
        solve_notin.
      * constructor; trivial.
        apply close_lc_typ. trivial.
      * simpl.
        assert (fv (close z x1) [<=] dom G). {
          apply fv_add_close_typ. trivial.
        }
        set solve.

    + ref_body r.
      pick_fresh z.
      rewrite <- (open_typ_same_measure _ 0 z) in wf2 at 1.
      assert (wf_env (z ~~ S1 ++ G)).
      { constructor; routine. }
      assert (fv (open z S2) [<=] dom (z ~~ S1 ++ G)). {
        etransitivity. apply fv_open_typ. simpl.
        rewrite AtomSetProperties.add_union_singleton.
        set solve.
      }
      assert (lc (open z S2)) by (apply open_lc_typ; trivial).
      
      pose proof (promo_demo (z ~~ S1 ++ G) x (open z S2) wf2 H6 H7 H8).
      destruct X. ref_body r2.
      ref.
      * eapply st_all. eassumption.
        cofinite.
        pose proof (@fv_close_self_typ x1 z 0).
        assert (x2 `notin` fv (close z x1)). {
          eapply notin_subset_relax.
          apply fv_close_typ. auto.
        }

        erewrite <- (@open_left_inv_typ x1) in H9.
        assert (uniq G) by (apply wf_uniq; trivial).
        eapply open_subst_subty in H9.

        -- eassumption.
        -- trivial.
        -- solve_notin.
        -- solve_notin.
        -- trivial.
      * assert (x `notin` fv (close z x1)). {
          eapply notin_subset_relax.
          apply fv_close_typ. auto.
        }
        solve_notin.
      * constructor; trivial.
        apply close_lc_typ. trivial.
      * simpl.
        assert (fv (close z x1) [<=] dom G). {
          apply fv_add_close_typ. trivial.
        }
        set solve.

  - assert (typ_struct_measure S2 < typ_struct_measure (typ_bnd S1 S2))
      by (simpl; lia).
    assert (typ_struct_measure S1 < typ_struct_measure (typ_bnd S1 S2))
      by (simpl; lia).
    simpl in H0. fold_fv.
    assert (fvS1 : fv S1 [<=] dom G) by set solve.
    assert (fvS2 : fv S2 [<=] dom G) by set solve.
    assert (lcS1 : lc S1) by routine.
    assert (lcS2 : lc S2).
    { invert H1. assumption. }
   
    pose proof (promo_demo G x S2 H2 H fvS2 lcS2).
    pose proof (promo_demo G x S1 H3 H fvS1 lcS1).
    destruct_conjs. split.
    + refine (x <- r1 ;; y <- r0 ;; _).
      solve_decl.
    + refine (x <- r2 ;; y <- r ;; _).
      solve_decl.
Defined.


Definition promotion G x S :
  wf_env G ->
  fv S [<=] dom G ->
  lc S ->
  Result { U | promo_spec G x S U }.
Proof.
  intros. edestruct (promo_demo); eassumption.
Defined.

Definition demotion G x S :
  wf_env G ->
  fv S [<=] dom G ->
  lc S ->
  Result { U | demo_spec G x S U }.
Proof.
  intros. edestruct (promo_demo); eassumption.
Defined.


(* subtyping/typing algorithm *)
Create HintDb subty discriminated.
Hint Rewrite -> dom_app : subty.
Ltac step := autorewrite with subty in *.
Ltac mass_discharge :=
  trivial; try solve [subst; simpl in *; step; (lia || set solve; routine)].

Lemma measure_shrinks : forall G x T,
    wf_env G ->
    get x G = Some T ->
    wf_search_measure G T <
    wf_search_measure G (typ_sel $ avar_f x).
Proof.
  intros. apply get_witness in H0.
  destruct_conjs.
  assert (wf_env (x ~~ T ++ X)) by (eapply wf_deapp; routine).
  progressive_inversions.
  pose proof (wf_lookup_lt H0 X x T ltac:(subst; trivial) ltac:(trivial)).
  rewrite wf_search_weaken with (G1 := H0 ++ x ~~ T) (G2 := X) in H1.
  all:repeat rewrite app_assoc in *; trivial.
Qed.

Lemma measure_unfold_bnd : forall G T U,
    wf_search_measure G (typ_bnd T U) =
    1 + wf_search_measure G T + wf_search_measure G U.
Proof. intros. simpl. ev_wf. Qed.
Hint Rewrite -> measure_unfold_bnd : subty.

Lemma measure_unfold_all : forall G T U x,
    wf_env G ->
    fv (typ_all T U) [<=] dom G ->
    x `notin` fv G \u fv T \u fv U ->
    lc (typ_all T U) ->
    wf_search_measure G (typ_all T U) =
    1 + wf_search_measure (x ~~ T ++ G) (open x U).
Proof.
  intros. simpl. ev_wf. untup.
  simpl. f_equal.
  eapply wf_open_shift; mass_discharge.
Qed.

Inductive SubtypUndec : Set :=
| st_undec : forall (T U : typ), SubtypUndec.
Notation "'!err!subtyping' 'relation' 'undetermined:' T '<:' U ?" :=
  (st_undec T U) (at level 10, only printing).

Inductive FuncInputTypeMismatch : Set :=
| st_all_mis : forall (S1 S2 : typ), FuncInputTypeMismatch.
Notation "'!err!subtyping' 'types' S1 'and' S2 'are' 'input' 'types' 'of' 'functions' 'but' 'they' 'are' 'not' 'structurally' 'equal'!" :=
  (st_all_mis S1 S2) (at level 10, only printing).


Ltac st_undec T U :=
  lazymatch goal with
  | _ : T = ?T', _ : U = ?U' |- _ => 
    exact (eret $ st_undec T' U')
  end.

Ltac lc_bv :=
  lazymatch goal with
  | H : lc_typ_at 0 (typ_sel $ avar_b _) |- _ =>
    exfalso; progressive_inversions
  | H : lc_trm_at 0 (trm_var $ avar_b _) |- _ =>
    exfalso; progressive_inversions
  | _ => idtac
  end.

Ltac lookup_in a G :=
  let InWit := fresh "H" in
  let H := fresh "H" in
  assert (InWit : a `in` dom G) by set solve;
  destruct (get_for_sure G InWit) as [? H];
  pose proof (get_witness _ _ H);
  destruct_conjs.

Ltac expose a T G :=
  let wfapp := fresh in
  let wfe := fresh in
  let fvT := fresh in
  let lcT := fresh in
  let er := fresh in
  let x' := fresh "x" in
  assert (wfapp : wf_env (a ~~ T ++ G)) by (eapply wf_deapp; routine);
  assert (wfe : wf_env G /\ lc T /\ fv T [<=] dom G) by routine;
  clear wfapp;
  destruct wfe as [wfe [lcT fvT]];
  pose proof (exposure_impl wfe fvT lcT) as er;
  unfold exposure_spec in er;
  refine (x' <- er ;; _);
  destruct_conjs.

Ltac measure_weaken H G1 G2 :=
  rewrite (wf_search_weaken G1 G2) in H;
  repeat rewrite app_assoc in *;
  try solve [subst; trivial; set solve].

Ltac shorten_env G :=
  repeat match goal with
         | H : _ = G |- _ => rewrite -> H in *
         | H : G = _ |- _ => rewrite <- H in *
         end.

Local Ltac precise_typ_upper a t tail pf :=
  let H0 := fresh "H" in
  precise_typ_pf a t tail H0;
  [ eapply st_trans;
    [eapply pf;
    eapply weaken_typing; apply H0 | eassumption] |].

Local Ltac precise_typ_lower a t tail pf :=
  let H0 := fresh "H" in
  precise_typ_pf a t tail H0;
  [ eapply st_trans;
    [ eassumption |
      eapply pf;
      eapply weaken_typing; apply H0] |].


Ltac solve_dep_typ a G pf :=
  lookup_in a G;
  match goal with
  | HwfG : wf_env G,
    Hfnd : get a G = Some ?T,
    Hwit : ?Hd ++ a ~~ ?T ++ ?X = G |- _ =>
    expose a T X;
    lazymatch goal with
    | Heq : ?X0 ++ ?X1 = X, Hsub : ?X1 ⊢ ?T <⦂ ?U |- _ =>
      destr a U; [
        ref; subst;
        precise_typ a T (X0 ++ X1) pf;
        repeat instantiate (1 := typ_bot);
        eapply st_trans; solve_by_weaken
      | step;
        lazymatch goal with
        | Hmeas : _ <= wf_search_measure X T |- _ =>
          repeat measure_weaken Hmeas (Hd ++ a ~~ T) X;
          shorten_env G;
          pose proof (measure_shrinks a HwfG Hfnd)
        end
      ]
    end
  end.

Ltac subgoal T tac :=
  let H := fresh "H" in
  unshelve evar (H : T);
  [| let H' := eval unfold H in H in
         clear H; tac H'].

Ltac rec_pose t :=
  let rec go t' :=
      lazymatch type of t' with
      | forall _ : ?T, _ =>
        subgoal T ltac:(fun gl => go constr:(t' gl))
      | _ => pose proof t'
      end in
  go t.

Ltac rec_subty_upper func G :=
  match goal with
  | Hsub : _ ⊢ _ <⦂ typ_bnd _ ?upper |- Result (_ ⊢ _ <⦂ ?target) =>
    rec_pose (func G upper target); mass_discharge
  end.

Ltac rec_subty_lower func G :=
  match goal with
  | Hsub : _ ⊢ _ <⦂ typ_bnd ?lower _ |- Result (_ ⊢ ?target <⦂ _) =>
    rec_pose (func G target lower); mass_discharge
  end.

Ltac upper_subty G pf :=
  let x := fresh "x" in
  match goal with
  | X : Result (G ⊢ _ <⦂ ?T),
    HeqG : _ ++ ?v ~~ ?V ++ ?Y = G,
    HeqY : ?L0 ++ ?L1 = ?Y |- Result (G ⊢ _ <⦂ ?T) =>
    refine (x <- X ;; _);
    ref; subst;
    precise_typ_upper v V (L0 ++ L1) pf;
    solve_by_weaken
  end.

Ltac lower_subty G pf :=
  let x := fresh "x" in
  lazymatch goal with
  | X : Result (G ⊢ ?T <⦂ _),
    HeqG : _ ++ ?v ~~ ?V ++ ?Y = G,
    HeqY : ?L0 ++ ?L1 = ?Y |- Result (G ⊢ ?T <⦂ _) =>
    refine (x <- X ;; _);
    ref; subst;
    precise_typ_lower v V (L0 ++ L1) pf;
    solve_by_weaken
  end.

Ltac lookup_from_left func a G :=
  solve_dep_typ a G @st_sel2;
  rec_subty_upper func G;
  upper_subty G @st_sel2.

Ltac lookup_from_right func a G :=
  solve_dep_typ a G @st_sel1;
  rec_subty_lower func G;
  lower_subty G @st_sel1.

Definition subty_weight G T U :=
  wf_search_measure G T + wf_search_measure G U.
Arguments subty_weight G T U/.
Arguments wf_search_measure : simpl never.

(** subtyping decision procedure.
 * 
 * G : the context
 * T : the supposed subtype
 * U : the supposed supertype
 *)
Program Fixpoint subty_dec G T U {measure (subty_weight G T U)} :
  wf_env G ->                    (* environment is wellformed *)
  fv T [<=] dom G ->              (* all free vars are bound *)
  lc T ->                        (* all types are closed *)
  fv U [<=] dom G ->
  lc U ->
  Result (G ⊢ T <⦂ U) := _.
Next Obligation.
  destruct T eqn:?; destruct U eqn:?;
    lazymatch goal with
    | _ : T = typ_bot |- _ => ref; apply st_bot (* trivial case: bot <: T *)
    | _ : U = typ_top |- _ => ref; apply st_top (* trivial case: T <: top *)
    | _ => idtac
    end;
    lazymatch goal with
    | _ : T = typ_top |- _ => st_undec T U (* trivial case: ??? top <: _ *)
    | _ : U = typ_bot |- _ => st_undec T U (* trivial case: ??? _ <: bot *)
    | _ => idtac
    end;
    repeat lazymatch goal with
           | a : avar |- _ => destruct a; lc_bv
           end.
  
  (* remaining 9 cases left. *)

  - destruct (a == a0). ref; subst; apply st_refl.
    lookup_from_left subty_dec a G.
  - lookup_from_left subty_dec a G.
  - lookup_from_left subty_dec a G.
  - lookup_from_right subty_dec a G.
  - destruct (t1 == t3); [ subst | exact (eret $ st_all_mis t1 t3)].
    pick_fresh z. simpl in *.
    rec_pose (subty_dec (z ~~ t3 ++ G) (open z t2) (open z t4)).
    + repeat (rewrite measure_unfold_all with (x := z); mass_discharge).
    + progressive_inversions.
      constructor; mass_discharge.
    + pose proof (fv_open_typ t2 z 0).
      etransitivity; [eassumption |].
      set solve.
    + invert H1. subst.
      apply open_lc_typ. trivial.
    + pose proof (fv_open_typ t4 z 0).
      etransitivity; [eassumption |].
      set solve.
    + invert H3. subst.
      apply open_lc_typ. trivial.
    + refine (x <- X ;; _).
      ref. eapply st_all; [ apply st_refl |].
      cofinite.
      apply (@open_subst_subty z); simpl in *; auto with meta_ext core.
      apply wf_uniq; trivial.
  - st_undec T U.             (* don't know how to compare typ_all and typ_bnd *)
  - lookup_from_right subty_dec a G.
  - st_undec T U.             (* don't know how to compare typ_bnd and typ_all *)
  - rec_pose (subty_dec G t3 t1); mass_discharge.
    rec_pose (subty_dec G t2 t4); mass_discharge.
    refine (x <- X ;; y <- X0 ;; _).
    ref. apply st_bnd; trivial.
Defined.

Ltac invref := ref; subst; progressive_inversions; simpl in *.
Ltac refsolve := invref; try solve [routine].

Ltac precise_typ_var a G :=
  match goal with
  | Heq : ?Hd ++ a ~~ ?T ++ ?Tl = G |- _ =>
    let H := fresh "H" in
    precise_typ_pf a T Tl H;
    [ | apply st_refl];
    eapply weaken_typing in H;
    match type of H with
    | ?Pre ++ a ~~ T ++ Tl ⊢ _ ⦂ ?T' =>
      unify Pre Hd; unify T' T
    end
  end.

Inductive NotBoundToFuncTypes : Set :=
| ty_nfunc : forall (a : var) (T : typ), NotBoundToFuncTypes.
Notation "'!err!typing' a 'binds' 'to' T 'which' 'is' 'not' 'a' 'function' 'type' !" :=
  (ty_nfunc a T) (at level 10, only printing).

(** the specification of type checking algorithm.
 * 
 * G : the context
 * t : the term
 * T : the result type
 *)
Definition typing_spec G t T :=
  (G ⊢ t ⦂ T) /\                 (* guarantees typing relation *)
  fv T [<=] dom G /\              (* free vars of result type is bound. *)
  lc T.                         (* result type is closed *)
Arguments typing_spec G t T/.

(** the actual type checking procedure.
 * 
 * G : the context
 * t : the term
 *)
Program Fixpoint typing_impl G t {measure (trm_struct_measure t)} :
  wf_env G ->                    (* context is well formed *)
  fv t [<=] dom G ->              (* free vars are bound *)
  lc t ->                        (* term is locally closed. *)
  Result {T : typ | typing_spec G t T } := _.
Next Obligation.
  destruct t.
  - destruct a; lc_bv.
    lookup_in a G.
    assert (wf_env (a ~~ x ++ X)) by (eapply wf_deapp; routine).
    refsolve. step. set solve.
  - destruct v.
    + refsolve; simpl; set solve.
    + pick_fresh z.
      rec_pose (typing_impl (z ~~ t ++ G) (open z t0));
        progressive_inversions.
      * rewrite open_trm_same_measure. simpl. lia.
      * constructor; mass_discharge.
      * pose proof (fv_open_trm t0 z 0).
        etransitivity; [eassumption |].
        set solve.
      * apply open_lc_trm. trivial.
      * simpl in X. ref_body X.
        invref.
        -- econstructor.
           intros.
           cofinite.
           rewrite <- (@open_left_inv_typ x 0 z) in H2 by trivial.
           assert (z `notin` fv (close z x)) by apply fv_close_self_typ.
           assert (x0 `notin` fv (close z x)). {
             eapply notin_subset_relax.
             apply fv_close_typ. auto.
           }
           eapply (@open_subst_typing z); try eassumption.
           apply wf_uniq; trivial.
           all:solve_notin.
        -- pose proof (fv_add_close_typ x G 0 H3).
           set solve.
        -- constructor; trivial. 
           apply close_lc_typ; trivial.
  - destruct a; destruct a0;
      lazymatch goal with
      | _ : nat |- _ => exfalso; progressive_inversions; lc_bv
      | _ => idtac
      end.

    lookup_in a G.
    expose a x X.
    lookup_in a0 G.
    precise_typ_var a0 G.
    assert (G ⊢ (trm_var $ avar_f a) ⦂ x0). {
      precise_typ_var a G. subst.
      eapply ty_sub. eassumption.
      reassoc 4 with 3.
      apply weaken_subty; apply H6.
    }
    
    shorten_env G;
      destruct x0 eqn:?;
               lazymatch goal with
               | _ : x0 = typ_bot |- _ => idtac
               | _ : x0 = typ_all _ _ |- _ => idtac
               | _ : x0 = ?t |- _ => exact (eret $ ty_nfunc a t)
               end.
    + ref. econstructor.
      instantiate (1 := typ_bot).

      eapply ty_sub. eassumption.
      all:mass_discharge.
    + assert (wf_env (a0 ~~ x1 ++ X2)). {
        eapply wf_deapp. rewrite <- H19 in *; eassumption.
      }
      assert (fv x1 [<=] dom X2 /\ lc x1) by routine.
      destruct_conjs.
      rec_pose (@subty_dec G x1 t1); trivial.
      * rewrite <- H19 in *. step. set solve.
      * simpl in *. rewrite <- H5 in *. rewrite <- H11 in *.
        step. simpl. set solve.
      * routine.
      * refine (sub <- X3 ;; _).
        ref. 
        -- econstructor. eassumption. eauto using ty_sub.
        -- assert (a0 `in` dom G) by (subst; set solve).
           pose proof (fv_open_typ t2 a0 0).
           etransitivity; [eassumption |].
           apply AtomSetProperties.union_subset_3.
           set solve. subst. step. set solve.
        -- apply open_lc_typ. routine.
  - rec_pose (typing_impl G t1); mass_discharge.
    ref_body X.
    pick_fresh z.
    rec_pose (typing_impl (z ~~ x ++ G) (open z t2)).
    + rewrite open_trm_same_measure. simpl. lia.
    + constructor; mass_discharge.
    + pose proof (fv_open_trm t2 z 0).
      etransitivity; [eassumption |].
      set solve.
    + apply open_lc_trm. routine.
    + ref_body X0.
      rec_pose (@promotion (z ~~ x ++ G) z x0); mass_discharge.
      constructor; mass_discharge.
      unfold promo_spec in X1.
      ref_body X1. ref.
      * eapply ty_let. eassumption.
        cofinite. instantiate (1 := x1).
        rewrite <- open_lc_le_typ with (m := 0) (n := 0) (x := x2)
          by mass_discharge.
        apply open_subst_typing with (x := z); mass_discharge.
        apply wf_uniq; trivial.
        erewrite open_lc_le_typ by mass_discharge.
        eapply ty_sub. eassumption. trivial.
      * autounfold in *. intros.
        specialize (H11 a H12). simpl in *.
        apply add_iff in H11. destruct H11; [subst | assumption].
        intuition.
      * trivial.
Defined.

Definition typecheck' (t : trm) :
  fv t [=] empty -> lc t -> Result {T : typ | typing_spec nil t T }.
Proof.
  intros. apply typing_impl; trivial.
  rewrite H. set solve.
Defined.

(** we then needs to discharge that two extra predicates which are decidable. *)

Inductive HasFreeVars : Set :=
| has_fv_typ : forall {a} {T : typ}, a `in` fv T -> HasFreeVars
| has_fv_trm : forall {a} {t : trm}, a `in` fv t -> HasFreeVars
| has_fv_val : forall {a} {vl : val}, a `in` fv vl -> HasFreeVars.
Notation "'!err!checking' a 'is' 'a' 'free' 'var' 'in' 'type' T !" :=
  (@has_fv_typ a T _) (at level 10, only printing).
Notation "'!err!checking' a 'is' 'a' 'free' 'var' 'in' 'term' t !" :=
  (@has_fv_trm a t _) (at level 10, only printing).
Notation "'!err!checking' a 'is' 'a' 'free' 'var' 'in' 'value' vl !" :=
  (@has_fv_trm a vl _) (at level 10, only printing).

Local Ltac res := apply res_ret; simpl.
Local Ltac resfv := solve [res; set solve].
Local Ltac hasfv t :=
  lazymatch goal with
  | _ : t = ?v |- _ =>
    lazymatch type of t with
    | typ => refine (eret $ @has_fv_typ _ v _)
    | trm => refine (eret $ @has_fv_trm _ v _)
    | val => refine (eret $ @has_fv_val _ v _)
    end
  end;
  subst; set solve.

Local Ltac rec t :=
  let x := fresh "x" in
  let X := fresh "X" in
  pose proof t as X;
  refine (x <- X ;; _).

Hint Extern 1 => match goal with
                | H : _ [=] empty |- _ => rewrite H in *
                | H : empty [=] _ |- _ => rewrite <- H in *
                end : meta_ext.

Fixpoint check_fv_typ (T : typ) {struct T} : Result (fv T [=] empty).
Proof.
  destruct T eqn:?; intros.
  - resfv.
  - resfv.
  - destruct a. resfv. hasfv T. 
  - rec (check_fv_typ t1);
      rec (check_fv_typ t2).
    resfv.
  - rec (check_fv_typ t1);
      rec (check_fv_typ t2).
    resfv.
Defined.

Fixpoint check_fv_trm (t : trm) {struct t} : Result (fv t [=] empty)
with check_fv_val (vl : val) {struct vl} : Result (fv vl [=] empty).
Proof.
  - destruct t eqn:?.
    + destruct a. resfv. hasfv t.
    + rec (check_fv_val v). resfv.
    + destruct a; destruct a0; try resfv;
        hasfv t.
    + rec (check_fv_trm t0_1);
        rec (check_fv_trm t0_2).
      resfv.

  - destruct vl eqn:?.
    + rec (check_fv_typ t). resfv.
    + rec (check_fv_typ t);
        rec (check_fv_trm t0).
      resfv.
Defined.

Inductive NotClosed : Set :=
| nc_at_typ : forall {n} {T : typ} {m}, lc_at m T -> n < m -> NotClosed
| nc_at_trm : forall {n} {t : trm} {m}, lc_at m t -> n < m -> NotClosed
| nc_at_val : forall {n} {vl : val} {m}, lc_at m vl -> n < m -> NotClosed.
Notation "'!err!checking' 'type' T 'is' 'not' 'closed' 'at' n !" :=
  (@nc_at_typ n T _ _ _) (at level 10, only printing).
Notation "'!err!checking' 'term' t 'is' 'not' 'closed' 'at' n !" :=
  (@nc_at_trm n t _ _ _) (at level 10, only printing).
Notation "'!err!checking' 'value' vl 'is' 'not' 'closed' 'at' n !" :=
  (@nc_at_val n vl _ _ _) (at level 10, only printing).

Local Ltac reslc := solve [res; repeat constructor; trivial; lia].
Local Ltac refnc n t :=
  lazymatch goal with
  | _ : t = ?v |- _ =>
    lazymatch type of t with
    | typ => refine (eret $ @nc_at_typ n v _ _ _)
    | trm => refine (eret $ @nc_at_trm n v _ _ _)
    | val => refine (eret $ @nc_at_val n v _ _ _)
    end
  end.

Local Ltac nc n t :=
  refnc n t;
  subst; try eassumption;
  repeat lazymatch goal with
         | |- lc_at _ _ => constructor
         end;
  auto.

Local Ltac compare_nat n :=
  repeat match goal with
         | n' : nat |- _ => different n n';
                        destruct (le_lt_dec (S n') n);
                        fail_if_dup
         end.

Fixpoint check_lc_at_typ n (T : typ) {struct T} : Result (lc_at n T).
Proof.
  destruct T eqn:?; intros; try reslc.
  - destruct a. compare_nat n.
    reslc. nc n T.
    reslc.
  - rec (check_lc_at_typ n t1);
      rec (check_lc_at_typ (S n) t2). 
    reslc.
  - rec (check_lc_at_typ n t1);
      rec (check_lc_at_typ n t2).
    reslc.
Defined.

Fixpoint check_lc_at_trm n (t : trm) {struct t} : Result (lc_at n t)
with check_lc_at_val n (vl : val) {struct vl} : Result (lc_at n vl).
Proof.
  - destruct t eqn:?; intros; try reslc.
    + destruct a; compare_nat n.
      reslc. nc n t.
      reslc.
    + rec (check_lc_at_val n v); reslc.
    + destruct a; destruct a0;
        compare_nat n;
        try (reslc || solve [nc n t; lia]).
      refnc n t.
      instantiate (1 := Nat.max (S n0) (S n1)).
      do 2 constructor; lia. lia.
    + rec (check_lc_at_trm n t0_1);
        rec (check_lc_at_trm (S n) t0_2).
      reslc.

  - destruct vl eqn:?; intros.
    + rec (check_lc_at_typ n t). reslc.
    + rec (check_lc_at_typ n t);
        rec (check_lc_at_trm (S n) t0).
      reslc.
Defined.

Definition typecheck (t : trm) : Result {T : typ | typing_spec nil t T } :=
  nfv <- check_fv_trm t ;;
  tlc <- check_lc_at_trm 0 t ;;
  typecheck' nfv tlc.
