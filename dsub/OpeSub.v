Set Implicit Arguments.
Require Import Definitions.
Require Import OperationProperties.
Require Import StructuralProperties.

Inductive ope_sub : env -> env -> Prop :=
| os_nil : ope_sub nil nil
| os_drop : forall G1 G2 x T,
    ope_sub G1 G2 ->
    ope_sub G1 ((x, T) :: G2)
| os_keep : forall G1 G2 x T1 T2,
    ope_sub G1 G2 ->
    G2 ⊢ T1 <⦂ T2 ->
    ope_sub ((x, T2) :: G1) ((x, T1) :: G2).
Local Hint Constructors ope_sub.

Section OpeProperties.

  Lemma ope_sub_length : forall G1 G2,
    ope_sub G1 G2 ->
    length G1 <= length G2.
  Proof.
    induction on ope_sub; routine.
  Qed.
  
  Hint Resolve weaken_subty.

  Lemma ope_narrow_var :
    forall G G' x T,
      ope_sub G G' ->
      binds x T G ->
      G' ⊢ trm_var (avar_f x) ⦂ T.
  Proof.
    induction on ope_sub; intros.
    - routine.
    - apply IHope_sub in H.
      reassoc 2 with 1. apply weaken_typing.
      trivial.
    - tidy_up.
      + eapply ty_sub; simpl_env; eauto.
      + apply IHope_sub in H0.
        simpl_env. apply weaken_typing.
        trivial.
  Qed.
  Hint Resolve ope_narrow_var.
  
  Lemma ope_narrow_trm : forall G t T,
      G ⊢ t ⦂ T ->
      forall G',
        ope_sub G G' ->
        G' ⊢ t ⦂ T
  with ope_narrow_subty : forall G T U,
      G ⊢ T <⦂ U ->
      forall G',
        ope_sub G G' ->
        G' ⊢ T <⦂ U.
  Proof.
    - clear ope_narrow_trm.
      induction on typing; intros; eauto.
      + econstructor. cofinite.
        apply H0; simpl; auto.
      + econstructor; auto. cofinite.
        apply H0; simpl; auto.
      + eapply ty_sub; eauto.
        
    - clear ope_narrow_subty.
      induction on subty; intros; eauto.

      eapply st_all. auto.
      cofinite.
      apply H0; simpl; auto.
  Qed.
  Hint Resolve ope_narrow_subty.
  
  Lemma ope_sub_trans : forall G1 G2 G3,
      ope_sub G1 G2 ->
      ope_sub G2 G3 ->
      ope_sub G1 G3.
  Proof.
    intros. gen G1. induction H0; intros; auto.
    invert H1; subst; eauto.
  Qed.

  Lemma ope_sub_refl : forall G,
      ope_sub G G.
  Proof. induction on env; routine. Qed.

  Lemma ope_sub_nil : forall G,
      ope_sub nil G.
  Proof. induction on env; routine. Qed.

  Lemma ope_sub_app_r : forall G G1 G2,
      ope_sub G G2 ->
      ope_sub G (G1 ++ G2).
  Proof. induction G1; routine. Qed.

End OpeProperties.  
