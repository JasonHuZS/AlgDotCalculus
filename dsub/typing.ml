
type __ = Obj.t
let __ = let rec f _ = Obj.repr f in Obj.repr f

type unit0 =
| Tt

type bool =
| True
| False

(** val negb : bool -> bool **)

let negb = function
| True -> False
| False -> True

type nat =
| O
| S of nat

type 'a option =
| Some of 'a
| None

type ('a, 'b) sum =
| Inl of 'a
| Inr of 'b

type ('a, 'b) prod =
| Pair of 'a * 'b

(** val fst : ('a1, 'a2) prod -> 'a1 **)

let fst = function
| Pair (x, _) -> x

(** val snd : ('a1, 'a2) prod -> 'a2 **)

let snd = function
| Pair (_, y) -> y

type 'a list =
| Nil
| Cons of 'a * 'a list

(** val length : 'a1 list -> nat **)

let rec length = function
| Nil -> O
| Cons (_, l') -> S (length l')

(** val app : 'a1 list -> 'a1 list -> 'a1 list **)

let rec app l m =
  match l with
  | Nil -> m
  | Cons (a, l1) -> Cons (a, (app l1 m))

type 'a sig0 = 'a
  (* singleton inductive, whose constructor was exist *)

type ('a, 'p) sigT =
| ExistT of 'a * 'p

(** val projT1 : ('a1, 'a2) sigT -> 'a1 **)

let projT1 = function
| ExistT (a, _) -> a

(** val projT2 : ('a1, 'a2) sigT -> 'a2 **)

let projT2 = function
| ExistT (_, h) -> h

type sumbool =
| Left
| Right

(** val flip : ('a1 -> 'a2 -> 'a3) -> 'a2 -> 'a1 -> 'a3 **)

let flip f x y =
  f y x

module type DecidableType =
 sig
  type t

  val eq_dec : t -> t -> sumbool
 end

module type DecidableTypeOrig =
 sig
  type t

  val eq_dec : t -> t -> sumbool
 end

module type UsualDecidableTypeOrig =
 sig
  type t

  val eq_dec : t -> t -> sumbool
 end

module Nat =
 struct
  (** val max : nat -> nat -> nat **)

  let rec max n m =
    match n with
    | O -> m
    | S n' -> (match m with
               | O -> n
               | S m' -> S (max n' m'))

  (** val eq_dec : nat -> nat -> sumbool **)

  let rec eq_dec n m =
    match n with
    | O -> (match m with
            | O -> Left
            | S _ -> Right)
    | S n0 -> (match m with
               | O -> Right
               | S m0 -> eq_dec n0 m0)
 end

(** val in_dec : ('a1 -> 'a1 -> sumbool) -> 'a1 -> 'a1 list -> sumbool **)

let rec in_dec h a = function
| Nil -> Right
| Cons (y, l0) ->
  let s = h y a in (match s with
                    | Left -> Left
                    | Right -> in_dec h a l0)

(** val rev : 'a1 list -> 'a1 list **)

let rec rev = function
| Nil -> Nil
| Cons (x, l') -> app (rev l') (Cons (x, Nil))

(** val map : ('a1 -> 'a2) -> 'a1 list -> 'a2 list **)

let rec map f = function
| Nil -> Nil
| Cons (a, t0) -> Cons ((f a), (map f t0))

(** val fold_left : ('a1 -> 'a2 -> 'a1) -> 'a2 list -> 'a1 -> 'a1 **)

let rec fold_left f l a0 =
  match l with
  | Nil -> a0
  | Cons (b, t0) -> fold_left f t0 (f a0 b)

(** val fold_right : ('a2 -> 'a1 -> 'a1) -> 'a1 -> 'a2 list -> 'a1 **)

let rec fold_right f a0 = function
| Nil -> a0
| Cons (b, t0) -> f b (fold_right f a0 t0)

(** val le_lt_dec : nat -> nat -> sumbool **)

let rec le_lt_dec n m =
  match n with
  | O -> Left
  | S n0 -> (match m with
             | O -> Right
             | S m0 -> le_lt_dec n0 m0)

module type Coq_DecidableType =
 DecidableTypeOrig

module type UsualDecidableType =
 UsualDecidableTypeOrig

module WFacts_fun =
 functor (E:Coq_DecidableType) ->
 functor (M:sig
  type elt = E.t

  type t

  val empty : t

  val is_empty : t -> bool

  val mem : elt -> t -> bool

  val add : elt -> t -> t

  val singleton : elt -> t

  val remove : elt -> t -> t

  val union : t -> t -> t

  val inter : t -> t -> t

  val diff : t -> t -> t

  val eq_dec : t -> t -> sumbool

  val equal : t -> t -> bool

  val subset : t -> t -> bool

  val fold : (elt -> 'a1 -> 'a1) -> t -> 'a1 -> 'a1

  val for_all : (elt -> bool) -> t -> bool

  val exists_ : (elt -> bool) -> t -> bool

  val filter : (elt -> bool) -> t -> t

  val partition : (elt -> bool) -> t -> (t, t) prod

  val cardinal : t -> nat

  val elements : t -> elt list

  val choose : t -> elt option
 end) ->
 struct
  (** val eqb : E.t -> E.t -> bool **)

  let eqb x y =
    match E.eq_dec x y with
    | Left -> True
    | Right -> False
 end

module WDecide_fun =
 functor (E:Coq_DecidableType) ->
 functor (M:sig
  type elt = E.t

  type t

  val empty : t

  val is_empty : t -> bool

  val mem : elt -> t -> bool

  val add : elt -> t -> t

  val singleton : elt -> t

  val remove : elt -> t -> t

  val union : t -> t -> t

  val inter : t -> t -> t

  val diff : t -> t -> t

  val eq_dec : t -> t -> sumbool

  val equal : t -> t -> bool

  val subset : t -> t -> bool

  val fold : (elt -> 'a1 -> 'a1) -> t -> 'a1 -> 'a1

  val for_all : (elt -> bool) -> t -> bool

  val exists_ : (elt -> bool) -> t -> bool

  val filter : (elt -> bool) -> t -> t

  val partition : (elt -> bool) -> t -> (t, t) prod

  val cardinal : t -> nat

  val elements : t -> elt list

  val choose : t -> elt option
 end) ->
 struct
  module F = WFacts_fun(E)(M)

  module FSetLogicalFacts =
   struct
   end

  module FSetDecideAuxiliary =
   struct
   end

  module FSetDecideTestCases =
   struct
   end
 end

module WProperties_fun =
 functor (E:Coq_DecidableType) ->
 functor (M:sig
  type elt = E.t

  type t

  val empty : t

  val is_empty : t -> bool

  val mem : elt -> t -> bool

  val add : elt -> t -> t

  val singleton : elt -> t

  val remove : elt -> t -> t

  val union : t -> t -> t

  val inter : t -> t -> t

  val diff : t -> t -> t

  val eq_dec : t -> t -> sumbool

  val equal : t -> t -> bool

  val subset : t -> t -> bool

  val fold : (elt -> 'a1 -> 'a1) -> t -> 'a1 -> 'a1

  val for_all : (elt -> bool) -> t -> bool

  val exists_ : (elt -> bool) -> t -> bool

  val filter : (elt -> bool) -> t -> t

  val partition : (elt -> bool) -> t -> (t, t) prod

  val cardinal : t -> nat

  val elements : t -> elt list

  val choose : t -> elt option
 end) ->
 struct
  module Dec = WDecide_fun(E)(M)

  module FM = Dec.F

  (** val coq_In_dec : M.elt -> M.t -> sumbool **)

  let coq_In_dec x s =
    match M.mem x s with
    | True -> Left
    | False -> Right

  (** val of_list : M.elt list -> M.t **)

  let of_list l =
    fold_right M.add M.empty l

  (** val to_list : M.t -> M.elt list **)

  let to_list =
    M.elements

  (** val fold_rec :
      (M.elt -> 'a1 -> 'a1) -> 'a1 -> M.t -> (M.t -> __ -> 'a2) -> (M.elt ->
      'a1 -> M.t -> M.t -> __ -> __ -> __ -> 'a2 -> 'a2) -> 'a2 **)

  let fold_rec f i s pempty pstep =
    let l = rev (M.elements s) in
    let pstep' = fun x a s' s'' x0 -> pstep x a s' s'' __ __ __ x0 in
    let rec f0 l0 pstep'0 s0 =
      match l0 with
      | Nil -> pempty s0 __
      | Cons (y, l1) ->
        pstep'0 y (fold_right f i l1) (of_list l1) s0 __ __ __
          (f0 l1 (fun x a0 s' s'' _ _ _ x0 ->
            pstep'0 x a0 s' s'' __ __ __ x0) (of_list l1))
    in f0 l (fun x a s' s'' _ _ _ x0 -> pstep' x a s' s'' x0) s

  (** val fold_rec_bis :
      (M.elt -> 'a1 -> 'a1) -> 'a1 -> M.t -> (M.t -> M.t -> 'a1 -> __ -> 'a2
      -> 'a2) -> 'a2 -> (M.elt -> 'a1 -> M.t -> __ -> __ -> 'a2 -> 'a2) -> 'a2 **)

  let fold_rec_bis f i s pmorphism pempty pstep =
    fold_rec f i s (fun s' _ -> pmorphism M.empty s' i __ pempty)
      (fun x a s' s'' _ _ _ x0 ->
      pmorphism (M.add x s') s'' (f x a) __ (pstep x a s' __ __ x0))

  (** val fold_rec_nodep :
      (M.elt -> 'a1 -> 'a1) -> 'a1 -> M.t -> 'a2 -> (M.elt -> 'a1 -> __ ->
      'a2 -> 'a2) -> 'a2 **)

  let fold_rec_nodep f i s x x0 =
    fold_rec_bis f i s (fun _ _ _ _ x1 -> x1) x (fun x1 a _ _ _ x2 ->
      x0 x1 a __ x2)

  (** val fold_rec_weak :
      (M.elt -> 'a1 -> 'a1) -> 'a1 -> (M.t -> M.t -> 'a1 -> __ -> 'a2 -> 'a2)
      -> 'a2 -> (M.elt -> 'a1 -> M.t -> __ -> 'a2 -> 'a2) -> M.t -> 'a2 **)

  let fold_rec_weak f i x x0 x1 s =
    fold_rec_bis f i s x x0 (fun x2 a s' _ _ x3 -> x1 x2 a s' __ x3)

  (** val fold_rel :
      (M.elt -> 'a1 -> 'a1) -> (M.elt -> 'a2 -> 'a2) -> 'a1 -> 'a2 -> M.t ->
      'a3 -> (M.elt -> 'a1 -> 'a2 -> __ -> 'a3 -> 'a3) -> 'a3 **)

  let fold_rel f g i j s rempty rstep =
    let l = rev (M.elements s) in
    let rstep' = fun x a b x0 -> rstep x a b __ x0 in
    let rec f0 l0 rstep'0 =
      match l0 with
      | Nil -> rempty
      | Cons (y, l1) ->
        rstep'0 y (fold_right f i l1) (fold_right g j l1) __
          (f0 l1 (fun x a0 b _ x0 -> rstep'0 x a0 b __ x0))
    in f0 l (fun x a b _ x0 -> rstep' x a b x0)

  (** val set_induction :
      (M.t -> __ -> 'a1) -> (M.t -> M.t -> 'a1 -> M.elt -> __ -> __ -> 'a1)
      -> M.t -> 'a1 **)

  let set_induction x x0 s =
    fold_rec (fun _ _ -> Tt) Tt s x (fun x1 _ s' s'' _ _ _ x2 ->
      x0 s' s'' x2 x1 __ __)

  (** val set_induction_bis :
      (M.t -> M.t -> __ -> 'a1 -> 'a1) -> 'a1 -> (M.elt -> M.t -> __ -> 'a1
      -> 'a1) -> M.t -> 'a1 **)

  let set_induction_bis x x0 x1 s =
    fold_rec_bis (fun _ _ -> Tt) Tt s (fun s0 s' _ _ x2 -> x s0 s' __ x2) x0
      (fun x2 _ s' _ _ x3 -> x1 x2 s' __ x3)

  (** val cardinal_inv_2 : M.t -> nat -> M.elt **)

  let cardinal_inv_2 s _ =
    let l = M.elements s in
    (match l with
     | Nil -> assert false (* absurd case *)
     | Cons (e, _) -> e)

  (** val cardinal_inv_2b : M.t -> M.elt **)

  let cardinal_inv_2b s =
    let n = M.cardinal s in
    let x = fun x -> cardinal_inv_2 s x in
    (match n with
     | O -> assert false (* absurd case *)
     | S n0 -> x n0)
 end

module MakeRaw =
 functor (X:DecidableType) ->
 struct
  type elt = X.t

  type t = elt list

  (** val empty : t **)

  let empty =
    Nil

  (** val is_empty : t -> bool **)

  let is_empty = function
  | Nil -> True
  | Cons (_, _) -> False

  (** val mem : elt -> t -> bool **)

  let rec mem x = function
  | Nil -> False
  | Cons (y, l) -> (match X.eq_dec x y with
                    | Left -> True
                    | Right -> mem x l)

  (** val add : elt -> t -> t **)

  let rec add x s = match s with
  | Nil -> Cons (x, Nil)
  | Cons (y, l) ->
    (match X.eq_dec x y with
     | Left -> s
     | Right -> Cons (y, (add x l)))

  (** val singleton : elt -> t **)

  let singleton x =
    Cons (x, Nil)

  (** val remove : elt -> t -> t **)

  let rec remove x = function
  | Nil -> Nil
  | Cons (y, l) ->
    (match X.eq_dec x y with
     | Left -> l
     | Right -> Cons (y, (remove x l)))

  (** val fold : (elt -> 'a1 -> 'a1) -> t -> 'a1 -> 'a1 **)

  let fold f =
    fold_left (flip f)

  (** val union : t -> t -> t **)

  let union s =
    fold add s

  (** val diff : t -> t -> t **)

  let diff s s' =
    fold remove s' s

  (** val inter : t -> t -> t **)

  let inter s s' =
    fold (fun x s0 -> match mem x s' with
                      | True -> add x s0
                      | False -> s0) s Nil

  (** val subset : t -> t -> bool **)

  let subset s s' =
    is_empty (diff s s')

  (** val equal : t -> t -> bool **)

  let equal s s' =
    match subset s s' with
    | True -> subset s' s
    | False -> False

  (** val filter : (elt -> bool) -> t -> t **)

  let rec filter f = function
  | Nil -> Nil
  | Cons (x, l) ->
    (match f x with
     | True -> Cons (x, (filter f l))
     | False -> filter f l)

  (** val for_all : (elt -> bool) -> t -> bool **)

  let rec for_all f = function
  | Nil -> True
  | Cons (x, l) -> (match f x with
                    | True -> for_all f l
                    | False -> False)

  (** val exists_ : (elt -> bool) -> t -> bool **)

  let rec exists_ f = function
  | Nil -> False
  | Cons (x, l) -> (match f x with
                    | True -> True
                    | False -> exists_ f l)

  (** val partition : (elt -> bool) -> t -> (t, t) prod **)

  let rec partition f = function
  | Nil -> Pair (Nil, Nil)
  | Cons (x, l) ->
    let Pair (s1, s2) = partition f l in
    (match f x with
     | True -> Pair ((Cons (x, s1)), s2)
     | False -> Pair (s1, (Cons (x, s2))))

  (** val cardinal : t -> nat **)

  let cardinal =
    length

  (** val elements : t -> elt list **)

  let elements s =
    s

  (** val choose : t -> elt option **)

  let choose = function
  | Nil -> None
  | Cons (x, _) -> Some x

  (** val isok : elt list -> bool **)

  let rec isok = function
  | Nil -> True
  | Cons (a, l0) ->
    (match negb (mem a l0) with
     | True -> isok l0
     | False -> False)
 end

module Make =
 functor (X:DecidableType) ->
 struct
  module Raw = MakeRaw(X)

  module E =
   struct
    type t = X.t

    (** val eq_dec : t -> t -> sumbool **)

    let eq_dec =
      X.eq_dec
   end

  type elt = X.t

  type t_ = Raw.t
    (* singleton inductive, whose constructor was Mkt *)

  (** val this : t_ -> Raw.t **)

  let this t0 =
    t0

  type t = t_

  (** val mem : elt -> t -> bool **)

  let mem x s =
    Raw.mem x (this s)

  (** val add : elt -> t -> t **)

  let add x s =
    Raw.add x (this s)

  (** val remove : elt -> t -> t **)

  let remove x s =
    Raw.remove x (this s)

  (** val singleton : elt -> t **)

  let singleton =
    Raw.singleton

  (** val union : t -> t -> t **)

  let union s s' =
    Raw.union (this s) (this s')

  (** val inter : t -> t -> t **)

  let inter s s' =
    Raw.inter (this s) (this s')

  (** val diff : t -> t -> t **)

  let diff s s' =
    Raw.diff (this s) (this s')

  (** val equal : t -> t -> bool **)

  let equal s s' =
    Raw.equal (this s) (this s')

  (** val subset : t -> t -> bool **)

  let subset s s' =
    Raw.subset (this s) (this s')

  (** val empty : t **)

  let empty =
    Raw.empty

  (** val is_empty : t -> bool **)

  let is_empty s =
    Raw.is_empty (this s)

  (** val elements : t -> elt list **)

  let elements s =
    Raw.elements (this s)

  (** val choose : t -> elt option **)

  let choose s =
    Raw.choose (this s)

  (** val fold : (elt -> 'a1 -> 'a1) -> t -> 'a1 -> 'a1 **)

  let fold f s =
    Raw.fold f (this s)

  (** val cardinal : t -> nat **)

  let cardinal s =
    Raw.cardinal (this s)

  (** val filter : (elt -> bool) -> t -> t **)

  let filter f s =
    Raw.filter f (this s)

  (** val for_all : (elt -> bool) -> t -> bool **)

  let for_all f s =
    Raw.for_all f (this s)

  (** val exists_ : (elt -> bool) -> t -> bool **)

  let exists_ f s =
    Raw.exists_ f (this s)

  (** val partition : (elt -> bool) -> t -> (t, t) prod **)

  let partition f s =
    let p = Raw.partition f (this s) in Pair ((fst p), (snd p))

  (** val eq_dec : t -> t -> sumbool **)

  let eq_dec s0 s'0 =
    let b = Raw.equal s0 s'0 in (match b with
                                 | True -> Left
                                 | False -> Right)
 end

module Coq_WDecide_fun =
 functor (E:Coq_DecidableType) ->
 functor (M:sig
  type elt = E.t

  type t

  val empty : t

  val is_empty : t -> bool

  val mem : elt -> t -> bool

  val add : elt -> t -> t

  val singleton : elt -> t

  val remove : elt -> t -> t

  val union : t -> t -> t

  val inter : t -> t -> t

  val diff : t -> t -> t

  val eq_dec : t -> t -> sumbool

  val equal : t -> t -> bool

  val subset : t -> t -> bool

  val fold : (elt -> 'a1 -> 'a1) -> t -> 'a1 -> 'a1

  val for_all : (elt -> bool) -> t -> bool

  val exists_ : (elt -> bool) -> t -> bool

  val filter : (elt -> bool) -> t -> t

  val partition : (elt -> bool) -> t -> (t, t) prod

  val cardinal : t -> nat

  val elements : t -> elt list

  val choose : t -> elt option
 end) ->
 struct
  module F = WFacts_fun(E)(M)

  module FSetLogicalFacts =
   struct
   end

  module FSetDecideAuxiliary =
   struct
   end

  module FSetDecideTestCases =
   struct
   end
 end

type 'a eqDec = 'a -> 'a -> sumbool

(** val nat_eq_eqdec : nat eqDec **)

let nat_eq_eqdec =
  Nat.eq_dec

type 'a eqDec_eq = 'a -> 'a -> sumbool

(** val eq_dec0 : 'a1 eqDec_eq -> 'a1 -> 'a1 -> sumbool **)

let eq_dec0 eqDec_eq0 =
  eqDec_eq0

(** val eqDec_eq_of_EqDec : 'a1 eqDec -> 'a1 eqDec_eq **)

let eqDec_eq_of_EqDec h =
  h

module Coq_Make =
 functor (X:UsualDecidableType) ->
 functor (KeySet:sig
  type elt = X.t

  type t

  val empty : t

  val is_empty : t -> bool

  val mem : elt -> t -> bool

  val add : elt -> t -> t

  val singleton : elt -> t

  val remove : elt -> t -> t

  val union : t -> t -> t

  val inter : t -> t -> t

  val diff : t -> t -> t

  val eq_dec : t -> t -> sumbool

  val equal : t -> t -> bool

  val subset : t -> t -> bool

  val fold : (elt -> 'a1 -> 'a1) -> t -> 'a1 -> 'a1

  val for_all : (elt -> bool) -> t -> bool

  val exists_ : (elt -> bool) -> t -> bool

  val filter : (elt -> bool) -> t -> t

  val partition : (elt -> bool) -> t -> (t, t) prod

  val cardinal : t -> nat

  val elements : t -> elt list

  val choose : t -> elt option
 end) ->
 struct
  (** val coq_EqDec_of_X : X.t eqDec **)

  let coq_EqDec_of_X =
    X.eq_dec

  (** val coq_EqDec_eq_of_X : X.t eqDec_eq **)

  let coq_EqDec_eq_of_X =
    eqDec_eq_of_EqDec coq_EqDec_of_X

  module D = Coq_WDecide_fun(X)(KeySet)

  module KeySetProperties = WProperties_fun(X)(KeySet)

  module KeySetFacts = WFacts_fun(X)(KeySet)

  (** val one : 'a1 -> 'a1 list **)

  let one item =
    Cons (item, Nil)

  (** val dom : (X.t, 'a1) prod list -> KeySet.t **)

  let rec dom = function
  | Nil -> KeySet.empty
  | Cons (p, e') -> let Pair (x, _) = p in KeySet.add x (dom e')

  (** val get : X.t -> (X.t, 'a1) prod list -> 'a1 option **)

  let rec get x = function
  | Nil -> None
  | Cons (p, f) ->
    let Pair (y, c) = p in
    (match eq_dec0 coq_EqDec_eq_of_X x y with
     | Left -> Some c
     | Right -> get x f)

  (** val map :
      ('a1 -> 'a2) -> (X.t, 'a1) prod list -> (X.t, 'a2) prod list **)

  let map f e =
    map (fun b -> let Pair (x, a) = b in Pair (x, (f a))) e

  (** val alist_ind :
      'a2 -> (X.t -> 'a1 -> (X.t, 'a1) prod list -> 'a2 -> 'a2) -> (X.t, 'a1)
      prod list -> 'a2 **)

  let rec alist_ind x x0 = function
  | Nil -> x
  | Cons (y, l) ->
    let iHxs = alist_ind x x0 l in let Pair (x1, a) = y in x0 x1 a l iHxs

  (** val binds_dec :
      X.t -> 'a1 -> (X.t, 'a1) prod list -> ('a1 -> 'a1 -> sumbool) -> sumbool **)

  let binds_dec x a e x0 =
    in_dec (fun x1 y ->
      let Pair (x2, x3) = x1 in
      let Pair (t0, a1) = y in
      (match X.eq_dec x2 t0 with
       | Left -> x0 x3 a1
       | Right -> Right)) (Pair (x, a)) e

  (** val binds_lookup : X.t -> (X.t, 'a1) prod list -> ('a1, __) sum **)

  let binds_lookup x e =
    alist_ind (Inr __) (fun x1 a1 _ x0 ->
      match x0 with
      | Inl s -> Inl s
      | Inr _ ->
        let s = X.eq_dec x x1 in
        (match s with
         | Left -> Inl a1
         | Right -> Inr __)) e
 end

module Coq0_Make =
 functor (X:Coq_DecidableType) ->
 struct
  module E =
   struct
    type t = X.t

    (** val eq_dec : t -> t -> sumbool **)

    let eq_dec =
      X.eq_dec
   end

  module X' =
   struct
    type t = X.t

    (** val eq_dec : t -> t -> sumbool **)

    let eq_dec =
      X.eq_dec
   end

  module MSet = Make(X')

  type elt = X.t

  type t = MSet.t

  (** val empty : t **)

  let empty =
    MSet.empty

  (** val is_empty : t -> bool **)

  let is_empty =
    MSet.is_empty

  (** val mem : elt -> t -> bool **)

  let mem =
    MSet.mem

  (** val add : elt -> t -> t **)

  let add =
    MSet.add

  (** val singleton : elt -> t **)

  let singleton =
    MSet.singleton

  (** val remove : elt -> t -> t **)

  let remove =
    MSet.remove

  (** val union : t -> t -> t **)

  let union =
    MSet.union

  (** val inter : t -> t -> t **)

  let inter =
    MSet.inter

  (** val diff : t -> t -> t **)

  let diff =
    MSet.diff

  (** val eq_dec : t -> t -> sumbool **)

  let eq_dec =
    MSet.eq_dec

  (** val equal : t -> t -> bool **)

  let equal =
    MSet.equal

  (** val subset : t -> t -> bool **)

  let subset =
    MSet.subset

  (** val fold : (elt -> 'a1 -> 'a1) -> t -> 'a1 -> 'a1 **)

  let fold =
    MSet.fold

  (** val for_all : (elt -> bool) -> t -> bool **)

  let for_all =
    MSet.for_all

  (** val exists_ : (elt -> bool) -> t -> bool **)

  let exists_ =
    MSet.exists_

  (** val filter : (elt -> bool) -> t -> t **)

  let filter =
    MSet.filter

  (** val partition : (elt -> bool) -> t -> (t, t) prod **)

  let partition =
    MSet.partition

  (** val cardinal : t -> nat **)

  let cardinal =
    MSet.cardinal

  (** val elements : t -> elt list **)

  let elements =
    MSet.elements

  (** val choose : t -> elt option **)

  let choose =
    MSet.choose

  module MF =
   struct
    (** val eqb : X.t -> X.t -> bool **)

    let eqb x y =
      match MSet.E.eq_dec x y with
      | Left -> True
      | Right -> False
   end
 end

module type ATOM =
 sig
  type atom

  type t = atom

  val eq_dec : atom -> atom -> sumbool

  val atom_fresh_for_list : t list -> atom

  val fresh : atom list -> atom

  val nat_of : atom -> nat
 end

module Atom =
 struct
  type atom = nat

  type t = atom

  (** val eq_dec : nat -> nat -> sumbool **)

  let eq_dec =
    Nat.eq_dec

  (** val nat_list_max : nat list -> nat **)

  let rec nat_list_max = function
  | Nil -> O
  | Cons (y, l0) -> Nat.max y (nat_list_max l0)

  (** val atom_fresh_for_list : nat list -> nat **)

  let atom_fresh_for_list xs =
    let s = nat_list_max xs in S s

  (** val fresh : atom list -> nat **)

  let fresh =
    atom_fresh_for_list

  (** val fresh_not_in : __ **)

  let fresh_not_in =
    __

  (** val nat_of : atom -> atom **)

  let nat_of x =
    x

  type eq = __

  (** val eq_equiv : __ **)

  let eq_equiv =
    __

  (** val eq_refl : __ **)

  let eq_refl =
    __

  (** val eq_sym : __ **)

  let eq_sym =
    __

  (** val eq_trans : __ **)

  let eq_trans =
    __
 end

module AtomSetImpl = Coq0_Make(Atom)

(** val atom_fresh : AtomSetImpl.t -> Atom.atom **)

let atom_fresh l =
  Atom.atom_fresh_for_list (AtomSetImpl.elements l)

module EnvImpl = Coq_Make(Atom)(AtomSetImpl)

type 'a canOpen =
  nat -> Atom.atom -> 'a -> 'a
  (* singleton inductive, whose constructor was Build_CanOpen *)

(** val open_rec : 'a1 canOpen -> nat -> Atom.atom -> 'a1 -> 'a1 **)

let open_rec canOpen0 =
  canOpen0

type 'a canClose =
  nat -> Atom.atom -> 'a -> 'a
  (* singleton inductive, whose constructor was Build_CanClose *)

(** val close_rec : 'a1 canClose -> nat -> Atom.atom -> 'a1 -> 'a1 **)

let close_rec canClose0 =
  canClose0

type typ_label = nat
  (* singleton inductive, whose constructor was typ_lab *)

type trm_label = nat
  (* singleton inductive, whose constructor was trm_lab *)

(** val eqDecTypLab : typ_label eqDec_eq **)

let eqDecTypLab =
  Nat.eq_dec

(** val eqDecTrmLab : trm_label eqDec_eq **)

let eqDecTrmLab =
  Nat.eq_dec

type label =
| Label_typ of typ_label
| Label_trm of trm_label

module Label =
 struct
  type t = label

  (** val eq_dec : t -> t -> sumbool **)

  let eq_dec x y =
    match x with
    | Label_typ x0 ->
      (match y with
       | Label_typ t1 -> eq_dec0 eqDecTypLab x0 t1
       | Label_trm _ -> Right)
    | Label_trm x0 ->
      (match y with
       | Label_typ _ -> Right
       | Label_trm t1 -> eq_dec0 eqDecTrmLab x0 t1)
 end

module LabelSetImpl = Coq0_Make(Label)

module LabelAssocList = Coq_Make(Label)(LabelSetImpl)

type 'm monad = { ret : (__ -> __ -> 'm);
                  bind : (__ -> __ -> 'm -> (__ -> 'm) -> 'm) }

(** val ret : 'a1 monad -> 'a2 -> 'a1 **)

let ret monad0 x =
  let { ret = ret0; bind = _ } = monad0 in Obj.magic ret0 __ x

(** val bind : 'a1 monad -> 'a1 -> ('a2 -> 'a1) -> 'a1 **)

let bind monad0 x x0 =
  let { ret = _; bind = bind0 } = monad0 in Obj.magic bind0 __ __ x x0

type 'm pMonad = { pret : (__ -> __ -> __ -> 'm);
                   pbind : (__ -> __ -> __ -> 'm -> (__ -> 'm) -> 'm) }

type ('m, 'x) monP = __

(** val pbind :
    'a1 pMonad -> ('a1, 'a3) monP -> 'a1 -> ('a2 -> 'a1) -> 'a1 **)

let pbind pMonad0 pu x x0 =
  let { pret = _; pbind = pbind0 } = pMonad0 in Obj.magic pbind0 __ __ pu x x0

(** val pMonad_Monad : 'a1 monad -> 'a1 pMonad **)

let pMonad_Monad m =
  { pret = (fun _ -> Obj.magic (fun _ x -> ret m x)); pbind = (fun _ _ ->
    Obj.magic (fun _ c f -> bind m c f)) }

type err =
| Err of __
| Err_cons of err * __

type 'a result =
| Res_ret of 'a
| Err_ret of err

(** val resultMonad : __ result monad **)

let resultMonad =
  { ret = (fun _ x -> Res_ret x); bind = (fun _ _ retA bmap ->
    match retA with
    | Res_ret a ->
      let r = bmap a in
      (match r with
       | Res_ret b -> Res_ret b
       | Err_ret e -> Err_ret (Err_cons (e, a)))
    | Err_ret e -> Err_ret e) }

(** val eret : 'a2 -> 'a1 result **)

let eret e =
  Err_ret (Err (Obj.magic e))

(** val get_witness :
    Atom.atom -> (Atom.atom, 'a1) prod list -> 'a1 -> ((Atom.atom, 'a1) prod
    list, (Atom.atom, 'a1) prod list) sigT **)

let rec get_witness v l r =
  match l with
  | Nil -> assert false (* absurd case *)
  | Cons (y, l0) ->
    let Pair (a, a0) = y in
    let s = eq_dec0 EnvImpl.coq_EqDec_eq_of_X v a in
    (match s with
     | Left -> ExistT (Nil, l0)
     | Right ->
       let h = get_witness v l0 r in
       let ExistT (h0, x) = h in
       ExistT ((app (LabelAssocList.one (Pair (a, a0))) h0), x))

(** val get_for_sure : (Atom.atom, 'a1) prod list -> Atom.atom -> 'a1 **)

let rec get_for_sure l x =
  match l with
  | Nil -> assert false (* absurd case *)
  | Cons (y, l0) ->
    let Pair (a, t0) = y in
    let s = eq_dec0 EnvImpl.coq_EqDec_eq_of_X x a in
    (match s with
     | Left -> t0
     | Right -> get_for_sure l0 x)

type avar =
| Avar_b of nat
| Avar_f of Atom.atom

(** val eqAvar : avar eqDec_eq **)

let eqAvar x y =
  match x with
  | Avar_b x0 ->
    (match y with
     | Avar_b n0 -> Nat.eq_dec x0 n0
     | Avar_f _ -> Right)
  | Avar_f x0 ->
    (match y with
     | Avar_b _ -> Right
     | Avar_f a0 -> Atom.eq_dec x0 a0)

(** val fv_values :
    ('a1 -> AtomSetImpl.t) -> (Atom.atom, 'a1) prod list -> AtomSetImpl.t **)

let fv_values f l =
  fold_right (fun b a -> AtomSetImpl.union a (let Pair (_, t0) = b in f t0))
    AtomSetImpl.empty l

type typ =
| Typ_top
| Typ_bot
| Typ_sel of avar
| Typ_all of typ * typ
| Typ_bnd of typ * typ

(** val eqDecTyp : typ eqDec_eq **)

let rec eqDecTyp t0 x0 =
  match t0 with
  | Typ_top -> (match x0 with
                | Typ_top -> Left
                | _ -> Right)
  | Typ_bot -> (match x0 with
                | Typ_bot -> Left
                | _ -> Right)
  | Typ_sel a ->
    (match x0 with
     | Typ_sel a0 -> eq_dec0 eqAvar a a0
     | _ -> Right)
  | Typ_all (t1, t2) ->
    (match x0 with
     | Typ_all (t3, t4) ->
       (match eqDecTyp t1 t3 with
        | Left -> eqDecTyp t2 t4
        | Right -> Right)
     | _ -> Right)
  | Typ_bnd (t1, t2) ->
    (match x0 with
     | Typ_bnd (t3, t4) ->
       (match eqDecTyp t1 t3 with
        | Left -> eqDecTyp t2 t4
        | Right -> Right)
     | _ -> Right)

type trm =
| Trm_var of avar
| Trm_val of val0
| Trm_app of avar * avar
| Trm_let of trm * trm
and val0 =
| Val_bnd of typ
| Val_lam of typ * trm

(** val open_rec_avar : nat -> Atom.atom -> avar -> avar **)

let open_rec_avar k u = function
| Avar_b i ->
  (match eq_dec0 (eqDec_eq_of_EqDec nat_eq_eqdec) k i with
   | Left -> Avar_f u
   | Right -> Avar_b i)
| Avar_f x -> Avar_f x

(** val open_rec_typ : nat -> Atom.atom -> typ -> typ **)

let rec open_rec_typ k u = function
| Typ_sel x -> Typ_sel (open_rec_avar k u x)
| Typ_all (t1, u0) ->
  Typ_all ((open_rec_typ k u t1), (open_rec_typ (S k) u u0))
| Typ_bnd (t1, u0) -> Typ_bnd ((open_rec_typ k u t1), (open_rec_typ k u u0))
| x -> x

(** val open_rec_trm : nat -> Atom.atom -> trm -> trm **)

let rec open_rec_trm k u = function
| Trm_var x -> Trm_var (open_rec_avar k u x)
| Trm_val v -> Trm_val (open_rec_val k u v)
| Trm_app (x, y) -> Trm_app ((open_rec_avar k u x), (open_rec_avar k u y))
| Trm_let (s, t1) -> Trm_let ((open_rec_trm k u s), (open_rec_trm (S k) u t1))

(** val open_rec_val : nat -> Atom.atom -> val0 -> val0 **)

and open_rec_val k u = function
| Val_bnd t0 -> Val_bnd (open_rec_typ k u t0)
| Val_lam (t0, t1) ->
  Val_lam ((open_rec_typ k u t0), (open_rec_trm (S k) u t1))

(** val openTyp : typ canOpen **)

let openTyp =
  open_rec_typ

(** val openTrm : trm canOpen **)

let openTrm =
  open_rec_trm

(** val close_rec_avar : nat -> Atom.atom -> avar -> avar **)

let close_rec_avar k u = function
| Avar_b n -> Avar_b n
| Avar_f x ->
  (match eq_dec0 EnvImpl.coq_EqDec_eq_of_X x u with
   | Left -> Avar_b k
   | Right -> Avar_f x)

(** val close_rec_typ : nat -> Atom.atom -> typ -> typ **)

let rec close_rec_typ k u = function
| Typ_sel x -> Typ_sel (close_rec_avar k u x)
| Typ_all (t1, u0) ->
  Typ_all ((close_rec_typ k u t1), (close_rec_typ (S k) u u0))
| Typ_bnd (t1, u0) -> Typ_bnd ((close_rec_typ k u t1), (close_rec_typ k u u0))
| x -> x

(** val closeTyp : typ canClose **)

let closeTyp =
  close_rec_typ

(** val fv_avar : avar -> AtomSetImpl.t **)

let fv_avar = function
| Avar_b _ -> AtomSetImpl.empty
| Avar_f x -> AtomSetImpl.singleton x

(** val fv_typ : typ -> AtomSetImpl.t **)

let rec fv_typ = function
| Typ_sel a -> fv_avar a
| Typ_all (t1, t2) -> AtomSetImpl.union (fv_typ t1) (fv_typ t2)
| Typ_bnd (t1, t2) -> AtomSetImpl.union (fv_typ t1) (fv_typ t2)
| _ -> AtomSetImpl.empty

(** val fv_trm : trm -> AtomSetImpl.t **)

let rec fv_trm = function
| Trm_var a -> fv_avar a
| Trm_val v -> fv_val v
| Trm_app (x, y) -> AtomSetImpl.union (fv_avar x) (fv_avar y)
| Trm_let (t1, t2) -> AtomSetImpl.union (fv_trm t1) (fv_trm t2)

(** val fv_val : val0 -> AtomSetImpl.t **)

and fv_val = function
| Val_bnd t0 -> fv_typ t0
| Val_lam (t0, t1) -> AtomSetImpl.union (fv_typ t0) (fv_trm t1)

type expoStuck =
| Stuck_err of Atom.atom * typ

(** val exposure_impl :
    (Atom.atom, typ) prod list -> typ -> (typ, ((Atom.atom, typ) prod list,
    (Atom.atom, typ) prod list) sigT) sigT result **)

let exposure_impl g s =
  let rec fix_F_sub x =
    let g0 = projT1 x in
    let s0 = projT2 x in
    let exposure_impl0 = fun g1 s1 -> fix_F_sub (ExistT (g1, s1)) in
    (fun _ _ _ ->
    match s0 with
    | Typ_sel a ->
      (match a with
       | Avar_b _ -> assert false (* absurd case *)
       | Avar_f a0 ->
         let s1 = get_for_sure g0 a0 in
         let h2 = get_witness a0 g0 s1 in
         let ExistT (h3, x0) = h2 in
         let recur = exposure_impl0 x0 s1 __ __ __ in
         pbind (pMonad_Monad (Obj.magic resultMonad)) (Obj.magic __) recur
           (fun x1 ->
           let ExistT (x2, x3) = x1 in
           let ExistT (x4, x5) = x3 in
           (match x2 with
            | Typ_bot ->
              Res_ret (ExistT (Typ_bot, (ExistT (h3,
                (app (EnvImpl.one (Pair (a0, s1))) (app x4 x5))))))
            | Typ_bnd (_, t2) ->
              let recur0 = exposure_impl0 x0 t2 __ __ __ in
              pbind (pMonad_Monad (Obj.magic resultMonad)) (Obj.magic __)
                recur0 (fun x6 ->
                let ExistT (x7, _) = x6 in
                Res_ret (ExistT (x7, (ExistT (h3, (Cons ((Pair (a0, s1)),
                (app x4 x5))))))))
            | x6 -> eret (Stuck_err (a0, x6)))))
    | x0 -> Res_ret (ExistT (x0, (ExistT (Nil, g0)))))
  in fix_F_sub (ExistT (g, s)) __ __ __

(** val promo_demo_func_obligation_1 :
    (Atom.atom, typ) prod list -> Atom.atom -> typ -> ((Atom.atom, typ) prod
    list -> Atom.atom -> typ -> __ -> __ -> __ -> __ -> (typ result, typ
    result) prod) -> (typ result, typ result) prod **)

let promo_demo_func_obligation_1 g x s promo_demo0 =
  match s with
  | Typ_sel a ->
    (match a with
     | Avar_b _ -> assert false (* absurd case *)
     | Avar_f a0 ->
       let s0 = eq_dec0 EnvImpl.coq_EqDec_eq_of_X x a0 in
       (match s0 with
        | Left ->
          let s1 = get_for_sure g a0 in
          let h2 = get_witness a0 g s1 in
          let ExistT (_, x0) = h2 in
          let recur = exposure_impl x0 s1 in
          Pair
          ((pbind (pMonad_Monad (Obj.magic resultMonad)) (Obj.magic __)
             (Obj.magic recur) (fun x1 ->
             let ExistT (x2, _) = x1 in
             (match x2 with
              | Typ_bot -> Res_ret Typ_bot
              | Typ_bnd (_, t2) -> Res_ret t2
              | x3 -> eret (Stuck_err (a0, x3))))),
          (pbind (pMonad_Monad (Obj.magic resultMonad)) (Obj.magic __)
            (Obj.magic recur) (fun x1 ->
            let ExistT (x2, _) = x1 in
            (match x2 with
             | Typ_bot -> Res_ret Typ_top
             | Typ_bnd (t1, _) -> Res_ret t1
             | x3 -> eret (Stuck_err (a0, x3))))))
        | Right ->
          Pair ((Res_ret (Typ_sel (Avar_f a0))), (Res_ret (Typ_sel (Avar_f
            a0))))))
  | Typ_all (s1, s2) ->
    let recur = promo_demo0 g x s1 __ __ __ __ in
    let Pair (r, r0) = recur in
    Pair
    ((pbind (pMonad_Monad (Obj.magic resultMonad)) (Obj.magic __) r0
       (fun x0 ->
       let s0 =
         atom_fresh
           (AtomSetImpl.union (AtomSetImpl.singleton x)
             (AtomSetImpl.union (EnvImpl.dom g)
               (AtomSetImpl.union (fv_values fv_typ g)
                 (AtomSetImpl.union (fv_typ s1)
                   (AtomSetImpl.union (fv_typ s2) (fv_typ x0))))))
       in
       let x1 =
         promo_demo0 (app (EnvImpl.one (Pair (s0, x0))) g) x
           (open_rec openTyp O s0 s2) __ __ __ __
       in
       let Pair (r1, _) = x1 in
       pbind (pMonad_Monad (Obj.magic resultMonad)) (Obj.magic __) r1
         (fun x2 -> Res_ret (Typ_all (x0, (close_rec closeTyp O s0 x2)))))),
    (pbind (pMonad_Monad (Obj.magic resultMonad)) (Obj.magic __) r (fun x0 ->
      let s0 =
        atom_fresh
          (AtomSetImpl.union (AtomSetImpl.singleton x)
            (AtomSetImpl.union (EnvImpl.dom g)
              (AtomSetImpl.union (fv_values fv_typ g)
                (AtomSetImpl.union (fv_typ s1)
                  (AtomSetImpl.union (fv_typ s2) (fv_typ x0))))))
      in
      let x1 =
        promo_demo0 (app (EnvImpl.one (Pair (s0, s1))) g) x
          (open_rec openTyp O s0 s2) __ __ __ __
      in
      let Pair (_, r2) = x1 in
      pbind (pMonad_Monad (Obj.magic resultMonad)) (Obj.magic __) r2
        (fun x2 -> Res_ret (Typ_all (x0, (close_rec closeTyp O s0 x2)))))))
  | Typ_bnd (s1, s2) ->
    let x0 = promo_demo0 g x s2 __ __ __ __ in
    let x1 = promo_demo0 g x s1 __ __ __ __ in
    let Pair (r, r0) = x1 in
    let Pair (r1, r2) = x0 in
    Pair
    ((pbind (pMonad_Monad (Obj.magic resultMonad)) (Obj.magic __) r1
       (fun x2 ->
       pbind (pMonad_Monad (Obj.magic resultMonad)) (Obj.magic __) r0
         (fun y -> Res_ret (Typ_bnd (y, x2))))),
    (pbind (pMonad_Monad (Obj.magic resultMonad)) (Obj.magic __) r2
      (fun x2 ->
      pbind (pMonad_Monad (Obj.magic resultMonad)) (Obj.magic __) r (fun y ->
        Res_ret (Typ_bnd (y, x2))))))
  | x0 -> Pair ((Res_ret x0), (Res_ret x0))

(** val promo_demo_func :
    ((Atom.atom, typ) prod list, (Atom.atom, typ) sigT) sigT -> (typ result,
    typ result) prod **)

let promo_demo_func recarg =
  let rec fix_F_sub x =
    let g = projT1 x in
    let x0 = projT1 (projT2 x) in
    let s = projT2 (projT2 x) in
    let promo_demo0 = fun g0 x1 s0 ->
      fix_F_sub (ExistT (g0, (ExistT (x1, s0))))
    in
    (fun _ _ _ ->
    promo_demo_func_obligation_1 g x0 s (fun g0 x1 s0 _ ->
      promo_demo0 g0 x1 s0))
  in fix_F_sub recarg __ __ __

(** val promo_demo :
    (Atom.atom, typ) prod list -> Atom.atom -> typ -> (typ result, typ
    result) prod **)

let promo_demo g x s =
  promo_demo_func (ExistT (g, (ExistT (x, s))))

(** val promotion :
    (Atom.atom, typ) prod list -> Atom.atom -> typ -> typ result **)

let promotion g x s =
  let p = promo_demo g x s in let Pair (r, _) = p in r

type subtypUndec =
| St_undec of typ * typ

type funcInputTypeMismatch =
| St_all_mis of typ * typ

(** val subty_dec : (Atom.atom, typ) prod list -> typ -> typ -> __ result **)

let subty_dec g t0 u =
  let rec fix_F_sub x =
    let g0 = projT1 x in
    let t1 = projT1 (projT2 x) in
    let u0 = projT2 (projT2 x) in
    let subty_dec0 = fun g1 t2 u1 ->
      fix_F_sub (ExistT (g1, (ExistT (t2, u1))))
    in
    (fun _ _ _ _ _ ->
    match t1 with
    | Typ_top ->
      (match u0 with
       | Typ_top -> Res_ret __
       | x0 -> eret (St_undec (Typ_top, x0)))
    | Typ_bot -> Res_ret __
    | Typ_sel a ->
      (match u0 with
       | Typ_top -> Res_ret __
       | Typ_bot -> eret (St_undec ((Typ_sel a), Typ_bot))
       | Typ_sel a0 ->
         (match a0 with
          | Avar_b _ -> assert false (* absurd case *)
          | Avar_f a1 ->
            (match a with
             | Avar_b _ -> assert false (* absurd case *)
             | Avar_f a2 ->
               let s = eq_dec0 EnvImpl.coq_EqDec_eq_of_X a2 a1 in
               (match s with
                | Left -> Res_ret __
                | Right ->
                  let s0 = get_for_sure g0 a2 in
                  let h6 = get_witness a2 g0 s0 in
                  let ExistT (_, x0) = h6 in
                  let h12 = exposure_impl x0 s0 in
                  pbind (pMonad_Monad (Obj.magic resultMonad)) (Obj.magic __)
                    (Obj.magic h12) (fun x1 ->
                    let ExistT (x2, _) = x1 in
                    (match x2 with
                     | Typ_bot -> Res_ret __
                     | Typ_bnd (_, t2) ->
                       let x3 =
                         subty_dec0 g0 t2 (Typ_sel (Avar_f a1)) __ __ __ __ __
                       in
                       pbind (pMonad_Monad (Obj.magic resultMonad))
                         (Obj.magic __) x3 (fun _ -> Res_ret __)
                     | x3 -> eret (Stuck_err (a2, x3)))))))
       | Typ_all (t2, t3) ->
         (match a with
          | Avar_b _ -> assert false (* absurd case *)
          | Avar_f a0 ->
            let s = get_for_sure g0 a0 in
            let h6 = get_witness a0 g0 s in
            let ExistT (_, x0) = h6 in
            let h12 = exposure_impl x0 s in
            pbind (pMonad_Monad (Obj.magic resultMonad)) (Obj.magic __)
              (Obj.magic h12) (fun x1 ->
              let ExistT (x2, _) = x1 in
              (match x2 with
               | Typ_bot -> Res_ret __
               | Typ_bnd (_, t4) ->
                 let x3 = subty_dec0 g0 t4 (Typ_all (t2, t3)) __ __ __ __ __
                 in
                 pbind (pMonad_Monad (Obj.magic resultMonad)) (Obj.magic __)
                   x3 (fun _ -> Res_ret __)
               | x3 -> eret (Stuck_err (a0, x3)))))
       | Typ_bnd (t2, t3) ->
         (match a with
          | Avar_b _ -> assert false (* absurd case *)
          | Avar_f a0 ->
            let s = get_for_sure g0 a0 in
            let h6 = get_witness a0 g0 s in
            let ExistT (_, x0) = h6 in
            let h12 = exposure_impl x0 s in
            pbind (pMonad_Monad (Obj.magic resultMonad)) (Obj.magic __)
              (Obj.magic h12) (fun x1 ->
              let ExistT (x2, _) = x1 in
              (match x2 with
               | Typ_bot -> Res_ret __
               | Typ_bnd (_, t4) ->
                 let x3 = subty_dec0 g0 t4 (Typ_bnd (t2, t3)) __ __ __ __ __
                 in
                 pbind (pMonad_Monad (Obj.magic resultMonad)) (Obj.magic __)
                   x3 (fun _ -> Res_ret __)
               | x3 -> eret (Stuck_err (a0, x3))))))
    | Typ_all (t2, t3) ->
      (match u0 with
       | Typ_top -> Res_ret __
       | Typ_sel a ->
         (match a with
          | Avar_b _ -> assert false (* absurd case *)
          | Avar_f a0 ->
            let s = get_for_sure g0 a0 in
            let h6 = get_witness a0 g0 s in
            let ExistT (_, x0) = h6 in
            let h12 = exposure_impl x0 s in
            pbind (pMonad_Monad (Obj.magic resultMonad)) (Obj.magic __)
              (Obj.magic h12) (fun x1 ->
              let ExistT (x2, _) = x1 in
              (match x2 with
               | Typ_bot -> Res_ret __
               | Typ_bnd (t4, _) ->
                 let x3 = subty_dec0 g0 (Typ_all (t2, t3)) t4 __ __ __ __ __
                 in
                 pbind (pMonad_Monad (Obj.magic resultMonad)) (Obj.magic __)
                   x3 (fun _ -> Res_ret __)
               | x3 -> eret (Stuck_err (a0, x3)))))
       | Typ_all (t4, t5) ->
         let s = eq_dec0 eqDecTyp t2 t4 in
         (match s with
          | Left ->
            let s0 =
              atom_fresh
                (AtomSetImpl.union (EnvImpl.dom g0)
                  (AtomSetImpl.union (fv_values fv_typ g0)
                    (AtomSetImpl.union (fv_typ t3)
                      (AtomSetImpl.union (fv_typ t4) (fv_typ t5)))))
            in
            let x0 =
              subty_dec0 (app (EnvImpl.one (Pair (s0, t4))) g0)
                (open_rec openTyp O s0 t3) (open_rec openTyp O s0 t5) __ __
                __ __ __
            in
            pbind (pMonad_Monad (Obj.magic resultMonad)) (Obj.magic __) x0
              (fun _ -> Res_ret __)
          | Right -> eret (St_all_mis (t2, t4)))
       | x0 -> eret (St_undec ((Typ_all (t2, t3)), x0)))
    | Typ_bnd (t2, t3) ->
      (match u0 with
       | Typ_top -> Res_ret __
       | Typ_sel a ->
         (match a with
          | Avar_b _ -> assert false (* absurd case *)
          | Avar_f a0 ->
            let s = get_for_sure g0 a0 in
            let h6 = get_witness a0 g0 s in
            let ExistT (_, x0) = h6 in
            let h12 = exposure_impl x0 s in
            pbind (pMonad_Monad (Obj.magic resultMonad)) (Obj.magic __)
              (Obj.magic h12) (fun x1 ->
              let ExistT (x2, _) = x1 in
              (match x2 with
               | Typ_bot -> Res_ret __
               | Typ_bnd (t4, _) ->
                 let x3 = subty_dec0 g0 (Typ_bnd (t2, t3)) t4 __ __ __ __ __
                 in
                 pbind (pMonad_Monad (Obj.magic resultMonad)) (Obj.magic __)
                   x3 (fun _ -> Res_ret __)
               | x3 -> eret (Stuck_err (a0, x3)))))
       | Typ_bnd (t4, t5) ->
         let x0 = subty_dec0 g0 t4 t2 __ __ __ __ __ in
         let x1 = subty_dec0 g0 t3 t5 __ __ __ __ __ in
         pbind (pMonad_Monad (Obj.magic resultMonad)) (Obj.magic __) x0
           (fun _ ->
           pbind (pMonad_Monad (Obj.magic resultMonad)) (Obj.magic __) x1
             (fun _ -> Res_ret __))
       | x0 -> eret (St_undec ((Typ_bnd (t2, t3)), x0))))
  in fix_F_sub (ExistT (g, (ExistT (t0, u)))) __ __ __ __ __

type notBoundToFuncTypes =
| Ty_nfunc of Atom.atom * typ

(** val typing_impl : (Atom.atom, typ) prod list -> trm -> typ result **)

let typing_impl g t0 =
  let rec fix_F_sub x =
    let g0 = projT1 x in
    let t1 = projT2 x in
    let typing_impl0 = fun g1 t2 -> fix_F_sub (ExistT (g1, t2)) in
    (fun _ _ _ ->
    match t1 with
    | Trm_var a ->
      (match a with
       | Avar_b _ -> assert false (* absurd case *)
       | Avar_f a0 -> let s = get_for_sure g0 a0 in Res_ret s)
    | Trm_val v ->
      (match v with
       | Val_bnd t2 -> Res_ret (Typ_bnd (t2, t2))
       | Val_lam (t2, t3) ->
         let s =
           atom_fresh
             (AtomSetImpl.union (EnvImpl.dom g0)
               (AtomSetImpl.union (fv_values fv_typ g0)
                 (AtomSetImpl.union (fv_typ t2) (fv_trm t3))))
         in
         let x0 =
           typing_impl0 (app (EnvImpl.one (Pair (s, t2))) g0)
             (open_rec openTrm O s t3) __ __ __
         in
         pbind (pMonad_Monad (Obj.magic resultMonad)) (Obj.magic __) x0
           (fun x1 -> Res_ret (Typ_all (t2, (close_rec closeTyp O s x1)))))
    | Trm_app (a, a0) ->
      (match a with
       | Avar_b _ -> assert false (* absurd case *)
       | Avar_f a1 ->
         (match a0 with
          | Avar_b _ -> assert false (* absurd case *)
          | Avar_f a2 ->
            let s = get_for_sure g0 a1 in
            let h4 = get_witness a1 g0 s in
            let ExistT (_, x0) = h4 in
            let h10 = exposure_impl x0 s in
            pbind (pMonad_Monad (Obj.magic resultMonad)) (Obj.magic __)
              (Obj.magic h10) (fun x1 ->
              let ExistT (x2, _) = x1 in
              let s0 = get_for_sure g0 a2 in
              (match x2 with
               | Typ_bot -> Res_ret (open_rec openTyp O a2 Typ_bot)
               | Typ_all (t2, t3) ->
                 let x3 = subty_dec g0 s0 t2 in
                 pbind (pMonad_Monad (Obj.magic resultMonad)) (Obj.magic __)
                   (Obj.magic x3) (fun _ -> Res_ret
                   (open_rec openTyp O a2 t3))
               | x3 -> eret (Ty_nfunc (a1, x3))))))
    | Trm_let (t2, t3) ->
      let x0 = typing_impl0 g0 t2 __ __ __ in
      pbind (pMonad_Monad (Obj.magic resultMonad)) (Obj.magic __) x0
        (fun x1 ->
        let s =
          atom_fresh
            (AtomSetImpl.union (EnvImpl.dom g0)
              (AtomSetImpl.union (fv_values fv_typ g0)
                (AtomSetImpl.union (fv_typ x1)
                  (AtomSetImpl.union (fv_trm t2) (fv_trm t3)))))
        in
        let x2 =
          typing_impl0 (app (EnvImpl.one (Pair (s, x1))) g0)
            (open_rec openTrm O s t3) __ __ __
        in
        pbind (pMonad_Monad (Obj.magic resultMonad)) (Obj.magic __) x2
          (fun x3 ->
          let x4 = promotion (app (EnvImpl.one (Pair (s, x1))) g0) s x3 in
          pbind (pMonad_Monad (Obj.magic resultMonad)) (Obj.magic __) x4
            (fun x5 -> Res_ret x5))))
  in fix_F_sub (ExistT (g, t0)) __ __ __

(** val typecheck' : trm -> typ result **)

let typecheck' t0 =
  typing_impl Nil t0

type hasFreeVars =
| Has_fv_typ of Atom.atom * typ
| Has_fv_trm of Atom.atom * trm
| Has_fv_val of Atom.atom * val0

(** val check_fv_typ : typ -> __ result **)

let rec check_fv_typ = function
| Typ_sel a ->
  (match a with
   | Avar_b _ -> Res_ret __
   | Avar_f a0 -> eret (Has_fv_typ (a0, (Typ_sel (Avar_f a0)))))
| Typ_all (t1, t2) ->
  let x = check_fv_typ t1 in
  pbind (pMonad_Monad (Obj.magic resultMonad)) (Obj.magic __) x (fun _ ->
    let x0 = check_fv_typ t2 in
    pbind (pMonad_Monad (Obj.magic resultMonad)) (Obj.magic __) x0 (fun _ ->
      Res_ret __))
| Typ_bnd (t1, t2) ->
  let x = check_fv_typ t1 in
  pbind (pMonad_Monad (Obj.magic resultMonad)) (Obj.magic __) x (fun _ ->
    let x0 = check_fv_typ t2 in
    pbind (pMonad_Monad (Obj.magic resultMonad)) (Obj.magic __) x0 (fun _ ->
      Res_ret __))
| _ -> Res_ret __

(** val check_fv_trm : trm -> __ result **)

let rec check_fv_trm = function
| Trm_var a ->
  (match a with
   | Avar_b _ -> Res_ret __
   | Avar_f a0 -> eret (Has_fv_trm (a0, (Trm_var (Avar_f a0)))))
| Trm_val v ->
  let x = check_fv_val v in
  pbind (pMonad_Monad (Obj.magic resultMonad)) (Obj.magic __) x (fun _ ->
    Res_ret __)
| Trm_app (a, a0) ->
  (match a with
   | Avar_b n ->
     (match a0 with
      | Avar_b _ -> Res_ret __
      | Avar_f a1 ->
        eret (Has_fv_trm (a1, (Trm_app ((Avar_b n), (Avar_f a1))))))
   | Avar_f a1 ->
     (match a0 with
      | Avar_b n ->
        eret (Has_fv_trm (a1, (Trm_app ((Avar_f a1), (Avar_b n)))))
      | Avar_f a2 ->
        eret (Has_fv_trm (a2, (Trm_app ((Avar_f a1), (Avar_f a2)))))))
| Trm_let (t0_1, t0_2) ->
  let x = check_fv_trm t0_1 in
  pbind (pMonad_Monad (Obj.magic resultMonad)) (Obj.magic __) x (fun _ ->
    let x0 = check_fv_trm t0_2 in
    pbind (pMonad_Monad (Obj.magic resultMonad)) (Obj.magic __) x0 (fun _ ->
      Res_ret __))

(** val check_fv_val : val0 -> __ result **)

and check_fv_val = function
| Val_bnd t0 ->
  let x = check_fv_typ t0 in
  pbind (pMonad_Monad (Obj.magic resultMonad)) (Obj.magic __) x (fun _ ->
    Res_ret __)
| Val_lam (t0, t1) ->
  let x = check_fv_typ t0 in
  pbind (pMonad_Monad (Obj.magic resultMonad)) (Obj.magic __) x (fun _ ->
    let x0 = check_fv_trm t1 in
    pbind (pMonad_Monad (Obj.magic resultMonad)) (Obj.magic __) x0 (fun _ ->
      Res_ret __))

type notClosed =
| Nc_at_typ of nat * typ * nat
| Nc_at_trm of nat * trm * nat
| Nc_at_val of nat * val0 * nat

(** val check_lc_at_typ : nat -> typ -> __ result **)

let rec check_lc_at_typ n = function
| Typ_sel a ->
  (match a with
   | Avar_b n0 ->
     let s = le_lt_dec (S n0) n in
     (match s with
      | Left -> Res_ret __
      | Right -> eret (Nc_at_typ (n, (Typ_sel (Avar_b n0)), (S n0))))
   | Avar_f _ -> Res_ret __)
| Typ_all (t1, t2) ->
  let x = check_lc_at_typ n t1 in
  pbind (pMonad_Monad (Obj.magic resultMonad)) (Obj.magic __) x (fun _ ->
    let x0 = check_lc_at_typ (S n) t2 in
    pbind (pMonad_Monad (Obj.magic resultMonad)) (Obj.magic __) x0 (fun _ ->
      Res_ret __))
| Typ_bnd (t1, t2) ->
  let x = check_lc_at_typ n t1 in
  pbind (pMonad_Monad (Obj.magic resultMonad)) (Obj.magic __) x (fun _ ->
    let x0 = check_lc_at_typ n t2 in
    pbind (pMonad_Monad (Obj.magic resultMonad)) (Obj.magic __) x0 (fun _ ->
      Res_ret __))
| _ -> Res_ret __

(** val check_lc_at_trm : nat -> trm -> __ result **)

let rec check_lc_at_trm n = function
| Trm_var a ->
  (match a with
   | Avar_b n0 ->
     let s = le_lt_dec (S n0) n in
     (match s with
      | Left -> Res_ret __
      | Right -> eret (Nc_at_trm (n, (Trm_var (Avar_b n0)), (S n0))))
   | Avar_f _ -> Res_ret __)
| Trm_val v ->
  let x = check_lc_at_val n v in
  pbind (pMonad_Monad (Obj.magic resultMonad)) (Obj.magic __) x (fun _ ->
    Res_ret __)
| Trm_app (a, a0) ->
  (match a with
   | Avar_b n0 ->
     (match a0 with
      | Avar_b n1 ->
        let s = le_lt_dec (S n1) n in
        (match s with
         | Left ->
           let s0 = le_lt_dec (S n0) n in
           (match s0 with
            | Left -> Res_ret __
            | Right ->
              eret (Nc_at_trm (n, (Trm_app ((Avar_b n0), (Avar_b n1))), (S
                n0))))
         | Right ->
           let s0 = le_lt_dec (S n0) n in
           (match s0 with
            | Left ->
              eret (Nc_at_trm (n, (Trm_app ((Avar_b n0), (Avar_b n1))), (S
                n1)))
            | Right ->
              eret (Nc_at_trm (n, (Trm_app ((Avar_b n0), (Avar_b n1))),
                (Nat.max (S n0) (S n1))))))
      | Avar_f a1 ->
        let s = le_lt_dec (S n0) n in
        (match s with
         | Left -> Res_ret __
         | Right ->
           eret (Nc_at_trm (n, (Trm_app ((Avar_b n0), (Avar_f a1))), (S n0)))))
   | Avar_f a1 ->
     (match a0 with
      | Avar_b n0 ->
        let s = le_lt_dec (S n0) n in
        (match s with
         | Left -> Res_ret __
         | Right ->
           eret (Nc_at_trm (n, (Trm_app ((Avar_f a1), (Avar_b n0))), (S n0))))
      | Avar_f _ -> Res_ret __))
| Trm_let (t0_1, t0_2) ->
  let x = check_lc_at_trm n t0_1 in
  pbind (pMonad_Monad (Obj.magic resultMonad)) (Obj.magic __) x (fun _ ->
    let x0 = check_lc_at_trm (S n) t0_2 in
    pbind (pMonad_Monad (Obj.magic resultMonad)) (Obj.magic __) x0 (fun _ ->
      Res_ret __))

(** val check_lc_at_val : nat -> val0 -> __ result **)

and check_lc_at_val n = function
| Val_bnd t0 ->
  let x = check_lc_at_typ n t0 in
  pbind (pMonad_Monad (Obj.magic resultMonad)) (Obj.magic __) x (fun _ ->
    Res_ret __)
| Val_lam (t0, t1) ->
  let x = check_lc_at_typ n t0 in
  pbind (pMonad_Monad (Obj.magic resultMonad)) (Obj.magic __) x (fun _ ->
    let x0 = check_lc_at_trm (S n) t1 in
    pbind (pMonad_Monad (Obj.magic resultMonad)) (Obj.magic __) x0 (fun _ ->
      Res_ret __))

(** val typecheck : trm -> typ result **)

let typecheck t0 =
  pbind (pMonad_Monad (Obj.magic resultMonad)) (Obj.magic __)
    (Obj.magic check_fv_trm t0) (fun _ ->
    pbind (pMonad_Monad (Obj.magic resultMonad)) (Obj.magic __)
      (Obj.magic check_lc_at_trm O t0) (fun _ -> typecheck' t0))
