Set Implicit Arguments.

Require Export StdSetup.
Require Export VarDef.

Inductive tvar : Set :=
| tvar_b : nat -> tvar
| tvar_f : var -> tvar.
Hint Constructors tvar.

Instance EqTvar : EqDec_eq tvar := { }.
Proof. decide equality. apply Nat.eq_dec. Defined.

Inductive ftyp : Set :=
| ftyp_top : ftyp
| ftyp_bot : ftyp
| ftyp_var : tvar -> ftyp
| ftyp_fun : ftyp -> ftyp -> ftyp (* trm function *)
| ftyp_all : ftyp -> ftyp -> ftyp. (* typ function *)
Hint Constructors ftyp.

Inductive lc_tvar_at : nat -> tvar -> Prop :=
| lc_ab : forall {n m}, n < m -> lc_tvar_at m (tvar_b n)
| lc_af : forall {n x}, lc_tvar_at n (tvar_f x).

Instance LcTvar : LC tvar := { lc_at := lc_tvar_at }.

Inductive lc_ftyp_at : nat -> ftyp -> Prop :=
| lc_ftyp_top : forall {n}, lc_ftyp_at n ftyp_top
| lc_ftyp_bot : forall {n}, lc_ftyp_at n ftyp_bot
| lc_ftyp_var : forall {n X}, lc_at n X -> lc_ftyp_at n (ftyp_var X)

| lc_ftyp_fun : forall {n T U}, lc_ftyp_at n T -> lc_ftyp_at n U ->
                          lc_ftyp_at n (ftyp_fun T U)
| lc_ftyp_all : forall {n T U}, lc_ftyp_at n T -> lc_ftyp_at (S n) U ->
                           lc_ftyp_at n (ftyp_all T U).

Instance LcFtyp : LC ftyp := { lc_at := lc_ftyp_at }.
Hint Constructors lc_tvar_at lc_ftyp_at.

Definition open_rec_tvar (k : nat) (u : var) (a : tvar) : tvar :=
  match a with
  | tvar_b i => if k == i then tvar_f u else tvar_b i
  | tvar_f x => tvar_f x
  end.

Fixpoint open_rec_ftyp (k : nat) (u : var) (T : ftyp) : ftyp :=
  match T with
  | ftyp_top => ftyp_top
  | ftyp_bot => ftyp_bot
  | ftyp_var X => ftyp_var (open_rec_tvar k u X)
  | ftyp_fun T U => ftyp_fun (open_rec_ftyp k u T) (open_rec_ftyp k u U)
  | ftyp_all T U => ftyp_all (open_rec_ftyp k u T) $ open_rec_ftyp (S k) u U
  end.

Instance OpenTvar : CanOpen tvar := { open_rec := open_rec_tvar }.
Instance OpenFtyp : CanOpen ftyp := { open_rec := open_rec_ftyp }.

Definition close_rec_tvar (k : nat) (u : var) (a : tvar) : tvar :=
    match a with
    | tvar_b n => tvar_b n
    | tvar_f x => if x == u then tvar_b k else tvar_f x
    end.

Fixpoint close_rec_ftyp (k : nat) (u : var) (T : ftyp) : ftyp :=
  match T with
  | ftyp_top => ftyp_top
  | ftyp_bot => ftyp_bot
  | ftyp_var X => ftyp_var (close_rec_tvar k u X)
  | ftyp_fun T U => ftyp_fun (close_rec_ftyp k u T) (close_rec_ftyp k u U)
  | ftyp_all T U => ftyp_all (close_rec_ftyp k u T) $ close_rec_ftyp (S k) u U
  end.

Instance CloseTvar : CanClose tvar := { close_rec := close_rec_tvar }.
Instance CloseFtyp : CanClose ftyp := { close_rec := close_rec_ftyp }.

Definition subst_tvar (z u: var) (a : tvar) : tvar :=
  match a with
  | tvar_b i as a => a
  | tvar_f x => tvar_f $ if x == z then u else x
  end.

Fixpoint subst_ftyp (z u: var) (T : ftyp) : ftyp := 
  match T with
  | ftyp_top => ftyp_top
  | ftyp_bot => ftyp_bot
  | ftyp_var X => ftyp_var (subst_tvar z u X)
  | ftyp_fun T U => ftyp_fun (subst_ftyp z u T) (subst_ftyp z u U)
  | ftyp_all T U => ftyp_all (subst_ftyp z u T) $ subst_ftyp z u  U
  end.

Instance SubstTvar : CanSubst tvar := { substi := subst_tvar }.
Instance SubstFtyp : CanSubst ftyp := { substi := subst_ftyp }.

Notation fenv := (list (var * ftyp)).

Definition fv_tvar (v : tvar) : vars :=
  match v with
  | tvar_b _ => {}
  | tvar_f x => {{ x }}
  end.
  
Fixpoint fv_ftyp (T : ftyp) : vars :=
  match T with
  | ftyp_top | ftyp_bot => empty
  | ftyp_var X => fv_tvar X
  | ftyp_fun T U => fv_ftyp T \u fv_ftyp U
  | ftyp_all T U => fv_ftyp T \u fv_ftyp U
  end.

Instance FvTvar : HasFv tvar := { fv := fv_tvar }.
Instance FvFtyp : HasFv ftyp := { fv := fv_ftyp }.

Local Notation "a ~~ b" := (one (a , b)) (at level 50).

Inductive fsubty : fenv -> ftyp -> ftyp -> Prop :=
| fst_top : forall G T, fsubty G T ftyp_top
| fst_bot : forall G T, fsubty G ftyp_bot T
| fst_vrefl : forall G X, fsubty G (ftyp_var X) (ftyp_var X)
| fst_bind : forall G x T U, binds x T G -> fsubty G T U ->
                        fsubty G (ftyp_var $ tvar_f x) U
| fst_fun : forall G S1 S2 T1 T2,
      fsubty G T1 S1 ->
      fsubty G S2 T2 ->
      fsubty G (ftyp_fun S1 S2) (ftyp_fun T1 T2)
| fst_all : forall L G S1 S2 T1 T2,
    fsubty G T1 S1 ->
    (forall x, x \notin L ->
          fsubty (x ~~ T1 ++ G) (open x S2) (open x T2)) ->
    fsubty G (ftyp_all S1 S2) (ftyp_all T1 T2).
Hint Constructors fsubty.

Notation "G ⊢F S <⦂ U" := (fsubty G S U) (at level 90).
