Set Implicit Arguments.

Require Export StdSetup.
Require Export VarDef.

From Equations Require Export Equations.

Inductive tvar : Set :=
| tvar_b : nat -> tvar
| tvar_f : var -> tvar.
Hint Constructors tvar.

Instance EqTvar : EqDec_eq tvar := { }.
Proof. decide equality. apply Nat.eq_dec. Defined.

Inductive ftyp : Set :=
| ftyp_top : ftyp
| ftyp_var : tvar -> ftyp
| ftyp_all : ftyp -> ftyp -> ftyp. (* typ function *)
Hint Constructors ftyp.

Inductive lc_tvar_at : nat -> tvar -> Prop :=
| lc_ab : forall {n m}, n < m -> lc_tvar_at m (tvar_b n)
| lc_af : forall {n x}, lc_tvar_at n (tvar_f x).

Instance LcTvar : LC tvar := { lc_at := lc_tvar_at }.

Inductive lc_ftyp_at : nat -> ftyp -> Prop :=
| lc_ftyp_top : forall {n}, lc_ftyp_at n ftyp_top
| lc_ftyp_var : forall {n X}, lc_at n X -> lc_ftyp_at n (ftyp_var X)
| lc_ftyp_all : forall {n T U}, lc_ftyp_at n T -> lc_ftyp_at (S n) U ->
                           lc_ftyp_at n (ftyp_all T U).

Instance LcFtyp : LC ftyp := { lc_at := lc_ftyp_at }.
Hint Constructors lc_tvar_at lc_ftyp_at.

Definition open_rec_tvar (k : nat) (u : var) (a : tvar) : tvar :=
  match a with
  | tvar_b i => if k == i then tvar_f u else tvar_b i
  | tvar_f x => tvar_f x
  end.

Fixpoint open_rec_ftyp (k : nat) (u : var) (T : ftyp) : ftyp :=
  match T with
  | ftyp_top => ftyp_top
  | ftyp_var X => ftyp_var (open_rec_tvar k u X)
  | ftyp_all T U => ftyp_all (open_rec_ftyp k u T) $ open_rec_ftyp (S k) u U
  end.

Instance OpenTvar : CanOpen tvar := { open_rec := open_rec_tvar }.
Instance OpenFtyp : CanOpen ftyp := { open_rec := open_rec_ftyp }.

Definition close_rec_tvar (k : nat) (u : var) (a : tvar) : tvar :=
    match a with
    | tvar_b n => tvar_b n
    | tvar_f x => if x == u then tvar_b k else tvar_f x
    end.

Fixpoint close_rec_ftyp (k : nat) (u : var) (T : ftyp) : ftyp :=
  match T with
  | ftyp_top => ftyp_top
  | ftyp_var X => ftyp_var (close_rec_tvar k u X)
  | ftyp_all T U => ftyp_all (close_rec_ftyp k u T) $ close_rec_ftyp (S k) u U
  end.

Instance CloseTvar : CanClose tvar := { close_rec := close_rec_tvar }.
Instance CloseFtyp : CanClose ftyp := { close_rec := close_rec_ftyp }.

Definition subst_tvar (z u: var) (a : tvar) : tvar :=
  match a with
  | tvar_b i as a => a
  | tvar_f x => tvar_f $ if x == z then u else x
  end.

Fixpoint subst_ftyp (z u: var) (T : ftyp) : ftyp := 
  match T with
  | ftyp_top => ftyp_top
  | ftyp_var X => ftyp_var (subst_tvar z u X)
  | ftyp_all T U => ftyp_all (subst_ftyp z u T) $ subst_ftyp z u  U
  end.

Instance SubstTvar : CanSubst tvar := { substi := subst_tvar }.
Instance SubstFtyp : CanSubst ftyp := { substi := subst_ftyp }.

Notation fenv := (list (var * ftyp)).

Definition fv_tvar (v : tvar) : vars :=
  match v with
  | tvar_b _ => {}
  | tvar_f x => {{ x }}
  end.
  
Fixpoint fv_ftyp (T : ftyp) : vars :=
  match T with
  | ftyp_top => empty
  | ftyp_var X => fv_tvar X
  | ftyp_all T U => fv_ftyp T \u fv_ftyp U
  end.

Instance FvTvar : HasFv tvar := { fv := fv_tvar }.
Instance FvFtyp : HasFv ftyp := { fv := fv_ftyp }.

Instance FvAssoc {T} `{HasFv T} : HasFv (list (var * T)) :=
  { fv := fun e => dom e `union` fv_values fv e }.

Lemma fv_union : forall {T} `{HasFv T} (e1 e2 : list (var * T)),
    fv (e1 ++ e2) [=] fv e1 `union` fv e2.
Proof.
  intros; simpl. 
  apply AtomSetProperties.subset_antisym;
    set solve.
Qed.

Hint Rewrite -> fv_union : meta_ext.

Inductive fmsubty : fenv -> ftyp -> ftyp -> Prop :=
| fst_top : forall G T, fmsubty G T ftyp_top
| fst_vrefl : forall G X, fmsubty G (ftyp_var X) (ftyp_var X)
| fst_bind : forall G x T U, binds x T G -> fmsubty G T U ->
                        fmsubty G (ftyp_var $ tvar_f x) U
| fst_all : forall L G S1 S2 T1 T2,
    fmsubty G T1 S1 ->
    (forall x, x \notin L ->
          fmsubty (x ~ T1 ++ G) (open x S2) (open x T2)) ->
    fmsubty G (ftyp_all S1 S2) (ftyp_all T1 T2).
Hint Constructors fmsubty.
Notation "G '⊢F-' S <⦂ U" := (fmsubty G S U) (at level 70, S at level 79).

Inductive cotyp : Set :=
| ct_top : cotyp
| ct_all : contyp -> contyp -> cotyp
with
contyp : Set :=
| cot_var : tvar -> contyp
| cot_all : cotyp -> contyp.
Hint Constructors cotyp contyp.

Fixpoint open_rec_cotyp (n : nat) (x : var) (T : cotyp) : cotyp :=
  match T with
  | ct_top => ct_top
  | ct_all T1 T2 => ct_all (open_rec_contyp n x T1) (open_rec_contyp (S n) x T2)
  end
with
open_rec_contyp (n : nat) (x : var) (T : contyp) : contyp :=
  match T with
  | cot_var y => cot_var (open_rec n x y)
  | cot_all T' => cot_all (open_rec_cotyp (S n) x T')
  end.

Instance OpenCotyp : CanOpen cotyp := { open_rec := open_rec_cotyp }.
Instance OpenContyp : CanOpen contyp := { open_rec := open_rec_contyp }.

Fixpoint close_rec_cotyp (n : nat) (x : var) (T : cotyp) : cotyp :=
  match T with
  | ct_top => ct_top
  | ct_all T1 T2 => ct_all (close_rec_contyp n x T1) (close_rec_contyp (S n) x T2)
  end
with
close_rec_contyp (n : nat) (x : var) (T : contyp) : contyp :=
  match T with
  | cot_var y => cot_var (close_rec n x y)
  | cot_all T' => cot_all (close_rec_cotyp (S n) x T')
  end.

Instance CloseCotyp : CanClose cotyp := { close_rec := close_rec_cotyp }.
Instance CloseContyp : CanClose contyp := { close_rec := close_rec_contyp }.

Fixpoint subst_cotyp (z u : var) (T : cotyp) : cotyp :=
  match T with
  | ct_top => ct_top
  | ct_all T1 T2 => ct_all (subst_contyp z u T1) (subst_contyp z u T2)
  end
with
subst_contyp (z u : var) (T : contyp) : contyp :=
  match T with
  | cot_var y => cot_var (substi z u y)
  | cot_all T' => cot_all (subst_cotyp z u T')
  end.

Instance SubstCotyp : CanSubst cotyp := { substi := subst_cotyp }.
Instance SubstContyp : CanSubst contyp := { substi := subst_contyp }.

Fixpoint fv_cotyp (T : cotyp) : vars :=
  match T with
  | ct_top => empty
  | ct_all T1 T2 => fv_contyp T1 `union` fv_contyp T2
  end
with
fv_contyp (T : contyp) : vars :=
  match T with
  | cot_var x => fv x
  | cot_all T' => fv_cotyp T'
  end.

Instance FvCotyp : HasFv cotyp := { fv := fv_cotyp }.
Instance FvContyp : HasFv contyp := { fv := fv_contyp }.

Inductive lc_cotyp_at : nat -> cotyp -> Prop :=
| lc_ct_top : forall n, lc_cotyp_at n ct_top
| lc_ct_all : forall n T1 T2,
    lc_contyp_at n T1 -> lc_contyp_at (S n) T2 ->
    lc_cotyp_at n (ct_all T1 T2)
with
lc_contyp_at : nat -> contyp -> Prop :=
| lc_cot_var : forall n x, lc_at n x -> lc_contyp_at n (cot_var x)
| lc_cot_all : forall n T, lc_cotyp_at n T -> lc_contyp_at n (cot_all T).
Hint Constructors lc_cotyp_at lc_contyp_at.

Instance LcCotyp : LC cotyp := { lc_at := lc_cotyp_at }.
Instance LcContyp : LC contyp := { lc_at := lc_contyp_at }.

Notation nenv := (list (var * contyp)).

Inductive fdsubty : nenv -> contyp -> cotyp -> Prop :=
| fd_top : forall G T, fdsubty G T ct_top
| fd_var : forall G x T T1 T2,
    binds x T G -> fdsubty G T (ct_all T1 T2) ->
    fdsubty G (cot_var (tvar_f x)) (ct_all T1 T2)
| fd_all : forall L G T T1 T2,
    (forall x, x `notin` L -> fdsubty (x ~ T1 ++ G) (open x T2) (open x T)) ->
    fdsubty G (cot_all T) (ct_all T1 T2).
Hint Constructors fdsubty.
Notation "G '⊢FD' S <⦂ U" := (fdsubty G S U) (at level 70, S at level 79).

Ltac gather_atoms ::=
  let A := gather_atoms_with (fun x : atoms => x) in
  let B := gather_atoms_with (fun x : atom => singleton x) in
  let C := gather_atoms_with (fun x : fenv => fv x) in
  let D := gather_atoms_with fv_tvar in
  let E := gather_atoms_with fv_ftyp in
  let F := gather_atoms_with fv_cotyp in
  let G := gather_atoms_with fv_contyp in
  let H := gather_atoms_with (fun x : nenv => fv x) in
  constr:(A `union` B `union` C `union` D `union` E `union` F `union` G `union` H).

Fixpoint embed_co (T : cotyp) : ftyp :=
  match T with
  | ct_top => ftyp_top
  | ct_all T1 T2 => ftyp_all (embed_cont T1) (ftyp_all (embed_cont T2)
                                                      (ftyp_var (tvar_b 0)))
  end
with
embed_cont (T : contyp) : ftyp :=
  match T with
  | cot_var x => ftyp_var x
  | cot_all T => ftyp_all ftyp_top (ftyp_all (embed_co T) (ftyp_var (tvar_b 0)))
  end.

Definition embed_env (G : nenv) : fenv := map embed_cont G.
Arguments embed_env G/.

Lemma embed_open_rec_comm_co : forall n x T,
    open_rec n x (embed_co T) = embed_co (open_rec n x T)
with embed_open_rec_comm_cont : forall n x T,
    open_rec n x (embed_cont T) = embed_cont (open_rec n x T).
Proof.
  - clear embed_open_rec_comm_co.
    induction on cotyp; routine.
  - clear embed_open_rec_comm_cont.
    induction on contyp; routine.
Qed.

Theorem fdsubty_to_fmsubty : forall G T U,
    G ⊢FD T <⦂ U ->
    embed_env G ⊢F- embed_cont T <⦂ embed_co U.
Proof.
  induction on fdsubty; eroutine.

  do 2 (econstructor; routine).
  fold_cls. rewrite embed_open_rec_comm_co.
  rewrite embed_open_rec_comm_cont.
  auto.
Qed.

Inductive covar : ftyp -> Set :=
| cv_top : covar ftyp_top
| cv_all : forall T1 T2,
    contra T1 -> contra T2 ->
    covar (ftyp_all T1 (ftyp_all T2 (ftyp_var (tvar_b 0))))
with
contra : ftyp -> Set :=
| cnt_var : forall x, contra (ftyp_var x)
| cnt_all : forall T,
    covar T ->
    contra (ftyp_all ftyp_top (ftyp_all T (ftyp_var (tvar_b 0)))).
Hint Constructors covar contra.

Derive Signature for covar.
Derive Signature for contra.
Derive NoConfusion for ftyp.

Equations covar_conv {T : ftyp} (coT : covar T) : cotyp :=
  {
    covar_conv cv_top := ct_top;
    covar_conv (cv_all contT1 contT2) := ct_all (cont_conv contT1) (cont_conv contT2)
  }
where cont_conv {T : ftyp} (contT : contra T) : contyp :=
  {
    cont_conv (cnt_var x) := cot_var x;
    cont_conv (cnt_all coT) := cot_all (covar_conv coT)
  }.

Lemma covar_open_rec : forall n x T,
    covar T ->
    covar (open_rec n x T)
with contra_open_rec : forall n x T,
    contra T ->
    contra (open_rec n x T).
Proof.
  - clear covar_open_rec.
    induction on covar; routine.
  - clear contra_open_rec.
    induction on contra; routine.
Defined.

Lemma covar_conv_open_rec_comm : forall n x T (coT : covar T),
    open_rec n x (covar_conv coT) = covar_conv (covar_open_rec n x coT)
with contra_conv_open_rec_comm : forall n x T (conT : contra T),
    open_rec n x (cont_conv conT) = cont_conv (contra_open_rec n x conT).
Proof.
  - clear covar_conv_open_rec_comm.
    induction on covar; simpl.
    + simp covar_conv.
    + simp covar_conv. simpl.
      fold_cls.
      rewrite 2 contra_conv_open_rec_comm.
      trivial.
  - clear contra_conv_open_rec_comm.
    induction on contra; simpl; trivial.
    fold_cls. rewrite covar_conv_open_rec_comm.
    trivial.
Qed.

Lemma embed_co_covar_conv_id : forall T (coT : covar T),
    embed_co (covar_conv coT) = T
with 
embed_cont_cont_conv_id : forall T (conT : contra T),
    embed_cont (cont_conv conT) = T. 
Proof.
  - clear embed_co_covar_conv_id.
    induction on covar; routine.
    simp covar_conv. simpl.
    rewrite 2 embed_cont_cont_conv_id. trivial.
  - clear embed_cont_cont_conv_id.
    induction on contra; routine.
Qed.

Lemma embed_co_covar : forall T,
    covar (embed_co T)
with embed_cont_contra : forall T,
    contra (embed_cont T).
Proof.
  - clear embed_co_covar.
    induction on cotyp; routine.
  - clear embed_cont_contra.
    induction on contyp; routine.
Defined.

Lemma covar_conv_embed_co_id : forall T,
    covar_conv (embed_co_covar T) = T
with cont_conv_embed_cont_id : forall T,
    cont_conv (embed_cont_contra T) = T.
Proof.
  - clear covar_conv_embed_co_id.
    induction on cotyp; routine.
    simp covar_conv.
    rewrite 2 cont_conv_embed_cont_id.
    trivial.
  - clear cont_conv_embed_cont_id.
    induction on contyp; routine.
Qed.

Section InductiveHyp.

  Lemma derive_eq : forall x G' G T (conT : contra T),
    G' = embed_env G ->
    x ~ T ++ G' = embed_env (x ~ cont_conv conT ++ G).
  Proof.
    routine. rewrite embed_cont_cont_conv_id. trivial.
  Qed.
  
  Variable P : forall G' T U G (conT : contra T) (coU : covar U), G' = embed_env G -> Prop.
  Variable Htop : forall G' T G (conT : contra T) (eq : G' = embed_env G),
      P G conT cv_top eq.
  Variable Hbinds : forall (G' : fenv) (x : atom) (T U : ftyp)
                      G (conT : contra T) (coU : covar U) (eq : G' = embed_env G),
      binds x T G' -> G' ⊢F- T <⦂ U -> P G conT coU eq ->
      P G (cnt_var (tvar_f x)) coU eq. 

  Variable Hall : forall (L : atoms) (G' : fenv) (S1 T1 T2 : ftyp)
                    (coS1 : covar S1) (conT1 : contra T1) (conT2 : contra T2)
                    G (eq : G' = embed_env G),
      (forall x : atom, x `notin` L -> [(x, T1)] ++ G' ⊢F- open x T2 <⦂ open x S1) ->
      (forall x (Hnotin : x `notin` L),
          P (x ~ cont_conv conT1 ++ G)
            (contra_open_rec 0 x conT2) (covar_open_rec 0 x coS1)
            (derive_eq x G conT1 eq)) ->
      P G (cnt_all coS1) (cv_all conT1 conT2) eq.

  Fixpoint fmsubty_img_ind G' T U
           (H : G' ⊢F- T <⦂ U) {struct H} :
    forall G (conT : contra T) (coU : covar U) (eq : G' = embed_env G),
      P G conT coU eq.
  Proof.
    destruct H; intros; subst.
    - dependent inversion coU. apply Htop.
    - inversion coU.
    - dependent inversion conT; subst.
      destruct coU; auto.

      pose proof H. simpl in H.
      apply binds_map_3 in H. destruct_conjs; subst.

      eapply Hbinds; try eassumption.
      instantiate (1 := embed_cont_contra H).
      eapply fmsubty_img_ind; trivial.
    - dependent inversion conT;
        dependent inversion coU; subst.

      eapply Hall; try cofinite; intros;
        specialize (H0 x ltac:(auto));
        invert H0; auto.
  Qed.

End InductiveHyp.

Lemma covar_conv_covar : forall T (coT : covar (embed_co T)),
    covar_conv coT = T
with cont_conv_contra : forall T (conT : contra (embed_cont T)),
    cont_conv conT = T.
Proof.
  - clear covar_conv_covar.
    induction on cotyp; routine;
      dependent inversion coT; routine.
    simp covar_conv. rewrite 2 cont_conv_contra.
    trivial.
  - clear cont_conv_contra.
    induction on contyp; routine;
      dependent inversion conT; routine.
Qed.

Theorem fmsubty_to_fdsubty_gen : forall G' T U,
    G' ⊢F- T <⦂ U ->
  forall G (conT : contra T) (coU : covar U),
    G' = embed_env G ->
    G ⊢FD cont_conv conT <⦂ covar_conv coU.
Proof.
  apply fmsubty_img_ind; intros; subst; auto.
  - destruct coU; auto.
    simp covar_conv.
    econstructor; eauto.
    simpl in *. apply binds_map_3 in H. tidy_up.
    rewrite cont_conv_contra. trivial.
  - simp covar_conv.
    econstructor; cofinite.
    rewrite covar_conv_open_rec_comm.
    rewrite contra_conv_open_rec_comm.
    auto.
Qed.    

Theorem fmsubty_to_fdsubty : forall G T U,
    embed_env G ⊢F- embed_cont T <⦂ embed_co U ->
    G ⊢FD T <⦂ U.
Proof.
  intros. rewrite <- (cont_conv_embed_cont_id T).
  rewrite <- covar_conv_embed_co_id.
  eapply fmsubty_to_fdsubty_gen; eauto.
Qed.

Theorem fmsubty_equiv_fdsubty : forall G T U,
    embed_env G ⊢F- embed_cont T <⦂ embed_co U <->
    G ⊢FD T <⦂ U.
Proof.
  split; intros.
  - apply fmsubty_to_fdsubty. trivial.
  - apply fdsubty_to_fmsubty. trivial.
Qed.
