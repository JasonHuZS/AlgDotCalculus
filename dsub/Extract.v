Require Import TypeCheck.
Require Import StdSetup.

Extraction Inline exposure_impl_func.
Extraction Inline TypeCheck.exposure_impl_func_obligation_1.
Extraction Inline TypeCheck.exposure_impl_func_obligation_2.

Extraction Inline subty_dec_func.
Extraction Inline TypeCheck.subty_dec_func_obligation_1.
Extraction Inline TypeCheck.subty_dec_func_obligation_2.

Extraction Inline typing_impl_func.
Extraction Inline TypeCheck.typing_impl_func_obligation_2.
Extraction Inline TypeCheck.typing_impl_func_obligation_1.

Extraction "typing.ml" typecheck.

Extraction Language Haskell.

(* Extract Inductive nat => Int [ "0" "(1+)" ] "(\fO fS n -> if n == 0 then fO () else fS (n - 1))". *)

Extraction "Typing.hs" typecheck.