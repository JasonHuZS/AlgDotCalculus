Set Implicit Arguments.

Require Import Coq.Lists.List.
Require Import PeanoNat.
Require Import FunInd.

Require Import Arith.Wf_nat.
Require Import Recdef.
Require Import Omega.

Notation "f $ x" := ((f) (x)) (at level 68, right associativity, only parsing).

Inductive typ : Set :=
| bol : typ
| arr : typ -> typ -> typ.

Definition env := list typ.

Fixpoint lookup {T : Type} (l : list T) (n : nat) : option T :=
  match l with
  | nil => None
  | cons x l' => if Nat.eq_dec (length l) n then Some x else lookup l' n
  end.

Inductive trm : Set :=
| var : nat -> trm
| tru : trm
| fals : trm
| lam : typ -> trm -> trm
| app : trm -> trm -> trm.

Inductive typing : env -> trm -> typ -> Set :=
| ty_var : forall G n T, lookup G n = Some T -> typing G (var n) T
| ty_tru : forall G, typing G tru bol
| ty_fals : forall G, typing G fals bol
| ty_lam : forall G T U t, typing (T :: G) t U ->
                      typing G (lam T t) (arr T U)
| ty_app : forall G T U x y, typing G x (arr T U) ->
                        typing G y T ->
                        typing G (app x y) U.

Definition type_eq (T U : typ) : {T = U} + {T <> U}.
Proof. decide equality. Defined.

Definition calc_typ (G : env) (t : trm) : option {T : typ & typing G t T}.
Proof.
  revert G. induction t; intros.
  - destruct (lookup G n) eqn:?.
    + refine (Some $ existT _ _ _).
      constructor. eassumption.
    + exact None.
  - apply Some. repeat econstructor.
  - apply Some. repeat econstructor.
  - rename t into T.
    specialize (IHt $ T :: G).
    destruct IHt.
    + destruct s.
      refine (Some $ existT _ _ _).
      constructor. eassumption.
    + exact None.
  - destruct (IHt1 G).
    + destruct s. destruct x.
      * exact None.
      * destruct (IHt2 G).
        -- destruct s. destruct (type_eq x x1).
           ++ subst. refine (Some $ existT _ _ _).
              econstructor; eassumption.
           ++ exact None.
        -- exact None.
    + exact None.
Defined.

Inductive val : Set :=
| t_val : val
| f_val : val
| lam_val : list val -> typ -> trm -> val.

Definition venv := list val.

Inductive valty : val -> typ -> Set :=
| t_vty : valty t_val bol
| f_vty : valty f_val bol
| lam_vty : forall G ve T t U,
    typing G (lam T t) (arr T U) ->
    econ ve G ->
    valty (lam_val ve T t) (arr T U)
with
econ : venv -> env -> Set :=
| econ_nil : econ nil nil
| econ_cons : forall ve G v T,
    valty v T -> econ ve G -> econ (v :: ve) (T :: G).

Inductive eval : venv -> trm -> val -> Set :=
| ev_t : forall e, eval e tru t_val
| ev_f : forall e, eval e fals f_val
| ev_lam : forall e T t, eval e (lam T t) (lam_val e T t)
| ev_var : forall e n v, lookup e n = Some v ->
                    eval e (var n) v
| ev_app : forall e x y e' Tx tx vy v,
    eval e x (lam_val e' Tx tx) -> eval e y vy ->
    eval (vy :: e') tx v ->
    eval e (app x y ) v.


Lemma econ_length : forall e G, econ e G -> length e = length G.
Proof.
  induction 1; intros; simpl in *; trivial.
  rewrite IHecon. trivial.
Defined.

Definition econ_lookup : forall e G n T,
    econ e G ->
    lookup G n = Some T ->
    { v : val & @sig (valty v T) $ fun _ => lookup e n = Some v }.
Proof.
  induction 1; intros.
  - simpl in H. inversion H.
  - cbn [lookup] in *.
    destruct (Nat.eq_dec $ length (T0 :: G));
      destruct (Nat.eq_dec $ length (v :: ve)).
    + inversion H0. subst. econstructor. constructor.
      eassumption. trivial.
    + apply econ_length in H. simpl in *. rewrite H in n0.
      congruence.
    + apply econ_length in H. simpl in *. rewrite H in e.
      congruence.
    + apply IHecon. eassumption.
Defined.

Require Import Coq.Program.Equality.

Definition trm_measure (t : trm): nat := 0.

Require Coq.Program.Wf.

Require Export Coq.Program.Tactics.

Ltac ref := refine (Some $ existT _ _ $ pair _ _).

Program Fixpoint run
        G e (t : trm) T (ty : typing G t T) (con : econ e G)
        {measure (trm_measure t)}
  : option { v : val & prod (valty v T) $ eval e t v } := _.
Next Obligation.
  destruct ty.
  - destruct (econ_lookup n con e0).
    destruct s.
    ref.
    + eassumption.
    + constructor. assumption.
  - ref; constructor.
  - ref; constructor.
  - ref; econstructor.
    constructor. eassumption. eassumption.
  - enough (trm_measure x < trm_measure (app x y)).
    enough (trm_measure y < trm_measure (app x y)).
    pose proof (run _ _ _ _ ty1 con H).
    destruct H1; [ | exact None].
    destruct_conjs.
    dependent destruction v.
    dependent destruction t0.
    
    pose proof (run _ _ _ _ ty2 con H0).
    destruct H1; [ | exact None].
    destruct_conjs.

    enough (trm_measure t < trm_measure (app x y)).
    pose proof (run _ _ t U t1 (econ_cons v e0) H1).
    destruct H2; [ | exact None].
    destruct_conjs.
    ref.
    + eassumption.
    + econstructor; try eassumption.
    + 
      Show Proof.
