module SystemF where

open import Relation.Binary.PropositionalEquality

open import Data.Nat
open import Data.List
open import Data.List.Any
open import Data.List.Membership.Propositional

open import Data.Bool
open import Data.Unit
open import Data.Product

open import Function

open import Level using (Lift)

data Type : Set where
  top    : Type
  var    : ℕ → Type
  _⟶_    : Type → Type → Type
  Π<:_∙_ : Type → Type → Type

TSubEnv = List (Set × Type)

data WfType : TSubEnv → Type → Set where
  top-wf : ∀ {env} → WfType env top
  var-wf : ∀ {env n} → n < length env → WfType env (var n)
  ⟶-wf   : ∀ {env S T} → WfType env S → WfType env T → WfType env (S ⟶ T)
  Π-wf   : ∀ {env S T} → WfType env S → WfType (S ∷ env) T → WfType env (Π<: S ∙ T)

WfTSubEnv : TSubEnv → Set
WfTSubEnv []        = ⊤
WfTSubEnv (x ∷ env) = WfType env x × WfTSubEnv env

lookup-< : ∀ {a} {A : Set a} {l : List A} {n} → n < length l → A
lookup-< {l = l} {n} <len                     with length l      | inspect length l
lookup-< {l = []} {n} (s≤s <len)                 | .(suc _)      | Reveal_·_is_.[ () ]
lookup-< {l = x ∷ l} {.0} (s≤s z≤n)              | .(suc _)      | Reveal_·_is_.[ eq ] = x
lookup-< {l = x ∷ l} {.(suc _)} (s≤s (s≤s <len)) | (suc (suc n)) | Reveal_·_is_.[ eq ]
  with suc n | length l | s≤s <len | lookup-< {l = l}
lookup-< {l = x ∷ l} {.(suc _)} (s≤s (s≤s <len)) | suc (suc n)   | Reveal_·_is_.[ refl ]
     | sn    | .sn      | s<len    | lkp = lkp s<len


-- Interpreting denotationally is not feasible, as impredicative polymorphism effectively requires
-- universes in full scale, which means denotational semantics requires power up to Set ω.

mutual
  typeDenot : ∀ {T} {env} → WfTSubEnv env → WfType env T → Set1
  typeDenot {.top} {env} wfe top-wf = Lift _ ⊤
  typeDenot {.(var _)} {env} wfe (var-wf <len) = varLookup wfe <len
  typeDenot {.(_ ⟶ _)} {env} wfe (⟶-wf s t) = typeDenot wfe s → typeDenot wfe t
  typeDenot {.(Π<: _ ∙ _)} {env} wfe (Π-wf s t) = {!!}

  varLookup : ∀ {env n} → WfTSubEnv env → n < length env → Set1
  varLookup {env} {n} wfe <len = {!!}

  -- typeDenot : ∀ {T env} → WfTSubEnv env → WfType env T → Set1
  -- typeDenot {.top} {env} wfe top-wf = Lift _ ⊤
  -- typeDenot {.(var _)} {env} wfe (var-wf <len)                                with length env | inspect length env
  -- typeDenot {.(var _)} {[]} wfe (var-wf (s≤s <len))                              | .(suc _) | Reveal_·_is_.[ () ]
  -- typeDenot {.(var _)} {x ∷ env} (fst , snd) (var-wf (s≤s z≤n))                  | .(suc _) | Reveal_·_is_.[ eq ] = Σ Set (λ R → R → typeDenot snd fst)
  -- typeDenot {.(var _)} {x ∷ env} (fst , snd) (var-wf (s≤s (s≤s {m} <len)))       | (suc (suc n)) | Reveal_·_is_.[ eq ]
  --   with suc n | length env | s≤s <len | typeDenot snd ∘ var-wf {env} {m}
  -- typeDenot {.(var (suc m))} {x ∷ env} (fst , snd) (var-wf (s≤s (s≤s {m} <len))) | suc (suc n) | Reveal_·_is_.[ refl ]
  --      | sn | .sn | s<len | tail = tail s<len
  -- typeDenot {.(_ ⟶ _)} {env} wfe (⟶-wf s t) = typeDenot wfe s → typeDenot wfe t
  -- typeDenot {.(Π<: _ ∙ _)} {env} wfe (Π-wf s t) = ∀ (S : Set) (R : S → typeDenot wfe s) → {!typeDenot (? , wfe) t!}
