module DSubSup where

open import Data.Nat
open import Data.List as List hiding (lookup)
open import Data.List.All as All hiding (lookup)
open import Data.List.Membership.Propositional

open import Data.Maybe as Maybe
open import Data.Empty

open import Data.Product
open import Function

open import Relation.Nullary
open import Relation.Binary
open import Relation.Binary.PropositionalEquality as ≡

BVar = ℕ

data Var : Set where
  vb vf : ℕ → Var

infix 10 _∙T
infix 10 ⟦T:_⋯_⟧
infixr 9 Π_⟶_

data Type : Set where
  top bot : Type
  _∙T     : Var → Type
  ⟦T:_⋯_⟧ : Type → Type → Type
  Π_⟶_    : Type → Type → Type


infixr 10 _$$_
infixr 8 LET_IN_
infix 9 ⟪T:=_⟫
infix 7 Λ_⟶_

mutual
  data Term : Set where
    var     : Var → Term
    val     : Val → Term
    _$$_    : Var → Var → Term
    LET_IN_ : Term → Term → Term

  data Val : Set where
    ⟪T:=_⟫ : Type → Val
    Λ_⟶_   : Type → Term → Val

record Openning (T : Set) : Set where
  field
    open-rec : ℕ → BVar → T → T

open Openning {{...}}

instance
 openVar : Openning Var
  open-rec ⦃ openVar ⦄ n v (vb x) with n ≟ x
  ... | yes _ = vf v
  ... | no _  = vb x
  open-rec ⦃ openVar ⦄ n v (vf x) = vf x

  openType : Openning Type
  open-rec ⦃ openType ⦄ n v top         = top
  open-rec ⦃ openType ⦄ n v bot         = bot
  open-rec ⦃ openType ⦄ n v (x ∙T)      = open-rec n v x ∙T
  open-rec ⦃ openType ⦄ n v ⟦T: S ⋯ U ⟧ = ⟦T: open-rec n v S ⋯ open-rec n v U ⟧
  open-rec ⦃ openType ⦄ n v (Π S ⟶ U)   = Π open-rec n v S ⟶ open-rec (suc n) v U

  mutual
    openTerm : Openning Term
    open-rec ⦃ openTerm ⦄ n v (var x)      = var $ open-rec n v x 
    open-rec ⦃ openTerm ⦄ n v (val x)      = val $ open-rec n v x
    open-rec ⦃ openTerm ⦄ n v (x $$ y)     = open-rec n v x $$ open-rec n v y
    open-rec ⦃ openTerm ⦄ n v (LET t IN u) = LET open-rec n v t IN open-rec (suc n) v u
  
    openVal : Openning Val
    open-rec ⦃ openVal ⦄ n v ⟪T:= T ⟫ = ⟪T:= open-rec n v T ⟫
    open-rec ⦃ openVal ⦄ n v (Λ T ⟶ t) = Λ open-rec n v T ⟶ open-rec n v t

open₀ : ∀ {T : Set} {{_ : Openning T}} → BVar → T → T
open₀ v t = open-rec 0 v t

Env = List Type

lookup-maybe : ∀ {a} {A : Set a} → List A → ℕ → Maybe A
lookup-maybe [] n = nothing
lookup-maybe (x ∷ l) n with length l ≟ n
... | yes _       = just x
... | no _        = lookup-maybe l n

infix 0 _⊢_∶_ _⊢_<∶_

mutual
  data _⊢_∶_ : Env → Term → Type → Set where
    ⊢-var : ∀ {Γ v T} → lookup-maybe Γ v ≡ just T → Γ ⊢ var (vf v) ∶ T
    ⊢-⟪⟫ : ∀ {Γ T} → Γ ⊢ val ⟪T:= T ⟫ ∶ ⟦T: T ⋯ T ⟧
    ⊢-Λ  : ∀ {Γ S U t} → S ∷ Γ ⊢ open₀ (length Γ) t ∶ U → Γ ⊢ val (Λ S ⟶ t) ∶ Π S ⟶ U
    ⊢-$$  : ∀ {Γ x y S U} → Γ ⊢ var (vf x) ∶ Π S ⟶ U → Γ ⊢ var (vf y) ∶ S → Γ ⊢ vf x $$ vf y ∶ U
    ⊢-let : ∀ {Γ s S u U} → Γ ⊢ s ∶ S → S ∷ Γ ⊢ open₀ (length Γ) u ∶ U → Γ ⊢ LET s IN u ∶ U
    ⊢-<:  : ∀ {Γ t S U} → Γ ⊢ t ∶ S → Γ ⊢ S <∶ U → Γ ⊢ t ∶ U

  data _⊢_<∶_ : Env → Type → Type → Set where
    ⊢-refl  : ∀ {Γ T} → Γ ⊢ T <∶ T
    ⊢-top   : ∀ {Γ T} → Γ ⊢ T <∶ top
    ⊢-bot   : ∀ {Γ T} → Γ ⊢ bot <∶ T
    ⊢-trans : ∀ {Γ S T U} → Γ ⊢ S <∶ T → Γ ⊢ T <∶ U → Γ ⊢ S <∶ U
    ⊢-⟦⟧-<∶ : ∀ {Γ S T₁ T₂ U} → Γ ⊢ S <∶ T₁ → Γ ⊢ T₂ <∶ U → Γ ⊢ ⟦T: T₁ ⋯ T₂ ⟧ <∶ ⟦T: S ⋯ U ⟧
    ⊢-Π-<∶  : ∀ {Γ S₁ U₁ S₂ U₂} → Γ ⊢ S₂ <∶ S₁ → S₂ ∷ Γ ⊢ U₁ <∶ U₂ → Γ ⊢ Π S₁ ⟶ U₁ <∶ Π S₂ ⟶ U₂
    ⊢-∙T-<∶ : ∀ {Γ v S U} → Γ ⊢ var (vf v) ∶ ⟦T: S ⋯ U ⟧ → Γ ⊢ (vf v) ∙T <∶ U
    ⊢-<∶-∙T : ∀ {Γ v S U} → Γ ⊢ var (vf v) ∶ ⟦T: S ⋯ U ⟧ → Γ ⊢ S <∶ (vf v) ∙T


mutual
  type : ∀ Γ t → Maybe (Σ[ T ∈ Type ] (Γ ⊢ t ∶ T))
  type Γ (var (vb _)) = nothing
  type Γ (var (vf x)) with lookup-maybe Γ x | inspect (lookup-maybe Γ) x
  type Γ (var (vf x)) | just T | Reveal_·_is_.[ eq ] = just (_ , (⊢-var eq))
  type Γ (var (vf x)) | nothing | Reveal_·_is_.[ eq ] = nothing
  type Γ (val ⟪T:= T ⟫) = just (_ , ⊢-⟪⟫)
  type Γ (val (Λ T ⟶ u)) with type (T ∷ Γ) (open₀ (length Γ) u)
  ... | res = {!!}
  type Γ (x $$ x₁) = {!!}
  type Γ (LET t IN t₁) = {!!}

  subType : ∀ Γ S U → Maybe (Γ ⊢ S <∶ U)
  subType = {!!}
