
-- try to implement a cbv interpreter

module STLC2 where

open import Data.Nat
open import Data.List as List hiding (lookup)
open import Data.List.All as All hiding (lookup)
open import Data.List.Membership.Propositional

open import Data.Maybe as Maybe
open import Data.Empty

open import Data.Product

open import Relation.Nullary
open import Relation.Binary
open import Relation.Binary.PropositionalEquality as ≡

data Type : Set where
  bool : Type
  _⟶_  : Type → Type → Type

Env = List Type

infixr 10 Λ_⟶_
infixl 9 _$$_

data Term : Set where
  var     : ℕ → Term
  true    : Term
  false   : Term
  Λ_⟶_    : Type → Term → Term
  _$$_    : Term → Term → Term

infix 0 _⊢_∶_

lookup-maybe : ∀ {a} {A : Set a} → List A → ℕ → Maybe A
lookup-maybe [] n                 = nothing
lookup-maybe (x ∷ l) n with length l ≟ n
...                       | yes p = just x
...                       | no ¬p = lookup-maybe l n

data _⊢_∶_ : Env → Term → Type → Set where
  t⊢bool : ∀ {Γ} → Γ ⊢ true ∶ bool
  f⊢bool : ∀ {Γ} → Γ ⊢ false ∶ bool
  v⊢lkup : ∀ {T Γ n} → lookup-maybe Γ n ≡ just T → Γ ⊢ var n ∶ T
  Λ⊢⟶    : ∀ {S T Γ t} → S ∷ Γ ⊢ t ∶ T → Γ ⊢ Λ S ⟶ t ∶ S ⟶ T
  $$⊢    : ∀ {S T Γ x y} → Γ ⊢ x ∶ S ⟶ T → Γ ⊢ y ∶ S → Γ ⊢ x $$ y ∶ T

type-eq : Decidable {A = Type} _≡_
type-eq bool     bool                                 = yes refl
type-eq bool     (T₁ ⟶ T₂)                            = no (λ ())
type-eq (S ⟶ S₁) bool                                 = no (λ ())
type-eq (S ⟶ S₁) (T ⟶ T₁) with type-eq S T | type-eq S₁ T₁
type-eq (S ⟶ S₁) (.S ⟶ .S₁)  | yes refl    | yes refl = yes refl
type-eq (S ⟶ S₁) (.S ⟶ T₁)   | yes refl    | no ¬p    = no (λ { refl → ¬p refl })
type-eq (S ⟶ S₁) (T ⟶ .S₁)   | no ¬p       | yes refl = no (λ { refl → ¬p refl })
type-eq (S ⟶ S₁) (T ⟶ T₁)    | no ¬p       | no _     = no λ { refl → ¬p refl }

type-decSetoid : DecSetoid _ _
type-decSetoid = ≡.decSetoid type-eq

lookup : ∀ n Γ → Maybe (Σ[ T ∈ Type ] (Γ ⊢ (var n) ∶ T))
lookup n Γ with lookup-maybe Γ n | inspect (lookup-maybe Γ) n
lookup n Γ    | just T           | Reveal_·_is_.[ eq ] = just (T , v⊢lkup eq)
lookup n Γ    | nothing          | Reveal_·_is_.[ eq ] = nothing

type : ∀ t Γ → Maybe (Σ[ T ∈ Type ] (Γ ⊢ t ∶ T))
type (var n)   Γ                                                               = lookup n Γ
type true      Γ                                                               = just (bool , t⊢bool)
type false     Γ                                                               = just (bool , f⊢bool)
type (Λ S ⟶ t) Γ with type t (S ∷ Γ)
...                 | just (T , t∶T)                                           = just ((S ⟶ T) , Λ⊢⟶ t∶T)
...                 | nothing                                                  = nothing
type (x $$ y)  Γ with type x Γ
...                 | just (bool , _)                                          = nothing
...                 | just (S ⟶ T , x∶S⟶T) with type y Γ
...                                           | just (S′ , y∶S′) with type-eq S S′
...                                                                 | yes refl = just (T , $$⊢ x∶S⟶T y∶S′)
...                                                                 | no ¬p    = nothing
type (x $$ y)  Γ    | just ((S ⟶ T) , x∶S⟶T)  | nothing                        = nothing
type (x $$ y)  Γ    | nothing                                                  = nothing


mutual
  VEnv = List Value

  infix 10 ⟨_⟩Λ_⟶_

  data Value : Set where
    true-val  : Value
    false-val : Value
    ⟨_⟩Λ_⟶_   : VEnv → Type → Term → Value

mutual
  infix 0 _∶ᵥ_
  data _∶ᵥ_ : Value → Type → Set where
    tv⊢bool : true-val ∶ᵥ bool
    fv⊢bool : false-val ∶ᵥ bool
    Λ⊢⟶     : ∀ {Γ venv T S t} →
                EnvConsistent Γ venv →
                Γ ⊢ Λ T ⟶ t ∶ T ⟶ S →
                (⟨ venv ⟩Λ T ⟶ t) ∶ᵥ T ⟶ S
  
  data EnvConsistent : Env → VEnv → Set where
    [] : EnvConsistent [] []
    _∷_ : ∀ {T Γ v vs} → v ∶ᵥ T → EnvConsistent Γ vs → EnvConsistent (T ∷ Γ) (v ∷ vs)

-- reduction closure
infix 0 ⟨_⟩_⇒*_
data ⟨_⟩_⇒*_ : List Value → Term → Value → Set where
  true-red  : ∀ {e} → ⟨ e ⟩ true ⇒* true-val
  false-red : ∀ {e} → ⟨ e ⟩ false ⇒* false-val
  Λ-red     : ∀ {e T t} → ⟨ e ⟩ Λ T ⟶ t ⇒* ⟨ e ⟩Λ T ⟶ t
  var-red   : ∀ {v e n} → lookup-maybe e n ≡ just v → ⟨ e ⟩ var n ⇒* v
  $$-red    : ∀ {e x ex T t y vy vt} →
               ⟨ e ⟩ x ⇒* ⟨ ex ⟩Λ T ⟶ t →
               ⟨ e ⟩ y ⇒* vy →
               ⟨ vy ∷ ex ⟩ t ⇒* vt →
               ⟨ e ⟩ x $$ y ⇒* vt

consistent-length : ∀ {Γ e} → EnvConsistent Γ e → length Γ ≡ length e
consistent-length []            = refl
consistent-length (_ ∷ con)
  rewrite consistent-length con = refl

consistent-lookup : ∀ {n Γ e T} →
                      lookup-maybe Γ n ≡ just T →
                      EnvConsistent Γ e →
                      Σ[ v ∈ Value ] ((lookup-maybe e n ≡ just v) × (v ∶ᵥ T))
consistent-lookup {n} {.[]} {.[]} {T} () []
consistent-lookup {n} {S ∷ Γ} {v ∷ e} {T} lkup (v∶ᵥS ∷ con) with length Γ ≟ n | length e ≟ n
consistent-lookup {n} {S ∷ Γ} {v ∷ e} {.S} refl (v∶ᵥS ∷ con)   | yes p        | yes p₁ = v , refl , v∶ᵥS
consistent-lookup {n} {S ∷ Γ} {v ∷ e} {.S} refl (v∶ᵥS ∷ con)   | yes p        | no ¬p  = ⊥-elim (¬p (trans (sym (consistent-length con)) p))
consistent-lookup {n} {S ∷ Γ} {v ∷ e} {T}  lkup (v∶ᵥS ∷ con)   | no ¬p        | yes p  = ⊥-elim (¬p (trans (consistent-length con) p))
consistent-lookup {n} {S ∷ Γ} {v ∷ e} {T}  lkup (v∶ᵥS ∷ con)   | no ¬p        | no ¬p₁ = consistent-lookup lkup con

eval : ℕ → ∀ {Γ t T e} → Γ ⊢ t ∶ T → EnvConsistent Γ e →
       Maybe (Σ[ v ∈ Value ] ((v ∶ᵥ T) × (⟨ e ⟩ t ⇒* v)))
eval zero    {Γ} {t}        {T}     {e} t∶T           con   = nothing
eval (suc n) {Γ} {.true}    {.bool} {e} t⊢bool        con   = just (true-val , tv⊢bool , true-red)
eval (suc n) {Γ} {.false}   {.bool} {e} f⊢bool        con   = just (false-val , fv⊢bool , false-red)
eval (suc n) {Γ} {.(var _)} {T}     {e} (v⊢lkup lkup) con
  with consistent-lookup lkup con
...  | v , vlkup , v∶ᵥT                                     = just (v , v∶ᵥT , var-red vlkup)
eval (suc n) {Γ} {Λ S ⟶ t} {.S ⟶ T} {e} (Λ⊢⟶ t∶T)       con = just (⟨ e ⟩Λ S ⟶ t , Λ⊢⟶ con (Λ⊢⟶ t∶T) , Λ-red)
eval (suc n) {Γ} {x $$ y}  {T}      {e} ($$⊢ x∶S⟶T y∶S) con
  with eval n x∶S⟶T con | eval n y∶S con
...  | just (.(⟨ _ ⟩Λ _ ⟶ _) , Λ⊢⟶ con′ (Λ⊢⟶ t∶T) , xsteps) | just (u , u∶ᵥS , ysteps)
     with eval n t∶T (u∶ᵥS ∷ con′)
...     | just (v , v∶ᵥT , steps)                                         = just (v , v∶ᵥT , $$-red xsteps ysteps steps)
...     | nothing                                                         = nothing
eval (suc n) {Γ} {x $$ y} {T} {e} ($$⊢ x∶S⟶T y∶S) con | just _  | nothing = nothing
eval (suc n) {Γ} {x $$ y} {T} {e} ($$⊢ x∶S⟶T y∶S) con | nothing | res2    = nothing

run : ℕ → ∀ t → Maybe (∃₂ λ T v → (v ∶ᵥ T) × (⟨ [] ⟩ t ⇒* v))
run n t with type t []
run n t    | just (T , t∶T) = Maybe.map (T ,_) (eval n t∶T [])
run n t    | nothing        = nothing


eval′ : ∀ {t Γ T e} → Γ ⊢ t ∶ T → EnvConsistent Γ e → Σ[ v ∈ Value ] ((v ∶ᵥ T) × (⟨ e ⟩ t ⇒* v))
eval′ {.true} {Γ} {.bool} {e} t⊢bool con = true-val , tv⊢bool , true-red
eval′ {.false} {Γ} {.bool} {e} f⊢bool con = false-val , fv⊢bool , false-red
eval′ {var x} {Γ} {T} {e} (v⊢lkup lkup) con
  with consistent-lookup lkup con
... | v , vlkup , v∶ᵥT = v , v∶ᵥT , var-red vlkup
eval′ {Λ .S ⟶ t} {Γ} {S ⟶ T} {e} (Λ⊢⟶ t∶T) con = ⟨ e ⟩Λ S ⟶ t , Λ⊢⟶ con (Λ⊢⟶ t∶T) , Λ-red
eval′ {x $$ y} {Γ} {T} {e} ($$⊢ x∶S⟶T y∶S) con
  with eval′ x∶S⟶T con | eval′ y∶S con
... | .(⟨ _ ⟩Λ _ ⟶ _) , Λ⊢⟶ con′ (Λ⊢⟶ t∶T) , xsteps | u , u∶ᵥS , ysteps with eval′ t∶T (u∶ᵥS ∷ con′)
... | qwe = {!!}
