\documentclass{article}
\usepackage[a4paper,
total={6.5in, 10in},
voffset=5px]{geometry}
\usepackage{fancyhdr}
\usepackage[fleqn]{amsmath}
\usepackage{titling}
\usepackage[shortlabels]{enumitem}
\usepackage{amsthm}
\usepackage{amssymb}
\usepackage[bb=boondox]{mathalfa}
\usepackage{ifthen}
\usepackage{amsfonts}
\usepackage{algorithm}
\usepackage{algpseudocode}
\usepackage{indentfirst}
\usepackage{tikz}
\usepackage{tkz-graph}
\usepackage{float}
\usepackage{hyperref}
\usepackage{cleveref}
\usepackage{MnSymbol}
\usepackage{ifthen}
\usepackage{listings}
\usepackage{bussproofs}
\usepackage{lineno}
\usepackage{multicol}
\usepackage{biblatex}

\bibliography{f2}

\linenumbers

\renewcommand\linenumberfont{\normalfont\small\sffamily}
\fontfamily{qcr}\selectfont

\usetikzlibrary{positioning}

\hypersetup{
  colorlinks=true,
  allcolors=blue,
}

% \pagestyle{fancy}

\algnewcommand\algorithmicinput{\textbf{Input:}}
\algnewcommand\Input{\item[\algorithmicinput]}
\algnewcommand\algorithmicoutput{\textbf{Output:}}
\algnewcommand\Output{\item[\algorithmicoutput]}
\algnewcommand\Ret[1]{\State \textbf{return }$#1$ }
\algnewcommand\Clarif{\item[\textbf{Clarification: }]}
\algnewcommand\Assign[2]{\State $#1 = #2$}

\algnewcommand \Foreach[3]{\For{$#1 \textbf{ in } #2$}
  #3
  \EndFor
}

\algnewcommand \IIf[2]{\If{$#1$}
  #2
  \EndIf}

\algnewcommand \IIfE[3]{\If{$#1$}
  #2
  \Else
  #3
  \EndIf}

\algnewcommand \Func[3]{\Function{#1}{#2}
  #3
  \EndFunction}

\algnewcommand \Whl[2]{
  \While{$#1$}
  #2
  \EndWhile
}

\algrenewcommand\textproc{}
\newcommand{\LineComment}[1]{\\/*\textit{#1}\hfill*/}

\definecolor{codegreen}{rgb}{0,0.6,0}
\definecolor{codegray}{rgb}{0.5,0.5,0.5}
\definecolor{codepurple}{rgb}{0.58,0,0.82}
\definecolor{backcolour}{rgb}{0.95,0.95,0.92}

\lstdefinestyle{mystyle}{
    backgroundcolor=\color{backcolour},   
    commentstyle=\color{codegreen},
    keywordstyle=\color{magenta},
    numberstyle=\small\color{codegray},
    stringstyle=\color{codepurple},
    breaklines=true,                 
    captionpos=b,                    
    numbers=left,                    
    numbersep=5pt,
    showspaces=false,                
    showstringspaces=false,
    showtabs=false,                  
    tabsize=4
}
\lstset{style=mystyle}

\newcommand{\axiom}[1]{\AxiomC{$#1$}}
\newcommand{\unary}[1]{\UnaryInfC{$#1$}}
\newcommand{\binary}[1]{\BinaryInfC{$#1$}}
\newcommand{\trinary}[1]{\TrinaryInfC{$#1$}}
\newcommand{\quaternary}[1]{\QuaternaryInfC{$#1$}}
\newcommand{\rlabel}[1]{\RightLabel{#1}}

\DeclareFieldFormat{url}{\ifhyperref{\href{#1}{Link.}}{\url{#1}}}
\nocite{*}

\theoremstyle{definition}
\newtheorem{question}{Question}
\newtheorem{lemma}{Lemma}
\newtheorem{corollary}{Corollary}
\newtheorem{definition}{Definition}

\begin{document}
\pagenumbering{arabic}
It seems that the attempt to prove canonical forms of object types reveals some issues
with the current formalization of this calculus. I will try to summarize what I see
from the proofs and try to understand what leads to this situation.

\section{Object Subtyping}

During the definition phase, I can already see some differences with the definition of
DOT. One major difference is how I attempt to achieve subtyping for objects. In my
code, I define it as follows:

\begin{prooftree}
  \axiom{\Gamma , y : \mu\{DS_1 ++ \{ a : T \} ++ DS_2 \} \vdash open\ y\ T <: open\
    y\ U }
  \rlabel{\textbf{SUB-FIELD}}
  \unary{\Gamma \vdash \mu\{DS_1 ++ \{ a : T \} ++ DS_2 \} <: \mu\{DS_1 ++ \{ a : U \}
    ++ DS_2 \}}
\end{prooftree}

where $y$ is not captured anywhere. Note the opening up top. We need the opening
there, to ensure the type we compare is meaningful, e.g. when $U \equiv 0.A$, we want
to open it and make it $y.A$ when comparing subtyping relation.

The rule for type declarations work similarly.

This type of definition, I believe, introduces the major difference to my variant of
DOT. To be specific, one character of recursive type $\mu$ in DOT, is that
wrapping/unwrapping it can only be done when a variable of such types is inside of the
context.

\nolinenumbers
\begin{multicols}{2}
  \begin{prooftree}
    \axiom{\Gamma\vdash x : open\ x\ T}
    \rlabel{\textbf{TY-REC-INTRO}}
    \unary{\Gamma\vdash x : \mu\{T\}}
  \end{prooftree}

    \begin{prooftree}
    \axiom{\Gamma\vdash x : \mu\{T\}}
    \rlabel{\textbf{TY-REC-ELIM}}
    \unary{\Gamma\vdash x : open\ x\ T}
  \end{prooftree}
\end{multicols}
\linenumbers

This turns out to be essential: it prevents the previous form of inference rule, which
we are forced to come up with a free variable and push the object to the object in
order to argue about the subtype relation.

Another consequence due to these two rules in original DOT is, whenever an object is
pushed into the context, that object type only represents ``itself''; while in my
definition, the object type represents all types alike. To be more specific, consider
following object type (assuming $T_1$ and $T_2$ come from somewhere else, irrelevant,
and well defined)

\nolinenumbers
\begin{lstlisting}[language=Haskell, mathescape=true]
  $\mu$ {
    A : $T_1$ .. $\top$
    B : $\bot$ .. $T_2$
    C : 0.A .. 0.B
  }
\end{lstlisting}
\linenumbers

gets pushed into the type context, and assigned to the variable $x$. Consider the
field $C$, in original DOT, we can \textbf{only} associate this type with $x$, namely

\nolinenumbers
\begin{prooftree}
  \axiom{\Gamma \vdash T_1 <: x.A}
  \axiom{\Gamma \vdash x.B <: T_2}
  \binary{\Gamma \vdash x : \{ C : T_1 .. T_2 \}}
\end{prooftree}
\linenumbers

We do not have to mention $x$ from the context, other than the fact that we know it's
there and what it looks like in order to make the proper derivation. On the other
hand, if there is another variable, $z$, in the context of the same type already, we
cannot rely on $z$ to give the same conclusion. However, in my current definition, it
achieves the latter; or in fact to say that we are forced to delegate the subtyping
judgment to a temporary variable, $y$:

\nolinenumbers
\begin{figure}[H]
  \begin{prooftree}
    \axiom{\Gamma , z : \mu\{A, B, C\} \vdash T_1 <: z.A}
    \axiom{\Gamma , z : \mu\{A, B, C\} \vdash z.B <: T_2}
    \binary{\Gamma \vdash x : \mu \{ C : T_1 .. T_2 \} }
  \end{prooftree}
  \caption{subtyping for type declaration}\label{fig:subtypdec}
\end{figure}
\linenumbers

I hope this illustrates the difference clearly. From the proof end, this gives big
trouble when I attempted to close the proof recipe. It turns out to be very difficult
to prove the derivation shown in \cref{fig:subtypdec} can also be derived purely using
$\Gamma$ only. This arguably can be achieved by renaming lemmas, but that complicates
the proof, not to mention there is one condition for the renaming lemma looks
phishy. I am not able to tell which problem it is: it can be either the limitation of
Coq's structural induction that fails to intelligently tell the argument is
semantically smaller, or there are occasions where the argument in fact is not smaller
and hence the proof diverges. That essentially prevents me from proceeding with my
current approach and the whole proof gets stuck at the current stage.

As indicated previously, pushing the object to the typing context every time
establishing a subtype relation seems problematic. This means that we might not be
able to establish subtyping relations between object types directly without going
through a variable in the context, and this argument directly implies the current DOT
is the ``right'' way to go. I am not sure what fixes can be done in the current
approach. However, I spotted a paper in ECOOP this year,
\href{https://2018.ecoop.org/event/ecoop-2018-papers-dependent-types-for-class-based-mutable-objects}{Dependent
  Types for Class-based Mutable Objects}, which seems largely relevant to my
project. I will check this paper once it's out there.

\section{Next Attempt}

I have two different thoughts for the next attempt.

\begin{enumerate}
\item Clearly the issue here is caused by the recursive nature of the object
  type. What if the object is not recursive? Then it becomes a record. I think at
  least I can check out how easy or difficult it is to come up with algorithmic typing
  by taking recursive type away entirely. I cannot forsee this type of issue up ahead,
  potentially because there are too many features to deal with at once: recursive
  types, path dependent types, dependent functions, etc. Hopefully attempting to
  define a record based language should largely reduce the difficulties and allow a
  fairly straightforward algorithmic typing to be defined. I plan to maintain the idea
  of first class object/record (so no intersect operation to join fields), so that we
  at least get to learn how to reasoning a list of fields altogether.
  
\item Continue staying with DOT but give up fitting objects inside of a single
  lattice. That said, we need to admit there isn't a lattice for types in DOT to begin
  with. Acknowledging that, the next thought is to \textbf{control}
  wrapping/unwrapping of $\mu$ in order to remove the introduction/elimination rules
  for the recursive types.
\end{enumerate}

I personally intend to try the first option, though the second one is also reasonable
and conservative to begin with. I have pilled up enough tactics to quickly solve
various of lemmas so I believe lots of ideas can be quickly examined. Do you have any
suggestion on that?

\end{document}